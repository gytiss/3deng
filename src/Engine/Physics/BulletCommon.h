#ifndef BULLETCOMMON_H_INCLUDED
#define BULLETCOMMON_H_INCLUDED

#include <BitMask.h>
typedef BitMask32 CollideMask;

class BulletNode
{
public:
    enum NodeType
    {
        e_NodeTypeGhost = 0,
        e_NodeTypeRigidBody = 1,
        e_NodeTypeCollision
    };

    BulletNode(NodeType type, void* nodePointer)
    : m_NodeType( type ),
      m_NodePointer( nodePointer )
    {
    }

    NodeType GetNodeType()
    {
        return m_NodeType;
    }

    void* GetNodePtr()
    {
        return m_NodePointer;
    }

private:
    NodeType m_NodeType;
    void* m_NodePointer;
};


#endif // BULLETCOMMON_H_INCLUDED
