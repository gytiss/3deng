#include "FontLoader.h"
#include <MsgLogger.h>
#include <StringUtils.h>
#include <Resources/FileManager.h>
#include <memory>

FontLoader* FontLoader::m_FontLoaderInstance = NULL;

    //////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS METHODS
	//////////////////////////////////////////////////////////////////////////

	//-----------------------------------------------------------------------

    FontLoader::FontLoader()
    : m_FreeTypeLibrary(),
      m_AllLoadedFonts(),
      m_FontFacesToMemoryMap()
    {
        FT_Error err = FT_Init_FreeType( &m_FreeTypeLibrary );
        if ( err )//Error has occured
        {
            MsgLogger::log(MsgLogger::ERROR_MSG,"An error has occured during the initialisation of FreeType library.");
        }
    }

	//-----------------------------------------------------------------------

	//////////////////////////////////////////////////////////////////////////
	// DESTRUCTORS METHODS
	//////////////////////////////////////////////////////////////////////////

	//-----------------------------------------------------------------------

    FontLoader::~FontLoader()
    {
        for(std::list<FT_Face>::iterator it = m_AllLoadedFonts.begin(); it != m_AllLoadedFonts.end(); ++it)
        {
            std::map<FT_Face, FT_Byte*>::iterator memIt = m_FontFacesToMemoryMap.find(*it);
            FT_Done_Face(*it);

            if( memIt != m_FontFacesToMemoryMap.end() )
            {
                delete[] memIt->second;
            }

        }
        FT_Done_FreeType(m_FreeTypeLibrary);
    }

	//-----------------------------------------------------------------------

	//////////////////////////////////////////////////////////////////////////
	// PUBLIC METHODS
	//////////////////////////////////////////////////////////////////////////

	//-----------------------------------------------------------------------

	FontLoader* FontLoader::getInstance()
	{
        if( m_FontLoaderInstance == NULL )
        {
            m_FontLoaderInstance = new FontLoader();
        }

        return m_FontLoaderInstance;
	}

	//-----------------------------------------------------------------------

    FT_Face FontLoader::loadNewFaceFromFile(std::string fontFileName,
                                            FileManager& fileManager)
    {
        FT_Face newFace = nullptr;
        FileManager::File fontFile;

        if( fileManager.OpenFile( fontFileName, FileManager::e_FileTypeBinary, fontFile ) )
        {
            const size_t fontDataSize = fontFile.Size();
            std::unique_ptr<FT_Byte[]> fontData( new FT_Byte[fontDataSize] );
            if( fontFile.Read( fontData.get(), 1, fontDataSize ) )
            {
                newFace = loadNewFaceFromMemory( fontData.get(), fontDataSize );

                if( newFace )
                {
                    fontData.release();
                }
            }
        }

        return newFace;
    }

    //-----------------------------------------------------------------------

    FT_Face FontLoader::loadNewFaceFromMemory(const FT_Byte* base_address, size_t numBytesToRead)
    {
        FT_Face newFace = nullptr;

        /*
            Certain font formats allow several font faces to be embedded in a single file.
            This index tells which face you want to load. An error will be returned if its value is too large.
            Index 0 always work though.
        */
        const int FACE_INDEX = 0;

        FT_Error err = FT_New_Memory_Face( m_FreeTypeLibrary, base_address,  static_cast<FT_Long>( numBytesToRead ), FACE_INDEX, &newFace );

        if( err )
        {
            MsgLogger::log(MsgLogger::ERROR_MSG,"FreeType: unknown error while loading font from memory address: " + numToString(reinterpret_cast<uintptr_t>(base_address)));
            return nullptr;
        }

        m_AllLoadedFonts.push_back(newFace); // Keep track of all loaded fonts
        return newFace;
    }

    //-----------------------------------------------------------------------

    size_t FontLoader::getNumofFacesInAFont(FT_Face font)
    {
        return font->num_faces;
    }

    //-----------------------------------------------------------------------

    void FontLoader::deleteFace(FT_Face face)
    {
        FT_Error err = FT_Done_Face(face);

        if( err )
        {
            MsgLogger::log(MsgLogger::ERROR_MSG,"FreeType: unknown error while deleting face");
        }
        else
        {
            m_AllLoadedFonts.remove(face);
            std::map<FT_Face, FT_Byte*>::iterator it = m_FontFacesToMemoryMap.find(face);
            if( it != m_FontFacesToMemoryMap.end() )
            {
                m_FontFacesToMemoryMap.erase( it );
            }
        }
    }

	//-----------------------------------------------------------------------

    void FontLoader::printFontDebugInfo()
    {
        std::string dbgMsg;

        dbgMsg += "-------------------FontLoader Debug Info-------------------\n";

        for(std::list<FT_Face>::iterator it = m_AllLoadedFonts.begin(); it != m_AllLoadedFonts.end(); ++it)
        {
            dbgMsg += "Face \""+std::string((*it)->family_name)+"\":\n"+
            "Face style: "+std::string((*it)->family_name)+"\n"+
            "Is scalable format: "+(((*it)->units_per_EM != 0)?"Yes":"No")+"\n"
            "Number of face:"+numToString((*it)->num_faces)+"\n"+
            "Face index: "+numToString((*it)->face_index)+"\n"+
            "Number of glyphs: "+numToString((*it)->num_glyphs)+"\n"+
            "Number of fixed sizes: "+numToString((*it)->num_fixed_sizes)+"\n"+
            "Available fixed sizes: \n";
            for(int i=0 ; i < (*it)->num_fixed_sizes ; i++)
            {
                dbgMsg += "       Height: "+numToString((*it)->available_sizes[i].height)+
                                    " Width: "+numToString((*it)->available_sizes[i].width)+
                                    " Size: "+numToString((*it)->available_sizes[i].size)+
                                    " x_ppem: "+numToString((*it)->available_sizes[i].x_ppem)+
                                    " y_ppem: "+numToString((*it)->available_sizes[i].y_ppem);
            }

            dbgMsg += "Scalable outlines properties:\nBounding Box:\n       xMin:"+numToString((*it)->bbox.xMin)+" "+numToString((*it)->bbox.yMin)+"\n"+
            "       xMax:"+numToString((*it)->bbox.xMax)+" "+numToString((*it)->bbox.yMax)+"\n"+
            "Units per EM: "+numToString((*it)->units_per_EM)+"\n"+
            "Ascender: "+numToString((*it)->ascender)+"\n"+
            "Descender: "+numToString((*it)->descender)+"\n"+
            "Height: "+numToString((*it)->height)+"\n"+
            "Max Advance Width: "+numToString((*it)->max_advance_width)+"\n"+
            "Max Advance Height: "+numToString((*it)->max_advance_height)+"\n"+
            "Underline Position: "+numToString((*it)->underline_position)+"\n"+
            "Underline Thickness: "+numToString((*it)->underline_thickness)+"\n";

            dbgMsg += "\n";
        }

        dbgMsg += "-----------------------------------------------------------\n";

        MsgLogger::log(MsgLogger::EVENT_MSG,dbgMsg);
    }

    //-----------------------------------------------------------------------

    void FontLoader::setupFacePixelSize(FT_Face face, long char_wdth, long char_height, size_t h_res, size_t v_res)
    {
        FT_Error err = FT_Set_Char_Size( face, char_wdth, char_height, static_cast<FT_UInt>( h_res ), static_cast<FT_UInt>( v_res ) );

        if( err )
        {
            MsgLogger::log(MsgLogger::ERROR_MSG,"FreeType: unknown error while setting up face pixel size");
        }
    }

    //-----------------------------------------------------------------------

    FT_GlyphSlot FontLoader::getCharBitmap(FT_Face face, unsigned long char_code,FT_UInt* gIndex)
    {
        FT_UInt glyphIndex = FT_Get_Char_Index( face, char_code );

        FT_Error err = FT_Load_Glyph( face, glyphIndex, FT_LOAD_RENDER | FT_LOAD_FORCE_AUTOHINT);

        if( err )
        {
            MsgLogger::log(MsgLogger::ERROR_MSG,"FreeType: unknown error while loading glyphs in function \"getCharBitmap()\"");
            return NULL;
        }

        if( face->glyph->format != FT_GLYPH_FORMAT_BITMAP )
        {
           // err = FT_Render_Glyph( face->glyph, FT_RENDER_MODE_NORMAL );

            if( err )
            {
                MsgLogger::log(MsgLogger::ERROR_MSG,"FreeType: unknown error while rendering glyph \"getCharBitmap()\"");
                return NULL;
            }
        }

        return face->glyph;
    }

	//-----------------------------------------------------------------------

	//////////////////////////////////////////////////////////////////////////
	// PRIVATE METHODS
	//////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
