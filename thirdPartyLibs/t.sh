#!/usr/bin/env bash

EXTRA_CMAKE_FLAGS="-DBUILD_SAMPLES=OFF \
                   -DBUILD_SHARED_LIBS=ON \
                   -DBUILD_LUA_BINDINGS=ON \
                   -DLUA_LIBRARIES=$LIB_LUA_BINARIES_DIR/<<ANDROID_ARCH>>/lib/liblua.a \
                   -DLUA_INCLUDE_DIR=$LIB_LUA_BINARIES_DIR/<<ANDROID_ARCH>>/include \
                   -DFREETYPE_LINK_DIRS=$LIB_FREE_TYPE_BINARIES_DIR/<<ANDROID_ARCH>>/lib \
                   -DFREETYPE_INCLUDE_DIRS=$LIB_FREE_TYPE_BINARIES_DIR/<<ANDROID_ARCH>>/include/freetype2 \
                   -DFREETYPE_LIBRARY=-lfreetype"

ReplaceArchMarkersWithArchName ()
{
    local STR_TO_DO_SUBSTITUTION_ON="$1"
    local ARCH_NAME="$2"
    
    local SED_SCRIPT_STR="s/<<ANDROID_ARCH>>/$ARCH_NAME/g"

    echo "$STR_TO_DO_SUBSTITUTION_ON" | sed -r $SED_SCRIPT_STR
}

RelativePathBetweenTwoAbsolutePaths ()
{
    # both $1 and $2 are absolute paths beginning with /
    # returns relative path to $2/$target from $1/$source
    local source=$1
    local target=$2

    local common_part=$source # for now
    local result="" # for now

    while [[ "${target#$common_part}" == "${target}" ]]; do
        # no match, means that candidate common part is not correct
        # go up one level (reduce common part)
        common_part="$(dirname $common_part)"
        # and record that we went back, with correct / handling
        if [[ -z $result ]]; then
            result=".."
        else
            result="../$result"
        fi
    done

    if [[ $common_part == "/" ]]; then
        # special case for root (no common path)
        result="$result/"
    fi

    # since we now have identified the common part,
    # compute the non-common part
    local forward_part="${target#$common_part}"

    # and now stick all parts together
    if [[ -n $result ]] && [[ -n $forward_part ]]; then
        result="$result$forward_part"
    elif [[ -n $forward_part ]]; then
        # extra slash removal
        result="${forward_part:1}"
    fi

    echo $result
}

RelativePathBetweenTwoAbsolutePaths "/home/john/projects/tank/android-project/libs/mips64/" "/home/john/projects/tank/thirdPartyLibs/android/libfreetype/mips64/lib/"