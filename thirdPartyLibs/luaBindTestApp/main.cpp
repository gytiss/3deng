extern "C" 
{
    #include <lua.hpp>
}
#include <luabind/luabind.hpp> 
#include <luabind/operator.hpp>

#include <iostream>

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

int luabindErrorHandler( lua_State* L ) ///< Fetch error message from the top of the stack and print it
{
    // log the error message
    luabind::object msg( luabind::from_stack( L, -1 ) );
    std::ostringstream str;
    str << "lua> run-time error: " << msg;
    std::cout << str.str() << std::endl;

    // log the callstack
    std::string traceback = luabind::call_function<std::string>( luabind::globals(L)["debug"]["traceback"] );
    traceback = std::string( "lua> " ) + traceback;
    std::cout << traceback.c_str() << std::endl;

    // return unmodified error object
    return 1;
}

bool IsRangeNameValid(const std::string& rangeName)
{
    bool retVal = false;
    
    static const char* c_RangeNames[] =
    {
        "RANGE_1",
        "RANGE_2"
    };
    
    for(size_t i = 0; i < ARRAY_SIZE(c_RangeNames); i++)
    {
        if( rangeName.compare( c_RangeNames[i] ) == 0 )
        {
            retVal = true;
            break;
        }
    }
    
    return retVal;
}

static const char* MandatoryRangesTableMembers[] =
{
    "MinInputVal",
    "MaxInputVal",
    "MinOutputVal",
    "MaxOutputVal"
};

int main(int argc, char* argv[])
{
    bool ok = true;
    
    // Create a new lua state
    lua_State *myLuaState = luaL_newstate();

    luaL_openlibs(myLuaState);
    
    const int loadFileErrCode =  luaL_loadfile(myLuaState, "/home/john/projects/tank/thirdPartyLibs/testApp/InputRanges.lua");
    if( loadFileErrCode == LUA_ERRFILE )
    {
        ok = false;
        std::cout << "Failed to open file 'InputRanges.lua'" << std::endl;
    }
    else if( loadFileErrCode == LUA_ERRSYNTAX )
    {
        std::cout << "Failed with syntax errors during the precompilation of file 'InputRanges.lua'" << std::endl;
        luabindErrorHandler(myLuaState);
        ok = false;
    }
    else if( loadFileErrCode == LUA_ERRMEM )
    {
        std::cout << "Lua memory allocation error while trying to load file 'InputRanges.lua'" << std::endl;
        ok = false;
    }
    
    if( ok && ( lua_pcall(myLuaState, 0, LUA_MULTRET, 0) != 0 ) )
    {
        ok = false;
        std::cout << "Failed to run file 'InputRanges.lua'" << std::endl;
    }
    
    if( ok )
    {
        // Connect LuaBind to this lua state
        luabind::open(myLuaState);

        luabind::object topTable = luabind::globals(myLuaState)["Ranges"];
        
        if( topTable )
        {
            if( luabind::type( topTable ) == LUA_TTABLE )
            {            
                for (luabind::iterator i(topTable), end; i != end && ok; ++i)
                {
                    if( luabind::type( *i ) == LUA_TTABLE )
                    {
                        if( IsRangeNameValid( luabind::tostring_operator( i.key() ) ) )
                        {
//                             for (luabind::iterator rangeVals(*i), rangeValsEnd; rangeVals != rangeValsEnd && ok; ++rangeVals)
//                             {
//                                 if( luabind::type( *rangeVals ) ==  LUA_TNUMBER )
//                                 {
//                                     std::cout << "Lua Object: " << luabind::tostring_operator( rangeVals.key() ) 
//                                             << " = " 
//                                             << luabind::tostring_operator( *rangeVals ) << std::endl;
//                                 }
//                                 else
//                                 {
//                                     std::cout << "Value of '" << rangeVals.key() << "' is not a number."
//                                               << " All values in ranges declaration must be numbers." << std::endl;
//                                     ok = false;
//                                 }
//                             }
                            for(size_t j = 0; j < ARRAY_SIZE(MandatoryRangesTableMembers) ; j++)
                            {
                                luabind::object tempVal = luabind::gettable( *i, MandatoryRangesTableMembers[j] );
                                if( tempVal )
                                {
                                    std::cout << MandatoryRangesTableMembers[j] << " = " << tempVal << std::endl;
                                }
                                else
                                {
                                    std::cout << "Mandatory member '"
                                              << MandatoryRangesTableMembers[j]
                                              << "' of ranges table has not been found in '" 
                                              << i.key() << "'" << std::endl;
                                }
                            }
                        }
                        std::cout << "------------------------------" << std::endl;
                        //std::cout << "Lua Object: " << luabind::tostring_operator( i.key() ) << std::endl;
                    }
                    else
                    {
                        std::cout << "'Ranges' table has non table member '" << luabind::tostring_operator( i.key() ) << "'" << std::endl;
                        ok = false;
                    }
                }
            }
            else
            {
                std::cout << "Type: " << luabind::type( topTable ) << std::endl;
            }
        }
        else
        {
            std::cout << "Global Lua table 'Ranges' does not exist" << std::endl;
        }
        
        /*
        // Define a lua function that we can call
        luaL_dostring(myLuaState,
                    "function add(first, second)\n"
                    "  return first + second\n"
                    "end\n");

        std::cout << "Result: "
                << luabind::call_function<int>(myLuaState, "add", 2, 3)
                << std::endl;

                */
    }
    
    lua_close(myLuaState);
    
    return 0;
}

/*
 g++ -I../linux/libLuaBind_0_9_1/include/ -I../linux/libLua_5_1_5/include/ -I../linux/libBoost_1_34_0/include/ main.cpp ../linux/libLuaBind_0_9_1/lib/libluabind.a ../linux/libLua_5_1_5/lib/liblua.a -ldl -o testApp
 */