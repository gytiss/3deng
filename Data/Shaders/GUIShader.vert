
in vec3 in_VertexPos;
in vec4 in_VertexColor;
in vec2 in_VertexTextureCoords;

uniform mat4 u_ModelMatrix;

out vec4 VertexColor;
out vec2 TextureCoords;

//float MapRange(float val, float R1S, float R1E, float R2S, float R2E)
//{
//    float range1Len = ( R1E - R1S );
//    float range2Len = ( R2E - R2S );
//    float conversionFactor = ( range2Len / range1Len );
//
//    // "val" converted from the range of (R1S,R1E) to the range (0,range1Len)
//    float valInPositiveRange = ( val - R1S );
//
//    return ( ( conversionFactor * valInPositiveRange ) + R2S );
//}

void main()
{
//    vec3 vertPos = vec3( MapRange( in_VertexPos.x, 0, 1024, -512, 512 ),
//                          MapRange( in_VertexPos.y, 0, 768, -512, 512 ) * -1.0f,
//                          0.0f );

    vec4 vertPos = u_ModelMatrix * vec4( in_VertexPos, 1.0f );
//    vertPos.y = vertPos.y * -1.0f;
//    vertPos.z = 0;
    gl_Position = vertPos;
    VertexColor = in_VertexColor;
    TextureCoords = in_VertexTextureCoords;
}
