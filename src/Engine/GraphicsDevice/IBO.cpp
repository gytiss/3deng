#include "IBO.h"
#include <MathUtils.h>
#include <Exceptions/GenericException.h>

//-----------------------------------------------------------------------

IBO::IBO(DataBufferUsagePattern usagePatter)
: m_Id( 0 ),
  m_DataBufUsagePatter( usagePatter )
{
    glGenBuffers( 1, &m_Id );
    CheckForGLErrors();
}

//-----------------------------------------------------------------------

IBO::~IBO()
{
    Unbind();
    glDeleteBuffers( 1, &m_Id );
    CheckForGLErrors();
}

//-----------------------------------------------------------------------

void IBO::BindData(size_t sizeInBytes, const EngineTypes::VertexIndexType* dataPtr)
{
    assert( MathUtils::IsDivisibleBy( sizeInBytes, sizeof( EngineTypes::VertexIndexType ) ) && "Size in bytes must be a multiple of sizeof( EngineTypes::VertexIndexType )" );

    if( dataPtr && ( sizeInBytes > 0 ) )
    {
        Bind();

        GLenum bufferUsagePatter = GL_STATIC_DRAW;
        if( m_DataBufUsagePatter == e_DataBufferUsagePatternDynamic )
        {
            bufferUsagePatter = GL_DYNAMIC_DRAW;
        }

        glBufferData ( GL_ELEMENT_ARRAY_BUFFER, sizeInBytes, dataPtr, bufferUsagePatter );
        CheckForGLErrors();

        Unbind();
    }
    else
    {
        throw GenericException( "Tried to bind data pointer with is null or data of size 0 bytes to IBO" );
    }
}

//-----------------------------------------------------------------------
