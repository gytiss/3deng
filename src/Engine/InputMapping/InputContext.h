/**
 * Wrapper class for managing input contexts
 */

#ifndef INPUTCONTEXT_H_INCLUDED
#define INPUTCONTEXT_H_INCLUDED

#include "RawInputConstants.h"
#include "InputConstants.h"
#include "RangeConverter.h"
#include <map>
#include <set>

namespace InputMapping
{
    class InputMapper;

	class InputContext
	{
	public:
		~InputContext();

        // Mapping interface
		bool MapButtonToAction(RawInputButton button, Action& out) const;
		bool MapButtonToState(RawInputButton button, State& out) const;
		bool MapAxisToRange(RawInputAxis axis, Range& out) const;

		double GetSensitivity(Range range) const;

		const RangeConverter& GetConversions() const
		{
		    return m_Conversions;
        }

    protected:
	    friend class InputMapper;
        explicit InputContext(const RangeConverter& conversions); // Only to be accessed from InputMapper

        void AddValidAction(Action action);
        void AddValidState(State state);
        void AddValidRange(Range range);
        bool SetRangeSensitivity(Range range, double sensitivity);
        bool AddRawButtonToActionMapping(RawInputButton button, Action action);
        bool AddRawButtonToStateMapping(RawInputButton button, State state);
        bool AddRawAxisToRangeMapping(RawInputAxis axis, Range range);

	// Internal tracking
	private:
		std::map<RawInputButton, Action> m_ActionMap;
		std::map<RawInputButton, State> m_StateMap;
		std::map<RawInputAxis, Range> m_RangeMap;

		std::set<Action> m_ValidActions;
		std::set<State> m_ValidStates;
		std::set<Range> m_ValidRanges;

		std::map<Range, double> m_SensitivityMap;
		const RangeConverter& m_Conversions;
	};
}

#endif // INPUTCONTEXT_H_INCLUDED
