#ifndef AUDIODEVICE_EFFECT_H_INCLUDED
#define AUDIODEVICE_EFFECT_H_INCLUDED

#include "OpenALIncludes.h"

namespace AudioDevice
{

class Context;
class EffectSlot;

class Effect
{
public:
    class EffectSettingsUpdateListener
    {
    protected:
        friend class Effect;

        virtual ~EffectSettingsUpdateListener() = default;
        virtual void EffectSettingsUpdated() = 0;
    };

    virtual ~Effect();

    inline unsigned int GetALHandle()
    {
        return m_ALEffect;
    }

protected:
    enum Type
    {
        // The EAX Reverb has a more advanced parameter set than the simple "Reverb" below,
        // but it is more CPU intensive.
        e_TypeEAXReverb = AL_EFFECT_EAXREVERB,

        // The Reverb effect is the standard Effects Extension's environmental reverberation
        // effect. It is less CPU intensive at the expense of reverb of quality
        e_TypeReverb = AL_EFFECT_REVERB,

        // The Echo effect generates discrete, delayed instances of the input signal.
        e_TypeEcho = AL_EFFECT_ECHO,

        // The Ring modulator multiplies an input signal by a carrier signal in the time domain,
        // resulting in tremolo or inharmonic effects.
        e_TypeRingModulator = AL_EFFECT_RING_MODULATOR,

        // Audio rendered to the dedicated dialogue effect (AL_EFFECT_DEDICATED_DIALOGUE) is routed to a
        // front centre speaker if one is present. Otherwise, it is rendered to the front centre using
        // the normal spatialisation logic.
        e_TypeDedicatedDialog = AL_EFFECT_DEDICATED_DIALOGUE,

        // Audio rendered to the dedicated low frequency effect (AL_EFFECT_DEDICATED_LOW_FREQUENCY_EFFECT)
        // is routed to a subwoofer if one is present. Otherwise, it is discarded.
        e_TypeDedicatedLowFrequencyEffect = AL_EFFECT_DEDICATED_LOW_FREQUENCY_EFFECT
    };

    Effect(Context& context, EffectSlot& effectSlot, Type type);

    inline Context& _GetContext()
    {
        return m_Context;
    }

    void _SettingsUpdated();

private:
    // Used to prevent copying
    Effect& operator = (const Effect& other) = delete;
    Effect(const Effect& other) = delete;

    unsigned int m_ALEffect;
    Context& m_Context;
    EffectSlot&  m_EffectSlotAttachedTo;
    Type m_Type;
};

}

#endif // AUDIODEVICE_EFFECT_H_INCLUDED
