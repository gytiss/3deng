#!/system/bin/sh
PACKAGE=org.TNK
VGPARAMS="--error-limit=no --trace-children=yes --tool=memcheck --log-file=/data/data/$PACKAGE/cache/valgrind.log --sigill-diagnostics=no --extra-debuginfo-path=/sdcard/system/symbols/ --suppressions=/data/local/valgrind.supp --kernel-variant=android-gpu-adreno3xx"
export TMPDIR=/data/data/$PACKAGE
echo "Executing through valgrind: $VGPARAMS $*"
exec /data/local/Inst/bin/valgrind $VGPARAMS $* 
