#include "MTC.h"
#include <cassert>
#include <ByteStream.h>
#include <MathUtils.h>

//-----------------------------------------------------------------------

MTC::MTC(FileManager& fileManager)
: m_FileManager( fileManager ),
  m_TextureType( e_TextureTypeCount ),
  m_TextureDataType( e_TextureDataTypeCount ),
  m_MipMapLevelDescriptorList(),
  m_TopLevelMipMapWidthInPixels( 0 ),
  m_TopLevelMipMapHeightInPixles( 0 ),
  m_TextureFaceCount( 0 ),
  m_TextureData()
{

}

//-----------------------------------------------------------------------

MTC::~MTC()
{

}

//-----------------------------------------------------------------------

bool MTC::LoadFromFile(const std::string& fileName)
{
    bool retVal = false;

    ByteStream byteStream( m_FileManager, ByteStream::e_DataAlignment4Bytes );
    if( byteStream.InitFromFile( fileName ) )
    {
        retVal = true;

        uint16_t textureType = 0;
        uint16_t textureDataType = 0;
        uint32_t mipMapDescriptorCount = 0;
        uint32_t widthInPixels = 0;
        uint32_t heightInPixles = 0;
        uint32_t totalTextureDataSizeInBytes = 0;

        retVal = retVal && _VerifyFileTypeAndVersion( byteStream );
        retVal = retVal && _ReadTextureDescriptionData( byteStream,
                                                        textureType,
                                                        textureDataType,
                                                        mipMapDescriptorCount,
                                                        widthInPixels,
                                                        heightInPixles,
                                                        totalTextureDataSizeInBytes );

        uint32_t facesPerTexture = 1;
        if( textureType == e_TextureTypeCubeMap )
        {
            facesPerTexture = 6;
        }

        retVal = retVal && _VerifyTextureDescriptionData( facesPerTexture,
                                                          mipMapDescriptorCount,
                                                          widthInPixels,
                                                          heightInPixles,
                                                          totalTextureDataSizeInBytes,
                                                          byteStream.InitialStreamSizeInBytes() );

        retVal = retVal && _ReadMipMapLevelDescriptors( byteStream,
                                                        facesPerTexture,
                                                        mipMapDescriptorCount,
                                                        m_MipMapLevelDescriptorList );

        retVal = retVal && _VerifyMipMapLevelDescriptor( m_MipMapLevelDescriptorList,
                                                         facesPerTexture,
                                                         totalTextureDataSizeInBytes );

        retVal = retVal && _ReadTextureData( byteStream, totalTextureDataSizeInBytes, m_TextureData );

        if( retVal )
        {
             m_TopLevelMipMapWidthInPixels = widthInPixels;
             m_TopLevelMipMapHeightInPixles = heightInPixles;
             m_TextureFaceCount = facesPerTexture;
             m_TextureType = static_cast<TextureType>( textureType) ;
             m_TextureDataType = static_cast<TextureDataType>( textureDataType );
        }
    }

    return retVal;
}

//-----------------------------------------------------------------------

MTC::TextureType MTC::GetTextureType() const
{
    return m_TextureType;
}

//-----------------------------------------------------------------------

MTC::TextureDataType MTC::GetTextureDataType() const
{
    return m_TextureDataType;
}

//-----------------------------------------------------------------------

size_t MTC::GetMipMapLevelCount() const
{
    return m_MipMapLevelDescriptorList.size();
}

//-----------------------------------------------------------------------

void MTC::GetMipLevelDimensions(size_t mipMapLevel,
                                size_t& outWidthInPixels,
                                size_t& outHeightInPixels) const
{
    outWidthInPixels = std::max( static_cast<size_t>( 1 ), m_TopLevelMipMapWidthInPixels >> mipMapLevel );
    outHeightInPixels = std::max( static_cast<size_t>( 1 ), m_TopLevelMipMapHeightInPixles >> mipMapLevel );
}

//-----------------------------------------------------------------------

void MTC::GetMipLevelTextureData(size_t mipMapLevel,
                                 size_t faceIndex,
                                 const uint8_t*& outDataBuffer,
                                 size_t& outDataSizeInBytes) const
{
    assert( ( faceIndex < 6 ) && "Only up to six faces are supported" );
    assert( ( mipMapLevel < m_MipMapLevelDescriptorList.size() ) && "Invalid mip map level" );

    outDataBuffer = &m_TextureData[m_MipMapLevelDescriptorList[mipMapLevel].dataOffset[faceIndex]];
    outDataSizeInBytes = m_MipMapLevelDescriptorList[mipMapLevel].dataSize[faceIndex];
}

//-----------------------------------------------------------------------

size_t MTC::GetTextureFaceCount() const
{
    return m_TextureFaceCount;
}

//-----------------------------------------------------------------------

size_t MTC::GetTextureDataRowAlignment() const
{
    return 4;
}

//-----------------------------------------------------------------------

bool MTC::_VerifyFileTypeAndVersion(ByteStream& byteStream) const
{
    bool retVal = true;

    std::string fileIdStr = "";
    uint32_t fileVersion = 0;
    retVal = retVal && byteStream.ReadString( fileIdStr, 4 );
    retVal = retVal && byteStream.ReadUInt32( fileVersion );
    retVal = retVal && !fileIdStr.compare( "MTC " );
    retVal = retVal && ( fileVersion == 1 );

    return retVal;
}

//-----------------------------------------------------------------------

bool MTC::_VerifyTextureDescriptionData(uint32_t facesPerTexture,
                                        uint32_t mipMapDescriptorCount,
                                        uint32_t widthInPixels,
                                        uint32_t heightInPixles,
                                        size_t totalTextureDataSizeInBytes,
                                        size_t totalFileSize) const
{
    bool retVal = true;

    // There must be one Mip Map Decriptor per face per mip map level
    // e.g.: every mip map level of a CubeMap would have six descriptors
    retVal = retVal && ( ( mipMapDescriptorCount % facesPerTexture ) == 0 );

    // The file is required to contain all of the mip map levels of a given texture
    const size_t numberOfMipMapLevelsInTheFile = ( mipMapDescriptorCount / facesPerTexture );
    const size_t mipMapLevelsNeededForGivenDimensions = MathUtils::MaxMipMapLevelForTextureDimensions( widthInPixels, heightInPixles );
    retVal = retVal && ( numberOfMipMapLevelsInTheFile == mipMapLevelsNeededForGivenDimensions );

    // The data section size must be divisible by 4 i.e. 4 bytes aligned
    retVal = retVal && ( ( totalTextureDataSizeInBytes & 0x03 ) == 0 );

    if( retVal )
    {
        const size_t c_HeaderSizeInBytes = 28;
        const size_t c_MipMapDscriptorDataFieldSize = 4;
        const size_t c_MipMapDscriptorOffsetFieldSize = 4;
        const size_t c_MipMapDscriptorSectionSizeInBytes = ( mipMapDescriptorCount * ( ( c_MipMapDscriptorDataFieldSize +
                                                                                         c_MipMapDscriptorOffsetFieldSize ) * facesPerTexture ) );

        const size_t c_ExpectedFileSize = ( c_HeaderSizeInBytes + c_MipMapDscriptorSectionSizeInBytes + totalTextureDataSizeInBytes );
        retVal = retVal && ( totalFileSize == c_ExpectedFileSize );
    }

    return retVal;
}

//-----------------------------------------------------------------------

bool MTC::_VerifyMipMapLevelDescriptor(const MipMapLevelDescriptorList& descriptors,
                                       uint32_t facesPerTexture,
                                       uint32_t totalTextureDataSizeInBytes) const
{
    size_t minimumRequiredTextureDataSectionSizeInBytes = 0;

    for(const MipMapLevelDescriptor& desc : descriptors)
    {
        for(size_t faceIdx = 0; faceIdx < facesPerTexture; faceIdx++)
        {
            size_t dataSize = desc.dataSize[faceIdx];

            // Round up to the nearest integer divisible by 4. This is needed,
            // because all of the data offsets are 4 bytes aligned
            dataSize = ( ( ( dataSize + 3 ) / 4 ) * 4 );

            minimumRequiredTextureDataSectionSizeInBytes += dataSize;
        }
    }

    return ( minimumRequiredTextureDataSectionSizeInBytes <= totalTextureDataSizeInBytes );
}

//-----------------------------------------------------------------------

bool MTC::_ReadTextureDescriptionData(ByteStream& byteStream,
                                      uint16_t& outTextureType,
                                      uint16_t& outTextureDataType,
                                      uint32_t& outMipMapDescriptorCount,
                                      uint32_t& outWidthInPixels,
                                      uint32_t& outHeightInPixles,
                                      uint32_t& outTotalTextureDataSizeInBytes) const
{
    bool retVal = true;

    retVal = retVal && byteStream.ReadUInt16( outTextureType );
    retVal = retVal && byteStream.ReadUInt16( outTextureDataType );
    retVal = retVal && byteStream.ReadUInt32( outMipMapDescriptorCount );
    retVal = retVal && byteStream.ReadUInt32( outWidthInPixels );
    retVal = retVal && byteStream.ReadUInt32( outHeightInPixles );
    retVal = retVal && byteStream.ReadUInt32( outTotalTextureDataSizeInBytes );

    return retVal;
}

//-----------------------------------------------------------------------

bool MTC::_ReadMipMapLevelDescriptors(ByteStream& byteStream,
                                      uint32_t facesPerTexture,
                                      uint32_t mipMapDescriptorCount,
                                      MipMapLevelDescriptorList& outDescriptors) const
{
    bool retVal = true;

    outDescriptors.clear();
    outDescriptors.assign( mipMapDescriptorCount / facesPerTexture, MipMapLevelDescriptor() );

    const size_t mipLevelCount = outDescriptors.size();
    for(size_t mipLevelIdx = 0; retVal && ( mipLevelIdx < mipLevelCount ); mipLevelIdx++)
    {
        MipMapLevelDescriptor& mipMapLevelDescriptor = outDescriptors[mipLevelIdx];

        for(size_t faceIdx = 0; retVal && ( faceIdx < facesPerTexture ); faceIdx++)
        {
            retVal = retVal && byteStream.ReadUInt32( mipMapLevelDescriptor.dataSize[faceIdx] );
            retVal = retVal && byteStream.ReadUInt32( mipMapLevelDescriptor.dataOffset[faceIdx] );
        }
    }

    return retVal;
}

//-----------------------------------------------------------------------

bool MTC::_ReadTextureData(ByteStream& byteStream,
                           uint32_t totalTextureDataSizeInBytes,
                           TextureDataBuffer& textureDataBuffer)
{
    textureDataBuffer.assign( totalTextureDataSizeInBytes, 0 );

    return byteStream.ReadBytesToBuffer( textureDataBuffer.data(), totalTextureDataSizeInBytes );
}

//-----------------------------------------------------------------------
