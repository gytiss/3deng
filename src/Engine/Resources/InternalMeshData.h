#ifndef INTERNALMESHDATA_H_INCLUDED
#define INTERNALMESHDATA_H_INCLUDED

#include <cstring>
#include <EngineTypes.h>

class InternalMeshData
{
    friend class MeshManager;

protected:
    struct MeshVertex
    {
        float position[3];
        float textureCoords[2];
    };

    static const MeshVertex m_ConeVertexBuffer[];
    static const EngineTypes::VertexIndexType m_ConeIndexBuffer[];

    static const MeshVertex m_SphereVertexBuffer[];
    static const EngineTypes::VertexIndexType m_SphereIndexBuffer[];

    static const MeshVertex m_CylinderVertexBuffer[];
    static const EngineTypes::VertexIndexType m_CylinderIndexBuffer[];
//
//    static const MeshVertex m_CubeVertexBuffer[];
//    static const EngineTypes::VertexIndexType m_CubeIndexBuffer[];

    static const MeshVertex m_PlaneVertexBuffer[];
    static const EngineTypes::VertexIndexType m_PlaneIndexBuffer[];

    static const size_t m_ConeIndexCount;
    static const size_t m_ConeVertexCount;
    static const EngineTypes::VertexIndexType m_ConeMinVertexIndexValue;
    static const EngineTypes::VertexIndexType m_ConeMaxVertexIndexValue;

    static const size_t m_SphereIndexCount;
    static const size_t m_SphereVertexCount;
    static const EngineTypes::VertexIndexType m_SphereMinVertexIndexValue;
    static const EngineTypes::VertexIndexType m_SphereMaxVertexIndexValue;

    static const size_t m_CylinderIndexCount;
    static const size_t m_CylinderVertexCount;
    static const EngineTypes::VertexIndexType m_CylinderMinVertexIndexValue;
    static const EngineTypes::VertexIndexType m_CylinderMaxVertexIndexValue;

//    static const size_t m_CubeIndexCount;
//    static const size_t m_CubeVertexCount;
//    static const EngineTypes::VertexIndexType m_CubeMinVertexIndexValue;
//    static const EngineTypes::VertexIndexType m_CubeMaxVertexIndexValue;

    static const size_t m_PlaneIndexCount;
    static const size_t m_PlaneVertexCount;
    static const EngineTypes::VertexIndexType m_PlaneMinVertexIndexValue;
    static const EngineTypes::VertexIndexType m_PlaneMaxVertexIndexValue;

private:
    InternalMeshData();
};

#endif // INTERNALMESHDATA_H_INCLUDED
