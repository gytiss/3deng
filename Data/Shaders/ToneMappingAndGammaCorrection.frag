
uniform sampler2D u_PostProcessTextureMap;
uniform vec2 u_ScreenSize;

layout(location = 0) out vec4 FragColor;

vec2 CalcTexCoord()
{
    return gl_FragCoord.xy / u_ScreenSize;
}

vec3 Uncharted2Tonemap(const in vec3 x)
{
    float A = 0.15f;
    float B = 0.50f;
    float C = 0.10f;
    float D = 0.20f;
    float E = 0.02f;
    float F = 0.30f;

    return ((x*(A*x+C*B)+D*E)/(x*(A*x+B)+D*F))-E/F;
}

void main()
{
    vec3 FragColorInLinearSpace = texture(u_PostProcessTextureMap, CalcTexCoord()).rgb;

    float exposure = 10.0f;
    FragColorInLinearSpace = Uncharted2Tonemap(FragColorInLinearSpace * exposure);

    FragColorInLinearSpace = clamp(FragColorInLinearSpace, 0.0f, 1.0f);
    float gammaCorrectionFactor = (1.0f / 2.2f);
    FragColor = vec4(pow(FragColorInLinearSpace, vec3(gammaCorrectionFactor)), 1.0f);
}

