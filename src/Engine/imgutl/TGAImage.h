#ifndef IMAGE_H
#define IMAGE_H

#include <ByteStream.h>
#include <cstdint>
#include <string>

#define LEFT  0x1
#define RIGHT 0x2
#define LOWER 0x10
#define UPPER 0x20

class FileManager;

class TGAImage
{
public:
    enum Origin { LOWER_LEFT = LOWER|LEFT, UPPER_LEFT = UPPER|LEFT,
                  LOWER_RIGHT = LOWER|RIGHT, UPPER_RIGHT = UPPER|RIGHT };

    TGAImage(FileManager& fileManager);
    bool Load(std::string name, Origin preferedOrigin);

    /**
     * @param dataBuffer - 4 bytes aligned pointer to a data buffer
     * @param dataBufferSizeInBytes - Total size of the data buffer in bytes
     */
    bool Load(const uint8_t* dataBuffer, size_t dataBufferSizeInBytes, Origin preferedOrigin);

    static bool SaveAsTGA(std::string fileName,
                          Origin origin,
                          size_t width,
                          size_t height,
                          unsigned int bytesPerPixel,
                          uint8_t* pixelData );

    inline unsigned short GetWidth() const { return m_TGAHeader.width; }
    inline unsigned short GetHeight() const { return m_TGAHeader.height; }
    inline unsigned char  GetDepth() const { return m_TGAHeader.bitsPerPixel; }
    inline unsigned char* GetData() const { return m_Data; }

private:
    static const size_t c_TGAHeaderSizeInBytes = 18u;

    struct TGAHeader
    {
        uint8_t  identSizeInBytes;   // size of ID field that follows 18 byte header (0 usually)
        uint8_t  colourMapType;      // type of colour map 0=none, 1=has palette
        uint8_t  imageType;          // type of image 0=none,1=indexed,2=rgb,3=grey,+8=rle packed

        uint16_t colourMapStart;     // first colour map entry in palette
        uint16_t colourMapLength;    // number of colours in palette
        uint8_t  colourMapBits;      // number of bits per palette entry 15,16,24,32

        uint16_t xStart;             // image x origin
        uint16_t yStart;             // image y origin
        uint16_t width;              // image width in pixels
        uint16_t height;             // image height in pixels
        uint8_t  bitsPerPixel;       // image bits per pixel 8,16,24,32
        uint8_t  descriptor;         // image descriptor bits (vh flip bits)
    };

    TGAImage&  operator = (const TGAImage& other);

    bool _Load(Origin preferedOrigin);

    uint8_t* m_Data;
    TGAHeader m_TGAHeader;
    ByteStream m_ByteStream;
};

#endif //IMAGE_H



