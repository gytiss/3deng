
# Uncomment this if you're using STL in your project
# See CPLUSPLUS-SUPPORT.html in the NDK documentation for more information
APP_STL := gnustl_shared 

#APP_ABI := armeabi-v7a arm64-v8a mips mips64 x86 x86_64
APP_ABI := armeabi-v7a

APP_PLATFORM := android-18

APP_CPPFLAGS += -fexceptions
APP_CPPFLAGS += -frtti