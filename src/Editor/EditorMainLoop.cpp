#include "EditorMainLoop.h"
#include "GLWidget.h"
#include <OpenGLIncludes.h>
#include <OpenGLErrorChecking.h>
#include <GraphicsDevice/GraphicsDevice.h>

//-----------------------------------------------------------------------

EditorMainLoop::EditorMainLoop(MainWindow& mainWindow,
                               InputMapping::InputMapper& inputMapper,
                               SystemInterface& systemInterface,
                               FileManager& fileManager)
: MainLoop( systemInterface, fileManager ),
  m_InputMapper( inputMapper ),
  m_EditorLogic( nullptr )
{
    m_EditorLogic = new EditorLogic( mainWindow,
                                     GetGraphicsDevice(),
                                     GetFileManager(),
                                     GetAnimationManager(),
                                     GetMeshManager(),
                                     GetTextureManager(),
                                     GetMaterialManager(),
                                     GetRenderManager(),
                                     GetPhysicsManager(),
                                     GetAudioDataManager(),
                                     GetSoundManager(),
                                     systemInterface );

    m_EditorLogic->SetAppIsStillRunning( true );

    m_InputMapper.AddCallback( EditorLogic::InputCallback, 1, m_EditorLogic );
}

//-----------------------------------------------------------------------

EditorMainLoop::~EditorMainLoop()
{
    SafeDelete( m_EditorLogic );
}

//-----------------------------------------------------------------------

void EditorMainLoop::_UpdateInputs(float frameTimeInSeconds)
{
    m_InputMapper.Dispatch( frameTimeInSeconds );
    m_InputMapper.Clear();
}

//-----------------------------------------------------------------------

void EditorMainLoop::_UpdateOtherApplicationState(float UNUSED_PARAM(frameTimeInSeconds))
{

}

//-----------------------------------------------------------------------

void EditorMainLoop::_Render(float frameTimeInSeconds)
{
    m_EditorLogic->Render( frameTimeInSeconds ); // Render Scene
    m_EditorLogic->SetCurrentFrameTimeInMs( GetCurrentFrameTimeInMilliseconds() );
}

//-----------------------------------------------------------------------
