#ifndef ENGINEDEFAULTS_H_INCLUDED
#define ENGINEDEFAULTS_H_INCLUDED

namespace EngineDefaults
{
    static const float c_NearClippingPlane = 0.01f;
    static const float c_FarClippingPlane = 60.0f;
}

#endif // ENGINEDEFAULTS_H_INCLUDED
