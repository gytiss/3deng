#include "SoundManager.h"

#include <Exceptions/GenericException.h>
#include <AudioDevice/Buffer.h>
#include <cassert>

//-----------------------------------------------------------------------

bool SoundManager::m_ClassAlreadyInstantiated = false;

//-----------------------------------------------------------------------

SoundManager::SoundManager()
: m_Device(),
  m_Context( m_Device ),
  m_HardwareSoundSourcesList(),
  m_ActiveSoundSources(),
  m_AvailableSoundSources(),
  m_NumberOfAvailableSoundSources( m_AvailableSoundSources.size() )
{
    if( m_ClassAlreadyInstantiated )
    {
        throw GenericException( "There can only be one instance of SoundManager()" );
    }
    else
    {
        m_ClassAlreadyInstantiated = true;
    }

    for(size_t i = 0; i < m_AvailableSoundSources.size(); i++)
    {
        m_AvailableSoundSources[i] = new AudioDevice::Source( m_Context );
    }
}

//-----------------------------------------------------------------------

SoundManager::~SoundManager()
{
    for(HardwareSoundSourcesList::Node* node : m_ActiveSoundSources)
    {
        if( node && ( node->value != nullptr ) )
        {
            delete node->value ;
        }
    }

    for(AudioDevice::Source* source : m_AvailableSoundSources)
    {
        if( source && ( source != nullptr ) )
        {
            delete source;
        }
    }

    m_HardwareSoundSourcesList.Clear();
}

//-----------------------------------------------------------------------

SoundManager::HardwareSoundSourceHandle SoundManager::AllocateHardwareSoundSource()
{
    HardwareSoundSourcesList::Node* dummySoundSource = m_HardwareSoundSourcesList.PushBack( nullptr );

    return HardwareSoundSourceHandle( dummySoundSource );
}

//-----------------------------------------------------------------------

void SoundManager::ReleaseHardwareSoundSource(HardwareSoundSourceHandle& handle)
{
    if( handle.IsValid() )
    {
        if( handle.m_HardwareSoundSourcesListNode->value != nullptr )
        {
            m_AvailableSoundSources[m_NumberOfAvailableSoundSources] = handle.m_HardwareSoundSourcesListNode->value;
            m_NumberOfAvailableSoundSources++;
            handle.m_HardwareSoundSourcesListNode->value = nullptr;
        }

        m_HardwareSoundSourcesList.Erase( handle.m_HardwareSoundSourcesListNode );
        handle.Invalidate();
    }
}

//-----------------------------------------------------------------------

void SoundManager::ActivateHardwareSoundSource(HardwareSoundSourceHandle& handle)
{
    if( handle.IsValid() )
    {
        handle.m_HardwareSoundSourcesListNode->value = _FindAHardwareSourceToUse();
    }
}

//-----------------------------------------------------------------------

bool SoundManager::IsHardwareSoundSourceActive(const HardwareSoundSourceHandle& handle)
{
    bool retVal = false;

    if( handle.IsValid() )
    {
        retVal = ( handle.m_HardwareSoundSourcesListNode->value != nullptr );
    }

    return retVal;
}

//-----------------------------------------------------------------------

AudioDevice::Source* SoundManager::_FindAHardwareSourceToUse()
{
    AudioDevice::Source* retVal = nullptr;

    if( m_NumberOfAvailableSoundSources > 0 )
    {
        retVal = m_AvailableSoundSources[m_NumberOfAvailableSoundSources - 1];
        m_AvailableSoundSources[m_NumberOfAvailableSoundSources - 1] = nullptr;
        m_NumberOfAvailableSoundSources--;
    }
    else
    {
        for(size_t i = 0; ( i < m_ActiveSoundSources.size() ) && ( retVal == nullptr ); i++)
        {
            if( m_ActiveSoundSources[i]->value->IsStopped() )
            {
                retVal = m_ActiveSoundSources[i]->value;
                m_ActiveSoundSources[i]->value = nullptr;
            }
        }

        if( retVal == nullptr )
        {
            for(size_t i = 0; ( i < m_ActiveSoundSources.size() ) && ( retVal == nullptr ); i++)
            {
                if( !m_ActiveSoundSources[i]->value->IsLooping() )
                {
                    retVal = m_ActiveSoundSources[i]->value;
                    retVal->Stop();
                    m_ActiveSoundSources[i]->value = nullptr;
                }
            }
        }
    }

    return retVal;
}

//-----------------------------------------------------------------------

void SoundManager::PlaySource(HardwareSoundSourceHandle& soundSourceHandle,
                              AudioDataManager::AudioDataHandle& audioDataHandle,
                              bool isLooping)
{
    if( !IsHardwareSoundSourceActive( soundSourceHandle ) )
    {
        ActivateHardwareSoundSource( soundSourceHandle );
    }

    AudioDevice::Buffer* buffer = new AudioDevice::Buffer( m_Context );

    soundSourceHandle.m_HardwareSoundSourcesListNode->value->SetSourceType( AudioDevice::Source::e_SourceTypeStatic );

    buffer->Feed( *audioDataHandle );
    soundSourceHandle.m_HardwareSoundSourcesListNode->value->SetBuffer( *buffer );
    soundSourceHandle.m_HardwareSoundSourcesListNode->value->Play();

    // TODO Use "isLooping"
}

//-----------------------------------------------------------------------

void SoundManager::PauseSource(HardwareSoundSourceHandle& soundSourceHandle)
{
    if( IsHardwareSoundSourceActive( soundSourceHandle ) )
    {
        soundSourceHandle.m_HardwareSoundSourcesListNode->value->Pause();
    }
}

//-----------------------------------------------------------------------

void SoundManager::StopSource(HardwareSoundSourceHandle& soundSourceHandle)
{
    if( IsHardwareSoundSourceActive( soundSourceHandle ) )
    {
        soundSourceHandle.m_HardwareSoundSourcesListNode->value->Stop();
    }
}

//-----------------------------------------------------------------------

//AudioDataHandle SoundManager::LoadAudioData(const std::string& audioFilePath)
//{
//    AudioDataList::Node* audioDataNode;
//
//    UniqueAudioDataIDsToAudioDataMap::iterator it = m_UniqueAudioDataIDsToAudioDataMap.find( audioFilePath );
//    if( it != m_UniqueAudioDataIDsToAudioDataMap.end() )
//    {
//        audioDataNode = it->second;
//        audioDataNode->value.refCount = ( audioDataNode->value.refCount + 1 );
//
//        outModelMeshes.clear();
//        outModelMeshes.push_back( MeshHandle( meshIt ) );
//    }
//    else
//    {
//        audioDataNode = _LoadAudioDataFromFile( audioFilePath );
//    }
//
//    return AnimationHandle( audioDataNode );
//}

//-----------------------------------------------------------------------

//void SoundManager::ReleaseAudioData(AudioDataHandle& handle)
//{
//    assert( handle.IsValid() && "Trying to release an invalid handle" );
//
//    if( handle.IsValid() )
//    {
//        RefCountedAudioDataPtr& audioDataPtr = handle.m_AudioDataListNode->value;
//        audioDataPtr.refCount = ( audioDataPtr.refCount - 1 );
//
//        if( audioDataPtr.refCount == 0 )
//        {
//            delete audioDataPtr.audioData;
//
//            m_UniqueAudioDataIDsToAudioDataMap.erase( audioDataPtr.audioDataFilePath );
//            m_AudioDataList.erase( handle.m_AudioDataListNode );
//            handle.Invalidate();
//        }
//    }
//}

//-----------------------------------------------------------------------

//AudioDataList::Node* SoundManager::_LoadAudioDataFromFile(const std::string& audioFilePath)
//{
//    AudioDataList::Node* audioDataListNode = nullptr;
//
//    std::unique_ptr<AudioData> audioData( new AudioData( m_FileManager ) );
//
//    if (audioData->InitFromOggFile( audioFilePath ))
//    {
//        audioDataListNode = m_AudioDataList.PushBack( RefCountedAudioDataPtr() );
//        audioDataListNode->value.refCount = 1;
//        audioDataListNode->value.audioData = audioData.release();
//        audioDataListNode->value.audioDataFilePath = audioFilePath;
//    }
//    else
//    {
//        throw GenericException( "_LoadAudioDataFromFile() failed to load audio file \"" + audioFilePath + "\"" );
//    }
//
//    return audioDataListNode;
//}

//-----------------------------------------------------------------------
