#ifndef MESHCOLLIDER_H_INCLUDED
#define MESHCOLLIDER_H_INCLUDED

#include "Collider.h"

class MeshCollider : public Collider
{
    GAME_OBJECT_COMPONENT_TYPE(e_TypeCollider);

public:
    friend class MeshFilter;

    MeshCollider(GameObject& owningGameObject,
                 PhysicsManager& physicsManager);

private:
    void _FindAndSetMesh();
};

#endif // MESHCOLLIDER_H_INCLUDED
