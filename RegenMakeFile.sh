#!/usr/bin/env sh

mkdir -p build 
cd build
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ../
cd ../
