#include "ShootingTarget.h"
#include "GameObjectTypes.h"
#include <SystemInterface.h>
#include <Scene/Transform.h>
#include <Scene/Animator.h>
#include <Scene/SkinnedMeshRenderer.h>
#include <Scene/BoxCollider.h>
#include <Resources/MeshManager.h>
#include <Resources/AnimationManager.h>
#include <Resources/MaterialManager.h>
#include <Camera.h>
#include <MathUtils.h>

//-----------------------------------------------------------------------

ShootingTarget::ShootingTarget(const std::string& name,
                               MeshManager& meshManager,
                               AnimationManager& animationManager,
                               MaterialManager& materialManager,
                               PhysicsManager& physicsManager)
: GameObject( name, GameObjectTypes::c_ShootingTarget ),
  m_CurrentState( e_StateActive ),
  m_Animator( nullptr ),
  m_ShootingTargetCollider( nullptr )
{
    /*---------------------------------------------------*/
    this->GetComponent<Transform>()->SetPosition( glm::vec3( 0.0f, 0.0f, 0.0f ) );
    this->GetComponent<Transform>()->SetRotation( glm::vec3( 0.0f, 90.0f, 0.0f ) );
    SkinnedMeshRenderer* shootingTargetRenderer = new SkinnedMeshRenderer( *this, meshManager, materialManager );
    MeshManager::ModelMeshes shootingTargetModelMeshes;
    meshManager.LoadModel( "DemoRoomData/Models/ShootingTarget/Target.mobj", shootingTargetModelMeshes );
    shootingTargetRenderer->SetMesh( shootingTargetModelMeshes[0] );
    shootingTargetRenderer->SetSubMeshMaterial( 0, materialManager.LoadMaterial( "DemoRoomData/Models/ShootingTarget/Target.mat" ) );
    shootingTargetRenderer->SetSubMeshMaterial( 1, materialManager.LoadMaterial( "DemoRoomData/Models/ShootingTarget/TargetShaft.mat" ) );
    glm::vec3 halfExtents = shootingTargetRenderer->GetAABB().halfExtents;
    halfExtents.y = ( halfExtents.y /  2.0f );
    m_ShootingTargetCollider = new  BoxCollider( *this, halfExtents, physicsManager );
    m_ShootingTargetCollider->SetCenterYPositionOffset( 1.5f );

    m_Animator = new Animator( *this, animationManager );
    m_Animator->AddAnimation( "Hit", animationManager.LoadAnimation( "DemoRoomData/Models/ShootingTarget/TargetHit.manim" ) );
    /*---------------------------------------------------*/

    _AddLogic( this );
}

//-----------------------------------------------------------------------

void ShootingTarget::SetState(State newState)
{
    if( newState != m_CurrentState )
    {
        if( newState == e_StateHit )
        {
            m_Animator->Play( "Hit", false );
            m_ShootingTargetCollider->SetEnabled( false );
        }
        else if( newState == e_StateActive )
        {
            //m_Animator->Play( "Active", false );
            m_ShootingTargetCollider->SetEnabled( true );
        }

        m_CurrentState = newState;
    }
}

//-----------------------------------------------------------------------

void ShootingTarget::_Initialize()
{
    SetState( e_StateActive );
}

//-----------------------------------------------------------------------

void ShootingTarget::_FixedUpdate()
{

}

//-----------------------------------------------------------------------

void ShootingTarget::_Update()
{
//    GetComponent<Transform>()->SetScale( glm::vec3( 0.125f ) ); // Make sure the gun does not collide with the walls
//    GetComponent<Transform>()->SetPosition( m_Camera.GetPosition() );
//    GetComponent<Transform>()->SetRotation( MathUtils::EulerXYZAnglesToQuaternion( glm::vec3( -m_Camera.GetPitchInDegrees(),
//                                                                                              m_Camera.GetYawInDegrees(), 0.0f ) ) );
//
//    if( m_CurrentState == e_StateShooting )
//    {
//        const float timeShooting = ( m_GlobalTimeInSecondDuringStateChange - m_SystemInterface.GetTicksInSeconds() );
//
//        m_BulletsInTheMagazine = ( c_MagazineCapacity - ( c_BulletsPerSecond * timeShooting ) );
//
//        if( m_BulletsInTheMagazine <= 0 )
//        {
//            SetState( e_StateReloading );
//        }
//    }
//    else if( m_CurrentState == e_StateReloading )
//    {
//        if( !m_Animator->IsPlaying() )
//        {
//            SetState( e_StateStatic );
//        }
//    }
}

//-----------------------------------------------------------------------

void ShootingTarget::_LastUpdate()
{

}

//-----------------------------------------------------------------------
