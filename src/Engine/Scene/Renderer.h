#ifndef RENDERER_H_INCLUDED
#define RENDERER_H_INCLUDED

#include <vector>
#include <Renderer/RenderableObject.h>
#include <Resources/MaterialManager.h>
#include <Resources/MeshManager.h>
#include <EngineTypes.h>
#include "Component.h"

class GameObject;

class Renderer : public Component
{
    GAME_OBJECT_COMPONENT_TYPE(e_TypeRenderer);

public:
    typedef std::vector<RenderableObject> RenderablesList;

    enum RendererType
    {
        e_RendererTypeMesh = 0,
        e_RendererTypeSkinnedMesh = 1,

        e_RendererTypeCount // Not a renderer type (used for counting)
    };

    virtual ~Renderer();

    size_t GetSubMeshCount() const;

    /**
     * @param subMeshIndex - Index of the sub mesh which must be lower than the value returned by the GetSubMeshCount()
     */
    MaterialManager::MaterialHandle* GetSubMeshMaterial(size_t subMeshIndex);

    /**
     * @param subMeshIndex - Index of the sub mesh which must be lower than the value returned by the GetSubMeshCount()
     * @param handle - Handle of the material this renderer will be using for the given sub mesh (Note: Mesh renderer takes ownership of this material handle)
     */
    bool SetSubMeshMaterial(size_t subMeshIndex, MaterialManager::MaterialHandle handle);

    void SetDepthTestingEnabled(bool enabled);
    bool GetDepthTestingEnabled() const;

    inline RendererType GetRendererType() const
    {
        return m_RendererType;
    }

    inline RenderablesList& GetRenderables()
    {
        return m_Renderables;
    }

    /**
     * Get axis aligned bounding box
     */
    virtual const EngineTypes::AABB& GetAABB() const = 0;

protected:
    Renderer(GameObject& owningGameObject,
             RendererType type,
             const std::string& name,
             MaterialManager& materialManager);

    virtual MeshManager::MeshHandle _GetMeshHandle() const = 0;
    inline MeshManager::MeshHandle _GetMeshHandle()
    {
        // All this casting is needed to allow reuse of the _GetMeshHandle() function defined above
        return static_cast<const Renderer*>( this )->_GetMeshHandle();
    }

    void _AddRenderable(const RenderableObject& renderable);
    void _ClearRenderables();

    virtual void _RegenerateRenderables() = 0;
    void _ResetSubMeshesMaterialsList(const MeshManager::MeshHandle& meshHandle);
    void _ReleaseExistingSubMeshesMaterials();

private:
    typedef std::vector<MaterialManager::MaterialHandle> MaterialList;

    MaterialManager& m_MaterialManager;
    bool m_DepthTestingEnabled;
    RendererType m_RendererType;
    RenderablesList m_Renderables;
    MaterialList m_SubMeshesMaterials;
};

#endif // RENDERER_H_INCLUDED
