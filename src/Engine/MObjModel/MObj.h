#ifndef MOBJ_H_INCLUDED
#define MOBJ_H_INCLUDED

#include <string>
#include <cstdint>
#include <vector>
#include <cassert>
#include <ByteStream.h>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#include <EngineTypes.h>
#include <MathUtils.h>

class MAnim;
class Model;
class FileManager;

/**
 * Class for loading MObj meshes
 */
class MObj
{
public:
    friend class Model;
    friend class MeshManager;

    static const size_t c_MD5HashLength = 16;

    struct SkinnedVertex
    {
        glm::vec3 m_Pos;
        glm::vec3 m_Normal;
        glm::vec3 m_BiTangent;
        glm::vec3 m_Tangent;
        glm::vec2 m_UV;
        glm::vec4 m_BoneWeights;
        glm::vec4 m_BoneIndices;
    };

    struct StaticVertex
    {
        glm::vec3 m_Pos;
        glm::vec3 m_Normal;
        glm::vec3 m_BiTangent;
        glm::vec3 m_Tangent;
        glm::vec2 m_UV;
    };

    struct Joint
    {
        int       m_ParentID;
        glm::vec3 m_Pos;
        glm::quat m_Orient;
        glm::vec3 m_Scale;

        glm::mat4 m_Transform;
        glm::mat4 m_CombinedTransform;

        Joint& operator= (const Joint& other)
        {
            m_ParentID = other.m_ParentID;
            m_Pos = other.m_Pos;
            m_Orient = other.m_Orient;
            m_Scale = other.m_Scale;
            m_Transform = other.m_Transform;
            m_CombinedTransform = other.m_CombinedTransform;

            return *this;
        }
    };

    typedef std::vector<EngineTypes::VertexIndexType> IndexBuffer;

    struct Mesh
    {
        enum MeshType
        {
            e_MeshTypeSkinned = 0,
            e_MeshTypeStatic = 1
        };

        struct SubMesh
        {
            std::string  m_MaterialName;
            uint16_t m_MeshType;
            uint32_t m_FirstIndex;
            uint32_t m_IndexCount;
            EngineTypes::VertexIndexType minVertexIndexValue;
            EngineTypes::VertexIndexType maxVertexIndexValue;
        };

        typedef std::vector<SkinnedVertex> SkinnedVertexList;
        typedef std::vector<StaticVertex> StaticVertexList;
        typedef std::vector<Mesh::SubMesh> SubMeshList;

        MeshType type;
        int16_t parentMeshID;
        std::string name;
        SkinnedVertexList  skinnedVertices;
        StaticVertexList staticVertices;
        IndexBuffer indices;
        SubMeshList subMeshes;
    };

    typedef std::vector<Joint> JointList;
    typedef std::vector<Mesh*> MeshList;

    MObj(FileManager& fileManager);
    ~MObj();
    bool LoadFromFile(const std::string& fileName);

    inline const Mesh* GetMesh(size_t meshIndex) const
    {
        assert( meshIndex < m_Meshes.size() && "Mesh index must be lower than the number of meshes" );
        return m_Meshes[meshIndex];
    }

    inline size_t GetMeshCount() const
    {
        return m_Meshes.size();
    }

    /**
     * @return A pointer to the requested Mesh transferring ownership
     *         of this Mesh pointer from the MObj to the caller
     */
    inline Mesh* RelinquishMesh(size_t meshIndex)
    {
        assert( meshIndex < m_Meshes.size() && "Mesh index must be lower than the number of meshes" );

        Mesh* retVal = m_Meshes[meshIndex];
        m_Meshes[meshIndex] = nullptr;

        return retVal;
    }

    void DebugDumpData();

protected:
    /**
     * Check if a given animation is compatible with this mesh
     */
    bool CheckAnimation(const MAnim& animation) const;

private:
    enum OptionFlag
    {
        e_OptionFlagContainsSkinnedMeshes = MathUtils::GetBit<0>::Result
    };

    // Used to prevent copying
    MObj& operator = (const MObj& other);
    MObj(const MObj& other);

    bool _VerifyFileTypeAndVersion(ByteStream& byteStream);
    bool _VerifyFileSize(ByteStream& byteStream,
                         const uint32_t jointNamesSectionSize,
                         const uint32_t meshesNamesSectionSize,
                         uint32_t totalMeshesMaterialsPathsStringLen,
                         uint16_t jointCount,
                         uint16_t meshCount,
                         uint16_t subMeshCount,
                         uint32_t vertexCount,
                         uint32_t indexCount);
    bool _ReadString(std::string& outVal, ByteStream& byteStream);
    bool _ReadOptionFlagsBitMask(ByteStream& byteStream, uint32_t& optionFlagsBitMask);
    bool _ReadJointMeshAndSubMeshCount(ByteStream& byteStream,
                                       uint16_t& jointCount,
                                       uint16_t& meshCount,
                                       uint16_t& subMeshCount);
    bool _ReadVertexCount(ByteStream& byteStream, uint32_t& vertexCount);
    bool _ReadIndexCount(ByteStream& byteStream, uint32_t& indexCount);
    bool _ReadJointNamesSectionSize(ByteStream& byteStream, uint32_t& jointNamesSectionSize);
    bool _ReadMeshesNamesSectionSize(ByteStream& byteStream, uint32_t& meshesNamesSectionSize);
    bool _ReadMeshsMaterialsPathsStringsLen(ByteStream& byteStream, uint32_t& totalStringsLen);
    bool _ReadBoundingBox(ByteStream& byteStream);
    bool _SkipJointNamesSection(ByteStream& byteStream, const uint32_t sectionSizeInBytes);
    bool _ReadMeshNames(ByteStream& byteStream,
                        uint16_t meshCount,
                        std::vector<std::string>& outMeshNames);
    bool _ReadJoints(ByteStream& byteStream, uint16_t jointCount, JointList& outJoints);
    bool _ReadMeshes(ByteStream& byteStream,
                     uint16_t meshCount,
                     const std::vector<std::string>& meshNames,
                     MeshList& outMeshes);
    bool _ReadSubMesh(ByteStream& byteStream,
                      std::string& outMaterialName,
                      uint32_t& firstIndex,
                      uint32_t& indexCount,
                      uint32_t& minMeshIndexValue,
                      uint32_t& maxMeshIndexValue);
    bool _ReadSkinnedVertices(ByteStream& byteStream,
                              Mesh::SkinnedVertexList& outVertexList,
                              const uint32_t vertexCount);
    bool _ReadStaticVertices(ByteStream& byteStream,
                             Mesh::StaticVertexList& outVertexList,
                             const uint32_t vertexCount);
    bool _ReadIndices(ByteStream& byteStream, IndexBuffer& outIndices, const uint32_t indexCount);
    void _GenerateInverseBindPose(const JointList& joints, EngineTypes::MatrixArray& outInverseBindPose);

    FileManager& m_FileManager;
    uint8_t m_BoneNamesMD5Hash[c_MD5HashLength];
    JointList   m_Joints;
    EngineTypes::MatrixArray m_InverseBindPose;
    EngineTypes::AABB m_BoundingBox;
    MeshList m_Meshes;
    std::string m_ModelDirectory;
    bool m_HasSkinnedMeshes;
};

#endif // MOBJ_H_INCLUDED
