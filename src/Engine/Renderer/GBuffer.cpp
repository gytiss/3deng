#include "GBuffer.h"
#include <CommonDefinitions.h>
#include <GraphicsDevice/FBOColorAttachmentTexture.h>
#include <GraphicsDevice/DepthTexture.h>
#include <Exceptions/GenericException.h>
#include <OpenGLErrorChecking.h>

//-----------------------------------------------------------------------

GBuffer::GBuffer(GraphicsDevice& graphicsDevice, SystemInterface& systemInterface)
: m_SystemInterface( systemInterface ),
  m_FBO( graphicsDevice, m_SystemInterface ),
  m_GraphicsDevice( graphicsDevice )
{

}

//-----------------------------------------------------------------------

bool GBuffer::Init(DepthTexture& depthBuffer)
{
    const size_t windowWidth = m_SystemInterface.GetDrawableSurfaceSize().widthInPixels;
    const size_t windowHeight = m_SystemInterface.GetDrawableSurfaceSize().heightInPixels;

    FBOColorAttachmentTexture* specularTexture = new FBOColorAttachmentTexture( m_GraphicsDevice,
                                                                                windowWidth,
                                                                                windowHeight,
                                                                                FBOColorAttachmentTexture::e_TypeIdxGBufferVertexPossitionMapViewSpace,
#ifdef OPENGL_ES
                                                                                FBOColorAttachmentTexture::e_DataFormatRGBA32UI );
#else
                                                                                FBOColorAttachmentTexture::e_DataFormatRGB32F );
#endif

    FBOColorAttachmentTexture* diffuseTexture = new FBOColorAttachmentTexture( m_GraphicsDevice,
                                                                               windowWidth,
                                                                               windowHeight,
                                                                               FBOColorAttachmentTexture::e_TypeIdxGBufferDiffuseMap,
#ifdef OPENGL_ES
                                                                               FBOColorAttachmentTexture::e_DataFormatRGB10Alpha2 );
#else
                                                                               FBOColorAttachmentTexture::e_DataFormatRGB32F );
#endif

    FBOColorAttachmentTexture* normalsTexture = new FBOColorAttachmentTexture( m_GraphicsDevice,
                                                                               windowWidth,
                                                                               windowHeight,
                                                                               FBOColorAttachmentTexture::e_TypeIdxGBufferVertexNormalMapViewSpace,
#ifdef OPENGL_ES
                                                                               FBOColorAttachmentTexture::e_DataFormatRGBA32UI );
#else
                                                                               FBOColorAttachmentTexture::e_DataFormatRGB32F );
#endif

    m_FBO.AttachColorTexture( specularTexture );
    m_FBO.AttachColorTexture( diffuseTexture );
    m_FBO.AttachColorTexture( normalsTexture );
    m_FBO.AttachDepthTexture( &depthBuffer );

    FBO::BindDefaultFBOForDrawing();

    return true;
}

//-----------------------------------------------------------------------

void GBuffer::BindForGeomPass()
{
    m_FBO.BindColorAttachmentsForDrawing( FBO::e_ColorAttachment0 | FBO::e_ColorAttachment1 | FBO::e_ColorAttachment2 );
}

//-----------------------------------------------------------------------

void GBuffer::BindForStencilPass()
{
    // Must disable the draw buffers
    m_FBO.UnbindColorAttachments();
}

//-----------------------------------------------------------------------

void GBuffer::BindForLightPass()
{
    m_FBO.BindDepthAttachmentTextureForReading();
    m_FBO.BindColorAttachmentTexturesForReading( FBO::e_ColorAttachment0 | FBO::e_ColorAttachment1 | FBO::e_ColorAttachment2 );
}

//-----------------------------------------------------------------------
