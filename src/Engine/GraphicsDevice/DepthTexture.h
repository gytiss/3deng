#ifndef DEPTHTEXTURE_H_INCLUDED
#define DEPTHTEXTURE_H_INCLUDED

#include "Texture.h"
#include <SystemInterface.h>
#include <list>

class FBO;

class DepthTexture : public Texture
{
public:
    friend class FBO;

    DepthTexture(GraphicsDevice& graphicsDevice,
                 SystemInterface& systemInterface,
                 TypeIdx textureTypeIdx);

    ~DepthTexture();

protected:
    /**
     * Make depth texture aware that it is attached to a given FBO
     * (Note: This function is called by a given FBO when depth texture is attached to it)
     */
    void RegisterFBO(FBO* fbo);

    /**
     * Inform depth buffer that it is no longer attached to a given FBO
     * (Note: This function is called by a given FBO when it is being destroyed)
     */
    void DregisterFBO(FBO* fbo);

private:
    typedef std::list<FBO*> FBOList;

    // Used to prevent copying
    DepthTexture& operator = (const DepthTexture& other);
    DepthTexture(const DepthTexture& other);

    void _Reinitialize(const SystemInterface::DrawableSurfaceSize& drawableSurfaceSize);
    static void _DrawableSurfaceSizeChaged(const SystemInterface::DrawableSurfaceSize& newSize, void* userData);

    /// All FBOs this  depth texture is attached to
    FBOList m_FBOsAttachedTo;

    SystemInterface& m_SystemInterface;
};

#endif // DEPTHTEXTURE_H_INCLUDED
