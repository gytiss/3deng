#include "AudioData.h"
#include <Exceptions/GenericException.h>
#include <Resources/FileManager.h>
#include <cerrno>
#include <cstring>
#include <tremor/ivorbisfile.h>

//-----------------------------------------------------------------------

AudioData::AudioData(FileManager& fileManager)
: m_SampleRate( 0 ),
  m_SampleWidthInBytes( 0 ),
  m_NumberOfChannels( 0 ),
  m_RawSamples(),
  m_Valid( false ),
  m_FileManager( fileManager )
{
}

//-----------------------------------------------------------------------

AudioData::AudioData(AudioData&& that)
: m_SampleRate( that.m_SampleRate ),
  m_SampleWidthInBytes( that.m_SampleWidthInBytes ),
  m_NumberOfChannels( that.m_NumberOfChannels ),
  m_RawSamples( std::move( that.m_RawSamples ) ),
  m_Valid( that.m_Valid ),
  m_FileManager( that.m_FileManager )
{
}

//-----------------------------------------------------------------------

AudioData::~AudioData()
{
}

//-----------------------------------------------------------------------

bool AudioData::InitFromOggFile(const std::string& oggFilePath)
{
    bool retVal = true;

    m_RawSamples.clear();

    RawOggDataBuffer oggRawData;
    oggRawData.dataReadSoFar = 0;

    retVal = retVal && _ReadRawOggData( oggFilePath, oggRawData );
    retVal = retVal && _ReadRawAudioSampesFromRawOggData( oggRawData, m_RawSamples );

    return retVal;
}

//-----------------------------------------------------------------------

bool AudioData::_ReadRawOggData(const std::string& oggFilePath,
                                RawOggDataBuffer& oggRawData)
{
    bool retVal = false;

    FileManager::File file;

    if( m_FileManager.OpenFile( oggFilePath, FileManager::e_FileTypeBinary, file ) )
    {
        const size_t fileSize = file.Size();
        std::unique_ptr<uint8_t[]> rawData( new uint8_t[fileSize] );

        if( file.Read( rawData.get(), sizeof( uint8_t ), fileSize ) )
        {
            oggRawData.data =  std::move( rawData );
            oggRawData.totalDataSize = fileSize;
            oggRawData.dataReadSoFar = 0;
            retVal = true;
        }
    }

    return retVal;
}

//-----------------------------------------------------------------------

bool AudioData::_ReadRawAudioSampesFromRawOggData(RawOggDataBuffer& oggRawData,
                                                  RawSamples& outRawSamples)
{
    m_Valid = false;

    ov_callbacks callbacks;
    callbacks.read_func = OggDataReadCallback;
    callbacks.seek_func = nullptr;
    callbacks.close_func = nullptr;
    callbacks.tell_func = nullptr;

    OggVorbis_File vorbisFileHandle;

    const int ovOpenReturnVal = ov_open_callbacks( &oggRawData, &vorbisFileHandle, nullptr, 0, callbacks );
    if( ovOpenReturnVal == 0 )
    {
        if( ov_streams( &vorbisFileHandle ) > 1 )
        {
            throw GenericException( "OGG: Trying to load file with more than one logical bitstream" );
        }
        else
        {
            const size_t bitStreamIndex = 0;
            vorbis_info* vorbisInfo = ov_info( &vorbisFileHandle, bitStreamIndex );
            if( vorbisInfo )
            {
                m_SampleRate = vorbisInfo->rate;
                m_NumberOfChannels = vorbisInfo->channels;
                m_SampleWidthInBytes = 2;

                const size_t tempReadBufferSize = 8192;
                uint8_t tempReadBuffer[tempReadBufferSize];
                int currentLogicalBitStreamNumber = 0;

                long ovReadRetVal = 0;

                do
                {
                    ovReadRetVal = ov_read( &vorbisFileHandle,
                                            reinterpret_cast<char*>( tempReadBuffer ),
                                            tempReadBufferSize,
                                            &currentLogicalBitStreamNumber );
                    if( ovReadRetVal > 0 )
                    {
                        for(long i = 0; i < ovReadRetVal; i++)
                        {
                            outRawSamples.push_back( tempReadBuffer[i] );
                        }
                    }
                }
                while( ovReadRetVal > 0 );

                // All samples read successfully
                if( ovReadRetVal == 0 )
                {
                    m_Valid = true;
                }
                else
                {
                    outRawSamples.clear();

                    switch( ovOpenReturnVal )
                    {
                        case OV_HOLE: throw GenericException( "OGG Reading Samples: There was an interruption in the data (one of: garbage between pages, loss of sync followed by recapture, or a corrupt page)" );
                        case OV_EBADLINK: throw GenericException( "OGG Reading Samples: An invalid stream section was supplied to libvorbisfile, or the requested link is corrupt" );
                        case OV_EINVAL: throw GenericException( "OGG Reading Samples:The initial file headers couldn't be read or are corrupt, or that the initial open call for vf failed" );
                        default: throw GenericException( "OGG Reading Samples: Unknown error" );
                    }
                }
            }
            else
            {
                throw GenericException( "OGG: Failed to get vorbis info" );
            }
        }
    }
    else
    {
        ov_clear( &vorbisFileHandle );

        switch( ovOpenReturnVal )
        {
            case OV_EREAD: throw GenericException( "OGG: A read from media returned an error" );
            case OV_ENOTVORBIS: throw GenericException( "OGG: Bitstream does not contain any Vorbis data" );
            case OV_EVERSION: throw GenericException( "OGG: Vorbis version mismatch" );
            case OV_EBADHEADER: throw GenericException( "OGG: Invalid Vorbis bitstream header" );
            case OV_EFAULT: throw GenericException( "OGG: Internal logic fault; indicates a bug or heap/stack corruption" );
            default: throw GenericException( "OGG: Unknown error" );
        }

    }

    return m_Valid;
}

//-----------------------------------------------------------------------

size_t AudioData::OggDataReadCallback(void* outDataPtr,
                                      size_t chunkSize,
                                      size_t chunksToRead,
                                      void* dataSource)
{
    size_t retVal = 0;

    RawOggDataBuffer* rawOggDataBuffer = static_cast<RawOggDataBuffer*>( dataSource );

    if( outDataPtr )
    {
        // If caller wants to read zero bytes then do nothing and return without error
        if ( !chunkSize || !chunksToRead )
        {
            errno = 0;
            retVal = 0;
        }
        else
        {
            if( ( rawOggDataBuffer == nullptr ) ||
                ( rawOggDataBuffer->data.get() == nullptr ) ||
                ( rawOggDataBuffer->dataReadSoFar > rawOggDataBuffer->totalDataSize ) )
            {
                errno = EBADF;
                retVal = 0;
            }
            else
            {
                const size_t bytesRemaining = ( rawOggDataBuffer->totalDataSize - rawOggDataBuffer->dataReadSoFar );
                size_t bytesToRead = ( chunkSize * chunksToRead );

                if( bytesToRead > bytesRemaining )
                {
                    bytesToRead = bytesRemaining;
                }

                memcpy( static_cast<uint8_t*>(outDataPtr), rawOggDataBuffer->data.get() + rawOggDataBuffer->dataReadSoFar, bytesToRead );
                rawOggDataBuffer->dataReadSoFar += bytesToRead;

                size_t chunksRead = ( bytesToRead / chunkSize );

                // A chunk was partially read
                if( ( bytesToRead % chunkSize ) != 0 )
                {
                    chunksRead++;
                }

                retVal = chunksRead;
            }
        }
    }
    else
    {
        errno = EFAULT;
        retVal = 0;
    }


    return retVal;
}
