#ifndef AUDIODEVICE_ECHOEFFECT_H_INCLUDED
#define AUDIODEVICE_ECHOEFFECT_H_INCLUDED

#include "Effect.h"

namespace AudioDevice
{

class Context;
class EffectSlot;

/**
 * The echo effect generates discrete, delayed instances of the input signal.  The amount of delay
 * and feedback is controllable.  The delay is ‘two tap’ – you can control the interaction between two
 * separate instances of echoes.
 */
class EchoEffect : public Effect
{
public:
    EchoEffect(Context& context, EffectSlot& effectSlot);
    ~EchoEffect() = default;

   /**
    * This property controls the delay between the original sound and the first ‘tap’, or echo
    * instance. Subsequently, the value for Echo Delay is us ed to determine the time delay
    * between each ‘second tap’ and the next ‘first tap’.
    *
    * Value Units: Second
    * Value range: 0.0 to 0.207
    */
    void SetDelay(float seconds);

   /**
    * This property controls the delay between the first ‘tap’ and the second ‘tap’. Subsequently, the
    * value for Echo LR Delay is used to determine the time delay between each ‘first tap’ and the next
    * ‘second tap’.
    *
    * Value Units: Seconds
    * Value range: 0.0 to 0.404
    */
    void SetLRDelay(float seconds);

   /**
    * This property controls the amount of high frequency damping applied to each echo.  As the sound
    * is subsequently fed back for further echoes, damping results in an echo which progressively gets
    * softer in tone as well as intensity.
    *
    * Value Units: N/A
    * Value range: 0.0 to 0.99
    */
    void SetDamping(float value);

   /**
    * This property controls the amount of feedback the output signal fed back into the input.
    * Use this parameter to create "cascading" echoes.  At full magnitude, the identical sample
    * will repeat endlessly.  Below full magnitude, the sample will repeat and fade.
    *
    * Value Units: N/A
    * Value range: 0.0 to 1.0
    */
    void SetEchoFeedback(float value);

   /**
    * This property controls how hard panned the individual echoes are.  With a value of 1.0,
    * the first ‘tap’ will be panned hard left, and the second tap hard right.  A value of
    * –1.0 gives the opposite result.  Settings nearer to 0.0 result in less emphasized panning.
    *
    * Value Units: N/A
    * Value range: -1.0 to 1.0
    */
    void SetEchoSpread(float value);

    float GetDelay();
    float GetLRDelay();
    float GetDamping();
    float GetEchoFeedback();
    float GetEchoSpread();

    inline float GetMinDelayValue()
    {
        return AL_ECHO_MIN_DELAY;
    }

    inline float GetMaxDelayValue()
    {
        return AL_ECHO_MAX_DELAY;
    }

    inline float GetMinLRDelayValue()
    {
        return AL_ECHO_MIN_LRDELAY;
    }

    inline float GetMaxLRDelayValue()
    {
        return AL_ECHO_MAX_LRDELAY;
    }

    inline float GetMinDampingValue()
    {
        return AL_ECHO_MIN_DAMPING;
    }

    inline float GetMaxDampingValue()
    {
        return AL_ECHO_MAX_DAMPING;
    }

    inline float GetMinEchoFeedbackValue()
    {
        return AL_ECHO_MIN_FEEDBACK;
    }

    inline float GetMaxEchoFeedbackValue()
    {
        return AL_ECHO_MAX_FEEDBACK;
    }

    inline float GetMinEchoSpreadValue()
    {
        return AL_ECHO_MIN_SPREAD;
    }

    inline float GetMaxEchoSpreadValue()
    {
        return AL_ECHO_MAX_SPREAD;
    }

private:
    // Used to prevent copying
    EchoEffect& operator = (const EchoEffect& other) = delete;
    EchoEffect(const EchoEffect& other) = delete;
};

}

#endif // AUDIODEVICE_ECHOEFFECT_H_INCLUDED
