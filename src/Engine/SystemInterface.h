#ifndef SYSTEMINTERFACE_H_INCLUDED
#define SYSTEMINTERFACE_H_INCLUDED

#include <list>
#include <cstring>
#include <cstdint>

class SystemInterface
{
public:
    struct DrawableSurfaceSize
    {
        DrawableSurfaceSize(size_t width, size_t height)
        : widthInPixels( width ),
          heightInPixels( height )
        {

        }

        size_t widthInPixels;
        size_t heightInPixels;
    };

    typedef void (*DrawableSurfaceSizeChangedCallback) (const DrawableSurfaceSize& newSize, void* userData);

    virtual ~SystemInterface()
    {
    }

private:
    typedef std::pair<DrawableSurfaceSizeChangedCallback, void*> CallbackAndDataPair;
    typedef std::list<CallbackAndDataPair> CallbackList;

public:

   /**
    * Returns number of seconds since the start of the application
    */
    inline double GetTicksInSeconds()
    {
        /**
         *  Even if you use doubles for time, the precision available will still change
         *  as game time marches on from zero to the length of your game. These precision
         *  changes – while smaller with doubles than with floats – can still be dangerous.
         *  Luckily there is a convenient way to get the consistent precision of an integer,
         *  with the convenient units of a double.
         *
         *  If you start your game clock at about 4 billion (more precisely 2^32, or any large power of two)
         *  then your exponent, and hence your precision, will remain constant for the next ~4 billion seconds, or ~136 years.
         *
         *  And, when using doubles, this precision is approximately one microsecond.
         *
         *  So there you have it. The one-true answer. Store elapsed game time in a double,
         *  starting at 2^32 seconds. You will get constant precision of better than a microsecond
         *  for over a century, and if you accidentally store time in a float you will precision
         *  errors immediately instead of after hours of game play. You read it here first.
         *
         *  Reference: https://randomascii.wordpress.com/2012/02/13/dont-store-that-in-a-float/
         */
        static const uint64_t c_StartingValueForConsistentPrecision = 4294967296;

        const uint64_t totalNumberOfMilliseconds = ( c_StartingValueForConsistentPrecision + _GetTicksInMilliseconds() );
        const uint64_t totalNumberOfSeconds = ( totalNumberOfMilliseconds / 1000 );
        const uint64_t leftoverMilliseconds = ( totalNumberOfMilliseconds - ( totalNumberOfSeconds * 1000 ) );

        const double leftoverMillisecondsValueInSeconds = ( static_cast<double>( leftoverMilliseconds ) * 0.001 );
        const double totalElapsedTimeInSeconds = ( static_cast<double>( totalNumberOfSeconds ) + leftoverMillisecondsValueInSeconds );

        return totalElapsedTimeInSeconds;
    }

    /**
     * Get size of the current drawable surface.
     */
    virtual const DrawableSurfaceSize& GetDrawableSurfaceSize() const = 0;

    /**
     * Register drawable surface size event listener
     */
    void RegisterDrawableSurfaceSizeChangedCallback(DrawableSurfaceSizeChangedCallback callback, void* userData)
    {
        m_DrawableSurfaceChangedCallbacks.push_back( CallbackAndDataPair( callback, userData ) );
    }

    void DeregisterDrawableSurfaceSizeChangedCallback(DrawableSurfaceSizeChangedCallback callbackToDeregister, void* userDataOfTheCallback)
    {
        bool done = false;

        CallbackList::iterator it = m_DrawableSurfaceChangedCallbacks.begin();
        while( !done && ( it != m_DrawableSurfaceChangedCallbacks.end() )  )
        {
            DrawableSurfaceSizeChangedCallback callBack = (*it).first;
            void* userData = (*it).second;

            if( ( callBack == callbackToDeregister ) && ( userData == userDataOfTheCallback ) )
            {
                m_DrawableSurfaceChangedCallbacks.erase( it );
                done = true;
            }
            else
            {
                ++it;
            }
        }
    }

protected:
    /**
     * Returns number of milliseconds since the start of the application
     */
     virtual uint64_t _GetTicksInMilliseconds() = 0;

    /**
     * Needs to be called every time drawable surface size changes
     */
    void _DrawableSurfaceSizeChanged(const DrawableSurfaceSize& newSize)
    {
        for(CallbackList::iterator it = m_DrawableSurfaceChangedCallbacks.begin(); it != m_DrawableSurfaceChangedCallbacks.end(); ++it)
        {
            DrawableSurfaceSizeChangedCallback callBack = (*it).first;
            void* userData = (*it).second;
            callBack( newSize, userData );
        }
    }

private:
    /// Callback functions which will be called every time drawable surface size changes
    CallbackList m_DrawableSurfaceChangedCallbacks;
};

#endif // SYSTEMINTERFACE_H_INCLUDED
