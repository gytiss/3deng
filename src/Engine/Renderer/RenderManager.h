#ifndef RENDERMANAGER_H_INCLUDED
#define RENDERMANAGER_H_INCLUDED

#include <list>
#include <vector>
#include <glm/glm.hpp>
#include <ScreenshotCapture.h>
#include <GraphicsDevice/FBO.h>
#include <GraphicsDevice/DepthTexture.h>
#include <EngineTypes.h>
#include <SystemInterface.h>
#include "ShaderPrograms.h"
#include "GBuffer.h"
#include "LightManager.h"
#include "RenderableObject.h"
#include "Renderer/RenderingHelperMeshes.h"
#ifdef VR_BACKEND_ENABLED
    #include "VRHMDBuffer.h"
#endif

class GUIRenderInterface;
class GraphicsDevice;
class FileManager;

#ifdef VR_BACKEND_ENABLED
    class VRBackend;
#endif

class RenderManager
{
public:
    enum RenderingPath
    {
        e_RenderingPathForward = 0,
        e_RenderingPathDeferred = 1
    };

    enum ProjectionType
    {
        e_ProjectionTypePerspective = 0,
        e_ProjectionTypeOrthograpchic = 1
    };

    RenderManager(GraphicsDevice& graphicsDevice,
                  SystemInterface& systemInterface,
#ifdef VR_BACKEND_ENABLED
                  VRBackend& vrBackend,
#endif
                  FileManager& fileManager);

    ~RenderManager();

    typedef std::pair<uint32_t, RenderableObject> PickingPair;

    /**
     * @param objectsForPicking - Renderables to be rendered for picking purposes
     * @param pickingPosX - X coordinate of the pixel to pick (Origin is at the lower left corner)
     * @param pickingPosY - Y coordinate of the pixel to pick (Origin is at the lower left corner)
     *
     * @return Value of the pixel data at the given coordinates
     */
    uint32_t DoGameObjectPicking(std::vector<PickingPair>& objectsForPicking, size_t pickingPosX, size_t pickingPosY);

    void Render();
    void AddRenderable(const RenderableObject& r, bool renderLast = false);
    void AddGUIRenderables(const RenderableObject& r);
    void SetViewTransform(const glm::mat4& viewTransform);
    void SetProjectionTransform(ProjectionType projectionType, const glm::mat4& projectionTransform);
    void SetFrustumNearAndFarClippingPlaneDistances(float nearDistance, float farDistance);

    inline LightManager& GetLightManager()
    {
        return m_LightManager;
    }

    inline void SetRenderingPath( RenderingPath path )
    {
        m_RenderingPath = path;
    }

    inline RenderingPath GetRenderingPath() const
    {
        return m_RenderingPath;
    }

    inline void SetGUIReinderingInterface(GUIRenderInterface* i)
    {
        m_GUIRenderInterface = i;
    }

    inline void SetRenderInWireframeMode(bool val)
    {
        m_RenderInWireframeMode = val;
    }

    bool GetRenderInWireframeMode() const
    {
        return m_RenderInWireframeMode;
    }

    void SetBackgroundColor(const EngineTypes::SRGBColor& color)
    {
        m_BackgroundColor = color;
    }

private:
    enum ShaderType
    {
        e_ShaderTypeInvalid = 0,
        e_ShaderTypeSkinned = 1,
        e_ShaderTypeStatic = 2,
        e_ShaderTypeColouredVertices = 3,
        e_ShaderTypeColouredMesh = 4,
        e_ShaderTypeTexturedMesh = 5
    };

    static EngineTypes::SRGBColor c_DefaultBackgroundColor;

    class RenderUnit : protected std::pair<ShaderType, RenderableObject>
    {
    public:
        RenderUnit(ShaderType shaderType, const RenderableObject& renderable)
        : std::pair<ShaderType, RenderableObject>(shaderType, renderable )
        {
        }

        inline ShaderType GetShaderType() const
        {
            return first;
        }

        inline RenderableObject& GetRenderable()
        {
            return second;
        }
    };

    typedef std::list<RenderUnit> RenderList_t;

    void _SetupPostProcessingFBO(DepthTexture& depthBuffer);
    void _ForwardPath();
    void _DeferredPath();
    void _GeometryRenderingPass();
    void _LightRenderingPass();
    void _HDRAndGammaCorrectionPass();
    void _DepthBufferDebugPass(const std::string& outputImageFileName);
    void _FinalRenderingPass();
    void _PostDefferedForwardPass();
    void _RenderObjects(RenderList_t& renderList);
    void _RenderObjectsForward(RenderList_t& renderList);
    void _RenderForwardOnlyObjects(RenderList_t& renderList);
    void _RenderPrimitives(const RenderableObject& renderable);
    void _RenderGUI();

    static void _DrawableSurfaceSizeChaged(const SystemInterface::DrawableSurfaceSize& newSize, void* userData);

    SystemInterface& m_SystemInterface;
    SystemInterface::DrawableSurfaceSize m_DrawableSurfaceSize;

    RenderList_t m_FordwardOrDefferedRenderList;
    RenderList_t m_FordwardOrDefferedRenderLastRenderList;
    RenderList_t m_FordwardOnlyRenderList;
    RenderList_t m_FordwardOnlyRenderLastRenderList;
    RenderList_t m_GUIRenderablesList;
    GraphicsDevice& m_GraphicsDevice;

    // Must be defined above any FBO that uses it, because otherwise it will get destructed before that FBO and cause
    // undefined behaviour when that FBO  tries  to deregister itself(upon destruction) from this depth texture
    DepthTexture m_DepthBuffer;

    FBO m_PostProcessingFBO;
    GBuffer m_GBuffer;
#ifdef VR_BACKEND_ENABLED
    VRBackend& m_VRBackend;
    VRHMDBuffer m_VRHMDBuffer;
    VRHMDBuffer::Eye m_VRHMDEyeBeingRendered;
#endif
    LightManager m_LightManager;
    ShaderProgGBufferSkinned m_GBufferSkinnedShaderProg;
    ShaderProgGBufferStatic m_GBufferStaticShaderProg;
    ShaderProgToneMappingAndGammaCorrection m_ToneMappingAndGammaCorrectionShaderProg;
    ShaderProgDebugObjects m_DebugObjectsShaderProg;
    ShaderProgGUI m_GUIShaderProg;
    ShaderProgDebugDepth m_DepthDebugShaderProg;

    ForwardRenderingPointLightShaderSkinned m_ForwardRenderingPointLightSkinnedShaderProg;
    ForwardRenderingPointLightShaderStatic m_ForwardRenderingPointLightStaticShaderProg;
    ForwardRenderingSpotLightShaderSkinned m_ForwardRenderingSpotLightSkinnedShaderProg;
    ForwardRenderingSpotLightShaderStatic m_ForwardRenderingSpotLightStaticShaderProg;
    ForwardRenderingDirectionalLightShaderSkinned m_ForwardRenderingDirectionalLightSkinnedShaderProg;
    ForwardRenderingDirectionalLightShaderStatic m_ForwardRenderingDirectionalLightStaticShaderProg;

    ForwardRenderingNoLightingShaderSkinned m_ForwardRenderingNoLightingSkinnedShaderProg;
    ForwardRenderingNoLightingShaderStatic m_ForwardRenderingNoLightingStaticShaderProg;
    ShaderProgGenericTextured m_GenericTexturedShader;

    ProjectionType m_ProjectionType;
    EngineTypes::VPTransformations m_VPTransformations;
    float m_FrustumNearClippingPlaneDistance;
    float m_FrustumFarClippingPlaneDistance;
    RenderingPath m_RenderingPath;
    GUIRenderInterface* m_GUIRenderInterface;
    FullScreenQuadMesh m_FullScreenQuadMesh;
    ScreenshotCapture m_ScreenshotCapture;
    glm::mat4 m_CustomGUIProjectionMatrix;

    // Forward rendering data below
    enum ForwardLigthType
    {
        e_ForwardLigthTypePoint = 1,
        e_ForwardLigthTypeSpot = 2,
        e_ForwardLigthTypeDirectional
    };

    std::vector<glm::vec3> m_ForwardLightColors;
    std::vector<glm::vec3> m_ForwardLightPositionsInViewSpace;
    std::vector<glm::vec3> m_ForwardLightDirectionsInViewSpace;
    std::vector<float> m_ForwardLightAmbientIntensities;
    std::vector<float> m_ForwardLightDiffuseIntensities;
    std::vector<float> m_ForwardLightRadiuses;
    std::vector<float> m_ForwardLightCutoffValues;
    std::vector<unsigned int> m_ForwardLightTypes;

    EngineTypes::SRGBColor m_BackgroundColor;

    bool m_RenderInWireframeMode;
    bool m_TakeADebugScreenshotOfDefferedDepthBuffers;
    bool m_TakeADebugScreenshotOfDefferedStencilBuffers;
};

#endif // RENDERMANAGER_H_INCLUDED
