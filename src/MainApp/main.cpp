#include <OpenGLIncludes.h>

#include <MsgLogger.h>
#include <StringUtils.h>
#include <InputMapping/InputMapper.h>
#include <Exceptions/GenericException.h>
#include <GUI/GUIContext.h>
#include <GUI/GUIManager.h>
#include <CommonDefinitions.h>
#include <Resources/FileManager.h>
#ifdef VR_BACKEND_ENABLED
    #include <VR/VRBackend.h>
#endif

#include <memory>
#include <cstdlib>

#include "WindowManager.h"
#include "MainAppMainLoop.h"
#include "MainConfig.h"

static void MainInputCallback(InputMapping::MappedInput& inputs,
                              float UNUSED_PARAM(frameTimeInSeconds),
                              void* windowManager)
{
    WindowManager* wm = reinterpret_cast<WindowManager*>( windowManager );

    if( inputs.FindAndConsumeAction( InputMapping::e_ActionToggleCaptureMouseInTheWindow )  )
    {
        wm->SetCaptureMouseInTheWindow( !wm->GetCaptureMouseInTheWindow() );
    }

    if( inputs.FindAndConsumeAction( InputMapping::e_ActionToggleCenterMouse )  )
    {
        wm->SetMouseIsRelativeToTheCenter( !wm->GetMouseIsRelativeToTheCenter() );
    }
}

static void MainAppLoop(WindowManager* window,
#ifdef VR_BACKEND_ENABLED
                        VRBackend* vrBackend,
#endif
                        InputMapping::InputMapper& inputMapper,
                        FileManager& fileManager)
{
    MainAppMainLoop mainAppLoop( inputMapper, 
                                *window, 
#ifdef VR_BACKEND_ENABLED
                                *vrBackend,
#endif
                                fileManager );

    /*--------------------- GUI ---------------------*/
    // Need to be created on the heap, because otherwise it would only be detroyed after the OpenGL context is destroyed and this is an error
    std::unique_ptr<GUIManager> guiManager( new GUIManager( mainAppLoop.GetGraphicsDevice(),
                                                            *window,
                                                            fileManager,
                                                            "Data/GUI",
                                                            true ) );

    if( !guiManager->AddContext( "MainContext" ) )
    {
        throw GenericException( "Failed to add context" );
    }

    guiManager->SwitchToContext( "MainContext" );
    GUIContext* mainContext = guiManager->GetCurrentContext();
//    Rocket::Core::ElementDocument* entityListWindow = mainContext->LoadDocument("Data/GUI/Editor/EntityListWindow.rml");
//
//    if( entityListWindow )
//    {
//        entityListWindow->Show();
//    }
    /*-----------------------------------------------*/

    inputMapper.AddCallback( MainInputCallback, 0, window );
    inputMapper.AddCallback( GUIManager::InputCallback, 1, guiManager.get() );

    mainAppLoop.SetGUIRenderInterface( &guiManager->GetRenderInterface() );
    mainAppLoop.SetGUIContext( mainContext );

    while( !mainAppLoop.Exit() )
    {
        mainAppLoop.Update();
    }
}

int main(int UNUSED_PARAM(argc), char** UNUSED_PARAM(argv))
{
#ifdef VR_BACKEND_ENABLED
    std::unique_ptr<VRBackend> vrBackend;
#endif
    std::unique_ptr<WindowManager> wm;

    try
    {
#ifndef OS_ANDROID
        if ( freopen( "ErrorLog.txt", "wt", stderr ) == nullptr ) // Redirect "stderr" to file named "ErrorLog"
        {
            assert(false && "Failed to redirect stderr");
            exit(1);
        }

        if ( freopen( "ExecutionLog.txt", "wt", stdout ) == nullptr ) // Rewdirect "stdcout" to file named "ExecutionLog"
        {
            assert(false && "Failed to redirect stdout");
            exit(1);
        }
#endif

        std::unique_ptr<FileManager> fileManager( new FileManager() );

        /*---------------------- Input Mapper ----------------------*/
        std::unique_ptr<InputMapping::InputMapper> inputMapper( new InputMapping::InputMapper( *fileManager,
                                                                                               "Data/Config/InputMapping/InputContexts.cfg",
                                                                                               "Data/Config/InputMapping/InputMap.cfg",
                                                                                               "Data/Config/InputMapping/InputRanges.cfg" ) );
        inputMapper->PushContext( "Base" );
        inputMapper->PushContext( "GamePlay" );
        inputMapper->PushContext( "CharacterControl" );
        /*----------------------------------------------------------*/

        MainConfig mainConfig( *fileManager, "Data/Config/Main.cfg" );

        std::string windowTitleStr = mainConfig.GetStrConfigItem(MainConfig::e_StrCfgItemWindowName);

#ifdef VR_BACKEND_ENABLED
        vrBackend.reset( new VRBackend() );
        vrBackend->Initialise();

        windowTitleStr += " ";
        windowTitleStr += vrBackend->GetHMDDriverName();
        windowTitleStr += " | ";
        windowTitleStr += vrBackend->GetHMDName();

        wm.reset( new WindowManager( *inputMapper,
                                     *vrBackend.get(),
                                     windowTitleStr,
                                     ( mainConfig.GetUIntConfigItem( MainConfig::e_UIntCfgItemVSyncEnabled ) == 1 ) ) );
#else
        wm.reset( new WindowManager( *inputMapper,
                                     mainConfig.GetUIntConfigItem( MainConfig::e_UIntCfgItemVideoHorizontalRes ),
                                     mainConfig.GetUIntConfigItem( MainConfig::e_UIntCfgItemVideoVerticalRes ),
                                     windowTitleStr,
                                     ( mainConfig.GetUIntConfigItem( MainConfig::e_UIntCfgItemVideoFullScreen ) == 1 ),
                                     false,
                                     ( mainConfig.GetUIntConfigItem( MainConfig::e_UIntCfgItemVSyncEnabled ) == 1 ) ) );
#endif

#ifndef OPENGL_ES
        /*------------------Setup GLEW-----------------------------*/
        glewExperimental = GL_TRUE; // Enables experimental GLEW features such as glGenVertexArrays etc.

        const GLenum initStatus = glewInit(); // Initialize GLEW

        if (GLEW_OK != initStatus) // Check if GLEW was initialized correctly
        {
            throw GenericException( "GLEW initialization failed with the following message: " + std::string( reinterpret_cast<const char*>( glewGetErrorString( initStatus ) ) ) );
        }

        if( !GL_VERSION_4_3 ) // Check if the required OpenGL version is supported
        {
            throw GenericException( "This program requires OpenGL 4.3 or higher" );
        }
        /*---------------------------------------------------------*/

        while( glGetError() != GL_NO_ERROR ) {} // Flush all redundant warnings introduced by GLEW
#endif

//        wm.SetCaptureMouseInTheWindow( true );
//        wm.SetMouseIsRelativeToTheCenter( true );

        MainAppLoop( wm.get(),
#ifdef VR_BACKEND_ENABLED
                     vrBackend.get(),
#endif
                     *inputMapper, 
                     *fileManager );

#ifdef VR_BACKEND_ENABLED
        vrBackend.reset( nullptr ); // Need to destroy the VR backend before the main window
#endif
        wm->DestroyWindow();
    }
    catch(GenericException& ex)
    {
        MsgLogger::LogError( ex.what() );
        SDL_ShowSimpleMessageBox( SDL_MESSAGEBOX_ERROR, "Fatal Error", ex.what(), nullptr );

        if( wm )
        {
#ifdef VR_BACKEND_ENABLED
            vrBackend.reset(nullptr); // Need to destroy the VR backend before the main window
#endif
            wm->DestroyWindow();
        }

#ifndef OS_ANDROID
        std::exit( EXIT_FAILURE );
#endif
    }

    return 0;
}
