#include "MeshRenderer.h"
#include "MeshFilter.h"
#include "GameObject.h"

//-----------------------------------------------------------------------

MeshRenderer::MeshRenderer(GameObject& owningGameObject, MaterialManager& materialManager)
: Renderer( owningGameObject, e_RendererTypeMesh, "MeshRenderer", materialManager )
{
    GetOwningGameObject().SetComponent( this );
    _FindMesh();
}

//-----------------------------------------------------------------------

const EngineTypes::AABB& MeshRenderer::GetAABB() const
{
    MeshManager::MeshHandle meshHandle = _GetMeshHandle();

    assert( meshHandle.IsValid() && "No mesh has been set, hence can not get AABB" );
    return meshHandle->GetAABB();
}

//-----------------------------------------------------------------------

MeshManager::MeshHandle MeshRenderer::_GetMeshHandle() const
{
    MeshManager::MeshHandle retVal;
    const MeshFilter* meshFilter = GetOwningGameObject().GetComponent<MeshFilter>();

    if( meshFilter &&
        meshFilter->IsEnabled() &&
        meshFilter->m_MeshHandle.IsValid() )
    {
        retVal = meshFilter->m_MeshHandle;
    }

    return retVal;
}

//-----------------------------------------------------------------------

void MeshRenderer::_FindMesh()
{
    MeshManager::MeshHandle meshHandle = _GetMeshHandle();

    if( meshHandle.IsValid() )
    {
        _ResetSubMeshesMaterialsList( meshHandle );
    }
    else
    {
        _ReleaseExistingSubMeshesMaterials();
    }

    _RegenerateRenderables();
}

//-----------------------------------------------------------------------

void MeshRenderer::_RegenerateRenderables()
{
    _ClearRenderables();

    MeshManager::MeshHandle meshHandle = _GetMeshHandle();

    if( meshHandle.IsValid() )
    {
        const Mesh::SubMeshList& subMeshes = meshHandle->GetSubMeshes();

        size_t subMeshIndex = 0;
        for(const Mesh::SubMesh& subMesh : subMeshes)
        {
            MaterialManager::MaterialHandle* materialHandle = GetSubMeshMaterial( subMeshIndex );
            if( materialHandle && !materialHandle->IsValid() )
            {
                materialHandle = nullptr;
            }

            RenderableObject renderable( RenderableObject::e_RenderingTypeIndexed,
                                         GetDepthTestingEnabled(),
                                         static_cast<unsigned int>( subMesh.m_FirstIndex ),
                                         static_cast<unsigned int>( subMesh.m_IndexCount ),
                                         subMesh.m_MinVertexIndexValue,
                                         subMesh.m_MaxVertexIndexValue,
                                         GraphicsDevice::e_DrawPrimitivesTypeTriangles,
                                         meshHandle->GetVAO(),
                                         materialHandle );
            _AddRenderable( renderable );

            subMeshIndex++;
        }
    }
}

//-----------------------------------------------------------------------
