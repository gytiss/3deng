#ifndef EDITORLOGIC_H_INCLUDED
#define EDITORLOGIC_H_INCLUDED

#include <map>
#include <string>
#include <glm/glm.hpp>
#include <SystemInterface.h>
#include <Renderer/RenderManager.h>
#include <InputMapping/InputMapper.h>
#include <GUI/ToolsPanel.h>
#include <Tools/ManipulatorTool.h>

#include "Camera.h"
#include "DrawableText.h"
#include "DrawableEntity.h"

class MainWindow;
class GraphicsDevice;
class FileManager;
class PointLight;
class SpotLight;
class DirectionalLight;
class AnimationManager;
class LightManager;
class MeshManager;
class TextureManager;
class MaterialManager;
class PhysicsManager;
class AudioDataManager;
class SoundManager;
class GameObject;
class GUIRenderInterface;

class TransformComponentWidget;
class RigidBodyComponentWidget;
class CapsuleColliderComponentWidget;
class ConeColliderComponentWidget;
class BoxColliderComponentWidget;
class SphereColliderComponentWidget;

class EditorLogic
{
public:
    EditorLogic(MainWindow& mainWindow,
                GraphicsDevice& graphicsDevice,
                FileManager& fileManager,
                AnimationManager& animationManager,
                MeshManager& meshManager,
                TextureManager& textureManager,
                MaterialManager& materialManager,
                RenderManager& renderManager,
                PhysicsManager& physicsManager,
                AudioDataManager& audioDataManager,
                SoundManager& soundManager,
                SystemInterface& systemInterface);
    ~EditorLogic();

    static void InputCallback(InputMapping::MappedInput& inputs, float frameTimeInSeconds, void* engine)
    {
        reinterpret_cast<EditorLogic*>(engine)->_InputCallback( inputs, frameTimeInSeconds );
    }

    void Render(float frameTimeInSeconds);

    inline void SetGUIRenderInterface(GUIRenderInterface* interface)
    {
        m_RenderManager.SetGUIReinderingInterface( interface );
    }

    inline bool IsAppStillRunning() const
    {
        return m_AppIsStillRunning;
    }

    inline void SetAppIsStillRunning(bool val)
    {
        m_AppIsStillRunning = val;
    }

    inline void SetCurrentFrameTimeInMs(float frameTimeInMilliseconds)
    {
         m_CurrentFrameTimeInMs = frameTimeInMilliseconds;
    }

    inline float GetCurrentFrameTimeInMs() const
    {
        return m_CurrentFrameTimeInMs;
    }

private:
    typedef std::vector<GameObject*> GameObjectList;

    enum KeyPressed
    {
        e_KeyPressedW = 0,
        e_KeyPressedS = 1,
        e_KeyPressedA = 2,
        e_KeyPressedD = 3,
        e_KeyPressedSpace = 4,
        e_KeyPressedLeftArrow = 5,
        e_KeyPressedRightArrow = 6,
        e_KeyPressedUpArrow = 7,
        e_KeyPressedDownArrow = 8,
        e_KeyPressedPageUp = 9,
        e_KeyPressedPageDown = 10,
        e_KeyPressedLastEnum // Used for counting
    };

    enum EntityGroup
    {
        e_EntityGroupPointLights = 0,
        e_EntityGroupSpotLights = 1,

        e_EntityGroupCount // Not a group used for counting
    };

    enum EntityManipType
    {
        e_EntityManipTypeMove = 0,
        e_EntityManipTypeRotate = 1
    };

    void _InputCallback(InputMapping::MappedInput& inputs, float frameTimeInSeconds);
    void _HandleMouseInput(float frameTimeInSeconds);

    void _SetupGUI();
    void _HideAllGUIWithGameObjectData();
    void _PopulateGUIWithGameObjectData(GameObject& gameObject);
    void _InitLights();
    void _BuildScene();
    void _AddRenderablesFromGameObjectHierarchyToTheRenderList(GameObject* gameObject);
    void _AddRenderablesFromGameObjectHierarchyToThePickingList(GameObject* gameObject);
    void _BuildRenderList();
    void _RenderText();

    EditorLogic& operator = (const EditorLogic& other); // Prevent copying of EditorLogic
    EditorLogic(const EditorLogic& other);              // Prevent copying of EditorLogic

    static void _DrawableSurfaceSizeChaged(const SystemInterface::DrawableSurfaceSize& newSize, void* userData);

    SystemInterface& m_SystemInterface;
    SystemInterface::DrawableSurfaceSize m_DrawableSurfaceSize;

    size_t m_CurrentLightNum;
    PointLight* m_PointLights[5];
    SpotLight* m_SpotLights[5];
    DirectionalLight* m_DirectionalLight;

    EntityGroup m_CurrentEntityGroup;
    EntityManipType m_EntityManipType;

    Camera* m_Camera;

    bool m_OtherEntitiesKeyVector[e_KeyPressedLastEnum];
    glm::vec2 m_CurrentAbsoluteMouseCoords;
    glm::vec2 m_PreviousAbsoluteMouseCoords;

    bool m_RenderDebugData;
    bool m_KeysAreControlingEntities;
    bool m_AppIsStillRunning;
    bool m_ClickedWithMouse;
    bool m_MetaKeyIsPressed;
    bool m_MouseLeftButtonIsPressed;
    bool m_MouseMiddleButtonIsPressed;
    bool m_MouseRightButtonIsPressed;
    float m_CurrentFrameTimeInMs;

    PointLight* m_CurrentPointLight;
    SpotLight* m_CurrentSpotLight;

    MeshManager& m_MeshManager;
    TextureManager& m_TextureManager;
    MaterialManager& m_MaterialManager;
    AnimationManager& m_AnimationManager;
    RenderManager& m_RenderManager;
    PhysicsManager& m_PhysicsManager;
    AudioDataManager& m_AudioDataManager;
    SoundManager& m_SoundManager;

    DrawableText m_DrawableText;
    GameObjectList m_GameObjectList;
    GameObject* m_CurrentlySelectedGameObject;

    // Tools Section
    ManipulatorTool m_ManipulatorTool;

    // GUI Section
    MainWindow& m_MainWindow;
    ToolsPanel m_ToolsPanel;
    TransformComponentWidget* m_TransformComponentWidget;
    RigidBodyComponentWidget* m_RigidBodyComponentWidget;
    CapsuleColliderComponentWidget* m_CapsuleColliderComponentWidget;
    ConeColliderComponentWidget* m_ConeColliderComponentWidget;
    BoxColliderComponentWidget* m_BoxColliderComponentWidget;
    SphereColliderComponentWidget* m_SphereColliderComponentWidget;
};

#endif // EDITORLOGIC_H_INCLUDED
