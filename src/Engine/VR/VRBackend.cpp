#include "VRBackend.h"
#include <Exceptions/GenericException.h>
#include <GraphicsDevice/GraphicsDevice.h>
#include <MsgLogger.h>
#include <memory>
#include <MathUtils.h>
#include <EngineDefaults.h>

//-----------------------------------------------------------------------

VRBackend::VRBackend()
: m_GraphicsDevice( nullptr ),
  m_HMD( nullptr ),
  m_RenderModels( nullptr ),
  m_HMDRenderWidth( 0 ),
  m_HMDRenderHeight( 0 ),
  m_VRDevicesMatrices(),
  m_TrackedDevicesPoses()
{
}

//-----------------------------------------------------------------------

VRBackend::~VRBackend()
{
}

//-----------------------------------------------------------------------

void VRBackend::Initialise()
{
    // Initialise all matrices with a unity matrix
    for (size_t i = 0; i < e_VRDeviceMatrix_Count; i++)
    {
        m_VRDevicesMatrices[i] = MathUtils::c_IdentityMatrix;
    }

    vr::EVRInitError eError = vr::VRInitError_None;

    // Loading the SteamVR Runtime
    m_HMD = vr::VR_Init( &eError, vr::VRApplication_Scene );

    if( eError != vr::VRInitError_None )
    {
        throw GenericException( "Unable to init VR runtime: " + std::string( vr::VR_GetVRInitErrorAsEnglishDescription( eError ) ) );
    }

    m_RenderModels = (vr::IVRRenderModels*)vr::VR_GetGenericInterface( vr::IVRRenderModels_Version, &eError );
    if( !m_RenderModels )
    {
        throw GenericException( "Unable to get render model interface: " + std::string( vr::VR_GetVRInitErrorAsEnglishDescription( eError ) ) );
    }

    if( vr::VRCompositor() == nullptr )
    {
        throw GenericException( "Failed to initialise VR compositor" );
    }

    m_HMD->GetRecommendedRenderTargetSize( &m_HMDRenderWidth, &m_HMDRenderHeight );
    
    _SetupStaticVRMatrices();
}

//-----------------------------------------------------------------------

void VRBackend::Shutdown()
{
    if( m_HMD )
    {
        vr::VR_Shutdown();
        m_HMD = nullptr;
        m_RenderModels = nullptr;
    }
}

//-----------------------------------------------------------------------

void VRBackend::SetGraphicsDevice(GraphicsDevice* graphicsDevice)
{
    m_GraphicsDevice = graphicsDevice;
}

//-----------------------------------------------------------------------

std::string VRBackend::GetHMDDriverName()
{
    return _GetVRDeviceStr( m_HMD, vr::k_unTrackedDeviceIndex_Hmd, vr::Prop_TrackingSystemName_String );
}

//-----------------------------------------------------------------------

std::string VRBackend::GetHMDName()
{
    return _GetVRDeviceStr( m_HMD, vr::k_unTrackedDeviceIndex_Hmd, vr::Prop_SerialNumber_String );
}

//-----------------------------------------------------------------------

size_t VRBackend::GetHMDRenderWidth() const
{
    return m_HMDRenderWidth;
}

//-----------------------------------------------------------------------

size_t VRBackend::GetHMDRenderHeight() const
{
    return m_HMDRenderHeight;
}

//-----------------------------------------------------------------------

void VRBackend::WaitForHMD()
{
    if( m_GraphicsDevice != nullptr )
    {
        vr::VRCompositor()->PostPresentHandoff();

        m_GraphicsDevice->SaveCurrentDepthBufferWritingState();

        m_GraphicsDevice->EnableDepthBufferWriting(); // Enable depth buffer writing, because otherwise it will not be cleared
        m_GraphicsDevice->ClearBuffers( GraphicsDevice::e_BufferTypeColor | GraphicsDevice::e_BufferTypeDepth );

        m_GraphicsDevice->Flush();
        m_GraphicsDevice->WaitTillFinished();

        m_GraphicsDevice->RestorePreviousDepthBufferWritingState();
    }
    else
    {
        MsgLogger::LogError( "Function SetGraphicsDevice() need to be called before calling WaitForHMD()" );
    }
}

//-----------------------------------------------------------------------

vr::EVRCompositorError VRBackend::SubmitAFrameToHDM(vr::EVREye eye, const vr::Texture_t* texture)
{
    const vr::VRTextureBounds_t* bounds = nullptr; 
    vr::EVRSubmitFlags submitFlags = vr::Submit_Default;

    vr::EVRCompositorError errorCode = vr::VRCompositor()->Submit( eye, texture, bounds, submitFlags );
    if( errorCode != vr::VRCompositorError_None )
    {
        std::string errMsg = ( "VR compositor returned an error \"" + _CompositorErrorCodeToString( errorCode ) + "\" while submitting a frame for the " );
        errMsg += ( eye == vr::Eye_Left ? "left" : "right" );
        errMsg += " eye";
        
        MsgLogger::LogError( errMsg );
    }

    return errorCode;
}

//-----------------------------------------------------------------------

void VRBackend::PollForVRDevicesPoses()
{
    vr::EVRCompositorError errorCode = vr::VRCompositor()->WaitGetPoses( m_TrackedDevicesPoses, vr::k_unMaxTrackedDeviceCount, nullptr, 0 );

    if( errorCode != vr::VRCompositorError_None )
    {
        std::string errMsg = ( "VR compositor returned an error \"" + _CompositorErrorCodeToString( errorCode ) + "\" while polling for poses" );        
        MsgLogger::LogError( errMsg );
    }

    if( m_TrackedDevicesPoses[vr::k_unTrackedDeviceIndex_Hmd].bPoseIsValid )
    {
        m_VRDevicesMatrices[e_VRDeviceMatrix_HMDOverallPose] = glm::inverse( _OpenVRMat34ToGLMMat44( m_TrackedDevicesPoses[vr::k_unTrackedDeviceIndex_Hmd].mDeviceToAbsoluteTracking ) );
    }
}

//-----------------------------------------------------------------------

const glm::mat4& VRBackend::GetVRDeviceMatrix(VRDeviceMatrix matrixID) const
{
    return m_VRDevicesMatrices[matrixID];
}

//-----------------------------------------------------------------------

std::string VRBackend::_GetVRDeviceStr(vr::IVRSystem* hmd, vr::TrackedDeviceIndex_t deviceIndex, vr::TrackedDeviceProperty property, vr::TrackedPropertyError* outError)
{
    std::string retVal = "";

    const uint32_t propertiesStrLen = hmd->GetStringTrackedDeviceProperty( deviceIndex, property, NULL, 0, outError );
    if( propertiesStrLen > 0 )
    {
        std::unique_ptr<char[]> strBuffer( new char[propertiesStrLen] );
        
        if( hmd->GetStringTrackedDeviceProperty( deviceIndex, property, strBuffer.get(), propertiesStrLen, outError ) == propertiesStrLen )
        {
            retVal = strBuffer.get();
        }
        else if ( outError == nullptr )
        {
            throw GenericException( "Failed to get string property of a VR device: " );
        }
    }

    return retVal;
}

//-----------------------------------------------------------------------

std::string VRBackend::_CompositorErrorCodeToString(vr::EVRCompositorError error) const
{
    std::string retVal = "";

    switch( error )
    {
        case vr::VRCompositorError_None:                         retVal = "None"; break;
        case vr::VRCompositorError_RequestFailed:                retVal = "Request Failed"; break;
        case vr::VRCompositorError_IncompatibleVersion:          retVal = "Incompatible Version"; break;
        case vr::VRCompositorError_DoNotHaveFocus:               retVal = "Do Not Have Focus"; break;
        case vr::VRCompositorError_InvalidTexture:               retVal = "Invalid Texture"; break;
        case vr::VRCompositorError_IsNotSceneApplication:        retVal = "Is Not Scene Application"; break;
        case vr::VRCompositorError_TextureIsOnWrongDevice:       retVal = "Texture Is On Wrong Device"; break;
        case vr::VRCompositorError_TextureUsesUnsupportedFormat: retVal = "Texture Uses Unsupported Format"; break;
        case vr::VRCompositorError_SharedTexturesNotSupported:   retVal = "Shared Textures Not Supported"; break;
        case vr::VRCompositorError_IndexOutOfRange:              retVal = "Index Out Of Range"; break;
        case vr::VRCompositorError_AlreadySubmitted:             retVal = "Already Submitted"; break;
        default:                                                 retVal = "Unkown"; break;
    }

    return retVal;
}

//-----------------------------------------------------------------------

void VRBackend::_SetupStaticVRMatrices()
{
    m_VRDevicesMatrices[e_VRDeviceMatrix_HMDLeftEyeProjection] = _OpenVRMat44ToGLMMat44( m_HMD->GetProjectionMatrix( vr::Eye_Left, 
                                                                                                                     EngineDefaults::c_NearClippingPlane, 
                                                                                                                     EngineDefaults::c_FarClippingPlane ) );
    m_VRDevicesMatrices[e_VRDeviceMatrix_HMDRightEyeProjection] = _OpenVRMat44ToGLMMat44( m_HMD->GetProjectionMatrix( vr::Eye_Right, 
                                                                                                                      EngineDefaults::c_NearClippingPlane, 
                                                                                                                      EngineDefaults::c_FarClippingPlane ) );
    m_VRDevicesMatrices[e_VRDeviceMatrix_HMDLeftEyePose] = _OpenVRMat34ToGLMMat44( m_HMD->GetEyeToHeadTransform( vr::Eye_Left ) );
    m_VRDevicesMatrices[e_VRDeviceMatrix_HMDRightEyePose] = _OpenVRMat34ToGLMMat44( m_HMD->GetEyeToHeadTransform( vr::Eye_Right ) );
}

//-----------------------------------------------------------------------

glm::mat4 VRBackend::_OpenVRMat34ToGLMMat44(const vr::HmdMatrix34_t mat) const
{
    return glm::mat4( 
        mat.m[0][0], mat.m[1][0], mat.m[2][0], 0.0,
        mat.m[0][1], mat.m[1][1], mat.m[2][1], 0.0,
        mat.m[0][2], mat.m[1][2], mat.m[2][2], 0.0,
        mat.m[0][3], mat.m[1][3], mat.m[2][3], 1.0f
    );
}

//-----------------------------------------------------------------------

glm::mat4 VRBackend::_OpenVRMat44ToGLMMat44(const vr::HmdMatrix44_t mat) const
{
    return glm::mat4(
        mat.m[0][0], mat.m[1][0], mat.m[2][0], mat.m[3][0],
        mat.m[0][1], mat.m[1][1], mat.m[2][1], mat.m[3][1],
        mat.m[0][2], mat.m[1][2], mat.m[2][2], mat.m[3][2],
        mat.m[0][3], mat.m[1][3], mat.m[2][3], mat.m[3][3]
    );
}

//-----------------------------------------------------------------------