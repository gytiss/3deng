#ifndef LIGHTINFLUENCEMESHES_H_INCLUDED
#define LIGHTINFLUENCEMESHES_H_INCLUDED

#include <GraphicsDevice/VAO.h>
#include <CommonDefinitions.h>
#include <EngineTypes.h>
#include <glm/glm.hpp>

class ShaderProgPointLightUberShader;
class ShaderProgDirectionalLightUberShader;
class ShaderProgSpotLightUberShader;
class ShaderProgLightingStencilPass;
class ShaderProgDebugObjects;

class RenderingHelperMesh
{
public:
    void Render();

protected:
    struct RenderingHelperMeshVertex
    {
        float position[3];
    };

    RenderingHelperMesh(const EngineTypes::VertexIndexType* indexBuffer,
                        size_t indexBufferSize,
                        const RenderingHelperMeshVertex* vertexBuffer,
                        size_t vertexBufferSize);

    // Used to prevent copying
    RenderingHelperMesh&  operator = (const RenderingHelperMesh& other);
    RenderingHelperMesh(const RenderingHelperMesh& other);

    const EngineTypes::VertexIndexType* m_IndexBuffer;
    const RenderingHelperMeshVertex* m_VertexBuffer;
    const size_t m_IndexBufferSize;
    const size_t m_VertexBufferSize;
    VAO m_VAO;
};

//-----------------------------------------------------------------------

class FullScreenQuadMesh : public RenderingHelperMesh
{
public:
    FullScreenQuadMesh();

private:
    static const RenderingHelperMeshVertex m_VerticexBuffer[];
    static const EngineTypes::VertexIndexType m_IndexBuffer[];
};

//-----------------------------------------------------------------------

class PointLightInfluenceMesh : public RenderingHelperMesh
{
public:
    PointLightInfluenceMesh();

    void Render(const glm::vec3& positionInWorldSpace,
                const float meshScale,
                const glm::mat4& viewMatrix,
                const glm::mat4& projectionMatrix,
                ShaderProgPointLightUberShader& shaderProg);

    void RenderStencilPass(const glm::vec3& positionInWorldSpace,
                           const float meshScale,
                           const glm::mat4& viewMatrix,
                           const glm::mat4& projectionMatrix,
                           ShaderProgLightingStencilPass& shaderProg);


private:
    static const RenderingHelperMeshVertex m_VerticexBuffer[];
    static const EngineTypes::VertexIndexType m_IndexBuffer[];
};

//-----------------------------------------------------------------------

class DirectionalLightInfluenceMesh : public FullScreenQuadMesh
{
public:
    DirectionalLightInfluenceMesh();

    void Render(ShaderProgDirectionalLightUberShader& shaderProg);

private:
    static const RenderingHelperMeshVertex m_VerticexBuffer[];
    static const EngineTypes::VertexIndexType m_IndexBuffer[];
};

//-----------------------------------------------------------------------

class SpotLightInfluenceMesh : public RenderingHelperMesh
{
public:
    SpotLightInfluenceMesh();

    void Render(const glm::vec3& positionInWorldSpace,
                const glm::vec3& directionInWorldSpace,
                const float meshScale,
                const float cutoffAngleInDegrees,
                const glm::mat4& viewMatrix,
                const glm::mat4& projectionMatrix,
                ShaderProgSpotLightUberShader& shaderProg);

    void RenderStencilPass(const glm::vec3& positionInWorldSpace,
                           const glm::vec3& directionInWorldSpace,
                           const float meshScale,
                           const float cutoffAngleInDegrees,
                           const glm::mat4& viewMatrix,
                           const glm::mat4& projectionMatrix,
                           ShaderProgLightingStencilPass& shaderProg);

    void RenderDebugData(const glm::vec3& positionInWorldSpace,
                const glm::vec3& directionInWorldSpace,
                const float meshScale,
                const float cutoffAngleInDegrees,
                const glm::mat4& viewMatrix,
                const glm::mat4& projectionMatrix,
                ShaderProgDebugObjects& shaderProg);

private:
    const glm::mat4 _GenerateConesMVMatrix(const glm::vec3& lightPosInWorldSpace,
                                           const glm::vec3& lightDirectionInWorldSpace,
                                           const float lightRadius,
                                           const float cutoffAngleInDegrees,
                                           const glm::mat4& viewMatrix) const;

    static const RenderingHelperMeshVertex m_VerticexBuffer[];
    static const EngineTypes::VertexIndexType m_IndexBuffer[];
};

//-----------------------------------------------------------------------

#endif // LIGHTINFLUENCEMESHES_H_INCLUDED
