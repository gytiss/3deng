#ifndef COMPONENTFLOATVALUEVALIDATOR_H_INCLUDED
#define COMPONENTFLOATVALUEVALIDATOR_H_INCLUDED

#include <QtGui/QDoubleValidator>

class ComponentFloatValueValidator : public QDoubleValidator
{
    Q_OBJECT

public:
    ComponentFloatValueValidator(int maxDecimalPlacesInTheFinalValue,
                                 int maxDecimalPlacesInTheWorkInProgressValue);

    State validate(QString& input, int& pos) const override;

    void fixup(QString& input) const override;

    inline int MaxDecimalPlacedInTheFinalNumber() const
    {
        return m_MaxDecimalPLacedInTheFinalValue;
    }

private:
    const int m_MaxDecimalPLacedInTheFinalValue;
};

#endif // COMPONENTFLOATVALUEVALIDATOR_H_INCLUDED
