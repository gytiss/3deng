#include "Buffer.h"
#include "Context.h"
#include "OpenALIncludes.h"
#include "OpenALErrorChecking.h"
#include <Exceptions/GenericException.h>
#include <Resources/Audio/AudioData.h>

namespace AudioDevice
{

//-----------------------------------------------------------------------

Buffer::Buffer(Context& context)
: m_ALBuffer( 0 ),
  m_Context( context )
{
    m_Context.Bind();

    alGenBuffers( 1, &m_ALBuffer );
    CheckForALErrors( m_Context );
}

//-----------------------------------------------------------------------

Buffer::Buffer(Buffer&& other)
: m_ALBuffer( other.m_ALBuffer ),
  m_Context( other.m_Context )
{
    other.m_ALBuffer = 0;
}

//-----------------------------------------------------------------------

Buffer::Buffer(Context& context, unsigned int alBufferHandle)
: m_ALBuffer( 0 ),
  m_Context( context )
{
    m_Context.Bind();

    ALboolean retVal = alIsBuffer( alBufferHandle );
    if( retVal == AL_TRUE )
    {
        m_ALBuffer = alBufferHandle;
    }
    else
    {
        throw GenericException( "Tried to create a buffer object with an invalid OpenAL buffer handle" );
    }
}

//-----------------------------------------------------------------------

Buffer::~Buffer()
{
    if( m_ALBuffer != 0 )
    {
        m_Context.Bind();

        alDeleteBuffers( 1, &m_ALBuffer );
        CheckForALErrors( m_Context );
    }
}

//-----------------------------------------------------------------------

void Buffer::Feed(const AudioData& audioData)
{
    if( m_ALBuffer != 0 )
    {
        ALenum format = AL_FORMAT_STEREO16;
        bool unsupportedSampleWidth = false;

        if( audioData.GetNumberOfChannels() == 2 )
        {
            if( audioData.GetSampleWidthInBytes() == 2 )
            {
                format = AL_FORMAT_STEREO16;
            }
            else if( audioData.GetSampleWidthInBytes() == 1 )
            {
                format = AL_FORMAT_STEREO8;
            }
            else
            {
                unsupportedSampleWidth = true;
            }
        }
        else if( audioData.GetNumberOfChannels() == 1 )
        {
            if( audioData.GetSampleWidthInBytes() == 2 )
            {
                format = AL_FORMAT_MONO16;
            }
            else if( audioData.GetSampleWidthInBytes() == 1 )
            {
                format = AL_FORMAT_MONO8;
            }
            else
            {
                unsupportedSampleWidth = true;
            }
        }
        else
        {
            throw GenericException( "Trying to load audio data with unsupported number of channels to an OpenAL buffer" );
        }

        if( unsupportedSampleWidth )
        {
            throw GenericException( "Trying to load audio data with unsupported sample width to an OpenAL buffer" );
        }

        m_Context.Bind();

        alBufferData( m_ALBuffer,
                      format,
                      &audioData.GetRawSampleData()[0],
                      static_cast<ALsizei>( audioData.GetRawSampleData().size() ),
                      audioData.GetSampleRate() );
        CheckForALErrors( m_Context );
    }
}

//-----------------------------------------------------------------------

}
