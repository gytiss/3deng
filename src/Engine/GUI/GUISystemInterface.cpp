#include "GUISystemInterface.h"
#include <SystemInterface.h>
#include <MsgLogger.h>

//-----------------------------------------------------------------------

GUISystemInterface::GUISystemInterface(::SystemInterface& systemInterface)
: m_SystemInterface( systemInterface )
{

}

//-----------------------------------------------------------------------

GUISystemInterface::~GUISystemInterface()
{

}

//-----------------------------------------------------------------------

float GUISystemInterface::GetElapsedTime()
{
    // TODO submit a patch to libRocket github repository changing time value type from float to double
	return m_SystemInterface.GetTicksInSeconds();
}

//-----------------------------------------------------------------------

bool GUISystemInterface::LogMessage(Rocket::Core::Log::Type type,
                                    const Rocket::Core::String &message)
{
    bool retVal = true;

    switch( type )
    {
        case Rocket::Core::Log::LT_ALWAYS:
        {
            MsgLogger::LogEvent( "libRocket Always: " + std::string( message.CString() ) );
            break;
        }

        case Rocket::Core::Log::LT_ERROR:
        {
            retVal = false;
            MsgLogger::LogError( "libRocket Error: " + std::string( message.CString() ) );
            break;
        }

        case Rocket::Core::Log::LT_ASSERT:
        {
            retVal = false;
            MsgLogger::LogError( "libRocket Assert: " + std::string( message.CString() ) );
            break;
        }

        case Rocket::Core::Log::LT_WARNING:
        {
            MsgLogger::LogWarning( "libRocket Warning: " + std::string( message.CString() ) );
            break;
        }

        case Rocket::Core::Log::LT_INFO:
        {
            MsgLogger::LogEvent( "libRocket Info: " + std::string( message.CString() ) );
            break;
        }

        default:
        {
            retVal = false;
            MsgLogger::LogError( "Unknown libRocket message type" );
            break;
        }
    }

    return retVal;
}

//-----------------------------------------------------------------------
