#ifndef SHOOTINGTARGET_H_INCLUDED
#define SHOOTINGTARGET_H_INCLUDED

#include <Scene/GameObject.h>

class MeshManager;
class AnimationManager;
class MaterialManager;
class PhysicsManager;
class Animator;

class ShootingTarget : public GameObject, public GameObject::GameObjectLogicInterface
{
public:
    enum State
    {
        e_StateActive = 0,
        e_StateHit = 1
    };

    ShootingTarget(const std::string& name,
                   MeshManager& meshManager,
                   AnimationManager& animationManager,
                   MaterialManager& materialManager,
                   PhysicsManager& physicsManager);

    void SetState(State newState);

private:
    void _Initialize();
    void _FixedUpdate();
    void _Update();
    void _LastUpdate();

    State m_CurrentState;
    Animator* m_Animator;
    BoxCollider* m_ShootingTargetCollider;
};


#endif // SHOOTINGTARGET_H_INCLUDED
