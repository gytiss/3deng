#include "CapsuleCollider.h"
#include <cassert>

//-----------------------------------------------------------------------

CapsuleCollider::CapsuleCollider(GameObject& owningGameObject,
                                 float radius,
                                 float height,
                                 PhysicsManager& physicsManager)
: Collider( owningGameObject, e_ColliderTypeCapsule, "Capsule Collider", physicsManager ),
  m_Direction( e_DirectionY )
{
    _Initialize( _CreateCapsuleCollisionShape( m_Direction, radius, height ) );
}

//-----------------------------------------------------------------------

void CapsuleCollider::SetRadius(float radius)
{
    _ChangeCollisionShape( _CreateCapsuleCollisionShape( m_Direction, radius, GetHeight() ) );
}

//-----------------------------------------------------------------------

float CapsuleCollider::GetRadius() const
{
    return m_PhysicsManager.GetCapsuleCollisionShapeRadius( _GetCollisionShapeHandle() );
}

//-----------------------------------------------------------------------

void CapsuleCollider::SetHeight(float height)
{
    _ChangeCollisionShape( _CreateCapsuleCollisionShape( m_Direction, GetRadius(), height ) );
}

//-----------------------------------------------------------------------

float CapsuleCollider::GetHeight() const
{
    return m_PhysicsManager.GetCapsuleCollisionShapeHeight( _GetCollisionShapeHandle() );
}

//-----------------------------------------------------------------------

void CapsuleCollider::SetDirection(Direction dir)
{
    if( dir != m_Direction )
    {
        m_Direction = dir;
        _ChangeCollisionShape( _CreateCapsuleCollisionShape( m_Direction, GetRadius(), GetHeight() ) );
    }
}

//-----------------------------------------------------------------------

CapsuleCollider::Direction CapsuleCollider::GetDirection() const
{
    return m_Direction;
}

//-----------------------------------------------------------------------

PhysicsManager::CollisionShapeHandle CapsuleCollider::_CreateCapsuleCollisionShape(Direction dir,
                                                                                   float radius,
                                                                                   float height)
{
    PhysicsManager::CollisionShapeHandle retVal;

    if( dir == e_DirectionX )
    {
        retVal = m_PhysicsManager.CreateCapsuleXCollisionShape( radius, height );
    }
    else if( dir == e_DirectionY )
    {
        retVal = m_PhysicsManager.CreateCapsuleYCollisionShape( radius, height );
    }
    else if( dir == e_DirectionZ )
    {
        retVal = m_PhysicsManager.CreateCapsuleZCollisionShape( radius, height );
    }
    else
    {
        assert( 0 && "Not implemented direction" );
    }

    return retVal;
}

//-----------------------------------------------------------------------
