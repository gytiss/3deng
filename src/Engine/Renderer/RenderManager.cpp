#include "RenderManager.h"
#include "PointLight.h"
#include "SpotLight.h"
#include "DirectionalLight.h"
#include <OpenGLErrorChecking.h>
#include <GUI/GUIRenderInterface.h>
#include <GraphicsDevice/GraphicsDevice.h>
#include <GraphicsDevice/DepthTexture.h>
#include <GraphicsDevice/FBOColorAttachmentTexture.h>
#include <GraphicsDevice/VBO.h>
#include <GraphicsDevice/IBO.h>
#include <Exceptions/GenericException.h>
#ifdef VR_BACKEND_ENABLED
    #include <VR/VRBackend.h>
#endif

#include <cstdlib>

EngineTypes::SRGBColor RenderManager::c_DefaultBackgroundColor( 0.1921f, 0.3019f, 0.4745f ); // Sky blue color

//-----------------------------------------------------------------------

RenderManager::RenderManager(GraphicsDevice& graphicsDevice,
                             SystemInterface& systemInterface,
#ifdef VR_BACKEND_ENABLED
                             VRBackend& vrBackend,
#endif
                             FileManager& fileManager)
: m_SystemInterface( systemInterface ),
  m_DrawableSurfaceSize( m_SystemInterface.GetDrawableSurfaceSize() ),
  m_FordwardOrDefferedRenderList(),
  m_FordwardOrDefferedRenderLastRenderList(),
  m_FordwardOnlyRenderList(),
  m_FordwardOnlyRenderLastRenderList(),
  m_GUIRenderablesList(),
  m_GraphicsDevice( graphicsDevice ),
  m_DepthBuffer( m_GraphicsDevice, m_SystemInterface, DepthTexture::e_TypeIdxDepthTexture ),
  m_PostProcessingFBO( m_GraphicsDevice, m_SystemInterface ),
  m_GBuffer( m_GraphicsDevice, m_SystemInterface ),
#ifdef VR_BACKEND_ENABLED
  m_VRBackend( vrBackend ),
  m_VRHMDBuffer( m_VRBackend, m_GraphicsDevice, m_SystemInterface ),
  m_VRHMDEyeBeingRendered( VRHMDBuffer::Eye::Left ),
#endif
  m_LightManager( m_GraphicsDevice, m_PostProcessingFBO, m_SystemInterface, fileManager ),
  m_GBufferSkinnedShaderProg( fileManager ),
  m_GBufferStaticShaderProg( fileManager ),
  m_ToneMappingAndGammaCorrectionShaderProg( fileManager ),
  m_DebugObjectsShaderProg( fileManager ),
  m_GUIShaderProg( fileManager ),
  m_DepthDebugShaderProg( fileManager ),
  m_ForwardRenderingPointLightSkinnedShaderProg( fileManager ),
  m_ForwardRenderingPointLightStaticShaderProg( fileManager ),
  m_ForwardRenderingSpotLightSkinnedShaderProg( fileManager ),
  m_ForwardRenderingSpotLightStaticShaderProg( fileManager ),
  m_ForwardRenderingDirectionalLightSkinnedShaderProg( fileManager ),
  m_ForwardRenderingDirectionalLightStaticShaderProg( fileManager ),
  m_ForwardRenderingNoLightingSkinnedShaderProg( fileManager ),
  m_ForwardRenderingNoLightingStaticShaderProg( fileManager ),
  m_GenericTexturedShader( fileManager ),
  m_ProjectionType( e_ProjectionTypePerspective ),
  m_VPTransformations( {MathUtils::c_IdentityMatrix, MathUtils::c_IdentityMatrix} ),
  m_FrustumNearClippingPlaneDistance( 0.0f ),
  m_FrustumFarClippingPlaneDistance( 0.0f ),
  m_RenderingPath( e_RenderingPathForward ),
  m_GUIRenderInterface( nullptr ),
  m_FullScreenQuadMesh(),
  m_ScreenshotCapture( m_SystemInterface, m_GraphicsDevice, "screenshots" ),
  m_CustomGUIProjectionMatrix( MathUtils::c_IdentityMatrix ),
  m_ForwardLightColors(),
  m_ForwardLightPositionsInViewSpace(),
  m_ForwardLightDirectionsInViewSpace(),
  m_ForwardLightAmbientIntensities(),
  m_ForwardLightDiffuseIntensities(),
  m_ForwardLightRadiuses(),
  m_ForwardLightCutoffValues(),
  m_ForwardLightTypes(),
  m_BackgroundColor( c_DefaultBackgroundColor ),
  m_RenderInWireframeMode( false ),
  m_TakeADebugScreenshotOfDefferedDepthBuffers( false ),
  m_TakeADebugScreenshotOfDefferedStencilBuffers( false )
{
    _DrawableSurfaceSizeChaged( m_DrawableSurfaceSize, this );

    _SetupPostProcessingFBO( m_DepthBuffer );

    m_GBuffer.Init( m_DepthBuffer );

    m_GBufferSkinnedShaderProg.LoadCompileAndLink();
    m_GBufferStaticShaderProg.LoadCompileAndLink();
    m_ToneMappingAndGammaCorrectionShaderProg.LoadCompileAndLink();
    m_DebugObjectsShaderProg.LoadCompileAndLink();

    m_ForwardRenderingPointLightSkinnedShaderProg.LoadCompileAndLink();
    m_ForwardRenderingPointLightStaticShaderProg.LoadCompileAndLink();
    m_ForwardRenderingSpotLightSkinnedShaderProg.LoadCompileAndLink();
    m_ForwardRenderingSpotLightStaticShaderProg.LoadCompileAndLink();
    m_ForwardRenderingDirectionalLightSkinnedShaderProg.LoadCompileAndLink();
    m_ForwardRenderingDirectionalLightStaticShaderProg.LoadCompileAndLink();

    m_ForwardRenderingNoLightingSkinnedShaderProg.LoadCompileAndLink();
    m_ForwardRenderingNoLightingStaticShaderProg.LoadCompileAndLink();
    m_GenericTexturedShader.LoadCompileAndLink();
    m_GUIShaderProg.LoadCompileAndLink();
    m_DepthDebugShaderProg.LoadCompileAndLink();

    m_SystemInterface.RegisterDrawableSurfaceSizeChangedCallback( _DrawableSurfaceSizeChaged, this );
}

//-----------------------------------------------------------------------

RenderManager::~RenderManager()
{
    m_SystemInterface.DeregisterDrawableSurfaceSizeChangedCallback( _DrawableSurfaceSizeChaged, this );
}

//-----------------------------------------------------------------------

uint32_t RenderManager::DoGameObjectPicking(std::vector<PickingPair>& objectsForPicking,
                                            size_t pickingPosX,
                                            size_t pickingPosY)
{
    uint32_t retVal = 0;

    m_GraphicsDevice.SaveCurrentClearColorValue();
    m_GraphicsDevice.SetClearColorValue( 0.0f, 0.0f, 0.0f, 0.0f );

    if( ( pickingPosX <= m_DrawableSurfaceSize.widthInPixels ) && ( pickingPosY <= m_DrawableSurfaceSize.heightInPixels ) )
    {
        // Mouse coordinates origin is at the top left corner, but we need coordinates with the origin at the bottom left corner, hence the remapping
        pickingPosY = ( m_DrawableSurfaceSize.heightInPixels - pickingPosY );

        m_DebugObjectsShaderProg.Use();
        m_DebugObjectsShaderProg.SetUniformUseVertexColors( false );

        const glm::mat4 viewProjctionMatrix = ( m_VPTransformations.projectionMatrix * m_VPTransformations.viewMatrix );

        m_PostProcessingFBO.BindColorAttachmentsForDrawing( FBO::e_ColorAttachment1 );
        m_GraphicsDevice.ClearBuffers( GraphicsDevice::e_BufferTypeColor | GraphicsDevice::e_BufferTypeDepth );

        for(PickingPair& pickingPair : objectsForPicking)
        {
            const uint32_t objectID = pickingPair.first;
            RenderableObject& obj = pickingPair.second;

            const uint32_t pickingColorRed = ( objectID & 0xFF );
            const uint32_t pickingColorGreen = ( ( objectID >> 8 ) & 0xFF );
            const uint32_t pickingColorBlue = ( ( objectID >> 16 ) & 0xFF );

            const glm::vec3 pickingColor = glm::vec3( pickingColorRed / 255.0f,
                                                      pickingColorGreen / 255.0f,
                                                      pickingColorBlue / 255.0f );

            m_DebugObjectsShaderProg.SetUniformColor( pickingColor );

            const glm::mat4& objModelMatrix = obj.GetWorldTransformMatrix();

            m_DebugObjectsShaderProg.SetUniformModelViewProjectionMatrix( viewProjctionMatrix * objModelMatrix );
            obj.BindForRendering();

            _RenderPrimitives( obj );
        }

        m_PostProcessingFBO.BindColorAttachmentForReading( FBO::e_ColorAttachmentIdx1 );
        FBO::BindDefaultFBOForDrawing();

        uint8_t pickedPixel[4];
        m_GraphicsDevice.ReadPixelsFromFrameBuffer( pickingPosX,
                                                    pickingPosY,
                                                    1,
                                                    1,
                                                    pickedPixel );

        retVal = ( pickedPixel[0] + ( pickedPixel[1] * 256 ) + ( pickedPixel[2] * 256 * 256 ) );
    }
    else
    {
        assert( false && "One or both of the picking coordinates is/are larger than the screen" );
    }

    m_GraphicsDevice.RestorePreviousClearColorValue();

    return retVal;
}

void RenderManager::Render()
{
    // Orthographic projection is supported only in forward rendering path
    if( m_ProjectionType == e_ProjectionTypeOrthograpchic )
    {
        m_RenderingPath = e_RenderingPathForward;
    }

    m_GraphicsDevice.SaveCurrentPolygonMode();

    if( m_RenderInWireframeMode )
    {
        m_GraphicsDevice.SetPolygonMode( GraphicsDevice::e_PolygonModeLine );
    }

#ifdef VR_BACKEND_ENABLED
    for(size_t i = 0; i < 2; i++)
    {
        m_VRHMDEyeBeingRendered = ( ( i == 0 ) ? VRHMDBuffer::Eye::Left : VRHMDBuffer::Eye::Right );

        if( m_VRHMDEyeBeingRendered == VRHMDBuffer::Eye::Left )
        {
            m_VPTransformations.viewMatrix = ( m_VRBackend.GetVRDeviceMatrix( VRBackend::e_VRDeviceMatrix_HMDLeftEyePose ) *
                                               m_VRBackend.GetVRDeviceMatrix( VRBackend::e_VRDeviceMatrix_HMDOverallPose ) );
            m_VPTransformations.projectionMatrix = m_VRBackend.GetVRDeviceMatrix( VRBackend::e_VRDeviceMatrix_HMDLeftEyeProjection );
        }
        else
        {
            m_VPTransformations.viewMatrix = ( m_VRBackend.GetVRDeviceMatrix( VRBackend::e_VRDeviceMatrix_HMDRightEyePose ) *
                                               m_VRBackend.GetVRDeviceMatrix( VRBackend::e_VRDeviceMatrix_HMDOverallPose ) );
            m_VPTransformations.projectionMatrix = m_VRBackend.GetVRDeviceMatrix( VRBackend::e_VRDeviceMatrix_HMDRightEyeProjection );
        }
#endif
        if( m_RenderingPath == e_RenderingPathForward )
        {
            m_GraphicsDevice.SetClearColorValue( m_BackgroundColor.LinearValue().x,
                                                 m_BackgroundColor.LinearValue().y,
                                                 m_BackgroundColor.LinearValue().z,
                                                 1.0f );
            _ForwardPath();
        }
        else if( m_RenderingPath == e_RenderingPathDeferred )
        {
            // Clear color for the deferred path needs to be black in order for the light calculations to work correctly.
            // Default background in the deferred path will be rendered separately in a dedicated pass.
            m_GraphicsDevice.SetClearColorValue( 0.0f, 0.0f, 0.0f, 1.0f );

            _DeferredPath();
        }
#ifdef VR_BACKEND_ENABLED
    }
#endif


    // GUI needs to be rendered the last, because it needs neither
    // gamma correction nor tone mapping and it must be on the top
    _RenderGUI();

    m_GraphicsDevice.RestorePreviousPolygonMode();

    m_FordwardOrDefferedRenderList.clear();
    m_FordwardOrDefferedRenderLastRenderList.clear();
    m_FordwardOnlyRenderList.clear();
    m_FordwardOnlyRenderLastRenderList.clear();
    m_GUIRenderablesList.clear();
}

//-----------------------------------------------------------------------

void RenderManager::AddRenderable(const RenderableObject& r, bool renderLast)
{
    const VBO::VertexAttrMask vertexAttrs = r.GetVertexAtributes();

    bool forwardOnly = false;
    ShaderType shaderType = e_ShaderTypeInvalid;

    if( vertexAttrs.ConsistOf( VBO::e_VertexAttrPosition, VBO::e_VertexAttrColor ) )
    {
        shaderType = e_ShaderTypeColouredVertices;
        forwardOnly = true;
    }
    else if( ( vertexAttrs.ConsistOf( VBO::e_VertexAttrPosition ) ) && r.HasDiffuseColor() )
    {
        shaderType = e_ShaderTypeColouredMesh;
        forwardOnly = true;
    }
    else if( vertexAttrs.ConsistOf( VBO::e_VertexAttrPosition,
                                    VBO::e_VertexAttrNormal,
                                    VBO::e_VertexAttrBiTangent,
                                    VBO::e_VertexAttrTangent,
                                    VBO::e_VertexAttrTextureCoords,
                                    VBO::e_VertexAttrBoneWeights,
                                    VBO::e_VertexAttrBoneIndices ) )
    {
        shaderType = e_ShaderTypeSkinned;
    }
    else if( vertexAttrs.ConsistOf( VBO::e_VertexAttrPosition,
                                    VBO::e_VertexAttrNormal,
                                    VBO::e_VertexAttrBiTangent,
                                    VBO::e_VertexAttrTangent,
                                    VBO::e_VertexAttrTextureCoords ) )
    {
        shaderType = e_ShaderTypeStatic;
    }

    if( shaderType != e_ShaderTypeInvalid )
    {
        if( forwardOnly )
        {
            if( renderLast )
            {
                m_FordwardOnlyRenderLastRenderList.push_back( RenderUnit( shaderType, r ) );
            }
            else
            {
                m_FordwardOnlyRenderList.push_back( RenderUnit( shaderType, r ) );
            }
        }
        else
        {
            if( renderLast )
            {
                m_FordwardOrDefferedRenderLastRenderList.push_back( RenderUnit( shaderType, r ) );
            }
            else
            {
                m_FordwardOrDefferedRenderList.push_back( RenderUnit( shaderType, r ) );
            }
        }
    }
    else
    {
        throw GenericException( "Could not determine shader type for a renderable" );
    }
}

//-----------------------------------------------------------------------

void RenderManager::AddGUIRenderables(const RenderableObject& r)
{
    ShaderType shaderType = e_ShaderTypeInvalid;

    const VBO::VertexAttrMask vertexAttrs = r.GetVertexAtributes();

    if( vertexAttrs.Contains( VBO::e_VertexAttrPosition, VBO::e_VertexAttrTextureCoords ) && r.HasDiffuseMap() )
    {
        shaderType = e_ShaderTypeTexturedMesh;
    }
    else if( ( vertexAttrs.Contains( VBO::e_VertexAttrPosition, VBO::e_VertexAttrTextureCoords ) ||
               vertexAttrs.Contains( VBO::e_VertexAttrPosition ) )
             && r.HasDiffuseColor() )
    {
        shaderType = e_ShaderTypeColouredMesh;
    }

    if( shaderType != e_ShaderTypeInvalid )
    {
        m_GUIRenderablesList.push_back( RenderUnit( shaderType, r ) );
    }
    else
    {
        throw GenericException( "Could not determine shader type for a GUI renderable" );
    }
}

//-----------------------------------------------------------------------

void RenderManager::SetViewTransform(const glm::mat4& viewTransform)
{
    m_VPTransformations.viewMatrix = viewTransform;
}

//-----------------------------------------------------------------------

void RenderManager::SetProjectionTransform(ProjectionType projectionType, const glm::mat4& projectionTransform)
{
    m_ProjectionType = projectionType;
    m_VPTransformations.projectionMatrix = projectionTransform;
}

//-----------------------------------------------------------------------

void RenderManager::SetFrustumNearAndFarClippingPlaneDistances(float nearDistance, float farDistance)
{
    m_FrustumNearClippingPlaneDistance = nearDistance;
    m_FrustumFarClippingPlaneDistance = farDistance;
}

//-----------------------------------------------------------------------

void RenderManager::_SetupPostProcessingFBO(DepthTexture& depthBuffer)
{
    FBOColorAttachmentTexture* postProcessDrawTexture1 = new FBOColorAttachmentTexture( m_GraphicsDevice,
                                                                                        m_DrawableSurfaceSize.widthInPixels,
                                                                                        m_DrawableSurfaceSize.heightInPixels,
                                                                                        FBOColorAttachmentTexture::e_TypeIdxPostProcessTextureMap,
#if defined(OPENGL_ES) && !defined(OS_GENERIC_UNIX)
                                                                                        FBOColorAttachmentTexture::e_DataFormatRGB10Alpha2 );
#else
                                                                                        FBOColorAttachmentTexture::e_DataFormatRGB16F );
#endif

    FBOColorAttachmentTexture* postProcessDrawTexture2 = new FBOColorAttachmentTexture( m_GraphicsDevice,
                                                                                        m_DrawableSurfaceSize.widthInPixels,
                                                                                        m_DrawableSurfaceSize.heightInPixels,
                                                                                        FBOColorAttachmentTexture::e_TypeIdxPostProcessTextureMap,
#if defined(OPENGL_ES) && !defined(OS_GENERIC_UNIX)
                                                                                        FBOColorAttachmentTexture::e_DataFormatRGB10Alpha2 );
#else
                                                                                        FBOColorAttachmentTexture::e_DataFormatRGB16F );
#endif

    m_PostProcessingFBO.AttachColorTexture( postProcessDrawTexture1 );
    m_PostProcessingFBO.AttachColorTexture( postProcessDrawTexture2 );
    m_PostProcessingFBO.AttachDepthTexture( &depthBuffer );
    FBO::BindDefaultFBOForDrawing();
}

//-----------------------------------------------------------------------

void RenderManager::_ForwardPath()
{
    m_GraphicsDevice.SaveCurrentDepthBufferWritingState();
    m_GraphicsDevice.EnableDepthBufferWriting();

    FBO::BindDefaultFBOForDrawing();
    m_GraphicsDevice.ClearBuffers( GraphicsDevice::e_BufferTypeColor | GraphicsDevice::e_BufferTypeDepth );

    m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionDepthTesting );
    m_GraphicsDevice.OptionEnable( GraphicsDevice::e_OptionDepthTesting );

#ifndef OPENGL_ES
    m_PostProcessingFBO.BindColorAttachmentsForDrawing( FBO::e_ColorAttachment0 );
    m_GraphicsDevice.ClearBuffers( GraphicsDevice::e_BufferTypeColor | GraphicsDevice::e_BufferTypeDepth );
#endif

    m_ForwardLightColors.clear();
    m_ForwardLightPositionsInViewSpace.clear();
    m_ForwardLightDirectionsInViewSpace.clear();
    m_ForwardLightAmbientIntensities.clear();
    m_ForwardLightDiffuseIntensities.clear();
    m_ForwardLightRadiuses.clear();
    m_ForwardLightCutoffValues.clear();
    m_ForwardLightTypes.clear();

    const LightManager::PointLightList_t& pointLights = m_LightManager.GetPointLights();
    const LightManager::SpotLightList_t& spotLight = m_LightManager.GetSpotLights();
    const DirectionalLight* const dirLight = m_LightManager.GetMainDirectionalLight();

    size_t lightCount = 0;

    /*------------------ Directional Light ------------------*/
    if( dirLight->Enabled() )
    {
        const glm::vec3 dirInViewSpace = glm::vec3( m_VPTransformations.viewMatrix * glm::vec4( dirLight->GetDirectionInWorldSpace(), 0.0f ) );

        m_ForwardLightColors.push_back( dirLight->GetColor() );
        m_ForwardLightPositionsInViewSpace.push_back( glm::vec3( 0.0f, 0.0f, 0.0f ) ); // Stub value
        m_ForwardLightDirectionsInViewSpace.push_back( dirInViewSpace );
        m_ForwardLightAmbientIntensities.push_back( dirLight->GetAmbientIntensity() );
        m_ForwardLightDiffuseIntensities.push_back( dirLight->GetDiffuseIntensity() );
        m_ForwardLightRadiuses.push_back( 0.0f ); // Stub value
        m_ForwardLightCutoffValues.push_back( 0.0f ); // Stub value
        m_ForwardLightTypes.push_back( e_ForwardLigthTypeDirectional );

        lightCount++;
    }
    /*-------------------------------------------------------*/

    for(LightManager::SpotLightList_t::const_iterator it = spotLight.begin(); it != spotLight.end(); ++it)
    {
        const SpotLight* spl = *it;
        if( spl && spl->Enabled() )
        {
            const glm::vec3 posInViewSpace = glm::vec3( m_VPTransformations.viewMatrix * glm::vec4( spl->GetPositionInWorldSpace(), 1.0f ) );
            const glm::vec3 dirInViewSpace = glm::vec3( m_VPTransformations.viewMatrix * glm::vec4( spl->GetDirectionInWorldSpace(), 0.0f ) );

            m_ForwardLightColors.push_back( spl->GetColor() );
            m_ForwardLightPositionsInViewSpace.push_back( posInViewSpace );
            m_ForwardLightDirectionsInViewSpace.push_back( dirInViewSpace );
            m_ForwardLightAmbientIntensities.push_back( spl->GetAmbientIntensity() );
            m_ForwardLightDiffuseIntensities.push_back( spl->GetDiffuseIntensity() );
            m_ForwardLightRadiuses.push_back( spl->GetRadius() );
            m_ForwardLightCutoffValues.push_back( cos( MathUtils::DegreesToRadians( spl->GetCutOffValueAngleInDegrees() ) ) );
            m_ForwardLightTypes.push_back( e_ForwardLigthTypeSpot );

            lightCount++;
        }
    }

    for(LightManager::PointLightList_t::const_iterator it = pointLights.begin(); it != pointLights.end(); ++it)
    {
        const PointLight* pl = *it;
        if( pl && pl->Enabled() )
        {
            const glm::vec3 posInViewSpace = glm::vec3( m_VPTransformations.viewMatrix * glm::vec4( pl->GetPositionInWorldSpace(), 1.0f ) );

            m_ForwardLightColors.push_back( pl->GetColor() );
            m_ForwardLightPositionsInViewSpace.push_back( posInViewSpace );
            m_ForwardLightDirectionsInViewSpace.push_back( glm::vec3( 0.0f, 0.0f, 0.0f ) ); // Stub value
            m_ForwardLightAmbientIntensities.push_back( pl->GetAmbientIntensity() );
            m_ForwardLightDiffuseIntensities.push_back( pl->GetDiffuseIntensity() );
            m_ForwardLightRadiuses.push_back( pl->GetRadius() );
            m_ForwardLightCutoffValues.push_back( 0.0f ); // Stub value
            m_ForwardLightTypes.push_back( e_ForwardLigthTypePoint );

            lightCount++;
        }
    }

//    m_ForwardRenderingSkinnedShaderProg.Use();
//    m_ForwardRenderingSkinnedShaderProg.SetUniformLightColorArray( m_ForwardLightColors.size(), &m_ForwardLightColors[0] );
//    m_ForwardRenderingSkinnedShaderProg.SetUniformLightPositionInViewSpaceArray( m_ForwardLightPositionsInViewSpace.size(), &m_ForwardLightPositionsInViewSpace[0] );
//    m_ForwardRenderingSkinnedShaderProg.SetUniformLightDirectionInViewSpaceArray( m_ForwardLightDirectionsInViewSpace.size(), &m_ForwardLightDirectionsInViewSpace[0] );
//    m_ForwardRenderingSkinnedShaderProg.SetUniformLightAmbientIntensityArray( m_ForwardLightAmbientIntensities.size(), &m_ForwardLightAmbientIntensities[0] );
//    m_ForwardRenderingSkinnedShaderProg.SetUniformLightDiffuseIntensityArray( m_ForwardLightDiffuseIntensities.size(), &m_ForwardLightDiffuseIntensities[0] );
//    m_ForwardRenderingSkinnedShaderProg.SetUniformLightRadiusArray( m_ForwardLightRadiuses.size(), &m_ForwardLightRadiuses[0] );
//    m_ForwardRenderingSkinnedShaderProg.SetUniformLightCutoffValueArray( m_ForwardLightCutoffValues.size(), &m_ForwardLightCutoffValues[0] );
//    m_ForwardRenderingSkinnedShaderProg.SetUniformLightTypeArray( m_ForwardLightTypes.size(), &m_ForwardLightTypes[0] );
//    m_ForwardRenderingSkinnedShaderProg.SetUniformLightCount( lightCount );
//
//    m_ForwardRenderingStaticShaderProg.Use();
//    m_ForwardRenderingStaticShaderProg.SetUniformLightColorArray( m_ForwardLightColors.size(), &m_ForwardLightColors[0] );
//    m_ForwardRenderingStaticShaderProg.SetUniformLightPositionInViewSpaceArray( m_ForwardLightPositionsInViewSpace.size(), &m_ForwardLightPositionsInViewSpace[0] );
//    m_ForwardRenderingStaticShaderProg.SetUniformLightDirectionInViewSpaceArray( m_ForwardLightDirectionsInViewSpace.size(), &m_ForwardLightDirectionsInViewSpace[0] );
//    m_ForwardRenderingStaticShaderProg.SetUniformLightAmbientIntensityArray( m_ForwardLightAmbientIntensities.size(), &m_ForwardLightAmbientIntensities[0] );
//    m_ForwardRenderingStaticShaderProg.SetUniformLightDiffuseIntensityArray( m_ForwardLightDiffuseIntensities.size(), &m_ForwardLightDiffuseIntensities[0] );
//    m_ForwardRenderingStaticShaderProg.SetUniformLightRadiusArray( m_ForwardLightRadiuses.size(), &m_ForwardLightRadiuses[0] );
//    m_ForwardRenderingStaticShaderProg.SetUniformLightCutoffValueArray( m_ForwardLightCutoffValues.size(), &m_ForwardLightCutoffValues[0] );
//    m_ForwardRenderingStaticShaderProg.SetUniformLightTypeArray( m_ForwardLightTypes.size(), &m_ForwardLightTypes[0] );
//    m_ForwardRenderingStaticShaderProg.SetUniformLightCount( lightCount );

    if( pointLights.size() > 0 )
    {
        m_ForwardRenderingPointLightSkinnedShaderProg.Use();
        m_ForwardRenderingPointLightSkinnedShaderProg.SetUniformLightColor( m_ForwardLightColors[0] );
        m_ForwardRenderingPointLightSkinnedShaderProg.SetUniformLightAmbientIntensity( m_ForwardLightAmbientIntensities[0] );
        m_ForwardRenderingPointLightSkinnedShaderProg.SetUniformLightDiffuseIntensity( m_ForwardLightDiffuseIntensities[0] );
        m_ForwardRenderingPointLightSkinnedShaderProg.SetUniformLightPositionInViewSpace( m_ForwardLightPositionsInViewSpace[0] );
        m_ForwardRenderingPointLightSkinnedShaderProg.SetUniformLightRadius( m_ForwardLightRadiuses[0] );
        //m_ForwardRenderingPointLightSkinnedShaderProg.SetUniformLightDirectionInViewSpaceArray
        //m_ForwardRenderingPointLightSkinnedShaderProg.SetUniformLightCutoffValueArray
        m_ForwardRenderingPointLightStaticShaderProg.Use();
        m_ForwardRenderingPointLightStaticShaderProg.SetUniformLightColor( m_ForwardLightColors[0] );
        m_ForwardRenderingPointLightStaticShaderProg.SetUniformLightAmbientIntensity( m_ForwardLightAmbientIntensities[0] );
        m_ForwardRenderingPointLightStaticShaderProg.SetUniformLightDiffuseIntensity( m_ForwardLightDiffuseIntensities[0] );
        m_ForwardRenderingPointLightStaticShaderProg.SetUniformLightPositionInViewSpace( m_ForwardLightPositionsInViewSpace[0] );
        m_ForwardRenderingPointLightStaticShaderProg.SetUniformLightRadius( m_ForwardLightRadiuses[0] );
    }

    _RenderObjectsForward( m_FordwardOrDefferedRenderList );

    _RenderForwardOnlyObjects( m_FordwardOnlyRenderList );
    _RenderForwardOnlyObjects( m_FordwardOnlyRenderLastRenderList );
    _FinalRenderingPass();

    m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionDepthTesting );
    m_GraphicsDevice.RestorePreviousDepthBufferWritingState();
}

//-----------------------------------------------------------------------

void RenderManager::_RenderObjectsForward(RenderList_t& renderList)
{
    for (RenderUnit& renderUnit : renderList)
    {
        const ShaderType shaderType = renderUnit.GetShaderType();

        if( ( shaderType == e_ShaderTypeSkinned ) || ( shaderType == e_ShaderTypeStatic ) )
        {
            RenderableObject& obj = renderUnit.GetRenderable();

            const glm::mat4& objModelMatrix = obj.GetWorldTransformMatrix();

            ForwardRenderingShader* forwardRenderingShaderProg = &m_ForwardRenderingPointLightStaticShaderProg;

#ifndef OPENGL_ES
            if( shaderType == e_ShaderTypeSkinned )
            {
                forwardRenderingShaderProg = &m_ForwardRenderingPointLightSkinnedShaderProg;
                forwardRenderingShaderProg->Use();
                m_ForwardRenderingPointLightSkinnedShaderProg.SetUniformJointMatrices( obj.GetBoneMatrices()->size(),
                                                                                       &(*obj.GetBoneMatrices())[0] );
            }
            forwardRenderingShaderProg->Use();
            forwardRenderingShaderProg->SetUniformViewMatrix( m_VPTransformations.viewMatrix );
            forwardRenderingShaderProg->SetUniformModelMatrix( objModelMatrix );
            forwardRenderingShaderProg->SetUniformModelViewProjectionMatrix( m_VPTransformations.projectionMatrix *
                                                                             m_VPTransformations.viewMatrix *
                                                                             objModelMatrix );
            forwardRenderingShaderProg->SetUniformNormalMatrix( glm::transpose( glm::inverse( m_VPTransformations.viewMatrix * objModelMatrix ) ) );
#else
            ForwardRenderingNoLightingShader* forwardRenderingShaderProg = &m_ForwardRenderingNoLightingStaticShaderProg;

            if( shaderType == e_ShaderTypeSkinned )
            {
                forwardRenderingShaderProg = &m_ForwardRenderingNoLightingSkinnedShaderProg;
                forwardRenderingShaderProg->Use();
                m_ForwardRenderingNoLightingSkinnedShaderProg.SetUniformJointMatrices( obj.GetBoneMatrices()->size(),
                                                                                       &(*obj.GetBoneMatrices())[0] );
            }

            forwardRenderingShaderProg->Use();
            forwardRenderingShaderProg->SetUniformModelViewProjectionMatrix( m_VPTransformations.projectionMatrix *
                                                                              m_VPTransformations.viewMatrix *
                                                                              objModelMatrix );
#endif
//            m_GenericTexturedShader.Use();
//            m_GenericTexturedShader.SetUniformModelViewProjectionMatrix( m_VPTransformations.projectionMatrix *
//                                                                         m_VPTransformations.viewMatrix *
//                                                                         objModelMatrix );

            obj.BindForRendering();

            _RenderPrimitives( obj );
        }
    }
}

//-----------------------------------------------------------------------

void RenderManager::_DeferredPath()
{
    m_GraphicsDevice.SaveCurrentDepthBufferWritingState();
    m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionDepthTesting );
    m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionBlending );

    m_PostProcessingFBO.BindColorAttachmentsForDrawing( FBO::e_ColorAttachment0 );
    m_GraphicsDevice.EnableDepthBufferWriting(); // Enable depth buffer writing, because otherwise it will not be cleared
    m_GraphicsDevice.ClearBuffers( GraphicsDevice::e_BufferTypeColor | GraphicsDevice::e_BufferTypeDepth );

    _GeometryRenderingPass();
    if( m_TakeADebugScreenshotOfDefferedDepthBuffers )
    {
        _DepthBufferDebugPass( "After_Geometry_Pass" );
    }
    _LightRenderingPass();
    if( m_TakeADebugScreenshotOfDefferedDepthBuffers )
    {
        _DepthBufferDebugPass( "After_Lighting_Pass" );
    }
    _PostDefferedForwardPass();
    _FinalRenderingPass();

    m_TakeADebugScreenshotOfDefferedDepthBuffers = false;

    m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionBlending );
    m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionDepthTesting );
    m_GraphicsDevice.RestorePreviousDepthBufferWritingState();
}

//-----------------------------------------------------------------------

void RenderManager::_DepthBufferDebugPass(const std::string& outputImageFileName)
{
    // Calculate projection constants used for converting depth buffer values to view space vertex positions
    const float projectionA = ( m_FrustumFarClippingPlaneDistance / ( m_FrustumFarClippingPlaneDistance - m_FrustumNearClippingPlaneDistance ) );
    const float projectionB = ( ( -m_FrustumFarClippingPlaneDistance * m_FrustumNearClippingPlaneDistance ) / ( m_FrustumFarClippingPlaneDistance - m_FrustumNearClippingPlaneDistance ) );

    m_PostProcessingFBO.BindColorAttachmentsForDrawing( FBO::e_ColorAttachment1 );
    m_PostProcessingFBO.BindDepthAttachmentTextureForReading();
    m_DepthDebugShaderProg.Use();
    m_DepthDebugShaderProg.SetUniformScreenSize( glm::vec2( m_DrawableSurfaceSize.widthInPixels, m_DrawableSurfaceSize.heightInPixels ) );
    m_DepthDebugShaderProg.SetUniformProjectionA( projectionA );
    m_DepthDebugShaderProg.SetUniformProjectionB( projectionB );

    m_GraphicsDevice.SaveCurrentDepthBufferWritingState();
    m_GraphicsDevice.DisableDepthBufferWriting();

    m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionDepthTesting );
    m_GraphicsDevice.OptionDisable( GraphicsDevice::e_OptionDepthTesting );

    m_FullScreenQuadMesh.Render();
    m_PostProcessingFBO.BindColorAttachmentForReading( FBO::e_ColorAttachmentIdx1 );
    m_ScreenshotCapture.CaptureScreenshot( outputImageFileName );

    m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionDepthTesting );
    m_GraphicsDevice.RestorePreviousDepthBufferWritingState();
}

//-----------------------------------------------------------------------

void RenderManager::_GeometryRenderingPass()
{
    m_GBuffer.BindForGeomPass();

    // Only the geometry pass updates the depth buffer
    m_GraphicsDevice.EnableDepthBufferWriting();

    m_GraphicsDevice.SaveCurrentClearStencilValue();
    m_GraphicsDevice.SetClearStencilValue( 0 ); // Set stencil buffer clear value to 0
    m_GraphicsDevice.ClearBuffers( GraphicsDevice::e_BufferTypeColor | GraphicsDevice::e_BufferTypeDepth | GraphicsDevice::e_BufferTypeStencil );
    m_GraphicsDevice.RestorePreviousClearStencilValue();

    m_GraphicsDevice.OptionEnable( GraphicsDevice::e_OptionDepthTesting );
    m_GraphicsDevice.OptionDisable( GraphicsDevice::e_OptionBlending ); // No blending is necessary when creating G-Buffer

    /*------------------------------------------------------------------------------------------------------*/
    // Setup a stencil buffer in such a way that it is filled with positive value in places where
    // geometry was rendered. This stencil buffer will be used below to render background around
    // the renderer geometry.
    //
    // For this we need the stencil test to be enabled but we want it to always succeed, because
    // when filling the stencil buffer for background rendering only the depth test matters.
    m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionStencilTesting );
    m_GraphicsDevice.OptionEnable( GraphicsDevice::e_OptionStencilTesting );

    m_GraphicsDevice.SaveCurrentStencilFunctionState();
    m_GraphicsDevice.SetStencilFunctionState( GraphicsDevice::e_StencilFunctionAlways, 0, 0 );

    m_GraphicsDevice.SaveCurrentBackFaceStencilOperationState();
    m_GraphicsDevice.SetBackFaceStencilOperationState( GraphicsDevice::e_StencilActionKeep, // Stencil if configured to always pass, hence this will have no effect

                                                       // If depth test was performed it means geometry was rendered, hence increase the value
                                                       GraphicsDevice::e_StencilActionIncrementWrap,
                                                       GraphicsDevice::e_StencilActionIncrementWrap );

    m_GraphicsDevice.SaveCurrentFrontFaceStencilOperationState();
    m_GraphicsDevice.SetFrontFaceStencilOperationState( GraphicsDevice::e_StencilActionKeep, // Stencil if configured to always pass, hence this will have no effect

                                                        // If depth test was performed it means geometry was rendered, hence increase the value
                                                        GraphicsDevice::e_StencilActionIncrementWrap,
                                                        GraphicsDevice::e_StencilActionIncrementWrap );
    /*------------------------------------------------------------------------------------------------------*/

    if( m_TakeADebugScreenshotOfDefferedStencilBuffers )
    {
        m_ScreenshotCapture.CaptureStencilBufferScreenshot( "Stencil_Before_Rendering" );
    }

    /*-------------- Do geometry rendering to GBuffer here -----------------*/
    _RenderObjects( m_FordwardOrDefferedRenderList );
    _RenderObjects( m_FordwardOrDefferedRenderLastRenderList );
    /*----------------------------------------------------------------------*/

    // When we get here the depth buffer is already populated and the stencil pass
    // depends on it, but it does not write to it.
    m_GraphicsDevice.DisableDepthBufferWriting(); // This is very important for correct functioning of the position reconstruction from the depth buffer
    m_GraphicsDevice.OptionDisable( GraphicsDevice::e_OptionDepthTesting );

    if( m_TakeADebugScreenshotOfDefferedStencilBuffers )
    {
        m_ScreenshotCapture.CaptureStencilBufferScreenshot( "Stencil_After_Rendering" );
    }

    /*--------------------------------------- Render background here ---------------------------------------*/
    // Render the background to this particular color attachment, because this same color attachment
    // will be used by the lighting pass to write the results of the final scene
    m_PostProcessingFBO.BindColorAttachmentsForDrawing( FBO::e_ColorAttachment0 );

    m_DebugObjectsShaderProg.Use();
    m_DebugObjectsShaderProg.SetUniformColor( glm::vec3( m_BackgroundColor.LinearValue().x,
                                                         m_BackgroundColor.LinearValue().y,
                                                         m_BackgroundColor.LinearValue().z ) );
    m_DebugObjectsShaderProg.SetUniformUseVertexColors( false );
    m_DebugObjectsShaderProg.SetUniformModelViewProjectionMatrix( MathUtils::c_IdentityMatrix );

    // Set the stencil test to pass only when stencil value is zero i.e. there was no geometry rendered in that location.
    // This is achieved by using EQUAL stencil function i.e. ( ref & mask ) == ( stencil & mask ) where "ref" value is 0
    // "mask" value is 0xFF and  "stencil" value is a value currently stored in the stencil buffer in the location being
    // tested.
    m_GraphicsDevice.SetStencilFunctionState( GraphicsDevice::e_StencilFunctionEqual, 0, 0xFF );

    // Keep stencil buffer values unchanged, because we will be using stencil
    // buffer as a stencil instead of writing it with new data as above
    m_GraphicsDevice.SetBackFaceStencilOperationState( GraphicsDevice::e_StencilActionKeep,
                                                       GraphicsDevice::e_StencilActionKeep,
                                                       GraphicsDevice::e_StencilActionKeep );
    m_GraphicsDevice.SetFrontFaceStencilOperationState( GraphicsDevice::e_StencilActionKeep,
                                                        GraphicsDevice::e_StencilActionKeep,
                                                        GraphicsDevice::e_StencilActionKeep );

    m_FullScreenQuadMesh.Render();

    if( m_TakeADebugScreenshotOfDefferedStencilBuffers )
    {
        m_ScreenshotCapture.CaptureStencilBufferScreenshot( "Stencil_After_Rendering_Background" );
    }

    m_GraphicsDevice.RestorePreviousFrontFaceStencilOperationState();
    m_GraphicsDevice.RestorePreviousBackFaceStencilOperationState();
    m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionStencilTesting );
    /*------------------------------------------------------------------------------------------------------*/
}

//-----------------------------------------------------------------------

void RenderManager::_LightRenderingPass()
{
    m_GBuffer.BindForLightPass();
    m_LightManager.RenderLights( m_VPTransformations.viewMatrix,
                                 m_VPTransformations.projectionMatrix,
                                 m_FrustumNearClippingPlaneDistance,
                                 m_FrustumFarClippingPlaneDistance );
}

//-----------------------------------------------------------------------

void RenderManager::_HDRAndGammaCorrectionPass()
{
    m_GraphicsDevice.SaveCurrentPolygonMode();
    // Need to set polygon mode to fill, just in case it was set to something else for some other purpose (e.g. wireframe debugging)
    m_GraphicsDevice.SetPolygonMode( GraphicsDevice::e_PolygonModeFill );

    m_PostProcessingFBO.BindColorAttachmentTexturesForReading( FBO::e_ColorAttachment0 );
    m_PostProcessingFBO.BindColorAttachmentsForDrawing( FBO::e_ColorAttachment1 );

    m_ToneMappingAndGammaCorrectionShaderProg.Use();
    m_ToneMappingAndGammaCorrectionShaderProg.SetUniformScreenSize( glm::vec2( m_DrawableSurfaceSize.widthInPixels, m_DrawableSurfaceSize.heightInPixels ) );

    m_GraphicsDevice.SaveCurrentDepthBufferWritingState();
    m_GraphicsDevice.DisableDepthBufferWriting();

    m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionDepthTesting );
    m_GraphicsDevice.OptionDisable( GraphicsDevice::e_OptionDepthTesting );

    m_FullScreenQuadMesh.Render();

    m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionDepthTesting );
    m_GraphicsDevice.RestorePreviousDepthBufferWritingState();
    m_GraphicsDevice.RestorePreviousPolygonMode();
}

//-----------------------------------------------------------------------

void RenderManager::_FinalRenderingPass()
{
#ifndef OPENGL_ES
    _HDRAndGammaCorrectionPass();

    // Final pass, here we copy the result from post processing frame buffer to the main frame buffer
    m_PostProcessingFBO.BindColorAttachmentForReading( FBO::e_ColorAttachmentIdx1 );
    FBO::BindDefaultFBOForDrawing();

    glBlitFramebuffer(0, 0, m_DrawableSurfaceSize.widthInPixels, m_DrawableSurfaceSize.heightInPixels,
                      0, 0, m_DrawableSurfaceSize.widthInPixels, m_DrawableSurfaceSize.heightInPixels, GL_COLOR_BUFFER_BIT, GL_LINEAR);
    CheckForGLErrors();

    #ifdef VR_BACKEND_ENABLED
        m_VRHMDBuffer.SubmitTextureToHMD( m_VRHMDEyeBeingRendered, m_PostProcessingFBO, FBO::e_ColorAttachmentIdx1 );
    #endif
#endif
}

//-----------------------------------------------------------------------

void RenderManager::_PostDefferedForwardPass()
{
    m_PostProcessingFBO.BindColorAttachmentsForDrawing( FBO::e_ColorAttachment0 );

    m_GraphicsDevice.SaveCurrentDepthBufferWritingState();
    m_GraphicsDevice.EnableDepthBufferWriting();

    m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionDepthTesting );
    m_GraphicsDevice.OptionEnable( GraphicsDevice::e_OptionDepthTesting );

    _RenderForwardOnlyObjects( m_FordwardOnlyRenderList );
    _RenderForwardOnlyObjects( m_FordwardOnlyRenderLastRenderList );

    m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionDepthTesting );
    m_GraphicsDevice.RestorePreviousDepthBufferWritingState();

    FBO::BindDefaultFBOForDrawing();
}

//-----------------------------------------------------------------------

void RenderManager::_RenderObjects(RenderList_t& renderList)
{
    for (RenderUnit& renderUnit : renderList)
    {
        const ShaderType shaderType = renderUnit.GetShaderType();

        if( ( shaderType == e_ShaderTypeSkinned ) || ( shaderType == e_ShaderTypeStatic ) )
        {
            RenderableObject& obj = renderUnit.GetRenderable();

            const glm::mat4& objModelMatrix = obj.GetWorldTransformMatrix();

            ShaderProgGBuffer* gBufferShaderProg = &m_GBufferStaticShaderProg;

            if( shaderType == e_ShaderTypeSkinned )
            {
                gBufferShaderProg = &m_GBufferSkinnedShaderProg;
                gBufferShaderProg->Use();
                m_GBufferSkinnedShaderProg.SetUniformJointMatrices( obj.GetBoneMatrices()->size(),
                                                             &(*obj.GetBoneMatrices())[0] );
            }

            gBufferShaderProg->Use();

            gBufferShaderProg->SetUniformModelViewProjectionMatrix( m_VPTransformations.projectionMatrix *
                                                                    m_VPTransformations.viewMatrix *
                                                                    objModelMatrix );
            gBufferShaderProg->SetUniformNormalMatrix( glm::transpose( glm::inverse( m_VPTransformations.viewMatrix * objModelMatrix ) ) );

            obj.BindForRendering();

            _RenderPrimitives( obj );
        }
    }
}

//-----------------------------------------------------------------------

void RenderManager::_RenderForwardOnlyObjects(RenderList_t& renderList)
{
    m_DebugObjectsShaderProg.Use();

    const glm::mat4 viewProjctionMatrix = ( m_VPTransformations.projectionMatrix * m_VPTransformations.viewMatrix );

    for (RenderUnit& renderUnit : renderList)
    {
        const ShaderType shaderType = renderUnit.GetShaderType();

        if( ( shaderType == e_ShaderTypeColouredVertices ) || ( shaderType == e_ShaderTypeColouredMesh ) )
        {
            RenderableObject& obj = renderUnit.GetRenderable();

            const glm::mat4& objModelMatrix = obj.GetWorldTransformMatrix();

            if( shaderType == e_ShaderTypeColouredMesh )
            {
                m_DebugObjectsShaderProg.SetUniformUseVertexColors( false );
                m_DebugObjectsShaderProg.SetUniformColor( obj.GetDiffuseColor().LinearValue() );
            }
            else
            {
                m_DebugObjectsShaderProg.SetUniformUseVertexColors( true );
            }
            m_DebugObjectsShaderProg.SetUniformModelViewProjectionMatrix( viewProjctionMatrix * objModelMatrix );
            obj.BindForRendering();

            _RenderPrimitives( obj );
        }
    }

//    m_LightManager.RenderLightsDebugData( m_VPTransformations.viewMatrix,
//                                          m_VPTransformations.projectionMatrix );
}

//-----------------------------------------------------------------------

void RenderManager::_RenderGUI()
{
    FBO::BindDefaultFBOForDrawing();

    // Render Rocket GUI
    if( m_GUIRenderInterface )
    {
        m_GUIRenderInterface->Render( m_GUIShaderProg );
    }


    m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionDepthTesting );
    m_GraphicsDevice.OptionDisable( GraphicsDevice::e_OptionDepthTesting );
    m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionBlending );
    m_GraphicsDevice.OptionEnable( GraphicsDevice::e_OptionBlending );
    m_GraphicsDevice.SaveCurrentBlendEquation();
    m_GraphicsDevice.SetBlendEquation( GraphicsDevice::e_BlendEquationAdd );
    m_GraphicsDevice.SaveCurrentBlendFunctionState();
    m_GraphicsDevice.SetBlendFunctionState( GraphicsDevice::e_BlendFunctionSrcAlpha, GraphicsDevice::e_BlendFunctionOneMinusSrcAlpha );

    // Render custom GUI
    for (RenderUnit& renderUnit : m_GUIRenderablesList)
    {
        RenderableObject& obj = renderUnit.GetRenderable();
        const glm::mat4& objModelMatrix = obj.GetWorldTransformMatrix();
        const ShaderType shaderType = renderUnit.GetShaderType();

        if(  shaderType == e_ShaderTypeColouredMesh )
        {
            m_DebugObjectsShaderProg.Use();
            m_DebugObjectsShaderProg.SetUniformUseVertexColors( false );
            m_DebugObjectsShaderProg.SetUniformColor( obj.GetDiffuseColor().SRGBValue() );
            m_DebugObjectsShaderProg.SetUniformModelViewProjectionMatrix( m_CustomGUIProjectionMatrix * objModelMatrix );

            obj.BindForRendering();
            _RenderPrimitives( obj );
        }
        else if( shaderType == e_ShaderTypeTexturedMesh )
        {
            m_GenericTexturedShader.Use();
            m_GenericTexturedShader.SetUniformModelViewProjectionMatrix( m_CustomGUIProjectionMatrix * objModelMatrix );

            obj.BindForRendering();
            _RenderPrimitives( obj );
        }
    }

    m_GraphicsDevice.RestorePreviousBlendFunctionState();
    m_GraphicsDevice.RestorePreviousBlendEquation();
    m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionBlending );
    m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionDepthTesting );
}

//-----------------------------------------------------------------------

void RenderManager::_RenderPrimitives(const RenderableObject& r)
{
    if( !r.GetRenderWithDepthTesting() )
    {
        m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionDepthTesting );
        m_GraphicsDevice.OptionDisable( GraphicsDevice::e_OptionDepthTesting );
    }

    if( r.GetRenderingType() == RenderableObject::e_RenderingTypeIndexed )
    {
        assert( ( r.GetVertexIndexCount() != 0 ) && "There needs to be at least one vertex index" );

        const uintptr_t c_StartOffsetOfTheMesh = ( sizeof( EngineTypes::VertexIndexType ) * r.GetFirstVertexIndex() );
        glDrawRangeElements( r.GetDrawPrimitivesType(),
                             r.GetMinVertexIndexValue(),
                             r.GetMaxVertexIndexValue(),
                             r.GetVertexIndexCount(),
                             IBO::GetVertexIndexOpenGLType(),
                             ( GLvoid* )c_StartOffsetOfTheMesh );
        CheckForGLErrors();
    }
    else if( r.GetRenderingType() == RenderableObject::e_RenderingTypeDirect )
    {
        assert( ( r.GetVertexCount() != 0 ) && "There needs to be at least one vertex" );

        glDrawArrays( r.GetDrawPrimitivesType(), r.GetFirstVertexIndex() , r.GetVertexCount() );
        CheckForGLErrors();
    }
    else
    {
        throw GenericException( "Unsupported rendering type" );
    }

    if( !r.GetRenderWithDepthTesting() )
    {
        m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionDepthTesting );
    }
}

//-----------------------------------------------------------------------

void RenderManager::_DrawableSurfaceSizeChaged(const SystemInterface::DrawableSurfaceSize& newSize, void* userData)
{
    RenderManager* manager = static_cast<RenderManager*>( userData );
    manager->m_DrawableSurfaceSize = newSize;
    manager->m_GraphicsDevice.SetViewportValue( 0,
                                                0,
                                                manager->m_DrawableSurfaceSize.widthInPixels,
                                                manager->m_DrawableSurfaceSize.heightInPixels );
    manager->m_CustomGUIProjectionMatrix = glm::ortho( 0.0f,
                                                       static_cast<float>( manager->m_DrawableSurfaceSize.widthInPixels ),
                                                       static_cast<float>( manager->m_DrawableSurfaceSize.heightInPixels ),
                                                       0.0f,
                                                       -1.0f,
                                                       1.0f );
}

//-----------------------------------------------------------------------

