#ifndef OPENGLDEBUGOUTPUT_H_INCLUDED
#define OPENGLDEBUGOUTPUT_H_INCLUDED

#include <OpenGLIncludes.h>

class GraphicsDevice;

class OpenGLDebugOutput
{
public:
    enum WarningType
    {
        e_WarningTypeError = 0,
        e_WarningTypeDeprecatedBehaviour = 1,
        e_WarningTypeUndefinedBehaviour,
        e_WarningTypePortability,
        e_WarningTypePerformance,
        e_WarningTypeOther,

        e_WarningTypeCount // Not a warning type (used for counting only)
    };

    OpenGLDebugOutput(GraphicsDevice& graphicsDevice);
    void SwitchWarningTypeOn(WarningType type);
    void SwitchWarningTypeOff(WarningType type);

    void Enable();
    void Disable();

private:
    static void _GLDebugCallback( GLenum source,
                                   GLenum type,
                                   GLuint id,
                                   GLenum severity,
                                   GLsizei length,
                                   const GLchar *message,
                                   const void* userParam );

    static GLenum m_GLDebugTypes[e_WarningTypeCount];

    GraphicsDevice& m_GraphicsDevice;
};


#endif // OPENGLDEBUGOUTPUT_H_INCLUDED
