#include "BulletCharacterController.h"
#include <bullet/btBulletDynamicsCommon.h>
#include <bullet/BulletCollision/CollisionDispatch/btGhostObject.h>
#include <Physics/PhysicsManager.h>
#include <CommonDefinitions.h>

//-----------------------------------------------------------------------

BulletCharacterController::BulletCharacterController(PhysicsManager& physicsManager,
                                                     float walkHeight,
                                                     float UNUSED_PARAM(crouchHeight), // TODO Check why is this parameter not being used
                                                     float stepHeight,
                                                     float radius)
: btKinematicCharacterController(new btPairCachingGhostObject(), new btCapsuleShape( radius, walkHeight ) , stepHeight),
  m_Transform(),
  m_ControllerGhostObject( m_ghostObject ),
  m_CollisionShape( m_convexShape ),
  m_Jumps( 0 ),
  m_MaxJumps( 1 ),
  m_Height( walkHeight ),
  m_PhysicsManager( physicsManager )
{
    m_Transform.SetPosition( glm::vec3( 0.0f ) );
    m_Transform.SetRotation( glm::angleAxis( 0.0f, glm::vec3( 0.0f, 1.0f, 0.0f ) ) );

    btTransform startTransform;
    m_Transform.getWorldTransform( startTransform );

    m_ControllerGhostObject->setWorldTransform( startTransform );
	m_ControllerGhostObject->setCollisionShape( m_CollisionShape );
	m_ControllerGhostObject->setCollisionFlags( btCollisionObject::CF_CHARACTER_OBJECT );

	// Only collide with static for now (no interaction with dynamic objects)
	m_PhysicsManager.m_DynamicsWorld->addCollisionObject( m_ControllerGhostObject,
                                                          btBroadphaseProxy::CharacterFilter,
                                                          btBroadphaseProxy::StaticFilter | btBroadphaseProxy::DefaultFilter );
}

//-----------------------------------------------------------------------

BulletCharacterController::~BulletCharacterController()
{
    m_PhysicsManager.m_DynamicsWorld->removeCollisionObject( m_ControllerGhostObject );
    delete m_ControllerGhostObject;
}

//-----------------------------------------------------------------------

void BulletCharacterController::updateAction(btCollisionWorld *collisionWorld, btScalar dt)
{
	if( onGround() )
    {
		m_Jumps = 0;
    }

    btTransform currentTransform;
    m_Transform.getWorldTransform( currentTransform );
    getGhostObject()->setWorldTransform( currentTransform );

	btKinematicCharacterController::updateAction( collisionWorld, dt );
	const btVector3& pos = getGhostObject()->getWorldTransform().getOrigin();
	m_Transform.SetPosition( glm::vec3( pos.x(), pos.y(), pos.z() ) );
}

//-----------------------------------------------------------------------

unsigned int BulletCharacterController::GetMaxJumps() const
{
	return m_MaxJumps;
}

//-----------------------------------------------------------------------

void BulletCharacterController::SetMaxJumps(unsigned int maxJumps)
{
	m_MaxJumps = maxJumps;
}

//-----------------------------------------------------------------------

unsigned int BulletCharacterController::GetJumpCount() const
{
	return m_Jumps;
}

//-----------------------------------------------------------------------

bool BulletCharacterController::CanJump() const
{
	return ( onGround() || ( m_Jumps < m_MaxJumps ) );
}

//-----------------------------------------------------------------------

void BulletCharacterController::StartJump(float UNUSED_PARAM(maxJumpHeight)) // TODO check why is this parameter not being used
{
    if( !CanJump() )
    {
       return;
    }

    m_verticalVelocity = m_jumpSpeed;
    m_wasJumping = true;
    m_Jumps++;
}

//-----------------------------------------------------------------------

void BulletCharacterController::SetLinearMovement(const glm::vec3& linearVelocity)
{
    setWalkDirection( btVector3( -linearVelocity.x, linearVelocity.y, -linearVelocity.z ) );
}

//-----------------------------------------------------------------------

void BulletCharacterController::SetAngularMovement(float headingInDegrees)
{
    m_Transform.SetRotation( glm::angleAxis( headingInDegrees, glm::vec3( 0.0f, 1.0f, 0.0f ) ) );
}

//-----------------------------------------------------------------------

void BulletCharacterController::SetPos(const glm::vec3& pos)
{
    m_Transform.SetPosition( pos );

    btTransform startTransform;
    m_Transform.getWorldTransform( startTransform );

    getGhostObject()->setWorldTransform( startTransform );
}

//-----------------------------------------------------------------------

const glm::vec3& BulletCharacterController::GetPos()
{
    return m_Transform.GetPosition();
}

//-----------------------------------------------------------------------

float BulletCharacterController::GetTop()
{
    return m_Height;
}

//-----------------------------------------------------------------------
