#include "MAnim.h"

#include <OpenGLIncludes.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/quaternion.hpp>
#include "glm/gtx/compatibility.hpp"
#include <glm/gtc/matrix_transform.hpp>

#include <GraphicsDevice/VBO.h>
#include <GraphicsDevice/GraphicsDevice.h>
#include <Renderer/ShaderPrograms.h>
#include <OpenGLErrorChecking.h>
#include <SystemInterface.h>

//-----------------------------------------------------------------------

MAnim::MAnim(GraphicsDevice& graphicsDevice,
             SystemInterface& systemInterface,
             FileManager& fileManager)
: m_BoneNamesMD5Hash(),
  m_JointCount( 0 ),
  m_AnimFrameCount( 0 ),
  m_AnimFrameRateInFramesPerSecond( 0 ),
  m_BoundingBoxCount( 0 ),
  m_Joints(),
  m_AnimFrames(),
  m_CurrentFrame( m_JointCount ),
  m_BoundingBoxes(),
  m_AnimDurationInSeconds( 0.0f ),
  m_FrameDurationInSeconds( 0.0f ),
  m_CurrentAnimTimeIsSeconds( 0.0f ),
  m_CurrBoundingBoxNum( 0 ),
  m_GlobalTimeInSecondDuringLastUpdate( 0.0 ),
  m_Loop( false ),
  m_FinishedPlaying( true ),
  m_JointsDebugVAO(),
  m_JointsDebugVBO( new VBO( sizeof( Bone ), VBO::e_DataBufferUsagePatternDynamic ) ), // Note: Owned by VAO
  m_BoundsVAO(),
  m_GraphicsDevice( graphicsDevice ),
  m_SystemInterface( systemInterface ),
  m_FileManager( fileManager )
{
    m_JointsDebugVBO->AddVertexAttribute(VBO::e_VertexAttrIdxPosition,
                                         VBO::e_VertexAttrComponentTypeFloat32,
                                         VBO::e_VertexAttrTypeVec3,
                                         offsetof( Bone, m_Pos ) );

    m_JointsDebugVAO.AddVBO( m_JointsDebugVBO );

    memset( m_BoneNamesMD5Hash, 0x00, c_MD5HashLength );
}

//-----------------------------------------------------------------------

MAnim::~MAnim()
{
}

//-----------------------------------------------------------------------

bool MAnim::_ReadHeader(ByteStream& byteStream, uint32_t& jointNamesSectionSize)
{
    bool retVal = true;

    retVal = retVal && _VerifyFileTypeAndVersion( byteStream );
    retVal = retVal && byteStream.ReadBytesToBuffer( m_BoneNamesMD5Hash, c_MD5HashLength );
    retVal = retVal && byteStream.ReadUInt16( m_JointCount );
    retVal = retVal && byteStream.ReadUInt16( m_AnimFrameCount );
    retVal = retVal && byteStream.ReadUInt16( m_AnimFrameRateInFramesPerSecond );
    retVal = retVal && byteStream.ReadUInt16( m_BoundingBoxCount );
    retVal = retVal && byteStream.ReadUInt32( jointNamesSectionSize );
    retVal = retVal && _VerifyFileSize( byteStream, jointNamesSectionSize );

    return retVal;
}

//-----------------------------------------------------------------------

bool MAnim::_VerifyFileTypeAndVersion(ByteStream& byteStream)
{
    bool retVal = true;

    std::string fileIdStr = "";
    uint32_t fileVersion = 0;
    retVal = retVal && byteStream.ReadString( fileIdStr, 4 );
    retVal = retVal && byteStream.ReadUInt32( fileVersion );
    retVal = retVal && !fileIdStr.compare( "MANI" );
    retVal = retVal && (fileVersion == 1);

    return retVal;
}

//-----------------------------------------------------------------------

bool MAnim::_VerifyFileSize(ByteStream& byteStream, const uint32_t jointNamesSectionSize)
{
    const size_t c_FileHeaderSize = 36;
    const size_t c_BytesPerJoint = 30;
    const size_t c_BytesPerAnimFrame = (28 * m_JointCount);
    const size_t c_BytesPerBoundingBox = 24;
    const size_t actualFileSize = byteStream.InitialStreamSizeInBytes();

    size_t expectedFileSize = ( c_FileHeaderSize + jointNamesSectionSize );
    expectedFileSize += ( c_BytesPerJoint * m_JointCount );
    expectedFileSize += ( c_BytesPerAnimFrame * m_AnimFrameCount );
    expectedFileSize += ( c_BytesPerBoundingBox * m_BoundingBoxCount );

    return ( expectedFileSize == actualFileSize );
}

//-----------------------------------------------------------------------

bool MAnim::_SkipJointNamesSection(ByteStream& byteStream, const uint32_t jointNamesSectionSize)
{
    return byteStream.SkipBytes( jointNamesSectionSize );;
}

//-----------------------------------------------------------------------

bool MAnim::_ReadJoints(ByteStream& byteStream)
{
    assert( (m_Joints.size() == m_JointCount) && "Joint array must be of the right size before use" );
    bool retVal = true;

    for(size_t i = 0; (i < m_JointCount) && retVal; i++)
    {
        Joint& joint = m_Joints[i];
        int16_t parentId = 0;
        retVal = retVal && byteStream.ReadInt16( parentId );
        joint.m_ParentID = parentId;
        retVal = retVal && byteStream.ReadBytesToBuffer( &joint.m_Pos[0], 3 );
        retVal = retVal && byteStream.ReadBytesToBuffer( &joint.m_Orient[0], 4 );
    }

    return retVal;
}

//-----------------------------------------------------------------------

bool MAnim::_ReadFrames(ByteStream& byteStream)
{
    assert( (m_AnimFrames.size() == m_AnimFrameCount) && "Animation Frames array must be of the right size before use" );
    bool retVal = true;

    for(size_t i = 0; (i < m_AnimFrameCount) && retVal; i++)
    {
        AnimFrame& animFrame = m_AnimFrames[i];
        assert( (animFrame.m_Joints.size() == m_JointCount) && "Fames' joints array must be of the right size before use");
        assert( (animFrame.m_Joints.size() == animFrame.m_BoneMatrices.size()) && "Frames' joints and bone matrixes arrays must be of the same size" );
        for(size_t j = 0; (j < animFrame.m_Joints.size()) && retVal; j++)
        {
            Joint& joint = animFrame.m_Joints[j];
            joint.m_ParentID = m_Joints[j].m_ParentID;
            retVal = retVal && byteStream.ReadBytesToBuffer( &joint.m_Pos[0], 3 );
            retVal = retVal && byteStream.ReadBytesToBuffer( &joint.m_Orient[0], 4 );

            const glm::mat4 boneTranslate = glm::translate( glm::mat4(1.0f), joint.m_Pos );
            const glm::mat4 boneRotate = glm::toMat4( joint.m_Orient );
            animFrame.m_BoneMatrices[j] = ( boneTranslate * boneRotate );
        }
    }

    return retVal;
}

//-----------------------------------------------------------------------

bool MAnim::_ReadBoundingBoxes(ByteStream& byteStream)
{
    assert( (m_BoundingBoxes.size() == m_BoundingBoxCount) && "Bounding boxes array must be of the right size before use" );
    bool retVal = true;

    for(size_t i = 0; (i < m_BoundingBoxCount) && retVal; i++)
    {
        glm::vec3 lowerLeftFrontCorner( 0.0f );
        glm::vec3 upperRightBackCorner( 0.0f );

        retVal = retVal && byteStream.ReadBytesToBuffer( &lowerLeftFrontCorner[0], 3 );
        retVal = retVal && byteStream.ReadBytesToBuffer( &upperRightBackCorner[0], 3 );

        if( retVal )
        {
            EngineTypes::AABB& boundingBox = m_BoundingBoxes[i];

            boundingBox.center = ( (upperRightBackCorner + lowerLeftFrontCorner) * 0.5f );
            boundingBox.halfExtents = ( (upperRightBackCorner - lowerLeftFrontCorner) * 0.5f );
        }
    }

    return retVal;
}

//-----------------------------------------------------------------------

bool MAnim::LoadFromFile(const std::string& fileName)
{
    bool retVal = false;

    ByteStream byteStream( m_FileManager );
    if( byteStream.InitFromFile( fileName ) )
    {
        retVal = true;

        uint32_t jointNamesSectionSize = 0;
        retVal = retVal && _ReadHeader( byteStream, jointNamesSectionSize );
        retVal = retVal && _SkipJointNamesSection( byteStream, jointNamesSectionSize );

        if( retVal )
        {
            m_Joints.reserve( m_JointCount );
            m_Joints.assign( m_JointCount, Joint() );
            m_CurrentFrame = AnimFrame( m_JointCount );
            m_AnimFrames.reserve( m_AnimFrameCount );
            m_AnimFrames.assign( m_AnimFrameCount, AnimFrame( m_JointCount ) );
            m_BoundingBoxes.reserve( m_BoundingBoxCount );
            m_BoundingBoxes.assign( m_BoundingBoxCount, EngineTypes::AABB() );
        }

        retVal = retVal && _ReadJoints( byteStream );
        retVal = retVal && _ReadBoundingBoxes( byteStream );
        retVal = retVal && _ReadFrames( byteStream );

        if( retVal )
        {
            m_FrameDurationInSeconds = ( 1.0f / static_cast<float>( m_AnimFrameRateInFramesPerSecond ) );
            m_AnimDurationInSeconds = ( m_FrameDurationInSeconds * static_cast<float>( m_AnimFrameCount ) );
        }
    }

    if( retVal )
    {
        _CreateBoundsVertexBuffers();
    }

    return retVal;
}

//-----------------------------------------------------------------------

bool MAnim::Update()
{
    if( m_FinishedPlaying == false )
    {
        const double currentTimeInSeconds = m_SystemInterface.GetTicksInSeconds();
        const float deltaTime = static_cast<float>( std::max( 0.0, ( currentTimeInSeconds - m_GlobalTimeInSecondDuringLastUpdate ) ) );
        m_GlobalTimeInSecondDuringLastUpdate = currentTimeInSeconds;

        if ( m_AnimFrameCount >= 1 )
        {
            m_CurrentAnimTimeIsSeconds += deltaTime;

            if( m_Loop || ( m_CurrentAnimTimeIsSeconds <= m_AnimDurationInSeconds ) )
            {
                while ( m_CurrentAnimTimeIsSeconds > m_AnimDurationInSeconds ) m_CurrentAnimTimeIsSeconds -= m_AnimDurationInSeconds;
                while ( m_CurrentAnimTimeIsSeconds < 0.0f ) m_CurrentAnimTimeIsSeconds += m_AnimDurationInSeconds;

                // Figure out which frame we're on
                float frameNumber = m_CurrentAnimTimeIsSeconds * static_cast<float>( m_AnimFrameRateInFramesPerSecond );
                unsigned int frame0 = static_cast<int>( floorf( frameNumber ) );
                unsigned int frame1 = static_cast<int>( ceilf( frameNumber ) );

                if( !m_Loop && ( ( frame0 >= m_AnimFrameCount ) || ( frame1 >= m_AnimFrameCount ) ) )
                {
                    const uint16_t theLastAnimFrameNumber = ( m_AnimFrameCount - 1 );

                    frame0 = theLastAnimFrameNumber;
                    frame1 = theLastAnimFrameNumber;

                    m_FinishedPlaying = true;
                }
                else
                {
                    frame0 = frame0 % m_AnimFrameCount;
                    frame1 = frame1 % m_AnimFrameCount;
                }

                const float interpolate = ( fmodf( m_CurrentAnimTimeIsSeconds, m_FrameDurationInSeconds ) / m_FrameDurationInSeconds );

                _InterpolateSkeletons( m_CurrentFrame, m_AnimFrames[frame0], m_AnimFrames[frame1], interpolate );

                m_CurrBoundingBoxNum = frame0;
            }
        }
    }

    return !m_FinishedPlaying;
}

//-----------------------------------------------------------------------

void MAnim::StartAnimtion(bool loop)
{
    m_CurrentAnimTimeIsSeconds = 0.0f;
    m_GlobalTimeInSecondDuringLastUpdate = m_SystemInterface.GetTicksInSeconds();
    m_Loop = loop;
    m_FinishedPlaying = false;
}

//-----------------------------------------------------------------------

void MAnim::_InterpolateSkeletons(AnimFrame& finalFrame, const AnimFrame& frame0, const AnimFrame& frame1, float interpolate)
{
    for ( int i = 0; i < m_JointCount; ++i )
    {
        Joint& finalJoint = finalFrame.m_Joints[i];
        glm::mat4& finalMatrix = finalFrame.m_BoneMatrices[i];

        const Joint& joint0 = frame0.m_Joints[i];
        const Joint& joint1 = frame1.m_Joints[i];

        finalJoint.m_ParentID = joint0.m_ParentID;

        finalJoint.m_Pos = glm::lerp( joint0.m_Pos, joint1.m_Pos, interpolate );
        finalJoint.m_Orient = glm::shortMix( joint0.m_Orient, joint1.m_Orient, interpolate );

        // Build the bone matrix for GPU skinning.
        finalMatrix = glm::translate( glm::mat4( 1.0f ), finalJoint.m_Pos ) * glm::toMat4( finalJoint.m_Orient );

//// This would be needed if animation frames were in local coordinates instead of world coordinates
//        if( m_Joints[i].m_ParentID >= 0)
//        {
//            finalMatrix = finalMatrix * finalFrame.m_BoneMatrices[m_Joints[i].m_ParentID];
//        }
    }
}

//-----------------------------------------------------------------------

void MAnim::_CopyFrame(AnimFrame& to, const AnimFrame& from)
{
    for ( int i = 0; i < m_JointCount; ++i )
    {
        Joint& toJoint = to.m_Joints[i];
        glm::mat4& toMatrix = to.m_BoneMatrices[i];

        const Joint& fromJoint = from.m_Joints[i];

        toJoint.m_ParentID = fromJoint.m_ParentID;
        toJoint.m_Pos = fromJoint.m_Pos;
        toJoint.m_Orient = fromJoint.m_Orient;

        toMatrix = glm::translate( glm::mat4( 1.0f ), toJoint.m_Pos ) * glm::toMat4( toJoint.m_Orient );
    }
}


//-----------------------------------------------------------------------

/**
 * Renders bones, joints and bounding box of the animation
 */
void MAnim::RenderDebugData(ShaderProgDebugObjects& shaderProg)
{
    const size_t numOfVertices = _CreateBonesVertexBuffers();

    m_GraphicsDevice.SaveCurrentPolygonMode();
    m_GraphicsDevice.SaveCurrentPointSizeValue();

    /*------------------------- Render Skeleton and Joints -------------------------*/
    m_JointsDebugVAO.Bind();

    static const glm::vec3 c_SilverGreyColour( 1.0f / 192.0f, 1.0f / 192.0f, 1.0f / 192.0f );
    shaderProg.SetUniformColor( c_SilverGreyColour );

    m_GraphicsDevice.SetPolygonMode( GraphicsDevice::e_PolygonModePoint );
    m_GraphicsDevice.SetPointSizeValue( 5.0f );
    glDrawArrays(GL_POINTS, 0, static_cast<GLsizei>(numOfVertices));
    CheckForGLErrors();

    m_GraphicsDevice.SetPolygonMode( GraphicsDevice::e_PolygonModeLine );
    glDrawArrays(GL_LINES, 0, static_cast<GLsizei>(numOfVertices));
    CheckForGLErrors();

    m_JointsDebugVAO.Unbind();
    /*------------------------------------------------------------------------------*/

    /*---------------------------- Render Bounding Box -----------------------------*/
    m_BoundsVAO.Bind();

    static const glm::vec3 c_RedColour( 1.0f, 0.0f, 0.0f );
    shaderProg.SetUniformColor( c_RedColour );

    m_GraphicsDevice.SetPolygonMode( GraphicsDevice::e_PolygonModeLine );
    m_GraphicsDevice.SetPointSizeValue( 5.0f );
    glDrawArrays(GL_LINES, m_CurrBoundingBoxNum * 24, 24);

    m_BoundsVAO.Unbind();
    /*------------------------------------------------------------------------------*/

    m_GraphicsDevice.RestorePreviousPointSizeValue();
    m_GraphicsDevice.RestorePreviousPolygonMode();
}

//-----------------------------------------------------------------------

size_t MAnim::_CreateBonesVertexBuffers()
{
    std::vector<Bone> bones;

    JointList joints;
    for(size_t i = 0; i < m_CurrentFrame.m_Joints.size(); i++)
    {
        //m_CurrentFrame.m_Joints[i].m_Scale = glm::vec3( 1.0f );
        joints.push_back( m_CurrentFrame.m_Joints[i] );
    }

    for ( size_t i = 0; i < joints.size(); ++i )
    {
        Joint& j0 = joints[i];
        if ( j0.m_ParentID != -1 )
        {
            Joint& j1 = joints[j0.m_ParentID];

            //j0.m_Transform = glm::translate( glm::mat4(1.0f), j0.m_Pos ) * glm::toMat4( j0.m_Orient ) * glm::scale( glm::mat4(1.0f), j0.m_Scale );
            //j0.m_Transform = j1.m_Transform * j0.m_Transform;

            Bone b1 = { j0.m_Pos, j0.m_Orient, static_cast<int>( i ) };
            Bone b2 = { j1.m_Pos, j1.m_Orient, j0.m_ParentID };

            bones.push_back( b2 );
            bones.push_back( b1 );
        }
        else
        {
            //j0.m_Transform = glm::translate( glm::mat4(1.0f), j0.m_Pos ) * glm::toMat4( j0.m_Orient ) * glm::scale(glm::mat4(1.0f), j0.m_Scale);
        }
    }

    m_JointsDebugVBO->BindData( bones.size() * sizeof ( Bone ), &( bones[0] ) );

    return bones.size();
}

//-----------------------------------------------------------------------

size_t MAnim::_CreateBoundsVertexBuffers()
{
    std::vector<glm::vec3> vertices;
    for(size_t i=0; i<m_BoundingBoxes.size(); i++)
    {
        const EngineTypes::AABB& bb = m_BoundingBoxes[i];

        const glm::vec3&   minVal = ( bb.center + bb.halfExtents );
        const glm::vec3&   maxVal = ( bb.center - bb.halfExtents );

        const glm::vec3 p1 =  minVal;
        const glm::vec3 p2 =  glm::vec3(minVal.x, maxVal.y, minVal.z);
        const glm::vec3 p3 =  glm::vec3(minVal.x, maxVal.y, maxVal.z);
        const glm::vec3 p4 =  glm::vec3(minVal.x, minVal.y, maxVal.z);
        const glm::vec3 p5 =  maxVal;
        const glm::vec3 p6 =  glm::vec3(maxVal.x, maxVal.y, minVal.z);
        const glm::vec3 p7 =  glm::vec3(maxVal.x, minVal.y, minVal.z);
        const glm::vec3 p8 =  glm::vec3(maxVal.x, minVal.y, maxVal.z);

        vertices.push_back( p1 );
        vertices.push_back( p2 );
        vertices.push_back( p2 );
        vertices.push_back( p3 );
        vertices.push_back( p3 );
        vertices.push_back( p4 );
        vertices.push_back( p4 );
        vertices.push_back( p1 );
        vertices.push_back( p3 );
        vertices.push_back( p5 );
        vertices.push_back( p5 );
        vertices.push_back( p6 );
        vertices.push_back( p6 );
        vertices.push_back( p2 );
        vertices.push_back( p5 );
        vertices.push_back( p8 );
        vertices.push_back( p8 );
        vertices.push_back( p7 );
        vertices.push_back( p7 );
        vertices.push_back( p6 );
        vertices.push_back( p7 );
        vertices.push_back( p1 );
        vertices.push_back( p8 );
        vertices.push_back( p4 );
    }

    VBO* vbo = new VBO( sizeof( glm::vec3 ) );

    vbo->AddVertexAttribute(VBO::e_VertexAttrIdxPosition,
                            VBO::e_VertexAttrComponentTypeFloat32,
                            VBO::e_VertexAttrTypeVec3,
                            0);

    vbo->BindData( vertices.size() * sizeof ( glm::vec3 ), &(vertices[0]) );

    m_BoundsVAO.AddVBO( vbo );

    return vertices.size();
}

//-----------------------------------------------------------------------

#include <iostream>
void MAnim::DebugDumpData()
{
    std::cout << "Joint Count           : " << m_JointCount << std::endl;
    std::cout << "Animation Frame Count : " << m_AnimFrameCount << std::endl;
    std::cout << "Animation Frame Rate  : " << m_AnimFrameRateInFramesPerSecond << std::endl;

    std::cout << "---------------------------- Joints ----------------------------" << std::endl;
    for(size_t i = 0; i < m_Joints.size(); i++)
    {
        const Joint& jnt = m_Joints[i];
        std::cout << "  Parent : " << jnt.m_ParentID << std::endl;
        std::cout << "  Pos    : " << jnt.m_Pos[0] << ", " << jnt.m_Pos[1] << ", " << jnt.m_Pos[2] << std::endl;
        std::cout << "  Orient : " << jnt.m_Orient[0] << ", " << jnt.m_Orient[1] << ", " << jnt.m_Orient[2] << ", " << jnt.m_Orient[3] << std::endl;
        std::cout << std::endl;
    }
    std::cout << "----------------------------------------------------------------" << std::endl;
    std::cout << std::endl;
    std::cout << "------------------------ Bounding Boxes ------------------------" << std::endl;
    for(size_t i = 0; i < m_BoundingBoxCount; i++)
    {
        const EngineTypes::AABB& boundingBox = m_BoundingBoxes[i];
        std::cout << "  Center : " << boundingBox.center[0] << ", "
                              << boundingBox.center[1] << ", "
                              << boundingBox.center[2] << std::endl;
        std::cout << "  Half Extents : " << boundingBox.halfExtents[0] << ", "
                                    << boundingBox.halfExtents[1] << ", "
                                    << boundingBox.halfExtents[2] << std::endl;
        std::cout << std::endl;
    }
    std::cout << "----------------------------------------------------------------" << std::endl;
    std::cout << std::endl;
    std::cout << "----------------------- Animation Frames -----------------------" << std::endl;
    for(size_t i = 0; i < m_AnimFrames.size(); i++)
    {
        const AnimFrame& animFrame = m_AnimFrames[i];
        std::cout << "  Frame : " << i << std::endl;
        std::cout << "  ---------------------- Joints ----------------------" << std::endl;
        for(size_t j = 0; j < animFrame.m_Joints.size(); j++)
        {
            const Joint& jnt = animFrame.m_Joints[j];
            std::cout << "  Pos    : " << jnt.m_Pos[0] << ", " << jnt.m_Pos[1] << ", " << jnt.m_Pos[2] << std::endl;
            std::cout << "  Orient : " << jnt.m_Orient[0] << ", " << jnt.m_Orient[1] << ", " << jnt.m_Orient[2] << ", " << jnt.m_Orient[3] << std::endl;
            std::cout << std::endl;
        }
        std::cout << "  ----------------------------------------------------" << std::endl;
        std::cout << std::endl;
    }
    std::cout << "----------------------------------------------------------------" << std::endl;
}

//-----------------------------------------------------------------------
