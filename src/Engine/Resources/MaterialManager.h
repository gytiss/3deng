#ifndef MATERIALMANAGER_H_INCLUDED
#define MATERIALMANAGER_H_INCLUDED

#include <list>
#include <map>
#include <string>
#include <utility>
#include <Resources/Material.h>
#include <GraphicsDevice/Texture.h>

class TextureManager;
class FileManager;

class MaterialManager
{
private:
    struct RefCountedMaterialPtr
    {
        RefCountedMaterialPtr()
        : refCount( 0 ),
          materialFilePath( "" ),
          material( nullptr )
        {

        }

        RefCountedMaterialPtr& operator= (const RefCountedMaterialPtr& other)
        {
            refCount = other.refCount;
            materialFilePath = other.materialFilePath;
            material = other.material;

            return *this;
        }

        RefCountedMaterialPtr(const RefCountedMaterialPtr& other)
        {
            operator=( other );
        }

        unsigned int refCount;
        std::string materialFilePath;
        Material* material;
    };

    typedef std::list<RefCountedMaterialPtr> MaterialList;
    typedef std::map<std::string, MaterialList::iterator> MaterialPathsToMaterialsMap;

public:
    class MaterialHandle
    {
    public:
        friend class MaterialManager;

        MaterialHandle()
        : m_MaterialListIt(),
          m_Valid( false )
        {
        }

        MaterialHandle& operator= (const MaterialHandle& other)
        {
            m_MaterialListIt = other.m_MaterialListIt;
            m_Valid = other.m_Valid;

            return *this;
        }

        MaterialHandle(const MaterialHandle& other)
        {
            operator= ( other );
        }

        Material* operator->()
        {
            return (*m_MaterialListIt).material;
        }

        const Material* operator->() const
        {
            return (*m_MaterialListIt).material;
        }

        bool IsValid() const
        {
            return m_Valid;
        }

        void Invalidate()
        {
            m_Valid = false;
        }

    protected:
        MaterialHandle(MaterialList::iterator materialListIt)
        : m_MaterialListIt( materialListIt ),
          m_Valid( true )
        {
        }

        MaterialList::iterator m_MaterialListIt;  // Iterator to the materials list i.e. m_MaterialList
        bool m_Valid;
    };

    MaterialManager(TextureManager& textureManager, FileManager& fileManager);
    ~MaterialManager();

    MaterialHandle LoadMaterial(const std::string& materialFilePath);

    MaterialHandle AddInternalMaterial(const MaterialDescriptor& descriptor);

    /**
     * Create a second material handle which is associated with the same material as the original handle.
     * This means that when "ReleaseMaterial()" is called on one of the handles the material is not released
     * until "ReleaseMaterial()" is called on the second handle as well.
     */
    MaterialHandle DuplicateMaterialHandle(const MaterialHandle& handle);

    void ReleaseMaterial(MaterialHandle& handle);

private:
    typedef std::pair<const char*, Texture::TypeIdx> MatMapNameToTextureTypeIdPair;
    typedef std::pair<const char*, Texture::StateParamArg> MatMapWrapModeNameToTextureWrapMode;

    struct MaterialMapEntry
    {
        std::string textureFilePath;
        Texture::TypeIdx textureTypeIdx;
        Texture::StateParamArg sWrapMode;
        Texture::StateParamArg tWrapMode;
    };

    // Used to prevent copying
    MaterialManager& operator = (const MaterialManager& other);
    MaterialManager(const MaterialManager& other);

    MaterialList::iterator _LoadMaterialFromFile(const std::string& materialFilePath);

    bool _MatMapNameToTextureTypeIdx(const std::string& matMapName, Texture::TypeIdx& idx);
    bool _MatMapSWrapModeNameToTextureWrapMode(const std::string& matWrapModeName, Texture::StateParamArg& mode);
    bool _MatMapTWrapModeNameToTextureWrapMode(const std::string& matWrapModeName, Texture::StateParamArg& mode);
    bool _MatMapRWrapModeNameToTextureWrapMode(const std::string& matWrapModeName, Texture::StateParamArg& mode);
    bool _CheckIfDiffuseMapIsPresent(const std::vector<MaterialMapEntry>& materialMapEntries);
    void _AddNullMapsInPlaceOfMissingOnes(const std::vector<MaterialMapEntry>& materialMapEntries, Material& mat);

    std::string _ConstructUniqueInternalMaterialName(Material& material) const;

    TextureManager& m_TextureManager;
    FileManager& m_FileManager;
    MaterialList m_MaterialList;
    MaterialPathsToMaterialsMap m_MaterialPathsToMaterialsMap;
    static const MatMapNameToTextureTypeIdPair m_MatMapNameToTextureType[];
    static const MatMapWrapModeNameToTextureWrapMode m_MatMapSWrapModeNameToTextureWrapMode[];
    static const MatMapWrapModeNameToTextureWrapMode m_MatMapTWrapModeNameToTextureWrapMode[];
    static const MatMapWrapModeNameToTextureWrapMode m_MatMapRWrapModeNameToTextureWrapMode[];
    static bool m_ClassAlreadyInstantiated;
};

#endif // MATERIALMANAGER_H_INCLUDED
