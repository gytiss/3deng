#ifndef SHADER_H_INCLUDED
#define SHADER_H_INCLUDED

#include <OpenGLIncludes.h>

#include <string>
#include <vector>
#include <fstream>

class ShaderProg;
class FileManager;

class Shader
{
public:
    friend class ShaderProg;

    enum ShaderType
    {
        e_ShaderTypeVertex = 0,
        e_ShaderTypeFragment = 1,
        e_ShaderTypeGeometry
    };

    ~Shader();

   /**
    * Compile the source code and associate it with a shader handle
    */
    void LoadAndCompile();

    inline GLuint GetHandle() const
    {
        return m_ShaderHandle;
    }

   /**
    * Returns the name of the file a shader source code was loaded from.
    */
    const std::string& GetName() const
    {
        return m_ShaderFilePath;
    }

protected:
    Shader(FileManager& fileManager, const std::string& shaderFilePath, ShaderType st, const std::string& defines);

private:
    // Used to prevent copying
    Shader& operator = (const Shader& other);
    Shader(const Shader& other);

   /**
    * Compile shader
    */
    void _Compile();

   /**
    * Checks if the shader was compilled correctly
    */
    void _CheckShaderCompileStatus();

   /**
    * Function that will read a file into an allocated char pointer buffer
    */
    bool _ReadShaderSourceFromFile(const std::string& fileName, std::string& outputBuf);

    /**
     * Scans a given piece of shader source code for "#pragma IncludeExternalSource "PathToSource""
     * directives and adds source file names specified by them to the given list
     */
    void _ExtractIncludeFileNames(std::vector<std::string>& includeFileNames,
                                  const std::string& shaderSourceCode);

    void _AddDefinesAndShadingLanguageVersion(std::string& shaderSourceCode) const;
    std::string _ConstructExceptionMessage(const std::string& errorMsg);

    static std::string c_ShadingLanguageVersion;
    const ShaderType m_ShaderType;
    const std::string m_ShaderFilePath;
    const std::string m_Defines;
    GLuint m_ShaderHandle;
    bool m_ShaderIsCompiled;
    bool m_ShaderIsLoaded;
    std::vector<std::string> m_ShaderSources;
    FileManager& m_FileManager;
};



#endif // SHADER_H_INCLUDED
