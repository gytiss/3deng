#ifndef MTC_H_INCLUDED
#define MTC_H_INCLUDED

#include <AlignedAlocator.h>
#include <cstdint>
#include <vector>
#include <string>

class ByteStream;
class FileManager;

class MTC
{
public:
    enum TextureType
    {
        e_TextureType1D = 0,
        e_TextureType2D = 1,
        e_TextureTypeCubeMap = 2,
        e_TextureTypeCount // Not a texture type (Used for counting)
    };

    enum TextureDataType
    {
        e_TextureDataTypeRawRGB = 0,
        e_TextureDataTypeRawRGBA = 1,
        e_TextureDataTypeRawSRGB = 2,
        e_TextureDataTypeRawSRGBA = 3,
        e_TextureDataTypeETC2_RGB = 4,
        e_TextureDataTypeETC2_SRGB = 5,
        e_TextureDataTypeETC2_EAC_RGBA = 6,
        e_TextureDataTypeETC2_EAC_SRGBA = 7,
        e_TextureDataTypeALPHA1_ETC2_RGBA = 8,
        e_TextureDataTypeALPHA1_ETC2_SRGBA = 9,
        e_TextureDataTypeCount // Not a texture data type  (Used for counting)
    };

    MTC(FileManager& fileManager);
    ~MTC();
    bool LoadFromFile(const std::string& fileName);

    TextureType GetTextureType() const;
    TextureDataType GetTextureDataType() const;
    size_t GetMipMapLevelCount() const;
    void GetMipLevelDimensions(size_t mipMapLevel,
                               size_t& outWidthInPixels,
                               size_t& outHeightInPixels) const;
    void GetMipLevelTextureData(size_t mipMapLevel,
                                size_t faceIndex,
                                const uint8_t*& outDataBuffer,
                                size_t& outDataSizeInBytes) const;
    size_t GetTextureFaceCount() const;
    size_t GetTextureDataRowAlignment() const;

private:
    MTC(const MTC&) = delete;
    MTC& operator=(const MTC&) = delete;

    struct MipMapLevelDescriptor
    {
        uint32_t dataSize[6];
        uint32_t dataOffset[6];
    };

    typedef std::vector<MipMapLevelDescriptor> MipMapLevelDescriptorList;
    typedef std::vector<uint8_t, AlignedAlocator<uint8_t, 4>> TextureDataBuffer;

    bool _VerifyFileTypeAndVersion(ByteStream& byteStream) const;
    bool _VerifyTextureDescriptionData(uint32_t facesPerTexture,
                                       uint32_t mipMapDescriptorCount,
                                       uint32_t widthInPixels,
                                       uint32_t heightInPixles,
                                       size_t totalTextureDataSizeInBytes,
                                       size_t totalFileSize) const;
    bool _VerifyMipMapLevelDescriptor(const MipMapLevelDescriptorList& descriptors,
                                      uint32_t facesPerTexture,
                                      uint32_t totalTextureDataSizeInBytes) const;
    bool _ReadTextureDescriptionData(ByteStream& byteStream,
                                     uint16_t& outTextureType,
                                     uint16_t& outTextureDataType,
                                     uint32_t& outMipMapDescriptorCount,
                                     uint32_t& outWidthInPixels,
                                     uint32_t& outHeightInPixles,
                                     uint32_t& outTotalTextureDataSizeInBytes) const;
    bool _ReadMipMapLevelDescriptors(ByteStream& byteStream,
                                     uint32_t facesPerTexture,
                                     uint32_t mipMapDescriptorCount,
                                     MipMapLevelDescriptorList& outDescriptors) const;
    bool _ReadTextureData(ByteStream& byteStream,
                          uint32_t totalTextureDataSizeInBytes,
                          TextureDataBuffer& textureDataBuffer);

    FileManager& m_FileManager;
    TextureType m_TextureType;
    TextureDataType m_TextureDataType;
    MipMapLevelDescriptorList m_MipMapLevelDescriptorList;
    size_t m_TopLevelMipMapWidthInPixels;
    size_t m_TopLevelMipMapHeightInPixles;
    size_t m_TextureFaceCount;
    TextureDataBuffer m_TextureData; // 4 bytes aligned memory buffer
};

#endif // MTC_H_INCLUDED
