#include "Animator.h"
#include "SkinnedMeshRenderer.h"
#include "GameObject.h"
#include <stack>
#include <cassert>

//-----------------------------------------------------------------------

Animator::Animator(GameObject& owningGameObject, AnimationManager& animationManager)
: Component( e_TypeAnimator, "Animator", owningGameObject ),
  m_AnimationManager( animationManager ),
  m_AnimationNameToIdxMap(),
  m_AnimationList(),
  m_CurrentlyPlayingAnimation( nullptr ),
  m_ConnectedSkinnedMeshRenderers(),
  m_ConnectionWithSkinnedMeshesAttempted( false )
{
    GetOwningGameObject().SetComponent( this );
}

//-----------------------------------------------------------------------

Animator::~Animator()
{
    m_AnimationNameToIdxMap.clear();

    for(AnimationList::iterator it = m_AnimationList.begin(); it != m_AnimationList.end(); ++it)
    {
        AnimationManager::AnimationHandle& handle = *it;
        m_AnimationManager.ReleaseAnimation( handle );
    }

    m_AnimationList.clear();

    _DisconnectFromSkinnedMeshes();
}

//-----------------------------------------------------------------------

void Animator::SetEnabled(bool val)
{
    const bool wasEnabled = IsEnabled();

    Component::SetEnabled( val );

    if( val && !wasEnabled )
    {
        _ConnectWithSkinnedMeshes();
    }
    else if( !val && wasEnabled )
    {
        _DisconnectFromSkinnedMeshes();
    }
}

//-----------------------------------------------------------------------

bool Animator::AddAnimation(const std::string& animationName,
                             AnimationManager::AnimationHandle animationHandle)
{
    bool retVal = false;

    // Make sure the given animation is interchangeable with the animations already added
    if( ( m_AnimationList.size() == 0 ) ||
        m_AnimationManager.CheckIfAnimationsAreInterchangeable( animationHandle, m_AnimationList[m_AnimationList.size() - 1] ) )
    {
        m_AnimationList.push_back( animationHandle );
        m_AnimationNameToIdxMap[animationName] = ( m_AnimationList.size() - 1 );
        retVal = true;

        if( !m_ConnectionWithSkinnedMeshesAttempted )
        {
            _ConnectWithSkinnedMeshes();
            m_ConnectionWithSkinnedMeshesAttempted = true;
        }
    }

    return retVal;
}

//-----------------------------------------------------------------------

bool Animator::Play(const std::string& animationName, bool loop)
{
    bool retVal = false;

    AnimationNameToAnimationListIdxMap::iterator it = m_AnimationNameToIdxMap.find( animationName );
    if( it != m_AnimationNameToIdxMap.end() )
    {
        retVal = true;
        const size_t animationListIndex = it->second;

        assert( ( animationListIndex < m_AnimationList.size() ) && "Animation list index must not be bigger than the number of animations in the list" );

        AnimationManager::AnimationHandle* animationToUse = &m_AnimationList[animationListIndex];

        if( m_CurrentlyPlayingAnimation && m_AnimationManager.IsAnimationActive( *m_CurrentlyPlayingAnimation )   )
        {
            Stop(); // Make sure any previously set animation is not playing
        }

        m_AnimationManager.ActivateAnimation( *animationToUse, this, loop );
        m_CurrentlyPlayingAnimation = animationToUse;
    }

    return retVal;
}

//-----------------------------------------------------------------------

bool Animator::IsPlaying()
{
    return ( m_CurrentlyPlayingAnimation && m_AnimationManager.IsAnimationActive( *m_CurrentlyPlayingAnimation ) );
}

//-----------------------------------------------------------------------

void Animator::Stop()
{
    if( m_CurrentlyPlayingAnimation != nullptr )
    {
        m_AnimationManager.DeactivateAnimation( *m_CurrentlyPlayingAnimation );
        m_CurrentlyPlayingAnimation = nullptr;
    }
}

//-----------------------------------------------------------------------

void Animator::AnimatedSkeletonChanged(const EngineTypes::MatrixArray& animatedSkeleton)
{
    if( m_AnimationList.size() > 0 )
    {
        for(SkinnedMeshRendererList::iterator it = m_ConnectedSkinnedMeshRenderers.begin();
            it != m_ConnectedSkinnedMeshRenderers.end();
            ++it)
        {
            SkinnedMeshRenderer* renderer = *it;
            assert( renderer->m_MeshHandle.IsValid() && "Connected Skinned Mesh Renderer does not have a valid mesh" );
            assert( ( animatedSkeleton.size() == renderer->m_MeshHandle->GetBoneCount() ) && "The animated skeleton must have the same number of joints as the mesh" );
            assert( ( animatedSkeleton.size() == renderer->m_AnimatedBones.size() ) && "The number of animated bone matrices in the animation and the mesh must be the same" );

            if( m_CurrentlyPlayingAnimation != nullptr )
            {
                const EngineTypes::MatrixArray& meshInverseBindPose = renderer->m_MeshHandle->GetInverseBindPose();

                // Multiply the animated skeleton joints by the inverse of the mesh bind pose.
                for (size_t i = 0; i < animatedSkeleton.size(); ++i)
                {
                    renderer->m_AnimatedBones[i] = ( animatedSkeleton[i] * meshInverseBindPose[i] );
                }
            }
        }
    }
    else
    {
       throw GenericException( "Animator received an animated skeleton update even though there are no animations in this Animator" );
    }
}

//-----------------------------------------------------------------------

void Animator::_ConnectWithSkinnedMeshes()
{
    _DisconnectFromSkinnedMeshes();

    if( m_AnimationList.size() > 0 )
    {
        std::stack<GameObject*> gameObjects;
        gameObjects.push( &GetOwningGameObject() );

        do
        {
            GameObject* currentGameObject = gameObjects.top();
            gameObjects.pop();

            Animator* animator = currentGameObject->GetComponent<Animator>();
            if( ( animator == this ) || // If we are processing owning GameObject
                !animator )             // or a child GameObject without its own animator then try to connect
            {
                GameObject::ChildNodeList& childNodeList = currentGameObject->GetChildren();
                for(size_t i = 0; i < childNodeList.size(); i++)
                {
                    gameObjects.push( childNodeList[i] );
                }

                SkinnedMeshRenderer* skinnedMeshRenderer = currentGameObject->GetComponent<SkinnedMeshRenderer>();
                if( skinnedMeshRenderer && skinnedMeshRenderer->m_MeshHandle.IsValid() )
                {
                    if( skinnedMeshRenderer->m_MeshHandle->CheckIfAnimationIsCompatible( m_AnimationList[m_AnimationList.size() - 1] ) )
                    {
                        m_ConnectedSkinnedMeshRenderers.push_back( skinnedMeshRenderer );
                    }
                }
            }
        }
        while( !gameObjects.empty() );
    }
}

//-----------------------------------------------------------------------

void Animator::_DisconnectFromSkinnedMeshes()
{
    m_ConnectedSkinnedMeshRenderers.clear();
}

//-----------------------------------------------------------------------
