in vec3 in_VertexPos;
in vec2 in_VertexTextureCoords;


out vec2 TexCoord;

uniform mat4 u_ModelViewProjectionMatrix;

void main()
{
    gl_Position = u_ModelViewProjectionMatrix * vec4( in_VertexPos, 1.0f );


    TexCoord = in_VertexTextureCoords;
}
 
