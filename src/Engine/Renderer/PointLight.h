#ifndef POINTLIGHT_H_INCLUDED
#define POINTLIGHT_H_INCLUDED

#include "RenderingHelperMeshes.h"

class ShaderProgPointLightUberShader;
class ShaderProgLightingStencilPass;

class PointLight
{
public:
    PointLight(ShaderProgPointLightUberShader& shaderProg,
               ShaderProgLightingStencilPass& stencilShaderProg);

    inline void SetColor(const glm::vec3& color)
    {
        m_Color = color;
    }

    inline const glm::vec3& GetColor() const
    {
        return m_Color;
    }

    inline void SetAmbientIntensity(float intensity)
    {
        m_AmbientIntensity = intensity;
    }

    inline float GetAmbientIntensity() const
    {
        return m_AmbientIntensity;
    }

    inline void SetDiffuseIntensity(float intensity)
    {
        m_DiffuseIntensity = intensity;
    }

    inline float GetDiffuseIntensity() const
    {
        return m_DiffuseIntensity;
    }

    inline void SetRadius(float radius)
    {
        m_Radius = radius;
    }

    inline void SetPositionInWorldSpace(const glm::vec3& pos)
    {
        m_PositionInWorldSpace = pos;
    }

    inline float GetRadius() const
    {
        return m_Radius;
    }

    inline glm::vec3 GetPositionInWorldSpace() const
    {
        return m_PositionInWorldSpace;
    }

    void RenderForLightPass(const glm::mat4& viewMatrix,
                            const glm::mat4& projectionMatrix);

    void RenderForStencilPass(const glm::mat4& viewMatrix,
                              const glm::mat4& projectionMatrix);

    inline bool Enabled() const
    {
        return m_Enabled;
    }

    inline void SetEnabled(bool val)
    {
        m_Enabled = val;
    }

private:
    bool m_Enabled;
    glm::vec3 m_Color;
    float m_Radius;
    float m_AmbientIntensity;
    float m_DiffuseIntensity;
    glm::vec3 m_PositionInWorldSpace;
    PointLightInfluenceMesh m_InfluenceMesh;
    ShaderProgPointLightUberShader& m_ShaderProg;
    ShaderProgLightingStencilPass& m_StencilShaderProg;
};

#endif // POINTLIGHT_H_INCLUDED
