/**
*  Copyright (c) Gytis Sabaliauskas - All Rights Reserved
*
*  Copying of this file via any medium and redistribution and use
*  in source and binary forms, with or without modification, are not
*  permitted without hand written consent of the authors.
*
*  Proprietary and confidential.
*/

template<class ElementType>
List<ElementType>::List()
: m_RootNode(nullptr),
  m_LastNode(nullptr)
{
}

template<class ElementType>
List<ElementType>::~List()
{
    Clear();
}

template<class ElementType>
inline typename List<ElementType>::iterator List<ElementType>::begin() noexcept
{
    return iterator(m_RootNode);
}

template<class ElementType>
inline typename List<ElementType>::const_iterator List<ElementType>::begin() const noexcept
{
    return cbegin();
}

template<class ElementType>
inline typename List<ElementType>::iterator List<ElementType>::end() noexcept
{
    return iterator();
}

template<class ElementType>
inline typename List<ElementType>::const_iterator List<ElementType>::end() const noexcept
{
    return cend();
}

template<class ElementType>
inline typename List<ElementType>::const_iterator List<ElementType>::cbegin() const noexcept
{
    return const_iterator(m_RootNode);
}

template<class ElementType>
inline typename List<ElementType>::const_iterator List<ElementType>::cend() const noexcept
{
    return const_iterator();
}

template<class ElementType>
typename List<ElementType>::Node* List<ElementType>::PushBack(const ElementType& value)
{
    Node* newNode = new Node();
    newNode->privateData.nextNode = nullptr;
    newNode->value = value;

    if (m_RootNode == nullptr)
    {
        newNode->privateData.previousNode = nullptr;
        m_RootNode = newNode;
        m_LastNode = m_RootNode;
    }
    else
    {
        newNode->privateData.previousNode = m_LastNode;
        m_LastNode->privateData.nextNode = newNode;
        m_LastNode = newNode;
    }

    return newNode;
}

template<class ElementType>
void List<ElementType>::Erase(Node* node)
{
    if (node != nullptr)
    {
        Node* previousNode = node->privateData.previousNode;
        Node* nextNode = node->privateData.nextNode;

        if (previousNode != nullptr)
        {
            previousNode->privateData.nextNode = nextNode;
        }

        if (nextNode != nullptr)
        {
            nextNode->privateData.previousNode = previousNode;
        }

        if (node == m_RootNode)
        {
            m_RootNode = nextNode;
        }

        if (node == m_LastNode)
        {
            m_LastNode = previousNode;
        }

        delete node;
    }
}

template<class ElementType>
void List<ElementType>::Clear()
{
    Node* nextNode = m_RootNode;

    while( nextNode != nullptr )
    {
        Node* currentNode = nextNode;
        nextNode = currentNode->privateData.nextNode;
        delete currentNode;
    }
    
    m_RootNode = nullptr;
    m_LastNode = nullptr;
}