#ifndef AUDIODEVICE_CONTEXT_H_INCLUDED
#define AUDIODEVICE_CONTEXT_H_INCLUDED

#include <cstddef>
#include <glm/glm.hpp>

struct ALCcontext_struct;
typedef struct ALCcontext_struct ALCcontext;

class Camera;

namespace AudioDevice
{

class Device;

class Context
{
public:
    static const size_t c_MinimumEffectSlotsConnectionsPerSource = 4;
    static const float c_SpeedOfSoundInAir;
    static const float c_SpeedOfSoundInWater;

    Context(Device& device);
    ~Context();

    /**
     * Make this context currently active context
     */
    void Bind();

    /**
     * Make this context currently not active context
     */
    void Unbind();

    inline size_t GetNumberOfEffectsSlotsSupportedPerSource() const
    {
        return m_NumberOfAuxiliaryEffectsSlotsSupportedPerSource;
    }

    /**
     * Note: The value must never be negative
     */
    void SetMasterGain(float positiveGainValue);

    void SetListenerPosition(const glm::vec3& positionInWorldSpace);

    /**
     * Velocity vector. Velocity in meters per second for each of the three axes i.e. X, Y and Z
     */
    void SetListenersVelocity(const glm::vec3& velocityVector);

    /**
     * Set orientation vectors
     *
     * @param atVector - A vector pointing in the same direction as the camera
     * @param upVector - A vector pointing straight up from the top of the camera
     */
    void SetOrienstation(const glm::vec3& atVector, const glm::vec3& upVector);

    /**
     * Set orientation to be the same as the one of a given camera
     */
    void SetOrienstation(const Camera& cameraToGetTheOrientationFrom);

    float GetMasterGain();
    glm::vec3 GetListenerPosition();
    glm::vec3 GetListenersVelocity();
    void GetOrienstation(glm::vec3& outAtVector, glm::vec3& outUpVector);

    /**
     * Doppler factor is a simple scaling of source and listener
     * velocities to exaggerate or deemphasize the Doppler (pitch)
     * shift resulting from the calculation.
     *
     * Note: The value must not be negative
     */
    void SetGlobalDopplerFactor(float factor);

    /**
     * Speed of sound allows the application to change the reference
     * (propagation) speed used in the Doppler calculation.
     *
     * Note: The value has to be greater than zero
     */
    void SetSpeedOfSound(float speedInMetersPerSecond);

    float GetGlobalDopplerFactor();
    float GetSpeedOfSound();

private:
    static ALCcontext* m_CurrentlyActiveContext;

    // Used to prevent copying
    Context& operator = (const Context& other) = delete;
    Context(const Context& other) = delete;

    ALCcontext* m_ALContext;
    Device& m_Device;
    size_t m_NumberOfAuxiliaryEffectsSlotsSupportedPerSource;
};

}

#endif // AUDIODEVICE_CONTEXT_H_INCLUDED
