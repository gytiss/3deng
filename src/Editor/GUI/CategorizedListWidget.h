#ifndef CATEGORIZEDLISTWIDGET_H_INCLUDED
#define CATEGORIZEDLISTWIDGET_H_INCLUDED

#include <QtWidgets/QTreeWidget>
#include <vector>

class QTreeWidgetItem;

class CategorizedListWidget : public QTreeWidget
{
    Q_OBJECT

public:
    typedef QTreeWidgetItem* Category;

    explicit CategorizedListWidget(QWidget *parent = 0);
    ~CategorizedListWidget();

    /**
     * @brief AddCetegory
     * @param categoryTitle - Title of the categorywhich will be created
     *
     * @return Handle to the created category
     */
    Category AddCetegory(const QString& categoryTitle);

    /**
     * @brief AddCatedoryItem - Add a new item to a given category in the list
     * @param category - Handle of the category to add the item to
     * @param newItem - Item to add to the category
     */
    void AddCatedoryItem(Category category, QWidget* newItem);

    /**
     * @param category - Handle of a category to set the value for
     * @param value - Set to true if category must not be shown and to false otherwise
     */
    static void SetCategoryHide(Category category, float value);

private slots:
    void HandleMousePress(QTreeWidgetItem* item);

private:
    typedef std::vector<QTreeWidgetItem*> CategoryList;

    CategoryList m_CategoryList;
};

#endif // CATEGORIZEDLISTWIDGET_H_INCLUDED
