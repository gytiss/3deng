#ifndef GRAPHICSDEVICESTENCILBUFFER_H_INCLUDED
#define GRAPHICSDEVICESTENCILBUFFER_H_INCLUDED

#include <stack>
#include <OpenGLIncludes.h>

class GraphicsDeviceStencilBuffer
{
public:
    friend class GraphicsDevice;

    enum StencilFunction
    {
        e_StencilFunctionNever = GL_NEVER,
        e_StencilFunctionLess = GL_LESS,
        e_StencilFunctionLowerThanOrEqual = GL_LEQUAL,
        e_StencilFunctionGreater = GL_GREATER,
        e_StencilFunctionGreaterThanOrEqual = GL_GEQUAL,
        e_StencilFunctionEqual = GL_EQUAL,
        e_StencilFunctionNotEqual = GL_NOTEQUAL,
        e_StencilFunctionAlways = GL_ALWAYS
    };

    enum StencilState
    {
        e_StencilStateFront = GL_FRONT,
        e_StencilStateBack = GL_BACK,
        e_StencilStateFrontAndBack = GL_FRONT_AND_BACK
    };

    enum StencilAction
    {
        e_StencilActionKeep = GL_KEEP,
        e_StencilActionZero = GL_ZERO,
        e_StencilActionReplace = GL_REPLACE,
        e_StencilActionIncrement = GL_INCR,
        e_StencilActionIncrementWrap = GL_INCR_WRAP,
        e_StencilActionDecrement = GL_DECR,
        e_StencilActionDecrementWrap = GL_DECR_WRAP,
        e_StencilActionInvert = GL_INVERT
    };

    void SaveCurrentStencilFunctionState();
    void RestorePreviousStencilFunctionState();
    void SetStencilFunctionState(StencilFunction func, int referenceValue, unsigned int mask);

    void SaveCurrentFrontFaceStencilOperationState();
    void RestorePreviousFrontFaceStencilOperationState();
    void SetFrontFaceStencilOperationState(StencilAction stencilTestFails,
                                           StencilAction stencilTestPasesAndDepthTestFails,
                                           StencilAction stencilAndDepthTestPases);

    void SaveCurrentBackFaceStencilOperationState();
    void RestorePreviousBackFaceStencilOperationState();
    void SetBackFaceStencilOperationState(StencilAction stencilTestFails,
                                          StencilAction stencilTestPasesAndDepthTestFails,
                                          StencilAction stencilAndDepthTestPases);

    void SaveCurrentClearStencilValue();
    void RestorePreviousClearStencilValue();
    void SetClearStencilValue(int stencilClearValue);

private:
    GraphicsDeviceStencilBuffer();

    struct StencilFunctionState
    {
        StencilFunction func;
        int referenceValue;
        unsigned int mask;
    };

    struct StencilOperationState
    {
        StencilAction stencilTestFails;
        StencilAction stencilTestPasesAndDepthTestFails;
        StencilAction stencilAndDepthTestPases;
    };

    std::stack<StencilFunctionState> m_StencilFunctionStatesStack;
    StencilFunctionState m_CurrentStencilFunctionState;

    std::stack<StencilOperationState> m_FrontFaceStencilOperationStatesStack;
    StencilOperationState m_CurrentFrontFaceStencilOperation;

    std::stack<StencilOperationState> m_BackFaceStencilOperationStatesStack;
    StencilOperationState m_CurrentBackFaceStencilOperation;

    std::stack<int> m_ClearStencilValuesStack;
    int m_CurrentClearStencilValue;
};


#endif // GRAPHICSDEVICESTENCILBUFFER_H_INCLUDED
