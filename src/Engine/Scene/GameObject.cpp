#include "GameObject.h"
#include "MathUtils.h"
#include "Transform.h"
#include "RigidBody.h"
#include "Collider.h"
#include "Renderer.h"
#include "SoundSource.h"

const EngineTypes::AABB GameObject::c_DefaultUnitSizeBoundingBox = { glm::vec3( -1.0f, -1.0f, 1.0f ), glm::vec3( 1.0f, 1.0f, -1.0f ) };

//-----------------------------------------------------------------------

GameObject::GameObject(const std::string& name, uint32_t id)
: m_ID( id ),
  m_Name( name ),
  m_Parent( nullptr ),
  m_Children(),
  m_GlobalTransformMatrix( MathUtils::c_IdentityMatrix ),
  m_TransformMatrixCacheIsValid( false ),
  m_NoPhysicsUpdate( false ),
  m_Components(),
//  m_CustomProperties()
  m_Logics()
{
    memset( m_Components, 0x00, ArraySize( m_Components ) );

    // Every GameObject must have a transform component
    Transform* transformComponent = new Transform( *this, MathUtils::c_DefaultTransform );
    SetComponent( transformComponent );
}

//-----------------------------------------------------------------------

GameObject::~GameObject()
{
    m_Logics.clear();

    for(GameObject* child : m_Children)
    {
        delete child;
    }
}

//-----------------------------------------------------------------------

const EngineTypes::AABB& GameObject::GetAABB() const
{
    const EngineTypes::AABB* retVal = &c_DefaultUnitSizeBoundingBox;

    const Renderer* renderer = GetComponent<Renderer>();

    if( renderer )
    {
        retVal = &renderer->GetAABB();
    }

    return *retVal;
}

//-----------------------------------------------------------------------

void GameObject::AddChild(GameObject* childNode)
{
    if( childNode == nullptr )
    {
        throw GenericException( "Trying to add a null child to a scene graph node" );
    }
    else if( childNode->GetParent() )
    {
        throw GenericException( "Trying to add a child node that is already parented to some other node" );
    }
    else if( childNode == this )
    {
        throw GenericException( "Trying to add a ourselves as our child node" );
    }

    SceneTypes::Transform childGlobalTransform = childNode->GetTransform();

    childNode->m_Parent = this;
    m_Children.push_back( childNode );

    childNode->SetGlobalTransform( childGlobalTransform );
}

//-----------------------------------------------------------------------

void GameObject::SetGlobalTransform(const SceneTypes::Transform& t)
{
    if( m_Parent != nullptr )
    {
        glm::mat4 globalTransformMatrix = MathUtils::SceneTransformToMatrix( t );
        glm::mat4 parentInverseGlobalTransform = glm::inverse( m_Parent->GetGlobalTransformMatrix() );
        SceneTypes::Transform relativeTransform = MathUtils::MatrixToSceneTransform( parentInverseGlobalTransform * globalTransformMatrix );

        SetTransform( relativeTransform );
    }
    else
    {
        SetTransform( t );
    }
}

//-----------------------------------------------------------------------

void GameObject::SetTransform(const SceneTypes::Transform& t)
{
    Transform* currentTransform = GetComponent<Transform>();

    if( ( currentTransform->GetPosition() != t.position ) ||
        ( currentTransform->GetRotation() != t.rotation ) ||
        ( currentTransform->GetScale() != t.scale ) )
    {
        currentTransform->SetPosition( t.position );
        currentTransform->SetRotation( t.rotation );
        currentTransform->SetScale( t.scale );
        _InvalidateTransformMatrixCache();
        _UpdateTransformOfThePhysicsComponents();
        _UpdateTransformOfTheSoundSourceComponent();
    }
}

//-----------------------------------------------------------------------

const SceneTypes::Transform& GameObject::GetTransform() const
{
    return GetComponent<Transform>()->GetValue();
}

//-----------------------------------------------------------------------

const glm::mat4& GameObject::GetGlobalTransformMatrix() const
{
    if( !m_TransformMatrixCacheIsValid )
    {
        m_GlobalTransformMatrix = MathUtils::SceneTransformToMatrix( GetComponent<Transform>()->GetValue() );

        if( m_Parent )
        {
            m_GlobalTransformMatrix = ( m_Parent->GetGlobalTransformMatrix() * m_GlobalTransformMatrix );
        }

        m_TransformMatrixCacheIsValid = true;
    }

    return m_GlobalTransformMatrix;
}

//-----------------------------------------------------------------------

////-----------------------------------------------------------------------
//
//void GameObject::AddCustomProperty(SceneGraphNodeCustomProperty* property)
//{
//    if( property == nullptr )
//    {
//        throw GenericException( "Trying to add a null property to an entity node" );
//    }
//    else if( property->GetType() == SceneGraphNodeProperty::e_TypeRenderable )
//    {
//        throw GenericException( "Renderable property is not a custom property, please use SetRenderableProperty() to add it." );
//    }
//    else if( property->GetType() == SceneGraphNodeProperty::e_TypePhysics )
//    {
//        throw GenericException( "Physics property is not a custom property, please use SetPhysicsProperty() to add it." );
//    }
//
//    m_CustomProperties.push_back( property );
//}

//-----------------------------------------------------------------------

void GameObject::GetRenderablesOfTheHierarchy(RenderablesList& renderables)
{
    _GetRenderablesOfTheHierarchy( renderables, this );
}

//-----------------------------------------------------------------------

void GameObject::_Initialize()
{
    for(GameObjectLogicInterface* logic : m_Logics)
    {
        logic->_Initialize();
    }

    for(GameObject* child : m_Children)
    {
        child->_Initialize();
    }
}

//-----------------------------------------------------------------------

void GameObject::_FixedUpdate()
{
    for(GameObjectLogicInterface* logic : m_Logics)
    {
        logic->_FixedUpdate();
    }

    for(GameObject* child : m_Children)
    {
        child->_FixedUpdate();
    }
}

//-----------------------------------------------------------------------

void GameObject::_Update()
{
    for(GameObjectLogicInterface* logic : m_Logics)
    {
        logic->_Update();
    }

    for(GameObject* child : m_Children)
    {
        child->_Update();
    }
}

//-----------------------------------------------------------------------

void GameObject::_LastUpdate()
{
    for(GameObjectLogicInterface* logic : m_Logics)
    {
        logic->_LastUpdate();
    }

    for(GameObject* child : m_Children)
    {
        child->_LastUpdate();
    }
}

//-----------------------------------------------------------------------

void GameObject::_AddLogic(GameObjectLogicInterface* logic)
{
    if( logic )
    {
        m_Logics.push_back( logic );
    }
}

//-----------------------------------------------------------------------

void GameObject::_GetRenderablesOfTheHierarchy(RenderablesList& renderables,
                                               GameObject* gameObject)
{
    Renderer* renderer = gameObject->GetComponent<Renderer>();

    if( renderer != nullptr )
    {
        Renderer::RenderablesList& renderablesList = renderer->GetRenderables();

        for(RenderableObject& renderable : renderablesList)
        {
            renderable.SetWorldTransformMatrix( gameObject->GetGlobalTransformMatrix() );
            renderables.push_back( renderable );
        }
    }

    GameObject::ChildNodeList& children = gameObject->GetChildren();

    for(GameObject* child : children)
    {
        _GetRenderablesOfTheHierarchy( renderables, child );
    }
}

//-----------------------------------------------------------------------

void GameObject::_InvalidateTransformMatrixCache()
{
    m_TransformMatrixCacheIsValid = false;

    for(GameObject* child : m_Children)
    {
        child->_InvalidateTransformMatrixCache();
    }
}

//-----------------------------------------------------------------------

void GameObject::_UpdateTransformOfThePhysicsComponents()
{
    RigidBody* rigidBody = GetComponent<RigidBody>();
    Collider* collider = GetComponent<Collider>();

    if( rigidBody )
    {
        if( !m_NoPhysicsUpdate )
        {
            // We can only set position on kinematic rigid bodies and not dynamic ones
            const bool isNotKinematic = !rigidBody->IsKinematic();

            if( isNotKinematic )
            {
                rigidBody->SetIsKinematic( true );
            }

            rigidBody->SetSceneTransform( MathUtils::MatrixToSceneTransform( GetGlobalTransformMatrix() ) );

            if( isNotKinematic )
            {
                rigidBody->SetIsKinematic( false );
            }
        }
    }

    // Note that the change to the transform of the collider will only be visible if it is not attached to a rigid body
    if( collider )
    {
        collider->SetSceneTransform( MathUtils::MatrixToSceneTransform( GetGlobalTransformMatrix() ) );
    }
}

void GameObject::_UpdateTransformOfTheSoundSourceComponent()
{
    SoundSource* soundSource = GetComponent<SoundSource>();

    // Sound source component might not be attached
    if( soundSource )
    {
        soundSource->SetPosition( MathUtils::MatrixToSceneTransform( GetGlobalTransformMatrix() ).position );
    }
}

//-----------------------------------------------------------------------

void GameObject::_SetGlobalTransformWithoutUptadingPhysicsPropertyTransform(const SceneTypes::Transform& t)
{
    m_NoPhysicsUpdate = true;
    SetGlobalTransform( t );
    m_NoPhysicsUpdate = false;
}

//-----------------------------------------------------------------------

RigidBody* GameObject::_FindRigidBody()
{
    RigidBody* retVal = static_cast<RigidBody*>( m_Components[RigidBody::m_ComponentType] );

    if( !retVal && m_Parent )
    {
        retVal = m_Parent->_FindRigidBody();
    }

    return retVal;
}

//-----------------------------------------------------------------------

void GameObject::_FindColliders(std::vector<Collider*>& outColliders)
{
    Collider* collider = static_cast<Collider*>( m_Components[Collider::m_ComponentType] );

    if( collider )
    {
        outColliders.push_back( collider );
    }

    for(GameObject* child : m_Children)
    {
        child->_FindColliders( outColliders );
    }
}

//-----------------------------------------------------------------------
