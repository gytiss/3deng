#ifndef SHADERPROGUNIFORMS_INCLUDED
#define SHADERPROGUNIFORMS_INCLUDED

#include <GraphicsDevice/ShaderProgBaseUniforms.h>

class ShaderProgUniformViewMatrix : protected ShaderProgUniformFloatMat4
{
public:
	ShaderProgUniformViewMatrix(ShaderProg* shaderProg)
	: ShaderProgUniformFloatMat4( shaderProg, "u_ViewMatrix" )
	{
	}

	void SetUniformViewMatrix(const glm::mat4& val)
	{
        SetValue( val, false );
	}
};

//-----------------------------------------------------------------------

class ShaderProgUniformModelMatrix : protected ShaderProgUniformFloatMat4
{
public:
	ShaderProgUniformModelMatrix(ShaderProg* shaderProg)
	: ShaderProgUniformFloatMat4( shaderProg, "u_ModelMatrix" )
	{
	}

	void SetUniformModelMatrix(const glm::mat4& val)
	{
        SetValue( val, false );
	}
};

//-----------------------------------------------------------------------

class ShaderProgUniformProjectionMatrix : protected ShaderProgUniformFloatMat4
{
public:
	ShaderProgUniformProjectionMatrix(ShaderProg* shaderProg)
	: ShaderProgUniformFloatMat4( shaderProg, "u_ProjectionMatrix" )
	{
	}

	void SetUniformProjectionMatrix(const glm::mat4& val)
	{
        SetValue( val, false );
	}
};

//-----------------------------------------------------------------------

class ShaderProgUniformInverseProjectionMatrix : protected ShaderProgUniformFloatMat4
{
public:
	ShaderProgUniformInverseProjectionMatrix(ShaderProg* shaderProg)
	: ShaderProgUniformFloatMat4( shaderProg, "u_InverseProjectionMatrix" )
	{
	}

	void SetUniformInverseProjectionMatrix(const glm::mat4& val)
	{
        SetValue( val, false );
	}
};

//-----------------------------------------------------------------------

class ShaderProgUniformModelViewMatrix : protected ShaderProgUniformFloatMat4
{
public:
	ShaderProgUniformModelViewMatrix(ShaderProg* shaderProg)
	: ShaderProgUniformFloatMat4( shaderProg, "u_ModelViewMatrix" )
	{
	}

	void SetUniformModelViewMatrix(const glm::mat4& val)
	{
        SetValue( val, false );
	}
};

//-----------------------------------------------------------------------

class ShaderProgUniformModelViewProjectionMatrix : protected ShaderProgUniformFloatMat4
{
public:
	ShaderProgUniformModelViewProjectionMatrix(ShaderProg* shaderProg)
	: ShaderProgUniformFloatMat4( shaderProg, "u_ModelViewProjectionMatrix" )
	{
	}

	void SetUniformModelViewProjectionMatrix(const glm::mat4& val)
	{
        SetValue( val, false );
	}
};

//-----------------------------------------------------------------------

class ShaderProgUniformNormalMatrix : protected ShaderProgUniformFloatMat4
{
public:
	ShaderProgUniformNormalMatrix(ShaderProg* shaderProg)
	: ShaderProgUniformFloatMat4( shaderProg, "u_NormalMatrix" )
	{
	}

	void SetUniformNormalMatrix(const glm::mat4& val)
	{
        SetValue( val, false );
	}
};

//-----------------------------------------------------------------------

class ShaderProgUniformJointMatrices : protected ShaderProgUniformFloatMat4Array
{
public:
	ShaderProgUniformJointMatrices(ShaderProg* shaderProg)
	: ShaderProgUniformFloatMat4Array( shaderProg, "u_JointMatrices" )
	{
	}

	void SetUniformJointMatrices(size_t matrixCount, const glm::mat4* matrixArray)
	{
        SetValue(matrixCount, matrixArray, false);
	}
};

//-----------------------------------------------------------------------

class ShaderProgUniformLightPositionInViewSpace : protected ShaderProgUniformFloatVec3
{
public:
	ShaderProgUniformLightPositionInViewSpace(ShaderProg* shaderProg)
	: ShaderProgUniformFloatVec3( shaderProg, "u_LightPosition_ViewSpace" )
	{
	}

	void SetUniformLightPositionInViewSpace(const glm::vec3& val)
	{
        SetValue( val );
	}
};

//-----------------------------------------------------------------------

class ShaderProgUniformLightColor : protected ShaderProgUniformFloatVec3
{
public:
	ShaderProgUniformLightColor(ShaderProg* shaderProg)
	: ShaderProgUniformFloatVec3( shaderProg, "u_LightColor" )
	{
	}

	void SetUniformLightColor(const glm::vec3& val)
	{
        SetValue( val );
	}
};

//-----------------------------------------------------------------------

class ShaderProgUniformLightAmbientIntensity : protected ShaderProgUniformFloat
{
public:
	ShaderProgUniformLightAmbientIntensity(ShaderProg* shaderProg)
	: ShaderProgUniformFloat( shaderProg, "u_LightAmbientIntensity" )
	{
	}

	void SetUniformLightAmbientIntensity(float val)
	{
        SetValue( val );
	}
};

//-----------------------------------------------------------------------

class ShaderProgUniformLightDiffuseIntensity : protected ShaderProgUniformFloat
{
public:
	ShaderProgUniformLightDiffuseIntensity(ShaderProg* shaderProg)
	: ShaderProgUniformFloat( shaderProg, "u_LightDiffuseIntensity" )
	{
	}

	void SetUniformLightDiffuseIntensity(float val)
	{
        SetValue( val );
	}
};

//-----------------------------------------------------------------------

class ShaderProgUniformLightDirectionInViewSpace : protected ShaderProgUniformFloatVec3
{
public:
	ShaderProgUniformLightDirectionInViewSpace(ShaderProg* shaderProg)
	: ShaderProgUniformFloatVec3( shaderProg, "u_LightDirection_ViewSpace" )
	{
	}

	void SetUniformLightDirectionInViewSpace(const glm::vec3& val)
	{
        SetValue( val );
	}
};

//-----------------------------------------------------------------------

class ShaderProgUniformColor : protected ShaderProgUniformFloatVec3
{
public:
	ShaderProgUniformColor(ShaderProg* shaderProg)
	: ShaderProgUniformFloatVec3( shaderProg, "u_Color" )
	{
	}

	void SetUniformColor(const glm::vec3& val)
	{
        SetValue( val );
	}
};

//-----------------------------------------------------------------------

class ShaderProgUniformUseVertexColors : protected ShaderProgUniformUnsignedInt
{
public:
	ShaderProgUniformUseVertexColors(ShaderProg* shaderProg)
	: ShaderProgUniformUnsignedInt( shaderProg, "u_UseVertexColors" )
	{
	}

	void SetUniformUseVertexColors(const unsigned int val)
	{
        SetValue( val );
	}
};

//-----------------------------------------------------------------------

class ShaderProgUniformScreenSize : protected ShaderProgUniformFloatVec2
{
public:
	ShaderProgUniformScreenSize(ShaderProg* shaderProg)
	: ShaderProgUniformFloatVec2( shaderProg, "u_ScreenSize" )
	{
	}

	void SetUniformScreenSize(const glm::vec2& val)
	{
        SetValue( val );
	}
};

//-----------------------------------------------------------------------

class ShaderProgUniformLightCutoffValue : protected ShaderProgUniformFloat
{
public:
	ShaderProgUniformLightCutoffValue(ShaderProg* shaderProg)
	: ShaderProgUniformFloat( shaderProg, "u_LightCutoffValue" )
	{
	}

	void SetUniformLightCutoffValue(float val)
	{
        SetValue( val );
	}
};

//-----------------------------------------------------------------------

class ShaderProgUniformLightRadius : protected ShaderProgUniformFloat
{
public:
	ShaderProgUniformLightRadius(ShaderProg* shaderProg)
	: ShaderProgUniformFloat( shaderProg, "u_LightRadius" )
	{
	}

	void SetUniformLightRadius(float val)
	{
        SetValue( val );
	}
};

//-----------------------------------------------------------------------

class ShaderProgUniformProjectionA : protected ShaderProgUniformFloat
{
public:
	ShaderProgUniformProjectionA(ShaderProg* shaderProg)
	: ShaderProgUniformFloat( shaderProg, "u_ProjectionA" )
	{
	}

	void SetUniformProjectionA(float val)
	{
        SetValue( val );
	}
};

//-----------------------------------------------------------------------

class ShaderProgUniformProjectionB: protected ShaderProgUniformFloat
{
public:
	ShaderProgUniformProjectionB(ShaderProg* shaderProg)
	: ShaderProgUniformFloat( shaderProg, "u_ProjectionB" )
	{
	}

	void SetUniformProjectionB(float val)
	{
        SetValue( val );
	}
};

//-----------------------------------------------------------------------

/////////////////////////////////////////////////////////////////////////
//                       Forward Renderer Uniforms                     //
/////////////////////////////////////////////////////////////////////////

//-----------------------------------------------------------------------

class ShaderProgUniformUseTexture : protected ShaderProgUniformUnsignedInt
{
public:
	ShaderProgUniformUseTexture(ShaderProg* shaderProg)
	: ShaderProgUniformUnsignedInt( shaderProg, "u_UseTexture" )
	{
	}

	void SetUniformUseTexture(const unsigned int val)
	{
        SetValue( val );
	}
};

//-----------------------------------------------------------------------


#endif // SHADERPROGUNIFORMS_INCLUDED
