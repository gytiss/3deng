#ifndef MESH_H_INCLUDED
#define MESH_H_INCLUDED

#include <Resources/AnimationManager.h>
#include <GraphicsDevice/VAO.h>
#include <MObjModel/MObj.h>
#include <EngineTypes.h>
#include <vector>

class Mesh
{
public:
    friend class MeshManager;

    struct SubMesh
    {
        SubMesh()
        : m_FirstIndex( 0 ),
          m_IndexCount( 0 ),
          m_MinVertexIndexValue( 0 ),
          m_MaxVertexIndexValue( 0 ),
          m_InitialMaterialPath()
        {
        }

        size_t m_FirstIndex;
        size_t m_IndexCount;
        EngineTypes::VertexIndexType m_MinVertexIndexValue;
        EngineTypes::VertexIndexType m_MaxVertexIndexValue;
        std::string m_InitialMaterialPath;
    };

    typedef std::vector<SubMesh> SubMeshList;

    Mesh(const std::string& name,
         VAO* vao,
         size_t indexCount,
         size_t vertexCount,
         const EngineTypes::AABB& boundingBox,
         const SubMeshList& subMeshes);

    Mesh(const std::string& name,
         VAO* vao,
         size_t indexCount,
         size_t vertexCount,
         const EngineTypes::AABB& boundingBox,
         const SubMeshList& subMeshes,
         size_t boneCount,
         const EngineTypes::MatrixArray& inverseBindPose,
         uint8_t animationCompatibilityHash[MObj::c_MD5HashLength]);

    ~Mesh();

    /**
     * Check if a given animation if compatible with this mesh
     */
    bool CheckIfAnimationIsCompatible(const AnimationManager::AnimationHandle& handle) const;

    bool IsSkinned() const;

    /**
     * @return true if has a copy of mesh data in memory accessible by the CPU, false otherwise
     */
    bool HasMeshDataOnCPUReadableMemory() const;

    const std::string& GetName() const;

    VAO* GetVAO();

    size_t GetIndexCount() const;
    size_t GetVertexCount() const;

    /**
     * @return Non zero value only for skinned meshes
     */
    size_t GetBoneCount() const;

    const EngineTypes::AABB& GetAABB() const;

    const SubMeshList& GetSubMeshes() const;

    /**
     * @return Non empty array only for skinned meshes
     */
    const EngineTypes::MatrixArray& GetInverseBindPose();

    /**
     * @return nullptr if (HasMeshDataOnCPUReadableMemory() == false)
     */
    const EngineTypes::VertexIndexType* GetIndexDataBase() const;

    /**
     * @return nullptr if (HasMeshDataOnCPUReadableMemory() == false)
     */
    const float* GetVertexDataBase() const;

    /**
     * @return zero if (HasMeshDataOnCPUReadableMemory() == false)
     */
    unsigned int GetVertexDataStride() const;

private:
    void _SetMeshDataOnCPUReadableMemory(const EngineTypes::VertexIndexType* indexDataBase,
                                         const float* vertexDataBase,
                                         unsigned int vertexDataString);

    // Used to prevent copying
    Mesh& operator = (const Mesh& other);
    Mesh(const Mesh& other);

    const EngineTypes::VertexIndexType* m_IndexDataBase;
    const float* m_VertexDataBase;
    unsigned int m_VertexDataStride;
    std::string m_Name;
    VAO* m_VAO;
    const size_t m_IndexCount;
    const size_t m_VertexCount;
    const size_t m_BoneCount;
    const EngineTypes::AABB m_BoundingBox;
    const SubMeshList m_SubMeshes;
    EngineTypes::MatrixArray m_InverseBindPose;
    uint8_t m_AnimationCompatibilityHash[MObj::c_MD5HashLength];
};

#endif // MESH_H_INCLUDED
