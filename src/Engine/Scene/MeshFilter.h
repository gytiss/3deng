#ifndef MESHFILTER_H_INCLUDED
#define MESHFILTER_H_INCLUDED

#include "Component.h"
#include <Resources/MeshManager.h>

class MeshFilter : public Component
{
    GAME_OBJECT_COMPONENT_TYPE(e_TypeMeshFilter);

public:
    friend class MeshRenderer;
    friend class MeshCollider;

    MeshFilter(GameObject& owningGameObject, MeshManager& meshManager);

    ~MeshFilter();

    void SetEnabled(bool val) override;

    /**
     * @param meshHandle - Handle of the mesh this renderer will be using (Note: Mesh renderer takes ownership of this mesh handle)
     */
    void SetMesh(MeshManager::MeshHandle meshHandle);

private:
    MeshManager& m_MeshManager;
    MeshManager::MeshHandle m_MeshHandle;
};

#endif // MESHFILTER_H_INCLUDED
