#include "VBO.h"
#include <Exceptions/GenericException.h>
#include <cstring>

//-----------------------------------------------------------------------

const char* VBO::m_AttrIdxToAttrShaderVarNameMap[e_VertexAttrIdxCount] =
{
    "in_VertexPos",
    "in_VertexNormal",
    "in_VertexBiTangent",
    "in_VertexTangent",
    "in_VertexTextureCoords",
    "in_VertexBoneWeights",
    "in_VertexBoneIndices",
    "in_VertexColor"
};

//-----------------------------------------------------------------------

const GLenum VBO::m_VertexAttributeComponentTypeToOpenGLDataTypeMap[e_VertexAttrComponentTypeCount] =
{
    GL_FLOAT,
    GL_INT,
    GL_UNSIGNED_INT,
    GL_SHORT,
    GL_UNSIGNED_SHORT,
    GL_BYTE,
    GL_UNSIGNED_BYTE
};

//-----------------------------------------------------------------------

VBO::VBO(size_t vertexStructSize, DataBufferUsagePattern usagePatter)
: m_DataBufUsagePatter( usagePatter ),
  m_VertexAttributes( 0 ),
  m_Id( 0 ),
  m_VertexSize( vertexStructSize )
{
    memset( m_AttributeDescriptions, 0x00, e_VertexAttrIdxCount * sizeof( m_AttributeDescriptions[0] ) );

    glGenBuffers( 1, &m_Id ); // Claim one buffer ID from OpenGL
    CheckForGLErrors();
}

//-----------------------------------------------------------------------

VBO::~VBO()
{
    Unbind();
    glDeleteBuffers( 1, &m_Id ); // Relinquish buffer ID to OpenGL
    CheckForGLErrors();
    m_Id = 0;
}

//-----------------------------------------------------------------------

void VBO::BindData(size_t sizeInBytes, const void* dataPtr)
{
    Bind();

    GLenum bufferUsagePatter = GL_STATIC_DRAW;
    if( m_DataBufUsagePatter == e_DataBufferUsagePatternDynamic )
    {
        bufferUsagePatter = GL_DYNAMIC_DRAW;
    }

    glBufferData ( GL_ARRAY_BUFFER, sizeInBytes, dataPtr, bufferUsagePatter );
    CheckForGLErrors();
}

//-----------------------------------------------------------------------

void VBO::AddVertexAttribute(VertexAttrIdx attrIdx,
                             VertexAttrComponentType componentType,
                             VertexAttrType attrType,
                             size_t attrOffset)
{
    m_VertexAttributes.SetBit( attrIdx );

    m_AttributeDescriptions[attrIdx].type = attrType;
    m_AttributeDescriptions[attrIdx].componentType = componentType;
    m_AttributeDescriptions[attrIdx].offsetInVertexStruct = attrOffset;
}

//-----------------------------------------------------------------------
