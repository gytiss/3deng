#include "OpenGLErrorChecking.h"
#include <OpenGLIncludes.h>
#include <MsgLogger.h>
#include <StringUtils.h>
#include <iostream>
#include <cstdlib>

void _CheckForGLErrors_(const std::string& fileName, int lineNum)
{
    bool errorFound = false;
    std::string errMsg = "";

    GLenum errorCode = glGetError();
    while( errorCode != GL_NO_ERROR )
    {
        switch( errorCode )
        {
            case GL_NO_ERROR:
                break;
            case GL_INVALID_ENUM:
                errMsg = "GL_INVALID_ENUM";
                break;
            case GL_INVALID_VALUE:
                errMsg = "GL_INVALID_VALUE";
                break;
            case GL_INVALID_OPERATION:
                errMsg = "GL_INVALID_OPERATION";
                break;
            case GL_INVALID_FRAMEBUFFER_OPERATION:
                errMsg = "GL_INVALID_FRAMEBUFFER_OPERATION";
                break;
            case GL_OUT_OF_MEMORY:
                errMsg = "GL_OUT_OF_MEMORY";
                break;
#ifndef OPENGL_ES
            case GL_STACK_UNDERFLOW:
                errMsg = "GL_STACK_UNDERFLOW";
                break;
            case GL_STACK_OVERFLOW:
                errMsg = "GL_STACK_OVERFLOW";
                break;
#endif
            default:
                errMsg = "Unknown OpenGL error received";
                break;
        }

        std::string errorString = "\"";
        errorString += fileName;
        errorString += "\"(";
        errorString += StringUitls::NumToString( lineNum );
        errorString += ": ";
        errorString += errMsg;

        MsgLogger::LogError( errorString );

        errorCode = glGetError();
        errorFound = true;
    }

    if( errorFound )
    {
        exit( EXIT_FAILURE );
    }
}
