#include "EditorLogic.h"

#include "MsgLogger.h"
#include "StringUtils.h"

#include <MainWindow.h>
#include <GUI/TransformComponentWidget.h>
#include <GUI/RigidBodyComponentWidget.h>
#include <GUI/CapsuleColliderComponentWidget.h>
#include <GUI/ConeColliderComponentWidget.h>
#include <GUI/BoxColliderComponentWidget.h>
#include <GUI/SphereColliderComponentWidget.h>

#include <ui_MainWindow.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <MathUtils.h>
#include <EngineDefaults.h>
#include <Exceptions/GenericException.h>
#include <Scene/GameObject.h>
#include <Renderer/PointLight.h>
#include <Renderer/SpotLight.h>
#include <Renderer/DirectionalLight.h>
#include <Renderer/LightManager.h>
#include <Resources/MeshManager.h>
#include <Resources/TextureManager.h>
#include <Resources/MaterialManager.h>
#include <Physics/PhysicsManager.h>
#include <Resources/Audio/AudioDataManager.h>
#include <Resources/Audio/SoundManager.h>
#include <Scene/GameObject.h>
#include <Scene/Renderer.h>
#include <Scene/MeshRenderer.h>
#include <Scene/SkinnedMeshRenderer.h>
#include <Scene/Animator.h>
#include <Scene/Transform.h>
#include <Scene/CapsuleCollider.h>
#include <Scene/ConeCollider.h>
#include <Scene/BoxCollider.h>
#include <Scene/SphereCollider.h>
#include <Scene/MeshCollider.h>
#include <Scene/MeshFilter.h>

#include <Engine/SpotLightMarker.h>
#include <Engine/PointLighMarker.h>

//-----------------------------------------------------------------------

EditorLogic::EditorLogic(MainWindow& mainWindow,
                         GraphicsDevice& graphicsDevice,
                         FileManager& fileManager,
                         AnimationManager& animationManager,
                         MeshManager& meshManager,
                         TextureManager& textureManager,
                         MaterialManager& materialManager,
                         RenderManager& renderManager,
                         PhysicsManager& physicsManager,
                         AudioDataManager& audioDataManager,
                         SoundManager& soundManager,
                         SystemInterface& systemInterface)
: m_SystemInterface( systemInterface ),
    m_DrawableSurfaceSize( m_SystemInterface.GetDrawableSurfaceSize() ),
    m_CurrentLightNum( 0 ),
    m_PointLights(),
    m_SpotLights(),
    m_DirectionalLight( nullptr ),
    m_CurrentEntityGroup( e_EntityGroupPointLights ),
    m_EntityManipType( e_EntityManipTypeMove ),
    m_Camera( nullptr ),
    m_OtherEntitiesKeyVector(),
    m_CurrentAbsoluteMouseCoords( 0.0f, 0.0f ),
    m_PreviousAbsoluteMouseCoords( 0.0f, 0.0f ),
    m_RenderDebugData( false ),
    m_KeysAreControlingEntities( false ),
    m_AppIsStillRunning( true ),
    m_ClickedWithMouse( false ),
    m_MetaKeyIsPressed( false ),
    m_MouseLeftButtonIsPressed( false ),
    m_MouseMiddleButtonIsPressed( false ),
    m_MouseRightButtonIsPressed( false ),
    m_CurrentFrameTimeInMs( 0.0f ),
    m_CurrentPointLight( nullptr ),
    m_CurrentSpotLight( nullptr ),
    m_MeshManager( meshManager ),
    m_TextureManager( textureManager ),
    m_MaterialManager( materialManager ),
    m_AnimationManager( animationManager ),
    m_RenderManager( renderManager ),
    m_PhysicsManager( physicsManager ),
    m_AudioDataManager( audioDataManager ),
    m_SoundManager( soundManager ),
    m_DrawableText( graphicsDevice, m_SystemInterface, fileManager,"Data/Fonts/Test.ttf", 16 ),
    m_GameObjectList(),
    m_CurrentlySelectedGameObject( nullptr ),
    m_ManipulatorTool( m_MeshManager, m_MaterialManager, m_TextureManager ),
    m_MainWindow( mainWindow ),
    m_ToolsPanel( m_MainWindow, m_ManipulatorTool ),
    m_TransformComponentWidget( new TransformComponentWidget( m_ManipulatorTool,
                                                              m_MainWindow.m_ComponentPanel,
                                                              m_MainWindow.m_ComponentPanel ) ),
    m_RigidBodyComponentWidget( new RigidBodyComponentWidget( m_MainWindow.m_ComponentPanel,
                                                              m_MainWindow.m_ComponentPanel ) ),
    m_CapsuleColliderComponentWidget( new CapsuleColliderComponentWidget( m_MainWindow.m_ComponentPanel,
                                                                          m_MainWindow.m_ComponentPanel ) ),
    m_ConeColliderComponentWidget( new ConeColliderComponentWidget( m_MainWindow.m_ComponentPanel,
                                                                    m_MainWindow.m_ComponentPanel ) ),
    m_BoxColliderComponentWidget( new BoxColliderComponentWidget( m_MainWindow.m_ComponentPanel,
                                                                  m_MainWindow.m_ComponentPanel ) ),
    m_SphereColliderComponentWidget( new SphereColliderComponentWidget( m_MainWindow.m_ComponentPanel,
                                                                        m_MainWindow.m_ComponentPanel ) )
{
    m_SystemInterface.RegisterDrawableSurfaceSizeChangedCallback( _DrawableSurfaceSizeChaged, this );

    memset(m_OtherEntitiesKeyVector, 0x00, sizeof(bool) * e_KeyPressedLastEnum);

    memset( m_PointLights, 0x00, ArraySize( m_PointLights ) );
    memset( m_SpotLights, 0x00, ArraySize( m_SpotLights ) );

    m_PointLights[0] = m_RenderManager.GetLightManager().CreateNewPointLight();
    m_PointLights[1] = m_RenderManager.GetLightManager().CreateNewPointLight();
    m_PointLights[2] = m_RenderManager.GetLightManager().CreateNewPointLight();
    m_PointLights[3] = m_RenderManager.GetLightManager().CreateNewPointLight();
    m_PointLights[4] = m_RenderManager.GetLightManager().CreateNewPointLight();

    m_PointLights[0]->SetEnabled(false);
    m_PointLights[1]->SetEnabled(false);
    m_PointLights[2]->SetEnabled(false);
    m_PointLights[3]->SetEnabled(false);
    m_PointLights[4]->SetEnabled(false);

    m_SpotLights[0] = m_RenderManager.GetLightManager().CreateNewSpotLight();
    m_SpotLights[1] = m_RenderManager.GetLightManager().CreateNewSpotLight();
    m_SpotLights[2] = m_RenderManager.GetLightManager().CreateNewSpotLight();
    m_SpotLights[3] = m_RenderManager.GetLightManager().CreateNewSpotLight();
    m_SpotLights[4] = m_RenderManager.GetLightManager().CreateNewSpotLight();

    m_SpotLights[0]->SetEnabled(false);
    m_SpotLights[1]->SetEnabled(false);
    m_SpotLights[2]->SetEnabled(false);
    m_SpotLights[3]->SetEnabled(false);
    m_SpotLights[4]->SetEnabled(false);

    m_DirectionalLight = m_RenderManager.GetLightManager().GetMainDirectionalLight();

    m_CurrentPointLight = m_PointLights[m_CurrentLightNum];
    m_CurrentSpotLight = m_SpotLights[m_CurrentLightNum];

    _InitLights();

    static const float c_CameraSpeedInMetersPerSecond = ( Units::OneMeter * 1.0f );
    m_Camera = new Camera( m_DrawableSurfaceSize, c_CameraSpeedInMetersPerSecond );
    m_Camera->ForcePosition( 0.0f, 0.5f, 0.0f );
    m_Camera->ForceOrientation( 90.0f, -40.0f );

    _BuildScene();

    _SetupGUI();
}

//-----------------------------------------------------------------------

void EditorLogic::_SetupGUI()
{
    m_TransformComponentWidget->ResetPaletteToDefault();
    m_RigidBodyComponentWidget->ResetPaletteToDefault();
    m_CapsuleColliderComponentWidget->ResetPaletteToDefault();
    m_ConeColliderComponentWidget->ResetPaletteToDefault();
    m_BoxColliderComponentWidget->ResetPaletteToDefault();
    m_SphereColliderComponentWidget->ResetPaletteToDefault();
}

//-----------------------------------------------------------------------

void EditorLogic::_HideAllGUIWithGameObjectData()
{
    m_TransformComponentWidget->SetHidden( true );
    m_RigidBodyComponentWidget->SetHidden( true );
    m_CapsuleColliderComponentWidget->SetHidden( true );
    m_ConeColliderComponentWidget->SetHidden( true );
    m_BoxColliderComponentWidget->SetHidden( true );
    m_SphereColliderComponentWidget->SetHidden( true );
}

//-----------------------------------------------------------------------

void EditorLogic::_PopulateGUIWithGameObjectData(GameObject& gameObject)
{
    _HideAllGUIWithGameObjectData();

    Transform* transformComponent = gameObject.GetComponent<Transform>();
    if( transformComponent )
    {
        m_TransformComponentWidget->SetTransformComponent( transformComponent );
        m_TransformComponentWidget->SetHidden( false );
    }

    RigidBody* rigidBody = gameObject.GetComponent<RigidBody>();
    if( rigidBody )
    {
        m_RigidBodyComponentWidget->SetRigidBodyComponent( rigidBody );
        m_RigidBodyComponentWidget->SetHidden( false );
    }

    CapsuleCollider* capsuleCollider = gameObject.GetComponent<CapsuleCollider>();
    if( capsuleCollider )
    {
        m_CapsuleColliderComponentWidget->SetCapsuleColliderComponent( capsuleCollider );
        m_CapsuleColliderComponentWidget->SetHidden( false );
    }

    ConeCollider* coneCollider = gameObject.GetComponent<ConeCollider>();
    if( coneCollider )
    {
        m_ConeColliderComponentWidget->SetConeColliderComponent( coneCollider );
        m_ConeColliderComponentWidget->SetHidden( false );
    }

    BoxCollider* boxCollider = gameObject.GetComponent<BoxCollider>();
    if( boxCollider )
    {
        m_BoxColliderComponentWidget->SetBoxColliderComponent( boxCollider );
        m_BoxColliderComponentWidget->SetHidden( false );
    }

    SphereCollider* sphereCollider = gameObject.GetComponent<SphereCollider>();
    if( sphereCollider )
    {
        m_SphereColliderComponentWidget->SetSphereColliderComponent( sphereCollider );
        m_SphereColliderComponentWidget->SetHidden( false );
    }
}

//-----------------------------------------------------------------------

void EditorLogic::_InitLights()
{
    for(size_t i = 0; i < ArraySize( m_PointLights ); i++)
    {
        m_PointLights[i]->SetAmbientIntensity( 0.1f );
        m_PointLights[i]->SetDiffuseIntensity( 1.0f );
        m_PointLights[i]->SetRadius( 3.0f );
        m_PointLights[i]->SetEnabled( false );
    }

    const glm::vec3 c_RedColor = glm::vec3( 1.0f, 0.0f, 0.0f );
    const glm::vec3 c_GreenColor = glm::vec3( 0.0f, 1.0f, 0.0 );
    const glm::vec3 c_BlueColor = glm::vec3( 0.0f, 0.0f, 1.0f );
    const glm::vec3 c_GoldColor = glm::vec3( 1.0f, 0.843137254902f, 0.0f );
    const glm::vec3 c_WhiteColor = glm::vec3( 1.0f, 1.0f, 1.0f );

    //Y = 2.31428f (Lights center is on the plane)

    m_PointLights[0]->SetColor( c_WhiteColor );
    m_PointLights[0]->SetPositionInWorldSpace( glm::vec3( -2.05717f, 0.885708f, 0.0570843f ) );
    m_PointLights[0]->SetEnabled( true );

    m_PointLights[1]->SetColor( c_GreenColor );
    m_PointLights[1]->SetPositionInWorldSpace( glm::vec3( 4.65718f, 3.57143f , 20.6288f ) );

    m_PointLights[2]->SetColor( c_BlueColor );
    m_PointLights[2]->SetPositionInWorldSpace( glm::vec3( 8.0f, 3.57143f , 8.0f ) );

    m_PointLights[3]->SetColor( c_GoldColor );
    m_PointLights[3]->SetPositionInWorldSpace( glm::vec3( 9.17146f, 3.57143f , 16.1859f ) );

    m_PointLights[4]->SetColor( c_WhiteColor );
    m_PointLights[4]->SetPositionInWorldSpace( glm::vec3( 14.6573f, 3.57143f , 22.0574f ) );

    const glm::vec3 c_DownwardsDirection = glm::vec3( 0.0f, -1.0f, 0.0f );

    for(size_t i = 0; i < 1/*ArraySize( m_SpotLights )*/; i++)
    {
        m_SpotLights[i]->SetAmbientIntensity( 0.1f );
        m_SpotLights[i]->SetDiffuseIntensity( 1.0f );
        m_SpotLights[i]->SetRadius( 3.0f );
        m_SpotLights[i]->SetCutoffAngleInDegrees( 20.0f );
        m_SpotLights[i]->SetDirectionInWorldSpace( c_DownwardsDirection );
        m_SpotLights[i]->SetEnabled( true );
    }

    m_SpotLights[0]->SetColor( c_RedColor );
    m_SpotLights[0]->SetPositionInWorldSpace( glm::vec3( 1.199f, 1.985f, -4.028f ) );
}

//-----------------------------------------------------------------------

EditorLogic::~EditorLogic()
{
    m_SystemInterface.DeregisterDrawableSurfaceSizeChangedCallback( _DrawableSurfaceSizeChaged, this );
    SafeDelete( m_Camera );
}

//-----------------------------------------------------------------------

void EditorLogic::_InputCallback(InputMapping::MappedInput& inputs,
                                 float frameTimeInSeconds)
{
    /*------------------------------- Mouse Input ----------------------------*/
    if( inputs.FindAndConsumeState( InputMapping::e_StateLeftMouseButtonIsDown ) )
    {
        m_MouseLeftButtonIsPressed = true;
    }
    else
    {
        m_MouseLeftButtonIsPressed = false;
    }

    if( inputs.FindAndConsumeState( InputMapping::e_StateMiddleMouseButtonIsDown ) )
    {
        m_MouseMiddleButtonIsPressed = true;
    }
    else
    {
        m_MouseMiddleButtonIsPressed = false;
    }

    if( inputs.FindAndConsumeState( InputMapping::e_StateRightMouseButtonIsDown ) )
    {
        m_MouseRightButtonIsPressed = true;
    }
    else
    {
        m_MouseRightButtonIsPressed = false;
    }

    if( inputs.ranges.find( InputMapping::e_RangePointerAbsoluteX ) != inputs.ranges.end() )
    {
        m_CurrentAbsoluteMouseCoords.x = inputs.ranges[InputMapping::e_RangePointerAbsoluteX];
    }

    if( inputs.ranges.find( InputMapping::e_RangePointerAbsoluteY ) != inputs.ranges.end() )
    {
        m_CurrentAbsoluteMouseCoords.y = inputs.ranges[InputMapping::e_RangePointerAbsoluteY];
    }

    if( inputs.ranges.find( InputMapping::e_RangeMouseWheelVertical ) != inputs.ranges.end() )
    {
        const float scrollValueAndDirection = inputs.ranges[InputMapping::e_RangeMouseWheelVertical];
        m_Camera->MoveCameraAFixedNumberOfUnits( ( ( -1.0f * scrollValueAndDirection ) * 0.25f ), 0.0f );
    }

    _HandleMouseInput( frameTimeInSeconds );
    /*------------------------------------------------------------------------*/

    if( inputs.FindAndConsumeAction( InputMapping::e_ActionToggleWireframeMode )  )
    {
        m_RenderManager.SetRenderInWireframeMode( !m_RenderManager.GetRenderInWireframeMode() );
    }

    if( inputs.FindAndConsumeAction( InputMapping::e_ActionToggleDebugDataRendering ) )
    {
        m_RenderDebugData = !m_RenderDebugData;
    }

    if( inputs.FindAndConsumeAction( InputMapping::e_ActionClickWithMouse ) )
    {
        m_ClickedWithMouse = true;
    }
    else
    {
        m_ClickedWithMouse = false;
    }

    if( inputs.FindAndConsumeState( InputMapping::e_StateLeftMetaKeyIsDown ) )
    {
        m_MetaKeyIsPressed = true;
    }
    else
    {
        m_MetaKeyIsPressed = false;
    }

    if( m_KeysAreControlingEntities )
    {
        m_OtherEntitiesKeyVector[EditorLogic::e_KeyPressedW] = inputs.FindAndConsumeState( InputMapping::e_StateMoveForward );
        m_OtherEntitiesKeyVector[EditorLogic::e_KeyPressedS] = inputs.FindAndConsumeState( InputMapping::e_StateMoveBackwards );
        m_OtherEntitiesKeyVector[EditorLogic::e_KeyPressedA] = inputs.FindAndConsumeState( InputMapping::e_StateMoveLeft );
        m_OtherEntitiesKeyVector[EditorLogic::e_KeyPressedD] = inputs.FindAndConsumeState( InputMapping::e_StateMoveRight );
        m_OtherEntitiesKeyVector[EditorLogic::e_KeyPressedPageUp] = inputs.FindAndConsumeState( InputMapping::e_StateMoveUp );
        m_OtherEntitiesKeyVector[EditorLogic::e_KeyPressedPageDown] = inputs.FindAndConsumeState( InputMapping::e_StateMoveDown );
    }
    else
    {
        if( inputs.FindAndConsumeState( InputMapping::e_StateMoveFast ) )
        {
            m_Camera->SetSpeedMultiplier( 4.0f );
        }
        else
        {
            m_Camera->SetSpeedMultiplier( 1.0f );
        }

        m_Camera->RecalculatePosition( frameTimeInSeconds,
                                       inputs.FindAndConsumeState( InputMapping::e_StateMoveForward ),
                                       inputs.FindAndConsumeState( InputMapping::e_StateMoveBackwards ),
                                       inputs.FindAndConsumeState( InputMapping::e_StateMoveLeft ),
                                       inputs.FindAndConsumeState( InputMapping::e_StateMoveRight ) );
    }

    if( inputs.FindAndConsumeAction( InputMapping::e_ActionToggleForwardDeferredRendering ) )
    {
        if( m_RenderManager.GetRenderingPath() == RenderManager::e_RenderingPathForward )
        {
            m_RenderManager.SetRenderingPath( RenderManager::e_RenderingPathDeferred );
        }
        else if( m_RenderManager.GetRenderingPath() == RenderManager::e_RenderingPathDeferred )
        {
            m_RenderManager.SetRenderingPath( RenderManager::e_RenderingPathForward );
        }
    }

    if( inputs.FindAndConsumeAction( InputMapping::e_ActionRotateEntity ) )
    {
        m_EntityManipType = e_EntityManipTypeRotate;
    }
    else if( inputs.FindAndConsumeAction( InputMapping::e_ActionMoveEntity ) )
    {
        m_EntityManipType = e_EntityManipTypeMove;
    }

    if( inputs.FindAndConsumeAction( InputMapping::e_ActionOpenMainMenu ) )
    {
        SetAppIsStillRunning( false );
    }

    if( inputs.FindAndConsumeAction( InputMapping::e_ActionChooseEntityToMove ) )
    {
        m_KeysAreControlingEntities = !m_KeysAreControlingEntities;
    }

    if( inputs.FindAndConsumeAction( InputMapping::e_ActionNextEntityGroup ) )
    {
        m_CurrentEntityGroup = static_cast<EntityGroup>(m_CurrentEntityGroup + 1);

        if( m_CurrentEntityGroup == e_EntityGroupCount )
        {
            m_CurrentEntityGroup = e_EntityGroupPointLights;
        }

        m_CurrentLightNum = 0;
    }

    if( inputs.FindAndConsumeAction( InputMapping::e_ActionNextEntity ) )
    {
        if( m_CurrentEntityGroup == e_EntityGroupPointLights )
        {
            if( ( m_CurrentLightNum + 1 ) < ArraySize( m_PointLights ) )
            {
                m_CurrentLightNum++;
                m_CurrentPointLight = m_PointLights[m_CurrentLightNum];
            }
        }
        else if( m_CurrentEntityGroup == e_EntityGroupSpotLights )
        {
            if( ( m_CurrentLightNum + 1 ) < ArraySize( m_SpotLights ) )
            {
                m_CurrentSpotLight = m_SpotLights[m_CurrentLightNum];
            }
        }
    }

    if( inputs.FindAndConsumeAction( InputMapping::e_ActionPreviousEntity ) )
    {
        if( m_CurrentLightNum > 0 )
        {
            m_CurrentLightNum--;
            if( m_CurrentEntityGroup == e_EntityGroupPointLights )
            {
                m_CurrentPointLight = m_PointLights[m_CurrentLightNum];
            }
            else if( m_CurrentEntityGroup == e_EntityGroupSpotLights )
            {
                m_CurrentSpotLight = m_SpotLights[m_CurrentLightNum];
            }
        }
    }

    /*--------------- Change Field Of View Of Currently Active Camera -----------*/
    if( inputs.FindAndConsumeAction( InputMapping::e_ActionIncreaseFieldOfViewOfTheCamera )  )
    {
        m_Camera->SetFieldOfView( m_Camera->GetFieldOfView() + 1.0f );
    }

    if( inputs.FindAndConsumeAction( InputMapping::e_ActionDecreaseFieldOfViewOfTheCamera )  )
    {
        m_Camera->SetFieldOfView( m_Camera->GetFieldOfView() - 1.0f );
    }
    /*--------------------------------------------------------------------------*/
}

//-----------------------------------------------------------------------

void EditorLogic::_HandleMouseInput(float frameTimeInSeconds)
{
    if( m_MouseRightButtonIsPressed && ( m_CurrentAbsoluteMouseCoords != m_PreviousAbsoluteMouseCoords ) )
    {
        const float relativeMouseCoordX = MathUtils::MapRange( ( m_CurrentAbsoluteMouseCoords.x - m_PreviousAbsoluteMouseCoords.x ),
                                                               ( -static_cast<float>( m_DrawableSurfaceSize.widthInPixels ) / 2.0f ),
                                                               ( static_cast<float>( m_DrawableSurfaceSize.widthInPixels ) / 2.0f ),
                                                               0.0f,
                                                               1.0f );

        const float relativeMouseCoordY = MathUtils::MapRange( ( m_CurrentAbsoluteMouseCoords.y - m_PreviousAbsoluteMouseCoords.y ),
                                                               ( -static_cast<float>( m_DrawableSurfaceSize.heightInPixels ) / 2.0f ),
                                                               ( static_cast<float>( m_DrawableSurfaceSize.heightInPixels ) / 2.0f ),
                                                               0.0f,
                                                               1.0f );

        m_Camera->RecalculateOrientation( frameTimeInSeconds, relativeMouseCoordY, relativeMouseCoordX, 400.0f );
    }

    m_PreviousAbsoluteMouseCoords = m_CurrentAbsoluteMouseCoords;
}

//-----------------------------------------------------------------------

void EditorLogic::_BuildScene()
{
//    /*-------------------------------------------*/
//
//    SpotLightMarker* spotLightMarker = new SpotLightMarker( m_PhysicsManager, m_MaterialManager, m_TextureManager );
//    spotLightMarker->SetDirectionInWorldSpace( m_SpotLights[0]->GetDirectionInWorldSpace() );
//    spotLightMarker->SetRadius( m_SpotLights[0]->GetRadius() );
//    spotLightMarker->SetCutOffAngleInDegrees( m_SpotLights[0]->GetCutOffValueAngleInDegrees() );
//    spotLightMarker->SetColor( m_SpotLights[0]->GetColor() );
//    spotLightMarker->GetComponent<Transform>()->SetPosition( m_SpotLights[0]->GetPositionInWorldSpace() );
//
//    /*-------------------------------------------*/
//
//    PointLighMarker* pointLightMarker = new PointLighMarker( m_PhysicsManager, m_MaterialManager, m_TextureManager );
//
//    /*-------------------------------------------*/

//    GameObject* knight = new GameObject( "Knight" );
//    knight->GetComponent<Transform>()->SetPosition( glm::vec3( -3.0f, 0.7f, -2.0f ) );
//    SkinnedMeshRenderer* knightRenderer = new SkinnedMeshRenderer( *knight, m_MeshManager, m_MaterialManager );
//    MeshManager::ModelMeshes knightModelMeshes;
//    m_MeshManager.LoadModel( "DemoRoomData/Models/BlackKnight/BlackKnight.mobj", knightModelMeshes );
//    knightRenderer->SetMesh( knightModelMeshes[0] );
//    knightRenderer->SetSubMeshMaterial( 0, m_MaterialManager.LoadMaterial( "DemoRoomData/Models/BlackKnight/BlackKnightMaterial.mat" ) );
//
//    Animator* knightAnimator = new Animator( *knight, m_AnimationManager );
//    knightAnimator->AddAnimation( "Taunt", m_AnimationManager.LoadAnimation( "DemoRoomData/Models/BlackKnight/BlackKnight.manim" ) );
//    knightAnimator->Play( "Taunt" );
//
//    //BoxCollider* knightCollider = new BoxCollider( knightRenderer->GetAABB().halfExtents, m_PhysicsManager, *knight );
//    //SphereCollider* knightCollider = new SphereCollider( knightRenderer->GetAABB().halfExtents.x, m_PhysicsManager, *knight );
//    ConeCollider* knightCollider = new ConeCollider( *knight,
//                                                     knightRenderer->GetAABB().halfExtents.x,
//                                                     knightRenderer->GetAABB().halfExtents.x,
//                                                     m_PhysicsManager );

    /*-------------------------------------------*/

//    GameObject* densePlane = new GameObject( "Dense Plane" );
//    densePlane->GetComponent<Transform>()->SetPosition( glm::vec3( -2.0f, 0.5f, 0.0f ) );
//    MeshRenderer* densePlaneRenderer = new MeshRenderer( *densePlane, m_MaterialManager );
//    MeshFilter* densePlaneMeshFilter = new MeshFilter( *densePlane, m_MeshManager );
//    MeshManager::ModelMeshes densePlaneModelMeshes;
//    m_MeshManager.LoadModel( "DemoRoomData/Models/DensePlane/DensePlaneNoSkin.mobj", densePlaneModelMeshes );
//    densePlaneMeshFilter->SetMesh( densePlaneModelMeshes[0] );
//    densePlaneRenderer->SetSubMeshMaterial( 0, m_MaterialManager.LoadMaterial( "DemoRoomData/Models/DensePlane/DensePlaneTestMat.mat" ) );
//    BoxCollider* densePlaneCollider = new  BoxCollider( *densePlane, densePlaneRenderer->GetAABB().halfExtents, m_PhysicsManager );

    /*-------------------------------------------*/

//    GameObject* box = new GameObject( "Box" );
//    box->GetComponent<Transform>()->SetPosition( glm::vec3( 3.0f, 0.5f, 0.0f ) );
//    MeshRenderer* boxRenderer = new MeshRenderer( *box, m_MaterialManager );
//    MeshFilter* boxMeshFilter = new MeshFilter( *box, m_MeshManager );
//    MeshManager::ModelMeshes boxModelMeshes;
//    m_MeshManager.LoadModel( "DemoRoomData/Models/Cube/CubeNoSkin.mobj", boxModelMeshes );
//    boxMeshFilter->SetMesh( boxModelMeshes[0] );
//    boxRenderer->SetSubMeshMaterial( 0, m_MaterialManager.LoadMaterial( "DemoRoomData/Models/Cube/WoodenBoxTexture.mat" ) );
//    //RigidBody* boxRigidBody = new RigidBody( true, 10.0f, m_PhysicsManager, *box );
//    BoxCollider* boxCollider = new  BoxCollider( *box, boxRenderer->GetAABB().halfExtents, m_PhysicsManager );
//    //CapsuleCollider* boxCollider = new  CapsuleCollider( 0.5f, 1.0f, m_PhysicsManager, *box );

    /*-------------------------------------------*/

    GameObject* roboMap = new GameObject( "RoboMap" );
    MeshRenderer* roboMapRenderer = new MeshRenderer( *roboMap, m_MaterialManager );
    MeshFilter* roboMapMeshFilter = new MeshFilter( *roboMap, m_MeshManager );
    MeshManager::ModelMeshes roboMapModelMeshes;
    m_MeshManager.LoadModel( "DemoRoomData/Models/RoboMap/RoboMapNoSkin.mobj", roboMapModelMeshes );
    roboMapMeshFilter->SetMesh( roboMapModelMeshes[0] );
    roboMapRenderer->SetSubMeshMaterial( 0, m_MaterialManager.LoadMaterial( "DemoRoomData/Models/RoboMap/WoodenDoorStencilLambertMat.mat" ) );
    roboMapRenderer->SetSubMeshMaterial( 1, m_MaterialManager.LoadMaterial( "DemoRoomData/Models/RoboMap/CobbleStoneLambertMat.mat" ) );
    roboMapRenderer->SetSubMeshMaterial( 2, m_MaterialManager.LoadMaterial( "DemoRoomData/Models/RoboMap/StoneBrickWallMaterial.mat" ) );
    roboMapRenderer->SetSubMeshMaterial( 3, m_MaterialManager.LoadMaterial( "DemoRoomData/Models/RoboMap/StoneCeilingMat.mat" ) );
    roboMapRenderer->SetSubMeshMaterial( 4, m_MaterialManager.LoadMaterial( "DemoRoomData/Models/RoboMap/ChurchStoneLambertMat.mat" ) );
    roboMapRenderer->SetSubMeshMaterial( 5, m_MaterialManager.LoadMaterial( "DemoRoomData/Models/RoboMap/BarsDarkMetalMaterial.mat" ) );
    MeshCollider* roboMapCollider = new  MeshCollider( *roboMap, m_PhysicsManager );

    /*-------------------------------------------*/

//    GameObject* tRex = new GameObject( "TRex" );
//    tRex->GetComponent<Transform>()->SetPosition( glm::vec3( 1.1f, 0.0f, -1.18f ) );
//    tRex->GetComponent<Transform>()->SetScale( glm::vec3( 0.5f ) );
//    MeshRenderer* tRexRenderer = new MeshRenderer( *tRex, m_MaterialManager );
//    MeshFilter* tRexMeshFilter = new MeshFilter( *tRex, m_MeshManager );
//    MeshManager::ModelMeshes tRexModelMeshes;
//    m_MeshManager.LoadModel( "DemoRoomData/Models/TRex/TRex.mobj", tRexModelMeshes );
//    tRexMeshFilter->SetMesh( tRexModelMeshes[0] );
//    tRexRenderer->SetSubMeshMaterial( 0, m_MaterialManager.LoadMaterial( "DemoRoomData/Models/TRex/TRexMaterial.mat" ) );
//    RigidBody* tRexRigidBody = new RigidBody( *tRex, false, 10.0f, m_PhysicsManager );
//    BoxCollider* tRexCollider = new  BoxCollider( *tRex, tRexRenderer->GetAABB().halfExtents * 0.5f, m_PhysicsManager );
//    tRexCollider->SetCenterPositionOffset( glm::vec3( 0.0f, 0.780f, -0.3f ) );

    /*-------------------------------------------*/

//    GameObject* norfolkPine = new GameObject( "NorfolkPine" );
//    norfolkPine->GetComponent<Transform>()->SetPosition( glm::vec3( -1.8f, 0.0f, -3.7f ) );
//    norfolkPine->GetComponent<Transform>()->SetScale( glm::vec3( 0.5f ) );
//    MeshRenderer* norfolkPineRenderer = new MeshRenderer( *norfolkPine, m_MaterialManager );
//    MeshFilter* norfolkPineMeshFilter = new MeshFilter( *norfolkPine, m_MeshManager );
//    MeshManager::ModelMeshes norfolkPineModelMeshes;
//    m_MeshManager.LoadModel( "DemoRoomData/Models/NorfolkPine/Pine.mobj", norfolkPineModelMeshes );
//    norfolkPineMeshFilter->SetMesh( norfolkPineModelMeshes[0] );
//    norfolkPineRenderer->SetSubMeshMaterial( 0, m_MaterialManager.LoadMaterial( "DemoRoomData/Models/NorfolkPine/PineLeaf.mat" ) );
//    norfolkPineRenderer->SetSubMeshMaterial( 1, m_MaterialManager.LoadMaterial( "DemoRoomData/Models/NorfolkPine/PineBark.mat" ) );
//    norfolkPineRenderer->SetSubMeshMaterial( 2, m_MaterialManager.LoadMaterial( "DemoRoomData/Models/NorfolkPine/PineTwig.mat" ) );
//    norfolkPineRenderer->SetSubMeshMaterial( 3, m_MaterialManager.LoadMaterial( "DemoRoomData/Models/NorfolkPine/PineBark.mat" ) );
//    norfolkPineRenderer->SetSubMeshMaterial( 4, m_MaterialManager.LoadMaterial( "DemoRoomData/Models/NorfolkPine/PineBark.mat" ) );
//    //RigidBody* norfolkPineRigidBody = new RigidBody( *norfolkPine, false, 10.0f, m_PhysicsManager );
//    BoxCollider* norfolkPineCollider = new  BoxCollider( *norfolkPine, norfolkPineRenderer->GetAABB().halfExtents * 0.5f, m_PhysicsManager );
//    norfolkPineCollider->SetCenterPositionOffset( glm::vec3( 0.0f, 0.780f, -0.3f ) );

    /*-------------------------------------------*/

//        GameObject* cone = new GameObject( "NorfolkPine" );
//        cone->GetComponent<Transform>()->SetPosition( glm::vec3( -1.8f, 1.0f, -3.7f ) );
//        MeshRenderer* coneRenderer = new MeshRenderer( *cone, m_MaterialManager );
//        MeshFilter* coneMeshFilter = new MeshFilter( *cone, m_MeshManager );
//        MeshManager::ModelMeshes coneModelMeshes;
//        MeshManager::MeshHandle coneMeshHandle = m_MeshManager.GetInternalMeshHandle( MeshManager::e_InternalMeshTypeCone );
//        coneMeshFilter->SetMesh( coneMeshHandle );
//
//        MaterialDescriptor coneMaterialDescriptor( m_TextureManager );
//        coneMaterialDescriptor.SetDiffuseColor( EngineTypes::SRGBColor( 1.0f, 0.0f, 0.0f ) );
//        MaterialManager::MaterialHandle coneMaterial = m_MaterialManager.AddInternalMaterial( coneMaterialDescriptor );
//
//        coneRenderer->SetSubMeshMaterial( 0, coneMaterial );
//
//        //RigidBody* coneRigidBody = new RigidBody( *cone, false, 10.0f, m_PhysicsManager );
//        ConeCollider* coneCollider = new  ConeCollider( *cone,
//                                                        coneRenderer->GetAABB().halfExtents.x,
//                                                        ( coneRenderer->GetAABB().halfExtents.y * 2.0f ),
//                                                        m_PhysicsManager );

        /*-------------------------------------------*/

    GameObject* monkeyHead = new GameObject( "MonkeyHead" );
    monkeyHead->GetComponent<Transform>()->SetPosition( glm::vec3( -2.0f, 0.5f, 0.0f ) );
    MeshRenderer* monkeyHeadRenderer = new MeshRenderer( *monkeyHead, m_MaterialManager );
    MeshFilter* monkeyHeadMeshFilter = new MeshFilter( *monkeyHead, m_MeshManager );
    MeshManager::ModelMeshes monkeyHeadModelMeshes;
    m_MeshManager.LoadModel( "DemoRoomData/Models/Monkey/MonkeyHead.mobj", monkeyHeadModelMeshes );
    monkeyHeadMeshFilter->SetMesh( monkeyHeadModelMeshes[0] );
    monkeyHeadRenderer->SetSubMeshMaterial( 0, m_MaterialManager.LoadMaterial( "DemoRoomData/Models/Monkey/MonkeyMat.mat" ) );

//    m_GameObjectList.push_back( spotLightMarker );
//    m_GameObjectList.push_back( pointLightMarker );
    m_GameObjectList.push_back( roboMap );
    m_GameObjectList.push_back( monkeyHead );
//    m_GameObjectList.push_back( knight );
//    m_GameObjectList.push_back( densePlane );
//    m_GameObjectList.push_back( box );
//    m_GameObjectList.push_back( tRex );
//    m_GameObjectList.push_back( norfolkPine );
//    m_GameObjectList.push_back( cone );
}

//-----------------------------------------------------------------------

void EditorLogic::_AddRenderablesFromGameObjectHierarchyToTheRenderList(GameObject* gameObject)
{
    Renderer* renderer = gameObject->GetComponent<Renderer>();

    if( renderer != nullptr )
    {
        Renderer::RenderablesList& renderablesList = renderer->GetRenderables();

        for(size_t j = 0; j < renderablesList.size(); j++)
        {
            RenderableObject& renderable = renderablesList[j];
            renderable.SetWorldTransformMatrix( gameObject->GetGlobalTransformMatrix() );
            m_RenderManager.AddRenderable( renderable );
        }
    }

    GameObject::ChildNodeList& children = gameObject->GetChildren();

    for(GameObject* child : children)
    {
        _AddRenderablesFromGameObjectHierarchyToTheRenderList( child );
    }
}

//-----------------------------------------------------------------------

void EditorLogic::_BuildRenderList()
{
    for(size_t i = 0; i < m_GameObjectList.size(); i++)
    {
        _AddRenderablesFromGameObjectHierarchyToTheRenderList( m_GameObjectList[i] );
    }

    m_ManipulatorTool.AddRenderables( m_RenderManager );

    if( m_RenderDebugData )
    {
        const RenderableObject* physcisVisualDebugData = m_PhysicsManager.GetVisualDebugDataRenderable();
        if( physcisVisualDebugData != nullptr )
        {
            m_RenderManager.AddRenderable( *physcisVisualDebugData );
        }
    }
}

//-----------------------------------------------------------------------

void EditorLogic::Render(float frameTimeInSeconds)
{
    m_AnimationManager.UpdateCurrentlyActiveAnimations();

    m_PhysicsManager.RefreshDebugDataMesh();

    EngineTypes::VPTransformations vpTrans;
    vpTrans.viewMatrix = m_Camera->GetViewMatrix();
    vpTrans.projectionMatrix = MathUtils::GeneratePerspectiveProjectionMatrix( m_Camera->GetFieldOfView(),
                                                                               m_DrawableSurfaceSize.widthInPixels,
                                                                               m_DrawableSurfaceSize.heightInPixels );

    m_ManipulatorTool.MaintainSize( *m_Camera );

    _BuildRenderList();

    /*-----------------------------------Draw Scene --------------------------------*/
    const float lSpeed = ( Units::OneMeter * frameTimeInSeconds );

    if( m_EntityManipType == e_EntityManipTypeMove )
    {
        glm::vec3 currLightPos;
        if( m_CurrentEntityGroup == e_EntityGroupPointLights )
        {
            currLightPos = m_CurrentPointLight->GetPositionInWorldSpace();
        }
        else if( m_CurrentEntityGroup == e_EntityGroupSpotLights )
        {
            currLightPos = m_CurrentSpotLight->GetPositionInWorldSpace();
        }
        currLightPos.z += ( m_OtherEntitiesKeyVector[EditorLogic::e_KeyPressedW] ? -lSpeed : 0.0f );
        currLightPos.z += ( m_OtherEntitiesKeyVector[EditorLogic::e_KeyPressedS] ? lSpeed : 0.0f );
        currLightPos.x += ( m_OtherEntitiesKeyVector[EditorLogic::e_KeyPressedA] ? -lSpeed : 0.0f );
        currLightPos.x += ( m_OtherEntitiesKeyVector[EditorLogic::e_KeyPressedD] ? lSpeed : 0.0f );
        currLightPos.y += ( m_OtherEntitiesKeyVector[EditorLogic::e_KeyPressedPageUp] ? lSpeed : 0.0f );
        currLightPos.y += ( m_OtherEntitiesKeyVector[EditorLogic::e_KeyPressedPageDown] ? -lSpeed : 0.0f );
        if( m_CurrentEntityGroup == e_EntityGroupPointLights )
        {
            m_CurrentPointLight->SetPositionInWorldSpace( currLightPos );
        }
        else if( m_CurrentEntityGroup == e_EntityGroupSpotLights )
        {
            m_CurrentSpotLight->SetPositionInWorldSpace( currLightPos );
        }
    }
    if( m_EntityManipType == e_EntityManipTypeRotate )
    {
        glm::vec3 currLightDirection;
        if( m_CurrentEntityGroup == e_EntityGroupSpotLights )
        {
            currLightDirection = m_CurrentSpotLight->GetDirectionInWorldSpace();
        }
        currLightDirection.z += ( m_OtherEntitiesKeyVector[EditorLogic::e_KeyPressedW] ? -lSpeed : 0.0f );
        currLightDirection.z += ( m_OtherEntitiesKeyVector[EditorLogic::e_KeyPressedS] ? lSpeed : 0.0f );
        currLightDirection.x += ( m_OtherEntitiesKeyVector[EditorLogic::e_KeyPressedA] ? -lSpeed : 0.0f );
        currLightDirection.x += ( m_OtherEntitiesKeyVector[EditorLogic::e_KeyPressedD] ? lSpeed : 0.0f );
        currLightDirection.y += ( m_OtherEntitiesKeyVector[EditorLogic::e_KeyPressedPageUp] ? lSpeed : 0.0f );
        currLightDirection.y += ( m_OtherEntitiesKeyVector[EditorLogic::e_KeyPressedPageDown] ? -lSpeed : 0.0f );
        if( m_CurrentEntityGroup == e_EntityGroupSpotLights )
        {
            m_CurrentSpotLight->SetDirectionInWorldSpace( currLightDirection );
        }
    }

    m_DirectionalLight->SetAmbientIntensity( 0.0f );
    m_DirectionalLight->SetDiffuseIntensity( 0.3f );
    m_DirectionalLight->SetDirectionInWorldSpace( glm::vec3( 0.0f, -1.0f, 0.0f ) );
    m_DirectionalLight->SetEnabled( false );

    m_RenderManager.SetViewTransform( vpTrans.viewMatrix );
    m_RenderManager.SetProjectionTransform( RenderManager::e_ProjectionTypePerspective, vpTrans.projectionMatrix );
    m_RenderManager.SetFrustumNearAndFarClippingPlaneDistances( EngineDefaults::c_NearClippingPlane,
                                                                EngineDefaults::c_FarClippingPlane );
    m_RenderManager.Render();
    /*------------------------------------------------------------------------------------*/

    static glm::vec2 prevMouseAbsCoord( 0.0f, 0.0f );

    if( m_MouseLeftButtonIsPressed )
    {
        if( m_ManipulatorTool.Process( m_CurrentAbsoluteMouseCoords, vpTrans, m_DrawableSurfaceSize ) )
        {
            m_TransformComponentWidget->SetTransformComponent( m_ManipulatorTool.GetTransformBeingManipulated() );
        }
    }
    else
    {
        m_ManipulatorTool.ClearPickedManipulator();
    }

    if( m_ClickedWithMouse && !m_ManipulatorTool.PickManipulator( m_RenderManager, m_CurrentAbsoluteMouseCoords ) )
    {
        glm::vec3 rayOrigin = m_Camera->GetPosition();
        glm::vec3 rayDirection = glm::vec3( 0.0f );

        MathUtils::CoordinateInScreenSpaceToRay( m_CurrentAbsoluteMouseCoords.x,
                                                 m_CurrentAbsoluteMouseCoords.y,
                                                 m_DrawableSurfaceSize.widthInPixels,
                                                 m_DrawableSurfaceSize.heightInPixels,
                                                 vpTrans,
                                                 m_Camera->GetPosition(),
                                                 rayDirection );

        GameObject* gameObjectHit = nullptr;
        m_PhysicsManager.CastARay( rayOrigin, rayDirection, gameObjectHit );

        if( gameObjectHit )
        {
            m_CurrentlySelectedGameObject = gameObjectHit;
            m_ManipulatorTool.SetTransformToManipulate( gameObjectHit->GetComponent<Transform>() );
            _PopulateGUIWithGameObjectData( *gameObjectHit );
        }
        else
        {
            m_ManipulatorTool.SetTransformToManipulate( nullptr );
            _HideAllGUIWithGameObjectData();
        }
    }

    _RenderText();
}

//-----------------------------------------------------------------------

void EditorLogic::_RenderText()
{
    const float fpsTxtXCoord = ((static_cast<float>(m_DrawableSurfaceSize.widthInPixels) / 2) * (-1)) + 20;
    const float fpsTxtYCoord = (static_cast<float>(m_DrawableSurfaceSize.heightInPixels) / 2) - 30;
    m_DrawableText.Render("ms/Frame: "+numToString(m_CurrentFrameTimeInMs), fpsTxtXCoord, fpsTxtYCoord);
    m_DrawableText.Render("FPS: "+numToString(static_cast<unsigned int>(1000.0f / m_CurrentFrameTimeInMs)), fpsTxtXCoord, fpsTxtYCoord-20);
    m_DrawableText.Render("<Not Used>", fpsTxtXCoord, fpsTxtYCoord-40);
    m_DrawableText.Render("CamPos X : "+numToString(m_Camera->GetPosition()[0]), fpsTxtXCoord, fpsTxtYCoord-60);
    m_DrawableText.Render("CamPos Y : "+numToString(m_Camera->GetPosition()[1]), fpsTxtXCoord, fpsTxtYCoord-80);
    m_DrawableText.Render("CamPos Z : "+numToString(m_Camera->GetPosition()[2]), fpsTxtXCoord, fpsTxtYCoord-100);
    m_DrawableText.Render("CamDir X : "+numToString(m_Camera->GetDirectionVector()[0]), fpsTxtXCoord, fpsTxtYCoord-120);
    m_DrawableText.Render("CamDir Y : "+numToString(m_Camera->GetDirectionVector()[1]), fpsTxtXCoord, fpsTxtYCoord-140);
    m_DrawableText.Render("CamDir Z : "+numToString(m_Camera->GetDirectionVector()[2]), fpsTxtXCoord, fpsTxtYCoord-160);
    m_DrawableText.Render("CamFOV : "+numToString(m_Camera->GetFieldOfView()), fpsTxtXCoord, fpsTxtYCoord-180);
    m_DrawableText.Render("<Not Used>", fpsTxtXCoord, fpsTxtYCoord-200);
    m_DrawableText.Render("<Not Used>", fpsTxtXCoord, fpsTxtYCoord-220);
    m_DrawableText.Render("MouseX(Abs.) :"+numToString(m_CurrentAbsoluteMouseCoords.x), fpsTxtXCoord, fpsTxtYCoord-240);
    m_DrawableText.Render("MouseY(Abs.) :"+numToString(m_CurrentAbsoluteMouseCoords.y), fpsTxtXCoord, fpsTxtYCoord-260);

    unsigned int nextTextLinePos = 280;

    if( m_CurrentEntityGroup == e_EntityGroupPointLights )
    {
        glm::vec3 lightPos = m_CurrentPointLight->GetPositionInWorldSpace();
        m_DrawableText.Render("Ent Name: Point Light( "+numToString(m_CurrentLightNum)+" )", fpsTxtXCoord, fpsTxtYCoord-280);
        m_DrawableText.Render("Ent X: "+numToString(lightPos.x), fpsTxtXCoord, fpsTxtYCoord-300);
        m_DrawableText.Render("Ent Y: "+numToString(lightPos.y), fpsTxtXCoord, fpsTxtYCoord-320);
        m_DrawableText.Render("Ent Z: "+numToString(lightPos.z), fpsTxtXCoord, fpsTxtYCoord-340);
        nextTextLinePos = 340;
    }
    else if( m_CurrentEntityGroup == e_EntityGroupSpotLights )
    {
        glm::vec3 lightPos = m_CurrentSpotLight->GetPositionInWorldSpace();
        m_DrawableText.Render("Ent Name: Spot Light( "+numToString(m_CurrentLightNum)+" )", fpsTxtXCoord, fpsTxtYCoord-280);
        m_DrawableText.Render("Ent X: "+numToString(lightPos.x), fpsTxtXCoord, fpsTxtYCoord-300);
        m_DrawableText.Render("Ent Y: "+numToString(lightPos.y), fpsTxtXCoord, fpsTxtYCoord-320);
        m_DrawableText.Render("Ent Z: "+numToString(lightPos.z), fpsTxtXCoord, fpsTxtYCoord-340);
        nextTextLinePos = 340;
    }

    if( m_EntityManipType == e_EntityManipTypeMove )
    {
        m_DrawableText.Render("Manip Type: Move", fpsTxtXCoord, fpsTxtYCoord-360);
        nextTextLinePos = 380;
    }
    else if( m_EntityManipType == e_EntityManipTypeRotate )
    {
        m_DrawableText.Render("Manip Type: Rotate", fpsTxtXCoord, fpsTxtYCoord-360);
        nextTextLinePos = 380;
    }

    if( m_RenderManager.GetRenderingPath() == RenderManager::e_RenderingPathForward )
    {
        m_DrawableText.Render("Renderer: Forward", fpsTxtXCoord, fpsTxtYCoord-380);
        nextTextLinePos = 400;
    }
    else if( m_RenderManager.GetRenderingPath() == RenderManager::e_RenderingPathDeferred )
    {
        m_DrawableText.Render("Renderer: Deferred", fpsTxtXCoord, fpsTxtYCoord-380);
        nextTextLinePos = 400;
    }

    const float heading = MathUtils::HeadingFromADirectionVector( m_Camera->GetDirectionVector() );
    m_DrawableText.Render("Heading: "+numToString(heading), fpsTxtXCoord, (fpsTxtYCoord - nextTextLinePos));

    std::string selectedObjectName = "";
    if( m_CurrentlySelectedGameObject )
    {
        selectedObjectName = m_CurrentlySelectedGameObject->GetName();
    }
    m_DrawableText.Render("Selected: " + selectedObjectName, fpsTxtXCoord, (fpsTxtYCoord - nextTextLinePos) - 20);
}

//-----------------------------------------------------------------------

void EditorLogic::_DrawableSurfaceSizeChaged(const SystemInterface::DrawableSurfaceSize& newSize, void* userData)
{
    static_cast<EditorLogic*>( userData )->m_DrawableSurfaceSize = newSize;
}

//-----------------------------------------------------------------------
