#ifndef BOXCOLLIDERCOMPONENTWIDGET_H_INCLUDED
#define BOXCOLLIDERCOMPONENTWIDGET_H_INCLUDED

#include "ColliderWidgetBase.h"
#include <ui_BoxColliderComponentUI.h>
#include <Engine/Scene/BoxCollider.h>

class BoxColliderComponentWidget : public ColliderWidgetBase<Ui::BoxColliderComponentUI, BoxCollider>
{
    Q_OBJECT

public:
    BoxColliderComponentWidget(CategorizedListWidget* listWidgetToAddOurselvesTo, QWidget* parent = 0);
    ~BoxColliderComponentWidget();

    void SetBoxColliderComponent(BoxCollider* collider);

    void ResetPaletteToDefault();

private slots:
    void XSizeLineEditEditingFinished();
    void YSizeLineEditEditingFinished();
    void ZSizeLineEditEditingFinished();
};

#endif // BOXCOLLIDERCOMPONENTWIDGET_H_INCLUDED
