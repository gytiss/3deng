#include "WindowManager.h"
#include "MsgLogger.h"
#include "StringUtils.h"
#include <sstream>
#include <CommonDefinitions.h>
#include <Exceptions/GenericException.h>
#include <InputMapping/RawInputConstants.h>

#include <OpenGLIncludes.h>
#include <OpenGLErrorChecking.h>

#include <SDL2/SDL_version.h>
#include <StringUtils.h>

#ifdef VR_BACKEND_ENABLED
    #include <VR/VRBackend.h>
#endif

//-----------------------------------------------------------------------

#ifdef VR_BACKEND_ENABLED
WindowManager::WindowManager(InputMapping::InputMapper& inputMapper,
                             size_t width,
                             size_t height,
                             std::string title,
                             bool fullScreen,
                             bool enableMultisampling,
                             bool enableVSync)
: WindowManager( inputMapper, nullptr, width, height, title, fullScreen, enableMultisampling, enableVSync )
{
}
#endif

//-----------------------------------------------------------------------

#ifdef VR_BACKEND_ENABLED
WindowManager::WindowManager(InputMapping::InputMapper& inputMapper,
                             VRBackend& vrBackend,
                             std::string title,
                             bool enableVSync)
: WindowManager( inputMapper, &vrBackend, vrBackend.GetHMDRenderWidth(), vrBackend.GetHMDRenderHeight(), title, false, false, enableVSync )
{
}
#endif

//-----------------------------------------------------------------------

WindowManager::WindowManager(InputMapping::InputMapper& inputMapper,
#ifdef VR_BACKEND_ENABLED
                             VRBackend* vrBackend,
#endif
                             size_t width,
                             size_t height,
                             std::string title,
                             bool fullScreen,
                             bool enableMultisampling,
                             bool enableVSync)
: m_InputMapper( inputMapper ),
#ifdef VR_BACKEND_ENABLED
  m_VRBackend( vrBackend ),
#endif
  m_AppWindow( nullptr ),
  m_AppWindowID( 0 ),
  m_OpenGL_Context( nullptr ),
  m_KeyCodeMap(),
  m_ScreenSize( width, height ),
  m_MouseCapturedInTheWindow( false ),
  m_MouseIsRelativeToTheCenter( false ),
  m_TextInputEnabled( false ),
  m_HideMouseCursor( false ),
  m_PreviousAbsoluteMousePosX( 0 ),
  m_PreviousAbsoluteMousePosY( 0 ),
  m_WindowCloseHasBeenRequested( false ),
  m_IsVisible( false ),
  m_VSyncIsEnabled( enableVSync )
{
    _InitializeKeyCodeMap( m_KeyCodeMap );
    _SetupWindow( width, height, title, fullScreen, enableMultisampling, enableVSync );
}

//-----------------------------------------------------------------------

WindowManager::~WindowManager()
{
    if( m_OpenGL_Context )
    {
        delete m_OpenGL_Context;
    }
}

//-----------------------------------------------------------------------

void WindowManager::DestroyWindow()
{
    if( m_OpenGL_Context )
    {
        SDL_GL_DeleteContext( *m_OpenGL_Context );
    }

    if( m_AppWindow )
    {
        SDL_DestroyWindow( m_AppWindow );
    }

    m_AppWindow = nullptr;

    SDL_Quit();
}

//-----------------------------------------------------------------------

bool WindowManager::IsVisible() const
{
#if defined(OS_ANDROID) || defined(VR_BACKEND_ENABLED)
    return true;
#else
    return m_IsVisible;
#endif
}

//-----------------------------------------------------------------------

void WindowManager::SwapFrameBuffers()
{
    SDL_GL_SwapWindow( m_AppWindow ); // Swap frame-buffers

#ifdef VR_BACKEND_ENABLED
    if ( ( m_VRBackend != nullptr ) && m_VSyncIsEnabled )
    { 
        m_VRBackend->WaitForHMD();
    }
#endif
}

//-----------------------------------------------------------------------

const WindowManager::DrawableSurfaceSize& WindowManager::GetDrawableSurfaceSize() const
{
    return m_ScreenSize;
}

//-----------------------------------------------------------------------

void WindowManager::SetCaptureMouseInTheWindow(bool val)
{
    m_MouseCapturedInTheWindow = val;
    SDL_bool setRelativeMouseMode = SDL_FALSE;

    if( m_MouseCapturedInTheWindow )
    {
        SDL_WarpMouseInWindow( m_AppWindow, static_cast<int>( m_ScreenSize.widthInPixels >> 1 ), static_cast<int>( m_ScreenSize.heightInPixels >> 1 ) );
        setRelativeMouseMode = SDL_TRUE;
    }

    if( SDL_SetRelativeMouseMode( setRelativeMouseMode ) != 0 )
    {
        throw GenericException( "Error(SDL has failed to transition mouse to relative mode): " + std::string( SDL_GetError() ) );
    }
}

//-----------------------------------------------------------------------

bool WindowManager::GetCaptureMouseInTheWindow() const
{
    return m_MouseCapturedInTheWindow;
}

//-----------------------------------------------------------------------

void WindowManager::SetMouseIsRelativeToTheCenter(bool val)
{
    m_MouseIsRelativeToTheCenter = val;
}

//-----------------------------------------------------------------------

bool WindowManager::GetMouseIsRelativeToTheCenter() const
{
    return m_MouseIsRelativeToTheCenter;
}

//-----------------------------------------------------------------------

void WindowManager::SetTextInputEnabled(bool val)
{
    if( !m_TextInputEnabled && val )
    {
        SDL_StartTextInput();
    }
    else if( m_TextInputEnabled && !val )
    {
        SDL_StopTextInput();
    }

    m_TextInputEnabled = val;
}

//-----------------------------------------------------------------------

bool WindowManager::GetTextInputEnabled() const
{
    return m_TextInputEnabled;
}

//-----------------------------------------------------------------------

void WindowManager::SetHideMouseCursor(bool val)
{
    m_HideMouseCursor = val;
    const int retVal = SDL_ShowCursor( ( m_HideMouseCursor ? 0 : 1 ) );
    if( retVal < 0 )
    {
        throw GenericException( "Error(SDL has failed to change mouse cursor visibility): " + std::string( SDL_GetError() ) );
    }
}

//-----------------------------------------------------------------------

bool WindowManager::GetHideMouseCursor() const
{
    return m_HideMouseCursor;
}

//-----------------------------------------------------------------------

bool WindowManager::WindowCloseWasRequested()
{
    return m_WindowCloseHasBeenRequested;
}

//-----------------------------------------------------------------------

void WindowManager::SetAsianTextInputHelperRect(int x, int y, int width, int heigh)
{
    SDL_Rect rect;
    rect.x = x;
    rect.y = y;
    rect.w = width;
    rect.h = heigh;
    SDL_SetTextInputRect( &rect );
}

//-----------------------------------------------------------------------

void WindowManager::ProcessWindowEvents()
{
    static const SDL_FingerID c_InvalidFingerID = -1;
    static SDL_FingerID fingerIDs[2] = { c_InvalidFingerID, c_InvalidFingerID };

#ifndef OS_ANDROID
    if( m_MouseCapturedInTheWindow && m_MouseIsRelativeToTheCenter )
    {
        int relativeMousePosX = 0;
        int relativeMousePosY = 0;

        // Get mouse position with origin at the center of the window
        SDL_GetRelativeMouseState ( &relativeMousePosX, &relativeMousePosY );

        if( ( relativeMousePosX != 0 ) || ( relativeMousePosY != 0 ) )
        {
            SDL_WarpMouseInWindow( m_AppWindow, static_cast<int>( m_ScreenSize.widthInPixels >> 1 ), static_cast<int>( m_ScreenSize.heightInPixels >> 1 ) );

            m_InputMapper.SetRawAxisValue( InputMapping::e_RawInputAxisMouseRelativeX, static_cast<double>( relativeMousePosX ) );
            m_InputMapper.SetRawAxisValue( InputMapping::e_RawInputAxisMouseRelativeY, static_cast<double>( relativeMousePosY ) );
        }
    }
    else
    {
        int absoluteMousePosX = 0;
        int absoluteMousePosY = 0;

        // Get mouse position with origin at the top left corner of the window
        SDL_GetMouseState( &absoluteMousePosX, &absoluteMousePosY );

        // Make sure that the mouse has moved before generating any new events
        if( ( absoluteMousePosX != m_PreviousAbsoluteMousePosX ) ||
            ( absoluteMousePosY != m_PreviousAbsoluteMousePosY ) )
        {
            m_PreviousAbsoluteMousePosX = absoluteMousePosX;
            m_PreviousAbsoluteMousePosY = absoluteMousePosY;

            m_InputMapper.SetRawAxisValue( InputMapping::e_RawInputAxisMouseAbsoluteX, static_cast<double>( absoluteMousePosX ) );
            m_InputMapper.SetRawAxisValue( InputMapping::e_RawInputAxisMouseAbsoluteY, static_cast<double>( absoluteMousePosY ) );
        }
    }
#endif

    /*---------Get Input And Other Events ---------------*/
    SDL_Event event;
    while ( SDL_PollEvent(&event) )
    {
        switch (event.type)
        {
            case SDL_WINDOWEVENT:
            {
                if( event.window.windowID == m_AppWindowID )
                {
                    switch (event.window.event)
                    {
                        case SDL_WINDOWEVENT_SIZE_CHANGED:
                        {
                        // Secondary window size changes are not used in VR mode
                        #ifndef VR_BACKEND_ENABLED
                            m_ScreenSize.widthInPixels = event.window.data1;
                            m_ScreenSize.heightInPixels = event.window.data2;

                            _DrawableSurfaceSizeChanged( m_ScreenSize );
                        #endif
                            break;
                        }


                        case SDL_WINDOWEVENT_HIDDEN:
                        {
                            m_IsVisible = false;
                            break;
                        }

                        case SDL_WINDOWEVENT_SHOWN:
                        {
                            m_IsVisible = true;
                            break;
                        }

                        case SDL_WINDOWEVENT_CLOSE:
                        {
                            m_WindowCloseHasBeenRequested = true;
                            break;
                        }
                    }
                }

                break;
            }

            case SDL_FINGERDOWN:
            {
                const float absPosX = event.tfinger.x * static_cast<float>( m_ScreenSize.widthInPixels );
                const float absPosY = event.tfinger.y * static_cast<float>( m_ScreenSize.heightInPixels );

                if( fingerIDs[0] == c_InvalidFingerID )
                {
                    fingerIDs[0] = event.tfinger.fingerId;
                    m_InputMapper.SetRawButtonState( InputMapping::e_RawInputFingerOne, true );

                    m_InputMapper.SetRawAxisValue( InputMapping::e_RawInputAxisFingerOneAbsoluteX, static_cast<double>( absPosX ) );
                    m_InputMapper.SetRawAxisValue( InputMapping::e_RawInputAxisFingerOneAbsoluteY, static_cast<double>( absPosY ) );
                }
                else  if( fingerIDs[1] == c_InvalidFingerID )
                {
                    fingerIDs[1] = event.tfinger.fingerId;
                    m_InputMapper.SetRawButtonState( InputMapping::e_RawInputFingerTwo, true );

                    m_InputMapper.SetRawAxisValue( InputMapping::e_RawInputAxisFingerTwoAbsoluteX, static_cast<double>( absPosX ) );
                    m_InputMapper.SetRawAxisValue( InputMapping::e_RawInputAxisFingerTwoAbsoluteY, static_cast<double>( absPosY ) );
                }

                break;
            }

            case SDL_FINGERUP:
            {
                if( event.tfinger.fingerId == fingerIDs[0] )
                {
                    fingerIDs[0] = c_InvalidFingerID;
                    m_InputMapper.SetRawButtonState( InputMapping::e_RawInputFingerOne, false );
                }
                else  if( event.tfinger.fingerId == fingerIDs[1] )
                {
                    fingerIDs[1] = c_InvalidFingerID;
                    m_InputMapper.SetRawButtonState( InputMapping::e_RawInputFingerTwo, false );
                }

                break;
            }

            case SDL_FINGERMOTION:
            {
                const float relativePosX = event.tfinger.dx * static_cast<float>( m_ScreenSize.widthInPixels );
                const float relativePosY = event.tfinger.dy * static_cast<float>( m_ScreenSize.heightInPixels );

                if( ( relativePosX != 0.0f ) || ( relativePosY != 0.0f ) )
                {
                    if( event.tfinger.fingerId == fingerIDs[0] )
                    {
                        m_InputMapper.SetRawAxisValue( InputMapping::e_RawInputAxisFingerOneRelativeX, static_cast<double>( relativePosX ) );
                        m_InputMapper.SetRawAxisValue( InputMapping::e_RawInputAxisFingerOneRelativeY, static_cast<double>( relativePosY ) );
                    }
                    else if( event.tfinger.fingerId == fingerIDs[1] )
                    {
                        m_InputMapper.SetRawAxisValue( InputMapping::e_RawInputAxisFingerTwoRelativeX, static_cast<double>( relativePosX ) );
                        m_InputMapper.SetRawAxisValue( InputMapping::e_RawInputAxisFingerTwoRelativeY, static_cast<double>( relativePosY ) );
                    }
                }

                break;
            }

            case SDL_MOUSEBUTTONDOWN:
            {
                switch(event.button.button)
                {
                    case SDL_BUTTON_LEFT:
                    {
                        m_InputMapper.SetRawButtonState( InputMapping::e_RawInputButtonMouseLeft, true );
                        break;
                    }

                    case SDL_BUTTON_RIGHT:
                    {
                        m_InputMapper.SetRawButtonState( InputMapping::e_RawInputButtonMouseRight, true );
                        break;
                    }

                    case SDL_BUTTON_MIDDLE:
                    {
                        m_InputMapper.SetRawButtonState( InputMapping::e_RawInputButtonMouseMiddle, true );
                        break;
                    }
                }

                break;
            }

            case SDL_MOUSEBUTTONUP:
            {
                switch(event.button.button)
                {
                    case SDL_BUTTON_LEFT:
                    {
                        m_InputMapper.SetRawButtonState( InputMapping::e_RawInputButtonMouseLeft, false );
                        break;
                    }

                    case SDL_BUTTON_RIGHT:
                    {
                        m_InputMapper.SetRawButtonState( InputMapping::e_RawInputButtonMouseRight, false );
                        break;
                    }

                    case SDL_BUTTON_MIDDLE:
                    {
                        m_InputMapper.SetRawButtonState( InputMapping::e_RawInputButtonMouseMiddle, false );
                        break;
                    }
                }

                break;
            }

            case SDL_MOUSEWHEEL:
            {
                Sint32 x = event.wheel.x;
                Sint32 y = event.wheel.y;

#if (SDL_MAJOR_VERSION >= 2) && (SDL_MINOR_VERSION >= 4)
                if( ( event.wheel.direction != SDL_MOUSEWHEEL_NORMAL ) &&
                    ( event.wheel.direction == SDL_MOUSEWHEEL_FLIPPED ))
                {
                    x = ( x * -1 );
                    y = ( y * -1 );
                }
#endif

                m_InputMapper.SetRawAxisValue( InputMapping::e_RawInputAxisMouseWheelHorizontal,
                                               static_cast<double>( x ) );
                m_InputMapper.SetRawAxisValue( InputMapping::e_RawInputAxisMouseWheelVertical,
                                               static_cast<double>( y ) );
                break;
            }

            case SDL_TEXTINPUT:
            {
               //event.text.text // UTF-8 Text
               break;
            }

            case SDL_TEXTEDITING:
            {
               /*
               Update the composition text.
               Update the cursor position.
               Update the selection length (if any).
               */
//                   composition = event.edit.text;
//                   cursor = event.edit.start;
//                   selection_len = event.edit.length;
               break;
            }

            case SDL_KEYDOWN:
            {
                InputMapping::RawInputButton rawInputButton = InputMapping::e_RawInputButtonInvalid;
                if( _MapSDLKeyCodeToRawInputButton( event.key.keysym.sym, rawInputButton ) )
                {
                    m_InputMapper.SetRawButtonState( static_cast<InputMapping::RawInputButton>( rawInputButton ), true );
                }

                break;
            }

            case SDL_KEYUP:
            {
                InputMapping::RawInputButton rawInputButton = InputMapping::e_RawInputButtonInvalid;
                if( _MapSDLKeyCodeToRawInputButton( event.key.keysym.sym, rawInputButton ) )
                {
                    m_InputMapper.SetRawButtonState( static_cast<InputMapping::RawInputButton>( rawInputButton ), false );
                }

                break;
            }
        }
    }
    /*---------------------------------------------------*/
}

//-----------------------------------------------------------------------

uint64_t WindowManager::_GetTicksInMilliseconds()
{
    return static_cast<uint64_t>( SDL_GetTicks() );
}

//-----------------------------------------------------------------------

void WindowManager::_InitializeKeyCodeMap(SDLKeyCodesToRawInputButtonsMap&  keyCodeMap)
{
    keyCodeMap[SDLK_RETURN] = InputMapping::e_RawInputButtonReturn;
    keyCodeMap[SDLK_ESCAPE] = InputMapping::e_RawInputButtonEscape;
    keyCodeMap[SDLK_BACKSPACE] = InputMapping::e_RawInputButtonBackspace;
    keyCodeMap[SDLK_TAB] = InputMapping::e_RawInputButtonTab;
    keyCodeMap[SDLK_SPACE] = InputMapping::e_RawInputButtonSpace;
    keyCodeMap[SDLK_EXCLAIM] = InputMapping::e_RawInputButtonExclaim;
    keyCodeMap[SDLK_QUOTEDBL] = InputMapping::e_RawInputButtonQuoteDbl;
    keyCodeMap[SDLK_HASH] = InputMapping::e_RawInputButtonHash;
    keyCodeMap[SDLK_PERCENT] = InputMapping::e_RawInputButtonPercent;
    keyCodeMap[SDLK_DOLLAR] = InputMapping::e_RawInputButtonDollar;
    keyCodeMap[SDLK_AMPERSAND] = InputMapping::e_RawInputButtonAmpersand;
    keyCodeMap[SDLK_QUOTE] = InputMapping::e_RawInputButtonQuote;
    keyCodeMap[SDLK_LEFTPAREN] = InputMapping::e_RawInputButtonLeftParen;
    keyCodeMap[SDLK_RIGHTPAREN] = InputMapping::e_RawInputButtonRightParent;
    keyCodeMap[SDLK_ASTERISK] = InputMapping::e_RawInputButtonAsterisk;
    keyCodeMap[SDLK_PLUS] = InputMapping::e_RawInputButtonPlus;
    keyCodeMap[SDLK_COMMA] = InputMapping::e_RawInputButtonComma;
    keyCodeMap[SDLK_MINUS] = InputMapping::e_RawInputButtonMinus;
    keyCodeMap[SDLK_PERIOD] = InputMapping::e_RawInputButtonPeriod;
    keyCodeMap[SDLK_SLASH] = InputMapping::e_RawInputButtonSlash;
    keyCodeMap[SDLK_0] = InputMapping::e_RawInputButton0;
    keyCodeMap[SDLK_1] = InputMapping::e_RawInputButton1;
    keyCodeMap[SDLK_2] = InputMapping::e_RawInputButton2;
    keyCodeMap[SDLK_3] = InputMapping::e_RawInputButton3;
    keyCodeMap[SDLK_4] = InputMapping::e_RawInputButton4;
    keyCodeMap[SDLK_5] = InputMapping::e_RawInputButton5;
    keyCodeMap[SDLK_6] = InputMapping::e_RawInputButton6;
    keyCodeMap[SDLK_7] = InputMapping::e_RawInputButton7;
    keyCodeMap[SDLK_8] = InputMapping::e_RawInputButton8;
    keyCodeMap[SDLK_9] = InputMapping::e_RawInputButton9;
    keyCodeMap[SDLK_COLON] = InputMapping::e_RawInputButtonColon;
    keyCodeMap[SDLK_SEMICOLON] = InputMapping::e_RawInputButtonSemicolon;
    keyCodeMap[SDLK_LESS] = InputMapping::e_RawInputButtonLess;
    keyCodeMap[SDLK_EQUALS] = InputMapping::e_RawInputButtonEquals;
    keyCodeMap[SDLK_GREATER] = InputMapping::e_RawInputButtonGreater;
    keyCodeMap[SDLK_QUESTION] = InputMapping::e_RawInputButtonQuestion;
    keyCodeMap[SDLK_AT] = InputMapping::e_RawInputButtonAt;

    keyCodeMap[SDLK_LEFTBRACKET] = InputMapping::e_RawInputButtonLeftBracket;
    keyCodeMap[SDLK_BACKSLASH] = InputMapping::e_RawInputButtonBackslash;
    keyCodeMap[SDLK_RIGHTBRACKET] = InputMapping::e_RawInputButtonRightBracket;
    keyCodeMap[SDLK_CARET] = InputMapping::e_RawInputButtonCarret;
    keyCodeMap[SDLK_UNDERSCORE] = InputMapping::e_RawInputButtonUnderscore;
    keyCodeMap[SDLK_BACKQUOTE] = InputMapping::e_RawInputButtonBackquote;
    keyCodeMap[SDLK_a] = InputMapping::e_RawInputButtonA;
    keyCodeMap[SDLK_b] = InputMapping::e_RawInputButtonB;
    keyCodeMap[SDLK_c] = InputMapping::e_RawInputButtonC;
    keyCodeMap[SDLK_d] = InputMapping::e_RawInputButtonD;
    keyCodeMap[SDLK_e] = InputMapping::e_RawInputButtonE;
    keyCodeMap[SDLK_f] = InputMapping::e_RawInputButtonF;
    keyCodeMap[SDLK_g] = InputMapping::e_RawInputButtonG;
    keyCodeMap[SDLK_h] = InputMapping::e_RawInputButtonH;
    keyCodeMap[SDLK_i] = InputMapping::e_RawInputButtonI;
    keyCodeMap[SDLK_j] = InputMapping::e_RawInputButtonJ;
    keyCodeMap[SDLK_k] = InputMapping::e_RawInputButtonK;
    keyCodeMap[SDLK_l] = InputMapping::e_RawInputButtonL;
    keyCodeMap[SDLK_m] = InputMapping::e_RawInputButtonM;
    keyCodeMap[SDLK_n] = InputMapping::e_RawInputButtonN;
    keyCodeMap[SDLK_o] = InputMapping::e_RawInputButtonO;
    keyCodeMap[SDLK_p] = InputMapping::e_RawInputButtonP;
    keyCodeMap[SDLK_q] = InputMapping::e_RawInputButtonQ;
    keyCodeMap[SDLK_r] = InputMapping::e_RawInputButtonR;
    keyCodeMap[SDLK_s] = InputMapping::e_RawInputButtonS;
    keyCodeMap[SDLK_t] = InputMapping::e_RawInputButtonT;
    keyCodeMap[SDLK_u] = InputMapping::e_RawInputButtonU;
    keyCodeMap[SDLK_v] = InputMapping::e_RawInputButtonV;
    keyCodeMap[SDLK_w] = InputMapping::e_RawInputButtonW;
    keyCodeMap[SDLK_x] = InputMapping::e_RawInputButtonX;
    keyCodeMap[SDLK_y] = InputMapping::e_RawInputButtonY;
    keyCodeMap[SDLK_z] = InputMapping::e_RawInputButtonZ;

    keyCodeMap[SDLK_CAPSLOCK] = InputMapping::e_RawInputButtonCapsLock;

    keyCodeMap[SDLK_F1] = InputMapping::e_RawInputButtonF1;
    keyCodeMap[SDLK_F2] = InputMapping::e_RawInputButtonF2;
    keyCodeMap[SDLK_F3] = InputMapping::e_RawInputButtonF3;
    keyCodeMap[SDLK_F4] = InputMapping::e_RawInputButtonF4;
    keyCodeMap[SDLK_F5] = InputMapping::e_RawInputButtonF5;
    keyCodeMap[SDLK_F6] = InputMapping::e_RawInputButtonF6;
    keyCodeMap[SDLK_F7] = InputMapping::e_RawInputButtonF7;
    keyCodeMap[SDLK_F8] = InputMapping::e_RawInputButtonF8;
    keyCodeMap[SDLK_F9] = InputMapping::e_RawInputButtonF9;
    keyCodeMap[SDLK_F10] = InputMapping::e_RawInputButtonF10;
    keyCodeMap[SDLK_F11] = InputMapping::e_RawInputButtonF11;
    keyCodeMap[SDLK_F12] = InputMapping::e_RawInputButtonF12;

    keyCodeMap[SDLK_PRINTSCREEN] = InputMapping::e_RawInputButtonPrintScreen;
    keyCodeMap[SDLK_SCROLLLOCK] = InputMapping::e_RawInputButtonScrollLock;
    keyCodeMap[SDLK_PAUSE] = InputMapping::e_RawInputButtonPause;
    keyCodeMap[SDLK_INSERT] = InputMapping::e_RawInputButtonInsert;
    keyCodeMap[SDLK_HOME] = InputMapping::e_RawInputButtonHome;
    keyCodeMap[SDLK_PAGEUP] = InputMapping::e_RawInputButtonPageUp;
    keyCodeMap[SDLK_DELETE] = InputMapping::e_RawInputButtonDelete;
    keyCodeMap[SDLK_END] = InputMapping::e_RawInputButtonEnd;
    keyCodeMap[SDLK_PAGEDOWN] = InputMapping::e_RawInputButtonPageDown;
    keyCodeMap[SDLK_RIGHT] = InputMapping::e_RawInputButtonRight;
    keyCodeMap[SDLK_LEFT] = InputMapping::e_RawInputButtonLeft;
    keyCodeMap[SDLK_DOWN] = InputMapping::e_RawInputButtonDown;
    keyCodeMap[SDLK_UP] = InputMapping::e_RawInputButtonUp;

    keyCodeMap[SDLK_NUMLOCKCLEAR] = InputMapping::e_RawInputButtonNumLock;
    keyCodeMap[SDLK_KP_DIVIDE] = InputMapping::e_RawInputButtonKeyPadDivide;
    keyCodeMap[SDLK_KP_MULTIPLY] = InputMapping::e_RawInputButtonKeyPadMultiply;
    keyCodeMap[SDLK_KP_MINUS] = InputMapping::e_RawInputButtonKeyPadMinus;
    keyCodeMap[SDLK_KP_PLUS] = InputMapping::e_RawInputButtonKeyPadPlus;
    keyCodeMap[SDLK_KP_ENTER] = InputMapping::e_RawInputButtonKeyPadEnter;
    keyCodeMap[SDLK_KP_1] = InputMapping::e_RawInputButtonKeyPad1;
    keyCodeMap[SDLK_KP_2] = InputMapping::e_RawInputButtonKeyPad2;
    keyCodeMap[SDLK_KP_3] = InputMapping::e_RawInputButtonKeyPad3;
    keyCodeMap[SDLK_KP_4] = InputMapping::e_RawInputButtonKeyPad4;
    keyCodeMap[SDLK_KP_5] = InputMapping::e_RawInputButtonKeyPad5;
    keyCodeMap[SDLK_KP_6] = InputMapping::e_RawInputButtonKeyPad6;
    keyCodeMap[SDLK_KP_7] = InputMapping::e_RawInputButtonKeyPad7;
    keyCodeMap[SDLK_KP_8] = InputMapping::e_RawInputButtonKeyPad8;
    keyCodeMap[SDLK_KP_9] = InputMapping::e_RawInputButtonKeyPad9;
    keyCodeMap[SDLK_KP_0] = InputMapping::e_RawInputButtonKeyPad0;
    keyCodeMap[SDLK_KP_PERIOD] = InputMapping::e_RawInputButtonKeyPadPeriod;

    keyCodeMap[SDLK_LCTRL] = InputMapping::e_RawInputButtonLeftControl;
    keyCodeMap[SDLK_LSHIFT] = InputMapping::e_RawInputButtonLeftShift;
    keyCodeMap[SDLK_LALT] = InputMapping::e_RawInputButtonLeftAlt;
    keyCodeMap[SDLK_RCTRL] = InputMapping::e_RawInputButtonRightControl;
    keyCodeMap[SDLK_RSHIFT] = InputMapping::e_RawInputButtonRightShift;
    keyCodeMap[SDLK_RALT] = InputMapping::e_RawInputButtonRightAlt;
    keyCodeMap[SDLK_LGUI] = InputMapping::e_RawInputButtonLeftMeta;
    keyCodeMap[SDLK_RGUI] = InputMapping::e_RawInputButtonRightMeta;
}

//-----------------------------------------------------------------------

bool WindowManager::_MapSDLKeyCodeToRawInputButton(SDL_Keycode sdlKeyCode,
                                                   InputMapping::RawInputButton& outRawInputButton)
{
    bool retVal = false;

    SDLKeyCodesToRawInputButtonsMap::iterator it = m_KeyCodeMap.find( sdlKeyCode );

    if( it != m_KeyCodeMap.end() )
    {
        outRawInputButton = it->second;
        retVal = true;
    }

    return  retVal;
}

//-----------------------------------------------------------------------

#ifdef VR_BACKEND_ENABLED
void WindowManager::_GetVRSecondaryWindowPositionAndSize(bool fullScreen, int& outXPos, int& outYPos, int& outWidth, int& outHeight)
{
    const int DefaultWindowWidth = 640;
    const int DefaultWindowHeight = 480;
    const int displayIndex = 0;

    SDL_DisplayMode modeOfTheFirstDisplay;
    if( SDL_GetDesktopDisplayMode( displayIndex, &modeOfTheFirstDisplay ) == 0 )
    {
        outXPos = SDL_WINDOWPOS_CENTERED_DISPLAY( displayIndex );
        outYPos = SDL_WINDOWPOS_CENTERED_DISPLAY( displayIndex );

        if( fullScreen )
        {
            outWidth = modeOfTheFirstDisplay.w;
            outHeight = modeOfTheFirstDisplay.h;
        }
        else
        {
            outWidth = DefaultWindowWidth;
            outHeight = DefaultWindowHeight;

            if( modeOfTheFirstDisplay.w < DefaultWindowWidth )
            {
                outWidth = modeOfTheFirstDisplay.w;
            }

            if( modeOfTheFirstDisplay.h < DefaultWindowHeight)
            {
                outHeight = modeOfTheFirstDisplay.h;
            }
        }
    }
    else
    {
        throw GenericException("Error(SDL failed to get DesktopDisplayMode for display " + std::to_string( displayIndex ) + "): " + std::string(SDL_GetError()));
    }
}
#endif

//-----------------------------------------------------------------------

inline void WindowManager::_SetupSDLWindow(SDL_Window*& window,
                                           SDL_GLContext *context,
                                           size_t width,
                                           size_t height,
                                           std::string title,
                                           bool fullScreen,
                                           bool enableMultisampling,
                                           bool enableVSync)
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0) //Initialize SDL's Video subsystem
    {
        MsgLogger::log(MsgLogger::ERROR_MSG,"SDL initialization failed: "+std::string(SDL_GetError()));
        SDL_Quit();
        exit(1);
    }


    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    // Require R8G8B8A8 colour buffers
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8 );
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8 );
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8 );

    // Enable a 24bit Z buffer.
    // This may need to be changed to 16 or 32 depending a given system
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24 );
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8 );

#ifdef OPENGL_ES
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
#else
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS,
                        SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG
#ifndef NDEBUG
                        | SDL_GL_CONTEXT_DEBUG_FLAG
#endif
                        );
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
#endif

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

    if( enableMultisampling )
    {
        // Enable multisampling for a nice antialiased effect
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
    }
    else
    {
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 0);
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 0);
    }


    int windowXPos = SDL_WINDOWPOS_CENTERED;
    int windowYPos = SDL_WINDOWPOS_CENTERED;
    int windowWidth = static_cast<int>( width );
    int windowHeight = static_cast<int>( height );
    Uint32 windowParamFlags = ( SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );

#ifdef VR_BACKEND_ENABLED
    if ( m_VRBackend != nullptr )
    {
        _GetVRSecondaryWindowPositionAndSize( fullScreen, windowXPos, windowYPos, windowWidth, windowHeight );
    }

    // Resizing makes sence only if we are not doing VR rendering
    if( m_VRBackend == nullptr )
#endif
    {
        windowParamFlags |= SDL_WINDOW_RESIZABLE;
    }

    if( fullScreen )
    {
        windowParamFlags |= SDL_WINDOW_FULLSCREEN;
    }
       
    // Create our window centered at a given resolution
    window = SDL_CreateWindow( title.c_str(), 
                               windowXPos, windowYPos,
                               windowWidth, windowHeight,
			                   windowParamFlags );


    if ( !window ) // Die if creation failed
    {
        throw GenericException("Error(SDL is unable to create window) " + std::string(SDL_GetError()));
    }

    m_AppWindowID = SDL_GetWindowID( window );

    // Create our opengl context and attach it to our window
    *context = SDL_GL_CreateContext( window );

    if( !*context )
    {
        throw GenericException("Error(SDL is unable to create OpenGL context): " + std::string(SDL_GetError()));
    }
    else
    {
        bool ok = true;

        if ( SDL_GL_SetSwapInterval( enableVSync ? 1 : 0) != 0 )
        {
            throw GenericException("Failed to enable VSync! SDL Error: "+std::string(SDL_GetError()));
        }

        const int swapInterval = SDL_GL_GetSwapInterval();

        std::string vSyncmsg = "(Context) VSync: ";
        if( swapInterval == 0 )
        {
            vSyncmsg += "Off";
        }
        else if( swapInterval == 1 )
        {
            vSyncmsg += "On";
        }
        else
        {
            vSyncmsg += "Unknown";
        }

        MsgLogger::LogEvent( vSyncmsg );

        int glContextProfile = 0;

        ok = !SDL_GL_GetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, &glContextProfile );

        if( ok )
        {
            std::string msg = "(Context) GL Profile: ";

            if( glContextProfile == SDL_GL_CONTEXT_PROFILE_CORE )
            {
                msg += "Core";
            }
            else if( glContextProfile == SDL_GL_CONTEXT_PROFILE_COMPATIBILITY )
            {
                msg += "Compatibility";
            }
            else if( glContextProfile == SDL_GL_CONTEXT_PROFILE_ES )
            {
                msg += "ES";
            }
            else
            {
                msg += "Unknown";
            }

            MsgLogger::LogEvent( msg );
        }

        int majorGLVersion = 0;
        int minorGLVersion = 0;

        ok = ok && !SDL_GL_GetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, &majorGLVersion );
        ok = ok && !SDL_GL_GetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, &minorGLVersion );

        if( ok )
        {
            std::string msg = "(Context) GL Version: ";
            msg += StringUitls::NumToString( majorGLVersion );
            msg += ".";
            msg += StringUitls::NumToString( minorGLVersion );

            MsgLogger::LogEvent( msg );
        }

        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

        // Require R8G8B8A8 colour buffers
        SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8 );
        SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8 );
        SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8 );

        // Enable a 24bit Z buffer.
        // This may need to be changed to 16 or 32 depending a given system
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24 );
        SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8 );

#if defined( OS_ANDROID ) || !defined( OPENGL_ES )
        int doubleBuffer = 0;
#else
        int doubleBuffer = 1;
#endif
        int redSize = 0;
        int greenSize = 0;
        int blueSize = 0;
        int alphaSize = 0;
        int depthSize = 0;
        int stencilSize = 0;

// Getting SDL_GL_DOUBLEBUFFER attribute does not work on PCs when using OpenGL ES context due
// to SDL compilation defines assuming OpenGL and not OpenGL ES is used on PCs
#if defined( OS_ANDROID ) || !defined( OPENGL_ES )
        ok = ok && !SDL_GL_GetAttribute( SDL_GL_DOUBLEBUFFER, &doubleBuffer );
#endif
        ok = ok && !SDL_GL_GetAttribute( SDL_GL_RED_SIZE, &redSize );
        ok = ok && !SDL_GL_GetAttribute( SDL_GL_GREEN_SIZE, &greenSize );
        ok = ok && !SDL_GL_GetAttribute( SDL_GL_BLUE_SIZE, &blueSize );
        ok = ok && !SDL_GL_GetAttribute( SDL_GL_ALPHA_SIZE, &alphaSize );
        ok = ok && !SDL_GL_GetAttribute( SDL_GL_DEPTH_SIZE, &depthSize );
        ok = ok && !SDL_GL_GetAttribute( SDL_GL_STENCIL_SIZE, &stencilSize );

        if( ok )
        {
            MsgLogger::LogEvent( "(Context) Double Buffered: " + StringUitls::NumToString( doubleBuffer ) );

            std::string msg = "(Context) Color Buffer:";
            msg += " R";
            msg += StringUitls::NumToString( redSize );
            msg += " G";
            msg += StringUitls::NumToString( greenSize );
            msg += " B";
            msg += StringUitls::NumToString( blueSize );
            msg += " A";
            msg += StringUitls::NumToString( alphaSize );

            MsgLogger::LogEvent( msg );
            MsgLogger::LogEvent( "(Context) Depth Bits: " + StringUitls::NumToString( depthSize ) );
            MsgLogger::LogEvent( "(Context) Stencil Bits: " + StringUitls::NumToString( stencilSize ) );
        }

        if( !ok )
        {
            MsgLogger::LogError( "Failed to get GL attribute values with error: " + std::string( SDL_GetError() ) );
        }
    }
}

//-----------------------------------------------------------------------

void WindowManager::_SetupWindow(size_t width,
                                size_t height,
                                std::string title,
                                bool fullScreen,
                                bool enableMultisampling,
                                bool enableVSync)
{
    m_OpenGL_Context = new SDL_GLContext();
    _SetupSDLWindow( m_AppWindow, m_OpenGL_Context, width, height, title, fullScreen, enableMultisampling, enableVSync );

    // Process events in case window resize event has been generated while creating the window
    ProcessWindowEvents();
}

//-----------------------------------------------------------------------
