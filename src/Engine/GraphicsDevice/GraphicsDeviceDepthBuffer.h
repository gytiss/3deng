#ifndef GRAPHICSDEVICEDEPTHBUFFER_H_INCLUDED
#define GRAPHICSDEVICEDEPTHBUFFER_H_INCLUDED

#include <stack>
#include <OpenGLIncludes.h>

class GraphicsDeviceDepthBuffer
{
public:
    friend class GraphicsDevice;

    enum DepthFunction
    {
        e_DepthFunctionNever = GL_NEVER,
        e_DepthFunctionLess = GL_LESS,
        e_DepthFunctionEqual = GL_EQUAL,
        e_DepthFunctionLessThanOrEqual = GL_LEQUAL,
        e_DepthFunctionGreater = GL_GREATER,
        e_DepthFunctionNotEqual = GL_NOTEQUAL,
        e_DepthFunctionGreaterThanOrEqual = GL_GEQUAL,
        e_DepthFunctionAlways = GL_ALWAYS
    };

    void SaveCurrentDepthBufferWritingState();
    void RestorePreviousDepthBufferWritingState();
    void EnableDepthBufferWriting();
    void DisableDepthBufferWriting();

    void SaveCurrentDepthFunction();
    void RestorePreviousDepthFunction();
    void SetDepthFunction(DepthFunction depthFunction);

private:
    GraphicsDeviceDepthBuffer();

    std::stack<bool> m_DepthBufferWriteEnabledStack;
    bool m_DepthBufferWriteEnabled;

    std::stack<DepthFunction> m_DepthFunctionsStack;
    DepthFunction m_CurrentDepthFunction;
};

#endif // GRAPHICSDEVICEDEPTHBUFFER_H_INCLUDED
