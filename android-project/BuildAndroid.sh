#/usr/bin/env bash 

pushd `dirname $0` > /dev/null # Save script call dir and "cd" into the script dir
SCRIPT_DIR=`pwd`
popd > /dev/null # "cd" back into the script call dir

export PATH=$PATH:/home/john/Android/Sdk/tools/:/home/john/Android/Sdk/platform-tools/:/home/john/Android/android-ndk-r10d/
export ANDROID_NDK_HOME=/home/john/Android/android-ndk-r10d/
export ANDROID_SDK_HOME=/home/john/Android/Sdk/

cd $SCRIPT_DIR

android update project --path . --target android-21

ndk-build -j 4 "$@"
$SCRIPT_DIR/../BuildLibsForAndroid.sh --targets="armv7" --buildtype=Debug --mainapponly --preservemainappbuildfiles
$SCRIPT_DIR/FetchAssetData.sh
rm -rf $SCRIPT_DIR/bin/TNKActivity.apk

#ant debug

#adb uninstall org.libsdl.app
#adb install -l bin/SDLActivity-debug-unaligned.apk

exit 0