#include "EntityMarker.h"
#include <glm/gtc/matrix_transform.hpp>
#include <Scene/MeshRenderer.h>

//-----------------------------------------------------------------------

EntityMarker::EntityMarker(PhysicsManager& physicsManager,
                           MaterialManager& materialManager,
                           TextureManager& textureManager)
: GameObject( "EntityMarker" ),
  m_Color( 1.0f, 1.0f, 1.0f ),
  m_VAO(),
  m_PhysicsManager( physicsManager ),
  m_MaterialManager( materialManager ),
  m_TextureManager( textureManager ),
  m_MaterialHandle()
{
    new MeshRenderer( *this, m_MaterialManager );
}

//-----------------------------------------------------------------------

EntityMarker::~EntityMarker()
{
    if( m_MaterialHandle.IsValid() )
    {
        m_MaterialManager.ReleaseMaterial( m_MaterialHandle );
    }
}

//-----------------------------------------------------------------------

size_t EntityMarker::_GetNumCircleSegments(float r) const
{
    const float c_CircleQuality = 45.0f;
    return static_cast<size_t>( c_CircleQuality * sqrtf( r ) );
}

//-----------------------------------------------------------------------

size_t EntityMarker::_GenerateCircleVertices(CircleType type,
                                              float cx,
                                              float cy,
                                              float r,
                                              size_t numOfSegments,
                                              VertexArray_t& vertArray)
{
    glm::mat4 rotate90DegressOnYAxis = glm::mat4( 1.0f );
    rotate90DegressOnYAxis = glm::rotate( rotate90DegressOnYAxis, 90.0f, glm::vec3(0.0f, 1.0f, 0.0f) );

    const float theta = ( ( 2.0f * static_cast<float>( M_PI ) )  / static_cast<float>( numOfSegments ) );
    const float c = cosf( theta ); // Precalculate the sine and cosine
    const float s = sinf( theta );

    float x = r; // We start at angle = 0
    float y = 0;

    for(size_t i = 0; i < numOfSegments; i++)
    {
        EntityMarkerVertex v;
        if( type == e_CircleTypeVertical )
        {
            v.position[0] = ( x + cx );
            v.position[1] = (y + cy);
            v.position[2] = 0.0f;
        }
        else if( type == e_CircleTypeHorizontal )
        {
            v.position[0] = ( x + cx );
            v.position[1] = cy;
            v.position[2] = (y);
        }
        else if( type == e_CircleTypeVerticalRotated90Degrees )
        {
            glm::vec4 pos = glm::vec4( ( x + cx ), (y + cy), 0.0f, 1.0f );
            pos = rotate90DegressOnYAxis * pos;
            v.position[0] = pos.x;
            v.position[1] = pos.y;
            v.position[2] = pos.z;
        }

        vertArray.push_back( v );

        // Apply the rotation matrix
        float t = x;
        x = c * x - s * y;
        y = s * t + c * y;
    }

    return numOfSegments;
}

//-----------------------------------------------------------------------

void EntityMarker::_RecalculateRenderingData()
{
    if( m_MaterialHandle.IsValid() )
    {
        m_MaterialManager.ReleaseMaterial( m_MaterialHandle );
    }

    MaterialDescriptor materialDescriptor( m_TextureManager );
    materialDescriptor.SetDiffuseColor( m_Color );
    m_MaterialHandle = m_MaterialManager.AddInternalMaterial( materialDescriptor );

    RecalculateRenderables();
}
