#include "DepthTexture.h"
#include "FBO.h"

#include <cassert>

//-----------------------------------------------------------------------

DepthTexture::DepthTexture(GraphicsDevice& graphicsDevice,
                           SystemInterface& systemInterface,
                           TypeIdx textureTypeIdx)
: Texture( graphicsDevice, textureTypeIdx, Texture::e_Class2DTexture, false ),
  m_FBOsAttachedTo(),
  m_SystemInterface( systemInterface )
{
    const SystemInterface::DrawableSurfaceSize& drawableSurfaceSize = systemInterface.GetDrawableSurfaceSize();

    Texture::InitializeStorage( e_InternalDataFormatDepth32Stencil8,
                                drawableSurfaceSize.widthInPixels,
                                drawableSurfaceSize.heightInPixels );

    m_SystemInterface.RegisterDrawableSurfaceSizeChangedCallback( _DrawableSurfaceSizeChaged, this );
}

//-----------------------------------------------------------------------

DepthTexture::~DepthTexture()
{
    m_SystemInterface.DeregisterDrawableSurfaceSizeChangedCallback( _DrawableSurfaceSizeChaged, this );
}

//-----------------------------------------------------------------------

void DepthTexture::RegisterFBO(FBO* fbo)
{
    assert( ( fbo != nullptr ) && "FBO must not be null" );

    m_FBOsAttachedTo.push_back( fbo );
}

//-----------------------------------------------------------------------

void DepthTexture::DregisterFBO(FBO* fbo)
{
    assert( ( fbo != nullptr ) && "FBO must not be null" );

    bool done = false;

    FBOList::iterator it = m_FBOsAttachedTo.begin();
    while( !done && ( it != m_FBOsAttachedTo.end() ) )
    {
        if( *it == fbo )
        {
            m_FBOsAttachedTo.erase( it );
            done = true;
        }
        else
        {
            ++it;
        }
    }
}

//-----------------------------------------------------------------------

void DepthTexture::_Reinitialize(const SystemInterface::DrawableSurfaceSize& drawableSurfaceSize)
{
    Texture::Reset();
    Texture::InitializeStorage( e_InternalDataFormatDepth32Stencil8,
                                drawableSurfaceSize.widthInPixels,
                                drawableSurfaceSize.heightInPixels );

    for(FBOList::iterator it = m_FBOsAttachedTo.begin(); it != m_FBOsAttachedTo.end(); ++it)
    {
        (*it)->DepthTextureHasBeenReset();
    }
}

//-----------------------------------------------------------------------

void DepthTexture::_DrawableSurfaceSizeChaged(const SystemInterface::DrawableSurfaceSize& newSize, void* userData)
{
    static_cast<DepthTexture*>( userData )->_Reinitialize( newSize );
}

//-----------------------------------------------------------------------
