#!/usr/bin/env bash 

pushd `dirname $0` > /dev/null # Save script call dir and "cd" into the script dir
SCRIPT_DIR=`pwd`
popd > /dev/null # "cd" back into the script call dir

source "$SCRIPT_DIR/AndroidBuildCommon.sh"

if [ -z "$ANDROID_NDK_HOME" ]; then
    echo "You should probably set ANDROID_NDK_HOME to the directory containing"
    echo "the Android NDK"
    exit
fi

if [ ! -f $BOOST_JAM_EXECUTABLE_DIR/b2 ]; then
    echo "Can not find  'b2' in the $BOOST_JAM_EXECUTABLE_DIR directory"
    exit 1
fi

# echo "########### COM: "$BOOST_JAM_EXECUTABLE_DIR
# echo "########### AR: : "$TARGET_ARCH
# echo "########### PREF: "$PREFIX
# echo "########### ARN: "$TARGET_ARCH_NAME
# echo "########### NDK: "$NDK_PLATFORM

# echo "########### CFLAGS: "$CFLAGS
# echo "########### CXXFLAGS: "$CXXFLAGS
# echo "########### LDFLAGS: "$LDFLAGS


if [ "x$TARGET_ARCH" = 'x' ] || [ "x$BOOST_JAM_EXECUTABLE_DIR" = 'x' ] || [ "x$PREFIX" = 'x' ] || [ "x$TARGET_ARCH_NAME" = 'x' ] || [ "x$NDK_PLATFORM" = 'x' ]; then
    echo "You shouldn't use $0 directly"
    exit 1
fi

export PREFIX=$PREFIX
export MAKE_TOOLCHAIN="${ANDROID_NDK_HOME}/build/tools/make-standalone-toolchain.sh"
export TOOLCHAIN_DIR="$(pwd)/android-toolchain-${TARGET_ARCH_NAME}"
export PATH="${PATH}:${TOOLCHAIN_DIR}/bin"

# Clean up before build
rm -rf "${TOOLCHAIN_DIR}"

# NOTE: MANIFEST_TOOL=/bin/true is provided to the below configure script to sidestep the problem where autotools insists on requiring the manifest tool "mt" to be present even though it is Windows only tool (i.e. not used on Linux)

export AndroidStandaloneToolchainDir=$TOOLCHAIN_DIR

DESIRED_BOOST_USER_CONFIG=$SCRIPT_DIR/BJamUserConfigs/${TARGET_ARCH_NAME}.jam
BOOST_USER_CONFIG_PATH=$BOOST_JAM_EXECUTABLE_DIR/tools/build/src/user-config.jam

if [ ! -f $DESIRED_BOOST_USER_CONFIG ]; then
    echo "File $DESIRED_BOOST_USER_CONFIG does not exist"
    exit 1
fi

# Delete  any existing user config
rm -rf $BOOST_USER_CONFIG_PATH

# Symlink desired boost user config to the boost user config dir 
ln -s $DESIRED_BOOST_USER_CONFIG $BOOST_USER_CONFIG_PATH


$MAKE_TOOLCHAIN --platform="${NDK_PLATFORM}" \
                --arch="$TARGET_ARCH" \
                --stl=gnustl \
                --install-dir="$TOOLCHAIN_DIR" && \
    $BOOST_JAM_EXECUTABLE_DIR/b2 -q \
        target-os=android \
        toolset="gcc-$TARGET_ARCH_NAME" \
        $(ReplaceArchMarkersWithArchName "$BJAM_FLAGS" $TARGET_ARCH_NAME) \
        --prefix=$PREFIX \
        install 2>&1 \
        || { rm -rf "${TOOLCHAIN_DIR}" ; rm -rf $BOOST_USER_CONFIG_PATH ; exit 1 ; }

# Cleanup by deleting created toolchain and symlinked boost user config
rm -rf "${TOOLCHAIN_DIR}" 
rm -rf $BOOST_USER_CONFIG_PATH
