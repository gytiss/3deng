#include "ShaderPrograms.h"
#include <GraphicsDevice/VBO.h>
#include "GBuffer.h"

//-----------------------------------------------------------------------

ShaderProgGenericTextured::ShaderProgGenericTextured(FileManager& fileManager)
: ShaderProg( fileManager,
              "gpuSkinningNoNormalsDebug",
              "Data/Shaders/TexturedShader.vert",
              "Data/Shaders/TexturedShader.frag",
              VBO::e_VertexAttrPosition |
              VBO::e_VertexAttrTextureCoords,
              Texture::e_TypeDiffuseMap ),
  ShaderProgUniformModelViewProjectionMatrix( this )
{

}

//-----------------------------------------------------------------------

ShaderProgEntityMarker::ShaderProgEntityMarker(FileManager& fileManager)
: ShaderProg( fileManager,
              "EntityMarkerShaderProg",
              "Data/Shaders/EntityMarkerShaderProg.vert",
              "Data/Shaders/EntityMarkerShaderProg.frag",
              VBO::e_VertexAttrPosition ),
  ShaderProgUniformModelViewProjectionMatrix( this ),
  ShaderProgUniformColor( this )
{

};

//-----------------------------------------------------------------------

ShaderProgGBuffer::ShaderProgGBuffer(FileManager& fileManager,
                                     unsigned int vertexAttributes,
                                     const std::string& vertexShaderDefines,
                                     const std::string& fragmentShaderDefines)
: ShaderProg( fileManager,
              "GBufferShaderProg",
              "Data/Shaders/GBufferShader.vert",
              "Data/Shaders/GBufferShader.frag",
              VBO::e_VertexAttrPosition |
              VBO::e_VertexAttrNormal |
              VBO::e_VertexAttrBiTangent |
              VBO::e_VertexAttrTangent |
              VBO::e_VertexAttrTextureCoords |
              vertexAttributes,
              Texture::e_TypeDiffuseMap |
              Texture::e_TypeNormalMap |
              Texture::e_TypeSpecularMap,
              vertexShaderDefines,
              fragmentShaderDefines ),
  ShaderProgUniformModelViewProjectionMatrix( this ),
  ShaderProgUniformNormalMatrix( this )
{

}

//-----------------------------------------------------------------------

ShaderProgGBufferSkinned::ShaderProgGBufferSkinned(FileManager& fileManager)
: ShaderProgGBuffer( fileManager,
                     VBO::e_VertexAttrBoneWeights |
                     VBO::e_VertexAttrBoneIndices,
                    "RENDERING_SKINNED_MESH",
                    "" ),
  ShaderProgUniformJointMatrices( this )
{

};

//-----------------------------------------------------------------------

ShaderProgGBufferStatic::ShaderProgGBufferStatic(FileManager& fileManager)
: ShaderProgGBuffer( fileManager, 0, "", "" )
{

};

//-----------------------------------------------------------------------

ShaderProgPointLightUberShader::ShaderProgPointLightUberShader(FileManager& fileManager)
: ShaderProg( fileManager,
              "PointLightUberShaderProg",
              "Data/Shaders/DeferredLightShader.vert",
              "Data/Shaders/DeferredLightShader.frag",
              VBO::e_VertexAttrPosition,
              Texture::e_TypeGBufferDiffuseMap |
              Texture::e_TypeGBufferVertexNormalMapViewSpace |
              Texture::e_TypeDepthTexture,
              "",
              "POINT_LIGHT" ),
  ShaderProgUniformModelViewProjectionMatrix( this ),
  ShaderProgUniformModelViewMatrix( this ),
  ShaderProgUniformProjectionA( this ),
  ShaderProgUniformProjectionB( this ),
  ShaderProgUniformScreenSize( this ),
  ShaderProgUniformLightColor( this ),
  ShaderProgUniformLightPositionInViewSpace( this ),
  ShaderProgUniformLightRadius( this ),
  ShaderProgUniformLightAmbientIntensity( this ),
  ShaderProgUniformLightDiffuseIntensity( this )
{

};

//-----------------------------------------------------------------------

ShaderProgDirectionalLightUberShader::ShaderProgDirectionalLightUberShader(FileManager& fileManager)
: ShaderProg( fileManager,
              "DirectionalLightUberShaderProg",
              "Data/Shaders/DeferredLightShader.vert",
              "Data/Shaders/DeferredLightShader.frag",
              VBO::e_VertexAttrPosition,
              Texture::e_TypeGBufferDiffuseMap |
              Texture::e_TypeGBufferVertexNormalMapViewSpace |
              Texture::e_TypeDepthTexture,
              "DIRECTIONAL_LIGHT",
              "DIRECTIONAL_LIGHT" ),
  ShaderProgUniformInverseProjectionMatrix( this ),
  ShaderProgUniformProjectionA( this ),
  ShaderProgUniformProjectionB( this ),
  ShaderProgUniformScreenSize( this ),
  ShaderProgUniformLightColor( this ),
  ShaderProgUniformLightDirectionInViewSpace( this ),
  ShaderProgUniformLightAmbientIntensity( this ),
  ShaderProgUniformLightDiffuseIntensity( this )
{

};

//-----------------------------------------------------------------------

ShaderProgSpotLightUberShader::ShaderProgSpotLightUberShader(FileManager& fileManager)
: ShaderProg( fileManager,
              "SpotLightUberShaderProg",
              "Data/Shaders/DeferredLightShader.vert",
              "Data/Shaders/DeferredLightShader.frag",
              VBO::e_VertexAttrPosition,
              Texture::e_TypeGBufferDiffuseMap |
              Texture::e_TypeGBufferVertexNormalMapViewSpace |
              Texture::e_TypeDepthTexture,
              "",
              "SPOT_LIGHT" ),
  ShaderProgUniformModelViewProjectionMatrix( this ),
  ShaderProgUniformModelViewMatrix( this ),
  ShaderProgUniformProjectionA( this ),
  ShaderProgUniformProjectionB( this ),
  ShaderProgUniformScreenSize( this ),
  ShaderProgUniformLightColor( this ),
  ShaderProgUniformLightDirectionInViewSpace( this ),
  ShaderProgUniformLightPositionInViewSpace( this ),
  ShaderProgUniformLightRadius( this ),
  ShaderProgUniformLightAmbientIntensity( this ),
  ShaderProgUniformLightDiffuseIntensity( this ),
  ShaderProgUniformLightCutoffValue( this )
{

};

//-----------------------------------------------------------------------

ShaderProgLightingStencilPass::ShaderProgLightingStencilPass(FileManager& fileManager)
: ShaderProg( fileManager,
              "LightingStencilPassShaderProg",
              "Data/Shaders/DeferredLightShader.vert",
              "Data/Shaders/NullShader.frag",
              VBO::e_VertexAttrPosition ),
  ShaderProgUniformModelViewProjectionMatrix( this )
{

};

//-----------------------------------------------------------------------

ShaderProgToneMappingAndGammaCorrection::ShaderProgToneMappingAndGammaCorrection(FileManager& fileManager)
: ShaderProg( fileManager,
              "ToneMappingAndGammaCorrectionShaderProg",
              "Data/Shaders/Passthrough.vert",
              "Data/Shaders/ToneMappingAndGammaCorrection.frag",
              VBO::e_VertexAttrPosition,
              Texture::e_TypePostProcessTextureMap ),
  ShaderProgUniformScreenSize( this )
{

};

//-----------------------------------------------------------------------

ShaderProgDebugDepth::ShaderProgDebugDepth(FileManager& fileManager)
: ShaderProg( fileManager,
              "ToneMappingAndGammaCorrectionShaderProg",
              "Data/Shaders/Passthrough.vert",
              "Data/Shaders/DepthDebug.frag",
              VBO::e_VertexAttrPosition,
              Texture::e_TypeDepthTexture ),
  ShaderProgUniformScreenSize( this ),
  ShaderProgUniformProjectionA( this ),
  ShaderProgUniformProjectionB( this )
{

};

//-----------------------------------------------------------------------

ShaderProgDebugObjects::ShaderProgDebugObjects(FileManager& fileManager)
: ShaderProg( fileManager,
              "DebugObjectsShaderProg",
              "Data/Shaders/DebugLinesShader.vert",
              "Data/Shaders/DebugLinesShader.frag",
              VBO::e_VertexAttrPosition |
              VBO::e_VertexAttrColor ),
  ShaderProgUniformModelViewProjectionMatrix( this ),
  ShaderProgUniformColor( this ),
  ShaderProgUniformUseVertexColors( this )
{

};

//-----------------------------------------------------------------------

ForwardRenderingShader::ForwardRenderingShader(FileManager& fileManager,
                                               unsigned int vertexAttributes,
                                               const std::string& vertexShaderDefines,
                                               const std::string& fragmentShaderDefines)
: ShaderProg( fileManager,
              "ForwardRenderingShaderProg",
              "Data/Shaders/ForwardRenderingShader.vert",
              "Data/Shaders/ForwardRenderingShader.frag",
              VBO::e_VertexAttrPosition |
              VBO::e_VertexAttrNormal |
              VBO::e_VertexAttrBiTangent |
              VBO::e_VertexAttrTangent |
              VBO::e_VertexAttrTextureCoords |
              vertexAttributes,
              Texture::e_TypeDiffuseMap |
              Texture::e_TypeNormalMap |
              Texture::e_TypeSpecularMap,
              vertexShaderDefines,
              fragmentShaderDefines ),
  ShaderProgUniformModelViewProjectionMatrix( this ),
  ShaderProgUniformViewMatrix( this ),
  ShaderProgUniformModelMatrix( this ),
  ShaderProgUniformNormalMatrix( this ),
  ShaderProgUniformLightColor( this ),
  ShaderProgUniformLightAmbientIntensity( this ),
  ShaderProgUniformLightDiffuseIntensity( this )
{

};

//-----------------------------------------------------------------------

ForwardRenderingPointLightShader::ForwardRenderingPointLightShader(FileManager& fileManager,
                                                                   unsigned int vertexAttributes,
                                                                   const std::string& vertexShaderDefines,
                                                                   const std::string& fragmentShaderDefines)
: ForwardRenderingShader( fileManager,
                          vertexAttributes,
                          vertexShaderDefines,
                          fragmentShaderDefines + " POINT_LIGHT" ),
  ShaderProgUniformLightPositionInViewSpace( this ),
  ShaderProgUniformLightRadius( this )
{

};

//-----------------------------------------------------------------------

ForwardRenderingSpotLightShader::ForwardRenderingSpotLightShader(FileManager& fileManager,
                                                                 unsigned int vertexAttributes,
                                                                 const std::string& vertexShaderDefines,
                                                                 const std::string& fragmentShaderDefines)
: ForwardRenderingShader( fileManager,
                          vertexAttributes,
                          vertexShaderDefines,
                          fragmentShaderDefines + " SPOT_LIGHT" ),
  ShaderProgUniformLightPositionInViewSpace( this ),
  ShaderProgUniformLightDirectionInViewSpace( this ),
  ShaderProgUniformLightCutoffValue( this ),
  ShaderProgUniformLightRadius( this )
{

};

//-----------------------------------------------------------------------

ForwardRenderingDirectionalShader::ForwardRenderingDirectionalShader(FileManager& fileManager,
                                                                     unsigned int vertexAttributes,
                                                                     const std::string& vertexShaderDefines,
                                                                     const std::string& fragmentShaderDefines)
: ForwardRenderingShader( fileManager,
                          vertexAttributes,
                          vertexShaderDefines,
                          fragmentShaderDefines + " DIRECTIONAL_LIGHT" ),
  ShaderProgUniformLightDirectionInViewSpace( this )
{

};

//-----------------------------------------------------------------------

ForwardRenderingNoLightingShader::ForwardRenderingNoLightingShader(FileManager& fileManager,
                                                                   unsigned int vertexAttributes,
                                                                   const std::string& vertexShaderDefines,
                                                                   const std::string& fragmentShaderDefines)
: ShaderProg( fileManager,
              "ForwardRenderingNoLightingShaderProg",
              "Data/Shaders/ForwardRenderingNoLightingShader.vert",
              "Data/Shaders/ForwardRenderingNoLightingShader.frag",
              VBO::e_VertexAttrPosition |
              VBO::e_VertexAttrTextureCoords |
              vertexAttributes,
              Texture::e_TypeDiffuseMap,
              vertexShaderDefines,
              fragmentShaderDefines ),
  ShaderProgUniformModelViewProjectionMatrix( this )
{

};

//-----------------------------------------------------------------------

ForwardRenderingNoLightingShaderSkinned::ForwardRenderingNoLightingShaderSkinned(FileManager& fileManager)
: ForwardRenderingNoLightingShader( fileManager,
                                    VBO::e_VertexAttrBoneWeights |
                                    VBO::e_VertexAttrBoneIndices,
                                    "RENDERING_SKINNED_MESH",
                                    "" ),
  ShaderProgUniformJointMatrices( this )
{

};

//-----------------------------------------------------------------------

ForwardRenderingNoLightingShaderStatic::ForwardRenderingNoLightingShaderStatic(FileManager& fileManager)
: ForwardRenderingNoLightingShader( fileManager, 0, "", "" )
{

};

//-----------------------------------------------------------------------

ShaderProgGUI::ShaderProgGUI(FileManager& fileManager)
: ShaderProg( fileManager,
              "ShaderProgGUI",
              "Data/Shaders/GUIShader.vert",
              "Data/Shaders/GUIShader.frag",
              VBO::e_VertexAttrPosition |
              VBO::e_VertexAttrColor |
              VBO::e_VertexAttrTextureCoords,
              Texture::e_TypeDiffuseMap ),
  ShaderProgUniformModelMatrix( this ),
  ShaderProgUniformUseTexture( this )
{

};

//-----------------------------------------------------------------------

ShaderProgText::ShaderProgText(FileManager& fileManager)
: ShaderProg( fileManager,
              "ShaderProgText",
              "Data/Shaders/TextShader.vert",
              "Data/Shaders/TextShader.frag",
              VBO::e_VertexAttrPosition |
              VBO::e_VertexAttrTextureCoords,
              Texture::e_TypeDiffuseMap ),
  ShaderProgUniformModelViewProjectionMatrix( this )
{

};

//-----------------------------------------------------------------------
