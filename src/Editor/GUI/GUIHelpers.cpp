#include "GUIHelpers.h"
#include "GUIDefaults.h"
#include "ComponentFloatValueValidator.h"

#include <QtWidgets/QWidget>
#include <QtWidgets/QLineEdit>
#include <CommonDefinitions.h>
#include <cassert>

//-----------------------------------------------------------------------

void GUIHelpers::UseParentsPaletteProperty(QWidget& widget,
                                           QPalette::ColorRole widgetRole)
{
    QWidget* parent = widget.parentWidget();

    if( parent != nullptr )
    {
        QPalette::ColorRole parentBackgroundRole = parent->backgroundRole();

        const QPalette& parentPalette = parent->palette();
        QPalette wigetPalette = widget.palette();

        wigetPalette.setColor( widgetRole, parentPalette.color( parentBackgroundRole ) );
        widget.setPalette( wigetPalette );
    }
}

//-----------------------------------------------------------------------

void GUIHelpers::SetNumericLineEditValue(QLineEdit* lineEdit, float value)
{
    int numberOfDecimalPlacesRequired = GUIDefaults::DefaultMaxDecimalPlacesInTheFinalLineEditValue();
    const ComponentFloatValueValidator* validator = qobject_cast<const ComponentFloatValueValidator*>( lineEdit->validator() );

    if( validator )
    {
        numberOfDecimalPlacesRequired = validator->MaxDecimalPlacedInTheFinalNumber();
    }

    lineEdit->setText( QString::number( value, 'f', numberOfDecimalPlacesRequired ) );
}

//-----------------------------------------------------------------------

float GUIHelpers::GetNumericLineEditValue(QLineEdit* lineEdit)
{
    bool ok = false;

    float retVal = lineEdit->text().toFloat( &ok );

    assert( ok && "Failed to convert QLineEdit value string to a float number" );

    return retVal;
}

//-----------------------------------------------------------------------
