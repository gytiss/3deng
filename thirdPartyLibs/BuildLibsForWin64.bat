@ECHO OFF 

set INITIAL_DIR=%CD%
set SCRIPT_DIR=%~dp0

set SOURCES_DIR=%SCRIPT_DIR%sources
set RESULTS_DIR=%SCRIPT_DIR%windows

set ICU_SOURCES_DIR=%SOURCES_DIR%\icu_58_2\
set BOOST_SOURCES_DIR=%SOURCES_DIR%\boost_1_57_0
set LUA_SOURCES_DIR=%SOURCES_DIR%\lua-5.1.5
set LUABIND_SOURCES_DIR=%SOURCES_DIR%\luabind-0.9.1_rpavlik
set BULLET_SOURCES_DIR=%SOURCES_DIR%\bullet-2.81-rev2613
set FREETYPE_SOURCES_DIR=%SOURCES_DIR%\freetype-2.5.5
set LIBROCKET_SOURCES_DIR=%SOURCES_DIR%\libRocket\Build
set SDL_SOURCES_DIR=%SOURCES_DIR%\SDL2-2.0.5
set GLEW_SOURCES_DIR=%SOURCES_DIR%\glew-1.12.0
set OGG_SOURCES_DIR=%SOURCES_DIR%\libogg-1.3.2
set TREMOR_SOURCES_DIR=%SOURCES_DIR%\tremor
set OPENAL_SOFT_SOURCES_DIR=%SOURCES_DIR%\openal-soft-1.17.2
set OPEN_VR_SOURCES_DIR=%SOURCES_DIR%\openvr-1.0.5


set ICU_INSTALL_DIR=%RESULTS_DIR%\libicu
set BOOST_INSTALL_DIR=%RESULTS_DIR%\libboost
set LUA_INSTALL_DIR=%RESULTS_DIR%\liblua
set LUABIND_INSTALL_DIR=%RESULTS_DIR%\libluabind
set BULLET_INSTALL_DIR=%RESULTS_DIR%\libBullet
set FREETYPE_INSTALL_DIR=%RESULTS_DIR%\libfreetype
set LIBROCKET_INSTALL_DIR=%RESULTS_DIR%\libRocket
set SDL_INSTALL_DIR=%RESULTS_DIR%\libSDL2
set GLEW_INSTALL_DIR=%RESULTS_DIR%\libGLEW
set OGG_INSTALL_DIR=%RESULTS_DIR%\libOgg
set TREMOR_INSTALL_DIR=%RESULTS_DIR%\libTremor
set OPENAL_INSTALL_DIR=%RESULTS_DIR%\libOpenAL
set OPEN_VR_INSTALL_DIR=%RESULTS_DIR%\libOpenVR

set CONTINUE=1


echo Please make sure that the following things are done before continuing:
echo   Visual Studio 14 2015 is installed in "C:\Program Files (x86)\Microsoft Visual Studio 14.0"
echo   cmake-3.7.1-win64-x64 is installed in "C:\Program Files\CMake\" (Can be downloaded from https://cmake.org/download/)
echo   A 64bit version of cygwin is installed in "C:\cygwin64" together with the newest available "autoconf", "autogen", "automake" and "automake wrapper scripts" packages (Can be downloaded from https://cygwin.com/install.html)
echo.
echo.
pause

set "ORIGINAL_PATH_VARIABLE=%PATH%"

set "VISUAL_STUDIO_DIR=C:\Program Files (x86)\Microsoft Visual Studio 14.0"
set "CYGWIN_BINARIES_DIR=C:\cygwin64\bin"
set "CMAKE_BINARIES_DIR=C:\Program Files\CMake\bin"

:: Make sure that all dependencies are satisfied
if NOT exist "%VISUAL_STUDIO_DIR%" (
    echo Required directory "%VISUAL_STUDIO_DIR%" does not exist
    goto COMPILATION_FAILED
)
if NOT exist "%CYGWIN_BINARIES_DIR%" (
    echo Required directory "%CYGWIN_BINARIES_DIR%" does not exist
    goto COMPILATION_FAILED
)
if NOT exist "%CMAKE_BINARIES_DIR%\cmake.exe" (
    echo cmake.exe not found in the directory "%CMAKE_BINARIES_DIR%"
    goto COMPILATION_FAILED
)

if %CONTINUE% NEQ 1 (
    goto COMPILATION_FAILED
)

    ::#------- Building ICU 58.2 -----

    :: Add Cygwin to the system path
    set "PATH=%PATH%;%CYGWIN_BINARIES_DIR%"
    
    rmdir /S /Q %ICU_INSTALL_DIR%
    
    ::#----- Release -----
    mkdir %ICU_SOURCES_DIR%\build
    cd %ICU_SOURCES_DIR%\build
    
    call "%VISUAL_STUDIO_DIR%\VC\vcvarsall.bat" x86_amd64
    
    :CXXFLAGS="--std=c++11 -DU_USING_ICU_NAMESPACE=0 -DU_CHARSET_IS_UTF8=1" ..\source\runConfigureICU Linux --enable-static --disable-shared --prefix=%ICU_INSTALL_DIR%

    set ICU_INSTALL_PATH="C:\3D\tnk\thirdPartyLibs\windows\libicu"
    for /f %%i in ('cygpath -u %ICU_INSTALL_DIR%') do set ICU_INSTALL_DIR_UNIX_PATH_FORMAT=%%i
    bash ../source/runConfigureICU Cygwin/MSVC --with-library-bits=64 --enable-static=no --enable-shared=yes --prefix=%ICU_INSTALL_DIR_UNIX_PATH_FORMAT%

    :make check
    make install
    
    cd ..\
    rmdir /S /Q build
    ::#-------------------
    
    ::#------- Debug -----
    mkdir %ICU_SOURCES_DIR%\build
    cd %ICU_SOURCES_DIR%\build
    
    call "%VISUAL_STUDIO_DIR%\VC\vcvarsall.bat" x86_amd64
    
    set ICU_INSTALL_PATH="C:\3D\tnk\thirdPartyLibs\windows\libicu"
    for /f %%i in ('cygpath -u %ICU_INSTALL_DIR%') do set ICU_INSTALL_DIR_UNIX_PATH_FORMAT=%%i
    bash ../source/runConfigureICU Cygwin/MSVC --enable-debug --with-library-bits=64 --enable-static=no --enable-shared=yes --prefix=%ICU_INSTALL_DIR_UNIX_PATH_FORMAT%

    :make check
    make install
    
    cd ..\
    rmdir /S /Q build
    ::#-------------------
    
    rename %ICU_INSTALL_DIR%\lib lib64
    
    cd %SCRIPT_DIR%
    
    :: Restore the original system path
    set "PATH=%ORIGINAL_PATH_VARIABLE%"

    ::#-------------------------------
    
    call CheckIfLibExists.bat libicu
    if %ERRORLEVEL% NEQ 0 set CONTINUE=0
    
if %CONTINUE% NEQ 1 (
    goto COMPILATION_FAILED
)

    ::#----- Building Boost 1.57.0 ---

    rmdir /S /Q %BOOST_INSTALL_DIR%
    
    cd %BOOST_SOURCES_DIR%

    rmdir /S /Q bin.v2
    
    call "%VISUAL_STUDIO_DIR%\VC\vcvarsall.bat" x86_amd64
    
    SETLOCAL
    call .\bootstrap.bat --with-icu=%ICU_INSTALL_DIR%
    ENDLOCAL
    
    ::#----- Release -----
    .\b2.exe --prefix=%BOOST_INSTALL_DIR% --without-python --without-mpi -sNO_BZIP2=1 -sHAVE_ICU=1 -sICU_PATH=%ICU_INSTALL_DIR% architecture=x86 address-model=64 link=static variant=release install
    ::#-------------------
    
    ::#------- Debug -----
    .\b2.exe --prefix=%BOOST_INSTALL_DIR% --without-python --without-mpi -sNO_BZIP2=1 -sHAVE_ICU=1 -sICU_PATH=%ICU_INSTALL_DIR% architecture=x86 address-model=64 link=static variant=debug install
    ::#-------------------

    rmdir /S /Q bin.v2
    
    move /Y %BOOST_INSTALL_DIR%/include/boost-1_57/boost %BOOST_INSTALL_DIR%/include/
    rmdir /S /Q %BOOST_INSTALL_DIR%/include/boost-1_57
    
    cd %SCRIPT_DIR%

    ::#-------------------------------
    
    call CheckIfLibExists.bat libboost
    if %ERRORLEVEL% NEQ 0 set CONTINUE=0


if %CONTINUE% NEQ 1 (
    goto COMPILATION_FAILED
)

    ::#------ Building Lua 5.1.5 -----

    rmdir /S /Q -rf %LUA_INSTALL_DIR%
    
    rmdir /S /Q %LUA_SOURCES_DIR%\build
    mkdir %LUA_SOURCES_DIR%\build
    cd %LUA_SOURCES_DIR%\build
    
    "%CMAKE_BINARIES_DIR%\cmake.exe" -G "Visual Studio 14 2015 Win64" -DCMAKE_INSTALL_PREFIX=%LUA_INSTALL_DIR% ../
    
    ::#----- Release -----
    "%CMAKE_BINARIES_DIR%\cmake.exe" --build . --config Release --target INSTALL
    ::#-------------------

    cd %SCRIPT_DIR%
    
    rmdir /S /Q %LUA_SOURCES_DIR%\build
    
    ::#-------------------------------
        
    call CheckIfLibExists.bat liblua
    if %ERRORLEVEL% NEQ 0 set CONTINUE=0

if %CONTINUE% NEQ 1 (
    goto COMPILATION_FAILED
)

    ::#---- Building LuaBind 0.9.1 ---

    rmdir /S /Q %LUABIND_INSTALL_DIR%
    
    cd %LUABIND_SOURCES_DIR%

    call "%VISUAL_STUDIO_DIR%\VC\vcvarsall.bat" x86_amd64
    
    ::#----- Release -----
    %BOOST_SOURCES_DIR%\bjam -sBOOST_BUILD_PATH=%BOOST_SOURCES_DIR% -sBOOST_ROOT=%BOOST_INSTALL_DIR%\include -sLUA_PATH=%LUA_INSTALL_DIR% --toolset=msvc --prefix=%LUABIND_INSTALL_DIR% architecture=x86 address-model=64 link=static variant=release install
    ::#-------------------
    
    ::#------- Debug -----
    %BOOST_SOURCES_DIR%\bjam -sBOOST_BUILD_PATH=%BOOST_SOURCES_DIR% -sBOOST_ROOT=%BOOST_INSTALL_DIR%\include -sLUA_PATH=%LUA_INSTALL_DIR% --toolset=msvc --prefix=%LUABIND_INSTALL_DIR% architecture=x86 address-model=64 link=static variant=debug install
    ::#-------------------
    
    rmdir /S /Q bin
    
    cd %SCRIPT_DIR%

    ::#-------------------------------
    
    call CheckIfLibExists.bat libluabind
    if %ERRORLEVEL% NEQ 0 set CONTINUE=0

if %CONTINUE% NEQ 1 (
    goto COMPILATION_FAILED
)

    ::#----- Building Bullet 2.81 ----

    rmdir /S /Q %BULLET_INSTALL_DIR%
    
    mkdir %BULLET_SOURCES_DIR%\buildObjects
    cd %BULLET_SOURCES_DIR%\buildObjects
    
    call "%VISUAL_STUDIO_DIR%\VC\vcvarsall.bat" x86_amd64
    "%CMAKE_BINARIES_DIR%\cmake.exe" ..\ -G "Visual Studio 14 2015 Win64" -DUSE_MSVC_RUNTIME_LIBRARY_DLL=ON -DINSTALL_LIBS=ON -DBUILD_SHARED_LIBS=OFF -DCMAKE_BUILD_TYPE=Release -DBUILD_EXTRAS=OFF -DINSTALL_EXTRA_LIBS=OFF -DBUILD_DEMOS=OFF -DCMAKE_INSTALL_PREFIX=%BULLET_INSTALL_DIR% -DCMAKE_INSTALL_RPATH=%BULLET_INSTALL_DIR%
    
    ::#----- Release -----
    "%CMAKE_BINARIES_DIR%\cmake.exe" --build . --config Release --target INSTALL
    ::#-------------------

    ::#------- Debug -----
    "%CMAKE_BINARIES_DIR%\cmake.exe" --build . --config Debug --target INSTALL
    ::#-------------------

    cd ..\
    rmdir /S /Q buildObjects

    rmdir /S /Q %BULLET_INSTALL_DIR%\include\OpenGL
            
    cd %SCRIPT_DIR%

    ::#-------------------------------
    
    call CheckIfLibExists.bat libBullet
    if %ERRORLEVEL% NEQ 0 set CONTINUE=0

if %CONTINUE% NEQ 1 (
    goto COMPILATION_FAILED
)

    ::#--- Building FreeType 2.5.5 ---

    :: Add Cygwin to the system path
    set "PATH=%PATH%;%CYGWIN_BINARIES_DIR%"
    
    rmdir /S /Q %FREETYPE_INSTALL_DIR%
    
    rmdir /S /Q %FREETYPE_SOURCES_DIR%\buildObjects
    mkdir %FREETYPE_SOURCES_DIR%\buildObjects
    cd %FREETYPE_SOURCES_DIR%\buildObjects
    
    call "%VISUAL_STUDIO_DIR%\VC\vcvarsall.bat" x86_amd64
    
    "%CMAKE_BINARIES_DIR%\cmake.exe" ..\ -G "Visual Studio 14 2015 Win64" -DBUILD_SHARED_LIBS:BOOL=true -DCMAKE_INSTALL_PREFIX=%FREETYPE_INSTALL_DIR% -DCMAKE_INSTALL_RPATH=%FREETYPE_INSTALL_DIR%
    "%CMAKE_BINARIES_DIR%\cmake.exe" --build . --config Release --target INSTALL

    cd %SCRIPT_DIR%

    rmdir /S /Q %FREETYPE_SOURCES_DIR%\buildObjects
    
    :: Restore the original system path
    set "PATH=%ORIGINAL_PATH_VARIABLE%"
    
    ::#-------------------------------
    
    call CheckIfLibExists.bat libfreetype
    if %ERRORLEVEL% NEQ 0 set CONTINUE=0

if %CONTINUE% NEQ 1 (
    goto COMPILATION_FAILED
)

    ::#----- Building libRocket ------

    rmdir /S /Q %LIBROCKET_INSTALL_DIR%
            
    call "%VISUAL_STUDIO_DIR%\VC\vcvarsall.bat" x86_amd64
    set "LIB_ROCKET_COMMONS_CMAKE_FLAGS=-DBUILD_SAMPLES=OFF -DBUILD_SHARED_LIBS=ON -DBUILD_LUA_BINDINGS=ON -DCMAKE_INSTALL_PREFIX=%LIBROCKET_INSTALL_DIR% -DLUA_LIBRARIES=%LUA_INSTALL_DIR%\lib\liblua.lib -DLUA_INCLUDE_DIR=%LUA_INSTALL_DIR%\include -DFREETYPE_LINK_DIRS=%FREETYPE_INSTALL_DIR%\lib -DFREETYPE_INCLUDE_DIRS=%FREETYPE_INSTALL_DIR%\include\freetype2 -DFREETYPE_LIBRARY=freetype -DFREETYPE_CUSTOM_LOCATION=ON ..\"
    
    mkdir %LIBROCKET_SOURCES_DIR%\buildData
    cd %LIBROCKET_SOURCES_DIR%\buildData
    
    "%CMAKE_BINARIES_DIR%\cmake.exe" -G "Visual Studio 14 2015 Win64" -DCMAKE_BUILD_TYPE=Release %LIB_ROCKET_COMMONS_CMAKE_FLAGS%

    ::#----- Release -----
    "%CMAKE_BINARIES_DIR%\cmake.exe" --build . --config Release --target INSTALL
    ::#-------------------

    cd %SCRIPT_DIR%
    rmdir /S /Q %LIBROCKET_SOURCES_DIR%\buildData    
    mkdir %LIBROCKET_SOURCES_DIR%\buildData
    cd %LIBROCKET_SOURCES_DIR%\buildData
    

    "%CMAKE_BINARIES_DIR%\cmake.exe" -G "Visual Studio 14 2015 Win64" -DCMAKE_DEBUG_POSTFIX="_Debug" -DCMAKE_BUILD_TYPE=Debug %LIB_ROCKET_COMMONS_CMAKE_FLAGS%    
    ::#------- Debug -----
    "%CMAKE_BINARIES_DIR%\cmake.exe" --build . --config Debug --target INSTALL
    ::#-------------------
                
    cd %SCRIPT_DIR%
    rmdir /S /Q %LIBROCKET_SOURCES_DIR%\buildData

    ::#-------------------------------
    
    call CheckIfLibExists.bat libRocket
    if %ERRORLEVEL% NEQ 0 set CONTINUE=0


if %CONTINUE% NEQ 1 (
    goto COMPILATION_FAILED
)

    ::#-------- Building SDL ---------

    rmdir /S /Q ${SDL_INSTALL_DIR}
    
    cd %SDL_SOURCES_DIR%

    rmdir /S /Q %SDL_SOURCES_DIR%\winBuild
    mkdir %SDL_SOURCES_DIR%\winBuild
    cd %SDL_SOURCES_DIR%\winBuild
    
    call "%VISUAL_STUDIO_DIR%\VC\vcvarsall.bat" x86_amd64
    "%CMAKE_BINARIES_DIR%\cmake.exe" -G "Visual Studio 14 2015 Win64" -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=%SDL_INSTALL_DIR% -DSDL_STATIC=OFF -DDIRECTX:BOOL=false ..\
    "%CMAKE_BINARIES_DIR%\cmake.exe" --build . --config Release --target INSTALL

    cd %SCRIPT_DIR%
    
    rmdir /S /Q %SDL_SOURCES_DIR%\winBuild

    ::#-------------------------------

    call CheckIfLibExists.bat libSDL2
    if %ERRORLEVEL% NEQ 0 set CONTINUE=0

    
if %CONTINUE% NEQ 1 (
    goto COMPILATION_FAILED
)

    ::#-------- Building GLEW --------

    rmdir /S /Q %GLEW_INSTALL_DIR%
        
    rmdir /S /Q %GLEW_SOURCES_DIR%\winBuild
    mkdir %GLEW_SOURCES_DIR%\winBuild
    cd %GLEW_SOURCES_DIR%\winBuild
    
    call "%VISUAL_STUDIO_DIR%\VC\vcvarsall.bat" x86_amd64
    "%CMAKE_BINARIES_DIR%\cmake.exe" -G "Visual Studio 14 2015 Win64" -DCMAKE_INSTALL_PREFIX=%GLEW_INSTALL_DIR% ..\
    "%CMAKE_BINARIES_DIR%\cmake.exe" --build . --config Release --target INSTALL

    cd %SCRIPT_DIR%

    rmdir /S /Q %GLEW_SOURCES_DIR%\winBuild
    
    ::#-------------------------------
    
    call CheckIfLibExists.bat libGLEW
    if %ERRORLEVEL% NEQ 0 set CONTINUE=0


if %CONTINUE% NEQ 1 (
    goto COMPILATION_FAILED
)

    ::#------- Building libOgg -------

    rmdir /S /Q %OGG_INSTALL_DIR%
    
    rmdir /S /Q %OGG_SOURCES_DIR%\winBuild
    mkdir %OGG_SOURCES_DIR%\winBuild
    cd %OGG_SOURCES_DIR%\winBuild
    
    call "%VISUAL_STUDIO_DIR%\VC\vcvarsall.bat" x86_amd64
    "%CMAKE_BINARIES_DIR%\cmake.exe" -G "Visual Studio 14 2015 Win64" -DBUILD_SHARED_LIBS=OFF -DCMAKE_INSTALL_PREFIX=%OGG_INSTALL_DIR% ..\
    "%CMAKE_BINARIES_DIR%\cmake.exe" --build . --config Release --target INSTALL

    cd %SCRIPT_DIR%
    
    rmdir /S /Q %OGG_SOURCES_DIR%\winBuild

    ::#-------------------------------
    
    call CheckIfLibExists.bat libOgg
    if %ERRORLEVEL% NEQ 0 set CONTINUE=0


if %CONTINUE% NEQ 1 (
    goto COMPILATION_FAILED
)

    ::#- Building Tremor(Vorbis decoder) -

    rmdir /S /Q %TREMOR_INSTALL_DIR%
    
    rmdir /S /Q %TREMOR_SOURCES_DIR%\winBuild
    mkdir %TREMOR_SOURCES_DIR%\winBuild
    cd %TREMOR_SOURCES_DIR%\winBuild

    call "%VISUAL_STUDIO_DIR%\VC\vcvarsall.bat" x86_amd64
    "%CMAKE_BINARIES_DIR%\cmake.exe" -G "Visual Studio 14 2015 Win64" -DBUILD_SHARED_LIBS=OFF -DCMAKE_INSTALL_PREFIX=%TREMOR_INSTALL_DIR% -DLIB_OGG_INCLUDES_DIR=%OGG_INSTALL_DIR%\include\ ..\
    "%CMAKE_BINARIES_DIR%\cmake.exe" --build . --config Release --target INSTALL
    
    cd %SCRIPT_DIR%

    rmdir /S /Q %TREMOR_SOURCES_DIR%\winBuild
    
    ::#-------------------------------
    
    call CheckIfLibExists.bat libTremor
    if %ERRORLEVEL% NEQ 0 set CONTINUE=0
    

if %CONTINUE% NEQ 1 (
    goto COMPILATION_FAILED
)

    ::#---- Building OpenAL Soft -----

    rmdir /S /Q %OPENAL_INSTALL_DIR%
    
    rmdir /S /Q %OPENAL_SOFT_SOURCES_DIR%\winBuild
    mkdir %OPENAL_SOFT_SOURCES_DIR%\winBuild
    cd %OPENAL_SOFT_SOURCES_DIR%\winBuild

    call "%VISUAL_STUDIO_DIR%\VC\vcvarsall.bat" x86_amd64
    "%CMAKE_BINARIES_DIR%\cmake.exe" -G "Visual Studio 14 2015 Win64" -DCMAKE_INSTALL_PREFIX=%OPENAL_INSTALL_DIR% ..\
    "%CMAKE_BINARIES_DIR%\cmake.exe" --build . --config Release --target INSTALL

    cd %SCRIPT_DIR%
    rmdir /S /Q %OPENAL_SOFT_SOURCES_DIR%\winBuild

    ::#-------------------------------
    
    call CheckIfLibExists.bat libOpenAL
    if %ERRORLEVEL% NEQ 0 set CONTINUE=0


if %CONTINUE% NEQ 1 (
    goto COMPILATION_FAILED
)

    ::#---- Building OpenVR Soft -----
    
    rmdir /S /Q %OPEN_VR_INSTALL_DIR%
    
    if not exist %OPEN_VR_SOURCES_DIR%\bin\win64\openvr_api.dll (
        set CONTINUE=0
        echo Error: openvr_api.dll not found
    )
    
    if %CONTINUE% EQU 1 ( 
        if not exist %OPEN_VR_SOURCES_DIR%\lib\win64\openvr_api.lib (
            set CONTINUE=0
            echo Error: openvr_api.lib not found
        )
    )
    
    if %CONTINUE% EQU 1 ( 
        if not exist %OPEN_VR_SOURCES_DIR%\headers\openvr.h (
            set CONTINUE=0
            echo Error: openvr.h not found
        )
    )
    
    mkdir %OPEN_VR_INSTALL_DIR%
    mkdir %OPEN_VR_INSTALL_DIR%\bin
    mkdir %OPEN_VR_INSTALL_DIR%\lib
    mkdir %OPEN_VR_INSTALL_DIR%\include
    
    copy /B /V %OPEN_VR_SOURCES_DIR%\bin\win64\openvr_api.dll /B %OPEN_VR_INSTALL_DIR%\bin
    copy /B /V %OPEN_VR_SOURCES_DIR%\lib\win64\openvr_api.lib /B %OPEN_VR_INSTALL_DIR%\lib
    copy /B /V %OPEN_VR_SOURCES_DIR%\headers\openvr.h /B %OPEN_VR_INSTALL_DIR%\include
        
    ::#-------------------------------

    call CheckIfLibExists.bat libOpenVR
    if %ERRORLEVEL% NEQ 0 set CONTINUE=0
    
    
if %CONTINUE% NEQ 1 (
    goto COMPILATION_FAILED
)
    
cd %INITIAL_DIR%
echo Success
pause
exit 0

:COMPILATION_FAILED
echo Failed to build at least one of the libraries
pause
cd %INITIAL_DIR%
exit 1


