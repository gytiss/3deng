#ifndef AUDIODEVICE_EFFECTSLOT_H_INCLUDED
#define AUDIODEVICE_EFFECTSLOT_H_INCLUDED

#include "Source.h"
#include "Effect.h"
#include <vector>
#include <memory>
#include <type_traits>

namespace AudioDevice
{

class Context;

class EffectSlot : public Source::SourceConnectionListener, public Source::SourceStatusListener, public Effect::EffectSettingsUpdateListener
{
public:
    EffectSlot(Context& context);
    ~EffectSlot();

    inline unsigned int GetALHandle()
    {
        return m_ALEffectSlot;
    }

    template<class EffectType>
    EffectType* CreateAndAttachAnEffect()
    {
        static_assert(std::is_base_of<Effect, EffectType>::value, "Template argument \"EffectType\" must be a subclass of \"Effect\"");

        EffectType* effect = new EffectType( m_Context, *this );
        _AttachAnEffect( effect );

        return effect;
    }

    /**
     * @param gainValue - Value between 0.0 and 1.0
     */
    void SetSlotSpecificGain(float gainValue);

    float GetSlotSpecificGain() const;

protected:
    void SourceConnected(Source* source) override;
    void SourceDisconnected(Source* source) override;
    void SourceStartedPlaying(Source* source) override;
    void SourceStoppedPlaying(Source* source) override;
    void EffectSettingsUpdated() override;

private:
    typedef std::vector<Source*> ConnectedSourcesList;

    // Used to prevent copying
    EffectSlot& operator = (const EffectSlot& other) = delete;
    EffectSlot(const EffectSlot& other) = delete;

    bool _IsKnownConnectedSource(Source* source) const;
    void _Activate();
    void _Deactivate();
    void _DeactivateIfNoneOfTheAttachedSourcesArePlaying();
    void _AttachAnEffect(Effect* effect);
    void _DetachCurrentlyAttachedEffect();
    void _ActivateAttachedEffect();
    void _DeactivateAttachedEffect();

    unsigned int m_ALEffectSlot;
    Context& m_Context;
    ConnectedSourcesList m_ConnectedSourcesList;
    std::unique_ptr<Effect> m_AttachedEffect;
    float m_SlotSpecificGain;
};

}

#endif // AUDIODEVICE_EFFECTSLOT_H_INCLUDED
