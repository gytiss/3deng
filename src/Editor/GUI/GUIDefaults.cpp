#include "GUIDefaults.h"

#include <QtGui/QColor>

//-----------------------------------------------------------------------

GUIDefaults& GUIDefaults::GetSingleton()
{
    static GUIDefaults instance;

    return instance;
}

//-----------------------------------------------------------------------

void GUIDefaults::_InitializeDarkPalette(QPalette& palette)
{
    QColor greyIsh = QColor(200,200,200);

    palette.setColor(QPalette::Window, QColor(67,67,67));
    palette.setColor(QPalette::WindowText, greyIsh);
    palette.setColor(QPalette::Base, QColor(42,42,42));
    palette.setColor(QPalette::AlternateBase, QColor(67,67,67));
    palette.setColor(QPalette::ToolTipBase, Qt::white);
    palette.setColor(QPalette::ToolTipText, greyIsh);
    palette.setColor(QPalette::Text, greyIsh);
    palette.setColor(QPalette::Button, QColor(67,67,67));
    palette.setColor(QPalette::ButtonText, greyIsh);
    palette.setColor(QPalette::BrightText, Qt::red);
    palette.setColor(QPalette::Link, QColor(42, 130, 218));

    palette.setColor(QPalette::Highlight, QColor(42, 130, 218));
    palette.setColor(QPalette::HighlightedText, Qt::black);
}


//-----------------------------------------------------------------------

GUIDefaults::GUIDefaults()
: m_DefaultPalette()
{
    _InitializeDarkPalette( m_DefaultPalette );
}

//-----------------------------------------------------------------------
