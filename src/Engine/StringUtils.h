#ifndef STRINGUTILS_H
#define STRINGUTILS_H

#include <sstream>
#include <vector>

template <class T>
inline std::string numToString (const T& t)
{
    std::stringstream ss;
    ss << t;
    return ss.str();
}

namespace StringUitls
{
    template <class T>
    inline std::string NumToString(const T& t)
    {
        return numToString( t );
    }

    /**
     * Splits a given string into smaller strings given a delimiter string
     *
     * @param strToSplit        - Input string
     * @param delimiter         - Delimiter string
     * @param outTokens         - An array of output strings
     * @param maxNumberOfTokens - Max limit on the number of splits to do e.g. if input string is "A:B:C" and "maxNumberOfTokens == 2" output array will
     *                            contain the following values: {"A", "B:C"}
     */
    inline void SplitStr(const std::string& strToSplit, const std::string& delimiter, std::vector<std::string>& outTokens, size_t maxNumberOfTokens = 0)
    {
        size_t start = 0U;
        size_t end = strToSplit.find(delimiter);

        while ((end != std::string::npos) && ((maxNumberOfTokens == 0) || ((maxNumberOfTokens > 0) && (outTokens.size() < (maxNumberOfTokens - 1)))))
        {
            if (start != end)
            {
                outTokens.push_back(strToSplit.substr(start, end - start));
            }

            start = (end + delimiter.length());
            end = strToSplit.find(delimiter, start);
        }

        if (start < strToSplit.length())
        {
            outTokens.push_back(strToSplit.substr(start));
        }
    }

    inline bool EndsWith(const std::string& str, const std::string& ending)
    {
        bool retVal = false;

        if( str.length() >= ending.length() )
        {
            retVal = ( str.compare( str.length() - ending.length(), ending.length(), ending ) == 0 );
        }

        return retVal;
    }
}

#endif // STRINGUTILS_H
