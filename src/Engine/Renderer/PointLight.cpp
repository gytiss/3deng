#include "PointLight.h"
#include "ShaderPrograms.h"

//-----------------------------------------------------------------------

PointLight::PointLight(ShaderProgPointLightUberShader& shaderProg,
                       ShaderProgLightingStencilPass& stencilShaderProg)
: m_Enabled( true ),
  m_Color( 1.0f, 1.0f, 1.0f ), // White
  m_Radius( 1.0f ),
  m_AmbientIntensity( 0.0f ),
  m_DiffuseIntensity( 1.0f ),
  m_PositionInWorldSpace( 0.0f, 0.0f, 0.0f ),
  m_InfluenceMesh(),
  m_ShaderProg( shaderProg ),
  m_StencilShaderProg( stencilShaderProg )
{

}

//-----------------------------------------------------------------------

void PointLight::RenderForLightPass(const glm::mat4& viewMatrix,
                                    const glm::mat4& projectionMatrix)
{
    m_ShaderProg.Use();
    m_ShaderProg.SetUniformLightColor( m_Color );
    m_ShaderProg.SetUniformLightRadius( m_Radius );
    m_ShaderProg.SetUniformLightAmbientIntensity( m_AmbientIntensity );
    m_ShaderProg.SetUniformLightDiffuseIntensity( m_DiffuseIntensity );
    const glm::vec3 positionInViewSpace = glm::vec3( viewMatrix * glm::vec4( m_PositionInWorldSpace, 1.0f ) );
    m_ShaderProg.SetUniformLightPositionInViewSpace( positionInViewSpace );

    m_InfluenceMesh.Render( m_PositionInWorldSpace,
                            m_Radius,
                            viewMatrix,
                            projectionMatrix,
                            m_ShaderProg );
}

//-----------------------------------------------------------------------

void PointLight::RenderForStencilPass(const glm::mat4& viewMatrix,
                                      const glm::mat4& projectionMatrix)
{
    m_StencilShaderProg.Use();

    m_InfluenceMesh.RenderStencilPass( m_PositionInWorldSpace,
                                       m_Radius,
                                       viewMatrix,
                                       projectionMatrix,
                                       m_StencilShaderProg );
}

//-----------------------------------------------------------------------
