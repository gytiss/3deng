#ifndef GUIEVENTINSTANCER_H_INCLUDED
#define GUIEVENTINSTANCER_H_INCLUDED

#include <Rocket/Core/EventListenerInstancer.h>

class GUIEventManager;

class GUIEventInstancer : public Rocket::Core::EventListenerInstancer
{
public:
	GUIEventInstancer(GUIEventManager* eventManager);
	virtual ~GUIEventInstancer();

	/**
	 * Instances a new event handle.
	 */
	virtual Rocket::Core::EventListener* InstanceEventListener(const Rocket::Core::String& value, Rocket::Core::Element* element);

	/**
	 * Destroys the instancer.
	 */
	virtual void Release();

private:
    GUIEventManager* m_EventManager;
};

#endif // GUIEVENTINSTANCER_H_INCLUDED
