#include "OpenGLDebugOutput.h"
#include "GraphicsDevice.h"
#include <MsgLogger.h>
#include <OpenGLErrorChecking.h>
#include <string>

//-----------------------------------------------------------------------

GLenum OpenGLDebugOutput::m_GLDebugTypes[e_WarningTypeCount] =
{
#ifndef OPENGL_ES
    GL_DEBUG_TYPE_ERROR,
    GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR,
    GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR,
    GL_DEBUG_TYPE_PORTABILITY,
    GL_DEBUG_TYPE_PERFORMANCE,
    GL_DEBUG_TYPE_OTHER
#else
    0u,  0u, 0u, 0u, 0u, 0u
#endif
};

//-----------------------------------------------------------------------

OpenGLDebugOutput::OpenGLDebugOutput(GraphicsDevice& graphicsDevice)
: m_GraphicsDevice( graphicsDevice )
{
#ifndef OPENGL_ES
    glDebugMessageControl( GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, GL_TRUE );
    CheckForGLErrors();
    glDebugMessageCallback( _GLDebugCallback, NULL );
    CheckForGLErrors();
#endif
}

//-----------------------------------------------------------------------

void OpenGLDebugOutput::SwitchWarningTypeOn(WarningType type)
{
#ifndef OPENGL_ES
    glDebugMessageControl( GL_DONT_CARE,
                           m_GLDebugTypes[type],
                           GL_DONT_CARE,
                           0,
                           NULL,
                           GL_TRUE);
    CheckForGLErrors();
#endif
}

//-----------------------------------------------------------------------

void OpenGLDebugOutput::SwitchWarningTypeOff(WarningType type)
{
#ifndef OPENGL_ES
    glDebugMessageControl( GL_DONT_CARE,
                           m_GLDebugTypes[type],
                           GL_DONT_CARE,
                           0,
                           NULL,
                           GL_FALSE);
    CheckForGLErrors();
#endif
}

//-----------------------------------------------------------------------

void OpenGLDebugOutput::Enable()
{
#ifndef OPENGL_ES
    m_GraphicsDevice.OptionEnable( GraphicsDevice::e_OptionDebugOutput );
    m_GraphicsDevice.OptionEnable( GraphicsDevice::e_OptionDebugOutputSynchronous ); // Enables us to trace call stack up to the error location.
#endif
}

//-----------------------------------------------------------------------

void OpenGLDebugOutput::Disable()
{
#ifndef OPENGL_ES
    m_GraphicsDevice.OptionDisable( GraphicsDevice::e_OptionDebugOutput );
    m_GraphicsDevice.OptionDisable( GraphicsDevice::e_OptionDebugOutputSynchronous );
#endif
}

//-----------------------------------------------------------------------

void OpenGLDebugOutput::_GLDebugCallback( GLenum source,
                                          GLenum type,
                                          GLuint id,
                                          GLenum severity,
                                          GLsizei length,
                                          const GLchar *message,
                                          const void* userParam )
{
    std::string errMsg = "";

#ifndef OPENGL_ES

    switch( type )
    {
        case GL_DEBUG_TYPE_ERROR:
        {
            errMsg += "GL_ERROR";
            break;
        }
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
        {
            errMsg += "GL_DEPRECATED_BEHAVIOR";
            break;
        }
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
        {
            errMsg += "GL_UNDEFINED_BEHAVIOR";
            break;
        }
        case GL_DEBUG_TYPE_PORTABILITY:
        {
            errMsg += "GL_PORTABILITY";
            break;
        }
        case GL_DEBUG_TYPE_PERFORMANCE:
        {
            errMsg += "GL_PERFORMANCE";
            break;
        }
        case GL_DEBUG_TYPE_OTHER:
        {
            errMsg += "GL_OTHER";
            break;
        }
        default:
        {
            errMsg += "GL_UNKNOWN";
            break;
        }
    }

    errMsg += "(Severity: ";

    switch( severity )
    {
        case GL_DEBUG_SEVERITY_HIGH:
        {
            errMsg += "High";
            break;
        }
        case GL_DEBUG_SEVERITY_MEDIUM:
        {
            errMsg += "Med";
            break;
        }
        case GL_DEBUG_SEVERITY_LOW:
        {
            errMsg += "Low";
            break;
        }
        case GL_DEBUG_SEVERITY_NOTIFICATION:
        {
            errMsg += "Notify";
            break;
        }
        default:
        {
            errMsg += "Unknown";
            break;
        }
    }

#endif

    errMsg += "): ";
    errMsg += message;
    errMsg += "\n";

    MsgLogger::LogWarning( errMsg );
}

//-----------------------------------------------------------------------
