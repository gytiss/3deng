#ifndef FBOCOLORATTACHMENTTEXTURE_H_INCLUDED
#define FBOCOLORATTACHMENTTEXTURE_H_INCLUDED

#include "Texture.h"

class FBO;

class FBOColorAttachmentTexture : public Texture
{
public:
    friend class FBO;

    enum DataFormat
    {
        e_DataFormatRGB8 = Texture::e_InternalDataFormatRGB8,
        e_DataFormatRGBA8 = Texture::e_InternalDataFormatRGBA8,
        e_DataFormatSRGB8 = Texture::e_InternalDataFormatSRGB8,
        e_DataFormatSRGB8Alpha8 = Texture::e_InternalDataFormatSRGB8Alpha8,
        e_DataFormatRGB10Alpha2 = Texture::e_InternalDataFormatRGB10Alpha2,
        e_DataFormatRGB16F = Texture::e_InternalDataFormatRGB16F,
        e_DataFormatRGBA16F = Texture::e_InternalDataFormatRGBA16F,
        e_DataFormatRGB16UI = Texture::e_InternalDataFormatRGB16UI,
        e_DataFormatRGBA16UI = Texture::e_InternalDataFormatRGBA16UI,
        e_DataFormatRGB32F = Texture::e_InternalDataFormatRGB32F,
        e_DataFormatRGBA32F = Texture::e_InternalDataFormatRGBA32F,
        e_DataFormatRGB32UI = Texture::e_InternalDataFormatRGB32UI,
        e_DataFormatRGBA32UI = Texture::e_InternalDataFormatRGBA32UI
    };

    FBOColorAttachmentTexture(GraphicsDevice& graphicsDevice,
                              size_t textureWidth,
                              size_t textureHeight,
                              TypeIdx textureTypeIdx,
                              DataFormat dataFormat);

#if defined(VR_BACKEND_ENABLED)
    GLuint GetOpenGLInternalTextureID() const;
#endif

protected:
    void Reinitialize(size_t textureWidth, size_t textureHeight);

private:
    // Used to prevent copying
    FBOColorAttachmentTexture& operator = (const FBOColorAttachmentTexture& other) = delete;
    FBOColorAttachmentTexture(const FBOColorAttachmentTexture& other) = delete;

    const DataFormat m_DataFormat;
};

#endif // FBOCOLORATTACHMENTTEXTURE_H_INCLUDED
