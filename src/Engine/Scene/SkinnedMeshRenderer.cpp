#include "SkinnedMeshRenderer.h"
#include "GameObject.h"
#include <Exceptions/GenericException.h>
#include <MathUtils.h>
#include "Animator.h"


//-----------------------------------------------------------------------

SkinnedMeshRenderer::SkinnedMeshRenderer(GameObject& owningGameObject,
                                         MeshManager& meshManager,
                                         MaterialManager& materialManager)
: Renderer( owningGameObject,
            e_RendererTypeSkinnedMesh,
            "SkinnedMeshRenderer",
            materialManager ),
  m_MeshManager( meshManager ),
  m_MeshHandle(),
  m_AnimatedBones()
{
    GetOwningGameObject().SetComponent( this );
    m_AnimatedBones.resize( 0 );
}

//-----------------------------------------------------------------------

SkinnedMeshRenderer::~SkinnedMeshRenderer()
{
    if( m_MeshHandle.IsValid() )
    {
        m_MeshManager.ReleaseMesh( m_MeshHandle );
    }

    // At this point (i.e. in the destructor) this Skinned Mesh Renderer is no longer connected to a GameObject,
    // hence redoing the animator connection sequence will effectively disconnect it from the animator.
    Animator* animator = _FindAnAnimatorInTheParentHierarchy();
    if( animator )
    {
        animator->_ConnectWithSkinnedMeshes();
    }
}

//-----------------------------------------------------------------------

void SkinnedMeshRenderer::SetMesh(MeshManager::MeshHandle skinnedMeshHandle)
{
    if( !skinnedMeshHandle->IsSkinned() )
    {
        throw GenericException( "SkinnedMeshRenderer does not support non-skinned meshes" );
    }

    if( m_MeshHandle.IsValid() )
    {
        m_MeshManager.ReleaseMesh( m_MeshHandle );
    }

    m_MeshHandle = skinnedMeshHandle;

    _ResetSubMeshesMaterialsList( m_MeshHandle );

    // SkinnedMeshManager deals with skinned meshes only, hence bones will always be present
    m_AnimatedBones.assign( m_MeshHandle->GetBoneCount() , MathUtils::c_IdentityMatrix );

    Animator* animator = _FindAnAnimatorInTheParentHierarchy();
    if( animator )
    {
        animator->_ConnectWithSkinnedMeshes();
    }

    _RegenerateRenderables();
}

//-----------------------------------------------------------------------

const EngineTypes::AABB& SkinnedMeshRenderer::GetAABB() const
{
    assert( m_MeshHandle.IsValid() && "No mesh has been set, hence can not get AABB" );
    return m_MeshHandle->GetAABB();
}

//-----------------------------------------------------------------------

MeshManager::MeshHandle SkinnedMeshRenderer::_GetMeshHandle() const
{
    return m_MeshHandle;
}

//-----------------------------------------------------------------------

Animator* SkinnedMeshRenderer::_FindAnAnimatorInTheParentHierarchy()
{
    Animator* animator = nullptr;
    GameObject* currentGameObject = &GetOwningGameObject();

    while( currentGameObject && !animator )
    {
        animator = currentGameObject->GetComponent<Animator>();
        currentGameObject = currentGameObject->GetParent();
    }

    if( animator && !animator->IsEnabled() )
    {
        animator = nullptr;
    }

    return animator;
}

//-----------------------------------------------------------------------

void SkinnedMeshRenderer::_RegenerateRenderables()
{
    _ClearRenderables();

    if( m_MeshHandle.IsValid() )
    {
        EngineTypes::MatrixArray* boneMatrices = nullptr;
        if( m_MeshHandle->IsSkinned() )
        {
            boneMatrices = &m_AnimatedBones;
        }

        const Mesh::SubMeshList& subMeshes = m_MeshHandle->GetSubMeshes();

        size_t subMeshIndex = 0;
        for(const Mesh::SubMesh& subMesh : subMeshes)
        {
            MaterialManager::MaterialHandle* materialHandle = GetSubMeshMaterial( subMeshIndex );
            if( materialHandle && !materialHandle->IsValid() )
            {
                materialHandle = nullptr;
            }

            RenderableObject renderable( RenderableObject::e_RenderingTypeIndexed,
                                         GetDepthTestingEnabled(),
                                         static_cast<unsigned int> (subMesh.m_FirstIndex ),
                                         static_cast<unsigned int>( subMesh.m_IndexCount ),
                                         subMesh.m_MinVertexIndexValue,
                                         subMesh.m_MaxVertexIndexValue,
                                         GraphicsDevice::e_DrawPrimitivesTypeTriangles,
                                         m_MeshHandle->GetVAO(),
                                         materialHandle,
                                         boneMatrices );
            _AddRenderable( renderable );

            subMeshIndex++;
        }
    }
}

//-----------------------------------------------------------------------
