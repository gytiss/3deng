#ifndef MATERIAL_DESCRIPTOR_H_INCLUDED
#define MATERIAL_DESCRIPTOR_H_INCLUDED

#include "TextureManager.h"
#include <EngineTypes.h>
#include <BitMask.h>
#include <functional>

class MaterialDescriptor
{
public:
    friend class Material;
    friend class MaterialManager;
    friend void swap(MaterialDescriptor& first, MaterialDescriptor& second);

    MaterialDescriptor(TextureManager& textureManager);

    MaterialDescriptor(MaterialDescriptor&& other) = default;
    MaterialDescriptor& operator=(MaterialDescriptor&& other) = default;
    MaterialDescriptor(const MaterialDescriptor& other);
    MaterialDescriptor& operator=(MaterialDescriptor other);

    ~MaterialDescriptor();

    uint8_t GetGlossiness() const;
    const EngineTypes::SRGBColor& GetDiffuseColor() const;
    const EngineTypes::SRGBColor& GetSpecularColor() const;

    void SetDiffuseColor(const EngineTypes::SRGBColor& srgbDiffuseColor);
    void SetSpercularColor(const EngineTypes::SRGBColor& srgbSpecularColor);
    void SetGlossiness(uint8_t glossiness);

    void SetDiffuseTextureMap(TextureManager::TextureHandle& textureHandle);
    void SetNormalTextureMap(TextureManager::TextureHandle& textureHandle);
    void SetSpecularTextureMap(TextureManager::TextureHandle& textureHandle);
    void SetGlossTextureMap(TextureManager::TextureHandle& textureHandle);

    inline bool HasGlossiness() const
    {
        return _IsMaterialPropertyPresent( e_MaterialPropertyTypeGlossiness );
    }

    inline bool HasDiffuseColor() const
    {
        return _IsMaterialPropertyPresent( e_MaterialPropertyTypeDiffuseColor );
    }

    inline bool HasSpecularColor() const
    {
        return _IsMaterialPropertyPresent( e_MaterialPropertyTypeSpecularColor );
    }

    inline bool HasDiffuseTextureMap() const
    {
        return _IsTextureMapPresent( Texture::e_TypeIdxDiffuseMap );
    }

    inline bool HasNormalTextureMap() const
    {
        return _IsTextureMapPresent( Texture::e_TypeIdxNormalMap );
    }

    inline bool HasSpecularTextureMap() const
    {
        return _IsTextureMapPresent( Texture::e_TypeIdxSpecularMap );
    }

    inline bool HasGlossTextureMap() const
    {
        return _IsTextureMapPresent( Texture::e_TypeIdxGlossMap );
    }

protected:
    void AddTexture(TextureManager::TextureHandle& textureHandle);

private:
    typedef std::vector<TextureManager::TextureHandle> TextureList;

    enum MaterialPropertyType
    {
        e_MaterialPropertyTypeDiffuseColor = 0,
        e_MaterialPropertyTypeSpecularColor = 1,
        e_MaterialPropertyTypeGlossiness
    };

    void _MarkMaterialPropertyAsPresent(MaterialPropertyType type);
    bool _IsMaterialPropertyPresent(MaterialPropertyType type) const;

    void _MarkTextureMapAsPresent(Texture::TypeIdx typeIndex);
    bool _IsTextureMapPresent(Texture::TypeIdx typeIndex) const;


    std::reference_wrapper<TextureManager> m_TextureManager;
    TextureList m_TextureList;
    EngineTypes::SRGBColor m_DiffuseColor;
    EngineTypes::SRGBColor m_SpecularColor;
    uint8_t m_Glossiness;

    BitMask32 m_MaterialPropertiesPresent;
    BitMask32 m_TextureMapTypesPresent;
};

void swap(MaterialDescriptor& first, MaterialDescriptor& second);

#endif // MATERIAL_DESCRIPTOR_H_INCLUDED
