#ifndef FILEMANAGER_H_INCLUDED
#define FILEMANAGER_H_INCLUDED

#ifdef OS_ANDROID
    #include <SDL2/SDL.h>
#else
    #include <cstdio>
#endif

#include <string>

class FileManager
{
public:
    enum FileType
    {
        e_FileTypeBinary = 0,
        e_FileTypeText = 1
    };

    class File
    {
    public:
        friend class FileManager;
        friend void swap(File& first, File& second);

        File();

        /**
         * @param buffer - Pointer to the buffer to read the data into
         * @param chunkSize - Size of each chunk of data
         * @param chunksToRead - Maximum number of chunks of data to read
         *
         * @return true on success, false otherwise
         */
        bool Read(void* buffer, size_t chunkSize, size_t chunksToRead);

        /**
         * @return File size;
         */
        size_t Size();

        ~File();

    private:
#ifdef OS_ANDROID
        typedef SDL_RWops* FileHanlde;
#else
        typedef FILE* FileHanlde;
#endif
        File(const std::string& filePath, FileType type);
        bool Open();
        bool Close();

        File(File&& other) = default;
        File& operator=(File&& other) = default;
        File(const File& other);
        File& operator=(File& other);

        FileHanlde m_FileHanlde;
        std::string m_FilePath;
        FileType m_Type;
        size_t m_Size;
        bool m_SizeQueriedAlready;
    };

    FileManager();

    bool OpenFile(const std::string& filePath, FileType type, File& outFile);
};

void swap(FileManager::File& first, FileManager::File& second);


#endif // FILEMANAGER_H_INCLUDED
