#ifndef MESHRENDERER_H_INCLUDED
#define MESHRENDERER_H_INCLUDED

#include "Renderer.h"
#include <string>

class GameObject;

class MeshRenderer : public Renderer
{
    GAME_OBJECT_COMPONENT_TYPE(e_TypeRenderer);

public:
    friend class MeshFilter;

    MeshRenderer(GameObject& owningGameObject, MaterialManager& materialManager);

    ~MeshRenderer() = default;

    //////////////////////////////
    // TODO Remove after line renderer is created
    /////////////////////////////
    void AddRenderable(const RenderableObject& renderable)
    {
        _AddRenderable( renderable );
    }

    void ClearRenderables()
    {
        _ClearRenderables();
    }
    /////////////////////////////

    const EngineTypes::AABB& GetAABB() const;

private:
    MeshManager::MeshHandle _GetMeshHandle() const override;
    void _FindMesh();
    void _RegenerateRenderables() override;
};

#endif // MESHRENDERER_H_INCLUDED
