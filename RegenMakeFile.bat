@ECHO OFF 

set "INITIAL_DIR=%CD%"
set "SCRIPT_DIR=%~dp0"

set "VISUAL_STUDIO_DIR=C:\Program Files (x86)\Microsoft Visual Studio 14.0"
set "CMAKE_BINARIES_DIR=C:\Program Files\CMake\bin"

:: Make sure that all dependencies are satisfied
if NOT exist "%VISUAL_STUDIO_DIR%" (
    echo Required directory "%VISUAL_STUDIO_DIR%" does not exist
    goto COMPILATION_FAILED
)
if NOT exist "%CMAKE_BINARIES_DIR%\cmake.exe" (
    echo cmake.exe not found in the directory "%CMAKE_BINARIES_DIR%"
    goto COMPILATION_FAILED
)

mkdir %SCRIPT_DIR%\winBuild
cd %SCRIPT_DIR%\winBuild

"%CMAKE_BINARIES_DIR%\cmake.exe" -G "Visual Studio 14 2015 Win64" -DBUILD_MAIN_APP=ON -DBUILD_EDITOR=OFF ../
pause
"%CMAKE_BINARIES_DIR%\cmake.exe" --build . --config Release --target MainApp

pause
cd %INITIAL_DIR%
