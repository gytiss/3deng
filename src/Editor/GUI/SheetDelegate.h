#ifndef SHEETDELEGATE_H
#define SHEETDELEGATE_H

#include <QtWidgets/QStyledItemDelegate>
#include <QtWidgets/QTreeView>

QT_BEGIN_NAMESPACE

class QTreeView;

class SheetDelegate: public QStyledItemDelegate
{
    Q_OBJECT
public:
    SheetDelegate(QTreeView *view, QWidget *parent);

    virtual void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    virtual QSize sizeHint(const QStyleOptionViewItem &opt, const QModelIndex &index) const;

private:
    QTreeView *m_view;
};


QT_END_NAMESPACE

#endif // SHEETDELEGATE_H
