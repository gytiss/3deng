#ifndef GRAPHICSDEVICEBLENDING_H_INCLUDED
#define GRAPHICSDEVICEBLENDING_H_INCLUDED

#include <stack>
#include <OpenGLIncludes.h>
#include <glm/glm.hpp>

class GraphicsDeviceBlending
{
public:
    friend class GraphicsDevice;

    enum BlendEquation
    {
        e_BlendEquationAdd = GL_FUNC_ADD,
        e_BlendEquationSubtract = GL_FUNC_SUBTRACT,
        e_BlendEquationReverseSubtract = GL_FUNC_REVERSE_SUBTRACT,
        e_BlendEquationMin = GL_MIN,
        e_BlendEquationMax = GL_MAX
    };

    enum BlendFunction
    {
        e_BlendFunctionZero = GL_ZERO,
        e_BlendFunctionOne = GL_ONE,
        e_BlendFunctionSrcColor = GL_SRC_COLOR,
        e_BlendFunctionOneMinusSrcColor = GL_ONE_MINUS_SRC_COLOR,
        e_BlendFunctionDstColor = GL_DST_COLOR,
        e_BlendFunctionOneMinusDstColor = GL_ONE_MINUS_DST_COLOR,
        e_BlendFunctionSrcAlpha = GL_SRC_ALPHA,
        e_BlendFunctionOneMinusSrcAlpha = GL_ONE_MINUS_SRC_ALPHA,
        e_BlendFunctionDstAlpha = GL_DST_ALPHA,
        e_BlendFunctionOneMinusDstAlpha = GL_ONE_MINUS_DST_ALPHA,
        e_BlendFunctionConstantColor = GL_CONSTANT_COLOR,
        e_BlendFunctionOneMinusConstantColor = GL_ONE_MINUS_CONSTANT_COLOR,
        e_BlendFunctionConstantAlpha = GL_CONSTANT_ALPHA,
        e_BlendFunctionOneMinusConstantAlpha = GL_ONE_MINUS_CONSTANT_ALPHA
    };

    void SaveCurrentBlendEquation();
    void RestorePreviousBlendEquation();
    void SetBlendEquation(BlendEquation equation);

    void SaveCurrentBlendFunctionState();
    void RestorePreviousBlendFunctionState();
    void SetBlendFunctionState(BlendFunction sourceFactor, BlendFunction destinationFactor);

    void SaveCurrentBlendColor();
    void RestorePreviousBlendColor();
    void SetBlendColor(const glm::vec4& rgbaColor);
    void SetBlendColor(float red, float green, float blue, float alpha);

private:
    GraphicsDeviceBlending();

    typedef std::pair<BlendFunction, BlendFunction> BlendFunctionState;

    std::stack<BlendEquation> m_BlendEquationValuesStack;
    BlendEquation m_CurrentBlendEquationValue;

    std::stack<BlendFunctionState> m_BlendFunctionStatesStack;
    BlendFunctionState m_CurrentBlendFunctionState;

    std::stack<glm::vec4> m_BlendColorsStack;
    glm::vec4 m_CurrentBlendColor;
};

#endif // GRAPHICSDEVICEBLENDING_H_INCLUDED
