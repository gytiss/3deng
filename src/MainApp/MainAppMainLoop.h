#ifndef MAINAPPMAINLOOP
#define MAINAPPMAINLOOP

#include <MainLoop.h>
#include "MainAppLogic.h"
#include "WindowManager.h"

namespace InputMapping
{
    class InputMapper;
}

class WindowManager;
#ifdef VR_BACKEND_ENABLED
    class VRBackend;
#endif
class GUIContext;
class GUIRenderInterface;

class MainAppMainLoop : public MainLoop
{
public:
    MainAppMainLoop(InputMapping::InputMapper& inputMapper,
                    WindowManager& windowManager,
#ifdef VR_BACKEND_ENABLED
                    VRBackend& vrBackend,
#endif
                    FileManager& fileManager);
    ~MainAppMainLoop();

    inline void SetGUIRenderInterface(GUIRenderInterface* renderInterface)
    {
        m_MainAppLogic->SetGUIRenderInterface( renderInterface );
    }

    inline void SetGUIContext(GUIContext* guiContext)
    {
        m_GUIContext = guiContext;
    }

    inline bool Exit()
    {
        return ( !m_MainAppLogic->IsAppStillRunning() || m_WindowManager.WindowCloseWasRequested() );
    }

private:
    void _UpdateInputs(float frameTimeInSeconds) override;
    void _UpdateFixedStepApplicationState(float frameTimeInSeconds) override;
    void _UpdateOtherApplicationState(float frameTimeInSeconds) override;
    void _LastUpdateOtherApplicationState(float frameTimeInSeconds) override;
    void _Render(float frameTimeInSeconds) override;

    InputMapping::InputMapper& m_InputMapper;
    WindowManager& m_WindowManager;
#ifdef VR_BACKEND_ENABLED
    VRBackend& m_VRBackend;
#endif
    GUIContext* m_GUIContext;
    MainAppLogic* m_MainAppLogic;
};

#endif // MAINAPPMAINLOOP
