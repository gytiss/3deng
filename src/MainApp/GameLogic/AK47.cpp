#include "AK47.h"
#include "ShootingTarget.h"
#include "GameObjectTypes.h"
#include <Scene/Transform.h>
#include <Scene/Animator.h>
#include <Scene/SkinnedMeshRenderer.h>
#include <Resources/MeshManager.h>
#include <Resources/AnimationManager.h>
#include <Resources/MaterialManager.h>
#include <Physics/PhysicsManager.h>
#include <Resources/Audio/AudioDataManager.h>
#include <Resources/Audio/SoundManager.h>
#include <Camera.h>
#include <EngineTypes.h>
#include <MathUtils.h>
#include <Scene/SoundSource.h>
#include <MsgLogger.h>

//-----------------------------------------------------------------------

AK47::AK47(Camera& camera,
           SystemInterface& systemInterface,
           MeshManager& meshManager,
           AnimationManager& animationManager,
           MaterialManager& materialManager,
           PhysicsManager& physicsManager,
           AudioDataManager& audioDataManager,
           SoundManager& soundManager)
: GameObject( "AK47", GameObjectTypes::c_Gun ),
  m_Camera( camera ),
  m_SystemInterface( systemInterface ),
  m_DrawableSurfaceSize( m_SystemInterface.GetDrawableSurfaceSize() ),
  m_PhysicsManager( physicsManager ),
  m_CurrentState( e_StateStatic ),
  m_ShootingSoundSource( nullptr ),
  m_ReloadSoundSource( nullptr ),
  m_Animator( nullptr ),
  m_Offset( nullptr ),
  m_BulletsInTheMagazine( c_MagazineCapacity ),
  m_GlobalTimeInSecondDuringStateChange( 0.0f )
{
    m_SystemInterface.RegisterDrawableSurfaceSizeChangedCallback( _DrawableSurfaceSizeChaged, this );

    GameObject* offset = new GameObject( "AK47 Offset" );
    m_Offset = offset->GetComponent<Transform>();

    AddChild( offset );


    /*---------------------------------------------------*/
    GameObject* ak47 = new GameObject( "AK47 Model" );
    ak47->GetComponent<Transform>()->SetPosition( glm::vec3( 0.0f, 0.0f, 0.0f ) );
    // Make sure the weapon model is pointing towards the negative Z axis
    ak47->GetComponent<Transform>()->SetRotation( MathUtils::EulerXYZAnglesToQuaternion( glm::vec3( 0.0f, 360.0f, 0.0f ) ) );

    SkinnedMeshRenderer* ak47Renderer = new SkinnedMeshRenderer( *ak47, meshManager, materialManager );
    MeshManager::ModelMeshes ak47ModelMeshes;
    meshManager.LoadModel( "DemoRoomData/Models/AK47/AK47.mobj", ak47ModelMeshes );
    ak47Renderer->SetMesh( ak47ModelMeshes[0] );
    ak47Renderer->SetSubMeshMaterial( 0, materialManager.LoadMaterial( "DemoRoomData/Models/AK47/RifleHands.mat" ) );
    ak47Renderer->SetSubMeshMaterial( 1, materialManager.LoadMaterial( "DemoRoomData/Models/AK47/AK47MainBody.mat" ) );
    ak47Renderer->SetSubMeshMaterial( 2, materialManager.LoadMaterial( "DemoRoomData/Models/AK47/AK47Stock.mat" ) );

    m_Animator = new Animator( *ak47, animationManager );
    if (!m_Animator->AddAnimation( "Reload", animationManager.LoadAnimation( "DemoRoomData/Models/AK47/AK47Reload.manim" ) ))
    {
        MsgLogger::LogWarning("Failed to load AK47 reloading animation");
    }
    //if (!m_Animator->AddAnimation( "Static", animationManager.LoadAnimation( "DemoRoomData/Models/AK47/AK47Static.manim" ) ))
    //{
    //    MsgLogger::LogWarning("Failed to load AK47 static animation");
    //}
    //m_Animator->Play( "Static", true );
    /*---------------------------------------------------*/

    /*---------------------------------------------------*/
    AudioDataManager::AudioDataHandle audioHandle = audioDataManager.LoadAudioData( "DemoRoomData/Audio/ak47SingleShot.ogg" );
    m_ShootingSoundSource = new SoundSource( *ak47, systemInterface, soundManager, audioDataManager );
    m_ShootingSoundSource->SetFloatParameter( AudioDevice::Source::e_FloatParamRolloffFactor, 0.0f );
    m_ShootingSoundSource->SetIsRelative( true );
    m_ShootingSoundSource->SetFloatParameter( AudioDevice::Source::e_FloatParamGain, 0.1f );
    m_ShootingSoundSource->SetAudioData( audioHandle );
    /*---------------------------------------------------*/


    GameObject* ak47_ReloadSound = new GameObject( "AK47 Model" );
    /*---------------------------------------------------*/
    AudioDataManager::AudioDataHandle audioHandle2 = audioDataManager.LoadAudioData( "DemoRoomData/Audio/ak47Reload.ogg" );
    m_ReloadSoundSource = new SoundSource( *ak47_ReloadSound, systemInterface, soundManager, audioDataManager );
    m_ReloadSoundSource->SetFloatParameter( AudioDevice::Source::e_FloatParamRolloffFactor, 0.0f );
    m_ReloadSoundSource->SetIsRelative( true );
    m_ReloadSoundSource->SetFloatParameter( AudioDevice::Source::e_FloatParamGain, 0.5f );
    m_ReloadSoundSource->SetAudioData( audioHandle2 );
    /*---------------------------------------------------*/

    ak47->AddChild( ak47_ReloadSound );

    offset->AddChild( ak47 );

    _AddLogic( this );

    m_Offset->SetPosition( glm::vec3( -0.5f, -0.5f, 0.95f ) );
    m_Offset->SetScale( glm::vec3( 1.7f ) );
}

//-----------------------------------------------------------------------

AK47::~AK47()
{
    m_SystemInterface.DeregisterDrawableSurfaceSizeChangedCallback( _DrawableSurfaceSizeChaged, this );
}

//-----------------------------------------------------------------------

void AK47::SetState(State newState)
{
    if( newState != m_CurrentState )
    {
        m_GlobalTimeInSecondDuringStateChange = m_SystemInterface.GetTicksInSeconds();

        if( newState == e_StateStatic )
        {
            m_Animator->Play( "Static", true );
        }
        else if( newState == e_StateReloading )
        {
            //m_Animator->Play( "Reload", false );
            m_ReloadSoundSource->Play();
        }
        else if( newState == e_StateShooting )
        {
            //m_Animator->Play( "Shoot" );
            m_ShootingSoundSource->Play();
        }

        m_CurrentState = newState;
    }
}

//-----------------------------------------------------------------------

AK47::State AK47::GetState() const
{
    return m_CurrentState;
}

//-----------------------------------------------------------------------

void AK47::_DrawableSurfaceSizeChaged(const SystemInterface::DrawableSurfaceSize& newSize, void* userData)
{
    static_cast<AK47*>( userData )->m_DrawableSurfaceSize = newSize;
}

//-----------------------------------------------------------------------

void AK47::_Initialize()
{
    SetState( e_StateStatic );
}

//-----------------------------------------------------------------------

void AK47::_FixedUpdate()
{

}

//-----------------------------------------------------------------------

void AK47::_Update()
{
    GetComponent<Transform>()->SetScale( glm::vec3( 0.125f ) ); // Make sure the gun does not collide with the walls
    GetComponent<Transform>()->SetPosition( m_Camera.GetPosition() );
    GetComponent<Transform>()->SetRotation( MathUtils::EulerXYZAnglesToQuaternion( glm::vec3( -m_Camera.GetPitchInDegrees(),
                                                                                              m_Camera.GetYawInDegrees(), 0.0f ) ) );

    if( m_CurrentState == e_StateShooting )
    {
        const float timeShooting = static_cast<float>( m_SystemInterface.GetTicksInSeconds() - m_GlobalTimeInSecondDuringStateChange );

        int previousMagazineValue = m_BulletsInTheMagazine;
        m_BulletsInTheMagazine = static_cast<int>( c_MagazineCapacity - ( c_BulletsPerSecond * timeShooting ) );

        if( m_BulletsInTheMagazine < previousMagazineValue )
        {
            m_ShootingSoundSource->Stop();
            m_ShootingSoundSource->Play();
        }

        if( m_BulletsInTheMagazine <= 0 )
        {
            m_ShootingSoundSource->Stop();
            SetState( e_StateReloading );
        }
        else
        {
            GameObject* shotObject = _GetShotObject();
            if( shotObject && ( shotObject->GetID() == GameObjectTypes::c_ShootingTarget ) )
            {
                ShootingTarget* shootingTarget = static_cast<ShootingTarget*>( shotObject );
                shootingTarget->SetState( ShootingTarget::e_StateHit );
            }
        }
    }
    else if( m_CurrentState == e_StateReloading )
    {
        if( !m_Animator->IsPlaying() && m_ReloadSoundSource->IsStopped() )
        {
            SetState( e_StateStatic );
        }
    }
}

//-----------------------------------------------------------------------

void AK47::_LastUpdate()
{

}

//-----------------------------------------------------------------------

GameObject* AK47::_GetShotObject()
{
    EngineTypes::VPTransformations vpTrans;
    vpTrans.viewMatrix = m_Camera.GetViewMatrix();
    vpTrans.projectionMatrix = m_Camera.GetProjectionMatrix();

    glm::vec3 rayOrigin = m_Camera.GetPosition();
    glm::vec3 rayDirection = glm::vec3( 0.0f );

    MathUtils::CoordinateInScreenSpaceToRay( static_cast<float>( m_DrawableSurfaceSize.widthInPixels / 2 ),
                                             static_cast<float>( m_DrawableSurfaceSize.heightInPixels / 2 ),
                                             m_DrawableSurfaceSize.widthInPixels,
                                             m_DrawableSurfaceSize.heightInPixels,
                                             vpTrans,
                                             m_Camera.GetPosition(),
                                             rayDirection );

    GameObject* gameObjectHit = nullptr;
    m_PhysicsManager.CastARay( rayOrigin, rayDirection, gameObjectHit );

    return gameObjectHit;
}
