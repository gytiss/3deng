#ifndef LUAUTILS_H_INCLUDED
#define LUAUTILS_H_INCLUDED

extern "C"
{
    #include <lua.hpp>
}

#include <string>

class FileManager;

namespace LuaUtils
{
    std::string GetLuaErrorMsg(lua_State* L); ///< Fetch error message from the top of the stack
    bool LoadLuaScript(lua_State* L,
                       const std::string& scriptPath,
                       FileManager& fileManager); ///< Loads and executes a given Lua script file

    class LuaStateWrapper
    {
    public:
       LuaStateWrapper();
//       LuaStateWrapper (lua_State* val);
       ~LuaStateWrapper();
       operator lua_State* ();

    private:
        lua_State* m_LuaState;
    };
}

#endif // LUAUTILS_H_INCLUDED
