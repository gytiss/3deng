#ifndef OPENGLINCLUDES_H_INCLUDED
#define OPENGLINCLUDES_H_INCLUDED

#if !defined(OS_ANDROID) && !defined(OS_IOS) && !defined(OPENGL_ES)
    #define GLEW_NO_GLU 1
    #define GL3_PROTOTYPES 1 // Ensure we are using opengl's core profile only
    #include <GL/glew.h>
#endif

#ifdef OPENGL_ES
    #if defined(OS_ANDROID) || defined(OS_GENERIC_UNIX)
        #include <GLES3/gl3.h>
        #include <GLES3/gl3ext.h>
        #include <GLES3/gl3platform.h>
    #else
        #ifdef OS_IOS
            #include <OpenGLES/ES3/gl.h>
        #endif
    #endif

    #ifndef GL_TEXTURE_MAX_ANISOTROPY_EXT
        #define GL_TEXTURE_MAX_ANISOTROPY_EXT 0x84FE
    #endif

    #ifndef GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT
        #define GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT 0x84FF
    #endif
#else
    #ifdef __APPLE__
        #include <OpenGL/gl.h>
    #else
        #include <GL/glcorearb.h>
    #endif
#endif

#endif // OPENGLINCLUDES_H_INCLUDED
