#ifndef BOUNDINGFRUSTUM_H_INCLUDED
#define BOUNDINGFRUSTUM_H_INCLUDED

#include <glm/glm.hpp>

class BoundingFrustum
{
public:
    BoundingFrustum(const glm::mat4& viewProjectionMatrix);

    bool Contains(const glm::vec3 pointInWorldSpace);

private:
    float _PlaneDotCoord(const glm::vec4 plane, const glm::vec3& vector);

    glm::vec4 m_FrustumPlanes[6];
};


#endif // BOUNDINGFRUSTUM_H_INCLUDED
