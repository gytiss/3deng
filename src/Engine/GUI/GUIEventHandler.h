#ifndef GUIEVENTHANDLER_H_INCLUDED
#define GUIEVENTHANDLER_H_INCLUDED

#include <Rocket/Core.h>

class GUIEventHandler
{
public:
    virtual ~GUIEventHandler();
	virtual void ProcessEvent(Rocket::Core::Event& event, const Rocket::Core::String& value) = 0;
};

#endif // GUIEVENTHANDLER_H_INCLUDED
