#!/usr/bin/env bash
 
export PATH=$PATH:/home/john/Android/Sdk/tools/:/home/john/Android/Sdk/platform-tools/:/home/john/Android/android-ndk-r10d/
 
PACKAGE=org.TNK

adb root

adb shell rm -rf /data/data/org.TNK/cache/valgrind.log

adb push start_valgrind.sh /data/local/
adb push valgrind.supp /data/local/
adb shell chmod 777 /data/local/start_valgrind.sh
 
adb shell setprop wrap.$PACKAGE "logwrapper /data/local/start_valgrind.sh"
# use this to disable valgrind for this pkg
# adb shell setprop wrap.se.frals.example ""

echo "wrap.$PACKAGE: $(adb shell getprop wrap.$PACKAGE)"
 
adb shell am force-stop $PACKAGE
adb shell am start -a android.intent.action.MAIN -n $PACKAGE/.TNKActivity
adb logcat -c 