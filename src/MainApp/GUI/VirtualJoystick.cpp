#include "VirtualJoystick.h"
#include <MathUtils.h>
#include <Renderer/RenderManager.h>
#include <Resources/MeshManager.h>
#include <Resources/MaterialManager.h>
#include <Resources/TextureManager.h>
#include <Scene/Transform.h>
#include <Scene/MeshRenderer.h>
#include <Scene/MeshFilter.h>

//-----------------------------------------------------------------------

VirtualJoystick::VirtualJoystick(const glm::vec2& centre,
                                 float radius,
                                 float sensitivity,
                                 MeshManager& meshManager,
                                 MaterialManager& materialManager,
                                 TextureManager& textureManager,
                                 SystemInterface& systemInterface)
: m_NormalizedCentre( centre ),
  m_NormalizedRadius( radius ),
  m_Sensitivity( sensitivity ),
  m_AbsoluteCenter( 0.0f ),
  m_AbsoluteRadius( 0.0f ),
  m_AbsoluteThumbRadius( 0.0f ),
  m_CurrentAbsolutePos( 0.0f ),
  m_MeshManager( meshManager ),
  m_MaterialManager( materialManager ),
  m_TextureManager( textureManager ),
  m_SystemInterface( systemInterface ),
  m_DrawableSurfaceSize( m_SystemInterface.GetDrawableSurfaceSize() ),
  m_Readings{ 0.0f },
  m_JoystickBaseGameObject( "Joystick Base" ),
  m_JoystickThumbGameObject( "Joystick Thumb" )
{
    _DrawableSurfaceSizeChaged( m_SystemInterface.GetDrawableSurfaceSize(), this );
    _CreateVisuals( m_JoystickBaseGameObject, m_JoystickThumbGameObject );
    m_SystemInterface.RegisterDrawableSurfaceSizeChangedCallback( _DrawableSurfaceSizeChaged, this );
}

//-----------------------------------------------------------------------

VirtualJoystick::~VirtualJoystick()
{
    m_SystemInterface.DeregisterDrawableSurfaceSizeChangedCallback( _DrawableSurfaceSizeChaged, this );
}

//-----------------------------------------------------------------------

bool VirtualJoystick::LocationIsOnTheJoystick(const glm::vec2& absoluteScreenCoord)
{
    bool retVal = true;

    retVal = retVal && ( absoluteScreenCoord.x >= ( m_AbsoluteCenter.x - m_AbsoluteThumbRadius ) );
    retVal = retVal && ( absoluteScreenCoord.x <= ( m_AbsoluteCenter.x + m_AbsoluteThumbRadius ) );
    retVal = retVal && ( absoluteScreenCoord.y >= ( m_AbsoluteCenter.y - m_AbsoluteThumbRadius ) );
    retVal = retVal && ( absoluteScreenCoord.y <= ( m_AbsoluteCenter.y + m_AbsoluteThumbRadius ) );

    return retVal;
}

//-----------------------------------------------------------------------

void VirtualJoystick::Update(const glm::vec2& newRelativeScreenCoordinates)
{
    if( ( newRelativeScreenCoordinates.x != 0.0f ) || ( newRelativeScreenCoordinates.y != 0.0f ) )
    {
        // Remap to the range of -1.0 to 1.0 and then scale with the screen size
        const float newX = MathUtils::MapRange( newRelativeScreenCoordinates.x, 0.0f, 1.0f, -1.0f, 1.0f ) * m_DrawableSurfaceSize.widthInPixels * 0.1f;
        const float newY = MathUtils::MapRange( newRelativeScreenCoordinates.y, 0.0f, 1.0f, -1.0f, 1.0f ) * m_DrawableSurfaceSize.heightInPixels * 0.1f;

        const glm::vec2 newAbsolutePos = ( m_CurrentAbsolutePos + glm::vec2( newX, newY ) );
        const glm::vec2 newDirection = ( newAbsolutePos - m_AbsoluteCenter );

        const float maxAbsoluteThumbMoveRadius = ( m_AbsoluteRadius - ( m_AbsoluteThumbRadius + ( m_AbsoluteThumbRadius / 2.0f ) ) );

        const float thumbsDistanceFromTheBaseCenter = glm::length( newDirection );
        if( thumbsDistanceFromTheBaseCenter <= maxAbsoluteThumbMoveRadius )
        {
            m_CurrentAbsolutePos = newAbsolutePos;
        }
        else // Clamp
        {
            m_CurrentAbsolutePos = ( ( glm::normalize( newDirection ) * maxAbsoluteThumbMoveRadius ) + m_AbsoluteCenter );
        }

        const glm::vec2 joystickDirection = ( m_CurrentAbsolutePos - m_AbsoluteCenter );
        const float joystickDirectionStrength = glm::clamp( 0.0f, 1.0f, glm::length( joystickDirection ) / maxAbsoluteThumbMoveRadius );
        const float heading = MathUtils::HeadingFromADirectionVector( glm::vec3( joystickDirection.x, 0.0f, joystickDirection.y ) );

        m_Readings[e_AxisVertical] = 0.0f;
        m_Readings[e_AxisHorizontal] = 0.0f;

        // Forward
        if( ( ( heading >= 0.0f ) && ( heading < 90.0f ) ) ||
            ( ( heading <= 360.0f ) && ( heading > 270.0f ) ) )
        {
            m_Readings[e_AxisVertical] = m_Sensitivity * joystickDirectionStrength;

            if( heading > 270.0f )
            {
                m_Readings[e_AxisVertical] *= MathUtils::MapRange( heading, 271.0f, 360.0f, 0.0f, 1.0f );
            }
            else
            {
                m_Readings[e_AxisVertical] *= 1.0f - MathUtils::MapRange( heading, 0.0f, 89.0f, 0.0f, 1.0f );
            }

            m_Readings[e_AxisVertical] *= -1.0f;
        }

        // Back
        if( ( heading >= 90.0f ) && ( heading <= 270.0f ) )
        {
            m_Readings[e_AxisVertical] = m_Sensitivity * joystickDirectionStrength;

            if( heading < 180.0f )
            {
                m_Readings[e_AxisVertical] *= MathUtils::MapRange( heading, 90.0f, 179.0f, 0.0f, 1.0f );
            }
            else
            {
                m_Readings[e_AxisVertical] *= 1.0f - MathUtils::MapRange( heading, 180.0f, 270.0f, 0.0f, 1.0f );
            }
        }

        // Left
        if( ( heading >= 0.0f ) && ( heading <= 180.0f ) )
        {
            m_Readings[e_AxisHorizontal] = m_Sensitivity * joystickDirectionStrength;

            if( heading < 90.0f )
            {
                m_Readings[e_AxisHorizontal] *= MathUtils::MapRange( heading, 0.0f, 89.0f, 0.0f, 1.0f );
            }
            else
            {
                m_Readings[e_AxisHorizontal] *= 1.0f - MathUtils::MapRange( heading, 90.0f, 180.0f, 0.0f, 1.0f );
            }

            m_Readings[e_AxisHorizontal] *= -1.0f;
        }

        // Right
        if( ( heading >= 180.0f ) && ( heading <= 360.0f ) )
        {
            m_Readings[e_AxisHorizontal] = m_Sensitivity * joystickDirectionStrength;

            if( heading < 270.0f )
            {
                m_Readings[e_AxisHorizontal] *= MathUtils::MapRange( heading, 180.0f, 269.0f, 0.0f, 1.0f );
            }
            else
            {
                m_Readings[e_AxisHorizontal] *= 1.0f - MathUtils::MapRange( heading, 270.0f, 360.0f, 0.0f, 1.0f );
            }
        }

        m_JoystickThumbGameObject.GetComponent<Transform>()->SetPosition( glm::vec3( m_CurrentAbsolutePos.x, m_CurrentAbsolutePos.y, 0.0f ) );
    }
}

//-----------------------------------------------------------------------

void VirtualJoystick::Reset()
{
    m_CurrentAbsolutePos = m_AbsoluteCenter;
    m_Readings[e_AxisVertical] = 0.0f;
    m_Readings[e_AxisHorizontal] = 0.0f;

    m_JoystickBaseGameObject.GetComponent<Transform>()->SetPosition( glm::vec3( m_AbsoluteCenter.x, m_AbsoluteCenter.y, 0.0f ) );
    m_JoystickBaseGameObject.GetComponent<Transform>()->SetScale( glm::vec3( m_AbsoluteRadius, 1.0f, m_AbsoluteRadius ) );

    m_JoystickThumbGameObject.GetComponent<Transform>()->SetPosition( glm::vec3( m_CurrentAbsolutePos.x, m_CurrentAbsolutePos.y, 0.0f ) );
    m_JoystickThumbGameObject.GetComponent<Transform>()->SetScale( glm::vec3( m_AbsoluteThumbRadius, 1.0f, m_AbsoluteThumbRadius ) );
}

//-----------------------------------------------------------------------

void VirtualJoystick::AddRenderables(RenderManager& renderManager)
{
    GameObject::RenderablesList renderables;
    m_JoystickBaseGameObject.GetRenderablesOfTheHierarchy( renderables );
    m_JoystickThumbGameObject.GetRenderablesOfTheHierarchy( renderables );

    for(const RenderableObject& r : renderables)
    {
        renderManager.AddGUIRenderables( r );
    }
}

//-----------------------------------------------------------------------

void VirtualJoystick::_CreateVisuals(GameObject& baseGameObject,
                                     GameObject& thumbGameObject)
{
    MaterialDescriptor joystickBaseMaterialDescriptor( m_TextureManager );
    MaterialDescriptor joystickThumbMaterialDescriptor( m_TextureManager );

    TextureManager::TextureHandle baseTextureHandle = m_TextureManager.Load2DTexture( "DemoRoomData/Models/VirtualJoystick/JoystickBackgroundTransparent.mtc",
                                                                                      Texture::e_TypeIdxDiffuseMap,
                                                                                      Texture::e_StateParamArgSCoordWrapClampToEdge,
                                                                                      Texture::e_StateParamArgTCoordWrapClampToEdge,
                                                                                      false,
                                                                                      false );

    TextureManager::TextureHandle thumbTextureHandle = m_TextureManager.Load2DTexture( "DemoRoomData/Models/VirtualJoystick/JoystickThumbTransparent.mtc",
                                                                                       Texture::e_TypeIdxDiffuseMap,
                                                                                       Texture::e_StateParamArgSCoordWrapClampToEdge,
                                                                                       Texture::e_StateParamArgTCoordWrapClampToEdge,
                                                                                       false,
                                                                                       false );
    joystickBaseMaterialDescriptor.SetDiffuseTextureMap( baseTextureHandle );
    joystickThumbMaterialDescriptor.SetDiffuseTextureMap( thumbTextureHandle );

    joystickBaseMaterialDescriptor.SetDiffuseColor( EngineTypes::SRGBColor( 0.0f, 0.0f, 1.0f ) );
    joystickThumbMaterialDescriptor.SetDiffuseColor( EngineTypes::SRGBColor( 0.0f, 0.0f, 1.0f ) );

    MaterialManager::MaterialHandle baseMaterial = m_MaterialManager.AddInternalMaterial( joystickBaseMaterialDescriptor );
    MaterialManager::MaterialHandle thumbMaterial = m_MaterialManager.AddInternalMaterial( joystickThumbMaterialDescriptor );

    MeshRenderer* baseRenderer = new MeshRenderer( baseGameObject, m_MaterialManager );
    MeshFilter* baseMeshFilter = new MeshFilter( baseGameObject, m_MeshManager );
    MeshManager::MeshHandle baseMeshHandle = m_MeshManager.GetInternalMeshHandle( MeshManager::e_InternalMeshTypePlane );
    baseMeshFilter->SetMesh( baseMeshHandle );
    baseRenderer->SetSubMeshMaterial( 0, baseMaterial );
    baseRenderer->SetDepthTestingEnabled( false );

    MeshRenderer* thumbRenderer = new MeshRenderer( thumbGameObject, m_MaterialManager );
    MeshFilter* thumbMeshFilter = new MeshFilter( thumbGameObject, m_MeshManager );
    MeshManager::MeshHandle thumbMeshHandle = m_MeshManager.GetInternalMeshHandle( MeshManager::e_InternalMeshTypePlane );
    thumbMeshFilter->SetMesh( thumbMeshHandle );
    thumbRenderer->SetSubMeshMaterial( 0, thumbMaterial );
    thumbRenderer->SetDepthTestingEnabled( false );

    // Rotate the planes so they are facing the screen
    baseGameObject.GetComponent<Transform>()->SetRotation( glm::vec3( 90.0f, 0.0f, 0.0f ) );
    thumbGameObject.GetComponent<Transform>()->SetRotation( glm::vec3( 90.0f, 0.0f, 0.0f ) );
}

//-----------------------------------------------------------------------

void VirtualJoystick::_RecalculateAbsoluteSize()
{
    m_AbsoluteCenter.x = ( m_NormalizedCentre.x * m_DrawableSurfaceSize.widthInPixels );
    m_AbsoluteCenter.y = ( m_NormalizedCentre.y * m_DrawableSurfaceSize.heightInPixels );

    size_t smallestSceenSide = m_DrawableSurfaceSize.heightInPixels;
    if( m_DrawableSurfaceSize.widthInPixels < m_DrawableSurfaceSize.heightInPixels )
    {
        smallestSceenSide = m_DrawableSurfaceSize.widthInPixels;
    }

    const float c_TumbScaleRelativeToTheBase = ( 1.0f / 2.608f );
    m_AbsoluteRadius = ( m_NormalizedRadius * smallestSceenSide );
    m_AbsoluteThumbRadius = ( m_AbsoluteRadius * c_TumbScaleRelativeToTheBase );

    Reset();
}

//-----------------------------------------------------------------------

void VirtualJoystick::_DrawableSurfaceSizeChaged(const SystemInterface::DrawableSurfaceSize& newSize, void* userData)
{
    static_cast<VirtualJoystick*>( userData )->m_DrawableSurfaceSize = newSize;
    static_cast<VirtualJoystick*>( userData )->_RecalculateAbsoluteSize();
}

//-----------------------------------------------------------------------
