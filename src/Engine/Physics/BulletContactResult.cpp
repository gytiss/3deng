#include "BulletContactResult.h"
#include <cassert>

BulletContact BulletContactResult::_empty;

////////////////////////////////////////////////////////////////////
//     Function: BulletContactResult::Constructor
//       Access: Protected
//  Description:
////////////////////////////////////////////////////////////////////
BulletContactResult::BulletContactResult()
: btCollisionWorld::ContactResultCallback()
{

}

BulletContactResult::~BulletContactResult()
{

}

////////////////////////////////////////////////////////////////////
//     Function: BulletContactResult::addSingleResult
//       Access: Published
//  Description:
////////////////////////////////////////////////////////////////////
btScalar BulletContactResult::addSingleResult(btManifoldPoint &mp,
                                              const btCollisionObjectWrapper* colObj0Wrap,
                                              int part_id0,
                                              int idx0,
                                              const btCollisionObjectWrapper* colObj1Wrap,
                                              int part_id1,
                                              int idx1)
{
    BulletContact contact;

    contact._mp = &mp;
    contact._obj0 = colObj0Wrap->getCollisionObject();
    contact._obj1 = colObj1Wrap->getCollisionObject();
    contact._part_id0 = part_id0;
    contact._part_id1 = part_id1;
    contact._idx0 = idx0;
    contact._idx1 = idx1;

    _contacts.push_back(contact);

    return 1.0f;
}


////////////////////////////////////////////////////////////////////
//     Function: BulletContactResult::get_node0
//       Access: Published
//  Description:
////////////////////////////////////////////////////////////////////
const btCollisionObject* BulletContact::get_node0() const
{
    return _obj0;
}

////////////////////////////////////////////////////////////////////
//     Function: BulletContactResult::get_node1
//       Access: Published
//  Description:
////////////////////////////////////////////////////////////////////
const btCollisionObject* BulletContact::get_node1() const
{
    return _obj1;
}

////////////////////////////////////////////////////////////////////
//     Function: BulletContactResult::get_manifold_point
//       Access: Published
//  Description:
////////////////////////////////////////////////////////////////////
const BulletManifoldPoint* BulletContact::get_manifold_point() const
{
    return new BulletManifoldPoint(*_mp);
}

////////////////////////////////////////////////////////////////////
//     Function: BulletContactResult::get_idx0
//       Access: Published
//  Description:
////////////////////////////////////////////////////////////////////
int BulletContact::get_idx0() const
{
    return _idx0;
}

////////////////////////////////////////////////////////////////////
//     Function: BulletContactResult::get_idx1
//       Access: Published
//  Description:
////////////////////////////////////////////////////////////////////
int BulletContact::get_idx1() const
{
    return _idx1;
}

////////////////////////////////////////////////////////////////////
//     Function: BulletContactResult::get_part_id0
//       Access: Published
//  Description:
////////////////////////////////////////////////////////////////////
int BulletContact::get_part_id0() const
{
    return _part_id0;
}

////////////////////////////////////////////////////////////////////
//     Function: BulletContactResult::get_part_id1
//       Access: Published
//  Description:
////////////////////////////////////////////////////////////////////
int BulletContact::get_part_id1() const
{
    return _part_id1;
}

////////////////////////////////////////////////////////////////////
//     Function: BulletContactResult::get_num_contacts
//       Access: Published
//  Description:
////////////////////////////////////////////////////////////////////
size_t BulletContactResult::get_num_contacts() const
{
    return _contacts.size();
}

////////////////////////////////////////////////////////////////////
//     Function: BulletContactResult::get_contact
//       Access: Published
//  Description:
////////////////////////////////////////////////////////////////////
const BulletContact& BulletContactResult::get_contact(size_t idx) const
{
    assert( idx < _contacts.size() );

    return _contacts[idx];
}
