#ifndef MODELBINOBJ_H_INCLUDED
#define MODELBINOBJ_H_INCLUDED

#include <string>
#include <cstdint>

class ModelBinObj
{
public:
    struct Material
    {
        float ambient[4];
        float diffuse[4];
        float specular[4];
        float shininess;        // [0 = min shininess, 1 = max shininess]
        float alpha;            // [0 = fully transparent, 1 = fully opaque]

        std::string name;
        std::string colorMapFilename;
        std::string bumpMapFilename;

        int materialID;
    };

    struct Vertex
    {
        float position[3];
        float texCoord[2];
        float normal[3];
        float tangent[4];
        float bitangent[3];
    };

    struct Mesh
    {
        int startIndex;
        int triangleCount;
        const Material *pMaterial;
    };

    ModelBinObj();
    ~ModelBinObj();

    void destroy();
    bool import(std::string modelFileName);

    const int *getIndexBuffer() const;
    int getIndexSize() const;

    const Material &getMaterial(int i) const;
    const Mesh &getMesh(int i) const;

    int getNumberOfIndices() const;
    int getNumberOfMaterials() const;
    int getNumberOfMeshes() const;
    int getNumberOfTriangles() const;
    int getNumberOfVertices() const;

    const std::string &getPath() const;

    const Vertex &getVertex(int i) const;
    const Vertex *getVertexBuffer() const;
    int getVertexSize() const;

private:
    int m_NumberOfVertexCoords;
    int m_NumberOfTextureCoords;
    int m_NumberOfNormals;
    int m_NumberOfTriangles;
    int m_NumberOfMaterials;
    int m_NumberOfMeshes;

    inline int32_t getInt(int8_t* data, size_t* bytesRead);
    inline int8_t getChar(int8_t* data, size_t* bytesRead);
    inline float getFloat(int8_t* data, size_t* bytesRead);

    std::string m_DirectoryPath;

    Mesh* m_Meshes;
    Material* m_Materials;
    Vertex* m_VertexBuffer;
    int* m_IndexBuffer;

};

	//-----------------------------------------------------------------------

    inline const int *ModelBinObj::getIndexBuffer() const
    {
        return m_IndexBuffer;
    }

	//-----------------------------------------------------------------------

    inline int ModelBinObj::getIndexSize() const
    {
        return static_cast<int>(sizeof(int));
    }

	//-----------------------------------------------------------------------

    inline const ModelBinObj::Material &ModelBinObj::getMaterial(int i) const
    {
        return m_Materials[i];
    }

	//-----------------------------------------------------------------------

    inline const ModelBinObj::Mesh &ModelBinObj::getMesh(int i) const
    {
        return m_Meshes[i];
    }

	//-----------------------------------------------------------------------

    inline int ModelBinObj::getNumberOfIndices() const
    {
        return (m_NumberOfTriangles * 3);
    }

	//-----------------------------------------------------------------------

    inline int ModelBinObj::getNumberOfMaterials() const
    {
        return m_NumberOfMaterials;
    }

	//-----------------------------------------------------------------------

    inline int ModelBinObj::getNumberOfMeshes() const
    {
        return m_NumberOfMeshes;
    }

	//-----------------------------------------------------------------------

    inline int ModelBinObj::getNumberOfTriangles() const
    {
        return m_NumberOfTriangles;
    }

	//-----------------------------------------------------------------------

    inline int ModelBinObj::getNumberOfVertices() const
    {
        return static_cast<int>(m_NumberOfVertexCoords);
    }

	//-----------------------------------------------------------------------

    inline const std::string &ModelBinObj::getPath() const
    {
        return m_DirectoryPath;
    }

	//-----------------------------------------------------------------------

    inline const ModelBinObj::Vertex &ModelBinObj::getVertex(int i) const
    {
        return m_VertexBuffer[i];
    }

	//-----------------------------------------------------------------------

    inline const ModelBinObj::Vertex *ModelBinObj::getVertexBuffer() const
    {
        return m_VertexBuffer;
    }

	//-----------------------------------------------------------------------

    inline int ModelBinObj::getVertexSize() const
    {
        return static_cast<int>(sizeof(Vertex));
    }

	//-----------------------------------------------------------------------

#endif // MODELBINOBJ_H_INCLUDED
