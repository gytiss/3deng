#ifndef BOXCOLLIDERCOMPONENT_H_INCLUDED
#define BOXCOLLIDERCOMPONENT_H_INCLUDED

#include "Collider.h"

class BoxCollider : public Collider
{
    GAME_OBJECT_COMPONENT_TYPE(e_TypeCollider)

public:
    BoxCollider(GameObject& owningGameObject,
                const glm::vec3& size,
                PhysicsManager& physicsManager);

    void SetSize(const glm::vec3& size);

    inline void SetSizeX(float xAxisSize)
    {
        glm::vec3 size = GetSize();
        size.x = xAxisSize;
        SetSize( size );
    }

    inline void SetSizeY(float yAxisSize)
    {
        glm::vec3 size = GetSize();
        size.y = yAxisSize;
        SetSize( size );
    }

    inline void SetSizeZ(float zAxisSize)
    {
        glm::vec3 size = GetSize();
        size.z = zAxisSize;
        SetSize( size );
    }

    glm::vec3 GetSize() const;

    inline float GetSizeX() const
    {
        glm::vec3 size = GetSize();
        return size.x;
    }

    inline float GetSizeY() const
    {
        glm::vec3 size = GetSize();
        return size.y;
    }

    inline float GetSizeZ() const
    {
        glm::vec3 size = GetSize();
        return size.z;
    }
};

#endif // BOXCOLLIDERCOMPONENT_H_INCLUDED
