#include "VAO.h"
#include "IBO.h"

#include <CommonDefinitions.h>

//-----------------------------------------------------------------------

VAO::VAO()
: m_Id( 0 ),
  m_AttachedIBO( nullptr ),
  m_AttachedVBOs(),
  m_VBOsToAttributesMap(),
  m_VertexAttributesSuppliedByAllVBOsCombined( 0 )
{
    for( size_t i = 0; i < ArraySize( m_AttachedVBOs ); i++ )
    {
        m_AttachedVBOs[i] = nullptr;
    }
    glGenVertexArrays( 1, &m_Id ); // Claim one vertex array ID from OpenGL
    CheckForGLErrors();
}

//-----------------------------------------------------------------------

VAO::~VAO()
{
    if( m_AttachedIBO )
    {
        delete m_AttachedIBO;
        m_AttachedIBO = nullptr;
    }

    // Delete all attached VBOs
    for (std::map<VBO*, VBO::VertexAttrMask>::iterator
         i = m_VBOsToAttributesMap.begin(); i != m_VBOsToAttributesMap.end(); ++i)
    {
        // *i is a key-value pair
        if( i->first )
        {
            delete i->first;
        }
    }

    Unbind();
    glDeleteVertexArrays( 1, &m_Id ); // Relinquish vertex array ID to OpenGL
    CheckForGLErrors();
    m_Id = 0;
}

//-----------------------------------------------------------------------

void VAO::AddVBO(VBO* vbo)
{
    if( vbo )
    {
        Bind();

        vbo->Bind();

        const GLsizei vboVertexSize = static_cast<GLsizei>( vbo->GetVertexSize() );

        const VBO::VertexAttrMask attributes = vbo->GetAtributes();
        for( unsigned int i = 0; i < VBO::e_VertexAttrIdxCount; i++ )
        {
            const VBO::VertexAttrIdx attrIdx = static_cast<VBO::VertexAttrIdx>( i );

            if( attributes.Contains( 1 << attrIdx ) ) // Attribute is set
            {
                if( _IsAttributeSuppliedByExistingVBO( attrIdx ) )
                {
                    _DissociateAttributeFromExistingVBO( attrIdx );
                }

                m_VertexAttributesSuppliedByAllVBOsCombined.AddMask( attributes );

                // Add new VBO to the VBO attribute map
                if( m_VBOsToAttributesMap.find( vbo ) != m_VBOsToAttributesMap.end() )
                {
                    m_VBOsToAttributesMap[vbo].SetBit( attrIdx );
                }
                else
                {
                    m_VBOsToAttributesMap[vbo] = VBO::VertexAttrMask( 1 << attrIdx );
                }

                m_AttachedVBOs[i] = vbo; // Set this VBO as a provider of a given attribute

                glVertexAttribPointer ( attrIdx,
                                        vbo->GetAttributeComponentCount( attrIdx ),
                                        VBO::GetAttributeOpenGLDataType( vbo->GetAttributeComponentType( attrIdx ) ),
                                        GL_FALSE,
                                        vboVertexSize,
                                        reinterpret_cast<const GLvoid*>( vbo->GetAttributeOffset( attrIdx ) ) );
                CheckForGLErrors();

                glEnableVertexAttribArray( attrIdx );
                CheckForGLErrors();
            }
        }

        vbo->Unbind();

        Unbind();
    }
}

//-----------------------------------------------------------------------

void VAO::SetIBO(IBO* ibo)
{
    if( ibo )
    {
        Bind();
        ibo->Bind();

        if( m_AttachedIBO ) // If set, delete old IBO
        {
            delete m_AttachedIBO;
            m_AttachedIBO = nullptr;
        }
        m_AttachedIBO = ibo;

        // Warning: !!! Must unbind VAO and NOT the IBO first, unbinding IBO before VAO would dissociate
        //              IBO from VAO and make application Segfault on a first draw call using this VAO !!!
        Unbind();
        ibo->Unbind();
    }
}

//-----------------------------------------------------------------------

bool VAO::_IsAttributeSuppliedByExistingVBO( VBO::VertexAttrIdx attrIdx ) const
{
    return ( m_AttachedVBOs[attrIdx] != nullptr );
}

//-----------------------------------------------------------------------

void VAO::_DissociateAttributeFromExistingVBO( VBO::VertexAttrIdx attrIdx )
{
    // Note: Deletion in done only when VBO pointer
    //       is not used by any of the other attributes

    VBO* existingVBO = m_AttachedVBOs[attrIdx];
    VBO::VertexAttrMask& attrsOfExistingVBO = m_VBOsToAttributesMap[existingVBO];
    attrsOfExistingVBO.ClearBit( attrIdx ); // Remove attribute from the list of attributes supplied by the VBO

    if( !attrsOfExistingVBO.Value() ) // Existing VBO has no more attributes
    {
        m_VBOsToAttributesMap.erase( existingVBO );
        delete existingVBO;
        existingVBO = nullptr;
        m_AttachedVBOs[attrIdx] = nullptr;
    }
}

//-----------------------------------------------------------------------
