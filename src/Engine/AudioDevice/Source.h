#ifndef AUDIODEVICE_SOURCE_H_INCLUDED
#define AUDIODEVICE_SOURCE_H_INCLUDED

#include <cstddef>
#include <array>
#include <glm/glm.hpp>
#include "OpenALIncludes.h"
#include "Context.h"
#include "EffectSlotActivationListener.h"

namespace AudioDevice
{

class EffectSlot;
class Buffer;

class Source : public EffectSlotActivationListener
{
public:
    class SourceConnectionListener
    {
    protected:
        friend class Source;

        virtual ~SourceConnectionListener() = default;

        virtual void SourceConnected(Source* source) = 0;
        virtual void SourceDisconnected(Source* source) = 0;
    };

    class SourceStatusListener
    {
    protected:
        friend class Source;

        virtual ~SourceStatusListener() = default;

        virtual void SourceStartedPlaying(Source* source) = 0;
        virtual void SourceStoppedPlaying(Source* source) = 0;
    };

    enum SourceType
    {
        e_SourceTypeStatic = 0,
        e_SourceTypeStreaming = 1,
        e_SourceTypeUninitialised
    };

    enum FloatParam
    {
        /**
         * Pitch multiplier (value should always be positive)
         */
        e_FloatParamPitch = 0,

        /**
         * Source gain (value should always be positive)
         */
        e_FloatParamGain = 1,

        /**
         * Used with the Inverse Clamped Distance Model to set the distance
         * where there will no longer be any attenuation of the source
         */
        e_FloatParamMaxDistance,

        /**
         * The rolloff rate for the source. Rolloff is the amount of attenuation
         * that is applied to sounds, based on the listener's distance from the
         * sound source
         */
        e_FloatParamRolloffFactor,

        /**
         * The distance under which the volume for the source would
         * normally drop by half (before being influenced by
         * rolloff factor or "e_FloatParamMaxDistance")
         */
        e_FloatParamReferenceDistance,

        /**
         * The minimum gain for this source
         */
        e_FloatParamMinGain,

        /**
         * The maximum gain for this source
         */
        e_FloatParamMaxGain,

        /**
         * The gain when outside the oriented cone
         */
        e_FloatParamConeOuterGain,

        /**
         * The gain when inside the oriented cone
         */
        e_FloatParamConeInnerAngle,

        /**
         * Outer angle of the sound cone, in degrees
         */
        e_FloatParamConeOuterAngle,

        e_FloatParamCount // Not a float parameter (Used for counting)
    };

    enum OpenALSourceType
    {
        e_OpenALSourceTypeUndetermined = AL_UNDETERMINED,
        e_OpenALSourceTypeStatic = AL_STATIC,
        e_OpenALSourceTypeStreaming = AL_STREAMING
    };

    typedef std::array<float, e_FloatParamCount> FloatParamValues;

    Source(Context& context);
    virtual ~Source();

    inline unsigned int GetALHandle()
    {
        return m_ALSource;
    }

    inline SourceType GetType() const
    {
        return m_CurrentSourceType;
    }

    void Play();
    void Pause();
    void Stop();
    bool IsStopped();

    void SetFloatParameter(FloatParam param, float value);
    void SetFloatParameters(const FloatParamValues& values);
    float GetFloatParameter(FloatParam param);

    /**
     * X, Y, Z position coordinates of the source in world space
     */
    void SetPosition(const glm::vec3& position);

    /**
     * Velocity vector. Velocity in meters per second for each of the three axes i.e. X, Y and Z
     */
    void SetVelocity(const glm::vec3& velocityVector);

    /**
     * Direction vector
     */
    void SetDirection(const glm::vec3& directionVector);

    glm::vec3 GetPosition();
    glm::vec3 GetVelocity();
    glm::vec3 GetDirection();

    /**
     * Determines if the positions are relative to the listener
     * i.e. when set to AL_TRUE, all position values are relative
     * to the listener
     */
    void SetIsRelative(bool value);
    bool IsRelative();

    /**
     * When set to true, the source will reply sound data assigned to it ad infinitum.
     */
    void SetIsLooping(bool value);
    bool IsLooping();

    /**
     * Connect this source to a given effects slots
     */
    bool ConnectToAnEffectSlot(EffectSlot* effectSlot);

    /**
     * Disconnect this source from a given effects slots
     */
    bool DisconnectFromAnEffectSlot(EffectSlot* effectSlot);

    /**
     * Resets the source state to the state it was in just after its construction
     */
    void ResetToDefaultState();

    /**
     * Change the type of this source
     */
    void SetSourceType(SourceType sourceType);

    /**
     * Get default float parameter values for this source
     */
    static void GetDefaultFloatParamValue(FloatParamValues& outFloatParamValues);

    ///////////////////////////////////////////////
    // Static Source interface
    ///////////////////////////////////////////////
    /**
     * Set internal buffer queue of the source to contain only this one buffer
     */
    void SetBuffer(Buffer& buffer);

    /**
     * Detaches any set buffer
     */
    void ResetBuffer();
    ///////////////////////////////////////////////

    ///////////////////////////////////////////////
    // Streaming Source interface
    ///////////////////////////////////////////////
    /**
     * Added this buffer to the internal buffer queue of the source
     *
     * Note: Since the queue takes ownership of the passed buffer object,
     *       after this operation this buffer object is no longer valid
     *       and can not be used for anything else.
     *
     * @param buffer - Buffer to add
     */
    void QueueBuffer(Buffer& buffer);

    /**
     * Removes an already processed buffer from the internal buffer queue of the source
     */
    Buffer DequeueBuffer();

    /**
     * Clears internal buffer queue and destroys all of the OpenAL buffers which where in it
     */
    void ClearBuffersQueue();

    size_t GetProcessedBufferCount();
    size_t GetQueuedBufferCount();
    ///////////////////////////////////////////////

protected:
    void EffectSlotActivated(EffectSlot* effectSlot);
    void EffectSlotDeactivated(EffectSlot* effectSlot);

    inline Context& _GetContext()
    {
        return m_Context;
    }

    OpenALSourceType _GetCurrentSourceType();

private:
    // Used to prevent copying
    Source& operator = (const Source& other) = delete;
    Source(const Source& other) = delete;

    void _DisconnectAllAuxiliaryEffectsConnectionLines();
    void _DisconnectAuxiliaryEffectsConnectionLine(size_t lineIndex);

    static const ALenum m_FloatParamToOpenALSourceFloatParamMap[e_FloatParamCount];

    unsigned int m_ALSource;
    Context& m_Context;
    SourceType m_CurrentSourceType;
    std::array<EffectSlot*, Context::c_MinimumEffectSlotsConnectionsPerSource> m_EffectSlotsConnectedTo;

    static const FloatParamValues m_DefaultFloatParamValues;
};

}

#endif // AUDIODEVICE_SOURCE_H_INCLUDED
