#ifndef SOUNDMANAGER_H_INCLUDED
#define SOUNDMANAGER_H_INCLUDED

#include <Resources/Audio/AudioData.h>
#include <Resources/Audio/AudioDataManager.h>
#include <AudioDevice/Device.h>
#include <AudioDevice/Context.h>
#include <Containers/List.hpp>
#include <AudioDevice/Source.h>
#include <array>

class SoundManager
{
private:
    typedef List<AudioDevice::Source*> HardwareSoundSourcesList;

public:
    class HardwareSoundSourceHandle
    {
    public:
        friend class SoundManager;

        HardwareSoundSourceHandle()
        : m_HardwareSoundSourcesListNode( nullptr ),
          m_Valid( false )
        {
        }

        HardwareSoundSourceHandle& operator= (const HardwareSoundSourceHandle& other)
        {
            m_HardwareSoundSourcesListNode = other.m_HardwareSoundSourcesListNode;
            m_Valid = other.m_Valid;
            return *this;
        }

        HardwareSoundSourceHandle(const HardwareSoundSourceHandle& other)
        {
            operator= ( other );
        }

        AudioDevice::Source* operator->()
        {
            return m_HardwareSoundSourcesListNode->value;
        }

        const AudioDevice::Source* operator->() const
        {
            return m_HardwareSoundSourcesListNode->value;
        }

        AudioDevice::Source& operator*()
        {
            return *m_HardwareSoundSourcesListNode->value;
        }

        const AudioDevice::Source& operator*() const
        {
            return *m_HardwareSoundSourcesListNode->value;
        }

        bool IsValid() const
        {
            return m_Valid;
        }

    protected:
        void Invalidate()
        {
            m_Valid = false;
        }

        HardwareSoundSourceHandle(HardwareSoundSourcesList::Node* hardwareSoundSourcesListNode)
        : m_HardwareSoundSourcesListNode( hardwareSoundSourcesListNode ),
          m_Valid( true )
        {
        }

        HardwareSoundSourcesList::Node* m_HardwareSoundSourcesListNode;  // Pointer to a node of the audio data list i.e. m_HardwareSoundSourcesList
        bool m_Valid;
    };

    SoundManager();
    ~SoundManager();

    HardwareSoundSourceHandle AllocateHardwareSoundSource();
    void ReleaseHardwareSoundSource(HardwareSoundSourceHandle& handle);

    void ActivateHardwareSoundSource(HardwareSoundSourceHandle& handle);
    bool IsHardwareSoundSourceActive(const HardwareSoundSourceHandle& handle);

    void PlaySource(HardwareSoundSourceHandle& soundSourceHandle, AudioDataManager::AudioDataHandle& audioDataHandle, bool isLooping);
    void PauseSource(HardwareSoundSourceHandle& soundSourceHandle);
    void StopSource(HardwareSoundSourceHandle& soundSourceHandle);

private:
    // Used to prevent copying
    SoundManager& operator = (const SoundManager& other) = delete;
    SoundManager(const SoundManager& other) = delete;

    AudioDevice::Source* _FindAHardwareSourceToUse();

    static const size_t c_MaxNumberOfHardwareSoundSources = 32;

    AudioDevice::Device m_Device;
    AudioDevice::Context m_Context;

    HardwareSoundSourcesList m_HardwareSoundSourcesList;
    std::array<HardwareSoundSourcesList::Node*, c_MaxNumberOfHardwareSoundSources> m_ActiveSoundSources;
    std::array<AudioDevice::Source*, c_MaxNumberOfHardwareSoundSources> m_AvailableSoundSources;
    size_t m_NumberOfAvailableSoundSources;

    static bool m_ClassAlreadyInstantiated;
};

#endif // SOUNDMANAGER_H_INCLUDED
