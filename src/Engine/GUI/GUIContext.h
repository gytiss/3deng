#ifndef GUICONTEXT_H_INCLUDED
#define GUICONTEXT_H_INCLUDED

#include "GUIEventManager.h"
#include "GUIEventInstancer.h"
#include <Rocket/Core.h>

class GUIContext
{
public:
    friend class GUIManager;
    friend class GUIEventManager;
    GUIContext(const std::string& name,
               const Rocket::Core::Vector2i& screenDimensions);

    Rocket::Core::ElementDocument* LoadDocument(const Rocket::Core::String& documentPath);

    inline void Render()
    {
        m_libRocketContext->Render();
    }

    inline void Update()
    {
        m_libRocketContext->Update();
    }

    inline void RegisterEventHandler(const std::string& handlerName,
                                       GUIEventHandler* handler)
    {
        m_EventManager.RegisterEventHandler( handlerName, handler );
    }

protected:
    GUIEventInstancer* GetEventInstancer();

    inline Rocket::Core::Context* GetLibRocketContext()
    {
        return m_libRocketContext;
    }

private:
    GUIEventManager m_EventManager;
    GUIEventInstancer m_EventInstancer;
    Rocket::Core::Context* m_libRocketContext;
};

#endif // GUICONTEXT_H_INCLUDED
