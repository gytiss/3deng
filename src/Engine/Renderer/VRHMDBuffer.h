#ifndef VR_HMD_BUFFER_H_INCLUDED

#include <GraphicsDevice/FBO.h>

class VRBackend;
class FBOColorAttachmentTexture;

class VRHMDBuffer
{
public:
    enum class Eye
    {
        Left,
        Right
    };

    VRHMDBuffer(VRBackend& vrBackend, GraphicsDevice& graphicsDevice, SystemInterface& systemInterface);
    virtual ~VRHMDBuffer();

    /**
     *
     * @param eye - Eye on the HDM this color data is intended for
     * @param fboWithColorData - FBO which the color data will be taken from
     * @param fboColorAttachmentToUse - Index of the FBO color attachment used for color data
     */
    void SubmitTextureToHMD(Eye eye, FBO& fboWithColorData, FBO::ColorAttachmentIdx fboColorAttachmentToUse);

private:
    // Used to prevent copying
    VRHMDBuffer& operator = (const VRHMDBuffer& other) = delete;
    VRHMDBuffer(const VRHMDBuffer& other) = delete;

    VRBackend& m_VRBackend;
    GraphicsDevice& m_GraphicsDevice;
    SystemInterface& m_SystemInterface;
    FBOColorAttachmentTexture* m_HMDLeftEyeTexture;
    FBOColorAttachmentTexture* m_HMDRightEyeTexture;
    FBO m_HMDLeftEyeFBO;
    FBO m_HMDRightEyeFBO;
};

#endif // VR_HMD_BUFFER_H_INCLUDED
