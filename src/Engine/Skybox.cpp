#include "Skybox.h"
#include "MsgLogger.h"
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "StringUtils.h"
#include <CommonDefinitions.h>
#include <iostream>
#include <OpenGLErrorChecking.h>
#include <GraphicsDevice/Texture.h>
#include <GraphicsDevice/GraphicsDevice.h>

#define BUFFER_OFFSET(i) ((int *)NULL + (i)) //BUFFER_OFFSET is used then specifying indices to be used in glDrawElements function

/*
                   Quad 5
                     |
                (1)  |   (5)
                 /--------/|
                /     (4)/ |
            (0) +-------+  |  <----- Quad 3
                |(3) Q2 |  |
 Quad 1 ------> | ------|--/(7)
                |/   Q4 | /
             (2)+-------+/(6)
                    ^
                    |
                    |
                 Quad 6

Quad 1 is on the left.
Quad 2 is at the back.
Quad 3 in on the right.
Quad 4 is in the front.
Quad 5 is on the top.
Quad 6 is on the bottom.


OpenGL UV Mapping(Starts at bottom left):
   x y
  (0,1)
    ^
    |
    |
    |             x y
    |----------> (1,0)
   /
(0,0)
 x y

 U -> x
 V -> y
*/

const Skybox::SkyboxVertex Skybox::c_SkyboxVertices[] =
{
    //
    // All the triangles in this cube are in counterclockwise order
    //

    // Quad 1
    { {-0.5f, -0.5f, 0.5f}, {0.0f, 0.5f} },  // 2
    { {-0.5f, -0.5f, -0.5f}, {0.5f, 0.5f} }, // 3
    { {-0.5f, 0.5f, 0.5f}, {0.0f, 1.0f} },   // 0
    { {-0.5f, 0.5f, 0.5f}, {0.0f, 1.0f} },   // 0
    { {-0.5f, -0.5f, -0.5f}, {0.5f, 0.5f} }, // 3
    { {-0.5f, 0.5f, -0.5f}, {0.5f, 1.0f} },  // 1

    // Quad 2
    { {-0.5f, -0.5f, -0.5f}, {0.5f, 0.5f} }, // 3
    { {0.5f, -0.5f, -0.5f}, {1.0f, 0.5f} },  // 7
    { {-0.5f, 0.5f, -0.5f}, {0.5f, 1.0f} },  // 1
    { {-0.5f, 0.5f, -0.5f}, {0.5f, 1.0f} },  // 1
    { {0.5f, -0.5f, -0.5f}, {1.0f, 0.5f} },  // 7
    { {0.5f, 0.5f, -0.5f}, {1.0f, 1.0f} },   // 5


    // Quad 3
    { {0.5f, -0.5f, -0.5f}, {0.0f, 0.0f} },  // 7
    { {0.5f, -0.5f, 0.5f}, {0.5f, 0.0f} },   // 6
    { {0.5f, 0.5f, -0.5f}, {0.0f, 0.5f} },   // 5
    { {0.5f, 0.5f, -0.5f}, {0.0f, 0.5f} },   // 5
    { {0.5f, -0.5f, 0.5f}, {0.5f, 0.0f} },   // 6
    { {0.5f, 0.5f, 0.5f}, {0.5f,0.5f} },     // 4

    // Quad 4
    { {0.5f, -0.5f, 0.5f}, {0.5f, 0.0f} },   // 6
    { {-0.5f, -0.5f, 0.5f}, {1.0f, 0.0f} },  // 2
    { {0.5f, 0.5f, 0.5f}, {0.5f, 0.5f} },    // 4
    { {0.5f, 0.5f, 0.5f}, {0.5f, 0.5f} },    // 4
    { {-0.5f, -0.5f, 0.5f}, {1.0f, 0.0f} },  // 2
    { {-0.5f, 0.5f, 0.5f}, {1.0f, 0.5f} },   // 0

    // Quad 5
    { {0.5f, 0.5f, 0.5f}, {0.0f, 0.0f} },    // 4
    { {-0.5f, 0.5f, 0.5f}, {0.5f, 0.0f} },   // 0
    { {0.5f, 0.5f, -0.5f}, {0.0f, 1.0f} },   // 5
    { {0.5f, 0.5f, -0.5f}, {0.0f, 1.0f} },   // 5
    { {-0.5f, 0.5f, 0.5f}, {0.5f, 0.0f} },   // 0
    { {-0.5f, 0.5f, -0.5f}, {0.5f, 1.0f} },  // 1

    // Quad 6
    { {-0.5f, -0.5f, 0.5f}, {0.5f, 0.0f} },  // 2
    { {0.5f, -0.5f, 0.5f}, {1.0f, 0.0f} },   // 6
    { {-0.5f, -0.5f, -0.5f}, {0.5f, 1.0f} }, // 3
    { {-0.5f, -0.5f, -0.5f}, {0.5f, 1.0f} }, // 3
    { {0.5f, -0.5f, 0.5f}, {1.0f, 0.0f} },   // 6
    { {0.5f, -0.5f, -0.5f}, {1.0f, 1.0f} },  // 7
};
/*
const Skybox::SkyboxVertex Skybox::c_SkyboxVertices[] =
{
    { {-0.5f, 0.5f, 0.5f}, {0.0f, 1.0f} },   // 0
    { {-0.5f, 0.5f, -0.5f}, {1.0f, 0.0f} },  // 1
    { {-0.5f, -0.5f, 0.5f}, {0.0f, 0.0f} },  // 2
    { {-0.5f, -0.5f, -0.5f}, {1.0f, 0.0f} }, // 3

    { {0.5f, 0.5f, 0.5f}, {0.0f} },    // 4
    { {0.5f, 0.5f, -0.5f}, {0.0f} },   // 5
    { {0.5f, -0.5f, 0.5f}, {0.0f} },   // 6
    { {0.5f, -0.5f, -0.5f}, {0.0f} },  // 7
*/
    /*
    { {-0.5f, 0.5f, 0.5f}, {0.0f, 1.0f} },   // 0
    { {-0.5f, 0.5f, -0.5f}, {0.5f, 1.0f} },  // 1
    { {-0.5f, -0.5f, 0.5f}, {0.0f, 0.625f} },  // 2
    { {-0.5f, -0.5f, -0.5f}, {0.5f, 0.625f} }, // 3

    { {0.5f, 0.5f, 0.5f}, {0.0f} },    // 4
    { {0.5f, 0.5f, -0.5f}, {0.0f} },   // 5
    { {0.5f, -0.5f, 0.5f}, {0.0f} },   // 6
    { {0.5f, -0.5f, -0.5f}, {0.0f} },  // 7
    */
//};

const Skybox::VextexIndex Skybox::c_SkyboxIndices[] =
{
    0u, 1u, 2u,    2u, 1u, 3u, // Quad 1
    4u, 5u, 6u,    6u, 5u, 7u, // Quad 2
    0u, 1u, 4u,    4u, 1u, 5u, // Quad 3
    2u, 3u, 6u,    6u, 3u, 7u, // Quad 4
    0u, 4u, 2u,    2u, 4u, 6u, // Quad 5
    3u, 1u, 5u,    3u, 5u, 7u  // Quad 6

};

const size_t Skybox::c_NumbexOfSkyboxVertices = ArraySize(c_SkyboxVertices);
const size_t Skybox::c_NumberOfSkyboxIndices = ArraySize(c_SkyboxIndices);
const size_t Skybox::c_NumberOfTrianglesInSkybox = (c_NumberOfSkyboxIndices / 3);




Skybox::Skybox(GraphicsDevice& graphicsDevice,
               TextureManager& textureManager,
               FileManager& fileManager,
               const std::string& texture1FileName,
               const std::string& texture2FileName)
: DrawableEntity( graphicsDevice, fileManager, false, false ),
  m_GraphicsDevice( graphicsDevice ),
  m_TextureManager( textureManager ),
  m_CustomTextureSet( false ),
  m_RenderFromDefaultTexture( true )

{
    _LoadTextures( texture1FileName, texture2FileName );
    _UploadIndexAndVertexDataToGPU();
}

Skybox::~Skybox()
{
    m_TextureManager.ReleaseTexture( m_Texture1 );
    m_TextureManager.ReleaseTexture( m_Texture2 );
}

void Skybox::render(ShaderProg* shaderProgToUse_1, ShaderProg* UNUSED_PARAM(shaderProgToUse_2), const DrawableEntity::Transformation * t )
{
    m_GraphicsDevice.SaveCurrentPolygonMode();

    glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), m_ObjLocation);
    modelMatrix = glm::rotate(modelMatrix, m_RotationVector.x, glm::vec3(1.0f, 0.0f, 0.0f));
    modelMatrix = glm::rotate(modelMatrix, m_RotationVector.y, glm::vec3(0.0f, 1.0f, 0.0f));
    modelMatrix = glm::rotate(modelMatrix,m_RotationVector.z, glm::vec3(0.0f, 0.0f, 1.0f));
    modelMatrix = glm::scale(modelMatrix,m_Scale);

    glm::mat4 modelViewMatrix = t->viewMatrix * modelMatrix;

    glm::mat3 normalMatrix=glm::transpose(glm::inverse(glm::mat3(t->viewMatrix)));


    glBindVertexArray(m_VAO);
    CheckForGLErrors();
    //glBindBuffer( GL_ARRAY_BUFFER, m_VBO );
    //CheckForGLErrors();
    //        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
    //        CheckForGLErrors();


    //if(m_UseShaders)
    {
        shaderProgToUse_1->Use();
        CheckForGLErrors();
    }

    glUniformMatrix4fv(glGetUniformLocation(shaderProgToUse_1->GetHandle(), "mvmatrix"), 1, GL_FALSE, glm::value_ptr(modelViewMatrix));
    glUniformMatrix4fv(glGetUniformLocation(shaderProgToUse_1->GetHandle(), "pmatrix"), 1, GL_FALSE, glm::value_ptr(t->projectionMatrix));
    glUniformMatrix3fv(glGetUniformLocation(shaderProgToUse_1->GetHandle(), "nmatrix"), 1, GL_FALSE, glm::value_ptr(normalMatrix));
    CheckForGLErrors();

    if(m_RenderingMode == DrawableEntity::RENDER_IN_WIREFRAME)
    {
        m_GraphicsDevice.SetPolygonMode( GraphicsDevice::e_PolygonModeLine );
    }

    glUniform1i(glGetUniformLocation(shaderProgToUse_1->GetHandle(), "tex2D"), Texture::e_TypeIdxDiffuseMap );
    CheckForGLErrors();

    m_Texture1->Activate();

    /*
    const uint32_t c_StartOffsetOfQuadsWithFirstTexture = sizeof(GLuint)*0;
    glDrawElements(GL_TRIANGLES, (c_NumberOfSkyboxIndices/6)*5, GL_UNSIGNED_INT, (GLvoid*)c_StartOffsetOfQuadsWithFirstTexture);
    //glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(sizeof(GLuint) * 36));
    */
    glDrawArrays(GL_TRIANGLES,0,((c_NumbexOfSkyboxVertices/6)*4));
    CheckForGLErrors();

    m_Texture2->Activate();

    glDrawArrays(GL_TRIANGLES,((c_NumbexOfSkyboxVertices/6)*4),(c_NumbexOfSkyboxVertices/6)*2);
    CheckForGLErrors();

    /*
    m_Texture2->Activate();

    const uint32_t c_StartOffsetOfQuadsWithSecondTexture = (sizeof(GLuint) * (c_NumberOfSkyboxIndices/6) * 5);
    glDrawElements(GL_TRIANGLES, (c_NumberOfSkyboxIndices/6), GL_UNSIGNED_INT, (GLvoid*)c_StartOffsetOfQuadsWithSecondTexture);
    CheckForGLErrors();
    */

    m_GraphicsDevice.RestorePreviousPolygonMode();
}

void Skybox::_LoadTextures(const std::string& textureOnePath, const std::string& textureTwoPath)
{
    if( m_ClampTexturesToEdge )
    {
        m_Texture1 = m_TextureManager.Load2DTexture( textureOnePath,
                                                     Texture::e_TypeIdxDiffuseMap,
                                                     Texture::e_StateParamArgSCoordWrapClampToEdge,
                                                     Texture::e_StateParamArgTCoordWrapClampToEdge );

        m_Texture2 = m_TextureManager.Load2DTexture( textureTwoPath,
                                                     Texture::e_TypeIdxDiffuseMap,
                                                     Texture::e_StateParamArgSCoordWrapClampToEdge,
                                                     Texture::e_StateParamArgTCoordWrapClampToEdge );
    }
    else
    {
        m_Texture1 = m_TextureManager.Load2DTexture( textureOnePath,
                                                     Texture::e_TypeIdxDiffuseMap,
                                                     Texture::e_StateParamArgSCoordWrapRepeat,
                                                     Texture::e_StateParamArgTCoordWrapRepeat );

        m_Texture2 = m_TextureManager.Load2DTexture( textureTwoPath,
                                                     Texture::e_TypeIdxDiffuseMap,
                                                     Texture::e_StateParamArgSCoordWrapRepeat,
                                                     Texture::e_StateParamArgTCoordWrapRepeat );
    }
}

void Skybox::_UploadIndexAndVertexDataToGPU()
{
    glBindVertexArray( m_VAO );
    CheckForGLErrors();

    glBindBuffer( GL_ARRAY_BUFFER, m_VBO );
    CheckForGLErrors();
    glBufferData ( GL_ARRAY_BUFFER, (c_NumbexOfSkyboxVertices * sizeof ( SkyboxVertex )), c_SkyboxVertices, GL_STATIC_DRAW );
    CheckForGLErrors();
    glVertexAttribPointer ( 0 /*in_Position*/, 3, GL_FLOAT, GL_FALSE,  sizeof ( SkyboxVertex), ( const GLvoid* ) offsetof (SkyboxVertex,position) );
    CheckForGLErrors();
    glEnableVertexAttribArray( 0 /*in_Position*/ );
    CheckForGLErrors();
    glVertexAttribPointer ( 2 /*in_uvCoord*/, 2, GL_FLOAT, GL_FALSE, sizeof ( SkyboxVertex ), ( const GLvoid* ) offsetof(SkyboxVertex,texCoord) );
    CheckForGLErrors();
    glEnableVertexAttribArray( 2 /*in_uvCoord*/ );
    CheckForGLErrors();
    /*
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_IBO );
    CheckForGLErrors();
    glBufferData ( GL_ELEMENT_ARRAY_BUFFER, (c_NumberOfSkyboxIndices * sizeof(VextexIndex)), c_SkyboxIndices, GL_STATIC_DRAW );
    CheckForGLErrors();
    */

    glBindVertexArray( 0 );
    CheckForGLErrors();
}




