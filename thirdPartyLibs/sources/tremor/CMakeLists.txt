project(tremor)
cmake_minimum_required (VERSION 2.8.8)

if ("${LIB_OGG_INCLUDES_DIR}" STREQUAL "")
    message(FATAL_ERROR "Variable LIB_OGG_INCLUDES_DIR was not provided to the script. Please call the script again with the missing variable added before continuing")
endif()

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

MACRO(ADD_TARGET_SOURCE targetSource)
    set (TARGERT_SOURCES ${TARGERT_SOURCES} "${PROJECT_SOURCE_DIR}/${targetSource}")
ENDMACRO(ADD_TARGET_SOURCE)

MACRO(ADD_HEADERS_TO_INSTALL headerFile)
    set (HEADERS_TO_INSTALL ${HEADERS_TO_INSTALL} "${PROJECT_SOURCE_DIR}/${headerFile}")
ENDMACRO(ADD_HEADERS_TO_INSTALL)

ADD_TARGET_SOURCE("block.c")
ADD_TARGET_SOURCE("codebook.c")
ADD_TARGET_SOURCE("floor0.c")
ADD_TARGET_SOURCE("floor1.c")
ADD_TARGET_SOURCE("info.c")
ADD_TARGET_SOURCE("iseeking_example.c")
ADD_TARGET_SOURCE("ivorbisfile_example.c")
ADD_TARGET_SOURCE("mapping0.c")
ADD_TARGET_SOURCE("mdct.c")
ADD_TARGET_SOURCE("registry.c")
ADD_TARGET_SOURCE("res012.c")
ADD_TARGET_SOURCE("sharedbook.c")
ADD_TARGET_SOURCE("synthesis.c")
ADD_TARGET_SOURCE("vorbisfile.c")
ADD_TARGET_SOURCE("window.c")

ADD_HEADERS_TO_INSTALL("config_types.h")
ADD_HEADERS_TO_INSTALL("ivorbiscodec.h")
ADD_HEADERS_TO_INSTALL("ivorbisfile.h")

include_directories ("${PROJECT_SOURCE_DIR}")

add_library(Tremor_static STATIC ${TARGERT_SOURCES} )

set_target_properties(Tremor_static PROPERTIES OUTPUT_NAME "vorbisidec")
target_include_directories(Tremor_static PUBLIC ${LIB_OGG_INCLUDES_DIR})

install(
    TARGETS Tremor_static
    RUNTIME DESTINATION bin
    LIBRARY DESTINATION lib
    ARCHIVE DESTINATION lib
)

install(
    FILES ${HEADERS_TO_INSTALL}
    DESTINATION "include/tremor"
)

