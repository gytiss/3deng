@ECHO OFF 

set "INITIAL_DIR=%CD%"
set "SCRIPT_DIR=%~dp0"


set "PATH=%SCRIPT_DIR%\thirdPartyLibs\windows\libRocket\bin;%SCRIPT_DIR%\thirdPartyLibs\windows\libfreetype\bin;%SCRIPT_DIR%\thirdPartyLibs\windows\libSDL2\bin\;%SCRIPT_DIR%\thirdPartyLibs\windows\libGLEW\bin;%SCRIPT_DIR%\thirdPartyLibs\windows\libOpenAL\bin;%SCRIPT_DIR%\thirdPartyLibs\windows\libOpenVR\bin;%PATH%"

if "%1"=="Debug" (
    echo Running debug build
    "%SCRIPT_DIR%\winBuild\bin\Debug\MainApp.exe"
) else (
    echo Running release build
    "%SCRIPT_DIR%\winBuild\bin\Release\MainApp.exe"
)

cd %INITIAL_DIR%
pause