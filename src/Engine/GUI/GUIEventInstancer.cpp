#include "GUIEventInstancer.h"
#include "GUIEvent.h"
#include <CommonDefinitions.h>

//-----------------------------------------------------------------------

GUIEventInstancer::GUIEventInstancer(GUIEventManager* eventManager)
: m_EventManager( eventManager )
{
}

//-----------------------------------------------------------------------

GUIEventInstancer::~GUIEventInstancer()
{
    m_EventManager = nullptr;
}

//-----------------------------------------------------------------------

Rocket::Core::EventListener* GUIEventInstancer::InstanceEventListener(const Rocket::Core::String& value,
                                                                      Rocket::Core::Element* UNUSED_PARAM(element))
{
	return new GUIEvent( value, m_EventManager );
}

//-----------------------------------------------------------------------

void GUIEventInstancer::Release()
{
    // Does nothing
}

//-----------------------------------------------------------------------
