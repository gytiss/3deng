#include "GUIRenderInterface.h"
#include <Rocket/Core.h>
#include <CommonDefinitions.h>
#include <imgutl/ImageUtils.h>
#include <MathUtils.h>
#include <OpenGLIncludes.h>
#include <GraphicsDevice/VBO.h>
#include <GraphicsDevice/IBO.h>
#include <GraphicsDevice/Texture.h>
#include <GraphicsDevice/GraphicsDevice.h>
#include <Renderer/ShaderPrograms.h>

//-----------------------------------------------------------------------

GUIRenderInterface::GUIRenderInterface(const std::string& guiDataRootDirectoryPath,
                                       GraphicsDevice& graphicsDevice,
                                       FileManager& fileManager,
                                       size_t windowWidth,
                                       size_t windowHeight)
: m_GraphicsDevice( graphicsDevice ),
  m_FileManager( fileManager ),
  m_Width( windowWidth ),
  m_Height( windowHeight ),
  m_VAO(),
  m_VBO( nullptr ),
  m_IBO( nullptr ),
  m_IndexList(),
  m_VertexList(),
  m_DrawCallList(),
  m_GLScissorEnabled( false ),
  m_ScissorRect(),
  m_DataRootDirPath( guiDataRootDirectoryPath )
{
    m_ScissorRect.x = 0;
    m_ScissorRect.y = 0;
    m_ScissorRect.width = static_cast<int>( windowWidth );
    m_ScissorRect.height = static_cast<int>( windowHeight );

    m_VBO = new VBO( sizeof( GUIDataVertex ), VBO::e_DataBufferUsagePatternDynamic );

    m_VBO->AddVertexAttribute(VBO::e_VertexAttrIdxPosition,
                              VBO::e_VertexAttrComponentTypeFloat32,
                              VBO::e_VertexAttrTypeVec3,
                              offsetof( GUIDataVertex, position ) );

    m_VBO->AddVertexAttribute(VBO::e_VertexAttrIdxColor,
                              VBO::e_VertexAttrComponentTypeFloat32,
                              VBO::e_VertexAttrTypeVec4,
                              offsetof( GUIDataVertex, color ) );

    m_VBO->AddVertexAttribute(VBO::e_VertexAttrIdxTextureCoords,
                              VBO::e_VertexAttrComponentTypeFloat32,
                              VBO::e_VertexAttrTypeVec2,
                              offsetof( GUIDataVertex, texCoord ) );

    m_IBO = new IBO( IBO::e_DataBufferUsagePatternDynamic );

    m_VAO.AddVBO( m_VBO );
    m_VAO.SetIBO( m_IBO );
}

//-----------------------------------------------------------------------

void GUIRenderInterface::SetViewport(int width, int height)
{
    m_Width = static_cast<size_t>( width );
    m_Height = static_cast<size_t>( height );
}

//-----------------------------------------------------------------------

void GUIRenderInterface::Render(ShaderProgGUI& shader)
{
    if( ( m_IndexList.size() > 0 ) && ( m_VertexList.size() > 0 ) )
    {
        m_IBO->BindData( m_IndexList.size() * sizeof( m_IndexList[0] ), &(m_IndexList[0]) ); // Bind data before binding VAO to prevent dissociating IBO from VAO
        m_VBO->BindData( m_VertexList.size() * sizeof( m_VertexList[0] ), &(m_VertexList[0]) );

        shader.Use();
        m_VAO.Bind();

        m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionBlending );
        m_GraphicsDevice.OptionEnable( GraphicsDevice::e_OptionBlending );

        m_GraphicsDevice.SaveCurrentBlendFunctionState();
        m_GraphicsDevice.SetBlendFunctionState( GraphicsDevice::e_BlendFunctionSrcAlpha,
                                                GraphicsDevice::e_BlendFunctionOneMinusSrcAlpha );

        m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionDepthTesting );
        m_GraphicsDevice.OptionDisable( GraphicsDevice::e_OptionDepthTesting ); // Need to disable depth testing, because we want to blend GUI elements not make them hide behind one another

        m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionScrissorTesting );

        m_GraphicsDevice.SaveCurrentScissorBox();

        for(size_t i = 0; i < m_DrawCallList.size(); i++)
        {
            const GUIDrawCall& drawCall = m_DrawCallList[i];
            shader.SetUniformModelMatrix( drawCall.modelMatrix );

            if( drawCall.texture == nullptr )
            {
                shader.SetUniformUseTexture( 0 );
            }
            else
            {
                shader.SetUniformUseTexture( 1 );
                drawCall.texture->Activate();
            }

            if( drawCall.vertexIndexCount > 0 )
            {
                if( drawCall.scissorEnabled )
                {
                    m_GraphicsDevice.OptionEnable( GraphicsDevice::e_OptionScrissorTesting );

                    const GLScissorRect& rect = drawCall.scissorRect;
                    m_GraphicsDevice.SetScissorBox( rect.x, rect.y, rect.width, rect.height );
                }
                else
                {
                    m_GraphicsDevice.OptionDisable( GraphicsDevice::e_OptionScrissorTesting );
                }

                const uintptr_t c_MeshIndexStartOffset = ( sizeof( EngineTypes::VertexIndexType ) * drawCall.firstVertexIndex );
                glDrawElements( GL_TRIANGLES, static_cast<GLsizei>( drawCall.vertexIndexCount ), IBO::GetVertexIndexOpenGLType(), ( GLvoid* )c_MeshIndexStartOffset );
                CheckForGLErrors();
            }
        }

        m_GraphicsDevice.RestorePreviousScissorBox();
        m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionScrissorTesting );
        m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionDepthTesting );
        m_GraphicsDevice.RestorePreviousBlendFunctionState();
        m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionBlending );

        m_VAO.Unbind();
    }

    m_IndexList.clear();
    m_VertexList.clear();
    m_DrawCallList.clear();
}

//-----------------------------------------------------------------------

// Called by Rocket when it wants to render geometry that it does not wish to optimise.
void GUIRenderInterface::RenderGeometry(Rocket::Core::Vertex* vertices,
                                        int num_vertices,
                                        int* indices,
                                        int num_indices,
                                        const Rocket::Core::TextureHandle texture,
                                        const Rocket::Core::Vector2f& translation)
{
    GUIDrawCall drawCall;

    drawCall.modelMatrix = glm::translate( MathUtils::c_IdentityMatrix,
                                           glm::vec3( translation.x,
                                                      translation.y,
                                                      0.0f ) );

    glm::mat4 projMat = glm::ortho( 0.0f,
                                    static_cast<float>( m_Width ),
                                    static_cast<float>( m_Height ),
                                    0.0f,
                                    -1.0f,
                                    1.0f );

    drawCall.modelMatrix = projMat * drawCall.modelMatrix;

    drawCall.firstVertexIndex = m_IndexList.size();
    drawCall.vertexIndexCount = num_indices;
    drawCall.texture = reinterpret_cast<Texture*>( texture );

    // These indices make up triangles which are in a counter counter clockwise order
    for(int i = 0; i < num_indices; i++)
    {
        const size_t indexValue = m_VertexList.size() + indices[i];

        assert( MathUtils::CanBeStoredInType<EngineTypes::VertexIndexType>( indexValue ) && "Index is out of supported range" );

        m_IndexList.push_back( static_cast<unsigned int>( indexValue ) );
    }

    for(int i = 0; i < num_vertices; i++)
    {
        GUIDataVertex v;
        v.position[0] = (vertices[i].position[0]);
        v.position[1] = (vertices[i].position[1]);
        v.position[2] = 0.0f;

        v.color[0] = (static_cast<float>(vertices[i].colour.red) / 255.0f);
        v.color[1] = (static_cast<float>(vertices[i].colour.green) / 255.0f);
        v.color[2] = (static_cast<float>(vertices[i].colour.blue) / 255.0f);
        v.color[3] = (static_cast<float>(vertices[i].colour.alpha) / 255.0f);
        v.texCoord[0] = (vertices[i].tex_coord[0]);
        v.texCoord[1] = (vertices[i].tex_coord[1]);
        m_VertexList.push_back( v );
    }

    drawCall.scissorEnabled = m_GLScissorEnabled;
    drawCall.scissorRect = m_ScissorRect;

    m_DrawCallList.push_back( drawCall );
}

//-----------------------------------------------------------------------

// Called by Rocket when it wants to compile geometry it believes will be static for the forseeable future.
Rocket::Core::CompiledGeometryHandle GUIRenderInterface::CompileGeometry(Rocket::Core::Vertex* ROCKET_UNUSED_PARAMETER(vertices), int ROCKET_UNUSED_PARAMETER(num_vertices), int* ROCKET_UNUSED_PARAMETER(indices), int ROCKET_UNUSED_PARAMETER(num_indices), const Rocket::Core::TextureHandle ROCKET_UNUSED_PARAMETER(texture))
{
	return static_cast<Rocket::Core::CompiledGeometryHandle>( 0 );
}

// Called by Rocket when it wants to render application-compiled geometry.
void GUIRenderInterface::RenderCompiledGeometry(Rocket::Core::CompiledGeometryHandle ROCKET_UNUSED_PARAMETER(geometry), const Rocket::Core::Vector2f& ROCKET_UNUSED_PARAMETER(translation))
{

}

//-----------------------------------------------------------------------

// Called by Rocket when it wants to release application-compiled geometry.
void GUIRenderInterface::ReleaseCompiledGeometry(Rocket::Core::CompiledGeometryHandle ROCKET_UNUSED_PARAMETER(geometry))
{
}

//-----------------------------------------------------------------------

// Called by Rocket when it wants to enable or disable scissoring to clip content.
void GUIRenderInterface::EnableScissorRegion(bool enable)
{
    m_GLScissorEnabled = enable;
}

//-----------------------------------------------------------------------

// Called by Rocket when it wants to change the scissor region.
void GUIRenderInterface::SetScissorRegion(int x, int y, int width, int height)
{
    m_ScissorRect.x = x;
    m_ScissorRect.y = (static_cast<int>( m_Height ) - ( y + height ) );
    m_ScissorRect.width = width;
    m_ScissorRect.height = height;
}

//-----------------------------------------------------------------------

// Called by Rocket when a texture is required by the library.
bool GUIRenderInterface::LoadTexture(Rocket::Core::TextureHandle& texture_handle, Rocket::Core::Vector2i& texture_dimensions, const Rocket::Core::String& source)
{
	bool retVal = false;

    // Try to load as an absolute path
	ImageUtils::ImageData_t* imageData = ImageUtils::LoadImage( m_FileManager, ImageUtils::TGA, source.CString(), ImageUtils::UPPER_LEFT );

    // If loading as an absolute path fails then try to load as a relative path to the root GUI data directory
    if( !imageData )
    {
        std::string absolutePath = m_DataRootDirPath;
        absolutePath += "/";
        absolutePath += source.CString();

        imageData = ImageUtils::LoadImage( m_FileManager, ImageUtils::TGA, absolutePath.c_str(), ImageUtils::UPPER_LEFT );
    }

	if( imageData )
    {
        if( imageData->format == ImageUtils::RGBA )
        {
            texture_dimensions.x = static_cast<int>( imageData->w );
            texture_dimensions.y = static_cast<int>( imageData->h );
            retVal = GenerateTexture( texture_handle, imageData->data, texture_dimensions );
        }
        else if( imageData->format == ImageUtils::RGB )
        {
            ImageUtils::ConvertRGBImageToRGBAImage( imageData );

            texture_dimensions.x = static_cast<int>( imageData->w );
            texture_dimensions.y = static_cast<int>( imageData->h );
            retVal = GenerateTexture( texture_handle, imageData->data, texture_dimensions );
        }
        else
        {
            std::string msg = "Can not use \"";
            msg += std::string( source.CString() );
            msg += "\", because it is neither RGB nor RGBA image";
            Rocket::Core::Log::Message( Rocket::Core::Log::LT_ERROR, msg.c_str() );
            retVal = false;
        }

        ImageUtils::DeleteImage( imageData );
    }
    else
    {
            std::string msg = "Failed to open image file \"";
            msg += std::string( source.CString() );
            msg += "\"";
            Rocket::Core::Log::Message( Rocket::Core::Log::LT_ERROR, msg.c_str() );
            retVal = false;
    }

	return retVal;
}

//-----------------------------------------------------------------------

// Called by Rocket when a texture is required to be built from an internally-generated sequence of pixels.
bool GUIRenderInterface::GenerateTexture(Rocket::Core::TextureHandle& texture_handle,
                                         const Rocket::Core::byte* source,
                                         const Rocket::Core::Vector2i& source_dimensions)
{
    static_assert( sizeof( Rocket::Core::TextureHandle ) >= sizeof(void*), "Rocket::Core::TextureHandle should be large enough to fit a pointer in it" );

    Texture* texture = new Texture( m_GraphicsDevice, Texture::e_TypeIdxDiffuseMap, Texture::e_Class2DTexture, false );

    const size_t imageBitsPerPixel = 32;
    ImageUtils::ImageData_t image;

    image.depth = imageBitsPerPixel;
    image.bytesPerPixel = ( imageBitsPerPixel / 8 );
    image.format = ImageUtils::RGBA;
    image.w = source_dimensions.x;
    image.h = source_dimensions.y;
    image.data = const_cast<uint8_t*>( source );

    texture->SetData( Texture::e_DataClass2DTexture, image, false );

	texture->SetFilteringMode( Texture::e_FilteringModeNoFiltering );
    texture->SetStateParameter( Texture::e_StateParamSCoordWrapType, Texture::e_StateParamArgSCoordWrapClampToEdge );
    texture->SetStateParameter( Texture::e_StateParamTCoordWrapType, Texture::e_StateParamArgTCoordWrapClampToEdge );

	texture_handle = reinterpret_cast<Rocket::Core::TextureHandle>( texture );

	return true;
}

//-----------------------------------------------------------------------

// Called by Rocket when a loaded texture is no longer required.
void GUIRenderInterface::ReleaseTexture(Rocket::Core::TextureHandle texture_handle)
{
    Texture* texture = reinterpret_cast<Texture*>( texture_handle );
	delete texture;
}

//-----------------------------------------------------------------------
