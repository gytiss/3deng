#include "MeshManager.h"

#include <Exceptions/GenericException.h>
#include <GraphicsDevice/VAO.h>
#include <GraphicsDevice/VBO.h>
#include <GraphicsDevice/IBO.h>
#include <MObjModel/MObj.h>
#include <StringUtils.h>
#include <boost/filesystem.hpp>
#include <boost/locale.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <cassert>
#include <memory>

//-----------------------------------------------------------------------

const char* MeshManager::c_InternalMeshPathPrefix = "\\internalMesh/:";

//-----------------------------------------------------------------------

bool MeshManager::m_ClassAlreadyInstantiated = false;

//-----------------------------------------------------------------------

const char* MeshManager::m_InternalMeshNames[e_InternalMeshCount] =
{
    "Cone",
    "Sphere",
    "Cylinder",
    "Cube",
    "Plane"
};

//-----------------------------------------------------------------------

MeshManager::MeshManager(FileManager& fileManager)
: m_FileManager( fileManager ),
  m_MeshList(),
  m_UniqueMeshIDsToMeshesMap()
{
    if( m_ClassAlreadyInstantiated )
    {
        throw GenericException( "There can only be one instance of MeshManager()" );
    }
    else
    {
        m_ClassAlreadyInstantiated = true;
    }
}

//-----------------------------------------------------------------------

MeshManager::~MeshManager()
{
    MeshList::iterator it = m_MeshList.begin();
    while( it != m_MeshList.end() )
    {
        RefCountedMeshPtr& meshPtr = *it;
        delete meshPtr.mesh;

        // Might not be present if no one has requested to store mesh in the CPU readable memory or if it is an internal mesh
        if( meshPtr.meshDataInCPUReadableMemory )
        {
            delete meshPtr.meshDataInCPUReadableMemory;
        }

        it = m_MeshList.erase( it );
    }
}

//-----------------------------------------------------------------------

MeshManager::MeshHandle MeshManager::GetInternalMeshHandle(InternalMeshType type)
{
    MeshList::iterator meshIt;

    std::string meshName = c_InternalMeshPathPrefix;
    meshName =+ m_InternalMeshNames[type];

    UniqueMeshIDsToMeshesMap::iterator it = m_UniqueMeshIDsToMeshesMap.find( meshName );
    if( it != m_UniqueMeshIDsToMeshesMap.end() )
    {
        meshIt = it->second;
        meshIt->refCount = ( meshIt->refCount + 1 );
    }
    else
    {
        meshIt = _LoadInternalMesh( type );
    }

    return MeshHandle( meshIt );
}

//-----------------------------------------------------------------------

void MeshManager::LoadModel(const std::string& modelFilePath,
                            ModelMeshes& outModelMeshes)
{
    MeshList::iterator meshIt;

    UniqueMeshIDsToMeshesMap::iterator it = m_UniqueMeshIDsToMeshesMap.find( modelFilePath );
    if( it != m_UniqueMeshIDsToMeshesMap.end() )
    {
        meshIt = it->second;
        meshIt->refCount = ( meshIt->refCount + 1 );

        outModelMeshes.clear();
        outModelMeshes.push_back( MeshHandle( meshIt ) );
    }
    else
    {
        _LoadModelFromFile( modelFilePath, outModelMeshes );
    }
}

//-----------------------------------------------------------------------

void MeshManager::ReleaseMesh(MeshHandle& handle)
{
    assert( handle.IsValid() && "Trying to release an invalid handle" );

    if( handle.IsValid() )
    {
        RefCountedMeshPtr& meshPtr = *handle.m_MeshListIt;
        meshPtr.refCount = ( meshPtr.refCount - 1 );

        if( meshPtr.refCount == 0 )
        {
            delete meshPtr.mesh;

            // Might not be present if no one has requested to store mesh in the CPU readable memory or if it is an internal mesh
            if( meshPtr.meshDataInCPUReadableMemory )
            {
                delete meshPtr.meshDataInCPUReadableMemory;
            }

            m_UniqueMeshIDsToMeshesMap.erase( meshPtr.modelFilePath );
            m_MeshList.erase( handle.m_MeshListIt );
            handle.Invalidate();
        }
    }
}

//-----------------------------------------------------------------------

void MeshManager::LoadMeshDataIntoCPUReadableMemory(MeshHandle& handle)
{
    if( handle.IsValid() &&
        !handle->HasMeshDataOnCPUReadableMemory() &&
        !handle.m_MeshListIt->isInternalMesh )
    {
        std::unique_ptr<MObj> mobjMesh( new MObj( m_FileManager ) );

        if( !mobjMesh->LoadFromFile(  handle.m_MeshListIt->modelFilePath ) )
        {
            throw GenericException( "LoadMeshDataIntoCPUReadaleMemory() failed to load Model \"" + handle.m_MeshListIt->modelFilePath + "\"" );
        }
        else if( handle.m_MeshListIt->meshIndexWithinTheModel >= mobjMesh->GetMeshCount() )
        {
            std::string msg = "LoadMeshDataIntoCPUReadaleMemory() mesh handle has a mesh index '";
            msg += StringUitls::NumToString( handle.m_MeshListIt->meshIndexWithinTheModel );
            msg += "' which is not valid for the mode '";
            msg += handle.m_MeshListIt->modelFilePath;
            msg += "'";

            throw GenericException( msg );
        }

        handle.m_MeshListIt->meshDataInCPUReadableMemory = mobjMesh->RelinquishMesh( handle.m_MeshListIt->meshIndexWithinTheModel );

        MObj::Mesh* meshData = handle.m_MeshListIt->meshDataInCPUReadableMemory;

        if( meshData->type == MObj::Mesh::e_MeshTypeSkinned )
        {
            handle->_SetMeshDataOnCPUReadableMemory( &(meshData->indices[0]),
                                                     glm::value_ptr( meshData->skinnedVertices[0].m_Pos ),
                                                     sizeof( MObj::SkinnedVertex ) );
        }
        else
        {
            handle->_SetMeshDataOnCPUReadableMemory( &(meshData->indices[0]),
                                                     glm::value_ptr( meshData->staticVertices[0].m_Pos ),
                                                     sizeof( MObj::StaticVertex ) );
        }
    }
}

//-----------------------------------------------------------------------

MeshManager::MeshList::iterator MeshManager::_LoadInternalMesh(InternalMeshType type)
{
    MeshList::iterator retVal = m_MeshList.end();

    const EngineTypes::VertexIndexType* indexBuffer = nullptr;
    size_t indexCount = 0;
    const InternalMeshData::MeshVertex* vertexBuffer = nullptr;
    size_t vertexCount = 0;
    EngineTypes::VertexIndexType minVertexIndexValue = 0;
    EngineTypes::VertexIndexType maxVertexIndexValue = 0;
    EngineTypes::AABB boundingBox;
    boundingBox.center = glm::vec3( 0.0f, 0.0f, 0.0f );

    if( type == e_InternalMeshTypeCone )
    {
        indexBuffer = InternalMeshData::m_ConeIndexBuffer;
        indexCount = InternalMeshData::m_ConeIndexCount;
        vertexBuffer = InternalMeshData::m_ConeVertexBuffer;
        vertexCount = InternalMeshData::m_ConeVertexCount;
        minVertexIndexValue = InternalMeshData::m_ConeMinVertexIndexValue;
        maxVertexIndexValue = InternalMeshData::m_ConeMaxVertexIndexValue;
        boundingBox.halfExtents = glm::vec3( 1.0f, 1.0f, 1.0f );
    }
    else if( type == e_InternalMeshTypeSphere )
    {
        indexBuffer = InternalMeshData::m_SphereIndexBuffer;
        indexCount = InternalMeshData::m_SphereIndexCount;
        vertexBuffer = InternalMeshData::m_SphereVertexBuffer;
        vertexCount = InternalMeshData::m_SphereVertexCount;
        minVertexIndexValue = InternalMeshData::m_SphereMinVertexIndexValue;
        maxVertexIndexValue = InternalMeshData::m_SphereMaxVertexIndexValue;
        boundingBox.halfExtents = glm::vec3( 1.0f, 1.0f, 1.0f );
    }
    else if( type == e_InternalMeshTypeCylinder )
    {
        indexBuffer = InternalMeshData::m_CylinderIndexBuffer;
        indexCount = InternalMeshData::m_CylinderIndexCount;
        vertexBuffer = InternalMeshData::m_CylinderVertexBuffer;
        vertexCount = InternalMeshData::m_CylinderVertexCount;
        minVertexIndexValue = InternalMeshData::m_CylinderMinVertexIndexValue;
        maxVertexIndexValue = InternalMeshData::m_CylinderMaxVertexIndexValue;
        boundingBox.halfExtents = glm::vec3( 1.0f, 1.0f, 1.0f );
    }
    else if( type == e_InternalMeshTypePlane )
    {
        indexBuffer = InternalMeshData::m_PlaneIndexBuffer;
        indexCount = InternalMeshData::m_PlaneIndexCount;
        vertexBuffer = InternalMeshData::m_PlaneVertexBuffer;
        vertexCount = InternalMeshData::m_PlaneVertexCount;
        minVertexIndexValue = InternalMeshData::m_PlaneMinVertexIndexValue;
        maxVertexIndexValue = InternalMeshData::m_PlaneMaxVertexIndexValue;
        boundingBox.halfExtents = glm::vec3( 1.0f, 0.01f, 1.0f );
    }
    else
    {
        throw GenericException( "Tried to load internal mesh. Loading of this internal mesh type is not yet implemented" );
    }

    std::string uniqueMeshID = c_InternalMeshPathPrefix;
    uniqueMeshID += m_InternalMeshNames[type];

    Mesh::SubMeshList subMeshes;
    subMeshes.push_back( Mesh::SubMesh() );
    Mesh::SubMesh& subMesh = subMeshes[subMeshes.size() - 1];
    subMesh.m_FirstIndex = 0;
    subMesh.m_IndexCount = indexCount;
    subMesh.m_MinVertexIndexValue = minVertexIndexValue;
    subMesh.m_MaxVertexIndexValue = maxVertexIndexValue;
    subMesh.m_InitialMaterialPath = "";

    m_MeshList.push_front( RefCountedMeshPtr() );
    retVal = m_MeshList.begin();

    m_UniqueMeshIDsToMeshesMap.insert( UniqueMeshIDsToMeshesMap::value_type( uniqueMeshID, retVal ) );

    retVal->refCount = 1;
    retVal->uniqueMeshID = uniqueMeshID;
    retVal->isInternalMesh = true;
    retVal->internalMeshType = type;
    retVal->mesh = new Mesh( m_InternalMeshNames[type],
                             _UploadInternalMeshToGPU( indexBuffer, indexCount, vertexBuffer, vertexCount ),
                             indexCount,
                             vertexCount,
                             boundingBox,
                             subMeshes );

    retVal->mesh->_SetMeshDataOnCPUReadableMemory( indexBuffer,
                                                   vertexBuffer[0].position,
                                                   sizeof( InternalMeshData::MeshVertex ) );

    return retVal;
}

//-----------------------------------------------------------------------

void MeshManager::_LoadModelFromFile(const std::string& modelFilePath,
                                     ModelMeshes& outModelMeshes)
{
    std::unique_ptr<MObj> mobjMesh( new MObj( m_FileManager ) );
    if( !mobjMesh->LoadFromFile( modelFilePath ) )
    {
        throw GenericException( "_LoadModelFromFile() failed to load model \"" + modelFilePath + "\"" );
    }

    const boost::filesystem::path meshFileDirectory = boost::filesystem::path( modelFilePath ).remove_leaf();

    outModelMeshes.clear();
    outModelMeshes.reserve( mobjMesh->GetMeshCount() );

    for(size_t meshIdx = 0; meshIdx < mobjMesh->GetMeshCount(); meshIdx++)
    {
        const MObj::Mesh* mesh = mobjMesh->GetMesh( meshIdx );

        Mesh::SubMeshList subMeshes;

        for(const MObj::Mesh::SubMesh& mobjSubMesh : mesh->subMeshes)
        {
            subMeshes.push_back( Mesh::SubMesh() );
            Mesh::SubMesh& subMesh = subMeshes[subMeshes.size() - 1];

            subMesh.m_FirstIndex = mobjSubMesh.m_FirstIndex;
            subMesh.m_IndexCount = mobjSubMesh.m_IndexCount;
            subMesh.m_MinVertexIndexValue = mobjSubMesh.minVertexIndexValue;
            subMesh.m_MaxVertexIndexValue = mobjSubMesh.maxVertexIndexValue;
            subMesh.m_InitialMaterialPath = boost::locale::conv::utf_to_utf<char>( ( meshFileDirectory / boost::filesystem::path( boost::locale::conv::utf_to_utf<boost::filesystem::path::string_type::value_type>( mobjSubMesh.m_MaterialName + ".mat" ) ) ).c_str() );
        }

        m_MeshList.push_front( RefCountedMeshPtr() );
        MeshList::iterator retVal = m_MeshList.begin();

        std::string uniqueMeshID = modelFilePath;
        uniqueMeshID += ":";
        uniqueMeshID += mesh->name;

        m_UniqueMeshIDsToMeshesMap.insert( UniqueMeshIDsToMeshesMap::value_type( uniqueMeshID, retVal ) );

        retVal->refCount = 1;
        retVal->modelFilePath = modelFilePath;
        retVal->uniqueMeshID = uniqueMeshID;
        retVal->isInternalMesh = false;
        retVal->meshIndexWithinTheModel = static_cast<uint16_t>( meshIdx );
        retVal->meshDataInCPUReadableMemory = nullptr;

        if( mesh->type == MObj::Mesh::e_MeshTypeSkinned )
        {
            retVal->mesh = new Mesh( mesh->name,
                                     _UploadMObjDataToGPU( *mesh ),
                                     mesh->indices.size(),
                                     mesh->skinnedVertices.size(),
                                     mobjMesh->m_BoundingBox,
                                     subMeshes,
                                     mobjMesh->m_Joints.size(),
                                     mobjMesh->m_InverseBindPose,
                                     mobjMesh->m_BoneNamesMD5Hash );
        }
        else
        {
            retVal->mesh = new Mesh( mesh->name,
                                     _UploadMObjDataToGPU( *mesh ),
                                     mesh->indices.size(),
                                     mesh->staticVertices.size(),
                                     mobjMesh->m_BoundingBox,
                                     subMeshes );
        }

        outModelMeshes.push_back( MeshHandle( retVal ) );
    }
}

//-----------------------------------------------------------------------

VAO* MeshManager::_UploadInternalMeshToGPU(const EngineTypes::VertexIndexType* indexBuffer,
                                           size_t indexCount,
                                           const InternalMeshData::MeshVertex* vertexBuffer,
                                           size_t vertexCount) const
{
    assert( indexBuffer && "Internal mesh index buffer must not be null" );
    assert( vertexBuffer && "Internal mesh vertex buffer must not be null" );
    assert( ( indexCount > 0 ) && "Number of internal mesh indices must be greater than zero" );
    assert( ( vertexCount > 0 ) && "Number of internal mesh vertices must be greater than zero" );

    VAO* vao = new VAO();
    VBO* vbo = new VBO( sizeof( InternalMeshData::MeshVertex ) );
    IBO* ibo = new IBO();

    vbo->AddVertexAttribute( VBO::e_VertexAttrIdxPosition,
                             VBO::e_VertexAttrComponentTypeFloat32,
                             VBO::e_VertexAttrTypeVec3,
                             offsetof( InternalMeshData::MeshVertex, position ) );

    vbo->AddVertexAttribute( VBO::e_VertexAttrIdxTextureCoords,
                             VBO::e_VertexAttrComponentTypeFloat32,
                             VBO::e_VertexAttrTypeVec2,
                             offsetof( InternalMeshData::MeshVertex, textureCoords ) );

    vbo->BindData( vertexCount * sizeof( vertexBuffer[0] ), vertexBuffer );
    ibo->BindData( indexCount * sizeof( indexBuffer[0] ), indexBuffer );

    vao->AddVBO( vbo );
    vao->SetIBO( ibo );

    return vao;
}

//-----------------------------------------------------------------------

VAO* MeshManager::_UploadMObjDataToGPU(const MObj::Mesh& mesh) const
{
    size_t vertexSizeInBytes = 0;
    size_t posAttributeOffsetWithinVertex = 0;
    size_t normalAttributeOffsetWithinVertex = 0;
    size_t biTangentAttributeOffsetWithinVertex = 0;
    size_t tangentAttributeOffsetWithinVertex = 0;
    size_t uvAttributeOffsetWithinVertex = 0;
    size_t boneWeightAttributeOffsetWithinVertex = 0;
    size_t boneIndicesAttributeOffsetWithinVertex = 0;

    if( mesh.type == MObj::Mesh::e_MeshTypeSkinned )
    {
        vertexSizeInBytes = sizeof( MObj::SkinnedVertex );

        posAttributeOffsetWithinVertex = offsetof( MObj::SkinnedVertex, m_Pos );
        normalAttributeOffsetWithinVertex = offsetof( MObj::SkinnedVertex, m_Normal );
        biTangentAttributeOffsetWithinVertex = offsetof( MObj::SkinnedVertex, m_BiTangent );
        tangentAttributeOffsetWithinVertex = offsetof( MObj::SkinnedVertex, m_Tangent );
        uvAttributeOffsetWithinVertex = offsetof( MObj::SkinnedVertex, m_UV );
        boneWeightAttributeOffsetWithinVertex = offsetof( MObj::SkinnedVertex, m_BoneWeights );
        boneIndicesAttributeOffsetWithinVertex = offsetof( MObj::SkinnedVertex, m_BoneIndices );
    }
    else
    {
        vertexSizeInBytes = sizeof( MObj::StaticVertex );

        posAttributeOffsetWithinVertex = offsetof( MObj::StaticVertex, m_Pos );
        normalAttributeOffsetWithinVertex = offsetof( MObj::StaticVertex, m_Normal );
        biTangentAttributeOffsetWithinVertex = offsetof( MObj::StaticVertex, m_BiTangent );
        tangentAttributeOffsetWithinVertex = offsetof( MObj::StaticVertex, m_Tangent );
        uvAttributeOffsetWithinVertex = offsetof( MObj::StaticVertex, m_UV );
    }

    VAO* vao = new VAO();
    VBO* vbo = new VBO( vertexSizeInBytes );
    IBO* ibo = new IBO();

    vbo->AddVertexAttribute( VBO::e_VertexAttrIdxPosition,
                             VBO::e_VertexAttrComponentTypeFloat32,
                             VBO::e_VertexAttrTypeVec3,
                             posAttributeOffsetWithinVertex );

    vbo->AddVertexAttribute( VBO::e_VertexAttrIdxNormal,
                             VBO::e_VertexAttrComponentTypeFloat32,
                             VBO::e_VertexAttrTypeVec3,
                             normalAttributeOffsetWithinVertex );

    vbo->AddVertexAttribute( VBO::e_VertexAttrIdxBiTangent,
                             VBO::e_VertexAttrComponentTypeFloat32,
                             VBO::e_VertexAttrTypeVec3,
                             biTangentAttributeOffsetWithinVertex );

    vbo->AddVertexAttribute( VBO::e_VertexAttrIdxTangent,
                             VBO::e_VertexAttrComponentTypeFloat32,
                             VBO::e_VertexAttrTypeVec3,
                             tangentAttributeOffsetWithinVertex );

    vbo->AddVertexAttribute( VBO::e_VertexAttrIdxTextureCoords,
                             VBO::e_VertexAttrComponentTypeFloat32,
                             VBO::e_VertexAttrTypeVec2,
                             uvAttributeOffsetWithinVertex );

    if( mesh.type == MObj::Mesh::e_MeshTypeSkinned )
    {
        vbo->AddVertexAttribute( VBO::e_VertexAttrIdxBoneWeights,
                                 VBO::e_VertexAttrComponentTypeFloat32,
                                 VBO::e_VertexAttrTypeVec4,
                                 boneWeightAttributeOffsetWithinVertex );

        vbo->AddVertexAttribute( VBO::e_VertexAttrIdxBoneIndices,
                                 VBO::e_VertexAttrComponentTypeFloat32,
                                 VBO::e_VertexAttrTypeVec4,
                                 boneIndicesAttributeOffsetWithinVertex );


        vbo->BindData( mesh.skinnedVertices.size() * sizeof ( MObj::SkinnedVertex ),
                       &(mesh.skinnedVertices[0]) );
    }
    else
    {
        vbo->BindData( mesh.staticVertices.size() * sizeof ( MObj::StaticVertex ),
                       &(mesh.staticVertices[0]) );
    }

    ibo->BindData( mesh.indices.size() * sizeof( mesh.indices[0] ),
                   &(mesh.indices[0]) );

    vao->AddVBO( vbo );
    vao->SetIBO( ibo );

    return vao;
}

//-----------------------------------------------------------------------
