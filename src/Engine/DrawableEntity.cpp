#include "DrawableEntity.h"

#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/matrix_transform.hpp"


#include "MsgLogger.h"
#include "StringUtils.h"
#include <MathUtils.h>
#include <CommonDefinitions.h>

#include <algorithm>
#include <iostream>

#include <imgutl/ImageUtils.h>
#include <OpenGLErrorChecking.h>
#include <GraphicsDevice/GraphicsDevice.h>

#define BUFFER_OFFSET(i) ((int *)NULL + (i)) //BUFFER_OFFSET is used then specifying indices to be used in glDrawElements function

//-----------------------------------------------------------------------

DrawableEntity::DrawableEntity(GraphicsDevice& graphicsDevice,
                               FileManager& fileManager,
                               bool UNUSED_PARAM( loadTextures ),
                               bool clampTexturesToEdge)
: m_GraphicsDevice(graphicsDevice),
  m_FileManager( fileManager )
{
    this->m_ObjLocation=glm::vec3(0.0f,0.0f,0.0f);
    this->m_Scale=glm::vec3(1.0f,1.0f,1.0f);

    m_RotationVector=glm::vec3(0.0f);//Set rotation vector values to 0.0

    this->m_AlphaBlendingEnabled=false;
    this->m_ClampTexturesToEdge=clampTexturesToEdge;

    this->m_RenderingMode=DrawableEntity::RENDER_IN_WIREFRAME;

    this->m_modelObj = NULL;

    glGenVertexArrays(1, &this->m_VAO);
    CheckForGLErrors();
    glGenBuffers(1, &this->m_VBO);
    CheckForGLErrors();
    glGenBuffers(1, &this->m_IBO);
    CheckForGLErrors();

    m_UseShaders = true;
}

//-----------------------------------------------------------------------

DrawableEntity::DrawableEntity(GraphicsDevice& graphicsDevice,
                               FileManager& fileManager,
                               std::string modelObjFileName,
                               bool loadTextures,
                               bool clampTexturesToEdge)
: m_GraphicsDevice(graphicsDevice),
  m_FileManager( fileManager )
{
    this->m_ObjLocation=glm::vec3(0.0f,0.0f,0.0f);
    this->m_Scale=glm::vec3(1.0f,1.0f,1.0f);

    m_RotationVector=glm::vec3(0.0f);//Set rotation vector values to 0.0

    this->m_AlphaBlendingEnabled=false;
    this->m_ClampTexturesToEdge=clampTexturesToEdge;

    this->m_RenderingMode=DrawableEntity::RENDER_IN_WIREFRAME;

    this->m_modelObj = NULL;
    this->m_modelObj = new ModelBinObj();
    this->m_modelObj->import(modelObjFileName.c_str());//,true);
    //this->m_modelObj->normalize();

    glGenVertexArrays(1, &this->m_VAO);
    CheckForGLErrors();
    glGenBuffers(1, &this->m_VBO);
    CheckForGLErrors();
    glGenBuffers(1, &this->m_IBO);
    CheckForGLErrors();

    this->prepareIBO_and_VBO();

    MsgLogger::log(MsgLogger::EVENT_MSG,"################ Loading Model: "+modelObjFileName+"##################");
    if(loadTextures)
    {
        this->loadTextures();
    }
    MsgLogger::log(MsgLogger::EVENT_MSG,"#####################################################################\n");

    m_UseShaders = true;
}

//-----------------------------------------------------------------------

DrawableEntity::~DrawableEntity()
{
    if( this->m_modelObj != NULL )
    {
        delete this->m_modelObj;
    }
    this->deleteTextures();
    glDeleteVertexArrays(1,&this->m_VAO);
    CheckForGLErrors();
    glDeleteBuffers(1,&this->m_VBO);
    CheckForGLErrors();
    glDeleteBuffers(1,&this->m_IBO);
    CheckForGLErrors();
}

//-----------------------------------------------------------------------

void DrawableEntity::render(ShaderProg* shaderProgToUse_1, ShaderProg* shaderProgToUse_2, const DrawableEntity::Transformation *  t)
{
    m_GraphicsDevice.SaveCurrentPolygonMode();
    m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionBlending );
    m_GraphicsDevice.SaveCurrentBlendFunctionState();

    glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), m_ObjLocation);
    modelMatrix = glm::rotate(modelMatrix, m_RotationVector.x, glm::vec3(1.0f, 0.0f, 0.0f));
    modelMatrix = glm::rotate(modelMatrix, m_RotationVector.y, glm::vec3(0.0f, 1.0f, 0.0f));
    modelMatrix = glm::rotate(modelMatrix,m_RotationVector.z, glm::vec3(0.0f, 0.0f, 1.0f));
    modelMatrix = glm::scale(modelMatrix,m_Scale);

    glm::mat4 modelViewMatrix = t->viewMatrix * modelMatrix;

    glm::mat3 normalMatrix=glm::transpose(glm::inverse(glm::mat3(modelMatrix)));


    glBindVertexArray(this->m_VAO);
    CheckForGLErrors();
    //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,this->m_IBO);
    //CheckForGLErrors();


    if(m_UseShaders)
    {
        shaderProgToUse_1->Use();
    }

    glUniformMatrix4fv(glGetUniformLocation(shaderProgToUse_1->GetHandle(), "mvmatrix"), 1, GL_FALSE, glm::value_ptr(modelViewMatrix));
    CheckForGLErrors();
    glUniformMatrix4fv(glGetUniformLocation(shaderProgToUse_1->GetHandle(), "pmatrix"), 1, GL_FALSE, glm::value_ptr(t->projectionMatrix));
    CheckForGLErrors();
    glUniformMatrix3fv(glGetUniformLocation(shaderProgToUse_1->GetHandle(), "nmatrix"), 1, GL_FALSE, glm::value_ptr(normalMatrix));
    CheckForGLErrors();

    if(m_RenderingMode==DrawableEntity::RENDER_IN_WIREFRAME)
    {
        m_GraphicsDevice.SetPolygonMode( GraphicsDevice::e_PolygonModeLine );
    }

    if(m_RenderingMode==DrawableEntity::RENDER_WITH_TEXTURE || m_RenderingMode==RENDER_IN_MIXED_MODE)
    {
        if(m_AlphaBlendingEnabled)
        {
            m_GraphicsDevice.OptionEnable( GraphicsDevice::e_OptionBlending );
            m_GraphicsDevice.SetBlendFunctionState( GraphicsDevice::e_BlendFunctionSrcAlpha,
                                                    GraphicsDevice::e_BlendFunctionOneMinusSrcAlpha );
        }

        for(int i=0;i < this->m_modelObj->getNumberOfMeshes();i++) //Draw all the meshes of an object
        {
            if(m_TexturesOfMeshes[i].colorTexture.hasTexture==false && m_RenderingMode==RENDER_IN_MIXED_MODE)
            {
                if(m_UseShaders)
                {
                    shaderProgToUse_2->Use();
                }

                glUniformMatrix4fv(glGetUniformLocation(shaderProgToUse_2->GetHandle(), "mvmatrix"), 1, GL_FALSE, glm::value_ptr(modelViewMatrix));
                CheckForGLErrors();
                glUniformMatrix4fv(glGetUniformLocation(shaderProgToUse_2->GetHandle(), "pmatrix"), 1, GL_FALSE, glm::value_ptr(t->projectionMatrix));
                CheckForGLErrors();
                glUniformMatrix3fv(glGetUniformLocation(shaderProgToUse_2->GetHandle(), "nmatrix"), 1, GL_FALSE, glm::value_ptr(normalMatrix));
                CheckForGLErrors();
                setUpObjMaterial(m_modelObj->getMesh(i).pMaterial,shaderProgToUse_2);
            }
            else
            {
                glUniform1i(glGetUniformLocation(shaderProgToUse_1->GetHandle(), "tex2D"), 0);
                CheckForGLErrors();
                setUpObjMaterial(m_modelObj->getMesh(i).pMaterial,shaderProgToUse_1);
                glActiveTexture(GL_TEXTURE0);
                CheckForGLErrors();
                glBindTexture(GL_TEXTURE_2D,m_TexturesOfMeshes[i].colorTexture.textureID);
                CheckForGLErrors();
            }

            glDrawElements(GL_TRIANGLES, this->m_modelObj->getMesh(i).triangleCount*3, GL_UNSIGNED_INT, BUFFER_OFFSET(this->m_modelObj->getMesh(i).startIndex));
            CheckForGLErrors();

            if(m_TexturesOfMeshes[i].colorTexture.hasTexture==false && m_RenderingMode==RENDER_IN_MIXED_MODE)
            {
                shaderProgToUse_1->Use();
                glUniformMatrix4fv(glGetUniformLocation(shaderProgToUse_1->GetHandle(), "mvmatrix"), 1, GL_FALSE, glm::value_ptr(modelViewMatrix));
                CheckForGLErrors();
                glUniformMatrix4fv(glGetUniformLocation(shaderProgToUse_1->GetHandle(), "pmatrix"), 1, GL_FALSE, glm::value_ptr(t->projectionMatrix));
                CheckForGLErrors();
                glUniformMatrix3fv(glGetUniformLocation(shaderProgToUse_1->GetHandle(), "nmatrix"), 1, GL_FALSE, glm::value_ptr(normalMatrix));
                CheckForGLErrors();
            }
        }
    }
    else
    {
        for(int i=0;i < this->m_modelObj->getNumberOfMeshes();i++) //Draw all the meshes of an object
        {
            setUpObjMaterial(m_modelObj->getMesh(i).pMaterial,shaderProgToUse_1);
            glDrawElements(GL_TRIANGLES, this->m_modelObj->getMesh(i).triangleCount*3, GL_UNSIGNED_INT, BUFFER_OFFSET(this->m_modelObj->getMesh(i).startIndex));
            CheckForGLErrors();
        }
    }

    m_GraphicsDevice.RestorePreviousBlendFunctionState();
    m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionBlending );
    m_GraphicsDevice.RestorePreviousPolygonMode();
}

//-----------------------------------------------------------------------

void DrawableEntity::prepareIBO_and_VBO()
{
    glBindVertexArray(this->m_VAO);
    CheckForGLErrors();

    glBindBuffer(GL_ARRAY_BUFFER,this->m_VBO);
    CheckForGLErrors();
    glBufferData ( GL_ARRAY_BUFFER, this->m_modelObj->getNumberOfVertices() * sizeof ( ModelBinObj::Vertex ), m_modelObj->getVertexBuffer(), GL_STATIC_DRAW );
    CheckForGLErrors();

    glVertexAttribPointer (0 /*in_Position*/, 3, GL_FLOAT, GL_FALSE,  sizeof ( ModelBinObj::Vertex), ( const GLvoid* ) offsetof (ModelBinObj::Vertex,position) );
    CheckForGLErrors();
    glEnableVertexAttribArray(0 /*in_Position*/);
    CheckForGLErrors();

    glVertexAttribPointer (1 /*in_Normal*/, 3, GL_FLOAT, GL_FALSE, sizeof ( ModelBinObj::Vertex ), ( const GLvoid* ) offsetof(ModelBinObj::Vertex,normal) );
    CheckForGLErrors();
    glEnableVertexAttribArray(1 /*in_Normal*/);
    CheckForGLErrors();

    glVertexAttribPointer (2 /*in_uvCoord*/, 2, GL_FLOAT, GL_FALSE, sizeof ( ModelBinObj::Vertex ), ( const GLvoid* ) offsetof(ModelBinObj::Vertex,texCoord) );
    CheckForGLErrors();
    glEnableVertexAttribArray(2 /*in_uvCoord*/);
    CheckForGLErrors();

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,this->m_IBO);
    CheckForGLErrors();
    glBufferData (GL_ELEMENT_ARRAY_BUFFER, m_modelObj->getNumberOfIndices() * m_modelObj->getIndexSize(), m_modelObj->getIndexBuffer(), GL_STATIC_DRAW );
    CheckForGLErrors();
}

//-----------------------------------------------------------------------

void DrawableEntity::deleteTextures()
{
    for(size_t i=0;i < m_TexturesOfMeshes.size();i++)
    {
        ImageUtils::DeleteImage(m_TexturesOfMeshes[i].colorTexture.pixelData);
        glDeleteTextures(1,&(m_TexturesOfMeshes[i].colorTexture.textureID));
        CheckForGLErrors();
    }
}

//-----------------------------------------------------------------------

DrawableEntity::Texture_t DrawableEntity::extractTexture(ImageUtils::ImageData_t* surface,const std::string* imgName)
{
    GLuint textureID;			// This is a handle to our texture object
    GLenum texture_format;

    switch(surface->format)
    {
        case ImageUtils::RGB:
        {
            texture_format = GL_RGB;
            MsgLogger::log(MsgLogger::EVENT_MSG,"Texture "+*imgName+" is RGB");
            break;
        }
        case ImageUtils::RGBA:
        {
            texture_format = GL_RGBA;
            MsgLogger::log(MsgLogger::EVENT_MSG,"Texture "+*imgName+" is RGBA");
            break;
        }
#ifndef OPENGL_ES
        case ImageUtils::BGR:
        {
            texture_format = GL_BGR;
            MsgLogger::log(MsgLogger::EVENT_MSG,"Texture "+*imgName+" is BGR");
            break;
        }
        case ImageUtils::BGRA:
        {
            texture_format = GL_BGRA;
            MsgLogger::log(MsgLogger::EVENT_MSG,"Texture "+*imgName+" is BGRA");
            break;
        }
#endif
        default:
        {
            MsgLogger::log(MsgLogger::ERROR_MSG,"the image "+*imgName+" has unsupported colour format...");
            exit(1);
            assert(false && "Not supported colour format");
            break;
        }
    }
    glGenTextures( 1, &textureID );
    CheckForGLErrors();


    Texture_t tex = {true,surface,textureID,texture_format};
    return tex;
}

//-----------------------------------------------------------------------

void DrawableEntity::loadTextures()
{
    MsgLogger::log(MsgLogger::EVENT_MSG,"------------------Loading Textures------------------");
    ImageUtils::ImageData_t* tempColorTex;

    for(int i=0;i<this->m_modelObj->getNumberOfMeshes();i++)
    {
        if(!m_modelObj->getMesh(i).pMaterial->colorMapFilename.empty())
        {
            /*----------------------------------------------------------------------------------------------------------------------------*/
            tempColorTex = ImageUtils::LoadImage(m_FileManager, ImageUtils::TGA, m_modelObj->getMesh(i).pMaterial->colorMapFilename);
            /*----------------------------------------------------------------------------------------------------------------------------*/

            if(tempColorTex==NULL) {
                MsgLogger::log(MsgLogger::ERROR_MSG,"Loading Texture("+m_modelObj->getMesh(i).pMaterial->colorMapFilename+")");
                exit(1);
            } else {
                DrawableEntity::Textures ts = {extractTexture(tempColorTex,&m_modelObj->getMesh(i).pMaterial->colorMapFilename)};
                m_TexturesOfMeshes.push_back(ts);

                MsgLogger::log(MsgLogger::EVENT_MSG,"Loaded("+numToString(i)+"): "+m_modelObj->getMesh(i).pMaterial->colorMapFilename);
            }
        }
        else //Mesh has no texture
        {
            Texture_t tex = {false,NULL,0,0};
            DrawableEntity::Textures ts = {tex};
            m_TexturesOfMeshes.push_back(ts);
        }
    }
    MsgLogger::log(MsgLogger::EVENT_MSG,"----------------------------------------------------");

    this->prepareTextures();
}

//-----------------------------------------------------------------------

void DrawableEntity::prepareTextures()
{
    MsgLogger::log(MsgLogger::EVENT_MSG,"-----------------Preparing Textures-----------------");
    for(int i=0;i<this->m_modelObj->getNumberOfMeshes();i++)
    {
        prepareATexture(m_TexturesOfMeshes[i].colorTexture, m_ClampTexturesToEdge, numToString(i));
    }
    MsgLogger::log(MsgLogger::EVENT_MSG,"----------------------------------------------------");
}

//-----------------------------------------------------------------------

void DrawableEntity::prepareATexture(const Texture_t& texture, bool clampToEdge, std::string textureFileName)
{
        if(texture.hasTexture==true)
        {
            glBindTexture(GL_TEXTURE_2D,texture.textureID);
            CheckForGLErrors();

            if(clampToEdge)
            {
                glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
                CheckForGLErrors();
                glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
                CheckForGLErrors();
            }
            else
            {
                glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
                CheckForGLErrors();
                glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
                CheckForGLErrors();
            }
            glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
            CheckForGLErrors();
            glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
            CheckForGLErrors();

                            //-------------------DEBUG code------------------------------
            std::string msg = "Texture ("+textureFileName+"):"+
                              "  Texture Width: "+numToString(texture.pixelData->w)+
                              "  Texture Height: "+numToString(texture.pixelData->h);

            msg.append("  Texture Format: ");
            switch(texture.textureFormat)
            {
                case GL_RGBA:
                    msg.append("GL_RGBA");
                    break;
#ifndef OPENGL_ES
                case GL_BGRA:
                    msg.append("GL_BGRA");
                    break;
#endif
                case GL_RGB:
                    msg.append("GL_RGB");
                    break;
#ifndef OPENGL_ES
                case GL_BGR:
                    msg.append("GL_BGR");
                    break;
#endif
                default:
                    MsgLogger::log(MsgLogger::ERROR_MSG,"texture with unknown pixel data format found while preparing textures.");
                    break;
            }
            MsgLogger::log(MsgLogger::EVENT_MSG,msg);

//            const GLsizei mipMapLevelsToGenerate = (floor(log2(std::max(texture.pixelData->w, texture.pixelData->h))) + 1);

//                glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, mipMapLevelsToGenerate );
//                CheckForGLErrors();

            glTexStorage2D(GL_TEXTURE_2D, 1/*mipMapLevelsToGenerate*/, GL_RGBA8, static_cast<GLsizei>( texture.pixelData->w ), static_cast<GLsizei>( texture.pixelData->h ) );
            CheckForGLErrors();
            glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0,
				            static_cast<GLsizei>( texture.pixelData->w ),
					        static_cast<GLsizei>( texture.pixelData->h ),
                            texture.textureFormat,
                            GL_UNSIGNED_BYTE,
                            texture.pixelData->data);
            CheckForGLErrors();
//                glGenerateMipmap(GL_TEXTURE_2D);  //Generate num_mipmaps number of mipmaps here.
//                CheckForGLErrors();

//                glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA8,
//                              texture.pixelData->w,
//                              texture.pixelData->h,
//                              0,
//                              texture.textureFormat,
//                              GL_UNSIGNED_BYTE,
//                              texture.pixelData->data );
//                CheckForGLErrors();
            //glGenerateMipmap(GL_TEXTURE_2D);
        }
}

//-----------------------------------------------------------------------

void DrawableEntity::setUpObjMaterial(const ModelBinObj::Material* mat,ShaderProg* shaderProgToUse)
{
    glUniform4f(glGetUniformLocation(shaderProgToUse->GetHandle(), "Material.ambient"), mat->ambient[0], mat->ambient[1], mat->ambient[2], mat->ambient[3]);
    CheckForGLErrors();
    glUniform4f(glGetUniformLocation(shaderProgToUse->GetHandle(), "Material.diffuse"), mat->diffuse[0],mat->diffuse[1], mat->diffuse[2], mat->diffuse[3]);
    CheckForGLErrors();
    glUniform4f(glGetUniformLocation(shaderProgToUse->GetHandle(), "Material.specular"), mat->specular[0], mat->specular[1], mat->specular[2], mat->specular[3]);
    CheckForGLErrors();
    glUniform1f(glGetUniformLocation(shaderProgToUse->GetHandle(), "Material.shininess"), mat->shininess);
    CheckForGLErrors();
}

//-----------------------------------------------------------------------
