#include "InputConstants.h"
#include <Config/CfgUtils.h>
#include <CommonDefinitions.h>

namespace InputMapping
{
    //-----------------------------------------------------------------------

    static const char* c_ActionNames[e_ActionCount] =
    {
        "ActionOpenMainMenu",
        "ActionEnableEffects",
        "ActionToggleConsole",
        "ActionToggleForwardDeferredRendering",
        "ActionToggleCaptureMouseInTheWindow",
        "ActionToggleCenterMouse",
        "ActionToggleWireframeMode",
        "ActionToggleDebugDataRendering",
        "ActionToggleGUIDebugger",
        "ActionClickWithMouse",

        "ActionTapWithFingerOne",
        "ActionTapWithFingerTwo",

        "ActionIncreaseFieldOfViewOfTheCamera",
        "ActionDecreaseFieldOfViewOfTheCamera",

        "ActionChooseEntityToMove",
        "ActionNextEntity",
        "ActionPreviousEntity",
        "ActionNextEntityGroup",
        "ActionRotateEntity",
        "ActionMoveEntity",

        "ActionToggleFlying",
        "ActionToggleCrouch",
        "ActionDoJump"
    };

    //-----------------------------------------------------------------------

    static const char* c_StateNames[e_StateCount] =
    {
        "StateMoveForward",
        "StateMoveBackwards",
        "StateMoveLeft",
        "StateMoveRight",
        "StateMoveFast",
        "StateMoveSlowly",
        "StateMoveUp",
        "StateMoveDown",
        "StateLeftMouseButtonIsDown",
        "StateMiddleMouseButtonIsDown",
        "StateRightMouseButtonIsDown",
        "StateLeftMetaKeyIsDown",

        "StateFingerOneIsDown",
        "StateFingerTwoIsDown",
    };

    //-----------------------------------------------------------------------

    static const char* c_RangeNames[e_RangeCount] =
    {
        "RangePointerX",
        "RangePointerY",
        "RangePointerAbsoluteX",
        "RangePointerAbsoluteY",
        "RangeMouseWheelHorizontal",
        "RangeMouseWheelVertical",

        "RangeFingerOneX",
        "RangeFingerOneY",
        "RangeFingerOneAbsoluteX",
        "RangeFingerOneAbsoluteY",

        "RangeFingerTwoX",
        "RangeFingerTwoY",
        "RangeFingerTwoAbsoluteX",
        "RangeFingerTwoAbsoluteY"
    };

    //-----------------------------------------------------------------------

	const char* GetActionName(Action action)
	{
        const char* retVal = nullptr;
        if( action < e_ActionCount )
        {
            retVal = c_ActionNames[action];
        }

        return retVal;
	}

	//-----------------------------------------------------------------------

	const char* GetStateName(State state)
	{
        const char* retVal = nullptr;
        if( state < e_StateCount )
        {
            retVal = c_StateNames[state];
        }

        return retVal;
	}

	//-----------------------------------------------------------------------

	const char* GetRangeName(Range range)
	{
        const char* retVal = nullptr;
        if( range < e_RangeCount )
        {
            retVal = c_RangeNames[range];
        }

        return retVal;
	}

	//-----------------------------------------------------------------------

	bool IsValidActionName(const std::string& actionName)
	{
        return CfgUtils::StringToCorrespondingEnum<Action>( actionName,
                                                            c_ActionNames,
                                                            ArraySize( c_ActionNames ) );
	}

	//-----------------------------------------------------------------------

	bool IsValidStateName(const std::string& stateName)
	{
        return CfgUtils::StringToCorrespondingEnum<State>( stateName,
                                                           c_StateNames,
                                                           ArraySize( c_StateNames ) );
	}

	//-----------------------------------------------------------------------

	bool IsValidRangeName(const std::string& rangeName)
	{
        return CfgUtils::StringToCorrespondingEnum<Range>( rangeName,
                                                           c_RangeNames,
                                                           ArraySize( c_RangeNames ) );
	}

    //-----------------------------------------------------------------------

	bool ActionNameToAction(const std::string& actionName, Action& outVal)
	{
        return CfgUtils::StringToCorrespondingEnum<Action>( actionName,
                                                            c_ActionNames,
                                                            ArraySize( c_ActionNames ),
                                                            &outVal );
	}

	//-----------------------------------------------------------------------

	bool StateNameToState(const std::string& stateName, State& outVal)
	{
        return CfgUtils::StringToCorrespondingEnum<State>( stateName,
                                                           c_StateNames,
                                                           ArraySize( c_StateNames ),
                                                           &outVal );
	}

	//-----------------------------------------------------------------------

	bool RangeNameToRange(const std::string& rangeName, Range& outVal)
	{
        return CfgUtils::StringToCorrespondingEnum<Range>( rangeName,
                                                           c_RangeNames,
                                                           ArraySize( c_RangeNames ),
                                                           &outVal );
	}

	//-----------------------------------------------------------------------
}
