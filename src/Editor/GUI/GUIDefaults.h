#ifndef GUIDEFAULTS_H_INCLUDED
#define GUIDEFAULTS_H_INCLUDED

#include <QtGui/QPalette>

class GUIDefaults
{
public:
    static GUIDefaults& GetSingleton();

    /**
     * @brief DefaultPalette - Get default palette for the whole GUI
     * @return Default GUI palette
     */
    inline const QPalette& DefaultPalette() const
    {
        return m_DefaultPalette;
    }

    static inline int DefaultMaxDecimalPlacesInTheFinalLineEditValue()
    {
        return 3;
    }

    static inline int DefaultMaxDecimalPlacesInTheWorkInProgressLineEditValue()
    {
        return 8;
    }

private:
    void _InitializeDarkPalette(QPalette& palette);

    GUIDefaults();

    QPalette m_DefaultPalette;
};

#endif // GUIDEFAULTS_H_INCLUDED
