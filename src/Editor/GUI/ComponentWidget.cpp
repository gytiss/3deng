#include "ComponentWidget.h"

//-----------------------------------------------------------------------

ComponentWidget::ComponentWidget(CategorizedListWidget* listWidgetToAddOurselvesTo,
                                 const std::string& name,
                                 QWidget* parent)
: QWidget( parent ),
  m_FloatValueValidator( GUIDefaults::DefaultMaxDecimalPlacesInTheFinalLineEditValue(),
                         GUIDefaults::DefaultMaxDecimalPlacesInTheWorkInProgressLineEditValue() ),
  m_Name( name.c_str() ),
  m_CategoryHandle()
{
    m_CategoryHandle = listWidgetToAddOurselvesTo->AddCetegory( GetName() );
    listWidgetToAddOurselvesTo->AddCatedoryItem( m_CategoryHandle, this );
    SetHidden( true );
}

//-----------------------------------------------------------------------

ComponentWidget::~ComponentWidget()
{
}

//-----------------------------------------------------------------------

void ComponentWidget::SetHidden(bool value)
{
    CategorizedListWidget::SetCategoryHide( m_CategoryHandle, value );
}

//-----------------------------------------------------------------------
