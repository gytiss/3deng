#include "OpenALErrorChecking.h"
#include "OpenALIncludes.h"
#include <AudioDevice/Device.h>
#include <CommonDefinitions.h>
#include <MsgLogger.h>
#include <StringUtils.h>
#include <iostream>
#include <cstdlib>

/**
 * Note: Although "context" parameter is not used in this function it is still required in order
 *       to guarantee that this function is never called without a valid OpenAL context
 */
void _CheckForALErrors_(AudioDevice::Context& UNUSED_PARAM(context),
                        const std::string& fileName,
                        int lineNum)
{
    std::string errMsg = "";

    ALenum errorCode = alGetError();

    if( errorCode != AL_NO_ERROR )
    {
        switch( errorCode )
        {
            case AL_INVALID_NAME:
                errMsg = "AL_INVALID_NAME";
                break;

            case AL_INVALID_ENUM:
                errMsg = "AL_INVALID_ENUM";
                break;

            case AL_INVALID_VALUE:
                errMsg = "AL_INVALID_VALUE";
                break;

            case AL_INVALID_OPERATION:
                errMsg = "AL_INVALID_OPERATION";
                break;

            case AL_OUT_OF_MEMORY:
                errMsg = "AL_OUT_OF_MEMORY";
                break;

            default:
                errMsg = "Unknown OpenAL AL error received";
                break;
        }

        std::string errorString = "\"";
        errorString += fileName;
        errorString += "\"(";
        errorString += StringUitls::NumToString( lineNum );
        errorString += ": ";
        errorString += errMsg;

        MsgLogger::LogError( errorString );

        exit( EXIT_FAILURE );
    }
}

void _CheckForALCErrors_(AudioDevice::Device& device, const std::string& fileName, int lineNum)
{
    std::string errMsg = "";

    ALenum errorCode = alcGetError( device.GetALHandle() );

    if( errorCode != AL_NO_ERROR )
    {
        switch( errorCode )
        {
            case ALC_INVALID_DEVICE:
                errMsg = "ALC_INVALID_DEVICE";
                break;

            case ALC_INVALID_CONTEXT:
                errMsg = "ALC_INVALID_CONTEXT";
                break;

            case ALC_INVALID_ENUM:
                errMsg = "ALC_INVALID_ENUM";
                break;

            case ALC_INVALID_VALUE:
                errMsg = "ALC_INVALID_VALUE";
                break;

            case ALC_OUT_OF_MEMORY:
                errMsg = "ALC_OUT_OF_MEMORY";
                break;

            default:
                errMsg = "Unknown OpenAL ALC error received";
                break;
        }

        std::string errorString = "\"";
        errorString += fileName;
        errorString += "\"(";
        errorString += StringUitls::NumToString( lineNum );
        errorString += ": ";
        errorString += errMsg;

        MsgLogger::LogError( errorString );

        exit( EXIT_FAILURE );
    }
}
