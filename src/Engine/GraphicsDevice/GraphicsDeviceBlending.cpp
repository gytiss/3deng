#include "GraphicsDeviceBlending.h"
#include <OpenGLErrorChecking.h>

//-----------------------------------------------------------------------

GraphicsDeviceBlending::GraphicsDeviceBlending()
: m_BlendEquationValuesStack(),
  m_CurrentBlendEquationValue( e_BlendEquationAdd ),
  m_BlendFunctionStatesStack(),
  m_CurrentBlendFunctionState( BlendFunctionState( e_BlendFunctionOne, e_BlendFunctionZero ) ),
  m_BlendColorsStack(),
  m_CurrentBlendColor( 0.0f, 0.0f, 0.0f, 0.0f )
{
    // Set initial value
    glBlendEquation( m_CurrentBlendEquationValue );
    CheckForGLErrors();

    // Set initial value
    glBlendFunc( m_CurrentBlendFunctionState.first,
                 m_CurrentBlendFunctionState.second );
    CheckForGLErrors();

    glBlendColor( m_CurrentBlendColor[0],
                  m_CurrentBlendColor[1],
                  m_CurrentBlendColor[2],
                  m_CurrentBlendColor[3] );
    CheckForGLErrors();
}

//-----------------------------------------------------------------------

void GraphicsDeviceBlending::SaveCurrentBlendEquation()
{
    m_BlendEquationValuesStack.push( m_CurrentBlendEquationValue );
}

//-----------------------------------------------------------------------

void GraphicsDeviceBlending::RestorePreviousBlendEquation()
{
    SetBlendEquation( m_BlendEquationValuesStack.top() );
    m_BlendEquationValuesStack.pop();
}

//-----------------------------------------------------------------------

void GraphicsDeviceBlending::SetBlendEquation(BlendEquation equation)
{
    if( equation != m_CurrentBlendEquationValue )
    {
        m_CurrentBlendEquationValue = equation;
        glBlendEquation( m_CurrentBlendEquationValue );
        CheckForGLErrors();
    }
}

//-----------------------------------------------------------------------

void GraphicsDeviceBlending::SaveCurrentBlendFunctionState()
{
    m_BlendFunctionStatesStack.push( m_CurrentBlendFunctionState );
}

//-----------------------------------------------------------------------

void GraphicsDeviceBlending::RestorePreviousBlendFunctionState()
{
    BlendFunctionState previousState = m_BlendFunctionStatesStack.top();
    SetBlendFunctionState( previousState.first, previousState.second );
    m_BlendFunctionStatesStack.pop();
}

//-----------------------------------------------------------------------

void GraphicsDeviceBlending::SetBlendFunctionState(BlendFunction sourceFactor,
                                                   BlendFunction destinationFactor)
{
    if( ( sourceFactor != m_CurrentBlendFunctionState.first ) ||
        ( destinationFactor != m_CurrentBlendFunctionState.second ) )
    {
        m_CurrentBlendFunctionState.first = sourceFactor;
        m_CurrentBlendFunctionState.second = destinationFactor;

        glBlendFunc( m_CurrentBlendFunctionState.first,
                     m_CurrentBlendFunctionState.second );
        CheckForGLErrors();
    }
}

//-----------------------------------------------------------------------

void GraphicsDeviceBlending::SaveCurrentBlendColor()
{
    m_BlendColorsStack.push( m_CurrentBlendColor );
}

//-----------------------------------------------------------------------

void GraphicsDeviceBlending::RestorePreviousBlendColor()
{
    SetBlendColor( m_BlendColorsStack.top() );
    m_BlendColorsStack.pop();
}

//-----------------------------------------------------------------------

void GraphicsDeviceBlending::SetBlendColor(const glm::vec4& rgbaColor)
{
    if( rgbaColor != m_CurrentBlendColor )
    {
        glBlendColor( m_CurrentBlendColor[0],
                      m_CurrentBlendColor[1],
                      m_CurrentBlendColor[2],
                      m_CurrentBlendColor[3] );
        CheckForGLErrors();
    }
}

//-----------------------------------------------------------------------

void GraphicsDeviceBlending::SetBlendColor(float red, float green, float blue, float alpha)
{
    SetBlendColor( glm::vec4( red, green, blue, alpha ) );
}

//-----------------------------------------------------------------------
