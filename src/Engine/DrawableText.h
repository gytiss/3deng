#ifndef DRAWABLETEXT_H_INCLUDED
#define DRAWABLETEXT_H_INCLUDED

#include <string>
#include <map>

#include "DrawableEntity.h"
#include <GraphicsDevice/ShaderProg.h>
#include <GraphicsDevice/VAO.h>
#include <SystemInterface.h>
#include <Renderer/ShaderPrograms.h>
#include <glm/glm.hpp>
#include "ModelBinObj.h"
#include "FontLoader.h"

class GraphicsDevice;
class FileManager;
class Texture;

class DrawableText
{
public:
    DrawableText(GraphicsDevice& graphicsDevice,
                 SystemInterface& systemInterface,
                 FileManager& fileManager,
                 std::string fontFileName,
                 size_t font_height);
    ~DrawableText();

    void Render(std::string text, float x, float y);

private:
    DrawableText&  operator = (const DrawableText& other); // Prevent copying of DrawableText
    DrawableText(const DrawableText& other);               // Prevent copying of DrawableText

    static const size_t c_CharTextureCount = 95u;

    typedef std::map<char,std::map<char,float> > charKerning_t;

    inline ModelBinObj::Vertex* generateCharDrawingQuad(float width, float height,float textureWidth, float textureHeight);
    void prepareRenderingContext();
    inline void prepareCharsTexture(unsigned char charCode, size_t width, size_t height, unsigned char* pixels);
    inline void twoChannelTextureToRGBA(unsigned char* twoChannelData, unsigned char* expandedData, size_t textureWidth, size_t textureHeight);
    inline void generateCharacterKerning(FT_Face face, char charCode);

    static void _DrawableSurfaceSizeChaged(const SystemInterface::DrawableSurfaceSize& newSize, void* userData);

    Texture* m_CharTextures[c_CharTextureCount];
    glm::vec3* m_CharSpacingTranforms;
    glm::vec3* m_CharElevationTransforms;
    charKerning_t m_CharKerning;

    glm::mat4 m_ProjectionMatrix;
    ShaderProgText m_TextShaderProg;

    glm::vec3* m_CharsSizes;

    glm::vec2* m_CharacterSpaces;
    ModelBinObj::Vertex* m_CharDrawingQuads;
    VAO m_VAO;

    GraphicsDevice& m_GraphicsDevice;
    SystemInterface& m_SystemInterface;
    FileManager& m_FileManager;
};

#endif // DRAWABLETEXT_H_INCLUDED
