

in vec3 in_VertexPos;

uniform mat4 u_ModelViewProjectionMatrix;

void main()
{
    gl_Position = u_ModelViewProjectionMatrix * vec4( in_VertexPos, 1.0f );
}
