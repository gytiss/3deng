#include "PhysicsDebugDataDrawer.h"
#include <GraphicsDevice/VBO.h>
#include <Renderer/ShaderPrograms.h>
#include <Renderer/RenderableObject.h>
#include <CommonDefinitions.h>

//-----------------------------------------------------------------------

PhysicsDebugDataDrawer::PhysicsDebugDataDrawer()
: m_DebugMode( btIDebugDraw::DBG_DrawWireframe | btIDebugDraw::DBG_DrawAabb | btIDebugDraw::DBG_DrawNormals ),//btIDebugDraw::DBG_NoDebug ),
  m_VAO(),
  m_VBO( nullptr ),
  m_DebugLinesDataVertices(),
  m_RenderableDataDescription(),
  m_RenderableDataDescriptionValid( false )
{
    m_VBO = new VBO( sizeof( DebugDataVertex ), VBO::e_DataBufferUsagePatternDynamic );

    m_VBO->AddVertexAttribute(VBO::e_VertexAttrIdxPosition,
                              VBO::e_VertexAttrComponentTypeFloat32,
                              VBO::e_VertexAttrTypeVec3,
                              offsetof( DebugDataVertex, position ) );

    m_VBO->AddVertexAttribute(VBO::e_VertexAttrIdxColor,
                              VBO::e_VertexAttrComponentTypeFloat32,
                              VBO::e_VertexAttrTypeVec3,
                              offsetof( DebugDataVertex, color ) );

    m_VAO.AddVBO( m_VBO );
}

//-----------------------------------------------------------------------

PhysicsDebugDataDrawer::~PhysicsDebugDataDrawer()
{

}

//-----------------------------------------------------------------------

const RenderableObject* PhysicsDebugDataDrawer::GetRenderable()
{
    RenderableObject* retVal = nullptr;

    if( m_DebugLinesDataVertices.size() > 0 )
    {
        if( !m_RenderableDataDescriptionValid )
        {
            m_VBO->BindData( m_DebugLinesDataVertices.size() * sizeof( m_DebugLinesDataVertices[0] ), &(m_DebugLinesDataVertices[0]) );

            m_RenderableDataDescription = RenderableObject( RenderableObject::e_RenderingTypeDirect,
                                                            true,
                                                            0,
                                                            static_cast<unsigned int>( m_DebugLinesDataVertices.size() ),
                                                            0,
                                                            0,
                                                            GraphicsDevice::e_DrawPrimitivesTypeLines,
                                                            &m_VAO );

            m_RenderableDataDescription.SetWorldTransformMatrix( MathUtils::c_IdentityMatrix ); // Location is at the origin, hence the identity matrix

            m_RenderableDataDescriptionValid = true;
        }

        retVal = &m_RenderableDataDescription;
    }

    return retVal;
}

//-----------------------------------------------------------------------

void PhysicsDebugDataDrawer::ClearDebugDataCache()
{
    m_DebugLinesDataVertices.clear();
    m_RenderableDataDescriptionValid = false;
}

//-----------------------------------------------------------------------

void PhysicsDebugDataDrawer::drawLine(const btVector3& from,
                                      const btVector3& to,
                                      const btVector3& color)
{
    DebugDataVertex v1;
    v1.position[0] = from.x();
    v1.position[1] = from.y();
    v1.position[2] = from.z();
    v1.color[0] = color.x();
    v1.color[1] = color.y();
    v1.color[2] = color.z();
    m_DebugLinesDataVertices.push_back( v1 );

    DebugDataVertex v2;
    v2.position[0] = to.x();
    v2.position[1] = to.y();
    v2.position[2] = to.z();
    v2.color[0] = color.x();
    v2.color[1] = color.y();
    v2.color[2] = color.z();
    m_DebugLinesDataVertices.push_back( v2 );
}

//-----------------------------------------------------------------------

void PhysicsDebugDataDrawer::drawContactPoint(const btVector3& UNUSED_PARAM(PointOnB),
                                              const btVector3& UNUSED_PARAM(normalOnB),
                                              btScalar UNUSED_PARAM(distance),
                                              int UNUSED_PARAM(lifeTime),
                                              const btVector3& UNUSED_PARAM(color))
{

}

//-----------------------------------------------------------------------

void PhysicsDebugDataDrawer::reportErrorWarning(const char* UNUSED_PARAM(warningString))
{

}

//-----------------------------------------------------------------------

void PhysicsDebugDataDrawer::draw3dText(const btVector3& UNUSED_PARAM(location),
                                        const char* UNUSED_PARAM(textString))
{

}

//-----------------------------------------------------------------------

void PhysicsDebugDataDrawer::setDebugMode(int debugMode)
{
    m_DebugMode = debugMode;
}

//-----------------------------------------------------------------------

int PhysicsDebugDataDrawer::getDebugMode() const
{
    return m_DebugMode;
}

//-----------------------------------------------------------------------
