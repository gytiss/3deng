#include "RigidBodyComponentWidget.h"
#include <ui_RigidBodyComponentUI.h>
#include <GUI/GUIDefaults.h>
#include <CommonDefinitions.h>
#include <Engine/Scene/RigidBody.h>
#include <Engine/MathUtils.h>


//-----------------------------------------------------------------------

RigidBodyComponentWidget::RigidBodyComponentWidget(CategorizedListWidget* listWidgetToAddOurselvesTo,
                                                   QWidget* parent)
: ComponentWidget( listWidgetToAddOurselvesTo, "Rigid Body", parent ),
  m_UI( new Ui::RigidBodyComponentUI() ),
  m_RigidBodyComponent( nullptr )
{
    m_UI->setupUi( this );

    connect( m_UI->massLineEdit, SIGNAL( editingFinished() ),
             this, SLOT( MassLineEditEditingFinished() ) );

    connect( m_UI->dragLineEdit, SIGNAL( editingFinished() ),
             this, SLOT( DragLineEditEditingFinished() ) );

    connect( m_UI->angularDragLineEdit, SIGNAL( editingFinished() ),
             this, SLOT( AngularDragLineEditEditingFinished() ) );

    connect( m_UI->isKinematicCheckBox, SIGNAL( stateChanged( int ) ),
             this, SLOT( IsKinematicCheckBoxStateChanged( int ) ) );

    connect( m_UI->xFreezePositionCheckBox, SIGNAL( stateChanged( int ) ),
             this, SLOT( XFreezePositionCheckBoxStateChanged( int ) ) );

    connect( m_UI->yFreezePositionCheckBox, SIGNAL( stateChanged( int ) ),
             this, SLOT( YFreezePositionCheckBoxStateChanged( int ) ) );

    connect( m_UI->zFreezePositionCheckBox, SIGNAL( stateChanged( int ) ),
             this, SLOT( zFreezePositionCheckBoxStateChanged( int ) ) );

    connect( m_UI->xFreezeRotationCheckBox, SIGNAL( stateChanged( int ) ),
             this, SLOT( XFreezeRotationCheckBoxStateChanged( int ) ) );

    connect( m_UI->yFreezeRotationCheckBox, SIGNAL( stateChanged( int ) ),
             this, SLOT( YFreezeRotationCheckBoxStateChanged( int ) ) );

    connect( m_UI->zFreezeRotationCheckBox, SIGNAL( stateChanged( int ) ),
             this, SLOT( ZFreezeRotationCheckBoxStateChanged( int ) ) );

    m_UI->massLineEdit->setValidator( &m_FloatValueValidator );
    m_UI->dragLineEdit->setValidator( &m_FloatValueValidator );
    m_UI->angularDragLineEdit->setValidator( &m_FloatValueValidator );
}

//-----------------------------------------------------------------------

RigidBodyComponentWidget::~RigidBodyComponentWidget()
{
    SafeDelete( m_UI );
}

//-----------------------------------------------------------------------

void RigidBodyComponentWidget::SetRigidBodyComponent(RigidBody* rigidBody)
{
    if( rigidBody )
    {
        GUIHelpers::SetNumericLineEditValue( m_UI->massLineEdit, rigidBody->GetMass() );
        GUIHelpers::SetNumericLineEditValue( m_UI->dragLineEdit, rigidBody->GetDrag() );
        GUIHelpers::SetNumericLineEditValue( m_UI->angularDragLineEdit, rigidBody->GetAngularDrag() );

        m_UI->isKinematicCheckBox->setCheckState( rigidBody->IsKinematic() ? Qt::Checked : Qt::Unchecked );

        m_UI->xFreezePositionCheckBox->setCheckState( rigidBody->GetPositionFrozenX() ? Qt::Checked : Qt::Unchecked );
        m_UI->yFreezePositionCheckBox->setCheckState( rigidBody->GetPositionFrozenY() ? Qt::Checked : Qt::Unchecked );
        m_UI->zFreezePositionCheckBox->setCheckState( rigidBody->GetPositionFrozenZ() ? Qt::Checked : Qt::Unchecked );

        m_UI->xFreezeRotationCheckBox->setCheckState( rigidBody->GetRotationFrozenX() ? Qt::Checked : Qt::Unchecked );
        m_UI->yFreezeRotationCheckBox->setCheckState( rigidBody->GetRotationFrozenY() ? Qt::Checked : Qt::Unchecked );
        m_UI->zFreezeRotationCheckBox->setCheckState( rigidBody->GetRotationFrozenZ() ? Qt::Checked : Qt::Unchecked );
    }

    m_RigidBodyComponent = rigidBody;
}

//-----------------------------------------------------------------------

void RigidBodyComponentWidget::ResetPaletteToDefault()
{
    m_UI->rigidBodyComponentContentWidget->setPalette( GUIDefaults::GetSingleton().DefaultPalette() );
}

//-----------------------------------------------------------------------

void RigidBodyComponentWidget::MassLineEditEditingFinished()
{
    if( m_RigidBodyComponent )
    {
        m_RigidBodyComponent->SetMass( GUIHelpers::GetNumericLineEditValue( m_UI->massLineEdit ) );
    }
}

//-----------------------------------------------------------------------

void RigidBodyComponentWidget::DragLineEditEditingFinished()
{
    if( m_RigidBodyComponent )
    {
        m_RigidBodyComponent->SetDrag( GUIHelpers::GetNumericLineEditValue( m_UI->dragLineEdit ) );
    }
}

//-----------------------------------------------------------------------

void RigidBodyComponentWidget::AngularDragLineEditEditingFinished()
{
    if( m_RigidBodyComponent )
    {
        m_RigidBodyComponent->SetAngularDrag( GUIHelpers::GetNumericLineEditValue( m_UI->angularDragLineEdit ) );
    }
}

//-----------------------------------------------------------------------

void RigidBodyComponentWidget::IsKinematicCheckBoxStateChanged(int state)
{
    if( m_RigidBodyComponent )
    {
        m_RigidBodyComponent->SetIsKinematic( ( state == Qt::Checked ) ? true : false );
    }
}

//-----------------------------------------------------------------------

void RigidBodyComponentWidget::XFreezePositionCheckBoxStateChanged(int state)
{
    if( m_RigidBodyComponent )
    {
        m_RigidBodyComponent->SetPositionFrozenX( ( state == Qt::Checked ) ? true : false );
    }
}

//-----------------------------------------------------------------------

void RigidBodyComponentWidget::YFreezePositionCheckBoxStateChanged(int state)
{
    if( m_RigidBodyComponent )
    {
        m_RigidBodyComponent->SetPositionFrozenY( ( state == Qt::Checked ) ? true : false );
    }
}

//-----------------------------------------------------------------------

void RigidBodyComponentWidget::ZFreezePositionCheckBoxStateChanged(int state)
{
    if( m_RigidBodyComponent )
    {
        m_RigidBodyComponent->SetPositionFrozenZ( ( state == Qt::Checked ) ? true : false );
    }
}

//-----------------------------------------------------------------------

void RigidBodyComponentWidget::XFreezeRotationCheckBoxStateChanged(int state)
{
    if( m_RigidBodyComponent )
    {
        m_RigidBodyComponent->SetRotationFrozenX( ( state == Qt::Checked ) ? true : false );
    }
}

//-----------------------------------------------------------------------

void RigidBodyComponentWidget::YFreezeRotationCheckBoxStateChanged(int state)
{
    if( m_RigidBodyComponent )
    {
        m_RigidBodyComponent->SetRotationFrozenY( ( state == Qt::Checked ) ? true : false );
    }
}

//-----------------------------------------------------------------------

void RigidBodyComponentWidget::ZFreezeRotationCheckBoxStateChanged(int state)
{
    if( m_RigidBodyComponent )
    {
        m_RigidBodyComponent->SetRotationFrozenZ( ( state == Qt::Checked ) ? true : false );
    }
}

//-----------------------------------------------------------------------
