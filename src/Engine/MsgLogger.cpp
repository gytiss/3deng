#include "MsgLogger.h"


#ifdef OS_ANDROID
    #include <SDL2/SDL.h>
#else
    #include <iostream>
#endif
//-----------------------------------------------------------------------

#ifdef OS_ANDROID
template<void (*SDLLogFuncAlias)(int, const char*, ...)>
static void AndroidLog(const std::string& msg)
{
    static const size_t c_MaxAndroidLogMessageLength = ( 3u * 1024u );

    if( msg.length() > c_MaxAndroidLogMessageLength ) // The message will have to be split up
    {
        const size_t numberOfParts = ( msg.length() / c_MaxAndroidLogMessageLength );
        const size_t leftover = ( msg.length() - ( numberOfParts * c_MaxAndroidLogMessageLength ) );

        for(size_t i = 0; i < numberOfParts; i++)
        {
            std::string part = std::string( msg, ( i * c_MaxAndroidLogMessageLength ), c_MaxAndroidLogMessageLength );
            (SDLLogFuncAlias)( SDL_LOG_CATEGORY_APPLICATION, "%s", part.c_str() );
        }

        if( leftover > 0 )
        {
            std::string theRest = std::string( msg, ( numberOfParts * c_MaxAndroidLogMessageLength ), leftover );
            (SDLLogFuncAlias)( SDL_LOG_CATEGORY_APPLICATION, "%s", theRest.c_str() );
        }
    }
    else
    {
        (SDLLogFuncAlias)( SDL_LOG_CATEGORY_APPLICATION, "%s", msg.c_str()  );
    }
};
#endif

//-----------------------------------------------------------------------

void MsgLogger::log(MsgType mt, std::string msg)
{
    switch( mt )
    {
        case ERROR_MSG:
        {
            #ifdef OS_ANDROID
                AndroidLog<SDL_LogError>( msg );
            #else
                std::cerr << "Error: " << msg << std::endl;
            #endif
            break;
        }

        case WARNING_MSG:
        {
            #ifdef OS_ANDROID
                AndroidLog<SDL_LogWarn>( msg );
            #else
                std::cerr << "Warning: " << msg << std::endl;
            #endif
            break;
        }

        case EVENT_MSG:
        {
            #ifdef OS_ANDROID
                AndroidLog<SDL_LogInfo>( msg );
            #else
                std::cout << msg << std::endl;
            #endif
            break;
        }

        default:
        {
            log( MsgLogger::ERROR_MSG, "invalid message type supplied to the MsgLogger." );
            break;
        }
    }
}

//-----------------------------------------------------------------------
