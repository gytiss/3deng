#include "GUIFileInterface.h"
#include <cstdio>

GUIFileInterface::GUIFileInterface(const Rocket::Core::String& root)
: m_RootDir( root )
{
}

GUIFileInterface::~GUIFileInterface()
{
}

// Opens a file.
Rocket::Core::FileHandle GUIFileInterface::Open(const Rocket::Core::String& path)
{
    // Attempt to open the file relative to the application's m_RootDir.
    FILE* fp = fopen((m_RootDir + "/" + path).CString(), "rb");
    if (fp != NULL)
        return (Rocket::Core::FileHandle) fp;

    // Attempt to open the file relative to the current working directory.
    fp = fopen(path.CString(), "rb");
    return (Rocket::Core::FileHandle) fp;
}

// Closes a previously opened file.
void GUIFileInterface::Close(Rocket::Core::FileHandle file)
{
    fclose((FILE*) file);
}

// Reads data from a previously opened file.
size_t GUIFileInterface::Read(void* buffer, size_t size, Rocket::Core::FileHandle file)
{
    return fread(buffer, 1, size, (FILE*) file);
}

// Seeks to a point in a previously opened file.
bool GUIFileInterface::Seek(Rocket::Core::FileHandle file, long offset, int origin)
{
    return fseek((FILE*) file, offset, origin) == 0;
}

// Returns the current position of the file pointer.
size_t GUIFileInterface::Tell(Rocket::Core::FileHandle file)
{
    return ftell((FILE*) file);
}
