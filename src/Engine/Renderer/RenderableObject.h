#ifndef RENDERABLEOBJECT_H_INCLUDED
#define RENDERABLEOBJECT_H_INCLUDED

#include <glm/glm.hpp>
#include <EngineTypes.h>
#include <Resources/MaterialManager.h>
#include <GraphicsDevice/VAO.h>
#include <GraphicsDevice/GraphicsDevice.h>
#include <CommonDefinitions.h>

class RenderableObject
{
public:
    enum RenderingType
    {
        e_RenderingTypeInvalid = 0,
        e_RenderingTypeDirect = 1,
        e_RenderingTypeIndexed = 2
    };

    /**
     * @param renderingType - Rendering type i.e. indexed or direct
     * @param firstVertexIndex - Index of the first vertex in the vertex buffer
     * @param vertexIndexOrVertexCount - If rendering mode is "e_RenderingTypeIndexed" represents the total number of indices otherwise represent the total number of vertices
     * @param minVertexIndexValue - Minimum value stored in the index buffer (Ignored if mode is "e_RenderingTypeDirect")
     * @param maxVertexIndexValue - Maximum value stored in the index buffer (Ignored if mode is "e_RenderingTypeDirect")
     * @param drawPrimitivesType - What kind of primitives are represented by the supplied vertices e.g.: "e_DrawPrimitivesTypeTriangles", "e_DrawPrimitivesTypeLines" etc.
     * @param vao - Vertex buffer object containing vertex data to be rendered
     * @param materialHandle - Handle to the material used by a shader when rendering this renderable (set to nullptr if renderable has no material)
     * @param boneMatrices - Pointer to an array of bone matrices if this renderable needs to be skinned nullptr otherwise
     */
    RenderableObject(RenderingType renderingType,
                     bool renderWithDepthTesting,
                     unsigned int firstVertexIndex,
                     unsigned int vertexIndexOrVertexCount,
                     EngineTypes::VertexIndexType minVertexIndexValue,
                     EngineTypes::VertexIndexType maxVertexIndexValue,
                     GraphicsDevice::DrawPrimitivesType drawPrimitivesType,
                     const VAO* vao,
                     MaterialManager::MaterialHandle* materialHandle = nullptr,
                     const EngineTypes::MatrixArray* boneMatrices = nullptr)
    : m_RenderingType( renderingType ),
      m_RenderWithDepthTesting( renderWithDepthTesting ),
      m_FirstVertexIndex( firstVertexIndex ),
      m_VertexCount( ( renderingType == e_RenderingTypeDirect ) ? vertexIndexOrVertexCount : 0 ),
      m_VertexIndexCount( ( renderingType == e_RenderingTypeIndexed ) ? vertexIndexOrVertexCount : 0 ),
      m_MinVertexIndexValue( ( renderingType == e_RenderingTypeIndexed ) ? minVertexIndexValue : 0 ),
      m_MaxVertexIndexValue( ( renderingType == e_RenderingTypeIndexed ) ? maxVertexIndexValue : 0 ),
      m_VAO( vao ),
      m_MaterialHandle(),
      m_BoneMatrices( boneMatrices ),
      m_WorldTransformMatrix( MathUtils::c_IdentityMatrix ),
      m_DrawPrimitivesType( drawPrimitivesType ),
      m_MaterialIsValid( ( materialHandle != nullptr ) ? true : false )
    {
        assert( ( renderingType != e_RenderingTypeInvalid ) && "Rendering type cannot be invalid" );

        if( m_MaterialIsValid )
        {
            m_MaterialHandle = *materialHandle;
        }
    }

    RenderableObject()
    : m_RenderingType( e_RenderingTypeInvalid ),
      m_RenderWithDepthTesting( false ),
      m_FirstVertexIndex( 0 ),
      m_VertexCount( 0 ),
      m_VertexIndexCount( 0 ),
      m_MinVertexIndexValue( 0 ),
      m_MaxVertexIndexValue( 0 ),
      m_VAO( nullptr ),
      m_MaterialHandle(),
      m_BoneMatrices( nullptr ),
      m_WorldTransformMatrix( MathUtils::c_IdentityMatrix ),
      m_DrawPrimitivesType(),
      m_MaterialIsValid( false )
    {
    }

    RenderableObject& operator= (const RenderableObject& other)
    {
        m_RenderingType = other.m_RenderingType;
        m_RenderWithDepthTesting = other.m_RenderWithDepthTesting;
        m_FirstVertexIndex = other.m_FirstVertexIndex;
        m_VertexCount = other.m_VertexCount;
        m_VertexIndexCount = other.m_VertexIndexCount;
        m_MinVertexIndexValue = other.m_MinVertexIndexValue;
        m_MaxVertexIndexValue = other.m_MaxVertexIndexValue;
        m_VAO = other.m_VAO;
        m_MaterialHandle = other.m_MaterialHandle;
        m_BoneMatrices = other.m_BoneMatrices;
        m_WorldTransformMatrix = other.m_WorldTransformMatrix;
        m_DrawPrimitivesType = other.m_DrawPrimitivesType;
        m_MaterialIsValid = other.m_MaterialIsValid;

        return *this;
    }

    RenderableObject(const RenderableObject& other)
    {
        operator= ( other );
    }

    inline RenderingType GetRenderingType() const
    {
        return m_RenderingType;
    }

    inline GraphicsDevice::DrawPrimitivesType GetDrawPrimitivesType() const
    {
        return m_DrawPrimitivesType;
    }

    inline VBO::VertexAttrMask GetVertexAtributes() const
    {
        return m_VAO->GetVertexAtributes();
    }

    inline unsigned int GetFirstVertexIndex() const
    {
        return m_FirstVertexIndex;
    }

    /**
     * @return Vertex count or zero if this renderable is not using direct rendering
     */
    inline unsigned int GetVertexCount() const
    {
        return m_VertexCount;
    }

    /**
     * @return Vertex index count or zero if this renderable is not using indexed rendering
     */
    inline unsigned int GetVertexIndexCount() const
    {
        return m_VertexIndexCount;
    }

    /**
     * @return Minimum vertex index value in the index buffer or zero if this renderable is not using indexed rendering
     */
    inline EngineTypes::VertexIndexType GetMinVertexIndexValue() const
    {
        return m_MinVertexIndexValue;
    }

    /**
     * @return Maximum vertex index value in the index buffer or zero if this renderable is not using indexed rendering
     */
    inline EngineTypes::VertexIndexType GetMaxVertexIndexValue() const
    {
        return m_MaxVertexIndexValue;
    }

    inline void SetWorldTransformMatrix(const glm::mat4& mat )
    {
        m_WorldTransformMatrix = mat;
    }

    inline const glm::mat4& GetWorldTransformMatrix() const
    {
        return m_WorldTransformMatrix;
    }

    const EngineTypes::MatrixArray* GetBoneMatrices() const
    {
        return m_BoneMatrices;
    }

    void BindForRendering()
    {
        m_VAO->Bind();

        if( m_MaterialIsValid )
        {
            m_MaterialHandle->Activate();
        }
    }

    inline bool GetRenderWithDepthTesting() const
    {
        return m_RenderWithDepthTesting;
    }

    inline bool IsAnimated() const
    {
        return ( m_BoneMatrices != nullptr );
    }

    const EngineTypes::SRGBColor& GetDiffuseColor() const
    {
        assert( m_MaterialIsValid && "RenderableObject has no valid material" );
        return m_MaterialHandle->GetDiffuseColor();
    }

    const EngineTypes::SRGBColor& GetSpecularColor() const
    {
        assert( m_MaterialIsValid && "RenderableObject has no valid material" );
        return m_MaterialHandle->GetSpecularColor();
    }

    uint8_t GetGlossiness() const
    {
        assert( m_MaterialIsValid && "RenderableObject has no valid material" );
        return m_MaterialHandle->GetGlossiness();
    }

    bool IsTransparent() const
    {
        // TODO
        return false;
    }

    bool HasDiffuseColor() const
    {
        return ( m_MaterialIsValid && m_MaterialHandle->HasDiffuseColor() );
    }

    bool HasSpecularColor() const
    {
        return ( m_MaterialIsValid && m_MaterialHandle->HasSpecularColor() );
    }

    bool HasGlossiness() const
    {
        return ( m_MaterialIsValid && m_MaterialHandle->HasGlossiness() );
    }

    bool HasDiffuseMap() const
    {
        return ( m_MaterialIsValid && m_MaterialHandle->HasDiffuseTextureMap() );
    }

    bool HasNormalMap() const
    {
        return ( m_MaterialIsValid && m_MaterialHandle->HasNormalTextureMap() );
    }

    bool HasSpecularMap() const
    {
        return ( m_MaterialIsValid && m_MaterialHandle->HasSpecularTextureMap() );
    }

    bool HasGlossMap() const
    {
        return ( m_MaterialIsValid && m_MaterialHandle->HasGlossTextureMap() );
    }

private:
    RenderingType m_RenderingType;
    bool m_RenderWithDepthTesting;
    unsigned int m_FirstVertexIndex;
    unsigned int m_VertexCount;
    unsigned int m_VertexIndexCount;
    EngineTypes::VertexIndexType m_MinVertexIndexValue;
    EngineTypes::VertexIndexType m_MaxVertexIndexValue;
    const VAO* m_VAO;
    MaterialManager::MaterialHandle m_MaterialHandle;
    const EngineTypes::MatrixArray* m_BoneMatrices;
    glm::mat4 m_WorldTransformMatrix;
    GraphicsDevice::DrawPrimitivesType m_DrawPrimitivesType;
    bool m_MaterialIsValid;
};

#endif // RENDERABLEOBJECT_H_INCLUDED
