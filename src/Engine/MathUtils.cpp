#include "MathUtils.h"

//-----------------------------------------------------------------------

const glm::quat MathUtils::c_IdentityQuaternion = glm::quat( 1.0f, 0.0f, 0.0f, 0.0f ); // W = 1, X = 0, Y = 0, Z = 0;
const glm::mat4 MathUtils::c_IdentityMatrix = glm::mat4( 1.0f );
const SceneTypes::Transform MathUtils::c_DefaultTransform = { glm::vec3( 0.0f ), c_IdentityQuaternion, glm::vec3( 1.0f ) };

//-----------------------------------------------------------------------

void MathUtils::CoordinateInScreenSpaceToRay(float xInScreenSpace,
                                             float yInScreenSpace,
                                             size_t screenWidthInPixels,
                                             size_t screenHeightInPixels,
                                             EngineTypes::VPTransformations& viewProjectionTransform,
                                             const glm::vec3& currentCameraPositionInWorldSpace,
                                             glm::vec3& outRayDirection)
{
    // Note: NDC is "Normalized Device Coordinates"
    const float mouseXInNDCSpace = ( ( ( 2.0f * xInScreenSpace ) / screenWidthInPixels ) - 1.0f );
    const float mouseYInNDCSpace = ( 1.0f - ( ( 2.0f * yInScreenSpace ) / screenHeightInPixels ) );

    const glm::mat4 inverseViewProjectionMatrix = glm::inverse( viewProjectionTransform.projectionMatrix * viewProjectionTransform.viewMatrix );

    const glm::vec4 mouseCoordsInNDCSpace( mouseXInNDCSpace,
                                           mouseYInNDCSpace,
                                           -1.0, // In NDC space OpenGL uses left-handed coordinate system, hence the near plane maps to "Z = -1"
                                            1.0 );

    glm::vec4 mouseCoordsInWorldSpace = ( inverseViewProjectionMatrix * mouseCoordsInNDCSpace );
    mouseCoordsInWorldSpace /= mouseCoordsInWorldSpace.w;

    const glm::vec3 rayOrigin = currentCameraPositionInWorldSpace;
    outRayDirection = glm::normalize( glm::vec3( mouseCoordsInWorldSpace ) - rayOrigin );
}

//-----------------------------------------------------------------------
