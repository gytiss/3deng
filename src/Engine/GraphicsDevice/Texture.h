#ifndef TEXTURE_H_INCLUDED
#define TEXTURE_H_INCLUDED

#include <OpenGLIncludes.h>
#include <MathUtils.h>
#include <imgutl/ImageUtils.h>

class MTC;
class FBO;
class GraphicsDevice;

class Texture
{
public:
    friend class FBO;

    enum TypeIdx
    {
        e_TypeIdxGBufferVertexPossitionMapViewSpace = 0,
        e_TypeIdxGBufferDiffuseMap = 1,
        e_TypeIdxGBufferVertexNormalMapViewSpace,
        e_TypeIdxDiffuseMap,
        e_TypeIdxNormalMap,
        e_TypeIdxSpecularMap,
        e_TypeIdxGlossMap,
        e_TypeIdxDepthTexture,
        e_TypeIdxPostProcessTextureMap,

        e_TypeIdxCount // Not a texture type index (Used for counting) (Note must not exceed GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS i.e. 32 for OpenGL ES 3.0)
    };

    enum Type
    {
        e_TypeGBufferVertexPossitionMapViewSpace = MathUtils::GetBit<e_TypeIdxGBufferVertexPossitionMapViewSpace>::Result,
        e_TypeGBufferDiffuseMap = MathUtils::GetBit<e_TypeIdxGBufferDiffuseMap>::Result,
        e_TypeGBufferVertexNormalMapViewSpace = MathUtils::GetBit<e_TypeIdxGBufferVertexNormalMapViewSpace>::Result,
        e_TypeDiffuseMap = MathUtils::GetBit<e_TypeIdxDiffuseMap>::Result,
        e_TypeNormalMap = MathUtils::GetBit<e_TypeIdxNormalMap>::Result,
        e_TypeSpecularMap = MathUtils::GetBit<e_TypeIdxSpecularMap>::Result,
        e_TypeGlossMap = MathUtils::GetBit<e_TypeIdxGlossMap>::Result,
        e_TypeDepthTexture = MathUtils::GetBit<e_TypeIdxDepthTexture>::Result,
        e_TypePostProcessTextureMap = MathUtils::GetBit<e_TypeIdxPostProcessTextureMap>::Result
    };

    enum Class
    {
        e_Class2DTexture = GL_TEXTURE_2D,
        e_Class3DTexture = GL_TEXTURE_3D,
        e_Class2DArrayTexture = GL_TEXTURE_2D_ARRAY,
        e_ClassCubeMapTexture = GL_TEXTURE_CUBE_MAP
    };

    enum DataClass
    {
        e_DataClass2DTexture = GL_TEXTURE_2D,
        e_DataClassCubeMapTexturePositiveX = GL_TEXTURE_CUBE_MAP_POSITIVE_X,
        e_DataClassCubeMapTextureNegativeX = GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
        e_DataClassCubeMapTexturePositiveY = GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
        e_DataClassCubeMapTextureNegativeY = GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
        e_DataClassCubeMapTexturePositiveZ = GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
        e_DataClassCubeMapTextureNegativeZ = GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
    };

    enum StateParam
    {
        e_StateParamMinifyingFilter = 0,
        e_StateParamMagnifyingFilter = 1,
        e_StateParamSwizzleRed,
        e_StateParamSwizzleGreen,
        e_StateParamSwizzleBlue,
        e_StateParamSwizzleAlpha,
        e_StateParamSCoordWrapType,
        e_StateParamTCoordWrapType,
        e_StateParamRCoordWrapType,

        e_StateParamCount // Not a texture parameter (Used for counting)
    };

    enum IntParam
    {
        e_IntParamLowestDefinedMipmapLevel = 0,
        e_IntParamHighestDefinedMipmapLevel = 1,

        e_IntParamCount
    };

    enum FloatParam
    {
        e_FloatParamMinLOD = 0,
        e_FloatParamMaxLOD = 1,
        e_MaxAnisotropyLevelToUse,

        e_FloatParamCount // Not a texture parameter (Used for counting)
    };

    enum StateParamArg
    {
        e_StateParamArgMinifingFilterNearest = GL_NEAREST,
        e_StateParamArgMinifingFilterLinear = GL_LINEAR,
        e_StateParamArgMinifingFilterNearestMipmapNearest = GL_NEAREST_MIPMAP_NEAREST,
        e_StateParamArgMinifingFilterNearestMipmapLinear = GL_NEAREST_MIPMAP_LINEAR,
        e_StateParamArgMinifingFilterLinearMipmapNearest = GL_LINEAR_MIPMAP_NEAREST,
        e_StateParamArgMinifingFilterLinearMipmapLinear = GL_LINEAR_MIPMAP_LINEAR,

        e_StateParamArgMagnifyingFilterNearest = GL_NEAREST,
        e_StateParamArgMagnifyingFilterLinear = GL_LINEAR,

        e_StateParamArgSwizzleRed = GL_RED,
        e_StateParamArgSwizzleGreen = GL_GREEN,
        e_StateParamArgSwizzleBlue = GL_BLUE,
        e_StateParamArgSwizzleAlpha = GL_ALPHA,
        e_StateParamArgSwizzleZero = GL_ZERO,
        e_StateParamArgSwizzleOne = GL_ONE,

        e_StateParamArgSCoordWrapClampToEdge = GL_CLAMP_TO_EDGE,
        e_StateParamArgSCoordWrapMirroredRepeat = GL_MIRRORED_REPEAT,
        e_StateParamArgSCoordWrapRepeat = GL_REPEAT,

        e_StateParamArgTCoordWrapClampToEdge = GL_CLAMP_TO_EDGE,
        e_StateParamArgTCoordWrapMirroredRepeat = GL_MIRRORED_REPEAT,
        e_StateParamArgTCoordWrapRepeat = GL_REPEAT,

        e_StateParamArgRCoordWrapClampToEdge = GL_CLAMP_TO_EDGE,
        e_StateParamArgRCoordWrapMirroredRepeat = GL_MIRRORED_REPEAT,
        e_StateParamArgRCoordWrapRepeat = GL_REPEAT
    };

    enum FilteringMode
    {
        e_FilteringModeNoFiltering = 0,
        e_FilteringModeBilinear = 1,
        e_FilteringModeTrilinear
    };

    /**
     * @param graphicsDevice - Interface to the graphics device
     * @param textureTypeIdx - Index of the texture type
     * @param textureClass - Texture class
     * @param isMipmapped - Whether the texture should use mipmapping or not
     */
    Texture(GraphicsDevice& graphicsDevice,
            TypeIdx textureTypeIdx,
            Class textureClass,
            bool isMipmapped = true);

    virtual ~Texture();

    void SetAnisotropyLevel(float anisotropyLevel);

    /**
     * @param dataClass - Texture type i.e. 2D, CUBE map positive X etc.
     * @param imgData - Image which will be stored in this texture
     * @param dataIsSRGBEncoded - A flag specifying if the pixel color data in this texture is encoded as SRGB
     */
    void SetData(DataClass dataClass, const ImageUtils::ImageData_t& imgData, bool dataIsSRGBEncoded = true);

    /**
     * @param imgData - Image which will be stored in this texture
     * @param dataIsSRGBEncoded - A flag specifying if the pixel color data in this texture is encoded as SRGB
     */
    void SetData(const MTC& imgData, bool dataIsSRGBEncoded = true);

    void SetFilteringMode(FilteringMode mode);

    void SetStateParameter(StateParam param, StateParamArg arg);
    void SetIntegerParameter(IntParam param, int arg);
    void SetFloatParameter(FloatParam param, float arg);

    StateParamArg GetStateParameterVal(StateParam param) const;
    int GetIntegerParameterVal(IntParam param) const;
    float GetFloatParameterVal(FloatParam param) const;

    /**
     * Binds texture to the appropriate texture unit
     */
    void Activate(bool forceActivation = false);

    /**
     * Invalidate any cached activation state stored in the Texture object.
     *
     * WARNING: This function has to be called before Activate() in cases where functions
     *          glActiveTexture() and/or glBindTexture() are called manually in other 
     *          parts of the code. 
     */
    static void InvalidateCashedActivationState();

    static inline const char* GetSamplerUniformName(TypeIdx textureType)
    {
        return m_TypeIdxToSamplerShaderUniformNameMap[textureType];
    }

    inline Type GetType() const
    {
        return m_TypeIdxToTypeMap[m_TypeIdx];
    }

    inline TypeIdx GetTypeIndex() const
    {
        return m_TypeIdx;
    }

    inline Class GetClass() const
    {
        return m_Class;
    }

    inline bool IsMipmapped() const
    {
        return m_IsMipmapped;
    }

protected:
    enum InternalDataFormat
    {
        e_InternalDataFormatRGB8 = GL_RGB8,
        e_InternalDataFormatRGBA8 = GL_RGBA8,
        e_InternalDataFormatSRGB8 = GL_SRGB8,
        e_InternalDataFormatSRGB8Alpha8 = GL_SRGB8_ALPHA8,
        e_InternalDataFormatRGB10Alpha2 = GL_RGB10_A2,
        e_InternalDataFormatRGB16F = GL_RGB16F,
        e_InternalDataFormatRGBA16F = GL_RGBA16F,
        e_InternalDataFormatRGB16UI = GL_RGB16UI,
        e_InternalDataFormatRGBA16UI = GL_RGBA16UI,
        e_InternalDataFormatRGB32F = GL_RGB32F,
        e_InternalDataFormatRGBA32F = GL_RGBA32F,
        e_InternalDataFormatRGB32UI = GL_RGB32UI,
        e_InternalDataFormatRGBA32UI = GL_RGBA32UI,
        e_InternalDataFormatDepth32Stencil8 = GL_DEPTH32F_STENCIL8,

        e_InternalDataFormatRGB8ETC2 = GL_COMPRESSED_RGB8_ETC2,
        e_InternalDataFormatSRGB8ETC2 = GL_COMPRESSED_SRGB8_ETC2,
        e_InternalDataFormatRGB8Alpha1ETC2 = GL_COMPRESSED_RGB8_PUNCHTHROUGH_ALPHA1_ETC2,
        e_InternalDataFormatSRGB8Alpha1ETC2 = GL_COMPRESSED_SRGB8_PUNCHTHROUGH_ALPHA1_ETC2,
        e_InternalDataFormatRGBA8ETC2EAC = GL_COMPRESSED_RGBA8_ETC2_EAC,
        e_InternalDataFormatSRGBA8ETC2EAC = GL_COMPRESSED_SRGB8_ALPHA8_ETC2_EAC,

        e_InternalDataFormatInvalid = 0
    };

    /**
     * Resets the state of this texture object making it possible to call InitializeStorage() or SetData() functions again
     */
    void Reset();

    /**
     * @param internalFormat - Format in which the data will be stored in the GPU
     * @param textureWidth - Texture image width in pixels
     * @param textureHeight - Texture image height in pixels
     *
     * @return Number of mipmap levels which will be generated for this texture (1 for non mipmapped textures)
     */
    size_t InitializeStorage(InternalDataFormat internalFormat,
                             size_t textureWidth,
                             size_t textureHeight);

    GLuint GetOpenGLInternalTextureID() const
    {
        return m_OpenGLTextureId;
    }

private:
    // Used to prevent copying
    Texture& operator = (const Texture& other);
    Texture(const Texture& other);

    void _Bind(bool forceBind);
    bool _IsTextureNotAlreadyBound() const;
    GLenum _GetImageDataClass(const MTC& imgData) const;
    InternalDataFormat _GetImageInternlFormat(const MTC& imgData, bool& outCompressed) const;
    GLenum _GetImageColourFormat(const MTC& imgData) const;
    GLenum _GetImageColourFormat(const ImageUtils::ImageData_t& imgData) const;
    void _TurnOffAllFilteringTheTexture();
    void _TurnOnBilinearFilteringForTheTexture();
    void _TurnOnTrilinearFilteringForTheTexture();
    void _InitialSetup();
    GLuint _CreateOpenGLTextureObject();
    void _DeleteOpenGLTextureObject(GLuint& openGLTextureID);

    static const GLuint c_InvalidOpenGLTextureId = 0;

    GraphicsDevice& m_GraphicsDevice;
    const TypeIdx m_TypeIdx;
    const Class m_Class;
    const bool m_IsMipmapped;
    const bool m_AnisotropicFilteringAvailable;
    bool m_TextureObjectHasBeenInitialised;
    bool m_ImageDataHasBeenSet;
    float m_CurrentAnisotropyLevel;
    GLuint m_OpenGLTextureId;
    FilteringMode m_FilteringMode;

    GLint m_CurrentTextureStateParamValues[e_StateParamCount];
    int m_CurrentTextureIntParamValues[e_IntParamCount];
    float m_CurrentTextureFloatParamValues[e_FloatParamCount];

    static const Type m_TypeIdxToTypeMap[e_TypeIdxCount];
    static const char* m_TypeIdxToSamplerShaderUniformNameMap[e_TypeIdxCount];
    static const GLenum m_TextureStateParamToOpenGLTextureParamMap[e_StateParamCount];
    static const GLenum m_TextureIntParamToOpenGLTextureParamMap[e_IntParamCount];
    static const GLenum m_TextureFloatParamToOpenGLTextureParamMap[e_FloatParamCount];
    static const GLint m_DefaultTextureStateParamValues[e_StateParamCount];
    static const int m_DefaultTextureIntParamValues[e_IntParamCount];
    static const float m_DefaultTextureFloatParamValues[e_FloatParamCount];

    static GLuint m_CurrentlyBoundTextureId; // This variable is used by all instances of the "Texture" class
    static GLuint m_TexturesActiveOnTextureUnits[e_TypeIdxCount];
};

#endif // TEXTURE_H_INCLUDED
