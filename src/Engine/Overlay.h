#ifndef TEXTOVERLAY_H
#define TEXTOVERLAY_H

#include "DrawableEntity.h"
#include <Resources/TextureManager.h>

class GraphicsDevice;
class FileManager;

class Overlay : public DrawableEntity
{
public:
    Overlay(GraphicsDevice& graphicsDevice,
            TextureManager& textureManager,
            FileManager& fileManager,
            std::string textTextureFileName,
            size_t drawableSurfaceWidth,
            size_t drawableSurfaceHeight);

    Overlay(GraphicsDevice& graphicsDevice,
            TextureManager& textureManager,
            FileManager& fileManager,
            size_t drawableSurfaceWidth,
            size_t drawableSurfaceHeight);

    virtual ~Overlay();
    virtual void render(ShaderProg* shaderProgToUse_1, ShaderProg* shaderProgToUse_2, const DrawableEntity::Transformation * t);
//    void setCustomOverlayTexture(GLuint textureID,glm::vec4 lightPos)
//    {
//        m_CustomTexture=textureID;
//        m_CustomTextureSet=true;
//        m_lightPos=lightPos;
//    }

private:
    ModelBinObj::Vertex* generateFullScreenQuad(float width, float height);
    void prepareFullScrenQuadRenderingContext();
    void _LoadTexture(const std::string& texturePath);

    GraphicsDevice& m_GraphicsDevice;
    TextureManager& m_TextureManager;
    TextureManager::TextureHandle m_FullScreenQuadTextureHandle;
    DrawableEntity::Texture_t m_FullScreenQuadTexture;
    ModelBinObj::Vertex* m_FullScreenQuadVertexes;
    float m_drawableSurfaceWidth;
    float m_drawableSurfaceHeight;

//    TextureManager::TextureHandle m_CustomTextureHandle;
    bool m_CustomTextureSet;
    bool m_RenderFromDefaultTexture;
    glm::vec4 m_lightPos;
};

#endif // TEXTOVERLAY_H
