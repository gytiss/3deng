

in vec3 in_VertexPos;
in vec3 in_VertexNormal;
in vec3 in_VertexBiTangent;
in vec3 in_VertexTangent;
in vec2 in_VertexTextureCoords;

#if defined(RENDERING_SKINNED_MESH)
    in vec4 in_VertexBoneWeights;
    in vec4 in_VertexBoneIndices;
#endif

// Output data ; will be interpolated for each fragment.
out vec2 TexCoord;
out mat3 TBN;

uniform mat4 u_ModelViewProjectionMatrix;
uniform mat4 u_NormalMatrix; // Inverse transpose of u_ModelViewMatrix

#if defined(RENDERING_SKINNED_MESH)
    uniform mat4 u_JointMatrices[58]; // All bones come in a huge list!

    void VertexSkinning(const vec3 inPosition,
                        const vec3 inNormal,
                        const vec3 inBiTangent,
                        const vec3 inTangent,

                        inout vec4 objPos,
                        inout vec4 objNormal,
                        inout vec4 objBiTangent,
                        inout vec4 objTangent)
    {
        mat4 matTransform = u_JointMatrices[int(in_VertexBoneIndices.x)] * in_VertexBoneWeights.x;
        if (in_VertexBoneWeights.x > 0.0f)
        {
            if (in_VertexBoneWeights.y > 0.0f)
            {
                matTransform += u_JointMatrices[int(in_VertexBoneIndices.y)] * in_VertexBoneWeights.y;
                if (in_VertexBoneWeights.z > 0.0f)
                {
                    matTransform += u_JointMatrices[int(in_VertexBoneIndices.z)] * in_VertexBoneWeights.z;
                    float finalWeight = 1.0f - ( in_VertexBoneWeights.x + in_VertexBoneWeights.y + in_VertexBoneWeights.z );
                    matTransform += u_JointMatrices[int(in_VertexBoneIndices.w)] * finalWeight;
                }
            }
        }

        objPos =  matTransform * vec4( inPosition, 1.0f );
        objNormal = matTransform * vec4( inNormal, 0.0f );
        objBiTangent = matTransform * vec4( inBiTangent, 0.0f );
        objTangent = matTransform * vec4( inTangent, 0.0f );
    }
#endif

void main()
{
    vec4 objPos = vec4( 0.0f, 0.0f, 0.0f, 1.0f );
    vec4 objNormal = vec4( 0.0f );
    vec4 objBiTangent = vec4( 0.0f );
    vec4 objTangent = vec4( 0.0f );

    #if defined(RENDERING_SKINNED_MESH)
        VertexSkinning( in_VertexPos,
                        in_VertexNormal,
                        in_VertexBiTangent,
                        in_VertexTangent,
                        objPos,
                        objNormal,
                        objBiTangent,
                        objTangent );
    #else
        objPos = vec4( in_VertexPos, 1.0f );
        objNormal = vec4( in_VertexNormal, 0.0f );
        objBiTangent = vec4( in_VertexBiTangent, 0.0f );
        objTangent = vec4( in_VertexTangent, 0.0f );
    #endif

    gl_Position = u_ModelViewProjectionMatrix * objPos;

    vec3 Normal_ViewSpace = normalize( ( u_NormalMatrix * objNormal ).xyz );
    vec3 Tangent_ViewSpace = normalize( ( u_NormalMatrix * objTangent ).xyz );
    vec3 BiTangent_ViewSpace = normalize( ( u_NormalMatrix * objBiTangent ).xyz );

    TBN = mat3( Tangent_ViewSpace, BiTangent_ViewSpace, Normal_ViewSpace );

    TexCoord = in_VertexTextureCoords;
}
