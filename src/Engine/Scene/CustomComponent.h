#ifndef CUSTOMCOMPONENT_H_INCLUDED
#define CUSTOMCOMPONENT_H_INCLUDED

#include <cstdint>
#include <cassert>

#include "Component.h"

class GameObject;

class CustomComponent : public Component
{
public:
    enum ValueType
    {
        e_ValueTypeUInt32 = 0,
        e_ValueTypeInt32 = 1,
        e_ValueTypeUInt64,
        e_ValueTypeInt64,
        e_ValueTypeReal32,
        e_ValueTypeReal64,
        e_ValueTypeString,

        e_ValueTypeCount // Not a value type (used for counting)
    };

    CustomComponent(const std::string& name, ValueType valueType, GameObject& owningGameObject)
    : Component( m_ComponentType, name, owningGameObject ),
      m_ValueType( valueType ),
      m_NumericValue(),
      m_StringValue()
    {

    }

    inline ValueType GetValueType() const
    {
        return m_ValueType;
    }

    inline void SetUInt32(uint32_t val)
    {
        assert( ( m_ValueType == e_ValueTypeUInt32 ) && "Custom property is not of e_ValueTypeUInt32 type." );

        m_NumericValue.uInt32 = val;
    }

    inline uint32_t GetUInt32() const
    {
        assert( ( m_ValueType == e_ValueTypeUInt32 ) && "Custom property is not of e_ValueTypeUInt32 type." );

        return m_NumericValue.uInt32;
    }

    inline void SetInt32(int32_t val)
    {
        assert( ( m_ValueType == e_ValueTypeInt32 ) && "Custom property is not of e_ValueTypeInt32 type." );

        m_NumericValue.int32 = val;
    }

    inline int32_t GetInt32() const
    {
        assert( ( m_ValueType == e_ValueTypeInt32 ) && "Custom property is not of e_ValueTypeInt32 type." );

        return m_NumericValue.int32;
    }

    inline void SetUInt64(uint64_t val)
    {
        assert( ( m_ValueType == e_ValueTypeUInt64 ) && "Custom property is not of e_ValueTypeUInt64 type." );

        m_NumericValue.uint64 = val;
    }

    inline uint64_t GetUInt64() const
    {
        assert( ( m_ValueType == e_ValueTypeUInt64 ) && "Custom property is not of e_ValueTypeUInt64 type." );

        return m_NumericValue.uint64;
    }

    inline void SetInt64(int64_t val)
    {
        assert( ( m_ValueType == e_ValueTypeInt64 ) && "Custom property is not of e_ValueTypeInt64 type." );

        m_NumericValue.int64 = val;
    }

    inline int64_t GetInt64() const
    {
        assert( ( m_ValueType == e_ValueTypeInt64 ) && "Custom property is not of e_ValueTypeInt64 type." );

        return m_NumericValue.int64;
    }

    inline void SetReal32(float val)
    {
        assert( ( m_ValueType == e_ValueTypeReal32 ) && "Custom property is not of e_ValueTypeReal32 type." );

        m_NumericValue.real32 = val;
    }

    inline float GetReal32() const
    {
        assert( ( m_ValueType == e_ValueTypeReal32 ) && "Custom property is not of e_ValueTypeReal32 type." );

        return m_NumericValue.real32;
    }

    inline void SetReal64(double val)
    {
        assert( ( m_ValueType == e_ValueTypeReal64 ) && "Custom property is not of e_ValueTypeReal64 type." );

        m_NumericValue.real64 = val;
    }

    inline double GetReal64() const
    {
        assert( ( m_ValueType == e_ValueTypeReal64 ) && "Custom property is not of e_ValueTypeReal64 type." );

        return m_NumericValue.real64;
    }

    inline void SetString(const std::string& val)
    {
        assert( ( m_ValueType == e_ValueTypeString ) && "Custom property is not of e_ValueTypeString type." );

        m_StringValue = val;
    }

    inline const std::string& GetString() const
    {
        assert( ( m_ValueType == e_ValueTypeString ) && "Custom property is not of e_ValueTypeString type." );

        return m_StringValue;
    }

protected:
    GAME_OBJECT_COMPONENT_TYPE(e_TypeCustom)

private:
    ValueType m_ValueType;

    union
    {
        uint32_t uInt32;
        int32_t int32;
        uint64_t uint64;
        int64_t int64;
        float real32;
        double real64;
    } m_NumericValue;

    std::string m_StringValue;
};

#endif // CUSTOMCOMPONENT_H_INCLUDED
