#include "Texture.h"
#include "GraphicsDevice.h"
#include <OpenGLErrorChecking.h>
#include <StringUtils.h>
#include <CommonDefinitions.h>
#include <Exceptions/GenericException.h>
#include <Resources/MTC.h>
#include <cassert>
#include <cstring>
#include <algorithm>

//-----------------------------------------------------------------------

const Texture::Type Texture::m_TypeIdxToTypeMap[e_TypeIdxCount] =
{
    e_TypeGBufferVertexPossitionMapViewSpace,
    e_TypeGBufferDiffuseMap,
    e_TypeGBufferVertexNormalMapViewSpace,
    e_TypeDiffuseMap,
    e_TypeNormalMap,
    e_TypeSpecularMap,
    e_TypeGlossMap
};

//-----------------------------------------------------------------------

const char* Texture::m_TypeIdxToSamplerShaderUniformNameMap[e_TypeIdxCount] =
{
    "u_GBufferVertexPossitionMap_ViewSpace",
    "u_GBufferDiffuseMap",
    "u_GBufferVertexNormalMap_ViewSpace",
    "u_DiffuseMap",
    "u_NormalMap",
    "u_SpecularMap",
    "u_GlossMap",
    "u_DepthBuffer",
    "u_PostProcessTextureMap"
};

//-----------------------------------------------------------------------

const GLenum Texture::m_TextureStateParamToOpenGLTextureParamMap[e_StateParamCount] =
{
    GL_TEXTURE_MIN_FILTER,
    GL_TEXTURE_MAG_FILTER,
    GL_TEXTURE_SWIZZLE_R,
    GL_TEXTURE_SWIZZLE_G,
    GL_TEXTURE_SWIZZLE_B,
    GL_TEXTURE_SWIZZLE_A,
    GL_TEXTURE_WRAP_S,
    GL_TEXTURE_WRAP_T,
    GL_TEXTURE_WRAP_R
};

//-----------------------------------------------------------------------

const GLenum Texture::m_TextureIntParamToOpenGLTextureParamMap[e_IntParamCount] =
{
    GL_TEXTURE_BASE_LEVEL,
    GL_TEXTURE_MAX_LEVEL,
};

//-----------------------------------------------------------------------

const GLenum Texture::m_TextureFloatParamToOpenGLTextureParamMap[e_FloatParamCount] =
{
    GL_TEXTURE_MIN_LOD,
    GL_TEXTURE_MAX_LOD,
    GL_TEXTURE_MAX_ANISOTROPY_EXT,
};

//-----------------------------------------------------------------------

const GLint Texture::m_DefaultTextureStateParamValues[e_StateParamCount] =
{
    e_StateParamArgMinifingFilterNearestMipmapLinear, // e_StateParamMinifyingFilter
    e_StateParamArgMagnifyingFilterLinear,            // e_StateParamMagnifyingFilter
    e_StateParamArgSwizzleRed,                        // e_StateParamSwizzleRed,
    e_StateParamArgSwizzleGreen,                      // e_StateParamSwizzleGreen,
    e_StateParamArgSwizzleBlue,                       // e_StateParamSwizzleBlue,
    e_StateParamArgSwizzleAlpha,                      // e_StateParamSwizzleAlpha,
    e_StateParamArgSCoordWrapRepeat,                  // e_StateParamSCoordWrapType,
    e_StateParamArgTCoordWrapRepeat,                  // e_StateParamTCoordWrapType,
    e_StateParamArgRCoordWrapRepeat                   // e_StateParamRCoordWrapType,
};

//-----------------------------------------------------------------------

const int Texture::m_DefaultTextureIntParamValues[e_IntParamCount] =
{
    0,    // e_IntParamLowestDefinedMipmapLevel
    1000, // e_IntParamHighestDefinedMipmapLevel
};

//-----------------------------------------------------------------------

const float Texture::m_DefaultTextureFloatParamValues[e_FloatParamCount] =
{
    -1000.0f, // e_FloatParamMinLOD
    1000.0f,  // e_FloatParamMaxLOD
    0.0f,     // e_MaxAnisotropyLevelToUse
};

//-----------------------------------------------------------------------

GLuint Texture::m_CurrentlyBoundTextureId = c_InvalidOpenGLTextureId;

//-----------------------------------------------------------------------

GLuint Texture::m_TexturesActiveOnTextureUnits[e_TypeIdxCount] =
{
    c_InvalidOpenGLTextureId,
    c_InvalidOpenGLTextureId,
    c_InvalidOpenGLTextureId,
    c_InvalidOpenGLTextureId,
    c_InvalidOpenGLTextureId,
    c_InvalidOpenGLTextureId,
    c_InvalidOpenGLTextureId
};

//-----------------------------------------------------------------------

Texture::Texture(GraphicsDevice& graphicsDevice, TypeIdx textureTypeIdx, Class textureClass, bool isMipmapped)
: m_GraphicsDevice( graphicsDevice ),
  m_TypeIdx(textureTypeIdx),
  m_Class( textureClass ),
  m_IsMipmapped( isMipmapped ),
  m_AnisotropicFilteringAvailable( m_IsMipmapped && graphicsDevice.ExtenstionIsSupported( GraphicsDevice::OpenGLExtenstionGL_EXT_texture_filter_anisotropic ) ), // Anisotropic filtering can only be used with mipmapped textures
  m_TextureObjectHasBeenInitialised( false ),
  m_ImageDataHasBeenSet( false ),
  m_OpenGLTextureId( _CreateOpenGLTextureObject() ),
  m_FilteringMode( e_FilteringModeNoFiltering ),
  m_CurrentTextureStateParamValues(),
  m_CurrentTextureIntParamValues(),
  m_CurrentTextureFloatParamValues()
{
    assert( ( m_TypeIdx < e_TypeIdxCount ) && "Texture Index must be less than e_TypeIdxCount" );

    static_assert( sizeof( m_CurrentTextureStateParamValues ) == sizeof( m_DefaultTextureStateParamValues ), "Must be equal" );
    static_assert( sizeof( m_CurrentTextureIntParamValues ) == sizeof( m_DefaultTextureIntParamValues ), "Must be equal" );
    static_assert( sizeof( m_CurrentTextureFloatParamValues ) == sizeof( m_DefaultTextureFloatParamValues ), "Must be equal" );

    _InitialSetup();
}

//-----------------------------------------------------------------------

Texture::~Texture()
{
    // Since it is being destroyed make sure this texture is not set as bound
    if( !_IsTextureNotAlreadyBound() )
    {
        m_CurrentlyBoundTextureId = c_InvalidOpenGLTextureId;
    }

    _DeleteOpenGLTextureObject( m_OpenGLTextureId );
}

//-----------------------------------------------------------------------

void Texture::SetAnisotropyLevel(float anisotropyLevel)
{
    if( m_AnisotropicFilteringAvailable && ( anisotropyLevel != GetFloatParameterVal( e_MaxAnisotropyLevelToUse ) ) )
    {
        if( anisotropyLevel <= m_GraphicsDevice.GetMaxSupportedAnisotropyLevel() )
        {
            SetFloatParameter( e_MaxAnisotropyLevelToUse, anisotropyLevel );
        }
        else
        {
            std::string errMsg = "Trying to set anisotropic level of \"";
            errMsg += StringUitls::NumToString( anisotropyLevel );
            errMsg += " while only \"";
            errMsg += StringUitls::NumToString( m_GraphicsDevice.GetMaxSupportedAnisotropyLevel() );
            errMsg += "\" is supported by the device";

            throw GenericException( errMsg );
        }
    }
}

//-----------------------------------------------------------------------

void Texture::Reset()
{
    // Since it is being recreated make sure this texture is not set as bound
    if( !_IsTextureNotAlreadyBound() )
    {
        m_CurrentlyBoundTextureId = c_InvalidOpenGLTextureId;
    }

    _DeleteOpenGLTextureObject( m_OpenGLTextureId );
    m_OpenGLTextureId = _CreateOpenGLTextureObject();

    _InitialSetup();

    m_TextureObjectHasBeenInitialised = false;
    m_ImageDataHasBeenSet = false;
}

//-----------------------------------------------------------------------

size_t Texture::InitializeStorage(InternalDataFormat internalFormat,
                                  size_t textureWidth,
                                  size_t textureHeight)
{
    GLsizei mipMapLevelsToGenerate = 1;

    if( !m_TextureObjectHasBeenInitialised )
    {
        if( m_IsMipmapped )
        {
            mipMapLevelsToGenerate = static_cast<GLsizei>( MathUtils::MaxMipMapLevelForTextureDimensions( textureWidth, textureHeight ) );
        }

        glTexStorage2D( m_Class,
                        mipMapLevelsToGenerate,
                        static_cast<GLenum>( internalFormat ),
                        static_cast<GLsizei>( textureWidth ),
                        static_cast<GLsizei>( textureHeight ) );
        CheckForGLErrors();

        SetIntegerParameter( e_IntParamHighestDefinedMipmapLevel, ( mipMapLevelsToGenerate - 1 ) );

        m_TextureObjectHasBeenInitialised = true;
    }
    else
    {
        throw GenericException( "Tried to initialize texture storage more than once. This is an error, becuase once it is initialized it becomes immutable" );
    }

    return mipMapLevelsToGenerate;
}

//-----------------------------------------------------------------------

void Texture::SetData(DataClass dataClass, const ImageUtils::ImageData_t& imgData, bool dataIsSRGBEncoded)
{
    if( !m_TextureObjectHasBeenInitialised && !m_ImageDataHasBeenSet )
    {
        Activate( true );

        InternalDataFormat internalFormat = ( ( _GetImageColourFormat( imgData ) == GL_RGB ) ? e_InternalDataFormatRGB8 : e_InternalDataFormatRGBA8 );

#ifndef OPENGL_ES
        if( dataIsSRGBEncoded )
        {
            internalFormat = ( ( _GetImageColourFormat( imgData ) == GL_RGB ) ? e_InternalDataFormatSRGB8 : e_InternalDataFormatSRGB8Alpha8 );
        }
#endif

        InitializeStorage( internalFormat,
                           imgData.w,
                           imgData.h );

        glTexSubImage2D( dataClass,
                         0, // MipMap level of which data is being set (0 means base level)
                         0, // Texel offset in the x direction within the texture array
                         0, // Texel offset in the y direction within the texture array
                         static_cast<GLsizei>( imgData.w ),
                         static_cast<GLsizei>( imgData.h ),
                         _GetImageColourFormat( imgData ),
                         GL_UNSIGNED_BYTE,
                         imgData.data );
        CheckForGLErrors();

        if( m_IsMipmapped )
        {
            glGenerateMipmap( m_Class );
            CheckForGLErrors();
        }

        m_ImageDataHasBeenSet = true;
    }
    else
    {
        if( m_TextureObjectHasBeenInitialised && m_ImageDataHasBeenSet )
        {
            throw GenericException( "Tried to set image data more than once" );
        }
        else
        {
            throw GenericException( "Tried to set image data for a texture which was initialized as a texture without image data" );
        }
    }
}

//-----------------------------------------------------------------------

void Texture::SetData(const MTC& imgData, bool UNUSED_PARAM(dataIsSRGBEncoded))
{
    if( !m_TextureObjectHasBeenInitialised && !m_ImageDataHasBeenSet )
    {
        Activate( true );

        m_GraphicsDevice.PixelStoreOptionSaveCurrentValue( GraphicsDevice::e_PixelStoreOptionUnpackAlignment );
        m_GraphicsDevice.SetPixelStoreOptionValue( GraphicsDevice::e_PixelStoreOptionUnpackAlignment, static_cast<int>( imgData.GetTextureDataRowAlignment() ) );


        size_t topMipMapWidthInPixels = 0;
        size_t topMipMapHeightInPixels = 0;

        imgData.GetMipLevelDimensions( 0, topMipMapWidthInPixels, topMipMapHeightInPixels );

        bool compressed = false;
        const InternalDataFormat internalFormat = _GetImageInternlFormat( imgData, compressed );

        InitializeStorage( internalFormat,
                           topMipMapWidthInPixels,
                           topMipMapHeightInPixels );

        const GLenum dataClass = _GetImageDataClass( imgData );
        const GLenum colorFormat = _GetImageColourFormat( imgData );
        const size_t mipMapLevelCount = ( m_IsMipmapped ? imgData.GetMipMapLevelCount() : 1 );
        const size_t faceCount = imgData.GetTextureFaceCount();
        for(size_t mipMapLevel = 0; mipMapLevel < mipMapLevelCount; mipMapLevel++)
        {
            size_t widthInPixels = 0;
            size_t heightInPixels = 0;
            const uint8_t* data = nullptr;
            size_t dataSize = 0;

            imgData.GetMipLevelDimensions( mipMapLevel, widthInPixels, heightInPixels );

            for(size_t faceIdx = 0; faceIdx < faceCount; faceIdx++) // Needed for CubeMaps
            {
                imgData.GetMipLevelTextureData( mipMapLevel, faceIdx, data, dataSize );

                if( compressed )
                {
                    glCompressedTexSubImage2D( static_cast<GLenum>( dataClass + faceIdx ),
                                               static_cast<GLint>( mipMapLevel ), // MipMap level of which data is being set (0 means base level)
                                               0, // Texel offset in the x direction within the texture array
                                               0, // Texel offset in the y direction within the texture array
                                               static_cast<GLsizei>( widthInPixels ),
                                               static_cast<GLsizei>( heightInPixels ),
                                               static_cast<GLenum>( internalFormat ),
                                               static_cast<GLsizei>( dataSize ),
                                               data );
                    CheckForGLErrors();
                }
                else
                {
                    glTexSubImage2D( static_cast<GLenum>( dataClass + faceIdx ),
                                     static_cast<GLint>( mipMapLevel ), // MipMap level of which data is being set (0 means base level)
                                     0, // Texel offset in the x direction within the texture array
                                     0, // Texel offset in the y direction within the texture array
                                     static_cast<GLsizei>( widthInPixels ),
                                     static_cast<GLsizei>( heightInPixels ),
                                     colorFormat,
                                     GL_UNSIGNED_BYTE,
                                     data );
                    CheckForGLErrors();
                }
            }
        }

        m_ImageDataHasBeenSet = true;

        m_GraphicsDevice.PixelStoreOptionRestorePreviousValue( GraphicsDevice::e_PixelStoreOptionUnpackAlignment );
    }
    else
    {
        if( m_TextureObjectHasBeenInitialised && m_ImageDataHasBeenSet )
        {
            throw GenericException( "Tried to set image data more than once" );
        }
        else
        {
            throw GenericException( "Tried to set image data for a texture which was initialized as a texture without image data" );
        }
    }
}

//-----------------------------------------------------------------------

void Texture::SetFilteringMode(FilteringMode mode)
{
    if( mode != m_FilteringMode )
    {
        m_FilteringMode = mode;

        switch( m_FilteringMode )
        {
            case e_FilteringModeNoFiltering:
            {
                _TurnOffAllFilteringTheTexture();
                break;
            }

            case e_FilteringModeBilinear:
            {
                _TurnOnBilinearFilteringForTheTexture();
                break;
            }

            case e_FilteringModeTrilinear:
            {
                _TurnOnTrilinearFilteringForTheTexture();
                break;
            }

            default:
            {
                assert( false && "Unhandled filtering mode" );
                break;
            }
        }
    }
}

//-----------------------------------------------------------------------

void Texture::SetStateParameter(StateParam param, StateParamArg arg)
{
    assert( ( param < e_StateParamCount ) && "Texture FloatParam must be less than e_StateParamCount" );

    if( arg != m_CurrentTextureStateParamValues[param] )
    {
        if( _IsTextureNotAlreadyBound() )
        {
            Activate( true );
        }

        m_CurrentTextureStateParamValues[param] = arg;

        glTexParameteri( m_Class, m_TextureStateParamToOpenGLTextureParamMap[param], arg );
        CheckForGLErrors();
    }
}

//-----------------------------------------------------------------------

void Texture::SetIntegerParameter(IntParam param, int arg)
{
    assert( ( param < e_IntParamCount ) && "Texture FloatParam must be less than e_IntParamCount" );

    if( arg != m_CurrentTextureIntParamValues[param] )
    {
        if( _IsTextureNotAlreadyBound() )
        {
            Activate( true );
        }

        m_CurrentTextureIntParamValues[param] = arg;

        glTexParameteri( m_Class, m_TextureIntParamToOpenGLTextureParamMap[param], arg );
        CheckForGLErrors();
    }
}

//-----------------------------------------------------------------------

void Texture::SetFloatParameter(FloatParam param, float arg)
{
    assert( ( param < e_FloatParamCount ) && "Texture FloatParam must be less than e_FloatParamCount" );

    if( arg != m_CurrentTextureFloatParamValues[param] )
    {
        if( _IsTextureNotAlreadyBound() )
        {
            Activate( true );
        }

        m_CurrentTextureFloatParamValues[param] = arg;

        glTexParameterf( m_Class, m_TextureFloatParamToOpenGLTextureParamMap[param], arg );
        CheckForGLErrors();
    }
}

//-----------------------------------------------------------------------

Texture::StateParamArg Texture::GetStateParameterVal(StateParam param) const
{
    assert( ( param < e_StateParamCount ) && "Texture FloatParam must be less than e_StateParamCount" );

    return static_cast<StateParamArg>( m_CurrentTextureStateParamValues[param] );
}

//-----------------------------------------------------------------------

int Texture::GetIntegerParameterVal(IntParam param) const
{
    assert( ( param < e_IntParamCount ) && "Texture FloatParam must be less than e_IntParamCount" );

    return m_CurrentTextureIntParamValues[param];
}

//-----------------------------------------------------------------------

float Texture::GetFloatParameterVal(FloatParam param) const
{
    assert( ( param < e_FloatParamCount ) && "Texture FloatParam must be less than e_FloatParamCount" );

    return m_CurrentTextureFloatParamValues[param];
}

//-----------------------------------------------------------------------

void Texture::Activate(bool UNUSED_PARAM(forceActivation))
{
    //if( ( m_OpenGLTextureId != m_TexturesActiveOnTextureUnits[m_TypeIdx] ) || forceActivation ) // TODO Temporarily disable checking, because not all of the code is using this Texture class yet
    {
        m_TexturesActiveOnTextureUnits[m_TypeIdx] = m_OpenGLTextureId;

        glActiveTexture( ( GL_TEXTURE0 + m_TypeIdx ) );
        CheckForGLErrors();

        _Bind( true );
    }
}

//-----------------------------------------------------------------------

void Texture::InvalidateCashedActivationState()
{
    for(size_t i = 0; i < e_TypeIdxCount; i++)
    {
        m_TexturesActiveOnTextureUnits[i] = c_InvalidOpenGLTextureId;
    }

    m_CurrentlyBoundTextureId = c_InvalidOpenGLTextureId;
}

//-----------------------------------------------------------------------

void Texture::_Bind(bool forceBind)
{
    if( _IsTextureNotAlreadyBound() || forceBind )
    {
        m_CurrentlyBoundTextureId = m_OpenGLTextureId;

        glBindTexture( m_Class, m_OpenGLTextureId );
        CheckForGLErrors();
    }
}

//-----------------------------------------------------------------------

bool Texture::_IsTextureNotAlreadyBound() const
{
    return ( m_CurrentlyBoundTextureId != m_OpenGLTextureId );
}

//-----------------------------------------------------------------------

GLenum Texture::_GetImageDataClass(const MTC& imgData) const
{
    GLenum dataClass = 0;

    switch( imgData.GetTextureType() )
    {
//        case e_TextureType1D:
//        {
//            dataClass = ;
//            break;
//        }

        case MTC::e_TextureType2D:
        {
            dataClass = e_DataClass2DTexture;
            break;
        }

        case MTC::e_TextureTypeCubeMap:
        {
            dataClass = GL_TEXTURE_CUBE_MAP_POSITIVE_X;
            break;
        }

        default:
        {
            assert( false && "Not supported class" );
            break;
        }
    }

    return dataClass;
}

//-----------------------------------------------------------------------

Texture::InternalDataFormat Texture::_GetImageInternlFormat(const MTC& imgData,
                                                            bool& outCompressed) const
{
    InternalDataFormat internalDataFormat = e_InternalDataFormatInvalid;
    outCompressed = false;

    switch( imgData.GetTextureDataType() )
    {
        case MTC::e_TextureDataTypeRawRGB:
        {
            internalDataFormat = e_InternalDataFormatRGB8;
            break;
        }
        case MTC::e_TextureDataTypeRawSRGB:
        {
            internalDataFormat = e_InternalDataFormatSRGB8;
            break;
        }
        case MTC::e_TextureDataTypeRawRGBA:
        {
            internalDataFormat = e_InternalDataFormatRGBA8;
            break;
        }
        case MTC::e_TextureDataTypeRawSRGBA:
        {
            internalDataFormat = e_InternalDataFormatSRGB8Alpha8;
            break;
        }
        case MTC::e_TextureDataTypeETC2_RGB:
        {
            internalDataFormat = e_InternalDataFormatRGB8ETC2;
            outCompressed = true;
            break;
        }
        case MTC::e_TextureDataTypeETC2_SRGB:
        {
            internalDataFormat = e_InternalDataFormatSRGB8ETC2;
            outCompressed = true;
            break;
        }
        case MTC::e_TextureDataTypeALPHA1_ETC2_RGBA:
        {
            internalDataFormat = e_InternalDataFormatRGB8Alpha1ETC2;
            outCompressed = true;
            break;
        }
        case MTC::e_TextureDataTypeALPHA1_ETC2_SRGBA:
        {
            internalDataFormat = e_InternalDataFormatSRGB8Alpha1ETC2;
            outCompressed = true;
            break;
        }
        case MTC::e_TextureDataTypeETC2_EAC_RGBA:
        {
            internalDataFormat = e_InternalDataFormatRGBA8ETC2EAC;
            outCompressed = true;
            break;
        }
        case MTC::e_TextureDataTypeETC2_EAC_SRGBA:
        {
            internalDataFormat = e_InternalDataFormatSRGBA8ETC2EAC;
            outCompressed = true;
            break;
        }
        default:
        {
            assert( false && "Not supported internal data format" );
            break;
        }
    }

    return internalDataFormat;
}

//-----------------------------------------------------------------------

GLenum Texture::_GetImageColourFormat(const MTC& imgData) const
{
    GLenum colourFormat = 0;

    switch( imgData.GetTextureDataType() )
    {
        case MTC::e_TextureDataTypeRawRGB:
        case MTC::e_TextureDataTypeRawSRGB:
        case MTC::e_TextureDataTypeETC2_RGB:
        case MTC::e_TextureDataTypeETC2_SRGB:
        {
            colourFormat = GL_RGB;
            break;
        }
        case MTC::e_TextureDataTypeRawRGBA:
        case MTC::e_TextureDataTypeRawSRGBA:
        case MTC::e_TextureDataTypeETC2_EAC_RGBA:
        case MTC::e_TextureDataTypeETC2_EAC_SRGBA:
        case MTC::e_TextureDataTypeALPHA1_ETC2_RGBA:
        case MTC::e_TextureDataTypeALPHA1_ETC2_SRGBA:
        {
            colourFormat = GL_RGBA;
            break;
        }

        default:
        {
            assert( false && "Not supported colour format" );
            break;
        }
    }

    return colourFormat;
}

//-----------------------------------------------------------------------

GLenum Texture::_GetImageColourFormat(const ImageUtils::ImageData_t& imgData) const
{
    GLenum colourFormat = 0;

    switch( imgData.format )
    {
        case ImageUtils::RGB:
        {
            colourFormat = GL_RGB;
            break;
        }
        case ImageUtils::RGBA:
        {
            colourFormat = GL_RGBA;
            break;
        }
//        // Not available in OpenGL ES
//        case ImageUtils::BGR:
//        {
//            colourFormat = GL_BGR;
//            break;
//        }
//        case ImageUtils::BGRA:
//        {
//            colourFormat = GL_BGRA;
//            break;
//        }
        default:
        {
            assert( false && "Not supported colour format" );
            break;
        }
    }

    return colourFormat;
}

//-----------------------------------------------------------------------

void Texture::_TurnOffAllFilteringTheTexture()
{
    if( m_IsMipmapped )
    {
        SetStateParameter( e_StateParamMinifyingFilter,
                           e_StateParamArgMinifingFilterNearestMipmapNearest );
    }
    else
    {
        SetStateParameter( e_StateParamMinifyingFilter,
                           e_StateParamArgMinifingFilterNearest );
    }

    SetStateParameter( e_StateParamMagnifyingFilter,
                       e_StateParamArgMagnifyingFilterNearest );
}

//-----------------------------------------------------------------------

void Texture::_TurnOnBilinearFilteringForTheTexture()
{
    if( m_IsMipmapped )
    {
        SetStateParameter( e_StateParamMinifyingFilter,
                           e_StateParamArgMinifingFilterLinearMipmapNearest );
    }
    else
    {
        SetStateParameter( e_StateParamMinifyingFilter,
                           e_StateParamArgMinifingFilterLinear );
    }

    SetStateParameter( e_StateParamMagnifyingFilter,
                       e_StateParamArgMagnifyingFilterLinear );
}

//-----------------------------------------------------------------------

void Texture::_TurnOnTrilinearFilteringForTheTexture()
{
    if( m_IsMipmapped )
    {
        SetStateParameter( e_StateParamMinifyingFilter,
                           e_StateParamArgMinifingFilterLinearMipmapLinear );
    }
    else
    {
        // Note this turns on bilinear filtering, since trilinear filtering is not supported on non-mipmapped textures
        SetStateParameter( e_StateParamMinifyingFilter,
                           e_StateParamArgMinifingFilterLinear );
    }

    SetStateParameter( e_StateParamMagnifyingFilter,
                       e_StateParamArgMagnifyingFilterLinear );
}

//-----------------------------------------------------------------------

void Texture::_InitialSetup()
{
    memcpy( m_CurrentTextureStateParamValues, m_DefaultTextureStateParamValues, sizeof( m_DefaultTextureStateParamValues ) );
    memcpy( m_CurrentTextureIntParamValues, m_DefaultTextureIntParamValues, sizeof( m_DefaultTextureIntParamValues ) );
    memcpy( m_CurrentTextureFloatParamValues, m_DefaultTextureFloatParamValues, sizeof( m_DefaultTextureFloatParamValues ) );

    _TurnOffAllFilteringTheTexture();
}

//-----------------------------------------------------------------------

GLuint Texture::_CreateOpenGLTextureObject()
{
    GLuint retVal = c_InvalidOpenGLTextureId;

    glGenTextures( 1, &retVal );
    CheckForGLErrors();

    assert( ( retVal != c_InvalidOpenGLTextureId ) &&  "Failed to generate texture" );

    return retVal;
}

//-----------------------------------------------------------------------

void Texture::_DeleteOpenGLTextureObject(GLuint& openGLTextureID)
{
    glDeleteTextures( 1, &openGLTextureID );
    CheckForGLErrors();

    openGLTextureID = c_InvalidOpenGLTextureId;
}

//-----------------------------------------------------------------------
