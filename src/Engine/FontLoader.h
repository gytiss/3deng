#ifndef FONTLOADER_H_INCLUDED
#define FONTLOADER_H_INCLUDED

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H

#include <string>
#include <list>
#include <map>

//////////////////////////////////////////////////////////////////////////
// This is a singleton class
//////////////////////////////////////////////////////////////////////////

class FileManager;

class FontLoader
{
public:
    static FontLoader * getInstance();
    FT_Face loadNewFaceFromFile(std::string fontFileName, FileManager& fileManager);
    FT_Face loadNewFaceFromMemory(const FT_Byte* base_address, size_t numByteToRead);
    size_t getNumofFacesInAFont(FT_Face font);
    void deleteFace(FT_Face face);
    void printFontDebugInfo();
    void setupFacePixelSize(FT_Face, long char_wdth, long char_height, size_t h_res, size_t v_res);
    FT_GlyphSlot getCharBitmap(FT_Face face, unsigned long char_code,FT_UInt* gIndex);

private:
    FontLoader();
    ~FontLoader();
    FontLoader&  operator = (const FontLoader& other);

    static FontLoader* m_FontLoaderInstance;
    FT_Library m_FreeTypeLibrary;
    std::list<FT_Face> m_AllLoadedFonts;
    std::map<FT_Face, FT_Byte*> m_FontFacesToMemoryMap;

};

#endif // FONTLOADER_H_INCLUDED
