#include "Shader.h"
#include <StringUtils.h>
#include <CommonDefinitions.h>
#include <OpenGLErrorChecking.h>
#include <Exceptions/GenericException.h>
#include <Resources/FileManager.h>
#include <boost/algorithm/string/trim.hpp>
#include <boost/filesystem.hpp>
#include <boost/tokenizer.hpp>
#include <boost/locale.hpp>

//-----------------------------------------------------------------------

#ifdef OPENGL_ES
    std::string Shader::c_ShadingLanguageVersion( "300 es" ); // OpenGL ES 3.0
#else
    std::string Shader::c_ShadingLanguageVersion( "330" );    // OpenGL 3.3
#endif

//-----------------------------------------------------------------------

Shader::Shader(FileManager& fileManager,
               const std::string& shaderFilePath,
               ShaderType st,
               const std::string& defines)
: m_ShaderType( st ),
  m_ShaderFilePath( shaderFilePath ),
  m_Defines( boost::algorithm::trim_copy( defines ) ),
  m_ShaderHandle( 0 ),
  m_ShaderIsCompiled( false ),
  m_ShaderIsLoaded( false ),
  m_ShaderSources(),
  m_FileManager( fileManager )
{
}

//-----------------------------------------------------------------------

Shader::~Shader()
{
    if( m_ShaderIsLoaded )
    {
        glDeleteShader( m_ShaderHandle );
        CheckForGLErrors();
    }
}

//-----------------------------------------------------------------------

void Shader::LoadAndCompile()
{
    if( m_ShaderIsLoaded )
    {
        throw GenericException( _ConstructExceptionMessage( "Shader is already loaded." ) );
    }

	const boost::filesystem::path::string_type shaderFilePath = boost::locale::conv::utf_to_utf<boost::filesystem::path::string_type::value_type>( m_ShaderFilePath );
    const boost::filesystem::path mainShaderSourceFileDir = boost::filesystem::path( shaderFilePath ).remove_leaf().c_str();

    std::vector<std::string> includeFilePaths;

    std::string mainShaderSource = "";

    // Check if the shader source file was read correctly
    if( _ReadShaderSourceFromFile( m_ShaderFilePath, mainShaderSource ) )
    {
        // Shader file must not be empty
        if( mainShaderSource.empty() )
        {
            throw GenericException( _ConstructExceptionMessage( "Shader source file is empty" ) );
        }

        _ExtractIncludeFileNames( includeFilePaths, mainShaderSource );

        const size_t numberOfShaderSourceFiles = ( 1 + includeFilePaths.size() );
        m_ShaderSources.reserve( numberOfShaderSourceFiles );
        m_ShaderSources.assign( numberOfShaderSourceFiles, "" );

        m_ShaderSources[0] = mainShaderSource;
        for(size_t i = 1; i < numberOfShaderSourceFiles; i++)
        {
            std::string& shaderSource = m_ShaderSources[i];
 
			const boost::filesystem::path::string_type includeFilePath = boost::locale::conv::utf_to_utf<boost::filesystem::path::string_type::value_type, char>( includeFilePaths[i - 1] );
            const boost::filesystem::path fullIncludeFilePath = ( mainShaderSourceFileDir / includeFilePath );

            if( !_ReadShaderSourceFromFile(boost::locale::conv::utf_to_utf<char>( fullIncludeFilePath.c_str() ), shaderSource ) )
            {
                std::string errMsg = "Include file \"";
                errMsg += boost::locale::conv::utf_to_utf<char>( fullIncludeFilePath.c_str() );
                errMsg += "\" was not found";

                throw GenericException( _ConstructExceptionMessage( errMsg ) );
            }
        }

        // The last shader source in this list will be compiled the first, hence add all
        // of the defines to it.
        _AddDefinesAndShadingLanguageVersion( m_ShaderSources[m_ShaderSources.size() - 1] );
    }
    else
    {
        throw GenericException( _ConstructExceptionMessage( "Shader source file was not found" ) );
    }

    switch( m_ShaderType )
    {
        case e_ShaderTypeVertex:
        {
            m_ShaderHandle = glCreateShader( GL_VERTEX_SHADER );
            CheckForGLErrors();
            break;
        }
        case e_ShaderTypeFragment:
        {
            m_ShaderHandle = glCreateShader( GL_FRAGMENT_SHADER );
            CheckForGLErrors();
            break;
        }
#ifndef OPENGL_ES
        case e_ShaderTypeGeometry:
        {
            m_ShaderHandle = glCreateShader( GL_GEOMETRY_SHADER );
            CheckForGLErrors();
            break;
        }
#endif
        default:
        {
            throw GenericException( _ConstructExceptionMessage( "Trying to load shader of unsupported type" ) );
        }
    }

    if( m_ShaderHandle == 0 )
    {
        throw GenericException( _ConstructExceptionMessage( "Failed to create shader" ) );
    }

    std::vector<const GLchar *> shaderSourcePointers;
    shaderSourcePointers.reserve( m_ShaderSources.size() );
    for(size_t i = ( m_ShaderSources.size() - 1 ); i > 0 ; i--)
    {
        shaderSourcePointers.push_back( reinterpret_cast<const GLchar *>( m_ShaderSources[i].c_str() ) );
    }
    shaderSourcePointers.push_back( reinterpret_cast<const GLchar *>( m_ShaderSources[0].c_str() ) );

    glShaderSource( m_ShaderHandle, static_cast<GLsizei>( shaderSourcePointers.size() ), &shaderSourcePointers[0], 0 );
    CheckForGLErrors();

    m_ShaderIsLoaded = true;

    _Compile();

    m_ShaderSources.clear();
    m_ShaderSources.resize( 0 );
}

//-----------------------------------------------------------------------

void Shader::_Compile()
{
    if( !m_ShaderIsLoaded )
    {
        throw GenericException( _ConstructExceptionMessage( "Shader needs to be loaded before compiling." ) );
    }

    glCompileShader( m_ShaderHandle );
    CheckForGLErrors();

    _CheckShaderCompileStatus();
}

//-----------------------------------------------------------------------

bool Shader::_ReadShaderSourceFromFile(const std::string& fileName,
                                       std::string& outputBuf)
{
    bool retVal = false;

    FileManager::File shaderFile;
    if( m_FileManager.OpenFile( fileName, FileManager::e_FileTypeBinary, shaderFile ) )
    {
        const size_t shaderSourceSize = shaderFile.Size();
        std::unique_ptr<char[]> shaderSource( new char[shaderSourceSize] );

        if( shaderFile.Read( shaderSource.get(), 1, shaderSourceSize ) )
        {
            outputBuf = std::string( shaderSource.get(), shaderSourceSize );
            retVal = true;
        }
    }

    return retVal;
}

//-----------------------------------------------------------------------

void Shader::_ExtractIncludeFileNames(std::vector<std::string>& includeFileNames,
                                      const std::string& shaderSourceCode)
{
    static const char c_PragmaKeyword[] = "#pragma";
    static const char c_IncludeExternalSourceKeyword[] = "IncludeExternalSource";

    size_t lineNumber = 1;

    boost::char_separator<char> lineSeparator( "\n", "", boost::keep_empty_tokens );
    boost::tokenizer<boost::char_separator<char> > lines( shaderSourceCode, lineSeparator );
    for ( boost::tokenizer<boost::char_separator<char> >::iterator it = lines.begin();
         it != lines.end();
         ++it)
    {
        const std::string line = boost::algorithm::trim_copy( *it );
        const size_t pragmaKeywordPos = line.find( c_PragmaKeyword );
        const size_t pragmaKeywordEndPos = ( pragmaKeywordPos + ArraySize( c_PragmaKeyword ) );

        if( ( pragmaKeywordPos != std::string::npos ) && ( pragmaKeywordEndPos < line.size() ) ) // Found a line containing pragma statement
        {
            const size_t includeExternalSourceKeywordPos = line.find( c_IncludeExternalSourceKeyword, pragmaKeywordEndPos );
            const size_t includeExternalSourceKeywordEndPos = ( includeExternalSourceKeywordPos + ArraySize( c_IncludeExternalSourceKeyword ) );

            if( ( includeExternalSourceKeywordPos != std::string::npos ) && // If it was found and is not the last thing in the line
                ( includeExternalSourceKeywordEndPos < line.size() ) )
            {
                const std::string lineWithKeywordsRemoved = boost::algorithm::trim_copy( line.substr( includeExternalSourceKeywordEndPos ) );

                bool pragmaDirectiveIsInvalid = true;

                if( ( lineWithKeywordsRemoved.size() >= 3 ) && // Have at least thre symbols e.g.: "A" and the first char
                    ( lineWithKeywordsRemoved[0] == '"' ) )    // is a double quote i.e. the next token is a string
                {
                    const size_t posOfTheSecondQuoteChar = lineWithKeywordsRemoved.find( "\"", 1 );


                    if( posOfTheSecondQuoteChar != std::string::npos )
                    {
                        const size_t posOfCommentStartingChar = lineWithKeywordsRemoved.substr( 0, posOfTheSecondQuoteChar ).find( "/", 1 );

                        // No comment starting char i.e. / was found before double quote char, hence the quote
                        // is not part of a comment and as a result represents the end of a string token
                        if( posOfCommentStartingChar == std::string::npos )
                        {
                            const std::string includeFileName = lineWithKeywordsRemoved.substr( 1, posOfTheSecondQuoteChar - 1 );
                            includeFileNames.push_back( includeFileName );
                            pragmaDirectiveIsInvalid = false;
                        }
                    }
                }

                if( pragmaDirectiveIsInvalid )
                {
                    std::string errMsg = "External source include pragma directive on line ";
                    errMsg += StringUitls::NumToString( lineNumber );
                    errMsg += " is invalid. Valid format is '";
                    errMsg += c_PragmaKeyword;
                    errMsg += " ";
                    errMsg += c_IncludeExternalSourceKeyword;
                    errMsg += " \"PathToShaderSourceFileToInclude\"";

                    throw GenericException( _ConstructExceptionMessage( errMsg ) );
                }
            }
        }

        lineNumber++;
    }
}

//-----------------------------------------------------------------------

void Shader::_AddDefinesAndShadingLanguageVersion(std::string& shaderSourceCode) const
{
    /*-------------- Add Shading Language Version --------------*/
    std::string extraShaderData = "#version ";
    extraShaderData += c_ShadingLanguageVersion;
    extraShaderData += "\n";
    /*----------------------------------------------------------*/

    /*---------------- Add Precision Qualifiers ----------------*/
#ifdef OPENGL_ES
    extraShaderData += "precision mediump float;";
    extraShaderData += "\n";
    extraShaderData += "precision mediump int;";
    extraShaderData += "\n";

    extraShaderData += "#define USING_OPENGL_ES";
    extraShaderData += "\n";

    extraShaderData += "#define HIGHEST_PRECISION mediump\n";
    extraShaderData += "#define LOWEST_PRECISION lowp\n";
#else
    extraShaderData += "#define HIGHEST_PRECISION highp\n";
    extraShaderData += "#define LOWEST_PRECISION highp\n";
#endif
    /*----------------------------------------------------------*/

    /*---------------------- Add Defines -----------------------*/
    if( m_Defines.length() > 0)
    {
        size_t nextDefinePos = m_Defines.find(' ', 0);
        if( nextDefinePos == std::string::npos )
        {
            extraShaderData += "#define ";
            extraShaderData += m_Defines;
            extraShaderData += "\n";
        }
        else
        {
            size_t prevDefinePos = 0;
            std::string define = "";
            do
            {
                define = "";
                define.assign( m_Defines, prevDefinePos, ( nextDefinePos - prevDefinePos ) );

                extraShaderData += "#define ";
                extraShaderData += define;
                extraShaderData += "\n";

                prevDefinePos = nextDefinePos + 1;
                nextDefinePos = m_Defines.find(' ', nextDefinePos + 1 );
            }
            while( nextDefinePos != std::string::npos );

            define = "";
            define.assign( m_Defines, prevDefinePos, ( m_Defines.length() - prevDefinePos ) );

            extraShaderData += "#define ";
            extraShaderData += define;
            extraShaderData += "\n";
        }
    }
    /*----------------------------------------------------------*/

    shaderSourceCode = ( extraShaderData + shaderSourceCode );
}

//-----------------------------------------------------------------------

void Shader::_CheckShaderCompileStatus()
{
    GLint status;
    GLsizei numOfCharsReceived;
    GLint messageLength;

    glGetShaderiv( m_ShaderHandle, GL_COMPILE_STATUS, &status );
    CheckForGLErrors();
    if( status == GL_FALSE ) // Shader Failed To Compile
    {
        glGetShaderiv( m_ShaderHandle, GL_INFO_LOG_LENGTH, &messageLength ); // Get Compilation Error Message length
        CheckForGLErrors();

        GLchar* message = new GLchar[messageLength];

        glGetShaderInfoLog( m_ShaderHandle, messageLength, &numOfCharsReceived, message ); // Get Compilation Error Message
        CheckForGLErrors();

        std::string msg( message );

        delete[] message;

        throw GenericException( _ConstructExceptionMessage( msg ) );
    }
}

//-----------------------------------------------------------------------

std::string Shader::_ConstructExceptionMessage(const std::string& errorMsg)
{
    std::string retVal = ("Shader \"" + m_ShaderFilePath + "\" failed with the following message: \n" + errorMsg);

    retVal += "\n";

    size_t lineNum  = 0;
    //for (std::vector<std::string>::const_reverse_iterator sourceIt = m_ShaderSources.rbegin(); sourceIt != m_ShaderSources.rend(); ++sourceIt)
    for(const std::string& source : m_ShaderSources)
    {
        //const std::string& source = *sourceIt;

        std::vector<std::string> outTokens;
        StringUitls::SplitStr( source, "\n", outTokens );

        for(const std::string& sourceLine : outTokens)
        {
            retVal += StringUitls::NumToString( lineNum );
            retVal += ": ";
            retVal += sourceLine;
            retVal += "\n";
            lineNum++;
        }
    }

    return retVal;
}

//-----------------------------------------------------------------------

