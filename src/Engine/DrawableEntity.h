#ifndef DRAWABLEENTITY_H
#define DRAWABLEENTITY_H

#include <string>
#include <vector>

#include <glm/glm.hpp>
#include <imgutl/ImageUtils.h>
#include "ModelBinObj.h"
#include <GraphicsDevice/ShaderProg.h>
#include <CommonDefinitions.h>

class GraphicsDevice;
class FileManager;

class DrawableEntity
{
public:
    enum renderingMode_t { RENDER_WITH_TEXTURE, RENDER_WITHOUT_TEXTURE, RENDER_IN_MIXED_MODE, RENDER_IN_WIREFRAME };

    struct Transformation
    {
        glm::mat4 viewMatrix;
        glm::mat4 projectionMatrix;
    };

    struct Texture_t
    {
        bool hasTexture;
        ImageUtils::ImageData_t* pixelData;
        GLuint textureID;
        GLenum textureFormat;
    };

    struct Textures
    {
        Texture_t colorTexture;
    };

    DrawableEntity(GraphicsDevice& graphicsDevice,
                   FileManager& fileManager,
                   std::string modelObjFileName,
                   bool loadTextures,
                   bool clampTexturesToEdge);
    virtual     ~DrawableEntity();
    virtual void render(ShaderProg* shaderProgToUse_1, ShaderProg* shaderProgToUse_2, const DrawableEntity::Transformation * t);
    void setRenderingMode(DrawableEntity::renderingMode_t mode) { m_RenderingMode=mode; }
    void setAlphaBlendingEnabled(bool val) { m_AlphaBlendingEnabled=val; }
    void setClampTexturesToEdge(bool UNUSED_PARAM(val)) {/* Stub */}
    void translateTo(glm::vec3 coord) { m_ObjLocation=coord; }
    void rotate(glm::vec3 angles, glm::vec3 axis);
    void scale(float scaleFactor) { m_Scale=glm::vec3(scaleFactor,scaleFactor,scaleFactor); }
    void scale(glm::vec3 scale) { m_Scale=scale; }
    const ModelBinObj * getModelData() { return m_modelObj; }
    void setUseShaderPrograms(bool val) { m_UseShaders = val; }
    static Texture_t extractTexture(ImageUtils::ImageData_t* surface,const std::string* imgName);
    static void prepareATexture(const Texture_t& texture, bool clampToEdge, std::string textureFileName);

protected:
    typedef std::vector<DrawableEntity::Textures> textureList_t;

    DrawableEntity(GraphicsDevice& graphicsDevice,
                   FileManager& fileManager,
                   bool loadTextures,
                   bool clampTexturesToEdge);

    GraphicsDevice& m_GraphicsDevice;
    FileManager& m_FileManager;
    ModelBinObj* m_modelObj;
    textureList_t m_TexturesOfMeshes; //Textures of each mesh
    renderingMode_t m_RenderingMode;
    GLuint m_VAO; //Vertex Array Object
    GLuint m_VBO; //Vertex Buffer Object
    GLuint m_IBO; //Index Buffer Object

    bool m_AlphaBlendingEnabled;
    bool m_ClampTexturesToEdge;

    bool m_UseShaders;

    glm::vec3 m_RotationVector;
    glm::vec3 m_ObjLocation;
    glm::vec3 m_Scale;

private:
    DrawableEntity&  operator = (const DrawableEntity& other); // Prevent copying of DrawableText
    DrawableEntity(const DrawableEntity& other);               // Prevent copying of DrawableText

    void loadTextures();
    void prepareTextures();
    void deleteTextures();
    void prepareIBO_and_VBO();
    void setUpObjMaterial(const ModelBinObj::Material* mat,ShaderProg* shaderProgToUse);
};

#endif // DRAWABLEENTITY_H
