#include "RingModulatorEffect.h"
#include "Context.h"
#include "OpenALErrorChecking.h"
#include <cassert>
#include <algorithm>

namespace AudioDevice
{

//-----------------------------------------------------------------------

RingModulatorEffect::RingModulatorEffect(Context& context, EffectSlot& effectSlot)
: Effect( context, effectSlot, e_TypeRingModulator )
{

}

//-----------------------------------------------------------------------

void RingModulatorEffect::SetFrequency(float frequency)
{
    const float c_MinValue = GetMinFrequencyValue();
    const float c_MaxValue = GetMaxFrequencyValue();

    assert( ( frequency >= c_MinValue ) && ( frequency <= c_MaxValue ) );
    frequency = std::max( c_MinValue, std::min( frequency, c_MaxValue ) );

    _GetContext().Bind();

    alEffectf( GetALHandle(), AL_RING_MODULATOR_FREQUENCY, frequency );
    CheckForALErrors( _GetContext() );

    _SettingsUpdated();
}

//-----------------------------------------------------------------------

void RingModulatorEffect::SetHighpassCutoff(float cutoffValue)
{
    const float c_MinValue = GetMinHighpassCutoffValue();
    const float c_MaxValue = GetMaxHighpassCutoffValue();

    assert( ( cutoffValue >= c_MinValue ) && ( cutoffValue <= c_MaxValue ) );
    cutoffValue = std::max( c_MinValue, std::min( cutoffValue, c_MaxValue ) );

    _GetContext().Bind();

    alEffectf( GetALHandle(), AL_RING_MODULATOR_HIGHPASS_CUTOFF, cutoffValue );
    CheckForALErrors( _GetContext() );

    _SettingsUpdated();
}

//-----------------------------------------------------------------------

void RingModulatorEffect::SetWaveForm(WaveForm waveForm)
{
    const int c_MinValue = GetMinWaveFormValue();
    const int c_MaxValue = GetMaxWaveFormValue();

    assert( ( waveForm >= c_MinValue ) && ( waveForm <= c_MaxValue ) );
    waveForm = static_cast<WaveForm>( std::max( c_MinValue, std::min( static_cast<int>( waveForm ), c_MaxValue ) ) );

    _GetContext().Bind();

    alEffecti( GetALHandle(), AL_RING_MODULATOR_WAVEFORM, waveForm );
    CheckForALErrors( _GetContext() );

    _SettingsUpdated();
}

//-----------------------------------------------------------------------

float RingModulatorEffect::GetFrequency()
{
    _GetContext().Bind();

    float frequency = 0.0;

    alGetEffectf( GetALHandle(), AL_RING_MODULATOR_FREQUENCY, &frequency );
    CheckForALErrors( _GetContext() );

    return frequency;
}

//-----------------------------------------------------------------------

float RingModulatorEffect::GetHighpassCutoff()
{
    _GetContext().Bind();

    float cutoffValue = 0.0;

    alGetEffectf( GetALHandle(), AL_RING_MODULATOR_HIGHPASS_CUTOFF, &cutoffValue );
    CheckForALErrors( _GetContext() );

    return cutoffValue;
}

//-----------------------------------------------------------------------

RingModulatorEffect::WaveForm RingModulatorEffect::GetWaveForm()
{
    _GetContext().Bind();

    int waveForm = 0;

    alGetEffecti( GetALHandle(), AL_RING_MODULATOR_WAVEFORM, &waveForm );
    CheckForALErrors( _GetContext() );

    return static_cast<WaveForm>( waveForm );
}

//-----------------------------------------------------------------------

}
