#include "EffectSlot.h"
#include "EffectSlotActivationListener.h"
#include "OpenALIncludes.h"
#include "OpenALErrorChecking.h"
#include <algorithm>

namespace AudioDevice
{

//-----------------------------------------------------------------------

EffectSlot::EffectSlot(Context& context)
: m_ALEffectSlot( AL_EFFECTSLOT_NULL ),
  m_Context( context ),
  m_ConnectedSourcesList(),
  m_AttachedEffect( nullptr ),
  m_SlotSpecificGain( 1.0f )
{

}

//-----------------------------------------------------------------------

EffectSlot::~EffectSlot()
{
    if( m_ALEffectSlot != AL_EFFECTSLOT_NULL )
    {
        m_Context.Bind();

        _DetachCurrentlyAttachedEffect();

        alDeleteAuxiliaryEffectSlots( 1, &m_ALEffectSlot );
        CheckForALErrors( m_Context );
    }
}

//-----------------------------------------------------------------------

void EffectSlot::SetSlotSpecificGain(float gainValue)
{
    m_SlotSpecificGain = gainValue;

    if( m_ALEffectSlot != AL_EFFECTSLOT_NULL )
    {
        alAuxiliaryEffectSlotf( m_ALEffectSlot, AL_EFFECTSLOT_GAIN, gainValue );
        CheckForALErrors( m_Context );
    }
}

//-----------------------------------------------------------------------

float EffectSlot::GetSlotSpecificGain() const
{
    return m_SlotSpecificGain;
}

//-----------------------------------------------------------------------

void EffectSlot::SourceConnected(Source* source)
{
    static_assert( std::is_base_of<EffectSlotActivationListener, Source>::value, "\"Source\" must be a subclass of the \"EffectSlotActivationListener\"" );

    if( source )
    {
        m_ConnectedSourcesList.push_back( source );

        if( !source->IsStopped() )
        {
            _Activate();

            static_cast<EffectSlotActivationListener*>( source )->EffectSlotActivated( this );
        }
    }
}

//-----------------------------------------------------------------------

void EffectSlot::SourceDisconnected(Source* source)
{
    if( source )
    {
        m_ConnectedSourcesList.erase( std::remove( m_ConnectedSourcesList.begin(),
                                                   m_ConnectedSourcesList.end(),
                                                   source ),
                                      m_ConnectedSourcesList.end() );

        _DeactivateIfNoneOfTheAttachedSourcesArePlaying();
    }
}

//-----------------------------------------------------------------------

void EffectSlot::SourceStartedPlaying(Source* source)
{
    static_assert( std::is_base_of<EffectSlotActivationListener, Source>::value, "\"Source\" must be a subclass of the \"EffectSlotActivationListener\"" );

    if( _IsKnownConnectedSource( source ) )
    {
        _Activate();

        static_cast<EffectSlotActivationListener*>( source )->EffectSlotActivated( this );
    }
}

//-----------------------------------------------------------------------

void EffectSlot::SourceStoppedPlaying(Source* source)
{
    static_assert( std::is_base_of<EffectSlotActivationListener, Source>::value, "\"Source\" must be a subclass of the \"EffectSlotActivationListener\"" );

    if( _IsKnownConnectedSource( source ) )
    {
        _DeactivateIfNoneOfTheAttachedSourcesArePlaying();

        static_cast<EffectSlotActivationListener*>( source )->EffectSlotDeactivated( this );
    }
}

//-----------------------------------------------------------------------

void EffectSlot::EffectSettingsUpdated()
{
    // The effect needs to be detached and attached again
    // for the settings update to take effect
    _DeactivateAttachedEffect();
    _ActivateAttachedEffect();
}

//-----------------------------------------------------------------------

bool EffectSlot::_IsKnownConnectedSource(Source* source) const
{
    bool retVal = false;

    if( source )
    {
        const size_t registeredSourceCount = m_ConnectedSourcesList.size();

        for(size_t i = 0; !retVal && ( i < registeredSourceCount ); i++)
        {
            if( source == m_ConnectedSourcesList[i] )
            {
                retVal = true;
            }
        }
    }

    return retVal;
}

//-----------------------------------------------------------------------

void EffectSlot::_Activate()
{
    if( m_ALEffectSlot == AL_EFFECTSLOT_NULL )
    {
        m_Context.Bind();

        // Acquire a new effect slot from OpenAL
        alGenAuxiliaryEffectSlots( 1, &m_ALEffectSlot );
        CheckForALErrors( m_Context );

        SetSlotSpecificGain( m_SlotSpecificGain );

        _ActivateAttachedEffect();
    }
}

//-----------------------------------------------------------------------

void EffectSlot::_Deactivate()
{
    if( m_ALEffectSlot != AL_EFFECTSLOT_NULL )
    {
        _DeactivateAttachedEffect();

        m_Context.Bind();

        // Release the unused effect slot back to OpenAL to avoid excessive CPU usage
        alDeleteAuxiliaryEffectSlots( 1, &m_ALEffectSlot );
        CheckForALErrors( m_Context );

        m_ALEffectSlot = AL_EFFECTSLOT_NULL;
    }
}

//-----------------------------------------------------------------------

void EffectSlot::_DeactivateIfNoneOfTheAttachedSourcesArePlaying()
{
    if( m_ConnectedSourcesList.empty() )
    {
        _Deactivate();
    }
    else
    {
        const size_t connectedSourceCount = m_ConnectedSourcesList.size();
        bool allStopped = true;
        for(size_t i = 0; (allStopped && ( i < connectedSourceCount ) ); i++)
        {
            if( !m_ConnectedSourcesList[i]->IsStopped() )
            {
                allStopped = false;
            }
        }

        if( allStopped )
        {
            _Deactivate();
        }
    }
}

//-----------------------------------------------------------------------

void EffectSlot::_AttachAnEffect(Effect* effect)
{
    if( effect )
    {
        _DetachCurrentlyAttachedEffect();
        m_AttachedEffect.reset( effect );

        if( m_ALEffectSlot != AL_EFFECTSLOT_NULL )
        {
            _ActivateAttachedEffect();
        }
    }
}

//-----------------------------------------------------------------------

void EffectSlot::_DetachCurrentlyAttachedEffect()
{
    _DeactivateAttachedEffect();

    if( m_AttachedEffect != nullptr )
    {
        m_AttachedEffect.release();
    }
}

//-----------------------------------------------------------------------

void EffectSlot::_ActivateAttachedEffect()
{
    if( ( m_ALEffectSlot != AL_EFFECTSLOT_NULL ) && ( m_AttachedEffect != nullptr ) )
    {
        m_Context.Bind();

        alAuxiliaryEffectSloti( m_ALEffectSlot, AL_EFFECTSLOT_EFFECT, m_AttachedEffect->GetALHandle() );
        CheckForALErrors( m_Context );
    }
}

//-----------------------------------------------------------------------

void EffectSlot::_DeactivateAttachedEffect()
{
    if( m_ALEffectSlot != AL_EFFECTSLOT_NULL )
    {
        m_Context.Bind();

        alAuxiliaryEffectSloti( m_ALEffectSlot, AL_EFFECTSLOT_EFFECT, AL_EFFECT_NULL );
        CheckForALErrors( m_Context );
    }
}

//-----------------------------------------------------------------------

}
