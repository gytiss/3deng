#ifndef AUDIODEVICE_BUFFER_H_INCLUDED
#define AUDIODEVICE_BUFFER_H_INCLUDED

class AudioData;

namespace AudioDevice
{

class Context;

class Buffer
{
public:
    /**
     * Creates an object with a brand new OpenAL buffer
     */
    Buffer(Context& context);

    Buffer(Buffer&& other);

    /**
     * Creates an object from an already existing OpenAL buffer (e.g. one poped from an OpenAL Source buffer queue)
     *
     * @param alBufferHandle - OpenAL handle of the OpenAL buffer to use
     */
    Buffer(Context& context, unsigned int alBufferHandle);

    ~Buffer();

    inline unsigned int GetALHandle()
    {
        return m_ALBuffer;
    }

    /**
     * Load audio data into the buffer
     *
     * @param audioData - Audio data to load
     */
    void Feed(const AudioData& audioData);

    /**
     * Make this buffer object no longer valid by transferring
     * ownership of its internal AL handle to a third party
     */
    inline unsigned int TransferALHandleOwnership()
    {
        unsigned int valueToReturn = m_ALBuffer;
        m_ALBuffer = 0;

        return valueToReturn;
    }

private:
    // Used to prevent copying
    Buffer& operator = (const Buffer& other) = delete;
    Buffer(const Buffer& other) = delete;

    unsigned int m_ALBuffer;
    Context& m_Context;
};

}

#endif // AUDIODEVICE_BUFFER_H_INCLUDED
