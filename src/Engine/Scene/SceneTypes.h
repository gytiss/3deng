#ifndef SCENETYPES_H_INCLUDED
#define SCENETYPES_H_INCLUDED

#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>

namespace SceneTypes
{
    struct Transform
    {
        glm::vec3 position;
        glm::quat rotation;
        glm::vec3 scale;
    };
}

#endif // SCENETYPES_H_INCLUDED
