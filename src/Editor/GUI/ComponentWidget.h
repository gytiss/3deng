#ifndef COMPONENTWIDGETS_H_INCLUDED
#define COMPONENTWIDGETS_H_INCLUDED

#include <QtWidgets/QWidget>
#include "ComponentFloatValueValidator.h"
#include "GUIHelpers.h"
#include "GUIDefaults.h"
#include <GUI/CategorizedListWidget.h>

class ComponentWidget : public QWidget
{
    Q_OBJECT

public:
    ComponentWidget(CategorizedListWidget* listWidgetToAddOurselvesTo,
                    const std::string& name,
                    QWidget* parent = 0);

    virtual ~ComponentWidget();

    void SetHidden(bool value);

    const QString& GetName() const
    {
        return m_Name;
    }

    virtual void ResetPaletteToDefault() = 0;

protected:
    ComponentFloatValueValidator m_FloatValueValidator;

private:
    const QString m_Name;
    CategorizedListWidget::Category m_CategoryHandle;
};
 
#endif // COMPONENTWIDGETS_H_INCLUDED
