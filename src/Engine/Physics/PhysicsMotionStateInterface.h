#ifndef PHYSICSMOTIONSTATEINTERFACE_H_INCLUDED
#define PHYSICSMOTIONSTATEINTERFACE_H_INCLUDED

#include <Scene/SceneTypes.h>
#include <bullet/LinearMath/btMotionState.h>

/**
 * This class is used for interfacing Physics representation of the world
 * with the Scene representation of the world.
 */
class PhysicsMotionStateInterface : public btMotionState
{
public:
    virtual ~PhysicsMotionStateInterface() {}

    /**
     * Sets position, rotation and scale of a scene node
     * implementing this interface.
     *
     * @param t - Transformation
     */
    virtual void SetSceneTransform(const SceneTypes::Transform& t) = 0;

    /**
     * Retrieves position, rotation and scale of a scene node
     * implementing this interface.
     *
     * @param t - (Out) Transformation
     */
    virtual void GetSceneTransform(SceneTypes::Transform& t) const = 0;

    virtual void getWorldTransform(btTransform& worldTrans) const
    {
        SceneTypes::Transform t;
        GetSceneTransform( t );

        worldTrans.setIdentity();
        worldTrans.setOrigin( btVector3( t.position.x, t.position.y, t.position.z ) );
        worldTrans.setRotation( btQuaternion ( t.rotation.x,
                                               t.rotation.y,
                                               t.rotation.z,
                                               t.rotation.w ) );
    }

    virtual void setWorldTransform(const btTransform& worldTrans)
    {
        SceneTypes::Transform t;
        const btVector3& pos = worldTrans.getOrigin();
        const btQuaternion& rotation = worldTrans.getRotation();

        t.position = glm::vec3( pos.x(), pos.y(), pos.z() );
        t.rotation = glm::quat( rotation.w(),
                                rotation.x(),
                                rotation.y(),
                                rotation.z() );
        SetSceneTransform( t );
    }
};

#endif // PHYSICSMOTIONSTATEINTERFACE_H_INCLUDED
