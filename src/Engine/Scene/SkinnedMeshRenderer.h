#ifndef SKINNEDMESHRENDERER_H_INCLUDED
#define SKINNEDMESHRENDERER_H_INCLUDED

#include <string>
#include <vector>
#include <EngineTypes.h>
#include "Renderer.h"

class GameObject;
class MeshManager;
class Animator;

class SkinnedMeshRenderer : public Renderer
{
    GAME_OBJECT_COMPONENT_TYPE(e_TypeRenderer);

public:
    friend class Animator;

    /**
     * @param meshManager - Mesh manager which will be used for handling meshes
     * @param materialManager - Material manager which will be used for handling materials of the submeshes
     */
    SkinnedMeshRenderer(GameObject& owningGameObject,
                        MeshManager& meshManager,
                        MaterialManager& materialManager);

    ~SkinnedMeshRenderer();

    /**
     * @param handle - Handle of the mesh this renderer will be using (Note: Mesh renderer takes ownership of this mesh handle)
     */
    void SetMesh(MeshManager::MeshHandle skinnedMeshHandle);

    const EngineTypes::AABB& GetAABB() const;

private:
    MeshManager::MeshHandle _GetMeshHandle() const override;
    Animator* _FindAnAnimatorInTheParentHierarchy();
    void _RegenerateRenderables() override;
    void _ClearMaterialsOfTheSubMeshes();

    MeshManager& m_MeshManager;
    MeshManager::MeshHandle m_MeshHandle;
    EngineTypes::MatrixArray m_AnimatedBones; // Animated bone matrices from the animation with the inverse bind pose applied.
};

#endif // SKINNEDMESHRENDERER_H_INCLUDED
