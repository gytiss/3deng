#ifndef COMPOSITETRANSFORMNODE_H_INCLUDED
#define COMPOSITETRANSFORMNODE_H_INCLUDED

#include <CommonDefinitions.h>
#include <Physics/PhysicsMotionStateInterface.h>
#include <MathUtils.h>

class CompositeTransformNode : public PhysicsMotionStateInterface
{
public:
    CompositeTransformNode()
    : m_ParentNode( nullptr ),
      m_Transform(),
      m_TransformMatrix( MathUtils::c_IdentityMatrix ),
      m_TransformMatrixCacheIsValid( false )
    {
        m_Transform.position = glm::vec3( 0.0f, 0.0f, 0.0f );
        m_Transform.rotation = MathUtils::c_IdentityQuaternion;
    }

    virtual void SetSceneTransform(const SceneTypes::Transform& t)
    {
        m_Transform = t;
        m_TransformMatrixCacheIsValid = false;
    }

    virtual void GetSceneTransform(SceneTypes::Transform& t) const
    {
        t = MathUtils::MatrixToSceneTransform( GetTransformMatrix() );
    }

    void SetPosition(const glm::vec3& pos)
    {
        m_Transform.position = pos;
        m_TransformMatrixCacheIsValid = false;
    }

    const glm::vec3& GetPosition() const
    {
        return m_Transform.position;
    }

    void SetRotation(const glm::quat& rot)
    {
        m_Transform.rotation = rot;
        m_TransformMatrixCacheIsValid = false;
    }

    const glm::quat& GetRotation() const
    {
        return m_Transform.rotation;
    }

    const SceneTypes::Transform& GetTransform() const
    {
        return m_Transform;
    }

    glm::mat4 GetTransformMatrix() const
    {
        if( !m_TransformMatrixCacheIsValid )
        {
            m_TransformMatrix = glm::translate( MathUtils::c_IdentityMatrix, m_Transform.position );
            m_TransformMatrix = m_TransformMatrix * glm::toMat4( m_Transform.rotation );
            m_TransformMatrixCacheIsValid = true;
        }

        glm::mat4 retVal = m_TransformMatrix;

        if( m_ParentNode )
        {
            retVal = retVal * m_ParentNode->GetTransformMatrix();
        }

        return retVal;
    }

    void AddParent(CompositeTransformNode* p)
    {
        m_ParentNode = p;
    }

private:
    CompositeTransformNode* m_ParentNode;
    SceneTypes::Transform m_Transform;
    mutable glm::mat4 m_TransformMatrix;
    mutable bool m_TransformMatrixCacheIsValid;
};

#endif // COMPOSITETRANSFORMNODE_H_INCLUDED
