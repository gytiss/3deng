#ifndef ANIMATIONMANAGER_H_INCLUDED
#define ANIMATIONMANAGER_H_INCLUDED

#include <map>
#include <list>
#include <string>
#include <MObjModel/MAnim.h>
#include <EngineTypes.h>

class GraphicsDevice;
class SystemInterface;
class FileManager;

class AnimationManager
{
public:
    class AnimationResultsListener
    {
    public:
        /**
         * Called every time skeleton data is updated by the animation
         */
        virtual void AnimatedSkeletonChanged(const EngineTypes::MatrixArray& animatedSkeleton) = 0;
    };

private:
    struct RefCountedAnimationPtr;
    typedef std::list<RefCountedAnimationPtr> AnimationList;
    typedef std::map<std::string, AnimationList::iterator> AnimationPathsToAnimationsMap;
    typedef std::pair<AnimationList::iterator, AnimationResultsListener*> ActiveAnimationRecord;
    typedef std::list<ActiveAnimationRecord> ActiveAnimationsList;

    struct RefCountedAnimationPtr
    {
        RefCountedAnimationPtr()
        : refCount( 0 ),
          animationFilePath(),
          animation( nullptr ),
          isCurrentlyActive( false ),
          isLooping( false ),
          activeAnimationListIt()
        {
        }

        RefCountedAnimationPtr& operator= (const RefCountedAnimationPtr& other)
        {
            refCount = other.refCount;
            animationFilePath = other.animationFilePath;
            animation = other.animation;
            isCurrentlyActive = other.isCurrentlyActive;
            isLooping = other.isLooping;
            activeAnimationListIt = other.activeAnimationListIt;

            return *this;
        }

        RefCountedAnimationPtr(const RefCountedAnimationPtr& other)
        {
            operator=( other );
        }

        unsigned int refCount;
        std::string animationFilePath;
        MAnim* animation;
        bool isCurrentlyActive;
        bool isLooping;
        ActiveAnimationsList::iterator activeAnimationListIt;
    };

public:
    class AnimationHandle
    {
    public:
        friend class AnimationManager;

        AnimationHandle()
        : m_AnimationListIt(),
          m_Valid( false )
        {
        }

        AnimationHandle& operator= (const AnimationHandle& other)
        {
            m_AnimationListIt = other.m_AnimationListIt;
            m_Valid = other.m_Valid;
            return *this;
        }

        AnimationHandle(const AnimationHandle& other)
        {
            operator= ( other );
        }

        MAnim* operator->()
        {
            return (*m_AnimationListIt).animation;
        }

        const MAnim* operator->() const
        {
            return (*m_AnimationListIt).animation;
        }

        MAnim& operator*()
        {
            return *(*m_AnimationListIt).animation;
        }

        const MAnim& operator*() const
        {
            return *(*m_AnimationListIt).animation;
        }

        bool IsValid() const
        {
            return m_Valid;
        }

    protected:
        void Invalidate()
        {
            m_Valid = false;
        }

        AnimationHandle(AnimationList::iterator AnimationListIt)
        : m_AnimationListIt( AnimationListIt ),
          m_Valid( true )
        {
        }

        AnimationList::iterator m_AnimationListIt;  // Iterator to the animations list i.e. m_AnimationList
        bool m_Valid;
    };

    AnimationManager(GraphicsDevice& graphicsDevice,
                     SystemInterface& systemInterface,
                     FileManager& fileManager);
    ~AnimationManager();

    void UpdateCurrentlyActiveAnimations();

    /**
     * Adds animation to currently playing animations list. ModelManager ensures that animations in this list are played.
     */
    void ActivateAnimation(AnimationHandle& handle, AnimationResultsListener* animationResultsListener, bool loop = true);

    /**
     * Removes animations from currently playing animations list
     */
    void DeactivateAnimation(AnimationHandle& handle);

    /**
     * Check if the given animation is currently playing
     */
    bool IsAnimationActive(AnimationHandle& handle);

    AnimationHandle LoadAnimation(const std::string& animationFilePath);
    void ReleaseAnimation(AnimationHandle& handle);

    bool CheckIfAnimationsAreInterchangeable(const AnimationHandle& animation1Handle,
                                             const AnimationHandle& animation2Handle);

private:
    // Used to prevent copying
    AnimationManager& operator = (const AnimationManager& other);
    AnimationManager(const AnimationManager& other);

    void _DeactivateAnimation(AnimationList::iterator it);

    AnimationList::iterator _LoadAnimationFromFile(const std::string& animationFilePath);

    ActiveAnimationsList m_ActiveAnimationsList;
    AnimationList m_AnimationList;
    AnimationPathsToAnimationsMap m_AnimationPathsToAnimationsMap;
    GraphicsDevice& m_GraphicsDevice;
    SystemInterface& m_SystemInterface;
    FileManager& m_FileManager;
    static bool m_ClassAlreadyInstantiated;
};

#endif // ANIMATIONMANAGER_H_INCLUDED
