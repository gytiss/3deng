#include "Source.h"
#include "Buffer.h"
#include "EffectSlot.h"
#include "OpenALIncludes.h"
#include "OpenALErrorChecking.h"
#include <Exceptions/GenericException.h>
#include <Resources/Audio/AudioData.h>
#include <cassert>
#include <algorithm>
#include <type_traits>

namespace AudioDevice
{

//-----------------------------------------------------------------------

const ALenum Source::m_FloatParamToOpenALSourceFloatParamMap[e_FloatParamCount] =
{
    AL_PITCH,
    AL_GAIN,
    AL_MAX_DISTANCE,
    AL_ROLLOFF_FACTOR,
    AL_REFERENCE_DISTANCE,
    AL_MIN_GAIN,
    AL_MAX_GAIN,
    AL_CONE_OUTER_GAIN,
    AL_CONE_INNER_ANGLE,
    AL_CONE_OUTER_ANGLE,
};

//-----------------------------------------------------------------------

const Source::FloatParamValues Source::m_DefaultFloatParamValues =
{
    1.0f,           // e_FloatParamPitch
    1.0f,           // e_FloatParamGain
    3.40282e+38f,   // e_FloatParamMaxDistance
    1.0f,           // e_FloatParamRolloffFactor
    1.0f,           // e_FloatParamReferenceDistance
    0.0f,           // e_FloatParamMinGain
    1.0f,           // e_FloatParamMaxGain
    0.0f,           // e_FloatParamConeOuterGain
    360.0f,         // e_FloatParamConeInnerAngle
    360.0f          // e_FloatParamConeOuterAngle
};

//-----------------------------------------------------------------------

Source::Source(Context& context)
: m_ALSource( 0 ),
  m_Context( context ),
  m_CurrentSourceType( e_SourceTypeUninitialised ),
  m_EffectSlotsConnectedTo( { nullptr } )
{
    m_Context.Bind();

    alGenSources( 1, &m_ALSource );
    CheckForALErrors( m_Context );

    SetIsRelative( false );
}

//-----------------------------------------------------------------------

Source::~Source()
{
    if( m_ALSource != 0 )
    {
        m_Context.Bind();

        _DisconnectAllAuxiliaryEffectsConnectionLines();

        alDeleteSources( 1, &m_ALSource );
        CheckForALErrors( m_Context );
    }
}

//-----------------------------------------------------------------------

void Source::Play()
{
    static_assert( std::is_base_of<SourceConnectionListener, EffectSlot>::value, "\"EffectSlot\" must be a subclass of the \"SourceStatusListener\"" );

    // Notify all of the concerned effect slots that this source has started playing
    for(EffectSlot* effectSlot : m_EffectSlotsConnectedTo)
    {
        if( effectSlot != nullptr )
        {
            static_cast<SourceStatusListener*>( effectSlot )->SourceStartedPlaying( this );
        }
    }

    m_Context.Bind();

    alSourcePlay( m_ALSource );
    CheckForALErrors( m_Context );
}

//-----------------------------------------------------------------------

void Source::Pause()
{
    m_Context.Bind();

    alSourcePause( m_ALSource );
    CheckForALErrors( m_Context );
}

//-----------------------------------------------------------------------

void Source::Stop()
{
    static_assert( std::is_base_of<SourceConnectionListener, EffectSlot>::value, "\"EffectSlot\" must be a subclass of the \"SourceStatusListener\"" );

    m_Context.Bind();

    alSourceStop( m_ALSource );
    CheckForALErrors( m_Context );

    // Notify all of the concerned effects slots that this source has stopped playing
    for(EffectSlot* effectSlot : m_EffectSlotsConnectedTo)
    {
        if( effectSlot != nullptr )
        {
            static_cast<SourceStatusListener*>( effectSlot )->SourceStoppedPlaying( this );
        }
    }
}

//-----------------------------------------------------------------------

bool Source::IsStopped()
{
    m_Context.Bind();

    ALint sourceState = AL_STOPPED;

    alGetSourcei( m_ALSource, AL_SOURCE_STATE, &sourceState );
    CheckForALErrors( m_Context );

    return ( ( sourceState == AL_STOPPED ) ||  ( sourceState == AL_INITIAL ) );
}

//-----------------------------------------------------------------------

void Source::SetFloatParameter(FloatParam param, float value)
{
    assert( ( param < e_FloatParamCount ) && "Source FloatParam must be less than e_FloatParamCount" );

    if( ( param == e_FloatParamPitch ) || ( param == e_FloatParamGain ) )
    {
        assert( ( value >= 0.0f ) && "Values of the  pitch multiplier and gain should always be positive" );
    }

    m_Context.Bind();

    alSourcef( GetALHandle(), m_FloatParamToOpenALSourceFloatParamMap[param], value );
    CheckForALErrors( m_Context );
}

//-----------------------------------------------------------------------

void Source::SetFloatParameters(const FloatParamValues& values)
{
    for(size_t i = 0; i < AudioDevice::Source::e_FloatParamCount; i++)
    {
        SetFloatParameter( static_cast<FloatParam>( i ), values[i] );
    }
}

//-----------------------------------------------------------------------

float Source::GetFloatParameter(FloatParam param)
{
    assert( ( param < e_FloatParamCount ) && "Source FloatParam must be less than e_FloatParamCount" );

    float retVal = 0.0;

    m_Context.Bind();

    alGetSourcef( GetALHandle(), m_FloatParamToOpenALSourceFloatParamMap[param], &retVal );
    CheckForALErrors( m_Context );

    return retVal;
}

//-----------------------------------------------------------------------

void Source::SetPosition(const glm::vec3& position)
{
    ALfloat positionValues[3] = { position.x, position.y, position.z };
    alSource3f( GetALHandle(),
                AL_POSITION,
                positionValues[0],
                positionValues[1],
                positionValues[2] );
    CheckForALErrors( m_Context );
}

//-----------------------------------------------------------------------

void Source::SetVelocity(const glm::vec3& velocityVector)
{
    ALfloat velocityVectorValues[3] = { velocityVector.x,
                                        velocityVector.y,
                                        velocityVector.z };
    alSource3f( GetALHandle(),
                AL_VELOCITY,
                velocityVectorValues[0],
                velocityVectorValues[1],
                velocityVectorValues[2] );
    CheckForALErrors( m_Context );
}

//-----------------------------------------------------------------------

void Source::SetDirection(const glm::vec3& directionVector)
{
    ALfloat directionVectorValues[3] = { directionVector.x,
                                         directionVector.y,
                                         directionVector.z };
    alSource3f( GetALHandle(),
                AL_DIRECTION,
                directionVectorValues[0],
                directionVectorValues[1],
                directionVectorValues[2] );
    CheckForALErrors( m_Context );
}

//-----------------------------------------------------------------------

glm::vec3 Source::GetPosition()
{
    ALfloat positionValues[3] = { 0.0f };
    alGetSource3f( GetALHandle(),
                   AL_POSITION,
                   &positionValues[0],
                   &positionValues[1],
                   &positionValues[2] );
    CheckForALErrors( m_Context );

    return glm::vec3( positionValues[0], positionValues[1], positionValues[2] );
}

//-----------------------------------------------------------------------

glm::vec3 Source::GetVelocity()
{
    ALfloat velocityValues[3] = { 0.0f };
    alGetSource3f( GetALHandle(),
                   AL_VELOCITY,
                   &velocityValues[0],
                   &velocityValues[1],
                   &velocityValues[2] );
    CheckForALErrors( m_Context );

    return glm::vec3( velocityValues[0], velocityValues[1], velocityValues[2] );
}

//-----------------------------------------------------------------------

glm::vec3 Source::GetDirection()
{
    ALfloat directionValues[3] = { 0.0f };
    alGetSource3f( GetALHandle(),
                   AL_DIRECTION,
                   &directionValues[0],
                   &directionValues[1],
                   &directionValues[2] );
    CheckForALErrors( m_Context );

    return glm::vec3( directionValues[0], directionValues[1], directionValues[2] );
}

//-----------------------------------------------------------------------

void Source::SetIsRelative(bool value)
{
    alSourcei( m_ALSource , AL_SOURCE_RELATIVE, ( value == true ? AL_TRUE : AL_FALSE ) );
    CheckForALErrors( m_Context );
}

//-----------------------------------------------------------------------

bool Source::IsRelative()
{
    ALint isSourceRelative = AL_FALSE;

    alGetSourcei( m_ALSource, AL_SOURCE_RELATIVE, &isSourceRelative );
    CheckForALErrors( m_Context );

    return ( isSourceRelative == AL_TRUE );
}

//-----------------------------------------------------------------------

void Source::SetIsLooping(bool value)
{
    alSourcei( m_ALSource , AL_LOOPING, ( value == true ? AL_TRUE : AL_FALSE ) );
    CheckForALErrors( m_Context );
}

//-----------------------------------------------------------------------

bool Source::IsLooping()
{
    ALint isSourceLooping = AL_FALSE;

    alGetSourcei( m_ALSource, AL_LOOPING, &isSourceLooping );
    CheckForALErrors( m_Context );

    return ( isSourceLooping == AL_TRUE );
}

//-----------------------------------------------------------------------

bool Source::ConnectToAnEffectSlot(EffectSlot* effectSlot)
{
    static_assert( std::is_base_of<SourceConnectionListener, EffectSlot>::value, "\"EffectSlot\" must be a subclass of the \"SourceConnectionListener\"" );

    bool retVal = false;

    if( effectSlot )
    {
        for(size_t connectionLineIndex = 0; !retVal && ( connectionLineIndex < m_EffectSlotsConnectedTo.size() ); connectionLineIndex++)
        {
            // If the connection line is not yet occupied
            if( m_EffectSlotsConnectedTo[connectionLineIndex] == nullptr )
            {
                m_EffectSlotsConnectedTo[connectionLineIndex] = effectSlot;
                static_cast<SourceConnectionListener*>(effectSlot)->SourceConnected( this );
                retVal = true;
            }
        }
    }

    return retVal;
}

//-----------------------------------------------------------------------

bool Source::DisconnectFromAnEffectSlot(EffectSlot* effectSlot)
{
    static_assert( std::is_base_of<SourceConnectionListener, EffectSlot>::value, "\"EffectSlot\" must be a subclass of the \"SourceConnectionListener\"" );

    bool retVal = false;

    if( effectSlot )
    {
        for(size_t connectionLineIndex = 0; !retVal && ( connectionLineIndex < m_EffectSlotsConnectedTo.size() ); connectionLineIndex++)
        {
            // If the effect slots is one of the effects slots we are connected to
            if( m_EffectSlotsConnectedTo[connectionLineIndex] == effectSlot )
            {
                _DisconnectAuxiliaryEffectsConnectionLine( connectionLineIndex );
                retVal = true;
            }
        }
    }

    return retVal;
}

//-----------------------------------------------------------------------

void Source::ResetToDefaultState()
{
    Stop();

    if ( m_CurrentSourceType == e_SourceTypeStreaming )
    {
        ClearBuffersQueue();
    }

    ResetBuffer();
    _DisconnectAllAuxiliaryEffectsConnectionLines();

    for(size_t i = 0; i < e_FloatParamCount; i++)
    {
        SetFloatParameter( static_cast<FloatParam>( i ), m_DefaultFloatParamValues[i] );
    }

    SetPosition( glm::vec3( 0.0f ) );
    SetVelocity( glm::vec3( 0.0f ) );
    SetDirection( glm::vec3( 0.0f ) );

    SetIsRelative( false );
    SetIsLooping( false );
}

//-----------------------------------------------------------------------

void Source::SetSourceType(SourceType sourceType)
{
    if( sourceType != m_CurrentSourceType )
    {
        if( m_CurrentSourceType == e_SourceTypeStreaming )
        {
            ClearBuffersQueue();
        }
        ResetBuffer();

        m_CurrentSourceType = sourceType;
    }
}

//-----------------------------------------------------------------------

void Source::GetDefaultFloatParamValue(FloatParamValues& outFloatParamValues)
{
    std::copy( m_DefaultFloatParamValues.cbegin(),
               m_DefaultFloatParamValues.cend(),
               outFloatParamValues.begin() );
}

//-----------------------------------------------------------------------

void Source::SetBuffer(Buffer& buffer)
{
    if( m_CurrentSourceType == e_SourceTypeStatic )
    {
        // To avoid OpenAL errors, source needs to be stopped before setting a new buffer
        if( !IsStopped() )
        {
            Stop();
        }

        alSourcei( GetALHandle() , AL_BUFFER, buffer.GetALHandle() );
        CheckForALErrors( _GetContext() );
    }
}

//-----------------------------------------------------------------------

void Source::ResetBuffer()
{
    // To avoid OpenAL errors, source needs to be stopped before resetting the buffer
    if( !IsStopped() )
    {
        Stop();
    }

    alSourcei( GetALHandle() , AL_BUFFER, 0 );
    CheckForALErrors( _GetContext() );
}

//-----------------------------------------------------------------------

void Source::QueueBuffer(Buffer& buffer)
{
    if( m_CurrentSourceType == e_SourceTypeStreaming )
    {
        const unsigned int alBufferHandle = buffer.TransferALHandleOwnership();

        alSourceQueueBuffers( GetALHandle(), 1, &alBufferHandle );
        CheckForALErrors( _GetContext() );
    }
}

//-----------------------------------------------------------------------

Buffer Source::DequeueBuffer()
{
    unsigned int alBufferHandle = 0;

    if( m_CurrentSourceType == e_SourceTypeStreaming )
    {
        alSourceUnqueueBuffers( GetALHandle(), 1, &alBufferHandle );
        CheckForALErrors( _GetContext() );
    }

    return Buffer( _GetContext(), alBufferHandle );
}

//-----------------------------------------------------------------------

void Source::ClearBuffersQueue()
{
    // If the source type is not "streaming" then there are no buffers in the internal buffer queue of the source
    if( m_CurrentSourceType == e_SourceTypeStreaming )
    {
        Stop(); // Source must be stopped before all of the queued buffers can be removed

        const size_t totalQueuedBufferCount = GetQueuedBufferCount();
        for(size_t i = 0; i < totalQueuedBufferCount; i++)
        {
            DequeueBuffer();
        }
    }
}

//-----------------------------------------------------------------------

size_t Source::GetProcessedBufferCount()
{
    int count = 0;

    if( m_CurrentSourceType == e_SourceTypeStreaming )
    {
        alGetSourcei( GetALHandle(), AL_BUFFERS_PROCESSED, &count );
        CheckForALErrors( _GetContext() );

        assert( ( count > 0 ) && "This value can never be negative" );
    }

    return count;
}

//-----------------------------------------------------------------------

size_t Source::GetQueuedBufferCount()
{
    int count = 0;

    if( m_CurrentSourceType == e_SourceTypeStreaming )
    {
        alGetSourcei( GetALHandle(), AL_BUFFERS_QUEUED, &count );
        CheckForALErrors( _GetContext() );

        assert( ( count > 0 ) && "This value can never be negative" );
    }

    return count;
}

//-----------------------------------------------------------------------

void Source::EffectSlotActivated(EffectSlot* effectSlot)
{
    if( effectSlot )
    {
        bool finished = false;

        for(size_t connectionLineIndex = 0; !finished && ( connectionLineIndex < m_EffectSlotsConnectedTo.size() ); connectionLineIndex++)
        {
            // If the effect slots is one of the effects slots we are connected to
            if( m_EffectSlotsConnectedTo[connectionLineIndex] == effectSlot )
            {
                m_Context.Bind();

                // TODO add support for non "AL_FILTER_NULL" filters
                alSource3i( m_ALSource, AL_AUXILIARY_SEND_FILTER, effectSlot->GetALHandle(), static_cast<ALint>( connectionLineIndex ), AL_FILTER_NULL );
                CheckForALErrors( m_Context );

                finished = true;
            }
        }
    }
}

//-----------------------------------------------------------------------

void Source::EffectSlotDeactivated(EffectSlot* effectSlot)
{
    if( effectSlot )
    {
        bool finished = false;

        for(size_t connectionLineIndex = 0; !finished && ( connectionLineIndex < m_EffectSlotsConnectedTo.size() ); connectionLineIndex++)
        {
            // If the effect slots is one of the effects slots we are connected to
            if( m_EffectSlotsConnectedTo[connectionLineIndex] == effectSlot )
            {
                m_Context.Bind();

                // TODO add support for non "AL_FILTER_NULL" filters
                alSource3i( m_ALSource, AL_AUXILIARY_SEND_FILTER, AL_EFFECTSLOT_NULL, static_cast<ALint>( connectionLineIndex ), AL_FILTER_NULL );
                CheckForALErrors( m_Context );

                finished = true;
            }
        }
    }
}

//-----------------------------------------------------------------------

Source::OpenALSourceType Source::_GetCurrentSourceType()
{
    m_Context.Bind();

    ALint openALsourceType = AL_UNDETERMINED;

    alGetSourcei( m_ALSource, AL_SOURCE_TYPE, &openALsourceType );
    CheckForALErrors( m_Context );

    return static_cast<OpenALSourceType>( openALsourceType );
}

//-----------------------------------------------------------------------

void Source::_DisconnectAllAuxiliaryEffectsConnectionLines()
{
    for(size_t connectionLineIndex = 0; connectionLineIndex < m_EffectSlotsConnectedTo.size(); connectionLineIndex++)
    {
        _DisconnectAuxiliaryEffectsConnectionLine( connectionLineIndex );
    }
}

//-----------------------------------------------------------------------

void Source::_DisconnectAuxiliaryEffectsConnectionLine(size_t lineIndex)
{
    if( ( lineIndex < m_EffectSlotsConnectedTo.size() ) &&
        ( m_EffectSlotsConnectedTo[lineIndex] != nullptr ) )
    {
        m_Context.Bind();

        alSource3i( m_ALSource, AL_AUXILIARY_SEND_FILTER, AL_EFFECTSLOT_NULL, static_cast<ALint>( lineIndex ), AL_FILTER_NULL );
        CheckForALErrors( m_Context );

        static_cast<SourceConnectionListener*>( m_EffectSlotsConnectedTo[lineIndex] )->SourceDisconnected( this );
        m_EffectSlotsConnectedTo[lineIndex] = nullptr;
    }
}

//-----------------------------------------------------------------------

}
