/*
 * the function prototype
 */

#ifdef __cplusplus
extern "C" {
#endif

jstring Java_org_assetbridge_Assetbridge_setassetdir(JNIEnv*, jobject, jstring);

#ifdef __cplusplus
}
#endif