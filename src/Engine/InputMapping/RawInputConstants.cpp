#include "RawInputConstants.h"
#include <CommonDefinitions.h>

namespace InputMapping
{
	//-----------------------------------------------------------------------

    static const struct
    {
        const char* buttonName;
        RawInputButton button;
    }
    c_RawInputButtonNames[e_RawInputButtonCount] =
    {
        { "RawInputButtonReturn", e_RawInputButtonReturn },
        { "RawInputButtonEscape", e_RawInputButtonEscape },
        { "RawInputButtonBackspace", e_RawInputButtonBackspace },
        { "RawInputButtonTab", e_RawInputButtonTab },
        { "RawInputButtonSpace", e_RawInputButtonSpace },
        { "RawInputButtonExclaim", e_RawInputButtonExclaim },
        { "RawInputButtonQuoteDbl", e_RawInputButtonQuoteDbl },
        { "RawInputButtonHash", e_RawInputButtonHash },
        { "RawInputButtonPercent", e_RawInputButtonPercent },
        { "RawInputButtonDollar", e_RawInputButtonDollar },
        { "RawInputButtonAmpersand", e_RawInputButtonAmpersand },
        { "RawInputButtonQuote", e_RawInputButtonQuote },
        { "RawInputButtonLeftParen", e_RawInputButtonLeftParen },
        { "RawInputButtonRightParent", e_RawInputButtonRightParent },
        { "RawInputButtonAsterisk", e_RawInputButtonAsterisk },
        { "RawInputButtonPlus", e_RawInputButtonPlus },
        { "RawInputButtonComma", e_RawInputButtonComma },
        { "RawInputButtonMinus", e_RawInputButtonMinus },
        { "RawInputButtonPeriod", e_RawInputButtonPeriod },
        { "RawInputButtonSlash", e_RawInputButtonSlash },
        { "RawInputButton0", e_RawInputButton0 },
        { "RawInputButton1", e_RawInputButton1 },
        { "RawInputButton2", e_RawInputButton2 },
        { "RawInputButton3", e_RawInputButton3 },
        { "RawInputButton4", e_RawInputButton4 },
        { "RawInputButton5", e_RawInputButton5 },
        { "RawInputButton6", e_RawInputButton6 },
        { "RawInputButton7", e_RawInputButton7 },
        { "RawInputButton8", e_RawInputButton8 },
        { "RawInputButton9", e_RawInputButton9 },
        { "RawInputButtonColon", e_RawInputButtonColon },
        { "RawInputButtonSemicolon", e_RawInputButtonSemicolon },
        { "RawInputButtonLess", e_RawInputButtonLess },
        { "RawInputButtonEquals", e_RawInputButtonEquals },
        { "RawInputButtonGreater", e_RawInputButtonGreater },
        { "RawInputButtonQuestion", e_RawInputButtonQuestion },
        { "RawInputButtonAt", e_RawInputButtonAt },

        { "RawInputButtonLeftBracket", e_RawInputButtonLeftBracket },
        { "RawInputButtonBackslash", e_RawInputButtonBackslash },
        { "RawInputButtonRightBracket", e_RawInputButtonRightBracket },
        { "RawInputButtonCarret", e_RawInputButtonCarret },
        { "RawInputButtonUnderscore", e_RawInputButtonUnderscore },
        { "RawInputButtonBackquote", e_RawInputButtonBackquote },
        { "RawInputButtonA", e_RawInputButtonA },
        { "RawInputButtonB", e_RawInputButtonB },
        { "RawInputButtonC", e_RawInputButtonC },
        { "RawInputButtonD", e_RawInputButtonD },
        { "RawInputButtonE", e_RawInputButtonE },
        { "RawInputButtonF", e_RawInputButtonF },
        { "RawInputButtonG", e_RawInputButtonG },
        { "RawInputButtonH", e_RawInputButtonH },
        { "RawInputButtonI", e_RawInputButtonI },
        { "RawInputButtonJ", e_RawInputButtonJ },
        { "RawInputButtonK", e_RawInputButtonK },
        { "RawInputButtonL", e_RawInputButtonL },
        { "RawInputButtonM", e_RawInputButtonM },
        { "RawInputButtonN", e_RawInputButtonN },
        { "RawInputButtonO", e_RawInputButtonO },
        { "RawInputButtonP", e_RawInputButtonP },
        { "RawInputButtonQ", e_RawInputButtonQ },
        { "RawInputButtonR", e_RawInputButtonR },
        { "RawInputButtonS", e_RawInputButtonS },
        { "RawInputButtonT", e_RawInputButtonT },
        { "RawInputButtonU", e_RawInputButtonU },
        { "RawInputButtonV", e_RawInputButtonV },
        { "RawInputButtonW", e_RawInputButtonW },
        { "RawInputButtonX", e_RawInputButtonX },
        { "RawInputButtonY", e_RawInputButtonY },
        { "RawInputButtonZ", e_RawInputButtonZ },

        { "RawInputButtonCapsLock", e_RawInputButtonCapsLock },

        { "RawInputButtonF1", e_RawInputButtonF1 },
        { "RawInputButtonF2", e_RawInputButtonF2 },
        { "RawInputButtonF3", e_RawInputButtonF3 },
        { "RawInputButtonF4", e_RawInputButtonF4 },
        { "RawInputButtonF5", e_RawInputButtonF5 },
        { "RawInputButtonF6", e_RawInputButtonF6 },
        { "RawInputButtonF7", e_RawInputButtonF7 },
        { "RawInputButtonF8", e_RawInputButtonF8 },
        { "RawInputButtonF9", e_RawInputButtonF9 },
        { "RawInputButtonF10", e_RawInputButtonF10 },
        { "RawInputButtonF11", e_RawInputButtonF11 },
        { "RawInputButtonF12", e_RawInputButtonF12 },

        { "RawInputButtonPrintScreen", e_RawInputButtonPrintScreen },
        { "RawInputButtonScrollLock", e_RawInputButtonScrollLock },
        { "RawInputButtonPause", e_RawInputButtonPause },
        { "RawInputButtonInsert", e_RawInputButtonInsert },
        { "RawInputButtonHome", e_RawInputButtonHome },
        { "RawInputButtonPageUp", e_RawInputButtonPageUp },
        { "RawInputButtonDelete", e_RawInputButtonDelete },
        { "RawInputButtonEnd", e_RawInputButtonEnd },
        { "RawInputButtonPageDown", e_RawInputButtonPageDown },
        { "RawInputButtonRight", e_RawInputButtonRight },
        { "RawInputButtonLeft", e_RawInputButtonLeft },
        { "RawInputButtonDown", e_RawInputButtonDown },
        { "RawInputButtonUp", e_RawInputButtonUp },

        { "RawInputButtonNumLock", e_RawInputButtonNumLock },
        { "RawInputButtonKeyPadDivide", e_RawInputButtonKeyPadDivide },
        { "RawInputButtonKeyPadMultiply", e_RawInputButtonKeyPadMultiply },
        { "RawInputButtonKeyPadMinus", e_RawInputButtonKeyPadMinus },
        { "RawInputButtonKeyPadPlus", e_RawInputButtonKeyPadPlus },
        { "RawInputButtonKeyPadEnter", e_RawInputButtonKeyPadEnter },
        { "RawInputButtonKeyPad1", e_RawInputButtonKeyPad1 },
        { "RawInputButtonKeyPad2", e_RawInputButtonKeyPad2 },
        { "RawInputButtonKeyPad3", e_RawInputButtonKeyPad3 },
        { "RawInputButtonKeyPad4", e_RawInputButtonKeyPad4 },
        { "RawInputButtonKeyPad5", e_RawInputButtonKeyPad5 },
        { "RawInputButtonKeyPad6", e_RawInputButtonKeyPad6 },
        { "RawInputButtonKeyPad7", e_RawInputButtonKeyPad7 },
        { "RawInputButtonKeyPad8", e_RawInputButtonKeyPad8 },
        { "RawInputButtonKeyPad9", e_RawInputButtonKeyPad9 },
        { "RawInputButtonKeyPad0", e_RawInputButtonKeyPad0 },
        { "RawInputButtonKeyPadPeriod", e_RawInputButtonKeyPadPeriod },

        { "RawInputButtonLeftControl", e_RawInputButtonLeftControl },
        { "RawInputButtonLeftShift", e_RawInputButtonLeftShift },
        { "RawInputButtonLeftAlt", e_RawInputButtonLeftAlt },
        { "RawInputButtonRightControl", e_RawInputButtonRightControl },
        { "RawInputButtonRightShift", e_RawInputButtonRightShift },
        { "RawInputButtonRightAlt", e_RawInputButtonRightAlt },

        { "RawInputButtonLeftMeta", e_RawInputButtonLeftMeta },
        { "RawInputButtonRightMeta", e_RawInputButtonRightMeta },

        { "RawInputButtonMouseLeft", e_RawInputButtonMouseLeft },
        { "RawInputButtonMouseRight", e_RawInputButtonMouseRight },
        { "RawInputButtonMouseMiddle", e_RawInputButtonMouseMiddle },

        { "RawInputFingerOne", e_RawInputFingerOne },
        { "RawInputFingerTwo", e_RawInputFingerTwo }
    };

	//-----------------------------------------------------------------------

    static const struct
    {
        const char* axisName;
        RawInputAxis axis;
    }
    c_RawInputAxisNames[e_RawInputAxisCount] =
    {
		{ "RawInputAxisMouseRelativeX", e_RawInputAxisMouseRelativeX },
		{ "RawInputAxisMouseRelativeY", e_RawInputAxisMouseRelativeY },
		{ "RawInputAxisMouseAbsoluteX", e_RawInputAxisMouseAbsoluteX },
		{ "RawInputAxisMouseAbsoluteY", e_RawInputAxisMouseAbsoluteY },
		{ "RawInputAxisMouseWheelHorizontal", e_RawInputAxisMouseWheelHorizontal },
		{ "RawInputAxisMouseWheelVertical", e_RawInputAxisMouseWheelVertical },
        { "RawInputAxisFingerOneRelativeX", e_RawInputAxisFingerOneRelativeX },
        { "RawInputAxisFingerOneRelativeY", e_RawInputAxisFingerOneRelativeY },
        { "RawInputAxisFingerOneAbsoluteX", e_RawInputAxisFingerOneAbsoluteX },
        { "RawInputAxisFingerOneAbsoluteY", e_RawInputAxisFingerOneAbsoluteY },
        { "RawInputAxisFingerTwoRelativeX", e_RawInputAxisFingerTwoRelativeX },
        { "RawInputAxisFingerTwoRelativeY", e_RawInputAxisFingerTwoRelativeY },
        { "RawInputAxisFingerTwoAbsoluteX", e_RawInputAxisFingerTwoAbsoluteX },
        { "RawInputAxisFingerTwoAbsoluteY", e_RawInputAxisFingerTwoAbsoluteY }
    };

	//-----------------------------------------------------------------------

	bool RawInputButtonNameToRawInputButton(const std::string& buttonName,
                                             RawInputButton& outVal)
    {
        bool retVal = false;

        for(size_t i = 0; (i < ArraySize( c_RawInputButtonNames )) && !retVal; i++)
        {
            if( buttonName.compare( c_RawInputButtonNames[i].buttonName ) == 0 )
            {
                outVal = c_RawInputButtonNames[i].button;
                retVal = true;
            }
        }

        return retVal;
    }

    //-----------------------------------------------------------------------

	bool RawInputAxisNameToRawInputAxis(const std::string& axisName,
                                         RawInputAxis& outVal)
    {
        bool retVal = false;

        for(size_t i = 0; (i < ArraySize( c_RawInputAxisNames )) && !retVal; i++)
        {
            if( axisName.compare( c_RawInputAxisNames[i].axisName ) == 0 )
            {
                outVal = c_RawInputAxisNames[i].axis;
                retVal = true;
            }
        }

        return retVal;
    }

	//-----------------------------------------------------------------------

	bool RawInputButtonToRawInputButtonName(RawInputButton button, std::string& outVal)
	{
        bool retVal = false;

        for(size_t i = 0; (i < ArraySize( c_RawInputButtonNames )) && !retVal; i++)
        {
            if( button == c_RawInputButtonNames[i].button )
            {
                outVal = c_RawInputButtonNames[i].buttonName;
                retVal = true;
            }
        }

        return retVal;
	}

	//-----------------------------------------------------------------------
}
