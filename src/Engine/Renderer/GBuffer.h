#ifndef GBUFFER_H_INCLUDED
#define GBUFFER_H_INCLUDED

#include <OpenGLIncludes.h>
#include <GraphicsDevice/FBO.h>
#include <GraphicsDevice/Texture.h>

class GraphicsDevice;
class SystemInterface;
class DepthTexture;

class GBuffer
{
public:
    GBuffer(GraphicsDevice& graphicsDevice, SystemInterface& systemInterface);

    bool Init(DepthTexture& depthBuffer);

    void BindForGeomPass();
    void BindForStencilPass();
    void BindForLightPass();

private:
    SystemInterface& m_SystemInterface;
    FBO m_FBO;
    GraphicsDevice& m_GraphicsDevice;
};

#endif // GBUFFER_H_INCLUDED
