#include <cstdlib>

#include "DrawableText.h"
#include "FontLoader.h"
#include "MsgLogger.h"
#include "StringUtils.h"
#include "FontLoader.h"

#include <iostream>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <OpenGLErrorChecking.h>
#include <GraphicsDevice/GraphicsDevice.h>
#include <GraphicsDevice/Texture.h>
#include <GraphicsDevice/VBO.h>
#include <imgutl/ImageUtils.h>

//-----------------------------------------------------------------------

static const unsigned char c_SpaceChar = 0x20;

//-----------------------------------------------------------------------

DrawableText::DrawableText(GraphicsDevice& graphicsDevice,
                           SystemInterface& systemInterface,
                           FileManager& fileManager,
                           std::string fontFileName,
                           size_t font_height)
: m_TextShaderProg( fileManager ),
  m_GraphicsDevice( graphicsDevice ),
  m_SystemInterface( systemInterface ),
  m_FileManager( fileManager )
{
    m_TextShaderProg.LoadCompileAndLink();

    const SystemInterface::DrawableSurfaceSize& screenSize = m_SystemInterface.GetDrawableSurfaceSize();

    m_ProjectionMatrix = glm::ortho( -(screenSize.widthInPixels / 2.0f),
                                      screenSize.widthInPixels / 2.0f,
                                     -(screenSize.heightInPixels / 2.0f),
                                      screenSize.heightInPixels / 2.0f,
                                      0.1f,
                                      100.0f );

    m_CharSpacingTranforms = new glm::vec3[128];
    m_CharElevationTransforms = new glm::vec3[128];
    m_CharacterSpaces = new glm::vec2[128];
    //m_CharDrawingQuads = new ModelOBJ::Vertex*[128];
    m_CharsSizes = new glm::vec3[128];

    m_CharDrawingQuads = generateCharDrawingQuad(1.0f,1.0f,1.0f,1.0f);

    //m_VBOs = new GLuint[128];

    memset(m_CharTextures, 0x00, c_CharTextureCount);

    for(size_t i = 1; i < c_CharTextureCount; i++)
    {
        m_CharTextures[i] = new Texture( m_GraphicsDevice, Texture::e_TypeIdxDiffuseMap, Texture::e_Class2DTexture, false );
    }

    prepareRenderingContext();

    /*------------------------- Load New Font -------------------------*/
    FontLoader* ft = FontLoader::getInstance();
    FT_Face face = ft->loadNewFaceFromFile(fontFileName, m_FileManager);
    FontLoader::getInstance()->setupFacePixelSize(face, static_cast<long>(font_height * 64) , 0, 72, 72);
    FT_GlyphSlot glyphSlot;
    /*-----------------------------------------------------------------*/

    for(unsigned char charCode = 0; charCode < c_CharTextureCount; charCode++)//Generate Textures for all ASCII alphanumeric characters
    {
        FT_UInt gIndex=0;
        glyphSlot = ft->getCharBitmap(face, (charCode + 32), &gIndex);

        if(glyphSlot == NULL)
        {
            exit(1);
        }

        if ((charCode + 32) == c_SpaceChar)
        {
            m_CharsSizes[charCode].x = static_cast<float>(glyphSlot->metrics.width);
            m_CharsSizes[charCode].y = static_cast<float>(glyphSlot->metrics.height);

            m_CharSpacingTranforms[charCode].x=glyphSlot->bitmap_left+(m_CharsSizes[charCode].x/2.0f);
            m_CharSpacingTranforms[charCode].y=0.0f;
            m_CharSpacingTranforms[charCode].z=0.0f;

            m_CharElevationTransforms[charCode].x=0.0f;
            m_CharElevationTransforms[charCode].y=(glyphSlot->bitmap_top-(m_CharsSizes[charCode].y/2.0f));
            m_CharElevationTransforms[charCode].z=0.0f;

            m_CharacterSpaces[charCode].x = static_cast<float>(((glyphSlot->advance.x >> 6)));
            m_CharacterSpaces[charCode].y = static_cast<float>((glyphSlot->advance.y >> 6));
            generateCharacterKerning(face, ( charCode + 32 ) );
        }
        else
        {
            int width = (glyphSlot->bitmap.width );
            int height = (glyphSlot->bitmap.rows);

            GLubyte* expanded_data = new GLubyte[ 4 * width * height];

            twoChannelTextureToRGBA(glyphSlot->bitmap.buffer, expanded_data,width,height);

            prepareCharsTexture(charCode, width, height, expanded_data);

            delete [] expanded_data;

            m_CharsSizes[charCode].x = static_cast<float>(width);
            m_CharsSizes[charCode].y = static_cast<float>(height);

            m_CharSpacingTranforms[charCode].x=glyphSlot->bitmap_left+(m_CharsSizes[charCode].x/2.0f);
            m_CharSpacingTranforms[charCode].y=0.0f;
            m_CharSpacingTranforms[charCode].z=0.0f;

            m_CharElevationTransforms[charCode].x=0.0f;
            m_CharElevationTransforms[charCode].y=(glyphSlot->bitmap_top-(m_CharsSizes[charCode].y/2.0f));
            m_CharElevationTransforms[charCode].z=0.0f;

            //FT_Load_Glyph( face, gIndex, FT_LOAD_RENDER | FT_LOAD_NO_HINTING);
            //glyphSlot = face->glyph;

            m_CharacterSpaces[charCode].x = static_cast<float>(((glyphSlot->advance.x >> 6)));
            m_CharacterSpaces[charCode].y = static_cast<float>((glyphSlot->advance.y >> 6));
            generateCharacterKerning(face, ( charCode + 32 ));

            //FT_Done_Glyph(glyphSlot);
        }
    }

    ft->deleteFace(face);

    m_SystemInterface.RegisterDrawableSurfaceSizeChangedCallback( _DrawableSurfaceSizeChaged, this );
}

//-----------------------------------------------------------------------

DrawableText::~DrawableText()
{
    m_SystemInterface.DeregisterDrawableSurfaceSizeChangedCallback( _DrawableSurfaceSizeChaged, this );

    for(size_t i = 0; i < c_CharTextureCount; i++)
    {
        if( m_CharTextures[i] != nullptr )
        {
            delete m_CharTextures[i];
        }
    }

    delete[] m_CharSpacingTranforms;
    delete[] m_CharElevationTransforms;
    delete[] m_CharDrawingQuads;
    delete[] m_CharacterSpaces;
    delete[] m_CharsSizes;
}

//-----------------------------------------------------------------------

void DrawableText::Render(std::string text, float x, float y)
{
    m_VAO.Bind();

    m_TextShaderProg.Use();

    m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionDepthTesting );
    m_GraphicsDevice.OptionDisable( GraphicsDevice::e_OptionDepthTesting );
    m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionBlending );
    m_GraphicsDevice.OptionEnable( GraphicsDevice::e_OptionBlending );
    m_GraphicsDevice.SaveCurrentBlendEquation();
    m_GraphicsDevice.SetBlendEquation( GraphicsDevice::e_BlendEquationAdd );
    m_GraphicsDevice.SaveCurrentBlendFunctionState();
    m_GraphicsDevice.SetBlendFunctionState( GraphicsDevice::e_BlendFunctionSrcAlpha, GraphicsDevice::e_BlendFunctionOneMinusSrcAlpha );

    int i = 0;

    while(text[i] != '\0')
    {
        if(i != 0)
        {
            x = x + m_CharKerning[text[i]][text[i-1]];
        }

        if( text[i] != c_SpaceChar )
        {
            glm::mat4 modelViewMatrix = glm::mat4(1);//glm::translate(glm::mat4(1), m_CharSpacingTranforms[(int)text[i]]);
            //modelViewMatrix = glm::translate(modelViewMatrix , m_CharElevationTransforms[(int)text[i]]);
            modelViewMatrix = glm::translate(modelViewMatrix ,
                              glm::vec3(x,y,0.0f)+
                              m_CharSpacingTranforms[(int)text[i] - 32]+
                              m_CharElevationTransforms[(int)text[i] - 32]);

            modelViewMatrix = glm::scale(modelViewMatrix,glm::vec3(m_CharsSizes[(int)text[i] - 32].x, m_CharsSizes[(int)text[i] - 32].y, 1.0));//Scales unit quad to the size of a char being drawn

            m_TextShaderProg.SetUniformModelViewProjectionMatrix( m_ProjectionMatrix * modelViewMatrix );

            assert((int)text[i] >= 32);
            assert((int)text[i] < 127);

            m_CharTextures[static_cast<size_t>( text[i] - 32 )]->Activate();

            glDrawArrays(GL_TRIANGLE_STRIP,0,4);
            CheckForGLErrors();
        }

        x = x + (float)m_CharacterSpaces[(int)text[i] - 32].x;
        //tempY += (float)m_CharacterSpaces[(int)text[i]].y;

//            if(prevX > x)
//            {
//                x = prevX;
//            }
//            else
//            {
//                prevX = x;
//            }

        i++;
    }

    m_GraphicsDevice.RestorePreviousBlendFunctionState();
    m_GraphicsDevice.RestorePreviousBlendEquation();
    m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionBlending );
    m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionDepthTesting );
}

//-----------------------------------------------------------------------

void DrawableText::prepareRenderingContext()
{
    VBO* vbo = new VBO( sizeof( ModelBinObj::Vertex ) );

    vbo->AddVertexAttribute(VBO::e_VertexAttrIdxPosition,
                            VBO::e_VertexAttrComponentTypeFloat32,
                            VBO::e_VertexAttrTypeVec3,
                            offsetof ( ModelBinObj::Vertex, position ));

    vbo->AddVertexAttribute(VBO::e_VertexAttrIdxTextureCoords,
                            VBO::e_VertexAttrComponentTypeFloat32,
                            VBO::e_VertexAttrTypeVec2,
                            offsetof ( ModelBinObj::Vertex, texCoord ));

    vbo->BindData( 4 * sizeof ( ModelBinObj::Vertex ), m_CharDrawingQuads );

    m_VAO.AddVBO( vbo );
}

//-----------------------------------------------------------------------


//////////////////////////////////////////////////////////////////////////
// Here we create RGBA texture data from 2 channel bitmap image returned by FreeType (first channel
// is brightness(luminosity) of visible pixels second channel is alpha component)
//////////////////////////////////////////////////////////////////////////
inline void DrawableText::twoChannelTextureToRGBA(unsigned char* twoChannelData, unsigned char* expandedData, size_t textureWidth, size_t textureHeight)
{
    for(size_t j=0; j < textureHeight;j++)
    {
        for(size_t i=0; i < textureWidth; i++)
        {
            expandedData[4*(i+j*textureWidth)] =
            expandedData[4*(i+j*textureWidth)+1] =
            expandedData[4*(i+j*textureWidth)+2] = 255;

            expandedData[4*(i+j*textureWidth)+3] =
                    (i>=textureWidth || j>=textureHeight) ?
                    0 : twoChannelData[i + textureWidth*j];
        }
    }
}

//-----------------------------------------------------------------------

inline void DrawableText::prepareCharsTexture(unsigned char charCode, size_t width, size_t height, unsigned char* pixels)
{
    ImageUtils::ImageData_t image;
    image.bytesPerPixel = 4;
    image.depth = 32;
    image.format = ImageUtils::RGBA;
    image.w = width;
    image.h = height;
    image.data = reinterpret_cast<uint8_t*>( pixels );

    Texture* texture = m_CharTextures[charCode];

    texture->SetData( Texture::e_DataClass2DTexture, image, false );
    texture->SetFilteringMode( Texture::e_FilteringModeNoFiltering );
    texture->SetStateParameter( Texture::e_StateParamSCoordWrapType, Texture::e_StateParamArgSCoordWrapClampToEdge );
    texture->SetStateParameter( Texture::e_StateParamTCoordWrapType, Texture::e_StateParamArgTCoordWrapClampToEdge );
}

//-----------------------------------------------------------------------

inline ModelBinObj::Vertex* DrawableText::generateCharDrawingQuad(float width, float height,float textureWidth, float textureHeight)
{
   ModelBinObj::Vertex* quad = new ModelBinObj::Vertex[4];

   //
   // All the triangles in this quad are in counterclockwise order
   //

   quad[0].position[0]=-width / 2.0f;
   quad[0].position[1]=-height / 2.0f;
   quad[0].position[2]=0.1f;
   quad[0].texCoord[0]=0.0f;
   quad[0].texCoord[1]=textureHeight;

   quad[1].position[0]=width / 2.0f;
   quad[1].position[1]=-height / 2.0f;
   quad[1].position[2]=0.1f;
   quad[1].texCoord[0]=textureWidth;
   quad[1].texCoord[1]=textureHeight;

   quad[2].position[0]=-width / 2.0f;
   quad[2].position[1]=height / 2.0f;
   quad[2].position[2]=0.1f;
   quad[2].texCoord[0]=0.0f;
   quad[2].texCoord[1]=0.0f;

   quad[3].position[0]=width / 2.0f;
   quad[3].position[1]=height / 2.0f;
   quad[3].position[2]=0.1f;
   quad[3].texCoord[0]=textureWidth;
   quad[3].texCoord[1]=0.0f;

   return quad;
}

//-----------------------------------------------------------------------

inline void DrawableText::generateCharacterKerning(FT_Face face, char charCode)
{
    FT_Vector kerning;
    FT_UInt mainGlyphIndex, prevGlyphIndex;
    mainGlyphIndex = FT_Get_Char_Index( face, charCode );

    for(unsigned char i = 0 ; i < c_CharTextureCount ; i++)
    {
        prevGlyphIndex = FT_Get_Char_Index( face, i );
        FT_Get_Kerning(face, prevGlyphIndex, mainGlyphIndex, FT_KERNING_DEFAULT, &kerning);
        if( kerning.x != 0.0 )
        {
            m_CharKerning[charCode - 32][i] = kerning.x / 64.0f;
        }
    }
}

//-----------------------------------------------------------------------

void DrawableText::_DrawableSurfaceSizeChaged(const SystemInterface::DrawableSurfaceSize& newSize, void* userData)
{
    static_cast<DrawableText*>( userData )->m_ProjectionMatrix = glm::ortho( -(newSize.widthInPixels / 2.0f),
                                                                              newSize.widthInPixels / 2.0f,
                                                                              -(newSize.heightInPixels / 2.0f),
                                                                              newSize.heightInPixels / 2.0f,
                                                                              0.1f,
                                                                              100.0f );
}

//-----------------------------------------------------------------------
