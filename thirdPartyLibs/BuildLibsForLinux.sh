#!/usr/bin/env bash

INITIAL_DIR=`pwd`

pushd `dirname $0` > /dev/null # Save script call dir and "cd" into the script dir
SCRIPT_DIR=`pwd`
popd > /dev/null # "cd" back into the script call dir


######################################
# Parse Arguments
######################################
INSTALL_PREREQUISITES=YES

PrintHelp ()
{
    echo "-sp, --skip-prerequisites  Do not try to install prerequisites using \"apt-get install\""
    echo "-h, --help            Show this help text"
    echo ""
    echo "Example usage: $0 [--skip-prerequisites]"
}

for i in "$@"
do
case $i in
    -sp*|--skip-prerequisites)
        INSTALL_PREREQUISITES=NO
        shift # Past argument with no value
    ;;
    -h*|--help)
        PrintHelp
        exit 0
    ;;
    *)
        # Unknown argument
        echo "Unrecognised argument \"${i}\""
        echo
        PrintHelp
        exit 1
    ;;
esac
done
######################################

###########################################################################
# Install required dependencies
###########################################################################
OS_TYPE=`uname -s`

if [[ $OS_TYPE != "Darwin" ]] && [[ $INSTALL_PREREQUISITES == YES ]]; # Check to see if not running on OS X and that script caller has not requested to skip prerequisites installation step
then
    sudo apt-get update
    
    sudo apt-get install -y cmake
    sudo apt-get install -y libtool
    sudo apt-get install -y libtool-bin
    sudo apt-get install -y pkg-config
    sudo apt-get install -y build-essential
    sudo apt-get install -y autoconf
    sudo apt-get install -y automake
    sudo apt-get install -y autotools-dev
    sudo apt-get install -y libreadline-dev
    sudo apt-get install -y libncurses5-dev
    
    # SDL Dependencies
    sudo apt-get install -y build-essential mercurial make cmake autoconf automake \
    libtool libasound2-dev libpulse-dev libaudio-dev libx11-dev libxext-dev \
    libxrandr-dev libxcursor-dev libxi-dev libxinerama-dev libxxf86vm-dev \
    libxss-dev libgl1-mesa-dev libesd0-dev libdbus-1-dev libudev-dev \
    libgles1-mesa-dev libgles2-mesa-dev libegl1-mesa-dev
    
    # Editor's Dependencies
    sudo apt-get install -y qt5-default qttools5-dev qttools5-dev-tools
fi
###########################################################################

SOURCES_DIR="${SCRIPT_DIR}/sources"
RESULTS_DIR="${SCRIPT_DIR}/linux"

ICU_SOURCES_DIR="${SOURCES_DIR}/icu_58_2/"
BOOST_SOURCES_DIR="${SOURCES_DIR}/boost_1_57_0"
LUA_SOURCES_DIR="${SOURCES_DIR}/lua-5.1.5"
LUABIND_SOURCES_DIR="${SOURCES_DIR}/luabind-0.9.1_rpavlik"
BULLET_SOURCES_DIR="${SOURCES_DIR}/bullet-2.81-rev2613"
FREETYPE_SOURCES_DIR="${SOURCES_DIR}/freetype-2.5.5"
LIBROCKET_SOURCES_DIR="${SOURCES_DIR}/libRocket/Build"
SDL_SOURCES_DIR="${SOURCES_DIR}/SDL2-2.0.5"
GLEW_SOURCES_DIR="${SOURCES_DIR}/glew-1.12.0"
OGG_SOURCES_DIR="${SOURCES_DIR}/libogg-1.3.2"
TREMOR_SOURCES_DIR="${SOURCES_DIR}/tremor"
OPENAL_SOFT_SOURCES_DIR="${SOURCES_DIR}/openal-soft-1.17.2"


ICU_INSTALL_DIR=${RESULTS_DIR}/libicu
BOOST_INSTALL_DIR=${RESULTS_DIR}/libboost
LUA_INSTALL_DIR=${RESULTS_DIR}/liblua
LUABIND_INSTALL_DIR=${RESULTS_DIR}/libluabind
BULLET_INSTALL_DIR=${RESULTS_DIR}/libBullet
FREETYPE_INSTALL_DIR=${RESULTS_DIR}/libfreetype
LIBROCKET_INSTALL_DIR=${RESULTS_DIR}/libRocket
SDL_INSTALL_DIR=${RESULTS_DIR}/libSDL2
GLEW_INSTALL_DIR=${RESULTS_DIR}/libGLEW
OGG_INSTALL_DIR=${RESULTS_DIR}/libOgg
TREMOR_INSTALL_DIR=${RESULTS_DIR}/libTremor
OPENAL_INSTALL_DIR=${RESULTS_DIR}/libOpenAL


CONTINUE=1

CheckIfLibExists ()
{
    local _RETURN_CODE=0

    if [ -d "$RESULTS_DIR/$1" ];
    then
        _RETURN_CODE=1
    fi
    
    return $_RETURN_CODE
}

if [ $OS_TYPE != "Darwin" ];
then
#     if [ "$CONTINUE" -eq 1 ];
#     then
#         CONTINUE=0
# 
#         #------- Building ICU 55.1 -----
#         
#         rm -rf ${ICU_INSTALL_DIR}
#         
#         mkdir ${ICU_SOURCES_DIR}/build
#         cd ${ICU_SOURCES_DIR}/build
#         
#         CXXFLAGS="--std=c++11 -DU_USING_ICU_NAMESPACE=0 -DU_CHARSET_IS_UTF8=1" ../source/runConfigureICU Linux --enable-static --disable-shared --prefix=${ICU_INSTALL_DIR}
#         make check
#         make install
#         
#         cd ../
#         rm -rf build
#         
#         cd $SCRIPT_DIR
# 
#         #-------------------------------
#         
#         CheckIfLibExists "libicu"
#         CONTINUE=$?
#     fi
#     
#     if [ "$CONTINUE" -eq 1 ];
#     then
#         CONTINUE=0
# 
#         #----- Building Boost 1.57.0 ---
# 
#         rm -rf ${BOOST_INSTALL_DIR}
#         
#         cd ${BOOST_SOURCES_DIR}
#         
#         ./bootstrap.sh --with-icu=${ICU_INSTALL_DIR} --prefix=${BOOST_INSTALL_DIR} link=static variant=release linkflags=-ldl
#         ./b2 -sNO_BZIP2=1 -sICU_PATH=${ICU_INSTALL_DIR} link=static variant=release linkflags=-ldl install --without-python --without-mpi
# 
#         rm -rf bin.v2
#         
#         cd $SCRIPT_DIR
# 
#         #-------------------------------
#         
#         CheckIfLibExists "libboost"
#         CONTINUE=$?
#     fi
# 
#     if [ "$CONTINUE" -eq 1 ];
#     then
#         CONTINUE=0
# 
#         #------ Building Lua 5.1.5 -----
# 
#         rm -rf ${LUA_INSTALL_DIR}
#         
#         cd ${LUA_SOURCES_DIR}
#         
#         make linux
#         make linux
#         make install INSTALL_TOP=${LUA_INSTALL_DIR}
#         make clean
#         
#         cd $SCRIPT_DIR
# 
#         #-------------------------------
#         
#         CheckIfLibExists "liblua"
#         CONTINUE=$?
#     fi
# 
#     if [ "$CONTINUE" -eq 1 ];
#     then
#         CONTINUE=0
# 
#         #---- Building LuaBind 0.9.1 ---
# 
#         rm -rf ${LUABIND_INSTALL_DIR}
#         
#         cd ${LUABIND_SOURCES_DIR}
#         
#         ${BOOST_SOURCES_DIR}/bjam -sBOOST_BUILD_PATH=${BOOST_SOURCES_DIR} -sBOOST_ROOT=${BOOST_INSTALL_DIR}/include -sLUA_PATH=${LUA_INSTALL_DIR} --toolset=gcc --prefix=${LUABIND_INSTALL_DIR} install link=static variant=release
# 
#         rm -rf bin
#         
#         cd $SCRIPT_DIR
# 
#         #-------------------------------
#         
#         CheckIfLibExists "libluabind"
#         CONTINUE=$?
#     fi
#     
#     if [ "$CONTINUE" -eq 1 ];
#     then
#         CONTINUE=0
# 
#         #----- Building Bullet 2.81 ----
# 
#         rm -rf ${BULLET_INSTALL_DIR}
#         
#         mkdir ${BULLET_SOURCES_DIR}/buildObjects
#         cd ${BULLET_SOURCES_DIR}/buildObjects
#         
#         cmake ../ -G "Unix Makefiles" -DBUILD_SHARED_LIBS=OFF -DCMAKE_BUILD_TYPE=Release -DBUILD_EXTRAS=ON -DINSTALL_EXTRA_LIBS=ON -DBUILD_DEMOS=ON -DCMAKE_INSTALL_PREFIX=${BULLET_INSTALL_DIR} -DCMAKE_INSTALL_RPATH=${BULLET_INSTALL_DIR}
#         make
#         make install
#         
#         cd ../
#         rm -rf buildObjects
# 
#         rm -rf ${BULLET_INSTALL_DIR}/include/OpenGL
#                 
#         cd $SCRIPT_DIR
# 
#         #-------------------------------
#         
#         CheckIfLibExists "libBullet"
#         CONTINUE=$?
#     fi
#         
#     if [ "$CONTINUE" -eq 1 ];
#     then
#         CONTINUE=0
# 
#         #--- Building FreeType 2.5.5 ---
# 
#         rm -rf ${FREETYPE_INSTALL_DIR}
#         
#         cd ${FREETYPE_SOURCES_DIR}
#         
#         ./configure --prefix=${FREETYPE_INSTALL_DIR} --with-png=no --with-harfbuzz=no
#         make
#         make check
#         make install
#         make distclean
#                 
#         cd $SCRIPT_DIR
# 
#         #-------------------------------
#         
#         CheckIfLibExists "libfreetype"
#         CONTINUE=$?
#     fi
#     
#     if [ "$CONTINUE" -eq 1 ];
#     then
#         CONTINUE=0
# 
#         #----- Building libRocket ------
# 
#         rm -rf ${LIBROCKET_INSTALL_DIR}
#         
#         cd ${LIBROCKET_SOURCES_DIR}
#         
#         mkdir ${LIBROCKET_SOURCES_DIR}/buildData
#         cd ${LIBROCKET_SOURCES_DIR}/buildData
#         
#         cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_SAMPLES=OFF -DBUILD_SHARED_LIBS=ON -DBUILD_LUA_BINDINGS=ON -DCMAKE_INSTALL_PREFIX=${LIBROCKET_INSTALL_DIR} -DLUA_LIBRARIES=${LUA_INSTALL_DIR}/lib/liblua.a -DLUA_INCLUDE_DIR=${LUA_INSTALL_DIR}/include -DFREETYPE_LINK_DIRS=${FREETYPE_INSTALL_DIR}/lib -DFREETYPE_INCLUDE_DIRS=${FREETYPE_INSTALL_DIR}/include/freetype2 -DFREETYPE_CUSTOM_LOCATION=ON ../
#         
#         make
#         make install
#         cd ../
#         rm -rf buildData
#                 
#         cd $SCRIPT_DIR
# 
#         #-------------------------------
#         
#         CheckIfLibExists "libRocket"
#         CONTINUE=$?
#     fi
#     
#     if [ "$CONTINUE" -eq 1 ];
#     then
#         CONTINUE=0
# 
#         #-------- Building SDL ---------
# 
#         rm -rf ${SDL_INSTALL_DIR}
#         
#         cd ${SDL_SOURCES_DIR}
#         
#         mkdir ${SDL_SOURCES_DIR}/linuxBuild
#         cd ${SDL_SOURCES_DIR}/linuxBuild
#         
#         cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=${SDL_INSTALL_DIR} -DSDL_SHARED=ON -DSDL_STATIC=OFF ../
#         
#         make
#         make install
#         
#         cd ../
#         rm -rf linuxBuild
# 
#         cd $SCRIPT_DIR
# 
#         #-------------------------------
#         
#         CheckIfLibExists "libSDL2"
#         CONTINUE=$?
#     fi
# 
#     if [ "$CONTINUE" -eq 1 ];
#     then
#         CONTINUE=0
# 
#         #-------- Building GLEW --------
# 
#         rm -rf ${GLEW_INSTALL_DIR}
#         
#         cd ${GLEW_SOURCES_DIR}
#         
#         mkdir ${GLEW_SOURCES_DIR}/linuxBuild
#         cd ${GLEW_SOURCES_DIR}/linuxBuild
#         
#         cmake -DCMAKE_INSTALL_PREFIX=${GLEW_INSTALL_DIR} ../
#         make
#         make install
# 
#         cd ../
#         rm -rf linuxBuild
# 
#         cd $SCRIPT_DIR
# 
#         #-------------------------------
#         
#         CheckIfLibExists "libGLEW"
#         CONTINUE=$?
#     fi
# 
#     if [ "$CONTINUE" -eq 1 ];
#     then
#         CONTINUE=0
# 
#         #------- Building libOgg -------
# 
#         rm -rf ${OGG_INSTALL_DIR}
#         
#         cd ${OGG_SOURCES_DIR}
#         
#         ./autogen.sh
#         ./configure --prefix=${OGG_INSTALL_DIR} --enable-static=yes --enable-shared=no
#         make
#         make install
#         make distclean
# 
#         cd $SCRIPT_DIR
# 
#         #-------------------------------
#         
#         CheckIfLibExists "libOgg"
#         CONTINUE=$?
#     fi
    
    if [ "$CONTINUE" -eq 1 ];
    then
        CONTINUE=0

        #- Building Tremor(Vorbis decoder) -

        rm -rf ${TREMOR_INSTALL_DIR}
        
        cd ${TREMOR_SOURCES_DIR}
        
        ./autogen.sh
        ./configure --prefix=${TREMOR_INSTALL_DIR} --enable-static=yes --enable-shared=no --with-ogg-libraries=${OGG_INSTALL_DIR}/lib/ --with-ogg-includes=${OGG_INSTALL_DIR}/libOgg/include/
        make
        make install
        make distclean

        cd $SCRIPT_DIR

        #-------------------------------
        
        CheckIfLibExists "libTremor"
        CONTINUE=$?
    fi

    if [ "$CONTINUE" -eq 1 ];
    then
        CONTINUE=0

        #---- Building OpenAL Soft -----

        rm -rf ${OPENAL_INSTALL_DIR}
        
        mkdir ${OPENAL_SOFT_SOURCES_DIR}/build
        cd ${OPENAL_SOFT_SOURCES_DIR}/build

        cmake -DCMAKE_INSTALL_PREFIX=${OPENAL_INSTALL_DIR} ../
        make
        make install
        
        cd ../
        rm -rf build

        cd $SCRIPT_DIR

        #-------------------------------
        
        CheckIfLibExists "libOpenAL"
        CONTINUE=$?
    fi
fi

cd ${INITIAL_DIR}

# Set script return code
if [ "$CONTINUE" -eq 1 ];
then
    exit 0 # Success
else
    echo "Failed to build at least one of the libraries"
    exit 1 # Failed to compile one or more libraries
fi
