#include "ManipulatorTool.h"
#include <glm/gtc/matrix_transform.hpp>
#include <Scene/Transform.h>
#include <Camera.h>
#include <BoundingFrustum.h>

//-----------------------------------------------------------------------

ManipulatorTool::ManipulatorTool(MeshManager& meshManager,
                                 MaterialManager& materialManager,
                                 TextureManager& textureManager)
: m_CurrentMode( e_ModeNone ),
  m_TransformBeingManipulated( nullptr ),
  m_CurrentlyActiveManipulatorTransform( nullptr ),
  m_Renderables(),
  m_PickingRenderables(),
  m_PickedManipulatorComponentID( 0 ),
  m_PreviouslyProcessedAbsouluteMouseCoordinates( 0.0f, 0.0f ),
  m_MoveManipulator( meshManager, materialManager, textureManager )
{
    static const uint32_t MoveManipulator = 1;
    static const uint32_t MoveManipulatorXAxis = 2;
    static const uint32_t MoveManipulatorYAxis = 3;
    static const uint32_t MoveManipulatorZAxis = 4;

    static_assert( ( MoveManipulator < MoveManipulatorXAxis ) &&
                   ( MoveManipulatorXAxis < MoveManipulatorYAxis ) &&
                   ( MoveManipulatorYAxis < MoveManipulatorZAxis ), "Move Manipulator IDs must be in descending order" );

    static_assert( ( ( MoveManipulatorXAxis - MoveManipulator ) == 1 ) &&
                   ( ( MoveManipulatorYAxis - MoveManipulatorXAxis ) == 1 ) &&
                   ( ( MoveManipulatorZAxis - MoveManipulatorYAxis ) == 1 ), "Move Manipulator IDs must be sequential" );
}

//-----------------------------------------------------------------------

bool ManipulatorTool::PickManipulator(RenderManager& renderManager,
                                      const glm::vec2& absoluteMouseCoordinates)
{
    m_PickedManipulatorComponentID = 0;

    if( m_TransformBeingManipulated )
    {
        m_PickedManipulatorComponentID = renderManager.DoGameObjectPicking( m_PickingRenderables,
                                                                            absoluteMouseCoordinates.x,
                                                                            absoluteMouseCoordinates.y );

        m_PreviouslyProcessedAbsouluteMouseCoordinates = absoluteMouseCoordinates;
    }

    return ( m_PickedManipulatorComponentID != 0 );
}

//-----------------------------------------------------------------------

void ManipulatorTool::ClearPickedManipulator()
{
    m_PickedManipulatorComponentID = 0;
}

//-----------------------------------------------------------------------

void ManipulatorTool::SetTransformToManipulate(Transform* t)
{
    m_TransformBeingManipulated = t;
    _SyncManipulatorAndManipulatedTransformPositions();
}

//-----------------------------------------------------------------------

Transform* ManipulatorTool::GetTransformBeingManipulated()
{
    return m_TransformBeingManipulated;
}

//-----------------------------------------------------------------------

void ManipulatorTool::SetMode(Mode mode)
{
    if( mode != m_CurrentMode )
    {
        m_CurrentMode = mode;
        m_CurrentlyActiveManipulatorTransform = nullptr;

        if( m_CurrentMode == e_ModeTranslation )
        {
            m_CurrentlyActiveManipulatorTransform = m_MoveManipulator.GetComponent<Transform>();
        }
        else if( m_CurrentMode == e_ModeRotation )
        {

        }
        else if( m_CurrentMode == e_ModeScale )
        {

        }

        _SyncManipulatorAndManipulatedTransformPositions();
    }
}

//-----------------------------------------------------------------------

ManipulatorTool::Mode ManipulatorTool::GetMode() const
{
    return m_CurrentMode;
}

//-----------------------------------------------------------------------

bool ManipulatorTool::Process(const glm::vec2& newAbsoluteMouseCoordinates,
                              const EngineTypes::VPTransformations& vpTrans,
                              const SystemInterface::DrawableSurfaceSize& drawableSurfaceSize)
{
    bool retVal = false;

    if( ( m_PickedManipulatorComponentID != 0 ) &&
        ( m_TransformBeingManipulated != nullptr ) &&
        ( newAbsoluteMouseCoordinates != m_PreviouslyProcessedAbsouluteMouseCoordinates ) )
    {
        glm::vec2 mouseCoordsDelta = ( m_PreviouslyProcessedAbsouluteMouseCoordinates - newAbsoluteMouseCoordinates );
        mouseCoordsDelta.y = ( mouseCoordsDelta.y * -1.0f );
        m_PreviouslyProcessedAbsouluteMouseCoordinates = newAbsoluteMouseCoordinates;

        if( ( ( m_PickedManipulatorComponentID == EditorObjectID::MoveManipulatorXAxis ) ||
              ( m_PickedManipulatorComponentID == EditorObjectID::MoveManipulatorYAxis ) ||
              ( m_PickedManipulatorComponentID == EditorObjectID::MoveManipulatorZAxis ) ) &&
              ( mouseCoordsDelta != glm::vec2( 0.0f ) ) )
        {

            // Get the unit version of the selected axis
            const size_t axisIndex = ( ( m_PickedManipulatorComponentID - EditorObjectID::MoveManipulator ) - 1 );
            glm::vec3 unitAxis( 0.0f );
            unitAxis[axisIndex] = 1.0f;

            // We need to project using the translation component of the manipulated
            // transform in order to obtain a projected unit axis originating
            // from the transform's position
            const glm::vec3 objectPos = m_TransformBeingManipulated->GetPosition();
            const glm::mat4 translate = glm::translate(MathUtils::c_IdentityMatrix, objectPos);

            const glm::vec4 viewPort = glm::vec4( 0.0f, 0.0f, drawableSurfaceSize.widthInPixels, drawableSurfaceSize.heightInPixels );

            // Project the origin onto the screen at the transform's position
            glm::vec3 startPos = glm::project( glm::vec3( 0.0f ),
                                               vpTrans.viewMatrix * translate,
                                               vpTrans.projectionMatrix,
                                               viewPort );

            // Project the unit axis onto the screen at the transform's position
            glm::vec3 endPos = glm::project( unitAxis,
                                             vpTrans.viewMatrix * translate,
                                             vpTrans.projectionMatrix,
                                             viewPort );

            // Calculate the normalized direction vector of the unit axis in screen space
            glm::vec3 screenDirection = glm::normalize( endPos - startPos );

            // Calculate the projected mouse delta along the screen direction vector
            endPos = startPos + ( screenDirection * glm::dot( glm::vec3( mouseCoordsDelta, 0.0f ), screenDirection ) );


            // Unproject the screen points back into world space using the
            // translation transform to get the world space start and end
            // positions with regard to the mouse delta along the mouse direction vector
            startPos = glm::vec3( 0.0f );
            endPos = glm::unProject( endPos,
                                     vpTrans.viewMatrix * translate,
                                     vpTrans.projectionMatrix,
                                     viewPort );

            // Calculate the difference vector between the world space start and end points
            const glm::vec3 difference = ( endPos - startPos );

            // Modify the appropriate axis of the manipulated transform
            glm::vec3 newObjectPos = objectPos;
            newObjectPos[axisIndex] -= difference[axisIndex];

            //BoundingFrustum boundingFrustum( vpTrans.projectionMatrix );
            //
            //if( boundingFrustum.Contains( newObjectPos ) )
            //{
                m_TransformBeingManipulated->SetPosition( newObjectPos );
            //}

            retVal = true;
        }

        _SyncManipulatorAndManipulatedTransformPositions();
    }

    return retVal;
}

//-----------------------------------------------------------------------

void ManipulatorTool::AddRenderables(RenderManager& renderManager) const
{
    for(const RenderableObject& r : m_Renderables)
    {
        renderManager.AddRenderable( r, true );
    }
}

//-----------------------------------------------------------------------

void ManipulatorTool::MaintainSize(const Camera& camera)
{
    if( m_CurrentlyActiveManipulatorTransform )
    {
        const float cameraObjectDistance = glm::length( camera.GetPosition() - m_CurrentlyActiveManipulatorTransform->GetPosition() );
        const float worldSize = ( 2.0f * glm::tan( camera.GetFieldOfView() / 2.0f ) )  * cameraObjectDistance;
        const float size = worldSize / 40.0f;

        const glm::vec3 currentScale = m_CurrentlyActiveManipulatorTransform->GetScale();
        glm::vec3 newScale( size );

        if(  newScale != currentScale )
        {
            m_CurrentlyActiveManipulatorTransform->SetScale( glm::vec3( size ) );
            _SyncManipulatorAndManipulatedTransformPositions();
        }
    }
}

//-----------------------------------------------------------------------

void ManipulatorTool::RefreshPosition()
{
    _SyncManipulatorAndManipulatedTransformPositions();
}

//-----------------------------------------------------------------------

void ManipulatorTool::_SyncManipulatorAndManipulatedTransformPositions()
{
    m_Renderables.clear();
    m_PickingRenderables.clear();

    if( m_TransformBeingManipulated && m_CurrentlyActiveManipulatorTransform )
    {
        m_CurrentlyActiveManipulatorTransform->SetPosition( m_TransformBeingManipulated->GetPosition() );

        _AddRenderablesFromGameObjectHierarchyToTheRenderList( &m_MoveManipulator );
        _AddRenderablesFromGameObjectHierarchyToThePickingList( &m_MoveManipulator );
    }
}

//-----------------------------------------------------------------------

void ManipulatorTool::_AddRenderablesFromGameObjectHierarchyToTheRenderList(GameObject* gameObject)
{
    Renderer* renderer = gameObject->GetComponent<Renderer>();

    if( renderer != nullptr )
    {
        Renderer::RenderablesList& renderablesList = renderer->GetRenderables();

        for(size_t j = 0; j < renderablesList.size(); j++)
        {
            RenderableObject& renderable = renderablesList[j];
            renderable.SetWorldTransformMatrix( gameObject->GetGlobalTransformMatrix() );
            m_Renderables.push_back( renderable );
        }
    }

    GameObject::ChildNodeList& children = gameObject->GetChildren();

    for(GameObject* child : children)
    {
        _AddRenderablesFromGameObjectHierarchyToTheRenderList( child );
    }
}

//-----------------------------------------------------------------------

void ManipulatorTool::_AddRenderablesFromGameObjectHierarchyToThePickingList(GameObject* gameObject)
{
    Renderer* renderer = gameObject->GetComponent<Renderer>();

    if( renderer != nullptr )
    {
        Renderer::RenderablesList& renderablesList = renderer->GetRenderables();

        for(size_t j = 0; j < renderablesList.size(); j++)
        {
            RenderableObject& renderable = renderablesList[j];
            renderable.SetWorldTransformMatrix( gameObject->GetGlobalTransformMatrix() );
            m_PickingRenderables.push_back( RenderManager::PickingPair( gameObject->GetID(), renderable ) );
        }
    }

    GameObject::ChildNodeList& children = gameObject->GetChildren();

    for(GameObject* child : children)
    {
        _AddRenderablesFromGameObjectHierarchyToThePickingList( child );
    }
}
