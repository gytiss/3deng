#include "SphereCollider.h"

//-----------------------------------------------------------------------

SphereCollider::SphereCollider(GameObject& owningGameObject,
                               float radius,
                               PhysicsManager& physicsManager)
: Collider( owningGameObject, e_ColliderTypeSphere, "Sphere Collider", physicsManager )
{
    _Initialize( physicsManager.CreateSphereCollisionShape( radius ) );
}

//-----------------------------------------------------------------------

void SphereCollider::SetRadius(float radius)
{
    _ChangeCollisionShape( m_PhysicsManager.CreateSphereCollisionShape( radius ) );
}

//-----------------------------------------------------------------------

float SphereCollider::GetRadius() const
{
    return m_PhysicsManager.GetSphereCollisionShapeRadius( _GetCollisionShapeHandle() );
}

//-----------------------------------------------------------------------
