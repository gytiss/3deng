#include "FBO.h"
#include "FBOColorAttachmentTexture.h"
#include "GraphicsDevice.h"
#include "DepthTexture.h"

#include <Exceptions/GenericException.h>
#include <OpenGLErrorChecking.h>
#include <CommonDefinitions.h>
#include <StringUtils.h>
#include <cstring>


//-----------------------------------------------------------------------

const size_t FBO::c_MaxNumberOfColorAttachmentTextures;

//-----------------------------------------------------------------------

bool FBO::m_CurrentlyBoundFBOIsBoundForReading = false;

//-----------------------------------------------------------------------

GLuint FBO::m_CurrentlyBoundFBOId = c_DefaultOpenGLFBOId;

//-----------------------------------------------------------------------

FBO::FBO(GraphicsDevice& graphicsDevice, SystemInterface& systemInterface)
: m_GraphicsDevice( graphicsDevice ),
  m_SystemInterface( systemInterface ),
  m_NumberOfAttachedColorTextures( 0 ),
  m_ColorAttachmentTextures(),
  m_DepthAttachmentTexture( nullptr ),
  m_OpenGLFBOId( 0 )
{
    if( m_GraphicsDevice.GetMaxNumberOfDrawBuffers() < c_MaxNumberOfColorAttachmentTextures )
    {
        std::string errMsg = "GPU has support for only less than ";
        errMsg += StringUitls::NumToString( c_MaxNumberOfColorAttachmentTextures );
        errMsg += " FBO color attachments";
        throw GenericException( errMsg );
    }

    // No color textures attached at the beginning
    memset( m_ColorAttachmentTextures, 0x00, ArraySize( m_ColorAttachmentTextures ) );

    // Create the FBO
    glGenFramebuffers( 1, &m_OpenGLFBOId );
    CheckForGLErrors();

    m_SystemInterface.RegisterDrawableSurfaceSizeChangedCallback( _DrawableSurfaceSizeChaged, this );
}

//-----------------------------------------------------------------------

FBO::~FBO()
{
    m_SystemInterface.RegisterDrawableSurfaceSizeChangedCallback( _DrawableSurfaceSizeChaged, this );

    if( m_OpenGLFBOId != 0 )
    {
        glDeleteFramebuffers( 1, &m_OpenGLFBOId );
        CheckForGLErrors();
    }

    for(size_t i = 0; i < m_NumberOfAttachedColorTextures; i++)
    {
        delete m_ColorAttachmentTextures[i];
    }

    if( m_DepthAttachmentTexture )
    {
        m_DepthAttachmentTexture->DregisterFBO( this );
    }

    // Set to null, because FBO does not own this pointer
    m_DepthAttachmentTexture = nullptr;
}

//-----------------------------------------------------------------------

void FBO::BindDefaultFBOForReading()
{
    if( ( m_CurrentlyBoundFBOId != c_DefaultOpenGLFBOId ) || !m_CurrentlyBoundFBOIsBoundForReading )
    {
        m_CurrentlyBoundFBOId = c_DefaultOpenGLFBOId;
        m_CurrentlyBoundFBOIsBoundForReading = true;

        glBindFramebuffer( GL_READ_FRAMEBUFFER, c_DefaultOpenGLFBOId );
        CheckForGLErrors();
    }
}

//-----------------------------------------------------------------------

void FBO::BindDefaultFBOForDrawing()
{
    if( ( m_CurrentlyBoundFBOId != c_DefaultOpenGLFBOId ) || m_CurrentlyBoundFBOIsBoundForReading )
    {
        m_CurrentlyBoundFBOId = c_DefaultOpenGLFBOId;
        m_CurrentlyBoundFBOIsBoundForReading = false;

        glBindFramebuffer( GL_DRAW_FRAMEBUFFER, c_DefaultOpenGLFBOId );
        CheckForGLErrors();
    }
}

//-----------------------------------------------------------------------

void FBO::BindColorAttachmentForReading(ColorAttachmentIdx colorAttachmentIdx)
{
    if( colorAttachmentIdx <= m_NumberOfAttachedColorTextures )
    {
        _Bind( true );

        glReadBuffer( GL_COLOR_ATTACHMENT0 + colorAttachmentIdx );
        CheckForGLErrors();
    }
    else
    {
        std::string errMsg = "Trying to bind not attached color attachment \"";
        errMsg += StringUitls::NumToString( colorAttachmentIdx );
        errMsg += "\" for reading";
        throw GenericException( errMsg );
    }
}

//-----------------------------------------------------------------------

void FBO::BindDepthAttachmentTextureForReading()
{
    if( m_DepthAttachmentTexture )
    {
        m_DepthAttachmentTexture->Activate();
    }
}

//-----------------------------------------------------------------------

void FBO::BindColorAttachmentTexturesForReading(size_t colorAttachmentMask)
{
    _Bind();
    UnbindColorAttachments();

    for(size_t i = 0; i < e_ColorAttachmentIdx_Count; i++)
    {
        const ColorAttachmentIdx colorAttachmentIdx = static_cast<ColorAttachmentIdx>( i );

        if( colorAttachmentMask & ( static_cast<size_t>( 1 ) << colorAttachmentIdx ) )
        {
            if( colorAttachmentIdx <= m_NumberOfAttachedColorTextures )
            {
                m_ColorAttachmentTextures[colorAttachmentIdx]->Activate();
            }
            else
            {
                std::string errMsg = "Trying to bind not attached color attachment \"";
                errMsg += StringUitls::NumToString( colorAttachmentIdx );
                errMsg += "\" for reading";
                throw GenericException( errMsg );
            }
        }
    }
}

//-----------------------------------------------------------------------

void FBO::BindColorAttachmentsForDrawing(size_t colorAttachmentMask)
{
    _Bind();

    GLenum drawBuffers[c_MaxNumberOfColorAttachmentTextures] = { GL_NONE };
    GLsizei numOfAttachmentsToEnableForDrawing = 0;

    for(size_t i = 0; i < e_ColorAttachmentIdx_Count; i++)
    {
        const ColorAttachmentIdx colorAttachmentIdx = static_cast<ColorAttachmentIdx>( i );

        if( colorAttachmentMask & ( static_cast<size_t>( 1 ) << colorAttachmentIdx ) )
        {
            if( colorAttachmentIdx <= m_NumberOfAttachedColorTextures )
            {
                drawBuffers[colorAttachmentIdx] = ( GL_COLOR_ATTACHMENT0 + static_cast<size_t>( colorAttachmentIdx ) );
                numOfAttachmentsToEnableForDrawing++;
            }
            else
            {
                std::string errMsg = "Trying to enable not attached color attachment \"";
                errMsg += StringUitls::NumToString( colorAttachmentIdx );
                errMsg += "\" for drawing";
                throw GenericException( errMsg );
            }
        }
    }

    if( numOfAttachmentsToEnableForDrawing > 0 )
    {
        glDrawBuffers( c_MaxNumberOfColorAttachmentTextures, drawBuffers );
        CheckForGLErrors();
    }
}

//-----------------------------------------------------------------------

void FBO::UnbindColorAttachments()
{
    static const GLenum c_NoneDrawBuffers[] = { GL_NONE };

    _Bind();

    glDrawBuffers( static_cast<GLsizei>( ArraySize( c_NoneDrawBuffers ) ), c_NoneDrawBuffers );
    CheckForGLErrors();
}

//-----------------------------------------------------------------------

void FBO::AttachColorTexture(FBOColorAttachmentTexture* colorTexture)
{
    if( colorTexture != nullptr )
    {
        if( m_NumberOfAttachedColorTextures < c_MaxNumberOfColorAttachmentTextures )
        {
            if( ( colorTexture->GetClass() == FBOColorAttachmentTexture::e_Class2DTexture ) && !colorTexture->IsMipmapped() )
            {
                m_NumberOfAttachedColorTextures++;

                const size_t colorTextureAttachmentNumber = (m_NumberOfAttachedColorTextures - 1);
                m_ColorAttachmentTextures[colorTextureAttachmentNumber] = colorTexture;

                _RegisterColorAttachmentTextureWithTheFBO( colorTextureAttachmentNumber );
            }
            else
            {
                throw GenericException( "Only 2D non-mipmapped color texture attachments are supported" );
            }
        }
        else
        {
            throw GenericException( "Trying to attach more color textures than the maximum limit" );
        }
    }
    else
    {
        throw GenericException( "Null pointer passed when attaching color texture" );
    }
}

//-----------------------------------------------------------------------

void FBO::AttachDepthTexture(DepthTexture* depthTexture)
{
    if( depthTexture != nullptr )
    {
        if( m_DepthAttachmentTexture == nullptr )
        {
            m_DepthAttachmentTexture = depthTexture;
            _RegisterDepthTextureWithTheFBO();
            m_DepthAttachmentTexture->RegisterFBO( this );
        }
        else
        {
            throw GenericException( "Trying to attach more than one depth texture" );
        }
    }
    else
    {
        throw GenericException( "Null pointer passed when attaching depth texture" );
    }
}

//-----------------------------------------------------------------------

void FBO::DepthTextureHasBeenReset()
{
    // Since the depth texture has been reset its OpenGL texture ID has changed and as a result it has to be re-registered with the FBO
    _RegisterDepthTextureWithTheFBO();
}

//-----------------------------------------------------------------------

const FBOColorAttachmentTexture* FBO::GetColorAttachmentTexture(ColorAttachmentIdx colorAttachmentIdx) const
{
    FBOColorAttachmentTexture* retVal = nullptr;

    if( colorAttachmentIdx < e_ColorAttachmentIdx_Count )
    {
        retVal = m_ColorAttachmentTextures[colorAttachmentIdx];
    }

    return retVal;
}

//-----------------------------------------------------------------------

void FBO::_Bind(bool bindForReading)
{
    if( _IsFBONotAlreadyBound() || ( m_CurrentlyBoundFBOIsBoundForReading != bindForReading ) )
    {
        m_CurrentlyBoundFBOId = m_OpenGLFBOId;
        m_CurrentlyBoundFBOIsBoundForReading = bindForReading;

        glBindFramebuffer( ( bindForReading ? GL_READ_FRAMEBUFFER : GL_DRAW_FRAMEBUFFER ),
                           m_OpenGLFBOId );
        CheckForGLErrors();
    }
}

//-----------------------------------------------------------------------

bool FBO::_IsFBONotAlreadyBound() const
{
    return ( m_CurrentlyBoundFBOId != m_OpenGLFBOId );
}

//-----------------------------------------------------------------------

void FBO::_RegisterColorAttachmentTextureWithTheFBO(size_t colorTextureAttachmentNumber)
{
    assert( ( colorTextureAttachmentNumber < m_NumberOfAttachedColorTextures ) && "Invalid texture attachment number" );

    _Bind();

    glFramebufferTexture2D( GL_DRAW_FRAMEBUFFER,
                            static_cast<GLenum>( GL_COLOR_ATTACHMENT0 + colorTextureAttachmentNumber ),
                            GL_TEXTURE_2D,
                            m_ColorAttachmentTextures[colorTextureAttachmentNumber]->GetOpenGLInternalTextureID(),
                            0 );
    CheckForGLErrors();

    _CheckFrameBufferStatus();
}

//-----------------------------------------------------------------------

void FBO::_RegisterDepthTextureWithTheFBO()
{
    assert( ( m_DepthAttachmentTexture != nullptr ) && "Trying to register depth texture with the FBO without any depth texture being attached" );

    _Bind();

    glFramebufferTexture2D( GL_DRAW_FRAMEBUFFER,
                            GL_DEPTH_STENCIL_ATTACHMENT,
                            GL_TEXTURE_2D,
                            m_DepthAttachmentTexture->GetOpenGLInternalTextureID(),
                            0 );
    CheckForGLErrors();

    _CheckFrameBufferStatus();
}

//-----------------------------------------------------------------------

void FBO::_CheckFrameBufferStatus()
{
    _Bind();

    bool ok = false;

    std::string errMsg = "";
    GLenum status = glCheckFramebufferStatus( GL_FRAMEBUFFER );
    CheckForGLErrors();

    switch( status )
    {
        case GL_FRAMEBUFFER_COMPLETE:
        {
            ok = true;
            break;
        }
        case GL_FRAMEBUFFER_UNSUPPORTED:
        {
            errMsg = "Unsupported framebuffer format";
            break;
        }
        case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
        {
            errMsg = "Framebuffer incomplete, missing attachment";
            break;
        }
        case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
        {
            errMsg = "Framebuffer incomplete, duplicate attachment";
            break;
        }
        case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
        {
            errMsg = "Framebuffer incomplete, GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE.";
            break;
        }
#ifndef OPENGL_ES
        case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
        {
            errMsg = "Framebuffer incomplete, combination of internal formats of the attached images violates an implementation-dependent set of restrictions.";
            break;
        }
        case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
        {
            errMsg = "Framebuffer incomplete, missing draw buffer";
            break;
        }
        case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
        {
            errMsg = "Framebuffer incomplete, missing read buffer";
            break;
        }
#endif
        default:
        {
            errMsg = "Warning: unknown frame buffer status";
            break;
        }
    }

    if( !ok )
    {
        throw GenericException( errMsg );
    }
}

//-----------------------------------------------------------------------

void FBO::_ReinitializeColorAttachments(const SystemInterface::DrawableSurfaceSize& newSize)
{
    for(size_t i = 0; i < m_NumberOfAttachedColorTextures; i++)
    {
        m_ColorAttachmentTextures[i]->Reinitialize( newSize.widthInPixels, newSize.heightInPixels );
        _RegisterColorAttachmentTextureWithTheFBO( i );
    }
}

//-----------------------------------------------------------------------

void FBO::_DrawableSurfaceSizeChaged(const SystemInterface::DrawableSurfaceSize& newSize, void* userData)
{
    static_cast<FBO*>( userData )->_ReinitializeColorAttachments( newSize );
}

//-----------------------------------------------------------------------
