#ifndef GLWIDGET_H_INCLUDED
#define GLWIDGET_H_INCLUDED

#define GL3_PROTOTYPES 1    // Ensure we are using opengl's core profile only
#include <OpenGLIncludes.h> // Needs to be included first, because "QGLWidget" includes "gl.h"

#include <SystemInterface.h>
#include <InputMapping/InputMapper.h>

#include <QtOpenGL/QGLWidget>
#include <QtCore/QElapsedTimer>

#include <map>
#include <CommonDefinitions.h>

class QGLContext;
class EditorMainLoop;
class MainWindow;

class GLWidget : public QGLWidget, public SystemInterface
{
public:
    typedef void (*CallbackFunctionPtr)(void* data);

    GLWidget(MainWindow& mainWindow,
             InputMapping::InputMapper& inputMapper,
             FileManager& fileManager,
             QGLContext* glContext,
             QWidget* parent = 0);

    virtual ~GLWidget();

    const DrawableSurfaceSize& GetDrawableSurfaceSize() const;
    
    void SetCaptureMouseInTheWindow(bool val);
    bool GetCaptureMouseInTheWindow() const;

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
    void keyPressEvent(QKeyEvent* event);
    void keyReleaseEvent(QKeyEvent* event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent* event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent* event);

    uint64_t _GetTicksInMilliseconds();

private:
    typedef std::map<int, InputMapping::RawInputButton> QtKeyCodesToRawInputButtonsMap;

    // Used to prevent copying
    GLWidget& operator = (const GLWidget& other);
    GLWidget(const GLWidget& other);

    void _CenterMouseCursor();
    void _SetMouseCursorVisible(bool val);
    void _InitializeMainKeyCodeMap(QtKeyCodesToRawInputButtonsMap& keyCodeMap);
    void _InitializeKeyPadKeyCodeMap(QtKeyCodesToRawInputButtonsMap& keyCodeMap);
    bool _MapKeyboardEventToRawInputButton(QKeyEvent* event, InputMapping::RawInputButton& outRawInputButton);
    bool _MapQtKeyCodeToRawInputButton(int qtKeyCode,
                                       const QtKeyCodesToRawInputButtonsMap& keyCodeMap,
                                       InputMapping::RawInputButton& outRawInputButton);

    bool _CheckIfContextSupportsOpenGLVersion(const QGLContext* glContext,
                                              int major,
                                              int minor);
    void _LogOpenGLContextDetails(const QGLContext* glContext);
    static void MainInputCallback(InputMapping::MappedInput& inputs, float frameTimeInSeconds, void* glWidget);

    InputMapping::InputMapper& m_InputMapper;
    FileManager& m_FileManager;
    QtKeyCodesToRawInputButtonsMap m_MainKeyCodeMap;
    QtKeyCodesToRawInputButtonsMap m_KeyPadKeyCodeMap;
    QElapsedTimer m_ElapsedTimer;
    SystemInterface::DrawableSurfaceSize m_DrawableSurfaceSize;
    int m_PreviousAbsoluteMousePosX;
    int m_PreviousAbsoluteMousePosY;
    bool m_MouseCapturedInTheWIndow;

    MainWindow& m_MainWindow;
    EditorMainLoop* m_EditorMainLoop;
};

#endif // GLWIDGET_H_INCLUDED
