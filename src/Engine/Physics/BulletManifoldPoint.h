#ifndef BULLETMANIFOLDPOINT_H_INCLUDED
#define BULLETMANIFOLDPOINT_H_INCLUDED

#include <bullet/BulletCollision/NarrowPhaseCollision/btManifoldPoint.h>

class BulletManifoldPoint
{
public:
  BulletManifoldPoint(btManifoldPoint &pt);
  inline ~BulletManifoldPoint()
  {
  }

  int get_lift_time() const;
  float get_distance() const;
  float get_applied_impulse() const;
  btVector3 get_position_world_on_a() const;
  btVector3 get_position_world_on_b() const;
  btVector3 get_local_point_a() const;
  btVector3 get_local_point_b() const;

  int get_part_id0() const;
  int get_part_id1() const;
  int get_index0() const;
  int get_index1() const;

private:
  btManifoldPoint &_pt;
};

#endif // BULLETMANIFOLDPOINT_H_INCLUDED
