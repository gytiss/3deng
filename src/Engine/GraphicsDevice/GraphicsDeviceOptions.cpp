#include "GraphicsDeviceOptions.h"

#include <OpenGLErrorChecking.h>
#include <cassert>
#include <cstring>

//-----------------------------------------------------------------------

/**
 * This array is indexed using "Option" enum
 */
const GLenum GraphicsDeviceOptions::m_OpenGLOptionList[GraphicsDeviceOptions::e_OptionCount] =
{
    GL_BLEND,
    GL_CULL_FACE,
    GL_DEPTH_TEST,
    GL_DITHER,
    GL_POLYGON_OFFSET_FILL,
    GL_PRIMITIVE_RESTART_FIXED_INDEX,
    GL_RASTERIZER_DISCARD,
    GL_SAMPLE_ALPHA_TO_COVERAGE,
    GL_SAMPLE_COVERAGE,
    GL_SCISSOR_TEST,
    GL_STENCIL_TEST,
#ifndef OPENGL_ES
    GL_FRAMEBUFFER_SRGB,
    GL_DEBUG_OUTPUT,
    GL_DEBUG_OUTPUT_SYNCHRONOUS
#endif
};

//-----------------------------------------------------------------------

/**
 * This array is indexed using "PixelStoreOption" enum
 */
const GLenum GraphicsDeviceOptions::m_OpenGLPixelStoreOptionList[GraphicsDeviceOptions::e_PixelStoreOptionCount] =
{
    GL_PACK_ROW_LENGTH,
#ifndef OPENGL_ES
    GL_PACK_IMAGE_HEIGHT,
#endif
    GL_PACK_SKIP_ROWS,
    GL_PACK_SKIP_PIXELS,
#ifndef OPENGL_ES
    GL_PACK_SKIP_IMAGES,
#endif
    GL_PACK_ALIGNMENT,

    GL_UNPACK_ROW_LENGTH,
    GL_UNPACK_IMAGE_HEIGHT,
    GL_UNPACK_SKIP_ROWS,
    GL_UNPACK_SKIP_PIXELS,
    GL_UNPACK_SKIP_IMAGES,
    GL_UNPACK_ALIGNMENT
};

//-----------------------------------------------------------------------

/**
 * This array is indexed using "PixelStoreBooleanOption" enum
 */
#ifndef OPENGL_ES
const GLenum GraphicsDeviceOptions::m_OpenGLPixelStoreBooleanOptionList[GraphicsDeviceOptions::e_PixelStoreBooleanOptionCount] =
{
    GL_PACK_SWAP_BYTES,
    GL_PACK_LSB_FIRST,
    GL_UNPACK_SWAP_BYTES,
    GL_UNPACK_LSB_FIRST
};
#endif

//-----------------------------------------------------------------------

GraphicsDeviceOptions::GraphicsDeviceOptions()
: m_OptionStatesStacks(),
  m_CurrentOptionStates(),
  m_PixelStoreOptionValuesStacks(),
  m_CurrentPixelStoreOptionValues()
#ifndef OPENGL_ES
  ,m_PixelStoreBooleanOptionStatesStacks(),
  m_CurrentPixelStoreBooleanOptionStates()
#endif
{
    // Initialise all entries to "OptionStateUnknown"
    memset( m_CurrentOptionStates, 0x00, sizeof( m_CurrentOptionStates ) );

#ifndef OPENGL_ES
    // Initialise all entries to "OptionStateUnknown"
    memset( m_CurrentPixelStoreBooleanOptionStates, 0x00, sizeof( m_CurrentPixelStoreBooleanOptionStates ) );
#endif

    // Get the default Pixel Store options values
    for(size_t i = 0; i < e_PixelStoreOptionCount; i++)
    {
        GLint value = 0;
        glGetIntegerv( m_OpenGLPixelStoreOptionList[i], &value );
        CheckForGLErrors();

        m_CurrentPixelStoreOptionValues[i] = value;
    }
}

//-----------------------------------------------------------------------

void GraphicsDeviceOptions::OptionEnable(Option op)
{
    if( m_CurrentOptionStates[op] != OptionStateEnabled )
    {
        m_CurrentOptionStates[op] = OptionStateEnabled;

        glEnable( m_OpenGLOptionList[op] );
        CheckForGLErrors();
    }
}

//-----------------------------------------------------------------------

void GraphicsDeviceOptions::OptionDisable(Option op)
{
    if( m_CurrentOptionStates[op] != OptionStateDisabled )
    {
        m_CurrentOptionStates[op] = OptionStateDisabled;

        glDisable( m_OpenGLOptionList[op] );
        CheckForGLErrors();
    }
}

//-----------------------------------------------------------------------

void GraphicsDeviceOptions::OptionSaveCurrentState(Option op)
{
    if( m_CurrentOptionStates[op] == OptionStateUnknown )
    {
        if( glIsEnabled( m_OpenGLOptionList[op] ) == GL_TRUE )
        {
            m_CurrentOptionStates[op] = OptionStateEnabled;
        }
        else
        {
            m_CurrentOptionStates[op] = OptionStateDisabled;
        }
        CheckForGLErrors();
    }

    m_OptionStatesStacks[op].push( m_CurrentOptionStates[op] );
}

//-----------------------------------------------------------------------

void GraphicsDeviceOptions::OptionRestorePreviousState(Option op)
{
    if( m_OptionStatesStacks[op].empty() )
    {
        assert( false && "OptionRestorePreviousState() called more times than OptionSaveCurrentState()" );
    }

    const OptionState previousState = m_OptionStatesStacks[op].top();
    m_OptionStatesStacks[op].pop();

    if( previousState != m_CurrentOptionStates[op] )
    {
        if ( previousState == OptionStateEnabled )
        {
            OptionEnable( op );
        }
        else if( previousState == OptionStateDisabled )
        {
            OptionDisable( op );
        }
        else
        {
            assert( false && "Cannot restore OptionStateUnknown option state" );
        }
    }
}

//-----------------------------------------------------------------------

void GraphicsDeviceOptions::SetPixelStoreOptionValue(PixelStoreOption op, int value)
{
    if( value != m_CurrentPixelStoreOptionValues[op] )
    {
        _SetPixelStoreOptionValue( m_OpenGLPixelStoreOptionList[op], value );
    }
}

//-----------------------------------------------------------------------

void GraphicsDeviceOptions::PixelStoreOptionSaveCurrentValue(PixelStoreOption op)
{
    m_PixelStoreOptionValuesStacks[op].push( m_CurrentPixelStoreOptionValues[op] );
}

//-----------------------------------------------------------------------

void GraphicsDeviceOptions::PixelStoreOptionRestorePreviousValue(PixelStoreOption op)
{
    const int previousValue = m_PixelStoreOptionValuesStacks[op].top();
    m_PixelStoreOptionValuesStacks[op].pop();

    SetPixelStoreOptionValue( op, previousValue );
}

//-----------------------------------------------------------------------

void GraphicsDeviceOptions::PixelStoreBooleanOptionEnable(PixelStoreBooleanOption op)
{
#ifndef OPENGL_ES
    if( m_CurrentPixelStoreBooleanOptionStates[op] != OptionStateEnabled )
    {
        m_CurrentPixelStoreBooleanOptionStates[op] = OptionStateEnabled;

        _SetPixelStoreOptionValue( m_OpenGLPixelStoreBooleanOptionList[op], GL_TRUE );
    }
#endif
}

//-----------------------------------------------------------------------

void GraphicsDeviceOptions::PixelStoreBooleanOptionDisable(PixelStoreBooleanOption op)
{
#ifndef OPENGL_ES
    if( m_CurrentPixelStoreBooleanOptionStates[op] != OptionStateDisabled )
    {
        m_CurrentPixelStoreBooleanOptionStates[op] = OptionStateDisabled;

        _SetPixelStoreOptionValue( m_OpenGLPixelStoreBooleanOptionList[op], GL_FALSE );
    }
#endif
}

//-----------------------------------------------------------------------

void GraphicsDeviceOptions::PixelStoreBooleanOptionSaveCurrentState(PixelStoreBooleanOption op)
{
#ifndef OPENGL_ES
    if( m_CurrentPixelStoreBooleanOptionStates[op] == OptionStateUnknown )
    {
        GLboolean value = GL_FALSE;
        glGetBooleanv( m_OpenGLPixelStoreBooleanOptionList[op], &value );
        CheckForGLErrors();

        if( value == GL_TRUE )
        {
            m_CurrentPixelStoreBooleanOptionStates[op] = OptionStateEnabled;
        }
        else
        {
            m_CurrentPixelStoreBooleanOptionStates[op] = OptionStateDisabled;
        }
    }

    m_PixelStoreBooleanOptionStatesStacks[op].push( m_CurrentPixelStoreBooleanOptionStates[op] );
#endif
}

//-----------------------------------------------------------------------

void GraphicsDeviceOptions::PixelStoreBooleanOptionRestorePreviousState(PixelStoreBooleanOption op)
{
#ifndef OPENGL_ES
    if( m_PixelStoreBooleanOptionStatesStacks[op].empty() )
    {
        assert( false && "PixelStoreBooleanOptionRestorePreviousState() called more times than PixelStoreBooleanOptionSaveCurrentState()" );
    }

    const OptionState previousState = m_PixelStoreBooleanOptionStatesStacks[op].top();
    m_PixelStoreBooleanOptionStatesStacks[op].pop();

    if( previousState != m_CurrentPixelStoreBooleanOptionStates[op] )
    {
        if ( previousState == OptionStateEnabled )
        {
            PixelStoreBooleanOptionEnable( op );
        }
        else if( previousState == OptionStateDisabled )
        {
            PixelStoreBooleanOptionDisable( op );
        }
        else
        {
            assert( false && "Cannot restore OptionStateUnknown option state" );
        }
    }
#endif
}

//-----------------------------------------------------------------------


void GraphicsDeviceOptions::_SetPixelStoreOptionValue(GLenum optionGLEnum, GLint optionValue)
{
    glPixelStorei( optionGLEnum, optionValue );
    CheckForGLErrors();
}

//-----------------------------------------------------------------------
