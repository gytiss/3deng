#include "MaterialManager.h"
#include "TextureManager.h"
#include "Material.h"
#include <Exceptions/GenericException.h>

#include <luabind/luabind.hpp>
#include <luabind/operator.hpp>
#include <LuaUtils.h>
#include <vector>
#include <glm/gtc/type_ptr.hpp>

#include <MD5.h>

//-----------------------------------------------------------------------

const MaterialManager::MatMapNameToTextureTypeIdPair MaterialManager::m_MatMapNameToTextureType[] =
{
    MatMapNameToTextureTypeIdPair( "DiffuseMap",  Texture::e_TypeIdxDiffuseMap ),
    MatMapNameToTextureTypeIdPair( "NormalMap",   Texture::e_TypeIdxNormalMap ),
    MatMapNameToTextureTypeIdPair( "SpecularMap", Texture::e_TypeIdxSpecularMap ),
    MatMapNameToTextureTypeIdPair( "GlossMap",    Texture::e_TypeIdxGlossMap )
};

//-----------------------------------------------------------------------

const MaterialManager::MatMapWrapModeNameToTextureWrapMode MaterialManager::m_MatMapSWrapModeNameToTextureWrapMode[] =
{
    MatMapWrapModeNameToTextureWrapMode( "Repeat", Texture::e_StateParamArgSCoordWrapRepeat ),
    MatMapWrapModeNameToTextureWrapMode( "MirroredRepeat", Texture::e_StateParamArgSCoordWrapMirroredRepeat ),
    MatMapWrapModeNameToTextureWrapMode( "ClampToEdge", Texture::e_StateParamArgSCoordWrapClampToEdge ),
};

//-----------------------------------------------------------------------

const MaterialManager::MatMapWrapModeNameToTextureWrapMode MaterialManager::m_MatMapTWrapModeNameToTextureWrapMode[] =
{
    MatMapWrapModeNameToTextureWrapMode( "Repeat", Texture::e_StateParamArgTCoordWrapRepeat ),
    MatMapWrapModeNameToTextureWrapMode( "MirroredRepeat", Texture::e_StateParamArgTCoordWrapMirroredRepeat ),
    MatMapWrapModeNameToTextureWrapMode( "ClampToEdge", Texture::e_StateParamArgTCoordWrapClampToEdge ),
};

//-----------------------------------------------------------------------

const MaterialManager::MatMapWrapModeNameToTextureWrapMode MaterialManager::m_MatMapRWrapModeNameToTextureWrapMode[] =
{
    MatMapWrapModeNameToTextureWrapMode( "Repeat", Texture::e_StateParamArgRCoordWrapRepeat ),
    MatMapWrapModeNameToTextureWrapMode( "MirroredRepeat", Texture::e_StateParamArgRCoordWrapMirroredRepeat ),
    MatMapWrapModeNameToTextureWrapMode( "ClampToEdge", Texture::e_StateParamArgRCoordWrapClampToEdge ),
};

//-----------------------------------------------------------------------

bool MaterialManager::m_ClassAlreadyInstantiated = false;

//-----------------------------------------------------------------------

MaterialManager::MaterialManager(TextureManager& textureManager,
                                 FileManager& fileManager)
: m_TextureManager( textureManager ),
  m_FileManager( fileManager )
{
    if( m_ClassAlreadyInstantiated )
    {
        assert( false && "There can only be one instance of MaterialManager()" );
    }
    else
    {
        m_ClassAlreadyInstantiated = true;
    }
}

//-----------------------------------------------------------------------

MaterialManager::~MaterialManager()
{
    MaterialList::iterator it = m_MaterialList.begin();
    while( it != m_MaterialList.end() )
    {
        RefCountedMaterialPtr& materialPtr = *it;
        delete materialPtr.material;
        it = m_MaterialList.erase( it );
    }
}

//-----------------------------------------------------------------------

MaterialManager::MaterialHandle MaterialManager::LoadMaterial(const std::string& materialFilePath)
{
    MaterialList::iterator materialIt;

    MaterialPathsToMaterialsMap::iterator it = m_MaterialPathsToMaterialsMap.find( materialFilePath );
    if( it != m_MaterialPathsToMaterialsMap.end() )
    {
        materialIt = it->second;
        materialIt->refCount = ( materialIt->refCount + 1 );
    }
    else
    {
        materialIt = _LoadMaterialFromFile( materialFilePath );
    }

    return MaterialHandle( materialIt );
}

//-----------------------------------------------------------------------

MaterialManager::MaterialHandle MaterialManager::AddInternalMaterial(const MaterialDescriptor& descriptor)
{
    static const char* c_InternalMaterialPathPrefix = "internalMaterial:";

    MaterialList::iterator retVal = m_MaterialList.end();

    RefCountedMaterialPtr materialPtr;
    materialPtr.refCount = 1;
    materialPtr.material = new Material( descriptor );
    materialPtr.materialFilePath = c_InternalMaterialPathPrefix;
    materialPtr.materialFilePath += _ConstructUniqueInternalMaterialName( *materialPtr.material );

    m_MaterialList.push_front( materialPtr );
    retVal = m_MaterialList.begin();

    m_MaterialPathsToMaterialsMap.insert( MaterialPathsToMaterialsMap::value_type( materialPtr.materialFilePath, retVal ) );

    return retVal;
}

//-----------------------------------------------------------------------

MaterialManager::MaterialHandle MaterialManager::DuplicateMaterialHandle(const MaterialHandle& handle)
{
    assert( handle.IsValid() && "Material handle must be valid" );

    MaterialHandle duplicateHandle;

    if( handle.IsValid() )
    {
        RefCountedMaterialPtr& materialPtr = *handle.m_MaterialListIt;
        materialPtr.refCount = ( materialPtr.refCount + 1 );
        duplicateHandle = handle;
    }

    return duplicateHandle;
}

//-----------------------------------------------------------------------

void MaterialManager::ReleaseMaterial(MaterialHandle& handle)
{
    assert( handle.IsValid() && "Material handle must be valid" );

    if( handle.IsValid() )
    {
        RefCountedMaterialPtr& materialPtr = *handle.m_MaterialListIt;
        materialPtr.refCount = ( materialPtr.refCount - 1 );

        if( materialPtr.refCount == 0 )
        {
            delete materialPtr.material;
            m_MaterialPathsToMaterialsMap.erase( materialPtr.materialFilePath );
            m_MaterialList.erase( handle.m_MaterialListIt );
        }

        handle.Invalidate();
    }
}

//-----------------------------------------------------------------------

template <class SecondPairElementType>
static std::string ListOfAvailableOptions(const std::pair<const char*, SecondPairElementType>* mapPairsArray,
                                            size_t mapPairsArraySize)
{
    std::string retVal = "";

    for(size_t i = 0; (i < mapPairsArraySize); i++)
    {
        retVal += "\"";
        retVal += mapPairsArray[i].first;
        retVal += "\"";

        if( i < ( mapPairsArraySize - 1 ) )
        {
            retVal += ", ";
        }
    }

    return retVal;
}

//-----------------------------------------------------------------------

static bool GetColorPropertyValue(const std::string& colorPropertyName,
                                   const luabind::object& materialProperty,
                                   std::string& outErrMsg,
                                   glm::vec3& outSRGBColorData)
{
    bool ok = true;

    size_t colorChannelIndex = 0;
    for (luabind::iterator valIt(materialProperty), end; ok && ( valIt != end ); ++valIt)
    {
        const luabind::object& val = *valIt;

        if( ( luabind::type( val ) == LUA_TNUMBER ) && ( colorChannelIndex < 3 ) )
        {
            const float colorChannelVal = luabind::object_cast<float>( val );

            if( ( colorChannelVal >= 0.0f ) && ( colorChannelVal <= 255.0f ) )
            {
                outSRGBColorData[static_cast<glm::length_t>( colorChannelIndex )] = glm::clamp( colorChannelVal / 255.0f, 0.0f, 1.0f );
                colorChannelIndex++;
            }
            else
            {
                ok = false;
            }
        }
        else
        {
            ok = false;
        }
    }

    if( !ok )
    {
        outErrMsg += "Color \"";
        outErrMsg += colorPropertyName;
        outErrMsg += "\" must consist of three comma separated integer numbers in the range of 0 to 255 e.g. { 255, 255, 255 } for while color";
    }

    return ok;
}


//-----------------------------------------------------------------------

MaterialManager::MaterialList::iterator MaterialManager::_LoadMaterialFromFile(const std::string& materialFilePath)
{
    MaterialList::iterator retVal = m_MaterialList.end();

    std::vector<MaterialMapEntry> materialMapEntries;
    glm::vec3 srgbDiffuseColor;
    glm::vec3 srgbSpecularColor;
    uint8_t glossinessValue = 0;
    bool diffuseColorPresent = false;
    bool specularColorPresent = false;
    bool glossinessValuePresent = false;

    std::string mainErrMsg = "Failed to load material from file '" + materialFilePath + "' ";
    bool ok = false;

    static const char c_TopTableName[] = "Material";
    static const char c_DiffuseColorPropertyName[] = "DiffuseSRGBColor";
    static const char c_SpecularColorPropertyName[] = "SpecularSRGBColor";
    static const char c_GlossinessPropertyName[] = "GlossinessValue";

    LuaUtils::LuaStateWrapper luaState;
    luaL_openlibs( luaState );
    if( LuaUtils::LoadLuaScript( luaState, materialFilePath, m_FileManager ) )
    {
        std::string secondaryErrMsg = "with the following error: ";
        ok = true;

        // Connect LuaBind to this lua state
        luabind::open( luaState );

        luabind::object topTable = luabind::globals( luaState )[c_TopTableName];

        if( !topTable )
        {
            secondaryErrMsg += "Global Lua table '";
            secondaryErrMsg += c_TopTableName;
            secondaryErrMsg += "' does not exist";
            ok = false;
        }

        if( ok && ( luabind::type( topTable ) == LUA_TTABLE ) )
        {
            for (luabind::iterator i(topTable), end; i != end && ok; ++i)
            {
                const luabind::object& materialProperty = *i;
                const std::string& materialPropertyName = luabind::tostring_operator( i.key() );

                if( luabind::type( materialProperty ) == LUA_TTABLE )
                {
                    Texture::TypeIdx textureTypeIdx = Texture::e_TypeIdxCount;

                    if( _MatMapNameToTextureTypeIdx( materialPropertyName, textureTypeIdx ) )
                    {
                        static const char c_TextureFieldName[] = "texture";

                        luabind::object tempVal = luabind::gettable( materialProperty, c_TextureFieldName );
                        if( tempVal )
                        {
                            if( luabind::type( tempVal ) == LUA_TSTRING )
                            {
                                MaterialMapEntry matMapEntry;
                                matMapEntry.textureFilePath = luabind::object_cast<const char *>( tempVal );
                                matMapEntry.textureTypeIdx = textureTypeIdx;
                                matMapEntry.sWrapMode = Texture::e_StateParamArgSCoordWrapRepeat;
                                matMapEntry.tWrapMode = Texture::e_StateParamArgTCoordWrapRepeat;

                                materialMapEntries.push_back( matMapEntry );
                            }
                            else
                            {
                                secondaryErrMsg += "Value of field '";
                                secondaryErrMsg += c_TextureFieldName;
                                secondaryErrMsg += "' in table '";
                                secondaryErrMsg += luabind::tostring_operator( i.key() );
                                secondaryErrMsg += "' must be a string";
                                ok = false;
                            }
                        }
                        else
                        {
                            secondaryErrMsg += "Mandatory member 'texture'(i.e. texture = \"PathTotexture\") has not been found in '";
                            secondaryErrMsg += luabind::tostring_operator( i.key() );
                            secondaryErrMsg += "'";
                            ok = false;
                        }

                        if( ok )
                        {
                            static const char c_SWrapMode[] = "sWrapMode";
                            tempVal = luabind::gettable( materialProperty, c_SWrapMode );
                            if( tempVal )
                            {
                                Texture::StateParamArg sWrapMode = Texture::e_StateParamArgSCoordWrapRepeat;

                                bool invalidVal = false;
                                if( luabind::type( tempVal ) == LUA_TSTRING )
                                {
                                    const char* mode = luabind::object_cast<const char *>( tempVal );

                                    if( !_MatMapSWrapModeNameToTextureWrapMode( mode, sWrapMode ) )
                                    {
                                        invalidVal = true;
                                    }
                                }
                                else
                                {
                                    invalidVal = true;
                                }

                                if( invalidVal )
                                {
                                    secondaryErrMsg += "Member '";
                                    secondaryErrMsg += c_SWrapMode;
                                    secondaryErrMsg += "' of table '";
                                    secondaryErrMsg += luabind::tostring_operator( i.key() );
                                    secondaryErrMsg += "' must have one of the following string values: ";
                                    secondaryErrMsg += ListOfAvailableOptions<Texture::StateParamArg>( m_MatMapSWrapModeNameToTextureWrapMode,
                                                                                                       ArraySize( m_MatMapSWrapModeNameToTextureWrapMode ) );
                                    ok = false;
                                }
                                else
                                {
                                    materialMapEntries[materialMapEntries.size() - 1].sWrapMode = sWrapMode;
                                }
                            }
                        }

                        if( ok )
                        {
                            static const char c_TWrapMode[] = "tWrapMode";
                            tempVal = luabind::gettable( materialProperty, c_TWrapMode );
                            if( tempVal )
                            {
                                Texture::StateParamArg tWrapMode = Texture::e_StateParamArgTCoordWrapRepeat;

                                bool invalidVal = false;
                                if( luabind::type( tempVal ) == LUA_TSTRING )
                                {
                                    const char* mode = luabind::object_cast<const char *>( tempVal );

                                    if( !_MatMapSWrapModeNameToTextureWrapMode( mode, tWrapMode ) )
                                    {
                                        invalidVal = true;
                                    }
                                }
                                else
                                {
                                    invalidVal = true;
                                }

                                if( invalidVal )
                                {
                                    secondaryErrMsg += "Member '";
                                    secondaryErrMsg += c_TWrapMode;
                                    secondaryErrMsg += "' of table '";
                                    secondaryErrMsg += luabind::tostring_operator( i.key() );
                                    secondaryErrMsg += "' must have one of the following string values: ";
                                    secondaryErrMsg += ListOfAvailableOptions<Texture::StateParamArg>( m_MatMapTWrapModeNameToTextureWrapMode,
                                                                                                       ArraySize( m_MatMapTWrapModeNameToTextureWrapMode ) );
                                    ok = false;
                                }
                                else
                                {
                                    materialMapEntries[materialMapEntries.size() - 1].tWrapMode = tWrapMode;
                                }
                            }
                        }
                    }
                    else if( materialPropertyName.compare( c_DiffuseColorPropertyName ) == 0 )
                    {
                        std::string errMsg = "";

                        if( GetColorPropertyValue( materialPropertyName, materialProperty, errMsg, srgbDiffuseColor ) )
                        {
                            diffuseColorPresent = true;
                        }
                        else
                        {
                            secondaryErrMsg += errMsg;
                            ok = false;
                        }
                    }
                    else if( materialPropertyName.compare( c_SpecularColorPropertyName ) == 0 )
                    {
                        std::string errMsg = "";

                        if( GetColorPropertyValue( materialPropertyName, materialProperty, errMsg, srgbSpecularColor ) )
                        {
                            specularColorPresent = true;
                        }
                        else
                        {
                            secondaryErrMsg += errMsg;
                            ok = false;
                        }
                    }
                    else
                    {
                        secondaryErrMsg += "Unknown material property '";
                        secondaryErrMsg += luabind::tostring_operator( i.key() );
                        secondaryErrMsg += "'";
                        ok = false;
                    }
                }

                else if( materialPropertyName.compare( c_GlossinessPropertyName ) == 0 )
                {
                    ok = false;

                    if( luabind::type( materialProperty ) == LUA_TNUMBER )
                    {
                        const float number = luabind::object_cast<float>( materialProperty );
                        if( ( number >= 0.0f ) && ( number <= 255.0f ) )
                        {
                            glossinessValue = static_cast<uint8_t>( number );
                            glossinessValuePresent = true;
                            ok = true;
                        }
                    }

                    if( !ok )
                    {
                        secondaryErrMsg += "Value \"";
                        secondaryErrMsg += c_GlossinessPropertyName;
                        secondaryErrMsg += "\" must be an integer number in the range of 0 to 255";
                    }
                }

                else
                {
                    secondaryErrMsg += "Unknown material property '";
                    secondaryErrMsg += luabind::tostring_operator( i.key() );
                    secondaryErrMsg += "'";
                    ok = false;
                }
            }
        }
        else if( ok )
        {
            secondaryErrMsg += "Global Lua variable '";
            secondaryErrMsg += c_TopTableName;
            secondaryErrMsg += "' is not a table";
            ok = false;
        }

        if( !ok )
        {
            mainErrMsg += secondaryErrMsg;
        }
    }

    if( ok )
    {
        if( !materialMapEntries.empty() )
        {
            if( _CheckIfDiffuseMapIsPresent( materialMapEntries ) )
            {
                Material* mat = new Material( m_TextureManager );

                if( diffuseColorPresent )
                {
                    mat->m_MaterialDescriptor.SetDiffuseColor( srgbDiffuseColor );
                }

                if( specularColorPresent )
                {
                    mat->m_MaterialDescriptor.SetSpercularColor( srgbSpecularColor );
                }

                if( glossinessValuePresent )
                {
                    mat->m_MaterialDescriptor.SetGlossiness( glossinessValue );
                }

                for(size_t i = 0; i < materialMapEntries.size(); i++)
                {
                    const MaterialMapEntry& matMapEntry = materialMapEntries[i];

                    bool isThisTextureMipMapped = true;
                    bool isThisTextureSRGBEncoded = true;

                    if( ( matMapEntry.textureTypeIdx == Texture::e_TypeIdxNormalMap ) ||
                        ( matMapEntry.textureTypeIdx == Texture::e_TypeIdxSpecularMap ) )
                    {
                        isThisTextureSRGBEncoded = false;
                    }

                    TextureManager::TextureHandle texHandle = m_TextureManager.Load2DTexture( matMapEntry.textureFilePath,
                                                                                              matMapEntry.textureTypeIdx,
                                                                                              matMapEntry.sWrapMode,
                                                                                              matMapEntry.tWrapMode,
                                                                                              isThisTextureMipMapped,
                                                                                              isThisTextureSRGBEncoded );
                    mat->m_MaterialDescriptor.AddTexture( texHandle );
                }

                _AddNullMapsInPlaceOfMissingOnes( materialMapEntries, *mat );

                RefCountedMaterialPtr materialPtr;
                materialPtr.refCount = 1;
                materialPtr.material = mat;
                materialPtr.materialFilePath = materialFilePath;

                m_MaterialList.push_front( materialPtr );
                retVal = m_MaterialList.begin();

                m_MaterialPathsToMaterialsMap.insert( MaterialPathsToMaterialsMap::value_type( materialFilePath, retVal ) );
            }
            else
            {
                mainErrMsg += "Table '";
                mainErrMsg += c_TopTableName;
                mainErrMsg += "' is required to have DiffuseMap map entry.";
                ok = false;
            }
        }
        else
        {
            mainErrMsg += "Table '";
            mainErrMsg += c_TopTableName;
            mainErrMsg += "' is empty. One or more of the following ";
            mainErrMsg += ListOfAvailableOptions<Texture::TypeIdx>( m_MatMapNameToTextureType,
                                                                    ArraySize( m_MatMapNameToTextureType ) );
            mainErrMsg += " map entries are expected to be in this table.";
            ok = false;
        }
    }

    if( !ok )
    {
        throw GenericException( mainErrMsg );
    }

    return retVal;
}

//-----------------------------------------------------------------------

template <class EnumType>
static bool StringToCorrespondingEnum(const std::string& strToFind,
                                        const std::pair<const char*, EnumType>* mapPairsArray,
                                        size_t mapPairsArraySize,
                                        EnumType& outEnum)
{
    bool retVal = false;

    for(size_t i = 0; (i < mapPairsArraySize) && !retVal; i++)
    {
        if( strToFind.compare( mapPairsArray[i].first ) == 0 )
        {
            outEnum = static_cast<EnumType>( mapPairsArray[i].second );

            retVal = true;
        }
    }

    return retVal;
}

//-----------------------------------------------------------------------

bool MaterialManager::_MatMapNameToTextureTypeIdx(const std::string& matMapName,
                                                   Texture::TypeIdx& idx)
{
    return StringToCorrespondingEnum<Texture::TypeIdx>( matMapName,
                                                         m_MatMapNameToTextureType,
                                                         ArraySize( m_MatMapNameToTextureType ),
                                                         idx );
}

//-----------------------------------------------------------------------

bool MaterialManager::_MatMapSWrapModeNameToTextureWrapMode(const std::string& matWrapModeName,
                                                            Texture::StateParamArg& mode)
{
    return StringToCorrespondingEnum<Texture::StateParamArg>( matWrapModeName,
                                                               m_MatMapSWrapModeNameToTextureWrapMode,
                                                               ArraySize( m_MatMapSWrapModeNameToTextureWrapMode ),
                                                               mode );
}

//-----------------------------------------------------------------------

bool MaterialManager::_MatMapTWrapModeNameToTextureWrapMode(const std::string& matWrapModeName,
                                                            Texture::StateParamArg& mode)
{
    return StringToCorrespondingEnum<Texture::StateParamArg>( matWrapModeName,
                                                               m_MatMapTWrapModeNameToTextureWrapMode,
                                                               ArraySize( m_MatMapTWrapModeNameToTextureWrapMode ),
                                                               mode );
}

//-----------------------------------------------------------------------

bool MaterialManager::_MatMapRWrapModeNameToTextureWrapMode(const std::string& matWrapModeName,
                                                            Texture::StateParamArg& mode)
{
    return StringToCorrespondingEnum<Texture::StateParamArg>( matWrapModeName,
                                                               m_MatMapRWrapModeNameToTextureWrapMode,
                                                               ArraySize( m_MatMapRWrapModeNameToTextureWrapMode ),
                                                               mode );
}

//-----------------------------------------------------------------------

bool MaterialManager::_CheckIfDiffuseMapIsPresent(const std::vector<MaterialMapEntry>& materialMapEntries)
{
    bool retVal = false;

    const size_t mapCount = materialMapEntries.size();
    for(size_t i = 0; !retVal && ( i < mapCount ); i++)
    {
        if( materialMapEntries[i].textureTypeIdx == Texture::e_TypeIdxDiffuseMap )
        {
            retVal = true;
        }
    }

    return retVal;
}

//-----------------------------------------------------------------------

void MaterialManager::_AddNullMapsInPlaceOfMissingOnes(const std::vector<MaterialMapEntry>& materialMapEntries, Material& mat)
{
    bool normalMapMissing = true;
    bool specularMapMissing = true;

    for(const MaterialMapEntry& matMapEntry : materialMapEntries)
    {
        if( matMapEntry.textureTypeIdx == Texture::e_TypeIdxNormalMap )
        {
            normalMapMissing = false;
        }
        else if( matMapEntry.textureTypeIdx == Texture::e_TypeIdxSpecularMap )
        {
            specularMapMissing = false;
        }
    }

    if( normalMapMissing )
    {
        TextureManager::TextureHandle texHandle = m_TextureManager.LoadInternal2DTexture( TextureManager::e_InternalTextureTypeNullNormapMap,
                                                                                          Texture::e_TypeIdxNormalMap,
                                                                                          Texture::e_StateParamArgSCoordWrapRepeat,
                                                                                          Texture::e_StateParamArgTCoordWrapRepeat,
                                                                                          true,
                                                                                          false );
        mat.m_MaterialDescriptor.AddTexture( texHandle );
    }

    if( specularMapMissing )
    {
        TextureManager::TextureHandle texHandle = m_TextureManager.LoadInternal2DTexture( TextureManager::e_InternalTextureTypeNullSpecularMap,
                                                                                          Texture::e_TypeIdxSpecularMap,
                                                                                          Texture::e_StateParamArgSCoordWrapRepeat,
                                                                                          Texture::e_StateParamArgTCoordWrapRepeat,
                                                                                          true,
                                                                                          false );
        mat.m_MaterialDescriptor.AddTexture( texHandle );
    }
}

//-----------------------------------------------------------------------

std::string MaterialManager::_ConstructUniqueInternalMaterialName(Material& material) const
{
    MD5 md5HashGenerator;

    BitMask32::WordType materialPropertiesPresent = material.m_MaterialDescriptor.m_MaterialPropertiesPresent.Value();
    BitMask32::WordType textureMapTypesPresent = material.m_MaterialDescriptor.m_TextureMapTypesPresent.Value();
    const glm::vec3& diffuseColor = material.m_MaterialDescriptor.m_DiffuseColor.SRGBValue();
    const glm::vec3& specularColor = material.m_MaterialDescriptor.m_SpecularColor.SRGBValue();

    md5HashGenerator.update( glm::value_ptr( diffuseColor ), sizeof( diffuseColor ) );
    md5HashGenerator.update( glm::value_ptr( specularColor ), sizeof( specularColor ) );
    md5HashGenerator.update( &material.m_MaterialDescriptor.m_Glossiness, sizeof( material.m_MaterialDescriptor.m_Glossiness ) );
    md5HashGenerator.update( &materialPropertiesPresent, sizeof( materialPropertiesPresent ) );
    md5HashGenerator.update( &textureMapTypesPresent, sizeof( textureMapTypesPresent ) );

    MaterialDescriptor::TextureList& textures = material.m_MaterialDescriptor.m_TextureList;
    for(MaterialDescriptor::TextureList::iterator it = textures.begin(); it != textures.end(); ++it)
    {
        const std::string& textureFilePath = m_TextureManager.GetTextureFilePath( *it );
        md5HashGenerator.update( textureFilePath.c_str(), static_cast<MD5::size_type>( textureFilePath.length() ) );
    }

    md5HashGenerator.finalize();

    return md5HashGenerator.digestInHexStringForm();
}

//-----------------------------------------------------------------------
