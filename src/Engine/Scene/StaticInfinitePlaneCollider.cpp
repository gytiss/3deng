#include "StaticInfinitePlaneCollider.h"

//-----------------------------------------------------------------------

StaticInfinitePlaneCollider::StaticInfinitePlaneCollider(GameObject& owningGameObject, PhysicsManager& physicsManager)
: Collider( owningGameObject, e_ColliderTypeStaticInfinitePlane, "Static Infinite Plane Collider", physicsManager )
{
    PhysicsManager::CollisionShapeHandle shape = physicsManager.CreateStaticInfinitePlaneCollisionShape();
    _Initialize( shape );
}

//-----------------------------------------------------------------------
