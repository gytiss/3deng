#include "MainAppLogic.h"

#include "MsgLogger.h"
#include "StringUtils.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <MathUtils.h>
#include <EngineDefaults.h>
#include <Exceptions/GenericException.h>
#include <Renderer/PointLight.h>
#include <Renderer/SpotLight.h>
#include <Renderer/DirectionalLight.h>
#include <Renderer/LightManager.h>
#include <GameLogic/AK47.h>

template <typename T>
std::string to_string(T value)
#ifdef OS_ANDROID
{
    std::ostringstream os ;
    os << value ;
    return os.str() ;
}
#else
{
    return std::to_string( value );
}
#endif

//-----------------------------------------------------------------------

static const float c_CameraSpeedInMetersPerSecond = ( Units::OneMeter * 1.0f );

MainAppLogic::MainAppLogic(GraphicsDevice& graphicsDevice,
                           FileManager& fileManager,
                           AnimationManager& animationManager,
                           MeshManager& meshManager,
                           TextureManager& textureManager,
                           MaterialManager& materialManager,
                           RenderManager& renderManager,
                           PhysicsManager& physicsManager,
                           AudioDataManager& audioDataManager,
                           SoundManager& soundManager,
                           SystemInterface& systemInterface)
: m_SystemInterface( systemInterface ),
  m_DrawableSurfaceSize( m_SystemInterface.GetDrawableSurfaceSize() ),
  m_CurrentLightNum( 0 ),
  m_PointLights(),
  m_SpotLights(),
  m_DirectionalLight( nullptr ),
  m_CurrentEntityGroup( e_EntityGroupPointLights ),
  m_EntityManipType( e_EntityManipTypeMove ),
  m_Camera( new Camera( m_DrawableSurfaceSize, c_CameraSpeedInMetersPerSecond ) ),
  m_OtherEntitiesKeyVector(),
  m_FingerReservedByJoystick{ false },
  m_FingerReservedByFireButton{ false },
  m_RelativeMouseCoords( 0.0f, 0.0f ),
  m_AbsoluteMouseCoords( 0.0f, 0.0f ),
  m_MouseMoved( false ),
  m_RenderDebugData( false ),
  m_KeysAreControlingEntities( true ),
#ifdef OS_ANDROID
  m_FreeLookEnabled( true ),
#else
  m_FreeLookEnabled( false ),
#endif
  m_AppIsStillRunning( true ),
  m_ClickedWithMouse( false ),
  m_LastPicked( "" ),
  m_CurrentFrameTimeInMs( 0.0f ),
  m_CurrentPointLight( nullptr ),
  m_CurrentSpotLight( nullptr ),
  m_FileManager( fileManager ),
  m_MeshManager( meshManager ),
  m_TextureManager( textureManager ),
  m_MaterialManager( materialManager ),
  m_AnimationManager( animationManager ),
  m_RenderManager( renderManager ),
  m_PhysicsManager( physicsManager ),
  m_AudioDataManager( audioDataManager ),
  m_SoundManager( soundManager ),
  m_TestCharacterController( m_PhysicsManager, 1.7f, 1.25f, 0.5f, 0.4f ),
  m_VirtualJoystick( glm::vec2( 0.125f, 0.80f ),
                     0.125f,
                     3.2f,
                     m_MeshManager,
                     m_MaterialManager,
                     m_TextureManager,
                     m_SystemInterface ),
  m_FireButton( glm::vec2( 0.875f, 0.75f ),
                0.0625f,
                m_MeshManager,
                m_MaterialManager,
                m_TextureManager,
                m_SystemInterface ),
  m_DrawableText( graphicsDevice, m_SystemInterface, m_FileManager, "Data/Fonts/Test.ttf", 16 ),
  m_Scene( *m_Camera,
            m_SystemInterface,
            m_PhysicsManager,
            m_MeshManager,
            m_AnimationManager,
            m_MaterialManager,
            m_AudioDataManager,
            m_SoundManager ),
  m_Weapon( nullptr )
{
    m_Scene.Initialize();
    m_Weapon = static_cast<AK47*>( m_Scene.FindGameObjectByName( "AK47" ) );

    m_SystemInterface.RegisterDrawableSurfaceSizeChangedCallback( _DrawableSurfaceSizeChaged, this );

    m_TestCharacterController.SetPos( glm::vec3( 0.0f, 0.5f/*3.73f*/, 0.0f ) );
    m_PhysicsManager.AddAction( &m_TestCharacterController );

    memset(m_OtherEntitiesKeyVector, 0x00, sizeof(bool) * e_KeyPressedLastEnum);

    memset( m_PointLights, 0x00, ArraySize( m_PointLights ) );
    memset( m_SpotLights, 0x00, ArraySize( m_SpotLights ) );

    m_PointLights[0] = m_RenderManager.GetLightManager().CreateNewPointLight();
    m_PointLights[1] = m_RenderManager.GetLightManager().CreateNewPointLight();
    m_PointLights[2] = m_RenderManager.GetLightManager().CreateNewPointLight();
    m_PointLights[3] = m_RenderManager.GetLightManager().CreateNewPointLight();
    m_PointLights[4] = m_RenderManager.GetLightManager().CreateNewPointLight();

    m_PointLights[0]->SetEnabled(false);
    m_PointLights[1]->SetEnabled(false);
    m_PointLights[2]->SetEnabled(false);
    m_PointLights[3]->SetEnabled(false);
    m_PointLights[4]->SetEnabled(false);

    m_SpotLights[0] = m_RenderManager.GetLightManager().CreateNewSpotLight();
    m_SpotLights[1] = m_RenderManager.GetLightManager().CreateNewSpotLight();
    m_SpotLights[2] = m_RenderManager.GetLightManager().CreateNewSpotLight();
    m_SpotLights[3] = m_RenderManager.GetLightManager().CreateNewSpotLight();
    m_SpotLights[4] = m_RenderManager.GetLightManager().CreateNewSpotLight();

    m_SpotLights[0]->SetEnabled(false);
    m_SpotLights[1]->SetEnabled(false);
    m_SpotLights[2]->SetEnabled(false);
    m_SpotLights[3]->SetEnabled(false);
    m_SpotLights[4]->SetEnabled(false);

    m_DirectionalLight = m_RenderManager.GetLightManager().GetMainDirectionalLight();

    m_CurrentPointLight = m_PointLights[m_CurrentLightNum];
    m_CurrentSpotLight = m_SpotLights[m_CurrentLightNum];

    _InitLights();

    m_Camera->ForcePosition( 0.0f, 0.0f, 0.0f );
    m_Camera->ForceOrientation( 90.0f, -40.0f );
}

//-----------------------------------------------------------------------

MainAppLogic::~MainAppLogic()
{
    m_SystemInterface.DeregisterDrawableSurfaceSizeChangedCallback( _DrawableSurfaceSizeChaged, this );
    SafeDelete( m_Camera );
}

//-----------------------------------------------------------------------

void MainAppLogic::UpdateScene()
{
    m_AnimationManager.UpdateCurrentlyActiveAnimations();

    m_PhysicsManager.RefreshDebugDataMesh();

    glm::vec3 posOfCharactersHead = m_TestCharacterController.GetPos();
    posOfCharactersHead.y = m_TestCharacterController.GetTop();
    posOfCharactersHead.y = posOfCharactersHead.y - 0.2f;
    m_Camera->ForcePosition( posOfCharactersHead );

#ifdef OS_ANDROID
    m_Shooting = m_FireButton.GetPressed();
#endif

    if( m_Shooting && ( m_Weapon->GetState() != AK47::e_StateReloading ) )
    {
        m_Weapon->SetState( AK47::e_StateShooting );
    }
    else if( m_Weapon->GetState() == AK47::e_StateShooting )
    {
        m_Weapon->SetState( AK47::e_StateStatic );
    }

    m_Scene.Update();
}

//-----------------------------------------------------------------------

void MainAppLogic::UpdateSceneAtFixedStep()
{
    m_Scene.FixedUpdate();
}

//-----------------------------------------------------------------------

void MainAppLogic::LastUpdateScene()
{
    m_Scene.LastUpdate();
}

//-----------------------------------------------------------------------

void MainAppLogic::Render(float frameTimeInSeconds)
{
    EngineTypes::VPTransformations vpTrans;
    vpTrans.viewMatrix = m_Camera->GetViewMatrix();
    vpTrans.projectionMatrix = m_Camera->GetProjectionMatrix();

    if( m_ClickedWithMouse )
    {
        m_ClickedWithMouse = false;
        glm::vec3 rayOrigin = m_Camera->GetPosition();
        glm::vec3 rayDirection = glm::vec3( 0.0f );

        MathUtils::CoordinateInScreenSpaceToRay( m_AbsoluteMouseCoords.x,
                                                 m_AbsoluteMouseCoords.y,
                                                 m_DrawableSurfaceSize.widthInPixels,
                                                 m_DrawableSurfaceSize.heightInPixels,
                                                 vpTrans,
                                                 m_Camera->GetPosition(),
                                                 rayDirection );

        GameObject* gameObjectHit = nullptr;
        m_PhysicsManager.CastARay( rayOrigin, rayDirection, gameObjectHit );

        if( gameObjectHit )
        {
            m_LastPicked = gameObjectHit->GetName();
        }
    }

    _BuildRenderList();

    /*-----------------------------------Draw Scene --------------------------------*/
    const float lSpeed = ( Units::OneMeter * frameTimeInSeconds );

    if( m_EntityManipType == e_EntityManipTypeMove )
    {
        glm::vec3 currLightPos;
        if( m_CurrentEntityGroup == e_EntityGroupPointLights )
        {
            currLightPos = m_CurrentPointLight->GetPositionInWorldSpace();
        }
        else if( m_CurrentEntityGroup == e_EntityGroupSpotLights )
        {
            currLightPos = m_CurrentSpotLight->GetPositionInWorldSpace();
        }
        currLightPos.z += ( m_OtherEntitiesKeyVector[MainAppLogic::e_KeyPressedW] ? -lSpeed : 0.0f );
        currLightPos.z += ( m_OtherEntitiesKeyVector[MainAppLogic::e_KeyPressedS] ? lSpeed : 0.0f );
        currLightPos.x += ( m_OtherEntitiesKeyVector[MainAppLogic::e_KeyPressedA] ? -lSpeed : 0.0f );
        currLightPos.x += ( m_OtherEntitiesKeyVector[MainAppLogic::e_KeyPressedD] ? lSpeed : 0.0f );
        currLightPos.y += ( m_OtherEntitiesKeyVector[MainAppLogic::e_KeyPressedPageUp] ? lSpeed : 0.0f );
        currLightPos.y += ( m_OtherEntitiesKeyVector[MainAppLogic::e_KeyPressedPageDown] ? -lSpeed : 0.0f );
        if( m_CurrentEntityGroup == e_EntityGroupPointLights )
        {
            m_CurrentPointLight->SetPositionInWorldSpace( currLightPos );
        }
        else if( m_CurrentEntityGroup == e_EntityGroupSpotLights )
        {
            m_CurrentSpotLight->SetPositionInWorldSpace( currLightPos );
        }
    }
    if( m_EntityManipType == e_EntityManipTypeRotate )
    {
        glm::vec3 currLightDirection;
        if( m_CurrentEntityGroup == e_EntityGroupSpotLights )
        {
            currLightDirection = m_CurrentSpotLight->GetDirectionInWorldSpace();
        }
        currLightDirection.z += ( m_OtherEntitiesKeyVector[MainAppLogic::e_KeyPressedW] ? -lSpeed : 0.0f );
        currLightDirection.z += ( m_OtherEntitiesKeyVector[MainAppLogic::e_KeyPressedS] ? lSpeed : 0.0f );
        currLightDirection.x += ( m_OtherEntitiesKeyVector[MainAppLogic::e_KeyPressedA] ? -lSpeed : 0.0f );
        currLightDirection.x += ( m_OtherEntitiesKeyVector[MainAppLogic::e_KeyPressedD] ? lSpeed : 0.0f );
        currLightDirection.y += ( m_OtherEntitiesKeyVector[MainAppLogic::e_KeyPressedPageUp] ? lSpeed : 0.0f );
        currLightDirection.y += ( m_OtherEntitiesKeyVector[MainAppLogic::e_KeyPressedPageDown] ? -lSpeed : 0.0f );
        if( m_CurrentEntityGroup == e_EntityGroupSpotLights )
        {
            m_CurrentSpotLight->SetDirectionInWorldSpace( currLightDirection );
        }
    }

    m_DirectionalLight->SetAmbientIntensity( 0.0f );
    m_DirectionalLight->SetDiffuseIntensity( 0.3f );
    m_DirectionalLight->SetDirectionInWorldSpace( glm::vec3( 0.0f, -1.0f, 0.0f ) );
    m_DirectionalLight->SetEnabled( false );

    m_RenderManager.SetViewTransform( vpTrans.viewMatrix );
    m_RenderManager.SetProjectionTransform( RenderManager::e_ProjectionTypePerspective, vpTrans.projectionMatrix );
    m_RenderManager.SetFrustumNearAndFarClippingPlaneDistances( EngineDefaults::c_NearClippingPlane,
                                                                EngineDefaults::c_FarClippingPlane );
    m_RenderManager.Render();
    /*------------------------------------------------------------------------------------*/

#ifndef OS_ANDROID
    _RenderText();
#endif
}

//-----------------------------------------------------------------------

void MainAppLogic::_InitLights()
{
    for(size_t i = 0; i < ArraySize( m_PointLights ); i++)
    {
        m_PointLights[i]->SetAmbientIntensity( 0.1f );
        m_PointLights[i]->SetDiffuseIntensity( 1.0f );
        m_PointLights[i]->SetRadius( 9.0f );
        m_PointLights[i]->SetEnabled( false );
    }

    const glm::vec3 c_RedColor = glm::vec3( 1.0f, 0.0f, 0.0f );
    const glm::vec3 c_GreenColor = glm::vec3( 0.0f, 1.0f, 0.0 );
    const glm::vec3 c_BlueColor = glm::vec3( 0.0f, 0.0f, 1.0f );
    const glm::vec3 c_GoldColor = glm::vec3( 1.0f, 0.843137254902f, 0.0f );
    const glm::vec3 c_WhiteColor = glm::vec3( 1.0f, 1.0f, 1.0f );

    //Y = 2.31428f (Lights center is on the plane)

    m_PointLights[0]->SetColor( c_WhiteColor );
    //m_PointLights[0]->SetPositionInWorldSpace( glm::vec3( -2.05717f, 0.885708f, 0.0570843f ) );
    m_PointLights[0]->SetPositionInWorldSpace( glm::vec3( -1.209f, 0.885708f, 0.354 ) );
    m_PointLights[0]->SetEnabled( true );

    m_PointLights[1]->SetColor( c_GreenColor );
    m_PointLights[1]->SetPositionInWorldSpace( glm::vec3( 4.65718f, 3.57143f , 20.6288f ) );

    m_PointLights[2]->SetColor( c_BlueColor );
    m_PointLights[2]->SetPositionInWorldSpace( glm::vec3( 8.0f, 3.57143f , 8.0f ) );

    m_PointLights[3]->SetColor( c_GoldColor );
    m_PointLights[3]->SetPositionInWorldSpace( glm::vec3( 9.17146f, 3.57143f , 16.1859f ) );

    m_PointLights[4]->SetColor( c_WhiteColor );
    m_PointLights[4]->SetPositionInWorldSpace( glm::vec3( 14.6573f, 3.57143f , 22.0574f ) );

    const glm::vec3 c_DownwardsDirection = glm::vec3( 0.0f, -1.0f, 0.0f );

    for(size_t i = 0; i < 1/*ArraySize( m_SpotLights )*/; i++)
    {
        m_SpotLights[i]->SetAmbientIntensity( 0.1f );
        m_SpotLights[i]->SetDiffuseIntensity( 1.0f );
        m_SpotLights[i]->SetRadius( 3.0f );
        m_SpotLights[i]->SetCutoffAngleInDegrees( 20.0f );
        m_SpotLights[i]->SetDirectionInWorldSpace( c_DownwardsDirection );
        m_SpotLights[i]->SetEnabled( false );
    }

    m_SpotLights[0]->SetColor( c_RedColor );
    m_SpotLights[0]->SetPositionInWorldSpace( glm::vec3( 1.199f, 1.985f, -4.028f ) );
}

//-----------------------------------------------------------------------

void MainAppLogic::_InputCallback(InputMapping::MappedInput& inputs,
                                  float frameTimeInSeconds)
{
#ifdef OS_ANDROID
    _TouchScreenInput( inputs, frameTimeInSeconds );
#else
    _PCInput( inputs, frameTimeInSeconds );
#endif

    _HandleInput( frameTimeInSeconds );
}

//-----------------------------------------------------------------------

void MainAppLogic::_PCInput(InputMapping::MappedInput& inputs, float UNUSED_PARAM(frameTimeInSeconds))
{
    static bool isFlying = false;

    if( inputs.FindAndConsumeAction( InputMapping::e_ActionToggleWireframeMode )  )
    {
        m_RenderManager.SetRenderInWireframeMode( !m_RenderManager.GetRenderInWireframeMode() );
    }

    if( inputs.FindAndConsumeAction( InputMapping::e_ActionToggleDebugDataRendering ) )
    {
        m_RenderDebugData = !m_RenderDebugData;
    }

    /*--------------- Kinematic Character Control -----------*/
    if( inputs.FindAndConsumeAction( InputMapping::e_ActionToggleFlying )  )
    {
        if( !isFlying )
        {
            m_TestCharacterController.StartFly();
        }
        else
        {
            m_TestCharacterController.StopFly();
        }

        isFlying = !isFlying;
    }

    if( inputs.FindAndConsumeAction( InputMapping::e_ActionToggleCrouch )  )
    {
        if( !m_TestCharacterController.IsCrouching() )
        {
            m_TestCharacterController.StartCrouch();
        }
        else
        {
            m_TestCharacterController.StopCrouch();
        }
    }

    if( inputs.FindAndConsumeAction( InputMapping::e_ActionDoJump )  )
    {
        m_TestCharacterController.StartJump( 1.3f );
    }
    /*-------------------------------------------------------*/

    /*------------------------------- Mouse Input ----------------------------*/
    m_MouseMoved = false;

    if( inputs.FindAndConsumeAction( InputMapping::e_ActionClickWithMouse ) )
    {
        m_ClickedWithMouse = true;
    }

    if( inputs.FindAndConsumeState( InputMapping::e_StateLeftMouseButtonIsDown ) )
    {
        if( !IsFreeLookEnabled() )
        {
            SetFreeLookEnabled( true );
        }
        m_Shooting = true;
    }
    else
    {
        m_Shooting = false;
    }

    if( inputs.FindAndConsumeState( InputMapping::e_StateRightMouseButtonIsDown ) )
    {
        if( IsFreeLookEnabled() )
        {
            SetFreeLookEnabled( false );
        }
    }


    if( inputs.ranges.find( InputMapping::e_RangePointerX ) != inputs.ranges.end() )
    {
        m_RelativeMouseCoords.x = static_cast<float>( inputs.ranges[InputMapping::e_RangePointerX] );
        m_MouseMoved = true;
    }

    if( inputs.ranges.find( InputMapping::e_RangePointerY ) != inputs.ranges.end() )
    {
        m_RelativeMouseCoords.y = static_cast<float>( inputs.ranges[InputMapping::e_RangePointerY] );
        m_MouseMoved = true;
    }

    if( inputs.ranges.find( InputMapping::e_RangePointerAbsoluteX ) != inputs.ranges.end() )
    {
        m_AbsoluteMouseCoords.x = static_cast<float>( inputs.ranges[InputMapping::e_RangePointerAbsoluteX] );
    }

    if( inputs.ranges.find( InputMapping::e_RangePointerAbsoluteY ) != inputs.ranges.end() )
    {
        m_AbsoluteMouseCoords.y = static_cast<float>( inputs.ranges[InputMapping::e_RangePointerAbsoluteY] );
    }
    /*------------------------------------------------------------------------*/

    if( !m_KeysAreControlingEntities )
    {
        m_OtherEntitiesKeyVector[MainAppLogic::e_KeyPressedW] = inputs.FindAndConsumeState( InputMapping::e_StateMoveForward );
        m_OtherEntitiesKeyVector[MainAppLogic::e_KeyPressedS] = inputs.FindAndConsumeState( InputMapping::e_StateMoveBackwards );
        m_OtherEntitiesKeyVector[MainAppLogic::e_KeyPressedA] = inputs.FindAndConsumeState( InputMapping::e_StateMoveLeft );
        m_OtherEntitiesKeyVector[MainAppLogic::e_KeyPressedD] = inputs.FindAndConsumeState( InputMapping::e_StateMoveRight );
        m_OtherEntitiesKeyVector[MainAppLogic::e_KeyPressedPageUp] = inputs.FindAndConsumeState( InputMapping::e_StateMoveUp );
        m_OtherEntitiesKeyVector[MainAppLogic::e_KeyPressedPageDown] = inputs.FindAndConsumeState( InputMapping::e_StateMoveDown );
    }
    else
    {
        glm::vec3 speed = glm::vec3( 0.0f, 0.0f, 0.0f );

        float v = 1.1f;

        if( inputs.FindAndConsumeState( InputMapping::e_StateMoveFast ) )
        {
            v = 3.3f;
        }

        if( inputs.FindAndConsumeState( InputMapping::e_StateMoveForward ) )
        {
            speed.z = -v;
        }

        if( inputs.FindAndConsumeState( InputMapping::e_StateMoveBackwards ) )
        {
            speed.z = v;
        }

        if( inputs.FindAndConsumeState( InputMapping::e_StateMoveLeft ) )
        {
            speed.x = -v;
        }

        if( inputs.FindAndConsumeState( InputMapping::e_StateMoveRight ) )
        {
            speed.x = v;
        }

        const float omega = MathUtils::HeadingFromADirectionVector( m_Camera->GetDirectionVector() );

        m_TestCharacterController.SetAngularMovement( omega );
        m_TestCharacterController.SetLinearMovement( speed );
    }

    if( inputs.FindAndConsumeAction( InputMapping::e_ActionToggleForwardDeferredRendering ) )
    {
        if( m_RenderManager.GetRenderingPath() == RenderManager::e_RenderingPathForward )
        {
            m_RenderManager.SetRenderingPath( RenderManager::e_RenderingPathDeferred );
        }
        else if( m_RenderManager.GetRenderingPath() == RenderManager::e_RenderingPathDeferred )
        {
            m_RenderManager.SetRenderingPath( RenderManager::e_RenderingPathForward );
        }
    }

    if( inputs.FindAndConsumeAction( InputMapping::e_ActionRotateEntity ) )
    {
        m_EntityManipType = e_EntityManipTypeRotate;
    }
    else if( inputs.FindAndConsumeAction( InputMapping::e_ActionMoveEntity ) )
    {
        m_EntityManipType = e_EntityManipTypeMove;
    }

    if( inputs.FindAndConsumeAction( InputMapping::e_ActionOpenMainMenu ) )
    {
        SetAppIsStillRunning( false );
    }

    if( inputs.FindAndConsumeAction( InputMapping::e_ActionChooseEntityToMove ) )
    {
        m_KeysAreControlingEntities = !m_KeysAreControlingEntities;
    }

    if( inputs.FindAndConsumeAction( InputMapping::e_ActionNextEntityGroup ) )
    {
        m_CurrentEntityGroup = static_cast<EntityGroup>(m_CurrentEntityGroup + 1);

        if( m_CurrentEntityGroup == e_EntityGroupCount )
        {
            m_CurrentEntityGroup = e_EntityGroupPointLights;
        }

        m_CurrentLightNum = 0;
    }

    if( inputs.FindAndConsumeAction( InputMapping::e_ActionNextEntity ) )
    {
        if( m_CurrentEntityGroup == e_EntityGroupPointLights )
        {
            if( ( m_CurrentLightNum + 1 ) < ArraySize( m_PointLights ) )
            {
                m_CurrentLightNum++;
                m_CurrentPointLight = m_PointLights[m_CurrentLightNum];
            }
        }
        else if( m_CurrentEntityGroup == e_EntityGroupSpotLights )
        {
            if( ( m_CurrentLightNum + 1 ) < ArraySize( m_SpotLights ) )
            {
                m_CurrentSpotLight = m_SpotLights[m_CurrentLightNum];
            }
        }
    }

    if( inputs.FindAndConsumeAction( InputMapping::e_ActionPreviousEntity ) )
    {
        if( m_CurrentLightNum > 0 )
        {
            m_CurrentLightNum--;
            if( m_CurrentEntityGroup == e_EntityGroupPointLights )
            {
                m_CurrentPointLight = m_PointLights[m_CurrentLightNum];
            }
            else if( m_CurrentEntityGroup == e_EntityGroupSpotLights )
            {
                m_CurrentSpotLight = m_SpotLights[m_CurrentLightNum];
            }
        }
    }

    /*--------------- Change Field Of View Of Currently Active Camera -----------*/
    if( inputs.FindAndConsumeAction( InputMapping::e_ActionIncreaseFieldOfViewOfTheCamera )  )
    {
        m_Camera->SetFieldOfView( m_Camera->GetFieldOfView() + 1.0f );
    }

    if( inputs.FindAndConsumeAction( InputMapping::e_ActionDecreaseFieldOfViewOfTheCamera )  )
    {
        m_Camera->SetFieldOfView( m_Camera->GetFieldOfView() - 1.0f );
    }
    /*--------------------------------------------------------------------------*/
}

//-----------------------------------------------------------------------

void MainAppLogic::_TouchScreenInput(InputMapping::MappedInput& inputs, float UNUSED_PARAM(frameTimeInSeconds))
{
    m_MouseMoved = false;

    const bool fingerOnePressed = inputs.FindAndConsumeAction( InputMapping::e_ActionTapWithFingerOne );
    const bool fingerTwoPressed = inputs.FindAndConsumeAction( InputMapping::e_ActionTapWithFingerTwo );

    if( fingerOnePressed )
    {
        glm::vec2 clickCoordinate( 0.0f );
        if( inputs.ranges.find( InputMapping::e_RangeFingerOneAbsoluteX ) != inputs.ranges.end() )
        {
            clickCoordinate.x = static_cast<float>( inputs.ranges[InputMapping::e_RangeFingerOneAbsoluteX] );
        }

        if( inputs.ranges.find( InputMapping::e_RangeFingerOneAbsoluteY ) != inputs.ranges.end() )
        {
            clickCoordinate.y = static_cast<float>( inputs.ranges[InputMapping::e_RangeFingerOneAbsoluteY] );
        }

        if( m_VirtualJoystick.LocationIsOnTheJoystick( clickCoordinate ) )
        {
            m_FingerReservedByJoystick[e_FingerOne] = true;
        }
        else
        {
            m_ClickedWithMouse = true;
            m_AbsoluteMouseCoords = clickCoordinate;
            m_FingerReservedByJoystick[e_FingerOne] = false;

            if( m_FireButton.LocationIsOnTheButton( m_AbsoluteMouseCoords ) )
            {
                m_FingerReservedByFireButton[e_FingerOne] = true;
                m_FireButton.SetPressed();
            }
        }
    }

    if( fingerTwoPressed )
    {
        glm::vec2 clickCoordinate( 0.0f );
        if( inputs.ranges.find( InputMapping::e_RangeFingerTwoAbsoluteX ) != inputs.ranges.end() )
        {
            clickCoordinate.x = static_cast<float>( inputs.ranges[InputMapping::e_RangeFingerTwoAbsoluteX] );
        }

        if( inputs.ranges.find( InputMapping::e_RangeFingerTwoAbsoluteY ) != inputs.ranges.end() )
        {
            clickCoordinate.y = static_cast<float>( inputs.ranges[InputMapping::e_RangeFingerTwoAbsoluteY] );
        }

        if( m_VirtualJoystick.LocationIsOnTheJoystick( clickCoordinate ) )
        {
            m_FingerReservedByJoystick[e_FingerTwo] = true;
        }
        else
        {
            m_ClickedWithMouse = true;
            m_AbsoluteMouseCoords = clickCoordinate;
            m_FingerReservedByJoystick[e_FingerTwo] = false;

            if( m_FireButton.LocationIsOnTheButton( m_AbsoluteMouseCoords ) )
            {
                m_FingerReservedByFireButton[e_FingerTwo] = true;
                m_FireButton.SetPressed();
                MsgLogger::LogEvent("Fire");
            }
        }
    }

    if( !inputs.FindAndConsumeState( InputMapping::e_StateFingerOneIsDown ) )
    {
        if( m_FingerReservedByJoystick[e_FingerOne] )
        {
            m_FingerReservedByJoystick[e_FingerOne] = false;
        }
        else if( m_FingerReservedByFireButton[e_FingerOne] )
        {
            m_FingerReservedByFireButton[e_FingerOne] = false;
        }
    }

    if( !inputs.FindAndConsumeState( InputMapping::e_StateFingerTwoIsDown ) )
    {
        if( m_FingerReservedByJoystick[e_FingerTwo] )
        {
            m_FingerReservedByJoystick[e_FingerTwo] = false;
        }
        else if( m_FingerReservedByFireButton[e_FingerTwo] )
        {
            m_FingerReservedByFireButton[e_FingerTwo] = false;
        }
    }

    glm::vec2 fingerOneRelativeCoords( 0.0f );
    if( inputs.ranges.find( InputMapping::e_RangeFingerOneX) != inputs.ranges.end() )
    {
        fingerOneRelativeCoords.x = static_cast<float>( inputs.ranges[InputMapping::e_RangeFingerOneX] );

        if( !m_FingerReservedByJoystick[e_FingerOne] )
        {
            m_RelativeMouseCoords.x = fingerOneRelativeCoords.x;
            m_MouseMoved = true;
        }
    }

    if( inputs.ranges.find( InputMapping::e_RangeFingerOneY) != inputs.ranges.end() )
    {
        fingerOneRelativeCoords.y = static_cast<float>( inputs.ranges[InputMapping::e_RangeFingerOneY] );

        if( !m_FingerReservedByJoystick[e_FingerOne] )
        {
            m_RelativeMouseCoords.y = fingerOneRelativeCoords.y;
            m_MouseMoved = true;
        }
    }

    if( m_FingerReservedByJoystick[e_FingerOne] )
    {
        m_VirtualJoystick.Update( fingerOneRelativeCoords );
    }


    glm::vec2 fingerTwoRelativeCoords( 0.0f );
    if( inputs.ranges.find( InputMapping::e_RangeFingerTwoX ) != inputs.ranges.end() )
    {
        fingerTwoRelativeCoords.x = static_cast<float>( inputs.ranges[InputMapping::e_RangeFingerTwoX] );

        if( !m_FingerReservedByJoystick[e_FingerTwo] )
        {
            m_RelativeMouseCoords.x = fingerTwoRelativeCoords.x;
            m_MouseMoved = true;
        }
    }

    if( inputs.ranges.find( InputMapping::e_RangeFingerTwoY ) != inputs.ranges.end() )
    {
        fingerTwoRelativeCoords.y = static_cast<float>( inputs.ranges[InputMapping::e_RangeFingerTwoY] );

        if( !m_FingerReservedByJoystick[e_FingerTwo] )
        {
            m_RelativeMouseCoords.y = fingerTwoRelativeCoords.y;
            m_MouseMoved = true;
        }
    }

    if( m_FingerReservedByJoystick[e_FingerTwo] )
    {
        m_VirtualJoystick.Update( fingerTwoRelativeCoords );
    }

    if( !m_FingerReservedByJoystick[e_FingerOne] &&
        !m_FingerReservedByJoystick[e_FingerTwo] )

    {
        m_VirtualJoystick.Reset();
    }

    if( !m_FingerReservedByFireButton[e_FingerOne] &&
        !m_FingerReservedByFireButton[e_FingerTwo])
    {
        m_FireButton.Reset();
    }

    glm::vec3 speed = glm::vec3( 0.0f, 0.0f, 0.0f );
    speed.z = m_VirtualJoystick.GetReading( VirtualJoystick::e_AxisVertical );
    speed.x = m_VirtualJoystick.GetReading( VirtualJoystick::e_AxisHorizontal );

    const float omega = MathUtils::HeadingFromADirectionVector( m_Camera->GetDirectionVector() );

    m_TestCharacterController.SetAngularMovement( omega );
    m_TestCharacterController.SetLinearMovement( speed );
}

//-----------------------------------------------------------------------

void MainAppLogic::_HandleInput(float frameTimeInSeconds)
{
    if( m_MouseMoved && m_FreeLookEnabled )
    {
#ifdef OS_ANDROID
        const float sensitivity = 120.0f;
#else
        const float sensitivity = 160.0f;
#endif
        m_Camera->RecalculateOrientation( frameTimeInSeconds, m_RelativeMouseCoords.x, m_RelativeMouseCoords.y, sensitivity );
        m_MouseMoved = false;
    }
    else if( m_MouseMoved )
    {
        m_MouseMoved = false;
    }
}

//-----------------------------------------------------------------------

void MainAppLogic::_BuildRenderList()
{
    GameObject::RenderablesList renderables;
    m_Scene.GetSceneRenderables( renderables );

    for(RenderableObject& r : renderables)
    {
        m_RenderManager.AddRenderable( r );
    }

    if( m_RenderDebugData )
    {
        const RenderableObject* physcisVisualDebugData = m_PhysicsManager.GetVisualDebugDataRenderable();
        if( physcisVisualDebugData != nullptr )
        {
            m_RenderManager.AddRenderable( *physcisVisualDebugData );
        }
    }

#ifdef OS_ANDROID
    m_VirtualJoystick.AddRenderables( m_RenderManager );
    m_FireButton.AddRenderables( m_RenderManager );
#endif
}

//-----------------------------------------------------------------------

void MainAppLogic::_RenderText()
{
    const float fpsTxtXCoord = ((static_cast<float>(m_DrawableSurfaceSize.widthInPixels) / 2) * (-1)) + 20;
    const float fpsTxtYCoord = (static_cast<float>(m_DrawableSurfaceSize.heightInPixels) / 2) - 30;
    m_DrawableText.Render("ms/Frame: "+numToString(m_CurrentFrameTimeInMs), fpsTxtXCoord, fpsTxtYCoord);
    m_DrawableText.Render("FPS: "+numToString(static_cast<unsigned int>(1000.0f / m_CurrentFrameTimeInMs)), fpsTxtXCoord, fpsTxtYCoord-20);
    m_DrawableText.Render("<Not Used>", fpsTxtXCoord, fpsTxtYCoord-40);
    m_DrawableText.Render("CamPos X : "+numToString(m_Camera->GetPosition()[0]), fpsTxtXCoord, fpsTxtYCoord-60);
    m_DrawableText.Render("CamPos Y : "+numToString(m_Camera->GetPosition()[1]), fpsTxtXCoord, fpsTxtYCoord-80);
    m_DrawableText.Render("CamPos Z : "+numToString(m_Camera->GetPosition()[2]), fpsTxtXCoord, fpsTxtYCoord-100);
    m_DrawableText.Render("CamDir X : "+numToString(m_Camera->GetDirectionVector()[0]), fpsTxtXCoord, fpsTxtYCoord-120);
    m_DrawableText.Render("CamDir Y : "+numToString(m_Camera->GetDirectionVector()[1]), fpsTxtXCoord, fpsTxtYCoord-140);
    m_DrawableText.Render("CamDir Z : "+numToString(m_Camera->GetDirectionVector()[2]), fpsTxtXCoord, fpsTxtYCoord-160);
    m_DrawableText.Render("CamFOV : "+numToString(m_Camera->GetFieldOfView()), fpsTxtXCoord, fpsTxtYCoord-180);
    m_DrawableText.Render("MouseX(Rel.) :"+numToString(m_RelativeMouseCoords.x), fpsTxtXCoord, fpsTxtYCoord-200);
    m_DrawableText.Render("MouseY(Rel.) :"+numToString(m_RelativeMouseCoords.y), fpsTxtXCoord, fpsTxtYCoord-220);
    m_DrawableText.Render("MouseX(Abs.) :"+numToString(m_AbsoluteMouseCoords.x), fpsTxtXCoord, fpsTxtYCoord-240);
    m_DrawableText.Render("MouseY(Abs.) :"+numToString(m_AbsoluteMouseCoords.y), fpsTxtXCoord, fpsTxtYCoord-260);

    unsigned int nextTextLinePos = 280;

    if( m_CurrentEntityGroup == e_EntityGroupPointLights )
    {
        glm::vec3 lightPos = m_CurrentPointLight->GetPositionInWorldSpace();
        m_DrawableText.Render("Ent Name: Point Light( "+numToString(m_CurrentLightNum)+" )", fpsTxtXCoord, fpsTxtYCoord-280);
        m_DrawableText.Render("Ent X: "+numToString(lightPos.x), fpsTxtXCoord, fpsTxtYCoord-300);
        m_DrawableText.Render("Ent Y: "+numToString(lightPos.y), fpsTxtXCoord, fpsTxtYCoord-320);
        m_DrawableText.Render("Ent Z: "+numToString(lightPos.z), fpsTxtXCoord, fpsTxtYCoord-340);
        nextTextLinePos = 340;
    }
    else if( m_CurrentEntityGroup == e_EntityGroupSpotLights )
    {
        glm::vec3 lightPos = m_CurrentSpotLight->GetPositionInWorldSpace();
        m_DrawableText.Render("Ent Name: Spot Light( "+numToString(m_CurrentLightNum)+" )", fpsTxtXCoord, fpsTxtYCoord-280);
        m_DrawableText.Render("Ent X: "+numToString(lightPos.x), fpsTxtXCoord, fpsTxtYCoord-300);
        m_DrawableText.Render("Ent Y: "+numToString(lightPos.y), fpsTxtXCoord, fpsTxtYCoord-320);
        m_DrawableText.Render("Ent Z: "+numToString(lightPos.z), fpsTxtXCoord, fpsTxtYCoord-340);
        nextTextLinePos = 340;
    }

    if( m_EntityManipType == e_EntityManipTypeMove )
    {
        m_DrawableText.Render("Manip Type: Move", fpsTxtXCoord, fpsTxtYCoord-360);
        nextTextLinePos = 380;
    }
    else if( m_EntityManipType == e_EntityManipTypeRotate )
    {
        m_DrawableText.Render("Manip Type: Rotate", fpsTxtXCoord, fpsTxtYCoord-360);
        nextTextLinePos = 380;
    }

    if( m_RenderManager.GetRenderingPath() == RenderManager::e_RenderingPathForward )
    {
        m_DrawableText.Render("Renderer: Forward", fpsTxtXCoord, fpsTxtYCoord-380);
        nextTextLinePos = 400;
    }
    else if( m_RenderManager.GetRenderingPath() == RenderManager::e_RenderingPathDeferred )
    {
        m_DrawableText.Render("Renderer: Deferred", fpsTxtXCoord, fpsTxtYCoord-380);
        nextTextLinePos = 400;
    }

    const float heading = MathUtils::HeadingFromADirectionVector( m_Camera->GetDirectionVector() );
    m_DrawableText.Render("Heading: "+numToString(heading), fpsTxtXCoord, (fpsTxtYCoord - nextTextLinePos));

    m_DrawableText.Render("Last Picked: " + m_LastPicked, fpsTxtXCoord, (fpsTxtYCoord - nextTextLinePos) - 20);
}

//-----------------------------------------------------------------------

void MainAppLogic::_DrawableSurfaceSizeChaged(const SystemInterface::DrawableSurfaceSize& newSize, void* userData)
{
    static_cast<MainAppLogic*>( userData )->m_DrawableSurfaceSize = newSize;
    static_cast<MainAppLogic*>( userData )->m_Camera->SetScreenDimensions( newSize );
}

//-----------------------------------------------------------------------
