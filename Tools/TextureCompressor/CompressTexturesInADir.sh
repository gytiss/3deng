#!/usr/bin/env bash

pushd `dirname $0` > /dev/null # Save script call dir and "cd" into the script dir
SCRIPT_DIR=`pwd`
popd > /dev/null # "cd" back into the script call dir

function ctrl_c()
{
    echo "Terminated Manually with CTRL-C"
    rm -rf alpha.pgm # When the script is terminated manually "alpha.pgm" temporary file might be left not deleted
    
    if ! [ "x$TEMP_DIR" = 'x' ]; then # If temp directory was created then delete it
        DeleteTempDir "$TEMP_DIR"
    fi
}

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

DOWNSCALE_BY=0
MIN_WIDTH=1
MIN_HEIGHT=1
FORCE_RECOMPRESSION=0

######################################
# Parse Arguments
######################################
PrintHelp ()
{
    echo "-d=, ----downscaletimes=<the number of time to reduse the size of each compressed image>"
    echo "-m, --minimumdimensions=<minumum dimenions beyond which no size reduction is performed> (Default: 1x1)"
    echo "-f, --force Recompress all of the textures even the ones which are up to date"
    echo "-h, --help          Show this help text"
    echo ""
    echo "Example usage: $0 -d=2 -m=512x512"
}

IsValidImageDimension ()
{
    local _RETURN_CODE=1 # Failure
    local DIMENSIONS="$1"
    local DIMENSION_VALUES
    
    IFS='x' read -ra DIMENSION_VALUES <<< "$DIMENSIONS"
    
    local ARRAY_LENGTH=${#DIMENSION_VALUES[@]}
    if ((ARRAY_LENGTH > 0)) && ((ARRAY_LENGTH < 3));
    then
        if $(IsPositiveInteger ${DIMENSION_VALUES[0]}) && $(IsPositiveInteger ${DIMENSION_VALUES[1]});
        then
            _RETURN_CODE=0
        fi
    fi

    return $_RETURN_CODE
}

GetWidthFromDimension ()
{
    local DIMENSIONS="$1"
    local DIMENSION_VALUES
    
    IFS='x' read -ra DIMENSION_VALUES <<< "$DIMENSIONS"
    echo "${DIMENSION_VALUES[0]}"
}

GetHighFromDimension ()
{
    local DIMENSIONS="$1"
    local DIMENSION_VALUES
    
    IFS='x' read -ra DIMENSION_VALUES <<< "$DIMENSIONS"
    echo "${DIMENSION_VALUES[1]}"
}

IsPositiveInteger ()
{
    local _RETURN_CODE=1 # Failure
    local VALUE="$1"

    if [[ $VALUE =~ ^[\-0-9]+$ ]] && (( VALUE > -1)); then
        _RETURN_CODE=0 # Success
    fi
    
    return $_RETURN_CODE
}

for i in "$@"
do
case $i in
    -d=*|--downscaletimes=*)
        if [ "x${i#*=}" = 'x' ] && $(IsPositiveInteger "${i#*=}");
        then
            echo "Invalid arguments"
            echo
            PrintHelp
            exit 1
        else
            DOWNSCALE_BY=${i#*=}
        fi
        shift # Past argument=value
    ;;
    -m=*|--minimumdimensions=*)
        if [ "x${i#*=}" = 'x' ] && $(IsValidImageDimension "${i#*=}");
        then
            echo "Invalid arguments"
            echo
            PrintHelp
            exit 1
        else
            MIN_WIDTH=$(GetWidthFromDimension "${i#*=}")
            MIN_HEIGHT=$(GetHighFromDimension "${i#*=}")
            
            if ((MIN_WIDTH < 1));
            then
                echo "Minimum width must be greater than 0"
                exit 1
            fi
            
            if ((MIN_HEIGHT < 1));
            then
                echo "Minimum height must be greater than 0"
                exit 1
            fi
        fi
        shift
    ;;
    -f*|--force)
        FORCE_RECOMPRESSION=1
    ;;
    -h*|--help)
        PrintHelp
        exit 0
    ;;
    *)
        # Unknown argument
        echo "Unrecognised argument \"${i}\""
        echo
        PrintHelp
        exit 1
    ;;
esac
done
######################################

GetImageWidth()
{
    local IMAGE_FILE_PATH="$1"
    identify -format "%[w]" "$IMAGE_FILE_PATH"
}

GetImageHeight()
{
    local IMAGE_FILE_PATH="$1"
    identify -format "%[h]" "$IMAGE_FILE_PATH"
}

GetImageColourComponents()
{
    local _RET_VAL=""
    local IMAGE_FILE_NAME="$1"
    local COLOUR_COMPONENTS=$(identify -format '%[channels]' "${IMAGE_FILE_NAME}")
    local COLOUR_COMPONENTS_IN_LOWER_CASE=${COLOUR_COMPONENTS,,}
    
    if [[ "$COLOUR_COMPONENTS_IN_LOWER_CASE" == *rgb ]]
    then
        _RET_VAL="RGB"
    elif [[ "$COLOUR_COMPONENTS_IN_LOWER_CASE" == *rgba ]]
    then
        _RET_VAL="RGBA8"
    fi
    
    echo $_RET_VAL
}

GetMTCFileNameFromTGAFileName()
{
    local FULL_TGA_FILE_NAME="$1"
    echo "${FULL_TGA_FILE_NAME%.*}.mtc"
}

GetKTXFileNameFromTGAFileName()
{
    local FULL_TGA_FILE_NAME="$1"
    echo "${FULL_TGA_FILE_NAME%.*}.ktx"
}

GetFlippedTGAFileNameFromTGAFileName()
{
    local FULL_TGA_FILE_NAME="$1"
    echo "${FULL_TGA_FILE_NAME%.*}VF.tga"
}

GetReducedTGAFileNameFromTGAFileName()
{
    local FULL_TGA_FILE_NAME="$1"
    echo "${FULL_TGA_FILE_NAME%.*}_Reduced.tga"
}

FlipTGAImageVertically()
{
    local INPUT_TGA_IMAGE="$1"
    local OUTPUT_TGA_IMAGE="$2"
    return `convert "$INPUT_TGA_IMAGE" -flip "$OUTPUT_TGA_IMAGE"`
}

CheckIfFileExists ()
{
    local _RETURN_CODE=1 # Failure

    if [ -f "$1" ];
    then
        _RETURN_CODE=0 # Success
    fi
    
    return $_RETURN_CODE
}

CheckIfConversionIsNeeded()
{
    local _RETURN_CODE=1 # Not needed
    
    local INPUT_TGA_TEXTURE="$1"
    local OUTPUT_MTC_TEXTURE="$2"

    if ! $(CheckIfFileExists $OUTPUT_MTC_TEXTURE);
    then
        # MTC file does not exist, hence new conversion is needed
        _RETURN_CODE=0 # Needed
    else
        if [[ $INPUT_TGA_TEXTURE -nt $OUTPUT_MTC_TEXTURE ]]; 
        then
            # TGA file has been modified after the MTC has been created, hence new conversion is needed
            _RETURN_CODE=0 # Needed
        fi
    fi
        
    return $_RETURN_CODE
}

ReduceImageSizeNTime()
{
    local _RETURN_CODE=1 # Not needed
    
    local REDUCE_N_TIMES="$1"
    local MINIMUM_WIDTH="$2"
    local MINIMUM_HEIGHT="$3"
    local IMAGE_FILE_PATH="$4"
    
    local IMAGE_WIDTH=$(GetImageWidth "$IMAGE_FILE_PATH")
    local IMAGE_HEIGHT=$(GetImageHeight "$IMAGE_FILE_PATH")
        
    if [ "$REDUCE_N_TIMES" -gt 1 ];
    then
        local REDUCED_FILE_NAME=$(GetReducedTGAFileNameFromTGAFileName "$IMAGE_FILE_PATH")
        
        if [ "$IMAGE_WIDTH" -gt "$IMAGE_HEIGHT" ];
        then
            local DESIRED_WIDTH=$(expr $IMAGE_WIDTH / $REDUCE_N_TIMES)

            if [ "$DESIRED_WIDTH" -lt "$MINIMUM_WIDTH" ];
            then
                DESIRED_WIDTH=$MINIMUM_WIDTH
            fi
        
            convert -geometry "${DESIRED_WIDTH}x" "$IMAGE_FILE_PATH" "$REDUCED_FILE_NAME"
            
            # Check the return code of the above command execution
            if [ "$?" -eq 0 ]; 
            then
                _RETURN_CODE=0 # Success
            fi
        else
            local DESIRED_HEIGHT=$(expr $IMAGE_HEIGHT / $REDUCE_N_TIMES)

            if [ "$DESIRED_HEIGHT" -lt "$MINIMUM_HEIGHT" ];
            then
                DESIRED_HEIGHT=$MINIMUM_HEIGHT
            fi
        
            convert -geometry "x${DESIRED_HEIGHT}" "$IMAGE_FILE_PATH" "$REDUCED_FILE_NAME"
            
            # Check the return code of the above command execution
            if [ "$?" -eq 0 ]; 
            then
                _RETURN_CODE=0 # Success
            fi
        fi
        
        rm -rf "$IMAGE_FILE_PATH" # Remove original image
        mv "$REDUCED_FILE_NAME" "$IMAGE_FILE_PATH" # Rename reduced image to the original image name
    else
        _RETURN_CODE=0 # Success
    fi
    
    return $_RETURN_CODE
}

CreateTempDir()
{
    mktemp -d
}

DeleteTempDir()
{
    rm -rf "$1"
}

IFS=$'\n' # Needed for correctly handling paths with spaces in them
TGA_INPUT_TEXTURE_FILE_PATHS=$(find . -name "*.tga" -type f)

TEMP_DIR=$(CreateTempDir)

for INPUT_TGA_IMAGE_FILE_PATH in $TGA_INPUT_TEXTURE_FILE_PATHS; do  
    INPUT_TGA_IMAGE_FILE_NAME=$(basename "$INPUT_TGA_IMAGE_FILE_PATH")
    INPUT_TGA_IMAGE_FILE_DIR=$(dirname "$INPUT_TGA_IMAGE_FILE_PATH")
    
    MTC_FILE_NAME=$(GetMTCFileNameFromTGAFileName "$INPUT_TGA_IMAGE_FILE_NAME")
    KTX_FILE_NAME=$(GetKTXFileNameFromTGAFileName "$INPUT_TGA_IMAGE_FILE_NAME")
    FLIPPED_TGA_FILE_NAME=$(GetFlippedTGAFileNameFromTGAFileName "$INPUT_TGA_IMAGE_FILE_NAME")
    
    if [ "$FORCE_RECOMPRESSION" -eq 1 ] || $(CheckIfConversionIsNeeded "$INPUT_TGA_IMAGE_FILE_PATH" "$INPUT_TGA_IMAGE_FILE_DIR/$MTC_FILE_NAME");
    then
        OUTPUT_IMAGE_COLOUR_COMPONENTS=$(GetImageColourComponents "$INPUT_TGA_IMAGE_FILE_PATH")    
        cp "$INPUT_TGA_IMAGE_FILE_PATH" "$TEMP_DIR/" # Copy the original TGA image to a temporary directory
        
        # OpenGL expects texture origin to be at the bottom left corner, since origin of ETC2 compressed images is at
        # the top left corner we need to flip the source image to make sure ETC2 image is displayed correctly
        if $(FlipTGAImageVertically "$TEMP_DIR/$INPUT_TGA_IMAGE_FILE_NAME" "$TEMP_DIR/$FLIPPED_TGA_FILE_NAME");
        then
            rm -rf "$TEMP_DIR/$INPUT_TGA_IMAGE_FILE_NAME" # Remove original image
            mv "$TEMP_DIR/$FLIPPED_TGA_FILE_NAME" "$TEMP_DIR/$INPUT_TGA_IMAGE_FILE_NAME" # Rename flipped image to the original image name

            if $(ReduceImageSizeNTime "$DOWNSCALE_BY" "$MIN_WIDTH" "$MIN_HEIGHT" "$TEMP_DIR/$INPUT_TGA_IMAGE_FILE_NAME" );
            then            
                # Convert the flipped and reduced image to ETC2 compressed KTX file
                if ${SCRIPT_DIR}/etcpack "$TEMP_DIR/$INPUT_TGA_IMAGE_FILE_NAME" "$TEMP_DIR" -s fast -e perceptual -c etc2 -f "$OUTPUT_IMAGE_COLOUR_COMPONENTS" -mipmaps -ktx >/dev/null;
                then
                    # Convert the resulting KTX file to MTC file
                    if ${SCRIPT_DIR}/KtxToMtc -Type 2D "$TEMP_DIR/$KTX_FILE_NAME" -o "$TEMP_DIR/$MTC_FILE_NAME";
                    then
                        mv "$TEMP_DIR/$MTC_FILE_NAME" "$INPUT_TGA_IMAGE_FILE_DIR/$MTC_FILE_NAME" # Move the MTC file from temp to the destination dir
                        
                        FULL_INPUT_FILE_PATH=$(readlink -e "${INPUT_TGA_IMAGE_FILE_PATH}")
                        FULL_OUTPUT_FILE_PATH=$(readlink -e "${INPUT_TGA_IMAGE_FILE_DIR}/${MTC_FILE_NAME}")
                        
                        echo "Converted \"${FULL_INPUT_FILE_PATH}\" to the ETC2 compressed \"${FULL_OUTPUT_FILE_PATH}\""
                    fi
                    
                    rm -rf "$TEMP_DIR/$KTX_FILE_NAME" # Remove the KTX file
                fi  
            else
                echo "Failed to reduce the size of the image \"${INPUT_TGA_IMAGE_FILE_PATH}"\"
            fi
        else
            echo "Failed to flip the image \"${INPUT_TGA_IMAGE_FILE_PATH}"\"
        fi
        
        rm -rf "$TEMP_DIR/$INPUT_TGA_IMAGE_FILE_NAME" # Remove the flipped and reduced or the original image
    fi
done


DeleteTempDir "$TEMP_DIR"