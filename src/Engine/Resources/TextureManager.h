#ifndef TEXTUREMANAGER_H_INCLUDED
#define TEXTUREMANAGER_H_INCLUDED

#include <map>
#include <list>
#include <functional>
#include <GraphicsDevice/Texture.h>
#include <CommonDefinitions.h>

class GraphicsDevice;
class FileManager;

class TextureManager
{
public:
    TextureManager(GraphicsDevice& graphicsDevice, FileManager& fileManager);
    ~TextureManager();

    class TextureHandle
    {
    public:
        friend class TextureManager;

        TextureHandle()
        : m_Texture( nullptr ),
          m_TexturePathIt()
        {
        }

        TextureHandle& operator= (const TextureHandle& other)
        {
            m_Texture = other.m_Texture;
            m_TexturePathIt = other.m_TexturePathIt;

            return *this;
        }

        TextureHandle(const TextureHandle& other)
        {
            operator= ( other );
        }

        Texture* operator->()
        {
            return m_Texture;
        }

    protected:
        TextureHandle(Texture* texture, std::list<std::string>::iterator texturePathIt)
        : m_Texture( texture ),
          m_TexturePathIt( texturePathIt )
        {
        }

        Texture* m_Texture;
        std::list<std::string>::iterator m_TexturePathIt;  // Iterator to the texture paths list i.e. m_TexturePathsList
    };

    enum TextureFilteringMode
    {
        e_TextureFilteringModeNoFiltering = Texture::e_FilteringModeNoFiltering,
        e_TextureFilteringModeBilinear = Texture::e_FilteringModeBilinear,
        e_TextureFilteringModeTrilinear = Texture::e_FilteringModeTrilinear
    };

    enum InternalTextureType
    {
        e_InternalTextureTypeNullNormapMap = 0,
        e_InternalTextureTypeNullSpecularMap = 1,

        e_InternalTextureTypeCount // Not an internal texture type (Used for counting)
    };

    TextureHandle LoadInternal2DTexture(InternalTextureType type,
                                        Texture::TypeIdx typeIdx,
                                        Texture::StateParamArg sWrapMode = Texture::e_StateParamArgSCoordWrapRepeat,
                                        Texture::StateParamArg tWrapMode = Texture::e_StateParamArgTCoordWrapRepeat,
                                        bool isThisTextureMipMapped = true,
                                        bool isThisTextureSRGBEncoded = true);

    TextureHandle Load2DTexture(const std::string& textureFilePath,
                                Texture::TypeIdx typeIdx,
                                Texture::StateParamArg sWrapMode = Texture::e_StateParamArgSCoordWrapRepeat,
                                Texture::StateParamArg tWrapMode = Texture::e_StateParamArgTCoordWrapRepeat,
                                bool isThisTextureMipMapped = true,
                                bool isThisTextureSRGBEncoded = true);

    TextureHandle Load3DTexture(const std::string& textureFilePath,
                                Texture::TypeIdx typeIdx,
                                Texture::StateParamArg sWrapMode = Texture::e_StateParamArgSCoordWrapRepeat,
                                Texture::StateParamArg tWrapMode = Texture::e_StateParamArgTCoordWrapRepeat,
                                Texture::StateParamArg rWrapMode = Texture::e_StateParamArgRCoordWrapRepeat);

    TextureHandle LoadCubeMapTexture(const std::string& textureFilePath,
                                     Texture::TypeIdx typeIdx,
                                     Texture::StateParamArg sWrapMode = Texture::e_StateParamArgSCoordWrapRepeat,
                                     Texture::StateParamArg tWrapMode = Texture::e_StateParamArgTCoordWrapRepeat,
                                     Texture::StateParamArg rWrapMode = Texture::e_StateParamArgRCoordWrapRepeat);

    void ReleaseTexture(TextureHandle& handle);

    /**
     * Create a second texture handle which is associated with the same texture as the original handle.
     * This means that when "ReleaseTexture()" is called on one of the handles the texture is not released
     * until "ReleaseTexture()" is called on the second handle as well.
     */
    TextureHandle DuplicateTextureHandle(const TextureHandle& handle);

    const std::string& GetTextureFilePath(const TextureHandle& handle) const;

    void SetTextureFilteringMode(Texture::FilteringMode mode);

private:
    static const char* c_InternalTexturePathPrefix;
    static const char* c_InternalTextureNames[e_InternalTextureTypeCount];

    struct RefCountedTexturePtr
    {
        RefCountedTexturePtr()
        : refCount( 0 ),
          texture( nullptr ),
          texturePathIt()
        {
        }

        RefCountedTexturePtr& operator= (const RefCountedTexturePtr& other)
        {
            refCount = other.refCount;
            texture = other.texture;
            texturePathIt = other.texturePathIt;

            return *this;
        }

        RefCountedTexturePtr(const RefCountedTexturePtr& other)
        {
            operator=( other );
        }

        unsigned int refCount;
        Texture* texture;
        std::list<std::string>::iterator texturePathIt; // Iterator to the texture paths list i.e. m_TexturePathsList
    };

    typedef std::list<RefCountedTexturePtr> RefCountedTexturePtrList;
    typedef std::map<std::string, RefCountedTexturePtrList > PathsToTexturesMap;

    // Used to prevent copying
    TextureManager& operator = (const TextureManager& other);
    TextureManager(const TextureManager& other);

    TextureHandle _Load2DTexture(const std::string& textureFilePath,
                                 Texture::TypeIdx typeIdx,
                                 std::function<void(const std::string&, bool, Texture*)> loadTexture,
                                 Texture::StateParamArg sWrapMode = Texture::e_StateParamArgSCoordWrapRepeat,
                                 Texture::StateParamArg tWrapMode = Texture::e_StateParamArgTCoordWrapRepeat,
                                 bool isThisTextureMipMapped = true,
                                 bool isThisTextureSRGBEncoded = true);

    RefCountedTexturePtrList::iterator _FindRefCountedTexturePtr(RefCountedTexturePtrList& ptrList,
                                                                 const TextureHandle& handle);

    GraphicsDevice& m_GraphicsDevice;
    FileManager& m_FileManager;
    PathsToTexturesMap m_PathsToTexturesMap;
    std::list<std::string> m_TexturePathsList;
    Texture::FilteringMode m_CurrentTextureFilteringMode;
    static bool m_ClassAlreadyInstantiated;
};

#endif // TEXTUREMANAGER_H_INCLUDED
