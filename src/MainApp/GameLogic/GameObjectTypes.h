#ifndef GAMEOBJECTTYPES_H_INCLUDED
#define GAMEOBJECTTYPES_H_INCLUDED

#include <cstdint>

namespace GameObjectTypes
{
    static const uint32_t c_Gun = 1;
    static const uint32_t c_ShootingTarget = 2;
    static const uint32_t c_Environment = 3;
}

#endif // GAMEOBJECTTYPES_H_INCLUDED
