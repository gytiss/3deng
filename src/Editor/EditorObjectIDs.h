#ifndef EDITOROBJECTIDS_H_INCLUDED
#define EDITOROBJECTIDS_H_INCLUDED

#include <cstdint>

class EditorObjectID
{
public:
    EditorObjectID() = delete;

    static const uint32_t MoveManipulator = 1;
    static const uint32_t MoveManipulatorXAxis = 2;
    static const uint32_t MoveManipulatorYAxis = 3;
    static const uint32_t MoveManipulatorZAxis = 4;
};

#endif // EDITOROBJECTIDS_H_INCLUDED
