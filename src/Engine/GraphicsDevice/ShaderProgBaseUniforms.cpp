#include "ShaderProgBaseUniforms.h"
#include "ShaderProg.h"
#include <Exceptions/GenericException.h>

//-----------------------------------------------------------------------

ShaderProgUniform::ShaderProgUniform(ShaderProg* shaderProg,
                                     const std::string& name)
: m_LocationIndex( 0 ),
  m_Name( name ),
  m_ShaderProg( shaderProg ),
  m_ValueCachedFlag( false ),
  m_ValueSizeInBytes( 0 )
{
    shaderProg->RegisterUniform( this );
}

//-----------------------------------------------------------------------

void ShaderProgUniform::UpdateLocationIndexCache()
{
    m_LocationIndex = glGetUniformLocation( m_ShaderProg->GetHandle(), GetName().c_str() );

    if( m_LocationIndex == -1 )
    {
        throw GenericException( "Uniform variable \"" +
                                 GetName() +
                                 "\" either is not defined in the shader program(or was optimized out by the compiler, because it is not being used) \"" +
                                 m_ShaderProg->GetName() +
                                 "\" or is the name of a uniform block" );
    }
}

//-----------------------------------------------------------------------

#ifdef DEBUG_BUILD
void ShaderProgUniform::CheckShaderIsInUse() const
{
    if( !m_ShaderProg->IsCurrenlyInUse() )
    {
        std::string msg = "Trying to set uniform \"";
        msg += m_Name;
        msg += "\" of the shader prog. \"";
        msg +=  m_ShaderProg->GetName();
        msg += "\" while this shader prog. not currently in use";
        throw GenericException( msg );
    }
}
#endif

//-----------------------------------------------------------------------