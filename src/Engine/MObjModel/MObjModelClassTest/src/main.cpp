#include <iostream>
#include <boost/filesystem.hpp>
#include "../../MObj.h"

void PrintHelp(std::string appName)
{
    std::cout << "This utility is used for testing loading of MObj models" << std::endl;
    std::cout << "Usage: " << appName << " <Path to input MObj model>" << std::endl;
}

int main(int argc, const char* argv[])
{
    if( argc >= 2 )
    {
        if( boost::filesystem::exists( argv[1] ) )
        {
            MObj mObjModel;
            if( mObjModel.LoadModel( argv[1] ) )
            {
                mObjModel.DebugDumpData();
                std::cout << "Success" << std::endl;
            }
        }
        else
        {
            std::cerr << "File \"" << argv[1] << "\" does not exist" << std::endl;
        }
    }
    else
    {
        PrintHelp( boost::filesystem::path( argv[0] ).leaf().c_str() );
    }

    return 0;
}
