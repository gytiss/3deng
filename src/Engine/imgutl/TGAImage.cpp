#include <cstdio>
#include <cstring>
#include <fstream>
#include "TGAImage.h"
#include "ImageUtils.h"

#define UNCOMPRESSSED_TRUE_COLOR (0x2)
#define RLE_ENCODED_TRUE_COLOR (0x0A)
#define COLOR_MAP_TYPE_COLOR_MAP_NOT_PRESENT (0x00)

//-----------------------------------------------------------------------

TGAImage::TGAImage(FileManager& fileManager)
: m_Data( nullptr ),
  m_TGAHeader(),
  m_ByteStream( fileManager, ByteStream::e_DataAlignment4Bytes )
{
    memset( &m_TGAHeader, 0x00, sizeof( TGAHeader ) );
}

//-----------------------------------------------------------------------

bool TGAImage::Load(std::string name, Origin preferedOrigin)
{
    return ( ( m_Data == nullptr ) &&
              m_ByteStream.InitFromFile( name ) &&
              _Load( preferedOrigin ) );
}

//-----------------------------------------------------------------------

bool TGAImage::Load(const uint8_t* dataBuffer, size_t dataBufferSizeInBytes, Origin preferedOrigin)
{
    return ( ( m_Data == nullptr ) &&
             m_ByteStream.InitFromMemory( dataBuffer, dataBufferSizeInBytes ) &&
             _Load( preferedOrigin ) );
}

//-----------------------------------------------------------------------

bool TGAImage::SaveAsTGA(std::string fileName,
                         TGAImage::Origin origin,
                         size_t width,
                         size_t height,
                         unsigned int bytesPerPixel,
                         uint8_t* pixelData)
{
    bool ret = false;

    std::ofstream tgaFile;

    tgaFile.open (fileName.c_str(), std::ios::out | std::ios::binary);

    if( !tgaFile.fail() )
    {

    //    const uint8_t IMG_ID_LENGTH = 0x0;
    //    const uint8_t IMG_ID[6] = "TEST1";
    //    const uint8_t COLOR_MAP_TYPE = 0x0; //No color map
    //    const uint8_t IMAGE_TYPE = 0x2; //Uncompressed truecolor image
    //
    //    /* Color map spec (Will not be used)*/
    //    const uint16_t FIRST_ENTRY_INDEX = 0x0;
    //    const uint16_t COLOR_MAP_LENGTH = 0x0;
    //    const uint8_t  COLOR_MAP_ENTRY_SIZE = 0x0;
    //    /*----------------------------------*/
    //
    //    /* Image specification */
    //    const uint16_t X_ORIGIN = 0x0;
    //    const uint16_t Y_ORIGIN = 0x0;
    //    const uint16_t IMG_WIDTH = width;
    //    const uint16_t IMG_HEIGHT = height;
    //    const uint8_t  IMG_PIXEL_DEPTH = 32;
    //    const uint8_t  IMG_DESCRIPTOR = 0x8; //32bit Targa image
    //    /*---------------------*/

        /*
        0x0 -  no image data is present
        0x1 - uncompressed color-mapped image
        0x2 - uncompressed true-color image
        0x3 - uncompressed black-and-white (grayscale) image
        0x9 - run-length encoded color-mapped image
        0x10 - run-length encoded true-color image
        0x11 - run-length encoded black-and-white (grayscale) image
        */

        /*---------- Write Header ----------*/
        tgaFile << (uint8_t)0x0;//IMG_ID_LENGTH;
        tgaFile << (uint8_t)0x0;//COLOR_MAP_TYPE;
        tgaFile << (uint8_t)UNCOMPRESSSED_TRUE_COLOR;//IMAGE_TYPE;

        tgaFile << (uint8_t)0x0;//FIRST_ENTRY_INDEX Byte 1
        tgaFile << (uint8_t)0x0;//FIRST_ENTRY_INDEX Byte 2
        tgaFile << (uint8_t)0x0;//COLOR_MAP_LENGTH Byte 1
        tgaFile << (uint8_t)0x0;//COLOR_MAP_LENGTH Byte 2

        tgaFile << (uint8_t)0x0;//COLOR_MAP_ENTRY_SIZE;

        tgaFile << (uint8_t)0x0;//X_ORIGIN Byte 1
        tgaFile << (uint8_t)0x0;//X_ORIGIN Byte 2
        tgaFile << (uint8_t)0x0;//Y_ORIGIN Byte 1
        tgaFile << (uint8_t)0x0;//Y_ORIGIN Byte 2

        tgaFile << (uint8_t)(width&0x00FF);//IMG_WIDTH;
        tgaFile << (uint8_t)((width>>8)&0x00FF);//IMG_WIDTH;
        tgaFile << (uint8_t)(height&0x00FF);//IMG_HEIGHT;
        tgaFile << (uint8_t)((height>>8)&0x00FF);//IMG_HEIGHT;

        tgaFile << (uint8_t)(bytesPerPixel * 8);//IMG_PIXEL_DEPTH (Bits per pixel)

        /*
        |Image Descriptor Byte:                                      |
        |  Bits 3-0 - number of attribute bits associated with each  |
        |             pixel.  For the Targa 16, this would be 0 or   |
        |             1.  For the Targa 24, it should be 0.  For the |
        |             Targa 32, it should be 8.                      |
        |  Bit 4    - 0 - image is stored right to left              |
        |             1 - image is stored left to right              |
        |  Bit 5    - 0 = image is stored bottom-to-top              |
        |             1 = image is stored top-to-bottom              |
        |  Bits 7-6 - Data storage interleaving flag.                |
        |             00 = non-interleaved.                          |
        |             01 = two-way (even/odd) interleaving.          |
        |             10 = four way interleaving.                    |
        |             11 = reserved.                                 |
        */

        uint8_t imgDesc = 0x0;
        if( (bytesPerPixel / 8) == 32 ) //32-bit Image i.e. RGBA
        {
            imgDesc = 0x8;
        }

        if( ( origin == UPPER_RIGHT  ) ||
            ( origin == LOWER_RIGHT ) )
        {
            imgDesc |= (1u << 4);
        }

        if( ( origin == UPPER_LEFT ) ||
            ( origin == UPPER_RIGHT ) )
        {
            imgDesc |= (1u << 5);
        }

        tgaFile << imgDesc;//IMG_DESCRIPTOR;
        /*----------------------------------*/

        const size_t dataSize = (bytesPerPixel * width * height);
        uint8_t* tmpData = new uint8_t[ dataSize ];

        memcpy( tmpData, pixelData, dataSize );

        // TGA stores RGB(A) as BGR(A) so
        // we need to swap red and blue.
        for( size_t i = 0; i < dataSize; i += bytesPerPixel )
        {
            tmpData[i] = pixelData[i+2];   // B
            tmpData[i+1] = pixelData[i+1]; // G
            tmpData[i+2] = pixelData[i];   // R
            if( bytesPerPixel > 3 )        // TGA has Alpha channel
            {
                tmpData [i+3] = pixelData[i+3]; // A
            }
        }

        /*-------- Write Color Data --------*/
        for( size_t i=0 ; i < dataSize ; i++ )
        {
            tgaFile << tmpData[i];
        }
        /*----------------------------------*/

        delete[] tmpData;

    /*

    28  4 bytes         Extension offset        Offset in bytes from the beginning of the file
    29  4 bytes         Developer area offset   Offset in bytes from the beginning of the file
    30  16 bytes        Signature       Contains "TRUEVISION-XFILE"
    31  1 byte          Contains "."
    32  1 byte          Contains NULL

    */

        tgaFile << (uint8_t)0x0;
        tgaFile << (uint8_t)0x0;
        tgaFile << (uint8_t)0x0;
        tgaFile << (uint8_t)0x0;

        tgaFile << (uint8_t)0x0;
        tgaFile << (uint8_t)0x0;
        tgaFile << (uint8_t)0x0;
        tgaFile << (uint8_t)0x0;

        char name[17]="TRUEVISION-XFILE";
        for(size_t i=0 ; i < 16 ; i++)
        {
            tgaFile << name[i];
        }

        tgaFile << '.';
        tgaFile << (uint8_t)0x0;

        tgaFile.close();

        ret = true;
    }

    return ret;
}

//-----------------------------------------------------------------------

bool TGAImage::_Load(Origin preferedOrigin)
{
    bool readingOK = false;

    if( m_Data == nullptr )
    {
        readingOK = true;

        readingOK &= m_ByteStream.ReadUInt8( m_TGAHeader.identSizeInBytes );
        readingOK &= m_ByteStream.ReadUInt8( m_TGAHeader.colourMapType );
        readingOK &= m_ByteStream.ReadUInt8( m_TGAHeader.imageType );

        readingOK &= m_ByteStream.ReadUInt16( m_TGAHeader.colourMapStart );
        readingOK &= m_ByteStream.ReadUInt16( m_TGAHeader.colourMapLength );
        readingOK &= m_ByteStream.ReadUInt8( m_TGAHeader.colourMapBits );

        readingOK &= m_ByteStream.ReadUInt16( m_TGAHeader.xStart );
        readingOK &= m_ByteStream.ReadUInt16( m_TGAHeader.yStart );
        readingOK &= m_ByteStream.ReadUInt16( m_TGAHeader.width );
        readingOK &= m_ByteStream.ReadUInt16( m_TGAHeader.height );
        readingOK &= m_ByteStream.ReadUInt8( m_TGAHeader.bitsPerPixel );
        readingOK &= m_ByteStream.ReadUInt8( m_TGAHeader.descriptor );

        const size_t bytesPerColorMapEntry = ( m_TGAHeader.colourMapBits / 8 );
        const size_t bytesPerPixel = ( m_TGAHeader.bitsPerPixel / 8 );
        const size_t colorMapSizeInBytes = ( m_TGAHeader.colourMapLength * bytesPerColorMapEntry );
        const size_t pixelDataSizeInBytes = ( m_TGAHeader.width * m_TGAHeader.height * bytesPerPixel );

        if( m_TGAHeader.imageType == UNCOMPRESSSED_TRUE_COLOR )
        {
            // Check if data in the header is consistent with the file size
            // Note: This can only be done for uncompressed images, because
            //       in compressed images compressed data section is variable size
            //       and that size can not be determined without reading the compressed data
            readingOK &= ( ( c_TGAHeaderSizeInBytes +
                              m_TGAHeader.identSizeInBytes +
                              colorMapSizeInBytes +
                              pixelDataSizeInBytes ) <= m_ByteStream.InitialStreamSizeInBytes() );
        }

        // Only True Color images are supported
        readingOK &= ( ( m_TGAHeader.imageType == UNCOMPRESSSED_TRUE_COLOR ) || ( m_TGAHeader.imageType == RLE_ENCODED_TRUE_COLOR ) );

        // If there are less than three bytes per pixel then image is not True Color
        readingOK &= ( bytesPerPixel >= 3 );

        if( readingOK )
        {
            // Skip ID field if present
            if( m_TGAHeader.identSizeInBytes > 0 )
            {
                m_ByteStream.SkipBytes( m_TGAHeader.identSizeInBytes );
            }

            // Skip Color Map if present
            if( m_TGAHeader.colourMapType != COLOR_MAP_TYPE_COLOR_MAP_NOT_PRESENT )
            {
                m_ByteStream.SkipBytes( colorMapSizeInBytes );
            }

            if( m_TGAHeader.imageType == UNCOMPRESSSED_TRUE_COLOR )
            {
                // Read pixel data
                m_Data = new uint8_t[pixelDataSizeInBytes];
                readingOK &= m_ByteStream.ReadBytesToBuffer( m_Data, pixelDataSizeInBytes );

                if( readingOK )
                {
                    // bytesPerPixel = 3 means RGB, bytesPerPixel = 4 means RGBA
                    // TGA stores RGB(A) as BGR(A), hence we need to swap red and blue.
                    for (size_t i = 0; i < pixelDataSizeInBytes; i += bytesPerPixel)
                    {
                        // Swap Red and Blue channels
                        uint8_t temp = m_Data[i];
                        m_Data[i] = m_Data[i+2]; //Red
                        m_Data[i+2] = temp;      //Blue
                    }
                }
            }
            else // RLE compressed image
            {
                int n = 0;
                uint8_t p[5];

                m_Data = new uint8_t[pixelDataSizeInBytes];

                while( readingOK && ( n < m_TGAHeader.width * m_TGAHeader.height ) )
                {
                    readingOK &= m_ByteStream.ReadBytesToBuffer( p, ( bytesPerPixel + 1 ) );

                    if( readingOK )
                    {
                        int j = p[0] & 0x7f;

                        m_Data[n * bytesPerPixel] = p[3];     //R
                        m_Data[n * bytesPerPixel + 1] = p[2]; //G
                        m_Data[n * bytesPerPixel + 2] = p[1]; //B

                        // TGA has an Alpha channel
                        if( bytesPerPixel > 3 )
                        {
                            m_Data[n * bytesPerPixel + 3] = p[4]; //A
                        }

                        n++;
                        if( p[0] & 0x80 )
                        {
                            for(int i=0; i < j; i++)
                            {
                                m_Data [n * bytesPerPixel] = p[3];     //R
                                m_Data [n * bytesPerPixel + 1] = p[2]; //G
                                m_Data [n * bytesPerPixel + 2] = p[1]; //B

                                // TGA has an Alpha channel
                                if( bytesPerPixel > 3 )
                                {
                                    m_Data[n * bytesPerPixel + 3] = p[4]; //A
                                }

                                n++;
                            }
                        }
                        else
                        {
                            for(int i = 0; readingOK && ( i < j ); i++)
                            {
                                readingOK &= m_ByteStream.ReadBytesToBuffer( p, bytesPerPixel );

                                if( readingOK )
                                {
                                    m_Data[n * bytesPerPixel] = p[2];     //R
                                    m_Data[n * bytesPerPixel + 1] = p[1]; //G
                                    m_Data[n * bytesPerPixel + 2] = p[0]; //B

                                    // TGA has an Alpha channel
                                    if( bytesPerPixel > 3 )
                                    {
                                        m_Data[n * bytesPerPixel + 3] = p[3]; //A
                                    }

                                    n++;
                                }
                            }
                        }
                    }
                }
            }

            if( readingOK )
            {
                Origin imageOrigin;

                if( m_TGAHeader.descriptor & (1u << 4) )
                {
                    if( m_TGAHeader.descriptor & (1u << 5) )
                    {
                        imageOrigin = UPPER_RIGHT;
                    }
                    else
                    {
                        imageOrigin = LOWER_RIGHT;
                    }
                }
                else
                {
                    if( m_TGAHeader.descriptor & (1u << 5) )
                    {
                        imageOrigin = UPPER_LEFT;
                    }
                    else
                    {
                        imageOrigin = LOWER_LEFT;
                    }
                }

                if( ( ( imageOrigin & UPPER ) && ( preferedOrigin & LOWER ) ) ||
                    ( ( imageOrigin & LOWER ) && ( preferedOrigin & UPPER ) ) )
                {
                    ImageUtils::FlipVertically( m_Data,
                                                m_TGAHeader.width,
                                                m_TGAHeader.height,
                                                static_cast<unsigned int>( bytesPerPixel ) );
                }

                if( ( ( imageOrigin & LEFT ) && ( preferedOrigin & RIGHT ) ) ||
                    ( ( imageOrigin & RIGHT ) && ( preferedOrigin & LEFT ) ) )
                {
                    ImageUtils::FlipHorizontally( m_Data,
                                                  m_TGAHeader.width,
                                                  m_TGAHeader.height,
                                                  static_cast<unsigned int>( bytesPerPixel ) );
                }
            }
            else if( m_Data != nullptr )
            {
                delete[] m_Data;
                m_Data = nullptr;
            }
        }
    }

    return readingOK;
}

//-----------------------------------------------------------------------
