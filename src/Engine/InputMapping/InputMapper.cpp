#include "InputMapper.h"
#include "InputContext.h"
#include <Exceptions/GenericException.h>
#include <CommonDefinitions.h>

#include <lua.hpp>
#include <luabind/luabind.hpp>
#include <luabind/operator.hpp>

#include <LuaUtils.h>

using namespace InputMapping;

//-----------------------------------------------------------------------

InputMapper::InputMapper(FileManager& fileManager,
                         const std::string& contextsDefCfgFilePath,
                         const std::string& contexctsInputMapCfgFilePath,
                         const std::string& inputRangesCfgFilePath)
: m_FileManager( fileManager ),
  m_InputContexts(),
  m_ActiveContexts(),
  m_CallbackTable(),
  m_CurrentMappedInput(),
  m_RawInputButtonStates(),
  m_Conversions( inputRangesCfgFilePath, m_FileManager )
{
    memset( m_RawInputButtonStates, e_RawInputButtonStateReleased,  ArraySize( m_RawInputButtonStates ) );

    bool ok = true;
    std::string errMsg = "";
    ok = ok && _ReadContextsDefinitionsCfgFile( contextsDefCfgFilePath, errMsg );
    ok = ok && _ReadContextsInputMapCfgFile( contexctsInputMapCfgFilePath , errMsg );

    if( !ok )
    {
        throw GenericException( errMsg );
    }
}

//-----------------------------------------------------------------------

InputMapper::~InputMapper()
{
	for(std::map<std::string, InputContext*>::iterator iter = m_InputContexts.begin(); iter != m_InputContexts.end(); ++iter)
    {
        delete iter->second;
    }
}

//-----------------------------------------------------------------------

//
// Clear all mapped input
//
void InputMapper::Clear()
{
	m_CurrentMappedInput.actions.clear();
	m_CurrentMappedInput.ranges.clear();
	m_CurrentMappedInput.textInput.clear();
	// Note: we do NOT clear states, because they need to remain set
	// across frames so that they don't accidentally show "off" for
	// a tick or two while the raw input is still pending.
}

//-----------------------------------------------------------------------

//
// Set the state of a raw button
//
void InputMapper::SetRawButtonState(RawInputButton button, bool pressed)
{
    assert( ( button < e_RawInputButtonInvalid ) && "Button must be valid" );

	Action action;
	State state;

	const bool previouslyPressed = ( m_RawInputButtonStates[button] == e_RawInputButtonStatePressed );
	m_RawInputButtonStates[button] = ( pressed ? e_RawInputButtonStatePressed : e_RawInputButtonStateReleased );

	bool eatButton = true;

	if( pressed && !previouslyPressed )
	{
		if( _MapButtonToAction( button, action ) )
		{
			m_CurrentMappedInput.actions.insert( action );
			eatButton = false;
		}
	}

	if( pressed )
	{
		if( _MapButtonToState( button, state ) )
		{
			m_CurrentMappedInput.states.insert( state );
			eatButton = false;
		}
	}

    if( eatButton )
    {
        _MapAndEatButton( button );
    }
}

//-----------------------------------------------------------------------

//
// Set the raw axis value of a given axis
//
void InputMapper::SetRawAxisValue(RawInputAxis axis, double value)
{
	for(const InputContext* context : m_ActiveContexts)
	{
		Range range;
		if( context->MapAxisToRange( axis, range ) )
		{
			m_CurrentMappedInput.ranges[range] = context->GetConversions().Convert(range, value * context->GetSensitivity(range));
			break;
		}
	}
}

//-----------------------------------------------------------------------

void InputMapper::SetNewTextInput(UTF8* newText)
{
    m_CurrentMappedInput.textInput = newText;
}

//-----------------------------------------------------------------------

//
// Dispatch input to all registered callbacks
//
void InputMapper::Dispatch(float frameTimeInSeconds) const
{
	MappedInput input = m_CurrentMappedInput;
	for(std::multimap<int, std::pair<InputCallback, void*> >::const_iterator iter = m_CallbackTable.begin(); iter != m_CallbackTable.end(); ++iter)
	{
	    std::pair<InputCallback, void*> callBackAndExtraArg = iter->second;
        (*callBackAndExtraArg.first)( input, frameTimeInSeconds, callBackAndExtraArg.second );
	}
}

//-----------------------------------------------------------------------

//
// Add a callback to the dispatch table
//
void InputMapper::AddCallback(InputCallback callback, int priority, void* extraData)
{
	m_CallbackTable.insert( std::make_pair( priority, std::make_pair( callback, extraData ) ) );
}

//-----------------------------------------------------------------------

//
// Push an active input context onto the stack
//
void InputMapper::PushContext(const std::string& name)
{
	std::map<std::string, InputContext*>::iterator iter = m_InputContexts.find(name);
	if( iter == m_InputContexts.end() )
    {
        throw GenericException("Invalid input context pushed");
    }

	m_ActiveContexts.push_front(iter->second);
}

//-----------------------------------------------------------------------

//
// Pop the current input context off the stack
//
void InputMapper::PopContext()
{
	if( m_ActiveContexts.empty() )
    {
        throw GenericException("Cannot pop input context, no contexts active!");
    }

	m_ActiveContexts.pop_front();
}

//-----------------------------------------------------------------------


template<class ValType, bool (*ValTypeNameToValType)(const std::string&, ValType&), void (InputContext::*AddValidValType)(ValType val)>
static bool ReadContexStringValTable(luabind::iterator& ctxIt,
                                     const std::string& strTableName,
                                     InputContext& inputCtx,
                                     std::string& errMsg)
{
    bool retVal = true;

    const std::string contextName = luabind::tostring_operator( ctxIt.key() );

    luabind::object strTable = luabind::gettable( *ctxIt, strTableName );
    if( !strTable )
    {
        errMsg += "'";
        errMsg += strTableName;
        errMsg += "' table in context '";
        errMsg += contextName;
        errMsg += "' does not exist";
        retVal = false;
    }

    if( retVal && ( luabind::type( strTable ) == LUA_TTABLE ) )
    {
        for (luabind::iterator valTypeIt(strTable), end; valTypeIt != end && retVal; ++valTypeIt)
        {
            const std::string valTypeName = luabind::tostring_operator( *valTypeIt );

            if( luabind::type( *valTypeIt ) == LUA_TSTRING )
            {
                ValType valType;

                if( ValTypeNameToValType( valTypeName, valType ) )
                {
                    (inputCtx.*AddValidValType)( valType );
                }
                else
                {
                    errMsg += "'";
                    errMsg += valTypeName;
                    errMsg += "' in table '";
                    errMsg += contextName;
                    errMsg += "' is not a valid member of '";
                    errMsg += strTableName;
                    errMsg += "' table";
                    retVal = false;
                }
            }
            else
            {
                errMsg += "Member '";
                errMsg += valTypeName;
                errMsg += "' of table '";
                errMsg += strTableName;
                errMsg += "' is not a string";
                retVal = false;
            }
        }
    }
    else if( retVal )
    {
        errMsg += "'";
        errMsg += strTableName;
        errMsg += "' variable in table '";
        errMsg += contextName;
        errMsg += "' is not a table";
        retVal = false;
    }

    return retVal;
}

//-----------------------------------------------------------------------

//
// Load definitions of contexts from a config file
//
bool InputMapper::_ReadContextsDefinitionsCfgFile(const std::string& cfgFilePath, std::string& errMsg)
{
    std::string mainErrMsg = "Failed to load InputContexts config from file '" + cfgFilePath + "' ";
    bool ok = false;

    LuaUtils::LuaStateWrapper luaState;
    luaL_openlibs( luaState );
    if( LuaUtils::LoadLuaScript( luaState, cfgFilePath, m_FileManager ) )
    {
        std::string secondaryErrMsg = "with the following error: ";
        ok = true;

        // Connect LuaBind to this lua state
        luabind::open( luaState );

        luabind::object topTable = luabind::globals( luaState )["Contexts"];

        if( !topTable )
        {
            secondaryErrMsg += "Global Lua table 'Contexts' does not exist";
            ok = false;
        }

        if( ok && ( luabind::type( topTable ) == LUA_TTABLE ) )
        {
            for (luabind::iterator contextIt(topTable), end; contextIt != end && ok; ++contextIt)
            {
                if( luabind::type( *contextIt ) == LUA_TTABLE )
                {
                    const std::string contextName = luabind::tostring_operator( contextIt.key() );
                    InputContext* inputContext = new InputContext( m_Conversions );

                    //
                    // Read valid actions for this context
                    //
                    ok = ok && ReadContexStringValTable<Action,
                                                        ActionNameToAction,
                                                        &InputContext::AddValidAction>( contextIt,
                                                                                        "Actions",
                                                                                        *inputContext,
                                                                                        secondaryErrMsg);
                    //
                    // Read valid states for this context
                    //
                    ok = ok && ReadContexStringValTable<State,
                                                        StateNameToState,
                                                        &InputContext::AddValidState>( contextIt,
                                                                                       "States",
                                                                                       *inputContext,
                                                                                       secondaryErrMsg);
                    //
                    // Read valid ranges for this context
                    //
                    ok = ok && ReadContexStringValTable<Range,
                                                        RangeNameToRange,
                                                        &InputContext::AddValidRange>( contextIt,
                                                                                       "Ranges",
                                                                                       *inputContext,
                                                                                       secondaryErrMsg);

                    static const char* c_RangeSensitivityTableName = "RangeSensitivity";
                    luabind::object rangeSensitivityTable = luabind::gettable( *contextIt,
                                                                                c_RangeSensitivityTableName );
                    if( ok && !rangeSensitivityTable )
                    {
                        secondaryErrMsg += "'";
                        secondaryErrMsg += c_RangeSensitivityTableName;
                        secondaryErrMsg += "' table in context '";
                        secondaryErrMsg += contextName;
                        secondaryErrMsg += "' does not exist";
                        ok = false;
                    }

                    if( ok && ( luabind::type( rangeSensitivityTable ) == LUA_TTABLE ) )
                    {
                        for (luabind::iterator rangeSensIt(rangeSensitivityTable), end; rangeSensIt != end && ok; ++rangeSensIt)
                        {
                            const std::string rangeName = luabind::tostring_operator( rangeSensIt.key() );

                            Range range;
                            if( RangeNameToRange( rangeName, range ) )
                            {
                                if( luabind::type( *rangeSensIt ) == LUA_TNUMBER )
                                {
                                    if( !inputContext->SetRangeSensitivity( range,
                                                                            luabind::object_cast<double>( *rangeSensIt ) ) )
                                    {
                                        secondaryErrMsg += "'";
                                        secondaryErrMsg += rangeName;
                                        secondaryErrMsg += "' is not a valid range in context '";
                                        secondaryErrMsg += contextName;
                                        secondaryErrMsg += "'";
                                        ok = false;
                                    }
                                }
                                else
                                {
                                    secondaryErrMsg += "Value '";
                                    secondaryErrMsg += luabind::tostring_operator( *rangeSensIt );
                                    secondaryErrMsg += "' assigned to '";
                                    secondaryErrMsg += rangeName;
                                    secondaryErrMsg += "' in table '";
                                    secondaryErrMsg += c_RangeSensitivityTableName;
                                    secondaryErrMsg += "' of context '";
                                    secondaryErrMsg += contextName;
                                    secondaryErrMsg += "' is not a number";
                                    ok = false;
                                }
                            }
                            else
                            {
                                secondaryErrMsg += "'";
                                secondaryErrMsg += rangeName;
                                secondaryErrMsg += "' in table '";
                                secondaryErrMsg += c_RangeSensitivityTableName;
                                secondaryErrMsg += "' of context '";
                                secondaryErrMsg += contextName;
                                secondaryErrMsg += "' is not a valid range ID";
                                ok = false;
                            }
                        }
                    }
                    else if( ok )
                    {
                        secondaryErrMsg += "'";
                        secondaryErrMsg += c_RangeSensitivityTableName;
                        secondaryErrMsg += "' variable in table '";
                        secondaryErrMsg += contextName;
                        secondaryErrMsg += "' is not a table";
                        ok = false;
                    }

                    if( ok )
                    {
                        m_InputContexts[contextName] = inputContext;
                    }
                    else
                    {
                        delete inputContext;
                        inputContext = 0;
                    }
                }
                else
                {
                    secondaryErrMsg += "'Contexts' table has non table member '";
                    secondaryErrMsg += luabind::tostring_operator( contextIt.key() );
                    secondaryErrMsg += "'";
                    ok = false;
                }
            }
        }
        else if( ok )
        {
            secondaryErrMsg += "Global Lua variable 'Contexts' is not a table";
            ok = false;
        }

        if( !ok )
        {
            mainErrMsg += secondaryErrMsg;
        }
    }

    if( !ok )
    {
        errMsg = mainErrMsg;
    }

    return ok;
}

//-----------------------------------------------------------------------

//
// Load mappings of Raw Input Data to Actions, States and Ranges for every context
//
bool InputMapper::_ReadContextsInputMapCfgFile(const std::string& filePath, std::string& errMsg)
{
    std::string mainErrMsg = "Failed to load InputContexts config from file '" + filePath + "' ";
    bool ok = false;

    LuaUtils::LuaStateWrapper luaState;
    luaL_openlibs( luaState );
    if( LuaUtils::LoadLuaScript( luaState, filePath, m_FileManager ) )
    {
        std::string secondaryErrMsg = "with the following error: ";
        ok = true;

        // Connect LuaBind to this lua state
        luabind::open( luaState );

        luabind::object topTable = luabind::globals( luaState )["Contexts"];

        if( !topTable )
        {
            secondaryErrMsg += "Global Lua table 'Contexts' does not exist";
            ok = false;
        }

        if( ok && ( luabind::type( topTable ) == LUA_TTABLE ) )
        {
            for (luabind::iterator contextIt(topTable), end; contextIt != end && ok; ++contextIt)
            {
                if( luabind::type( *contextIt ) == LUA_TTABLE )
                {
                    const std::string contextName = luabind::tostring_operator( contextIt.key() );

                    std::map<std::string, InputContext*>::iterator it = m_InputContexts.find( contextName );

                    if( it != m_InputContexts.end() )
                    {
                        ok = ok && _ReadRawInputButtonsToActionsMap(contextIt,
                                                                    *(it->second),
                                                                    secondaryErrMsg);
                        ok = ok && _ReadRawInputButtonsToStatesMap(contextIt,
                                                                   *(it->second),
                                                                   secondaryErrMsg);
                        ok = ok && _ReadRawInputAxisToRangesMap(contextIt,
                                                                *(it->second),
                                                                secondaryErrMsg);
                    }
                    else
                    {
                        secondaryErrMsg += "'";
                        secondaryErrMsg += contextName;
                        secondaryErrMsg += "' is not a valid context";
                        ok = false;
                    }
                }
                else
                {
                    secondaryErrMsg += "'Contexts' table has non table member '";
                    secondaryErrMsg += luabind::tostring_operator( contextIt.key() );
                    secondaryErrMsg += "'";
                    ok = false;
                }
            }
        }
        else if( ok )
        {
            secondaryErrMsg += "Global Lua variable 'Contexts' is not a table";
            ok = false;
        }

        if( !ok )
        {
            mainErrMsg += secondaryErrMsg;
        }
    }

    if( !ok )
    {
        errMsg = mainErrMsg;
    }

    return ok;
}

//-----------------------------------------------------------------------

bool InputMapper::_ReadRawInputButtonsToActionsMap(luabind::iterator& ctxIt,
                                                    InputContext& inputCtx,
                                                    std::string& errMsg)
{
    bool retVal = true;
    static const char* c_ActionTableName = "Actions";
    const std::string contextName = luabind::tostring_operator( ctxIt.key() );

    luabind::object actionsTable = luabind::gettable( *ctxIt, c_ActionTableName );
    if( !actionsTable )
    {
        errMsg += "'";
        errMsg += c_ActionTableName;
        errMsg += "' table in context '";
        errMsg += contextName;
        errMsg += "' does not exist";
        retVal = false;
    }

    if( retVal && ( luabind::type( actionsTable ) == LUA_TTABLE ) )
    {
        for (luabind::iterator buttonMapIt(actionsTable), end; buttonMapIt != end && retVal; ++buttonMapIt)
        {
            const std::string rawInputButtonName = luabind::tostring_operator( buttonMapIt.key() );
            const std::string mappedActionName = luabind::tostring_operator( *buttonMapIt );

            if( luabind::type( *buttonMapIt ) == LUA_TSTRING )
            {
                Action action;
                if( !ActionNameToAction( mappedActionName, action ) )
                {
                    errMsg += "'";
                    errMsg += mappedActionName;
                    errMsg += "' in table '";
                    errMsg += contextName;
                    errMsg += "' is not a valid action";
                    retVal = false;
                }

                RawInputButton button;
                if( retVal && RawInputButtonNameToRawInputButton( rawInputButtonName, button ) )
                {
                    if( !inputCtx.AddRawButtonToActionMapping( button, action ) )
                    {
                        errMsg += "Action '";
                        errMsg += mappedActionName;
                        errMsg += "' is not valid in context '";
                        errMsg += contextName;
                        errMsg += "'";
                        retVal = false;
                    }
                }
                else if( retVal )
                {
                    errMsg += "'";
                    errMsg += rawInputButtonName;
                    errMsg += "' in table '";
                    errMsg += contextName;
                    errMsg += "' is not a valid raw input button'";
                    retVal = false;
                }
            }
            else
            {
                errMsg += "Member '";
                errMsg += mappedActionName;
                errMsg += "' of table '";
                errMsg += c_ActionTableName;
                errMsg += "' in context '";
                errMsg += contextName;
                errMsg += "' is not a string";
                retVal = false;
            }
        }
    }
    else if( retVal )
    {
        errMsg += "'";
        errMsg += c_ActionTableName;
        errMsg += "' variable in table '";
        errMsg += contextName;
        errMsg += "' is not a table";
        retVal = false;
    }

    return retVal;
}

bool InputMapper::_ReadRawInputButtonsToStatesMap(luabind::iterator& ctxIt,
                                                   InputContext& inputCtx,
                                                   std::string& errMsg)
{
    bool retVal = true;
    static const char* c_StateTableName = "States";
    const std::string contextName = luabind::tostring_operator( ctxIt.key() );

    luabind::object statesTable = luabind::gettable( *ctxIt, c_StateTableName );
    if( !statesTable )
    {
        errMsg += "'";
        errMsg += c_StateTableName;
        errMsg += "' table in context '";
        errMsg += contextName;
        errMsg += "' does not exist";
        retVal = false;
    }

    if( retVal && ( luabind::type( statesTable ) == LUA_TTABLE ) )
    {
        for (luabind::iterator buttonMapIt(statesTable), end; buttonMapIt != end && retVal; ++buttonMapIt)
        {
            const std::string rawInputButtonName = luabind::tostring_operator( buttonMapIt.key() );
            const std::string mappedStateName = luabind::tostring_operator( *buttonMapIt );

            if( luabind::type( *buttonMapIt ) == LUA_TSTRING )
            {
                State state;
                if( !StateNameToState( mappedStateName, state ) )
                {
                    errMsg += "'";
                    errMsg += mappedStateName;
                    errMsg += "' in table '";
                    errMsg += contextName;
                    errMsg += "' is not a valid state";
                    retVal = false;
                }

                RawInputButton button;
                if( retVal && RawInputButtonNameToRawInputButton( rawInputButtonName, button ) )
                {
                    if( !inputCtx.AddRawButtonToStateMapping( button, state ) )
                    {
                        errMsg += "State '";
                        errMsg += mappedStateName;
                        errMsg += "' is not valid in context '";
                        errMsg += contextName;
                        errMsg += "'";
                        retVal = false;
                    }
                }
                else if( retVal )
                {
                    errMsg += "'";
                    errMsg += rawInputButtonName;
                    errMsg += "' in table '";
                    errMsg += contextName;
                    errMsg += "' is not a valid raw input button'";
                    retVal = false;
                }
            }
            else
            {
                errMsg += "Member '";
                errMsg += mappedStateName;
                errMsg += "' of table '";
                errMsg += c_StateTableName;
                errMsg += "' in context '";
                errMsg += contextName;
                errMsg += "' is not a string";
                retVal = false;
            }
        }
    }
    else if( retVal )
    {
        errMsg += "'";
        errMsg += c_StateTableName;
        errMsg += "' variable in table '";
        errMsg += contextName;
        errMsg += "' is not a table";
        retVal = false;
    }

    return retVal;
}

//-----------------------------------------------------------------------

bool InputMapper::_ReadRawInputAxisToRangesMap(luabind::iterator& ctxIt,
                                                InputContext& inputCtx,
                                                std::string& errMsg)
{
    bool retVal = true;
    static const char* c_RangeTableName = "Ranges";
    const std::string contextName = luabind::tostring_operator( ctxIt.key() );

    luabind::object rangesTable = luabind::gettable( *ctxIt, c_RangeTableName );
    if( !rangesTable )
    {
        errMsg += "'";
        errMsg += c_RangeTableName;
        errMsg += "' table in context '";
        errMsg += contextName;
        errMsg += "' does not exist";
        retVal = false;
    }

    if( retVal && ( luabind::type( rangesTable ) == LUA_TTABLE ) )
    {
        for (luabind::iterator axisMapIt(rangesTable), end; axisMapIt != end && retVal; ++axisMapIt)
        {
            const std::string rawInputAxisName = luabind::tostring_operator( axisMapIt.key() );
            const std::string mappedRangeName = luabind::tostring_operator( *axisMapIt );

            if( luabind::type( *axisMapIt ) == LUA_TSTRING )
            {
                Range range;
                if( !RangeNameToRange( mappedRangeName, range ) )
                {
                    errMsg += "'";
                    errMsg += mappedRangeName;
                    errMsg += "' in table '";
                    errMsg += contextName;
                    errMsg += "' is not a valid range";
                    retVal = false;
                }

                RawInputAxis axis;
                if( retVal && RawInputAxisNameToRawInputAxis( rawInputAxisName, axis ) )
                {
                    if( !inputCtx.AddRawAxisToRangeMapping( axis, range ) )
                    {
                        errMsg += "Range '";
                        errMsg += mappedRangeName;
                        errMsg += "' is not valid in context '";
                        errMsg += contextName;
                        errMsg += "'";
                        retVal = false;
                    }
                }
                else if( retVal )
                {
                    errMsg += "'";
                    errMsg += rawInputAxisName;
                    errMsg += "' in table '";
                    errMsg += contextName;
                    errMsg += "' is not a valid raw input axis'";
                    retVal = false;
                }
            }
            else
            {
                errMsg += "Member '";
                errMsg += mappedRangeName;
                errMsg += "' of table '";
                errMsg += c_RangeTableName;
                errMsg += "' in context '";
                errMsg += contextName;
                errMsg += "' is not a string";
                retVal = false;
            }
        }
    }
    else if( retVal )
    {
        errMsg += "'";
        errMsg += c_RangeTableName;
        errMsg += "' variable in table '";
        errMsg += contextName;
        errMsg += "' is not a table";
        retVal = false;
    }

    return retVal;
}

//-----------------------------------------------------------------------

//
// Helper: map a button to an action in the active context(s)
//
bool InputMapper::_MapButtonToAction(RawInputButton button, Action& action) const
{
    assert( ( button < e_RawInputButtonInvalid ) && "Button must be valid" );

	for(const InputContext* context : m_ActiveContexts)
	{
		if( context->MapButtonToAction( button, action ) )
        {
            return true;
        }
	}

	return false;
}

//-----------------------------------------------------------------------

//
// Helper: map a button to a state in the active context(s)
//
bool InputMapper::_MapButtonToState(RawInputButton button, State& state) const
{
    assert( ( button < e_RawInputButtonInvalid ) && "Button must be valid" );

	for(const InputContext* context : m_ActiveContexts)
	{
		if( context->MapButtonToState( button, state ) )
        {
            return true;
        }
	}

	return false;
}

//-----------------------------------------------------------------------

//
// Helper: eat all input mapped to a given button
//
void InputMapper::_MapAndEatButton(RawInputButton button)
{
    assert( ( button < e_RawInputButtonInvalid ) && "Button must be valid" );

	Action action;
	State state;

	if( _MapButtonToAction( button, action ) )
    {
        m_CurrentMappedInput.EatAction( action );
    }

	if( _MapButtonToState( button, state ) )
    {
        m_CurrentMappedInput.EatState( state );
    }
}

//-----------------------------------------------------------------------
