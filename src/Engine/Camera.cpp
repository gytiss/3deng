#include "Camera.h"

#include <cmath>
#include <MathUtils.h>
#include <glm/gtc/matrix_transform.hpp>

//-----------------------------------------------------------------------

const float Camera::c_HorizontalAngleOffsetInRadians = static_cast<float>( M_PI );
const float Camera::c_VerticalAngleLimitInRadians = (static_cast<float>( M_PI ) / 2.0f ) - MathUtils::DegreesToRadians( 0.01f ); // -0.01 degrees is needed to prevent heading calculation from jumping +/- 180 degrees when camera is looking all the way up or down

//-----------------------------------------------------------------------

Camera::Camera(const SystemInterface::DrawableSurfaceSize& screenDimensions,
               const float speedInMetersPerSecond)
: m_Position(),
  m_DirectionVector(),
  m_UpVector(),
  m_RightVector(),
  m_FieldOfView( 45.0f ),
  m_HorizontalAngleInRadians( c_HorizontalAngleOffsetInRadians ),
  m_VerticalAngleInRadians( 0.0f ),
  m_SpeedMetersPerSecond( speedInMetersPerSecond ),
  m_SpeedMultiplier( 1.0f ),
  m_ScreenDimensions( screenDimensions ),
  m_CachedProjectionMatrix( MathUtils::c_IdentityMatrix )
{
    assert( ( m_SpeedMetersPerSecond >= 0.0f ) && "Speed must not be negative" );
    _RecalculateProjectionMatrix();
    _RecalculateVectorData( m_HorizontalAngleInRadians, m_VerticalAngleInRadians );
}

Camera::Camera(const SystemInterface::DrawableSurfaceSize& screenDimensions)
: m_Position(),
  m_DirectionVector(),
  m_UpVector(),
  m_RightVector(),
  m_FieldOfView( 45.0f ),
  m_HorizontalAngleInRadians( c_HorizontalAngleOffsetInRadians ),
  m_VerticalAngleInRadians( 0.0f ),
  m_SpeedMetersPerSecond( Units::OneMeter ),
  m_SpeedMultiplier( 1.0f ),
  m_ScreenDimensions( screenDimensions ),
  m_CachedProjectionMatrix( MathUtils::c_IdentityMatrix )
{
    _RecalculateProjectionMatrix();
    _RecalculateVectorData( m_HorizontalAngleInRadians, m_VerticalAngleInRadians );
}

//-----------------------------------------------------------------------

glm::mat4 Camera::GetViewMatrix() const
{
    return glm::lookAt( m_Position, // The position of camera(In world space)
                        m_Position + m_DirectionVector, // Where camera is looking at(In world space)
                        m_UpVector );

}

//-----------------------------------------------------------------------

void Camera::ForcePosition(const glm::vec3& pos)
{
    m_Position = pos;
}

//-----------------------------------------------------------------------

void Camera::ForcePosition(const float x, const float y, const float z)
{
    ForcePosition( glm::vec3( x, y, z ) );
}

//-----------------------------------------------------------------------

void Camera::ForceOrientation( const float horizAngleInDegrees,
                               const float vertAngleInDegrees )
{
    m_HorizontalAngleInRadians = MathUtils::DegreesToRadians( horizAngleInDegrees ) + c_HorizontalAngleOffsetInRadians;
    m_VerticalAngleInRadians = MathUtils::DegreesToRadians( vertAngleInDegrees );

    m_VerticalAngleInRadians = glm::clamp( m_VerticalAngleInRadians,
                                          -c_VerticalAngleLimitInRadians,
                                           c_VerticalAngleLimitInRadians );

    _RecalculateVectorData( m_HorizontalAngleInRadians , m_VerticalAngleInRadians );
}

//-----------------------------------------------------------------------

void Camera::RecalculateOrientation( const float frameTimeInSeconds,
                                      const float x,
                                      const float y,
                                      const float sensitivity )
{
    m_HorizontalAngleInRadians += sensitivity * frameTimeInSeconds * ( ( 1.0f / 2.0f ) - x );
    m_VerticalAngleInRadians   += sensitivity * frameTimeInSeconds * ( ( 1.0f / 2.0f ) - y );
    m_VerticalAngleInRadians = glm::clamp( m_VerticalAngleInRadians,
                                          -c_VerticalAngleLimitInRadians,
                                           c_VerticalAngleLimitInRadians );

    _RecalculateVectorData( m_HorizontalAngleInRadians, m_VerticalAngleInRadians );
}

//-----------------------------------------------------------------------

void Camera::RecalculatePosition( const float frameTimeInSeconds,
                                   const bool moveFront,
                                   const bool moveBack,
                                   const bool moveLeft,
                                   const bool moveRight )
{
    m_Position += ( moveFront ? ( glm::normalize( m_DirectionVector ) * Units::OneMeter * m_SpeedMetersPerSecond * m_SpeedMultiplier * frameTimeInSeconds ) : glm::vec3( 0.0f ) );
    m_Position -= ( moveBack ? ( glm::normalize( m_DirectionVector ) * Units::OneMeter * m_SpeedMetersPerSecond * m_SpeedMultiplier * frameTimeInSeconds ) : glm::vec3( 0.0f ) );
    m_Position -= ( moveLeft ? ( glm::normalize( m_RightVector ) * Units::OneMeter * m_SpeedMetersPerSecond * m_SpeedMultiplier * frameTimeInSeconds ) : glm::vec3( 0.0f ) );
    m_Position += ( moveRight ? ( glm::normalize( m_RightVector ) * Units::OneMeter * m_SpeedMetersPerSecond * m_SpeedMultiplier * frameTimeInSeconds ) : glm::vec3( 0.0f ) );
}

//-----------------------------------------------------------------------

void Camera::MoveCameraAFixedNumberOfUnits(const float metersToMoveAlongViewDirection,
                                           const float metersToMoveToEitherSides)
{
    const float metersToMoveInViewDirection = ( std::abs( metersToMoveAlongViewDirection ) * Units::OneMeter );
    const float metersToMoveToEitherSide = ( std::abs( metersToMoveToEitherSides ) * Units::OneMeter );

    m_Position += ( ( metersToMoveAlongViewDirection < 0.0f ) ? ( glm::normalize( m_DirectionVector ) * metersToMoveInViewDirection ) : glm::vec3( 0.0f ) );
    m_Position -= ( ( metersToMoveAlongViewDirection > 0.0f ) ? ( glm::normalize( m_DirectionVector ) * metersToMoveInViewDirection ) : glm::vec3( 0.0f ) );
    m_Position -= ( ( metersToMoveToEitherSides < 0.0f ) ? ( glm::normalize( m_RightVector ) * metersToMoveToEitherSide ) : glm::vec3( 0.0f ) );
    m_Position += ( ( metersToMoveToEitherSides > 0.0f ) ? ( glm::normalize( m_RightVector ) * metersToMoveToEitherSide ) : glm::vec3( 0.0f ) );
}

//-----------------------------------------------------------------------

void Camera::_RecalculateVectorData( const float horizAngleInRadians,
                                      float vertAngleInInRadians )
{
    //                         Vertical Angle
    //                   ^         /
    //      Dir. Vec. -->|        /
    //                   |-------/
    //                ---|      |
    // Horiz. Angle-->|  /--------->
    //                | /     ^
    //                |/      |
    //                /    Right Vec.
    //   Up Vec. --> /
    //              /

    vertAngleInInRadians = glm::clamp( vertAngleInInRadians,
                                      -c_VerticalAngleLimitInRadians,
                                       c_VerticalAngleLimitInRadians );

    m_DirectionVector = glm::vec3(
        cos( vertAngleInInRadians ) * sin( horizAngleInRadians ),
        sin( vertAngleInInRadians ),
        cos( vertAngleInInRadians ) * cos( horizAngleInRadians )
    );

    m_RightVector = glm::vec3(  // Vector which is on the right side of the camera and is perpendicular to the direction vector
        sin( horizAngleInRadians - M_PI/2.0f ),
        0,
        cos( horizAngleInRadians - M_PI/2.0f )
    );

    // Since the “up” vector is a vector that is perpendicular to both "direction" and "right" vectors,
    // it is possible to calculate it with a cross product of the two.
    m_UpVector = glm::cross( m_RightVector, m_DirectionVector );
}

//-----------------------------------------------------------------------

void Camera::_RecalculateProjectionMatrix()
{
    m_CachedProjectionMatrix = MathUtils::GeneratePerspectiveProjectionMatrix( GetFieldOfView(),
                                                                               m_ScreenDimensions.widthInPixels,
                                                                               m_ScreenDimensions.heightInPixels );
}
