#ifndef ENTITYMARKER_H_INCLUDED
#define ENTITYMARKER_H_INCLUDED

#include <Scene/GameObject.h>
#include <GraphicsDevice/VAO.h>
#include <Resources/MaterialManager.h>
#include <EngineTypes.h>
#include <glm/glm.hpp>
#include <vector>

class PhysicsManager;
class TextureManager;

class EntityMarker : public GameObject
{
public:
    bool IsAnimated() const
    {
        return false;
    }

    virtual void RecalculateRenderables() = 0;

    inline void SetColor(const glm::vec3& color)
    {
        m_Color.SetSRGBValue( color );
        _RecalculateRenderingData();
    }

protected:
    enum CircleType
    {
        e_CircleTypeVertical = 0,
        e_CircleTypeVerticalRotated90Degrees = 1,
        e_CircleTypeHorizontal
    };

    struct EntityMarkerVertex
    {
        float position[3];
    };

    typedef std::vector<EntityMarkerVertex> VertexArray_t;

    EntityMarker(PhysicsManager& physicsManager,
                 MaterialManager& materialManager,
                 TextureManager& textureManager);

    virtual ~EntityMarker();

    size_t _GetNumCircleSegments(float r) const;
    size_t _GenerateCircleVertices(CircleType type,
                                   float cx,
                                   float cy,
                                   float r,
                                   size_t numOfSegments,
                                   VertexArray_t& vertArray);
    void _RecalculateRenderingData();

    EngineTypes::SRGBColor m_Color;
    VAO m_VAO;
    PhysicsManager& m_PhysicsManager;
    MaterialManager& m_MaterialManager;
    TextureManager& m_TextureManager;
    MaterialManager::MaterialHandle m_MaterialHandle;

private:
    // Used to prevent copying
    EntityMarker&  operator = (const EntityMarker& other);
    EntityMarker(const EntityMarker& other);
};

//-----------------------------------------------------------------------

#endif // ENTITYMARKER_H_INCLUDED
