#ifndef COMPONENT_H_INCLUDED
#define COMPONENT_H_INCLUDED

#include <string>

class GameObject;

#define GAME_OBJECT_COMPONENT_TYPE(type) public: \
                                         static const Type m_ComponentType = (type); \
                                         private:

/**
 * Every GameObject can have components assigned to it. This is the
 * base component class which all of the components are derived from
 */
class Component
{
public:
    enum Type
    {
        e_TypeTransform = 0,
        e_TypeRenderer = 1,
        e_TypeRigidBody = 2,
        e_TypeCollider,
        e_TypeAnimator,
        e_TypeMeshFilter,
        e_TypeSoundSource,
        e_TypeCustom,

        e_TypeCount // Not a type (used for counting)
    };

    inline Type GetType() const
    {
        return m_Type;
    }

    inline const std::string& GetName() const
    {
        return m_Name;
    }

    virtual ~Component() = default;

    inline bool IsEnabled() const
    {
        return m_Enabled;
    }

    virtual void SetEnabled(bool val)
    {
        m_Enabled = val;
    }

protected:
    // The constructor is protected because this object is not supposed to be instanciated on its own
    Component(Type type, const std::string& name, GameObject& owningGameObject)
    : m_Type( type ),
      m_Name( name ),
      m_OwningGameObject( owningGameObject ),
      m_Enabled( true )
    {
    }

    inline GameObject& GetOwningGameObject() const
    {
        return m_OwningGameObject;
    }

    inline GameObject& GetOwningGameObject()
    {
        return m_OwningGameObject;
    }

private:
    const Type m_Type;
    std::string m_Name;
    GameObject& m_OwningGameObject;
    bool m_Enabled;
};

#endif // COMPONENT_H_INCLUDED
