#ifndef AK47_H_INCLUDED
#define AK47_H_INCLUDED

#include <Scene/GameObject.h>
#include <SystemInterface.h>

class MeshManager;
class AnimationManager;
class MaterialManager;
class AudioDataManager;
class SoundManager;
class Animator;
class Transform;
class Camera;
class SoundSource;

class AK47 : public GameObject, public GameObject::GameObjectLogicInterface
{
public:
    enum State
    {
        e_StateStatic = 0,
        e_StateReloading = 1,
        e_StateShooting
    };

    AK47(Camera& camera,
         SystemInterface& systemInterface,
         MeshManager& meshManager,
         AnimationManager& animationManager,
         MaterialManager& materialManager,
         PhysicsManager& physicsManager,
         AudioDataManager& audioDataManager,
         SoundManager& soundManager);

    virtual ~AK47();

    void SetState(State newState);
    State GetState() const;

private:
    static const unsigned int c_MagazineCapacity = 20;
    static const unsigned int c_BulletsPerSecond = 10;

    static void _DrawableSurfaceSizeChaged(const SystemInterface::DrawableSurfaceSize& newSize, void* userData);

    void _Initialize() override;
    void _FixedUpdate() override;
    void _Update() override;
    void _LastUpdate() override;
    GameObject* _GetShotObject();

    Camera& m_Camera;
    SystemInterface& m_SystemInterface;
    SystemInterface::DrawableSurfaceSize m_DrawableSurfaceSize;
    PhysicsManager& m_PhysicsManager;
    State m_CurrentState;
    SoundSource* m_ShootingSoundSource;
    SoundSource* m_ReloadSoundSource;
    Animator* m_Animator;
    Transform* m_Offset;
    int m_BulletsInTheMagazine;
    double m_GlobalTimeInSecondDuringStateChange;
};


#endif // AK47_H_INCLUDED
