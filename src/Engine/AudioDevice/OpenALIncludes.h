#ifndef OPENALINCLUDES_H_INCLUDED
#define OPENALINCLUDES_H_INCLUDED

#define AL_ALEXT_PROTOTYPES
#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alext.h>
#include <AL/efx.h>

#endif // OPENALINCLUDES_H_INCLUDED
