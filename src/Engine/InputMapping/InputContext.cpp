//
// Wrapper class for managing input contexts
//
#include "InputContext.h"

using namespace InputMapping;

//-----------------------------------------------------------------------

//
// Construct and initialize an input context given data in a file
//
InputContext::InputContext(const RangeConverter& conversions)
: m_Conversions( conversions )
{
//	std::wifstream infile(contextfilename.c_str());
//
//	unsigned rangecount = AttemptRead<unsigned>(infile);
//	for(unsigned i = 0; i < rangecount; ++i)
//	{
//		RawInputAxis axis = static_cast<RawInputAxis>(AttemptRead<unsigned>(infile));
//		Range range = static_cast<Range>(AttemptRead<unsigned>(infile));
//		m_RangeMap[axis] = range;
//	}
//
//	unsigned statecount = AttemptRead<unsigned>(infile);
//	for(unsigned i = 0; i < statecount; ++i)
//	{
//		RawInputButton button = static_cast<RawInputButton>(AttemptRead<unsigned>(infile));
//		State state = static_cast<State>(AttemptRead<unsigned>(infile));
//		m_StateMap[button] = state;
//	}
//
//	unsigned actioncount = AttemptRead<unsigned>(infile);
//	for(unsigned i = 0; i < actioncount; ++i)
//	{
//		RawInputButton button = static_cast<RawInputButton>(AttemptRead<unsigned>(infile));
//		Action action = static_cast<Action>(AttemptRead<unsigned>(infile));
//		m_ActionMap[button] = action;
//	}
//
//	m_Conversions = new RangeConverter(infile);
//
//	unsigned sensitivitycount = AttemptRead<unsigned>(infile);
//	for(unsigned i = 0; i < sensitivitycount; ++i)
//	{
//		Range range = static_cast<Range>(AttemptRead<unsigned>(infile));
//		double sensitivity = AttemptRead<double>(infile);
//		m_SensitivityMap[range] = sensitivity;
//	}
}

//-----------------------------------------------------------------------

//
// Destruct and clean up an input context
//
InputContext::~InputContext()
{
}

//-----------------------------------------------------------------------

//
// Attempt to map a raw button to an action
//
bool InputContext::MapButtonToAction(RawInputButton button, Action& out) const
{
	std::map<RawInputButton, Action>::const_iterator iter = m_ActionMap.find(button);
	if(iter == m_ActionMap.end())
		return false;

	out = iter->second;
	return true;
}

//-----------------------------------------------------------------------

//
// Attempt to map a raw button to a state
//
bool InputContext::MapButtonToState(RawInputButton button, State& out) const
{
	std::map<RawInputButton, State>::const_iterator iter = m_StateMap.find(button);
	if(iter == m_StateMap.end())
		return false;

	out = iter->second;
	return true;
}

//-----------------------------------------------------------------------

//
// Attempt to map a raw axis to a range
//
bool InputContext::MapAxisToRange(RawInputAxis axis, Range& out) const
{
	std::map<RawInputAxis, Range>::const_iterator iter = m_RangeMap.find(axis);
	if(iter == m_RangeMap.end())
		return false;

	out = iter->second;
	return true;
}

//-----------------------------------------------------------------------

//
// Retrieve the sensitivity associated with a given range
//
double InputContext::GetSensitivity(Range range) const
{
	std::map<Range, double>::const_iterator iter = m_SensitivityMap.find(range);
	if(iter == m_SensitivityMap.end())
		return 1.0;

	return iter->second;
}

//-----------------------------------------------------------------------

//
// Add an action to the set of actions supported by the context
//
void InputContext::AddValidAction(Action action)
{
    m_ValidActions.insert( action );
}

//-----------------------------------------------------------------------

//
// Add a state to the set of states supported by the context
//
void InputContext::AddValidState(State state)
{
    m_ValidStates.insert( state );
}

//-----------------------------------------------------------------------

//
// Add a range to the set of ranges supported by the context
//
void InputContext::AddValidRange(Range range)
{
    m_ValidRanges.insert( range );
}

//-----------------------------------------------------------------------

//
// Set sensitivity value for a range supported by the context
//
bool InputContext::SetRangeSensitivity(Range range, double sensitivity)
{
    bool retVal = false;

    if( m_ValidRanges.find( range ) != m_ValidRanges.end() )
    {
        m_SensitivityMap[range] = sensitivity;
        retVal = true;
    }

    return retVal;
}

//-----------------------------------------------------------------------

//
// Tie Raw Input Button with an action supported by the context
//
bool InputContext::AddRawButtonToActionMapping(RawInputButton button, Action action)
{
    bool retVal = false;

    if( m_ValidActions.find( action ) != m_ValidActions.end() )
    {
        m_ActionMap[button] = action;
        retVal = true;
    }

    return retVal;
}

//-----------------------------------------------------------------------

//
// Tie Raw Input Button with a state supported by the context
//
bool InputContext::AddRawButtonToStateMapping(RawInputButton button, State state)
{
    bool retVal = false;

    if( m_ValidStates.find( state ) != m_ValidStates.end() )
    {
        m_StateMap[button] = state;
        retVal = true;
    }

    return retVal;
}

//-----------------------------------------------------------------------

//
// Tie Raw Input Axis with a range supported by the context
//
bool InputContext::AddRawAxisToRangeMapping(RawInputAxis axis, Range range)
{
    bool retVal = false;

    if( m_ValidRanges.find( range ) != m_ValidRanges.end() )
    {
        m_RangeMap[axis] = range;
        retVal = true;
    }

    return retVal;
}

//-----------------------------------------------------------------------
