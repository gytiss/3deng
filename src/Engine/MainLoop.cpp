#include "MainLoop.h"

#include <CommonDefinitions.h>
#include <MsgLogger.h>
#include <StringUtils.h>
#include <SystemInterface.h>
#include <GraphicsDevice/GraphicsDevice.h>
#include <GraphicsDevice/OpenGLDebugOutput.h>
#include <Resources/AnimationManager.h>
#include <Resources/MeshManager.h>
#include <Resources/TextureManager.h>
#include <Resources/MaterialManager.h>
#include <Renderer/RenderManager.h>
#include <Physics/PhysicsManager.h>
#include <Resources/Audio/AudioDataManager.h>
#include <Resources/Audio/SoundManager.h>

//-----------------------------------------------------------------------

MainLoop::MainLoop(SystemInterface& systemInterface, 
#ifdef VR_BACKEND_ENABLED
                   VRBackend& vrBackend,
#endif   
                   FileManager& fileManager)
: m_SystemInterface( systemInterface ),
  m_GraphicsDevice( new GraphicsDevice() ),
  m_OpenGLDebugOutput( new OpenGLDebugOutput( *m_GraphicsDevice ) ),
  m_FileManager( fileManager ),
  m_AnimationManager( nullptr ),
  m_MeshManager( nullptr ),
  m_TextureManager( nullptr ),
  m_MaterialManager( nullptr ),
  m_RenderManager( nullptr ),
  m_PhysicsManager( nullptr ),
  m_AudioDataManager( nullptr ),
  m_SoundManager( nullptr ),
  m_FirstUpdate( true ),
  m_NumOfFramesSinceTheLastFPSCheck( 0 ),
  m_CurrentTimeInSeconds( m_SystemInterface.GetTicksInSeconds() ),
  m_AccumulatedFrameTimeInSeconds( 0.0f ),
  m_TimeSinceTheLastFPSMeasurementInSeconds( 0.0f ),
  m_CurrentFrameTimeInMilliseconds( 0.0f )
{
    _SetupGraphicsDevice( *m_GraphicsDevice, *m_OpenGLDebugOutput );

#ifdef VR_BACKEND_ENABLED
    vrBackend.SetGraphicsDevice( *m_GraphicsDevice );
#endif

    _CreateManagers( m_SystemInterface,
#ifdef VR_BACKEND_ENABLED
                     vrBackend,
#endif 
                     *m_GraphicsDevice,
                     m_FileManager,
                     m_AnimationManager,
                     m_MeshManager,
                     m_TextureManager,
                     m_MaterialManager,
                     m_RenderManager,
                     m_PhysicsManager,
                     m_AudioDataManager,
                     m_SoundManager );
}

//-----------------------------------------------------------------------

MainLoop::~MainLoop()
{
#ifdef VR_BACKEND_ENABLED
    // Needs to be called before destruction of the OpenGL state
    vrBackend.Shutdown();
#endif
    
    _DestroyManagers( m_AnimationManager,
                      m_MeshManager,
                      m_TextureManager,
                      m_MaterialManager,
                      m_RenderManager,
                      m_PhysicsManager,
                      m_AudioDataManager,
                      m_SoundManager );

    SafeDelete( m_OpenGLDebugOutput );
    SafeDelete( m_GraphicsDevice );
}

//-----------------------------------------------------------------------

void MainLoop::Update()
{
    const double newTimeInSeconds = m_SystemInterface.GetTicksInSeconds();
    float frameTimeInSeconds = static_cast<float>( newTimeInSeconds - m_CurrentTimeInSeconds );

    if( m_FirstUpdate )
    {
        frameTimeInSeconds = PhysicsManager::c_PhysicsUpdateRateInSeconds;
        m_FirstUpdate = false;
    }

    m_CurrentTimeInSeconds = newTimeInSeconds;

    _UpdateInputs( frameTimeInSeconds );
    _MeasureFPS( frameTimeInSeconds );
    _StepPhysics( frameTimeInSeconds );
    _UpdateOtherApplicationState( frameTimeInSeconds );
    _LastUpdateOtherApplicationState( frameTimeInSeconds );
    _Render( frameTimeInSeconds );
}

//-----------------------------------------------------------------------

void MainLoop::_UpdateInputs(float UNUSED_PARAM(frameTimeInSeconds))
{

}

//-----------------------------------------------------------------------

void MainLoop::_UpdateFixedStepApplicationState(float UNUSED_PARAM(frameTimeInSeconds))
{

}

//-----------------------------------------------------------------------

void MainLoop::_UpdateOtherApplicationState(float UNUSED_PARAM(frameTimeInSeconds))
{

}

//-----------------------------------------------------------------------

void MainLoop::_LastUpdateOtherApplicationState(float UNUSED_PARAM(frameTimeInSeconds))
{

}

//-----------------------------------------------------------------------

void MainLoop::_Render(float UNUSED_PARAM(frameTimeInSeconds))
{

}

//-----------------------------------------------------------------------

void MainLoop::_MeasureFPS(float frameTimeInSeconds)
{
    m_TimeSinceTheLastFPSMeasurementInSeconds += frameTimeInSeconds;
    m_NumOfFramesSinceTheLastFPSCheck++;

    if( m_TimeSinceTheLastFPSMeasurementInSeconds >= 1.0f ) // If one second has passed
    {
        const float millisecondsPerFrame = ( 1000.0f / static_cast<float>( m_NumOfFramesSinceTheLastFPSCheck ) );
        MsgLogger::LogEvent( "ms/Frame: " + StringUitls::NumToString( millisecondsPerFrame ) );
        m_CurrentFrameTimeInMilliseconds = millisecondsPerFrame;
        m_TimeSinceTheLastFPSMeasurementInSeconds = 0.0f;
        m_NumOfFramesSinceTheLastFPSCheck = 0;
    }
}

//-----------------------------------------------------------------------

void MainLoop::_StepPhysics(float frameTimeInSeconds)
{
    m_AccumulatedFrameTimeInSeconds += frameTimeInSeconds;

    if( m_AccumulatedFrameTimeInSeconds >= PhysicsManager::c_PhysicsUpdateRateInSeconds )
    {
        _UpdateFixedStepApplicationState( frameTimeInSeconds );

        static const float threshold = ( PhysicsManager::c_PhysicsUpdateRateInSeconds * static_cast<float>( PhysicsManager::c_MaxSimulationStepsPerSimulationCall ) );
        if( m_AccumulatedFrameTimeInSeconds > threshold ) // If rendering is very slow e.g. (c_PhysicsUpdateRateInSeconds / c_MaxSimulationStepsPerSimulationCall) or slower
        {
            // This is needed because, in the PhysicsManager bullet is set up to simulate at most 2 steps per "StepPhysicsSimulation()" call
            size_t numOfSimsToDo = static_cast<size_t>( m_AccumulatedFrameTimeInSeconds / PhysicsManager::c_PhysicsUpdateRateInSeconds );
            
            if( numOfSimsToDo > 0 )
            {
                numOfSimsToDo -= 1;
            }

            for (size_t i = 0; i < numOfSimsToDo; i++)
            {
                m_PhysicsManager->StepSimulation( PhysicsManager::c_PhysicsUpdateRateInSeconds );
            }

            m_AccumulatedFrameTimeInSeconds -= ( static_cast<float>( numOfSimsToDo ) * PhysicsManager::c_PhysicsUpdateRateInSeconds );
        }

        // In theory should always be greater than zero, but in practise due to floating point
        // inaccuracies it sometimes can become lower or equal to zero
        if( m_AccumulatedFrameTimeInSeconds > 0.0f )
        {
            m_PhysicsManager->StepSimulation( m_AccumulatedFrameTimeInSeconds );
            m_AccumulatedFrameTimeInSeconds = 0.0f;
        }
    }
}

//-----------------------------------------------------------------------

void MainLoop::_SetupGraphicsDevice(GraphicsDevice& graphicsDevice,
                                    OpenGLDebugOutput& openGLDebugOutput)
{
    openGLDebugOutput.Enable();
    //glDgb->SwitchWarningTypeOff( OpenGLDebugOutput::e_WarningTypeOther );

    /*
        Enable Z depth testing so objects closest to the viewpoint are in front of objects further away
    */
    graphicsDevice.OptionEnable( GraphicsDevice::e_OptionDepthTesting );
    graphicsDevice.SetDepthFunction( GraphicsDevice::e_DepthFunctionLess );
    graphicsDevice.EnableDepthBufferWriting();


    graphicsDevice.SetFrontFacingPolygonOrientation( GraphicsDevice::e_PolygonOrientationCounterClockWise ); // When doing Back-Face Culling assume that triangles are in counterclockwise order
    graphicsDevice.OptionEnable( GraphicsDevice::e_OptionDepthTesting ); // Enable depth testing i.e. make use of z-buffer
    graphicsDevice.OptionEnable( GraphicsDevice::e_OptionFaceCulling );
    graphicsDevice.SetFacesToCull( GraphicsDevice::e_FacesTypeBack );     // Cull back side of faces i.e. things we don't see unless we are inside a given mesh and not looking at it from outside

    graphicsDevice.SetClearColorValue( 0.0f, 0.0f, 0.0f, 1.0f );
}

//-----------------------------------------------------------------------

/**
 * Note: The below manager creation order is important
 */
void MainLoop::_CreateManagers(SystemInterface& systemInterface,
#ifdef VR_BACKEND_ENABLED
                               VRBackend& vrBackend,
#endif 
                               GraphicsDevice& graphicsDevice,
                               FileManager& fileManager,
                               AnimationManager*& animationManager,
                               MeshManager*& meshManager,
                               TextureManager*& textureManager,
                               MaterialManager*& materialManager,
                               RenderManager*& renderManager,
                               PhysicsManager*& physicsManager,
                               AudioDataManager*& audioDataManager,
                               SoundManager*& soundManager)
{
    textureManager = new TextureManager( graphicsDevice, fileManager );
    materialManager = new MaterialManager( *textureManager, fileManager );
    animationManager = new AnimationManager( graphicsDevice, systemInterface, fileManager );
    meshManager = new MeshManager( fileManager );
    renderManager = new RenderManager( graphicsDevice, 
                                       systemInterface, 
#ifdef VR_BACKEND_ENABLED
                                       vrBackend,
#endif                                        
                                       fileManager );
    physicsManager = new PhysicsManager();
    audioDataManager = new AudioDataManager( fileManager );
    soundManager = new SoundManager();
}

//-----------------------------------------------------------------------

/**
 * Note: The below manager destruction order is important,
 *       this is, because some managers depend on others
 */
void MainLoop::_DestroyManagers(AnimationManager*& animationManager,
                                MeshManager*& meshManager,
                                TextureManager*& textureManager,
                                MaterialManager*& materialManager,
                                RenderManager*& renderManager,
                                PhysicsManager*& physicsManager,
                                AudioDataManager*& audioDataManager,
                                SoundManager*& soundManager)
{
    SafeDelete( physicsManager );
    SafeDelete( renderManager );
    SafeDelete( meshManager );
    SafeDelete( animationManager );
    SafeDelete( materialManager );
    SafeDelete( textureManager );
    SafeDelete( soundManager );
    SafeDelete( audioDataManager );
}

//-----------------------------------------------------------------------
