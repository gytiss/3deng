#ifndef PHYSICSMANAGER_H_INCLUDED
#define PHYSICSMANAGER_H_INCLUDED

#include <list>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <bullet/LinearMath/btAlignedObjectArray.h>
#include <bullet/LinearMath/btDefaultMotionState.h>
#include <Scene/SceneTypes.h>
#include <Resources/MeshManager.h>
#include "PhysicsMotionStateInterface.h"

class btDefaultCollisionConfiguration;
class btCollisionDispatcher;
class btBroadphaseInterface;
class btSequentialImpulseConstraintSolver;
class btDiscreteDynamicsWorld;
class btCollisionShape;
class btRigidBody;
class btCollisionObject;
class btEmptyShape;
class btTransform;
class btCompoundShape;
class btStridingMeshInterface;
class btActionInterface;
class PhysicsDebugDataDrawer;
class PhysicsCharacterController;
class BulletCharacterController;
class RenderableObject;
class GameObject;

class PhysicsManager
{
private:
    struct RigidBodyWorldState
    {
        btScalar mass;
        btVector3 localInertia;
        bool currentlyInThePhysicsWorld;
    };

    struct ColliderWorldState
    {
        btVector3 centerPositionOffset;
        bool currentlyInThePhysicsWorld;
    };

    typedef std::list<btCollisionShape*> CollisionShapesList;
    typedef std::list<ColliderWorldState> ColliderWorldStateList;  // true - collider is in the physics world, false - is out of the physics world
    typedef std::list<RigidBodyWorldState> RigidBodyWorldStateList;

public:
    friend class PhysicsCharacterController;
    friend class BulletCharacterController;

    class RigidBodyHandle
    {
    public:
        friend class PhysicsManager;

        RigidBodyHandle()
        : m_RigidBody( nullptr ),
          m_RigidBodyWorldStateListIt(),
          m_HasBeenInitialised( false )
        {
        }

        RigidBodyHandle& operator= (const RigidBodyHandle& other)
        {
            m_RigidBody = other.m_RigidBody;
            m_RigidBodyWorldStateListIt = other.m_RigidBodyWorldStateListIt;
            m_HasBeenInitialised = other.m_HasBeenInitialised;
            return *this;
        }

        RigidBodyHandle(const RigidBodyHandle& other)
        {
            operator= ( other );
        }

        inline bool IsValid() const
        {
            return m_HasBeenInitialised;
        }

    private:
        inline void Invalidate()
        {
            m_HasBeenInitialised = false;
        }

        /**
         * @param body - Bullet rigid body
         * @param mass - Mass of the rigid body
         */
        RigidBodyHandle(btRigidBody* body, RigidBodyWorldStateList::iterator it)
        : m_RigidBody( body ),
          m_RigidBodyWorldStateListIt( it ),
          m_HasBeenInitialised( true )
        {
        }

        btRigidBody* m_RigidBody;
        RigidBodyWorldStateList::iterator m_RigidBodyWorldStateListIt;
        bool m_HasBeenInitialised;
    };

    class CollisionShapeHandle
    {
    public:
        friend class PhysicsManager;

        CollisionShapeHandle()
        : m_CollisionShapeListIt(),
          m_HasBeenInitialised( false )
        {
        }

        CollisionShapeHandle& operator= (const CollisionShapeHandle& other)
        {
            m_CollisionShapeListIt = other.m_CollisionShapeListIt;
            m_HasBeenInitialised = other.m_HasBeenInitialised;

            return *this;
        }

        CollisionShapeHandle(const CollisionShapeHandle& other)
        {
            operator= ( other );
        }

        inline bool IsValid() const
        {
            return m_HasBeenInitialised;
        }

    private:
        inline void Invalidate()
        {
            m_HasBeenInitialised = false;
        }

        /**
         * @param collisionShapeListIt - Iterator pointing to the collision shape assigned to this rigid body
         */
        CollisionShapeHandle(CollisionShapesList::iterator collisionShapeListIt)
        : m_CollisionShapeListIt( collisionShapeListIt ),
          m_HasBeenInitialised( true )
        {
        }

        CollisionShapesList::iterator m_CollisionShapeListIt;
        bool m_HasBeenInitialised;
    };

    class ColliderHandle
    {
    public:
        friend class PhysicsManager;

        ColliderHandle()
        : m_CollisionObject( nullptr ),
          m_ColliderWorldStateListIt(),
          m_HasBeenInitialised( false )
        {
        }

        ColliderHandle& operator= (const ColliderHandle& other)
        {
            m_CollisionObject = other.m_CollisionObject;
            m_ColliderWorldStateListIt = other.m_ColliderWorldStateListIt;
            m_HasBeenInitialised = other.m_HasBeenInitialised;

            return *this;
        }

        ColliderHandle(const ColliderHandle& other)
        {
            operator= ( other );
        }

        inline bool IsValid() const
        {
            return m_HasBeenInitialised;
        }

    private:
        inline void Invalidate()
        {
            m_HasBeenInitialised = false;
        }

        /**
         * @param collisionObject - Bullet collision object
         */
        ColliderHandle(btCollisionObject* collisionObject, ColliderWorldStateList::iterator it)
        : m_CollisionObject( collisionObject ),
          m_ColliderWorldStateListIt( it ),
          m_HasBeenInitialised( true )
        {
        }

        btCollisionObject* m_CollisionObject;
        ColliderWorldStateList::iterator m_ColliderWorldStateListIt;
        bool m_HasBeenInitialised;
    };

    typedef std::vector<RigidBodyHandle> RigidBodyHandleList;
    typedef std::vector<ColliderHandle> ColliderHandleList;

    PhysicsManager();
    ~PhysicsManager();

    void StepSimulation(float deltaTime);

    CollisionShapeHandle CreateStaticInfinitePlaneCollisionShape();
    CollisionShapeHandle CreateCapsuleXCollisionShape(float radius, float height);
    CollisionShapeHandle CreateCapsuleYCollisionShape(float radius, float height);
    CollisionShapeHandle CreateCapsuleZCollisionShape(float radius, float height);
    CollisionShapeHandle CreateConeXCollisionShape(float radius, float height);
    CollisionShapeHandle CreateConeYCollisionShape(float radius, float height);
    CollisionShapeHandle CreateConeZCollisionShape(float radius, float height);
    CollisionShapeHandle CreateBoxCollisionShape(const glm::vec3& dimensions);
    CollisionShapeHandle CreateSphereCollisionShape(float radius);
    CollisionShapeHandle CreateMeshCollisionShape(const MeshManager::MeshHandle& meshHandle);
    void DestroyCollisionShape(CollisionShapeHandle& shape);

    ColliderHandle CreateCollider();
    void DestroyCollider(ColliderHandle& collider);

    void AddColliderToTheWorld(ColliderHandle collider);
    void RemoveColliderFromTheWorld(ColliderHandle collider);

    void SetCollidersCollisionShape(ColliderHandle& collider, CollisionShapeHandle& collisionShape);

    RigidBodyHandle CreateRigidBody(bool kinematic, float mass, PhysicsMotionStateInterface* motionState);
    void DestroyRigidBody(RigidBodyHandle& body);

    void AddRigidBodyToTheWorld(RigidBodyHandle body);
    void RemoveRigidBodyFromTheWorld(RigidBodyHandle body);

    void AttachCollidersToARigidBody(RigidBodyHandle& body, ColliderHandleList& colliders);
    void DetachCollidersFromARigidBody(RigidBodyHandle& body);

    void AddAction(btActionInterface *action);

    void SwitchRigidBodyToKinematicMode(RigidBodyHandle& body);
    void SwitchRigidBodyToDynamicMode(RigidBodyHandle& body);

    bool IsRigidBodyKinematic(RigidBodyHandle& body);

    const RenderableObject* GetVisualDebugDataRenderable();
    void RefreshDebugDataMesh();

    /**
     * Cast a ray and returns the first entity hit by it. Note: Only entities which have physics property set will be considered during calculation
     *
     * @param rayOrigin - Point of origin of the ray in world coordinates
     * @param rayDirection - Direction of the ray in world coordinates
     * @param outGameObject - The first GameObject hit by this way (outGameObject is set to null in cases where ray does not hit anything)
     */
    void CastARay(const glm::vec3& rayOrigin, const glm::vec3& rayDirection, GameObject*& outGameObject);

    void SetRigidBodyMass(RigidBodyHandle& body, float mass);
    float GetRigidBodyMass(const RigidBodyHandle& body) const;

    /**
     * @param body - A rigid body to apply the drag to
     * @param drag - How much air resistance affects the object when moving from forces. 0 means no air resistance.
     */
    void SetRigidBodyLinearDrag(RigidBodyHandle& body, float drag);

    float GetRigidBodyLinearDrag(const RigidBodyHandle& body) const;

    /**
     * @param body - A rigid body to apply the drag to
     * @param drag - How much air resistance affects the object when rotating from torque. 0 means no air resistance.
     */
    void SetRigidBodyAngularDrag(RigidBodyHandle& body, float drag);

    float GetRigidBodyAngularDrag(const RigidBodyHandle& body) const;

    /**
     * Sets a given rigid body position axis as frozen (i.e. position value of the rigid body is not allowed to change on them) or not
     *
     * @param body - A rigid body to add/remove restrictions to/from
     * @param freezeAxis[3] - An array of boolean values representing X, Y and Z axis, each value in the array is
     *                        either set to true if a given rigid body is not allowed to change position
     *                        (i.e. move) along the X, Y or Z axis respectively or false if not
     */
    void SetRigidBodyPositionAxisFrozen(const RigidBodyHandle& body,
                                        const bool freezeAxis[3]);

    void GetRigidBodyPositionAxisFrozen(const RigidBodyHandle& body,
                                        bool outFreezeAxis[3]) const;

    /**
     * Sets a given rigid body rotation axis as frozen (i.e. rotation value of the rigid body is not allowed to change on them) or not
     *
     * @param body - A rigid body to add/remove restrictions to/from
     * @param freezeAxis[3] - An array of boolean values representing X, Y and Z axis, each value in the array is
     *                        either set to true if a given rigid body is not allowed to rotate around the
     *                        X, Y or Z axis respectively or false if not
     */
    void SetRigidBodyRotationAxisFrozen(const RigidBodyHandle& body,
                                        const bool freezeAxis[3]);

    void GetRigidBodyRotationAxisFrozen(const RigidBodyHandle& body,
                                        bool outFreezeAxis[3]) const;

    static float GetCapsuleCollisionShapeRadius(const CollisionShapeHandle& shape);
    static float GetCapsuleCollisionShapeHeight(const CollisionShapeHandle& shape);

    static float GetConeCollisionShapeRadius(const CollisionShapeHandle& shape);
    static float GetConeCollisionShapeHeight(const CollisionShapeHandle& shape);

    static glm::vec3 GetBoxCollisionShapeSize(const CollisionShapeHandle& shape);
    static float GetSphereCollisionShapeRadius(const CollisionShapeHandle& shape);

    static glm::vec3 GetRigidBodyPosition(const RigidBodyHandle& body);
    static glm::quat GetRigidBodyRotation(const RigidBodyHandle& body);
    static glm::mat4 GetRigidBodyFullTransform(const RigidBodyHandle& body);
    static void SetRigidBodyUserPointer(RigidBodyHandle& body, void* userPointer);
    static void* GetRigidBodyUserPointer(RigidBodyHandle& body);

    static void SetColliderCenterPositionOffset(ColliderHandle& collider, const glm::vec3& centerPosition);
    static void SetColliderTransform(ColliderHandle& collider, const SceneTypes::Transform& t);
    static void SetColliderPosition(ColliderHandle& collider, const glm::vec3& position);
    static void SetColliderRotation(ColliderHandle& collider, const glm::quat& rotation);
    static glm::vec3 GetColliderCenterPositionOffset(const ColliderHandle& collider);
    static glm::vec3 GetColliderPosition(const ColliderHandle& collider);
    static glm::quat GetColliderRotation(const ColliderHandle& collider);
    static glm::mat4 GetColliderFullTransform(const ColliderHandle& collider);
    static void SetColliderUserPointer(ColliderHandle& collider, void* userPointer);
    static void* GetColliderUserPointer(ColliderHandle& collider);

    static const float c_PhysicsUpdateRateInSeconds;
    static const int c_MaxSimulationStepsPerSimulationCall;

private:
    // Used to prevent copying
    PhysicsManager& operator = (const PhysicsManager& other);
    PhysicsManager(const PhysicsManager& other);

    static void _SetBulletColliderTransformWithoutCenterPositionOffset(ColliderHandle& collider, const btTransform& tansform);
    static void _GetBulletColliderTransformWithoutCenterPositionOffset(const ColliderHandle& collider, btTransform& tansform);
    void _DestroyBulletCollisionShape(btCollisionShape* collisionShape);
    void _RecalculateLocalInertiaOfABulletRigidBody(const RigidBodyHandle& body);
    void _CalculateLocalInertiaOfABulletCompoundCollisionShape(btCompoundShape* compoundCollisionShape,
                                                               float totalObjectMass,
                                                               btTransform& outprincipalTransform,
                                                               btVector3& outLocalInertia);

    static void _GetRigidBodyFullTransform(const RigidBodyHandle& body, btTransform& transform);
    static void _SetColliderFullTransform(ColliderHandle& collider, const btTransform& transform);
    static const btTransform& _GetColliderFullTransform(const ColliderHandle& collider);
    bool _CollidersCenterPositionOffsetSet(const ColliderHandle& collider) const;

    btDefaultCollisionConfiguration* m_CollisionConf;
    btCollisionDispatcher* m_Dispatcher;
    btBroadphaseInterface* m_OverlappingPairCache;
    btSequentialImpulseConstraintSolver* m_Solver;
    btDiscreteDynamicsWorld* m_DynamicsWorld;
    btDefaultMotionState m_DefaultMotionState;
    CollisionShapesList m_CollisionShapes;
    PhysicsDebugDataDrawer* m_DebugDataDrawer;
    btEmptyShape* m_EmptyCollisionShape;

    ColliderWorldStateList m_ColliderWorldStateList;
    RigidBodyWorldStateList m_RigidBodyWorldStateList;
};

#endif // PHYSICSMANAGER_H_INCLUDED
