/**
 * Class for converting raw input range values to sensitivity-calibrated range values
 */

#ifndef RANGECONVERTER_H_INCLUDED
#define RANGECONVERTER_H_INCLUDED

#include <map>
#include "InputConstants.h"

class FileManager;

namespace InputMapping
{
	class RangeConverter
	{
    public:
		explicit RangeConverter(const std::string& configFileName, FileManager& fileManager);

        // Conversion interface
		template <typename RangeType>
		RangeType Convert(Range rangeid, RangeType invalue) const
		{
			ConversionMap_t::const_iterator iter = m_ConversionMap.find(rangeid);
			if(iter == m_ConversionMap.end())
				return invalue;

			return iter->second.Convert<RangeType>(invalue);
		}

	private:
		struct Converter
		{
			double MinimumInput;
			double MaximumInput;

			double MinimumOutput;
			double MaximumOutput;

			template <typename RangeType>
			RangeType Convert(RangeType invalue) const
			{
				double v = static_cast<double>( invalue );
				if(v < MinimumInput)
                {
					v = MinimumInput;
                }
				else if(v > MaximumInput)
                {
					v = MaximumInput;
                }

				double interpolationfactor = ( v - MinimumInput ) / ( MaximumInput - MinimumInput );
				return static_cast<RangeType>( ( interpolationfactor * ( MaximumOutput - MinimumOutput ) ) + MinimumOutput );
			}
		};

		typedef std::map<Range, Converter> ConversionMap_t;
		ConversionMap_t m_ConversionMap;
		static const std::string c_MandatoryRangesTableMembers[4];
	};
}

#endif // RANGECONVERTER_H_INCLUDED
