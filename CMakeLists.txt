# This will add arguments not found in ${parameter} to the end.  It
# does not attempt to remove duplicate arguments already existing in
# ${parameter}.
MACRO(FORCE_ADD_FLAGS parameter)
  # Create a separated list of the arguments to loop over
  SET(p_list ${${parameter}})
  SEPARATE_ARGUMENTS(p_list)
  # Make a copy of the current arguments in ${parameter}
  SET(new_parameter ${${parameter}})
  # Now loop over each required argument and see if it is in our
  # current list of arguments.
  FOREACH(required_arg ${ARGN})
    # This helps when we get arguments to the function that are
    # grouped as a string:
    #
    # ["-O3 -g"]  instead of [-O3 -g]
    SET(TMP ${required_arg}) #elsewise the Seperate command doesn't work)
    SEPARATE_ARGUMENTS(TMP)
    FOREACH(option ${TMP})
      # Look for the required argument in our list of existing arguments
      SET(found FALSE)
      FOREACH(p_arg ${p_list})
        IF (${p_arg} STREQUAL ${option})
          SET(found TRUE)
        ENDIF (${p_arg} STREQUAL ${option})
      ENDFOREACH(p_arg)
      IF(NOT found)
        # The required argument wasn't found, so we need to add it in.
        SET(new_parameter "${new_parameter} ${option}")
      ENDIF(NOT found)
    ENDFOREACH(option ${TMP})
  ENDFOREACH(required_arg ${ARGN})
  SET(${parameter} ${new_parameter} CACHE STRING "" FORCE)
ENDMACRO(FORCE_ADD_FLAGS)



##################################################################
# Eclipse can not parse errors spanning multiple lines. The below
# makes sure that GCC generates errors on a single line
##################################################################
if (CMAKE_COMPILER_IS_GNUCC)
    #SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fmessage-length=0")
    FORCE_ADD_FLAGS(${CMAKE_C_FLAGS} -fmessage-length=0)
endif (CMAKE_COMPILER_IS_GNUCC)
if (CMAKE_COMPILER_IS_GNUCXX)
    #SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fmessage-length=0")
    FORCE_ADD_FLAGS(${CMAKE_CXX_FLAGS} -fmessage-length=0)
endif (CMAKE_COMPILER_IS_GNUCXX)
##################################################################

##################################################################
# Make sure GCC commands are printed in the console. This allows
# Eclipse to discover and note down all of the include directories
# during the first build by parsing "-I" compiler arguments.
##################################################################
set(CMAKE_VERBOSE_MAKEFILE true)
##################################################################

project (TNK)

cmake_minimum_required (VERSION 2.8.8)



################################################################################
# Configuration options
################################################################################

option( ENABLE_VR_SUPPORT_IN_MAIN_APP "Enable VR HMDs support in the MainApp" 0)
option( BUILD_MAIN_APP "Build MainApp" 1 )
option( BUILD_EDITOR "Build Editor" 1 )

################################################################################

set_property(GLOBAL PROPERTY TARGET_SUPPORTS_SHARED_LIBS TRUE)

if( ANDROID )
    unset( BUILD_EDITOR )
endif()


set (CMAKE_PREFIX_PATH "/usr/lib/x86_64-linux-gnu/cmake/") # Needed for Qt5
set (CMAKE_BUILD_TYPE Release CACHE STRING "Build configuration (Debug, Release, RelWithDebInfo, MinSizeRel)")
#set (CMAKE_C_COMPILER gcc)
#set (CMAKE_CXX_COMPILER g++)

set (TARGET_MAIN_APP "MainApp")
set (TARGET_EDITOR "Editor")

set(EXTRA_CXX_DEFINES ${EXTRA_CXX_DEFINES} "-DGLM_FORCE_RADIANS") # Force GLM to use radians for all angle inputs and outputs

if ( NOT MSVC )
    set(EXTRA_CXX_FLAGS "-std=c++11;-Wall;-Wextra;-Wno-unused-local-typedefs;-Wno-deprecated-declarations;-D_REENTRANT")

    if( NOT ANDROID )
        set(EXTRA_CXX_FLAGS ${EXTRA_CXX_FLAGS} "-m64;-rdynamic")
    endif()
endif()

if( APPLE )
    set(EXTRA_CXX_DEFINES ${EXTRA_CXX_DEFINES} "-DOS_APPLE")
elseif( ANDROID )
    set(EXTRA_CXX_DEFINES ${EXTRA_CXX_DEFINES} "-DOS_ANDROID")
    set(EXTRA_CXX_DEFINES ${EXTRA_CXX_DEFINES} "-D__ANDROID__")
    set(EXTRA_CXX_DEFINES ${EXTRA_CXX_DEFINES} "-DOPENGL_ES")
elseif( UNIX )
    set(EXTRA_CXX_DEFINES ${EXTRA_CXX_DEFINES} "-DOS_GENERIC_UNIX")
elseif( WIN32 )
    set(EXTRA_CXX_DEFINES ${EXTRA_CXX_DEFINES} "-DOS_WINDOWS")
    if ( MSVC )
        # This is define is needed, because when compiling C++ code 
        # Visual Studio does not define math constants by default
        set(EXTRA_CXX_DEFINES ${EXTRA_CXX_DEFINES} "-D_USE_MATH_DEFINES")   
    endif()
    set(ENABLE_VR_SUPPORT_IN_MAIN_APP 1)
else()
    message( FATAL_ERROR "Unsupported platform" )
endif()

if ( ENABLE_VR_SUPPORT_IN_MAIN_APP )
    set(EXTRA_CXX_DEFINES ${EXTRA_CXX_DEFINES} "-DVR_BACKEND_ENABLED")
endif()

if (BUILD_EDITOR)
    find_package(Qt5Core)
    find_package(Qt5Widgets)
    find_package(Qt5Gui)
    find_package(Qt5OpenGL)
    
    set(EXTRA_CXX_FLAGS ${EXTRA_CXX_FLAGS} ${Qt5Core_EXECUTABLE_COMPILE_FLAGS})
    set(EXTRA_CXX_FLAGS ${EXTRA_CXX_FLAGS} ${Qt5Core_DEFINITIONS})
    
    set(EXTRA_CXX_FLAGS ${EXTRA_CXX_FLAGS} ${Qt5Widgets_EXECUTABLE_COMPILE_FLAGS})
    set(EXTRA_CXX_FLAGS ${EXTRA_CXX_FLAGS} ${Qt5Widgets_DEFINITIONS})
    
    set(EXTRA_CXX_FLAGS ${EXTRA_CXX_FLAGS} ${Qt5Gui_EXECUTABLE_COMPILE_FLAGS})
    set(EXTRA_CXX_FLAGS ${EXTRA_CXX_FLAGS} ${Qt5Gui_DEFINITIONS})
    
    set(EXTRA_CXX_FLAGS ${EXTRA_CXX_FLAGS} ${Qt5OpenGL_EXECUTABLE_COMPILE_FLAGS})
    set(EXTRA_CXX_FLAGS ${EXTRA_CXX_FLAGS} ${Qt5OpenGL_DEFINITIONS})
endif()

if (CMAKE_BUILD_TYPE STREQUAL "Release")
    if (NOT MSVC)
        set(EXTRA_CXX_FLAGS "${EXTRA_CXX_FLAGS};-O3")
    endif()
else()
    if (NOT MSVC)
        set(EXTRA_CXX_FLAGS "${EXTRA_CXX_FLAGS};-O0;-g;")
    endif()
    set(EXTRA_CXX_DEFINES ${EXTRA_CXX_DEFINES} "DEBUG_BUILD")
endif()

# The version number.
set (${TARGET_MAIN_APP}_VERSION_MAJOR 1)
set (${TARGET_MAIN_APP}_VERSION_MINOR 0)
set (${TARGET_EDITOR}_VERSION_MAJOR 1)
set (${TARGET_EDITOR}_VERSION_MINOR 0)

MACRO(ADD_MAIN_APP_SOURCE targetSource)
    set (TARGET_MAIN_APP_SOURCES "${TARGET_MAIN_APP_SOURCES};${PROJECT_SOURCE_DIR}/src/MainApp/${targetSource}")
ENDMACRO(ADD_MAIN_APP_SOURCE)

MACRO(ADD_EDITOR_SOURCE targetSource)
    set (TARGET_EDITOR_SOURCES "${TARGET_EDITOR_SOURCES};${PROJECT_SOURCE_DIR}/src/Editor/${targetSource}")
ENDMACRO(ADD_EDITOR_SOURCE)

MACRO(ADD_COMMON_INCLUDE_DIR parameter)
    set (EXTRA_COMMON_INCLUDE_DIRS ${EXTRA_COMMON_INCLUDE_DIRS} ${parameter})
ENDMACRO(ADD_COMMON_INCLUDE_DIR)

MACRO(ADD_MAIN_APP_INCLUDE_DIR parameter)
    set (EXTRA_MAIN_APP_INCLUDE_DIRS "${EXTRA_MAIN_APP_INCLUDE_DIRS};${parameter}")
ENDMACRO(ADD_MAIN_APP_INCLUDE_DIR)

MACRO(ADD_EDITOR_INCLUDE_DIR parameter)
    set (EXTRA_EDITOR_INCLUDE_DIRS "${EXTRA_EDITOR_INCLUDE_DIRS};${parameter}")
ENDMACRO(ADD_EDITOR_INCLUDE_DIR)

set(ENGINE_DIR "${PROJECT_SOURCE_DIR}/src/Engine")

if( ANDROID )
    set (LIB_OS "android")
    set (LIB_ARCH "${ANDROID_BUILD_ARCHITECTURE}")
elseif ( WIN32 )
    set (LIB_OS "windows")
    set (LIB_ARCH "")
elseif ( APPLE )
    set (LIB_OS "osx")
    set (LIB_ARCH "")
else()
    set (LIB_OS "linux")
    set (LIB_ARCH "")
endif()

ADD_COMMON_INCLUDE_DIR("${PROJECT_SOURCE_DIR}/thirdPartyLibs/${LIB_OS}/libGLEW/${LIB_ARCH}/include")
ADD_COMMON_INCLUDE_DIR("${PROJECT_SOURCE_DIR}/thirdPartyLibs/${LIB_OS}/libfreetype/${LIB_ARCH}/include/freetype2")
ADD_COMMON_INCLUDE_DIR("${PROJECT_SOURCE_DIR}/thirdPartyLibs/${LIB_OS}/libSDL2/${LIB_ARCH}/include/")
ADD_COMMON_INCLUDE_DIR("${PROJECT_SOURCE_DIR}/thirdPartyLibs/${LIB_OS}/libSDL2/${LIB_ARCH}/include/SDL2")
ADD_COMMON_INCLUDE_DIR("${PROJECT_SOURCE_DIR}/thirdPartyLibs/${LIB_OS}/liblua/${LIB_ARCH}/include")
ADD_COMMON_INCLUDE_DIR("${PROJECT_SOURCE_DIR}/thirdPartyLibs/${LIB_OS}/libluabind/${LIB_ARCH}/include")
ADD_COMMON_INCLUDE_DIR("${PROJECT_SOURCE_DIR}/thirdPartyLibs/${LIB_OS}/libboost/${LIB_ARCH}/include")
ADD_COMMON_INCLUDE_DIR("${PROJECT_SOURCE_DIR}/thirdPartyLibs/${LIB_OS}/libBullet/${LIB_ARCH}/include")
ADD_COMMON_INCLUDE_DIR("${PROJECT_SOURCE_DIR}/thirdPartyLibs/${LIB_OS}/libBullet/${LIB_ARCH}/include/bullet")
ADD_COMMON_INCLUDE_DIR("${PROJECT_SOURCE_DIR}/thirdPartyLibs/${LIB_OS}/libRocket/${LIB_ARCH}/include")
ADD_COMMON_INCLUDE_DIR("${PROJECT_SOURCE_DIR}/thirdPartyLibs/${LIB_OS}/libTremor/${LIB_ARCH}/include")
ADD_COMMON_INCLUDE_DIR("${PROJECT_SOURCE_DIR}/thirdPartyLibs/${LIB_OS}/libOgg/${LIB_ARCH}/include")
ADD_COMMON_INCLUDE_DIR("${PROJECT_SOURCE_DIR}/thirdPartyLibs/${LIB_OS}/libOpenAL/${LIB_ARCH}/include")
ADD_COMMON_INCLUDE_DIR("${PROJECT_SOURCE_DIR}/thirdPartyLibs/${LIB_OS}/libOpenVR/${LIB_ARCH}/include")
ADD_COMMON_INCLUDE_DIR("${PROJECT_SOURCE_DIR}/thirdPartyLibs/glm")
ADD_COMMON_INCLUDE_DIR("${PROJECT_SOURCE_DIR}/src/")
if ( WIN32 )
    include_directories ("${PROJECT_SOURCE_DIR}/thirdPartyLibs/sources/OpenGLInclues")
endif()

ADD_COMMON_INCLUDE_DIR(${ENGINE_DIR})
add_subdirectory(${ENGINE_DIR})
add_definitions(-DSTATIC_LIB)

if (BUILD_MAIN_APP)
    set (EXTRA_MAIN_APP_INCLUDE_DIRS ${EXTRA_MAIN_APP_INCLUDE_DIRS} ${EXTRA_COMMON_INCLUDE_DIRS})
    ADD_MAIN_APP_INCLUDE_DIR("${PROJECT_SOURCE_DIR}/src/MainApp")
endif()

if (BUILD_EDITOR)
    set (EXTRA_EDITOR_INCLUDE_DIRS ${EXTRA_EDITOR_INCLUDE_DIRS} ${EXTRA_COMMON_INCLUDE_DIRS})
    ADD_EDITOR_INCLUDE_DIR("${PROJECT_SOURCE_DIR}/src/Editor")
    ADD_EDITOR_INCLUDE_DIR(${Qt5Core_INCLUDE_DIRS})
    ADD_EDITOR_INCLUDE_DIR(${Qt5Widgets_INCLUDE_DIRS})
    ADD_EDITOR_INCLUDE_DIR(${Qt5Gui_INCLUDE_DIRS})
    ADD_EDITOR_INCLUDE_DIR(${Qt5OpenGL_INCLUDE_DIRS})
    
    set (EDITOR_QT5_UI_SOURCE_FILE ${EDITOR_QT5_UI_SOURCE_FILE} "${PROJECT_SOURCE_DIR}/src/Editor/GUI/MainWindow.ui")
    set (EDITOR_QT5_UI_SOURCE_FILE ${EDITOR_QT5_UI_SOURCE_FILE} "${PROJECT_SOURCE_DIR}/src/Editor/GUI/TransformComponentUI.ui")
    set (EDITOR_QT5_UI_SOURCE_FILE ${EDITOR_QT5_UI_SOURCE_FILE} "${PROJECT_SOURCE_DIR}/src/Editor/GUI/RigidBodyComponentUI.ui")
    set (EDITOR_QT5_UI_SOURCE_FILE ${EDITOR_QT5_UI_SOURCE_FILE} "${PROJECT_SOURCE_DIR}/src/Editor/GUI/CapsuleColliderComponentUI.ui")
    set (EDITOR_QT5_UI_SOURCE_FILE ${EDITOR_QT5_UI_SOURCE_FILE} "${PROJECT_SOURCE_DIR}/src/Editor/GUI/ConeColliderComponentUI.ui")
    set (EDITOR_QT5_UI_SOURCE_FILE ${EDITOR_QT5_UI_SOURCE_FILE} "${PROJECT_SOURCE_DIR}/src/Editor/GUI/BoxColliderComponentUI.ui")
    set (EDITOR_QT5_UI_SOURCE_FILE ${EDITOR_QT5_UI_SOURCE_FILE} "${PROJECT_SOURCE_DIR}/src/Editor/GUI/SphereColliderComponentUI.ui")
    qt5_wrap_ui(EDITOR_QT5_UIC_OUT_HEADER_FILES ${EDITOR_QT5_UI_SOURCE_FILE})
    set (EXTRA_EDITOR_INCLUDE_DIRS ${EXTRA_EDITOR_INCLUDE_DIRS} ${CMAKE_CURRENT_BINARY_DIR}) # Add a path to includes generated by the qt5_wrap_ui()
endif()

if (BUILD_MAIN_APP)
    if( ANDROID )
        set (TARGET_MAIN_APP_SOURCES "${TARGET_MAIN_APP_SOURCES};${PROJECT_SOURCE_DIR}/thirdPartyLibs/sources/SDL2-2.0.5/src/main/android/SDL_android_main.c")
    endif()
    ADD_MAIN_APP_SOURCE("GameLogic/AK47.cpp")
    ADD_MAIN_APP_SOURCE("GameLogic/AK47.h")
    ADD_MAIN_APP_SOURCE("GameLogic/GameObjectTypes.h")
    ADD_MAIN_APP_SOURCE("GameLogic/ShootingRangeScene.cpp")
    ADD_MAIN_APP_SOURCE("GameLogic/ShootingRangeScene.h")
    ADD_MAIN_APP_SOURCE("GameLogic/ShootingTarget.cpp")
    ADD_MAIN_APP_SOURCE("GameLogic/ShootingTarget.h")
    ADD_MAIN_APP_SOURCE("GUI/FireButton.cpp")
    ADD_MAIN_APP_SOURCE("GUI/FireButton.h")
    ADD_MAIN_APP_SOURCE("GUI/VirtualJoystick.cpp")
    ADD_MAIN_APP_SOURCE("GUI/VirtualJoystick.h")
    ADD_MAIN_APP_SOURCE("WindowManager.cpp")
    ADD_MAIN_APP_SOURCE("WindowManager.h")
    ADD_MAIN_APP_SOURCE("MainAppLogic.cpp")
    ADD_MAIN_APP_SOURCE("MainAppLogic.h")
    ADD_MAIN_APP_SOURCE("MainAppMainLoop.cpp")
    ADD_MAIN_APP_SOURCE("MainAppMainLoop.h")
    ADD_MAIN_APP_SOURCE("MainConfig.cpp")
    ADD_MAIN_APP_SOURCE("MainConfig.h")
    ADD_MAIN_APP_SOURCE("main.cpp")
endif()

if (BUILD_EDITOR)
    ADD_EDITOR_SOURCE("EditorLogic.h")
    ADD_EDITOR_SOURCE("EditorLogic.cpp")
    ADD_EDITOR_SOURCE("EditorMainLoop.h")
    ADD_EDITOR_SOURCE("EditorMainLoop.cpp")
    ADD_EDITOR_SOURCE("EditorObjectIDs.h")
    ADD_EDITOR_SOURCE("main.cpp")
    ADD_EDITOR_SOURCE("MainWindow.h")
    ADD_EDITOR_SOURCE("MainWindow.cpp")
    ADD_EDITOR_SOURCE("GLWidget.h")
    ADD_EDITOR_SOURCE("GLWidget.cpp")
    ADD_EDITOR_SOURCE("Tools/ManipulatorTool.h")
    ADD_EDITOR_SOURCE("Tools/ManipulatorTool.cpp")
    ADD_EDITOR_SOURCE("Tools/MoveManipulator.h")
    ADD_EDITOR_SOURCE("Tools/MoveManipulator.cpp")
    ADD_EDITOR_SOURCE("GUI/BoxColliderComponentWidget.h")
    ADD_EDITOR_SOURCE("GUI/BoxColliderComponentWidget.cpp")
    ADD_EDITOR_SOURCE("GUI/CapsuleColliderComponentWidget.h")
    ADD_EDITOR_SOURCE("GUI/CapsuleColliderComponentWidget.cpp")
    ADD_EDITOR_SOURCE("GUI/CategorizedListWidget.h")
    ADD_EDITOR_SOURCE("GUI/CategorizedListWidget.cpp")
    ADD_EDITOR_SOURCE("GUI/ColliderWidgetBase.h")
    ADD_EDITOR_SOURCE("GUI/ColliderWidgetBaseQObjectParent.h")
    ADD_EDITOR_SOURCE("GUI/ColliderWidgetBaseQObjectParent.cpp")
    ADD_EDITOR_SOURCE("GUI/ComponentFloatValueValidator.h")
    ADD_EDITOR_SOURCE("GUI/ComponentFloatValueValidator.cpp")
    ADD_EDITOR_SOURCE("GUI/ComponentWidget.h")
    ADD_EDITOR_SOURCE("GUI/ComponentWidget.cpp")
    ADD_EDITOR_SOURCE("GUI/ConeColliderComponentWidget.h")
    ADD_EDITOR_SOURCE("GUI/ConeColliderComponentWidget.cpp")
    ADD_EDITOR_SOURCE("GUI/SphereColliderComponentWidget.h")
    ADD_EDITOR_SOURCE("GUI/SphereColliderComponentWidget.cpp")
    ADD_EDITOR_SOURCE("GUI/ToolsPanel.h")
    ADD_EDITOR_SOURCE("GUI/ToolsPanel.cpp")
    ADD_EDITOR_SOURCE("GUI/GUIDefaults.h")
    ADD_EDITOR_SOURCE("GUI/GUIDefaults.cpp")
    ADD_EDITOR_SOURCE("GUI/SheetDelegate.h")
    ADD_EDITOR_SOURCE("GUI/SheetDelegate.cpp")
    ADD_EDITOR_SOURCE("GUI/GUIHelpers.h")
    ADD_EDITOR_SOURCE("GUI/GUIHelpers.cpp")
    ADD_EDITOR_SOURCE("GUI/RigidBodyComponentWidget.h")
    ADD_EDITOR_SOURCE("GUI/RigidBodyComponentWidget.cpp")
    ADD_EDITOR_SOURCE("GUI/TransformComponentWidget.h")
    ADD_EDITOR_SOURCE("GUI/TransformComponentWidget.cpp")
endif()

set (LIB_ROOT_DIR "${PROJECT_SOURCE_DIR}/thirdPartyLibs/${LIB_OS}")

# MACRO(ADD_COMMON_LIBS libDir releaseLib debugLib)
    # set (EXTRA_COMMON_LIBS "${EXTRA_COMMON_LIBS};debug ${libDir}/${debugLib} optimized ${libDir}/${releaseLib}")
# ENDMACRO(ADD_COMMON_LIBS)

set (EXTRA_COMMON_LIBS "")
function(ADD_COMMON_LIBS libDir releaseLib debugLib)
    set (EXTRA_COMMON_LIBS ${EXTRA_COMMON_LIBS} debug ${libDir}${debugLib} optimized ${libDir}${releaseLib} PARENT_SCOPE)
endfunction()

function(ADD_COMMON_LIBS_UNIX parameter)
    set (EXTRA_COMMON_LIBS ${EXTRA_COMMON_LIBS} debug ${parameter} optimized ${parameter} PARENT_SCOPE)
endfunction()

MACRO(ADD_EDITOR_LIB_PARAM parameter)
    set (EXTRA_EDITOR_LIBS "${EXTRA_EDITOR_LIBS};${parameter}")
ENDMACRO(ADD_EDITOR_LIB_PARAM)

if ( MSVC )
    ADD_COMMON_LIBS("" "ENGINE_LIB" "ENGINE_LIB")
    ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libSDL2/${LIB_ARCH}/lib/" "SDL2main.lib" "SDL2main.lib")
    ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libSDL2/${LIB_ARCH}/lib/" "SDL2.lib" "SDL2.lib")
    ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libfreetype/${LIB_ARCH}/lib/" "freetype.lib" "freetype.lib")
    ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libOpenAL/${LIB_ARCH}/lib/" "OpenAL32.lib" "OpenAL32.lib") 
    ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libGLEW/${LIB_ARCH}/lib/" "glew32.lib" "glew32.lib")
    ADD_COMMON_LIBS("" "opengl32.lib" "opengl32.lib")
    ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libRocket/${LIB_ARCH}/lib/" "RocketCore.lib" "RocketCore_Debug.lib")
    ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libRocket/${LIB_ARCH}/lib/" "RocketControls.lib" "RocketControls_Debug.lib")
    ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libRocket/${LIB_ARCH}/lib/" "RocketDebugger.lib" "RocketDebugger_Debug.lib")
    ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libluabind/${LIB_ARCH}/lib/" "libluabind.lib" "libluabindd.lib")
    ADD_COMMON_LIBS("${LIB_ROOT_DIR}/liblua/${LIB_ARCH}/lib/" "liblua.lib" "liblua.lib")
    ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libboost/${LIB_ARCH}/lib/" "libboost_system-vc140-mt-1_57.lib" "libboost_system-vc140-mt-gd-1_57.lib")
    ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libboost/${LIB_ARCH}/lib/" "libboost_filesystem-vc140-mt-1_57.lib" "libboost_filesystem-vc140-mt-gd-1_57.lib")
    ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libboost/${LIB_ARCH}/lib/" "libboost_date_time-vc140-mt-1_57.lib" "libboost_date_time-vc140-mt-gd-1_57.lib")
    ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libboost/${LIB_ARCH}/lib/" "libboost_locale-vc140-mt-1_57.lib" "libboost_locale-vc140-mt-gd-1_57.lib")
    ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/" "BulletDynamics.lib" "BulletDynamics_Debug.lib")
    ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/" "BulletSoftBody.lib" "BulletSoftBody_Debug.lib")
    ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/" "BulletCollision.lib" "BulletCollision_Debug.lib")
    ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/" "LinearMath.lib" "LinearMath_Debug.lib")
    ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libTremor/${LIB_ARCH}/lib/" "vorbisidec.lib" "vorbisidec.lib")
    ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libOgg/${LIB_ARCH}/lib/" "ogg.lib" "ogg.lib")
    if ( ENABLE_VR_SUPPORT_IN_MAIN_APP )
        ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libOpenVR/${LIB_ARCH}/lib/" "openvr_api.lib" "openvr_api.lib")
    endif()
else()
    ADD_COMMON_LIBS_UNIX("ENGINE_LIB")
    ADD_COMMON_LIBS_UNIX("-L${LIB_ROOT_DIR}/libSDL2/${LIB_ARCH}/lib -lSDL2")
    ADD_COMMON_LIBS_UNIX("-L${LIB_ROOT_DIR}/libfreetype/${LIB_ARCH}/lib -lfreetype -lz")
    ADD_COMMON_LIBS_UNIX("-L${LIB_ROOT_DIR}/libOpenAL/${LIB_ARCH}/lib -lopenal") 
    if( ANDROID )
        ADD_COMMON_LIBS_UNIX("${LIB_ROOT_DIR}/libGLEW/${LIB_ARCH}/lib/libGLEW.a")
        ADD_COMMON_LIBS_UNIX("-lGLESv3")
        ADD_COMMON_LIBS_UNIX("-L${LIB_ROOT_DIR}/libRocket/${LIB_ARCH}/lib/")
    else()
        ADD_COMMON_LIBS_UNIX("-L${LIB_ROOT_DIR}/libGLEW/${LIB_ARCH}/lib -lGLEW")
        ADD_COMMON_LIBS_UNIX("GL")
        ADD_COMMON_LIBS_UNIX("-L${LIB_ROOT_DIR}/libRocket/${LIB_ARCH}/lib/") # On some systems this is the valid path
        ADD_COMMON_LIBS_UNIX("-L${LIB_ROOT_DIR}/libRocket/${LIB_ARCH}/lib/x86_64-linux-gnu") # On others this is the valid path
    endif()
    ADD_COMMON_LIBS_UNIX("-lRocketCore -lRocketControls -lRocketDebugger")
    ADD_COMMON_LIBS_UNIX("${LIB_ROOT_DIR}/libluabind/${LIB_ARCH}/lib/libluabind.a")
    ADD_COMMON_LIBS_UNIX("${LIB_ROOT_DIR}/liblua/${LIB_ARCH}/lib/liblua.a")
    ADD_COMMON_LIBS_UNIX("-ldl")
    ADD_COMMON_LIBS_UNIX("${LIB_ROOT_DIR}/libboost/${LIB_ARCH}/lib/libboost_system.a")
    ADD_COMMON_LIBS_UNIX("${LIB_ROOT_DIR}/libboost/${LIB_ARCH}/lib/libboost_filesystem.a")
    ADD_COMMON_LIBS_UNIX("${LIB_ROOT_DIR}/libboost/${LIB_ARCH}/lib/libboost_locale.a")
    ADD_COMMON_LIBS_UNIX("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/libBulletWorldImporter.a")
    ADD_COMMON_LIBS_UNIX("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/libBulletFileLoader.a")
    ADD_COMMON_LIBS_UNIX("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/libBulletDynamics.a")
    ADD_COMMON_LIBS_UNIX("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/libBulletSoftBody.a")
    ADD_COMMON_LIBS_UNIX("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/libBulletCollision.a")
    ADD_COMMON_LIBS_UNIX("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/libLinearMath.a")
    ADD_COMMON_LIBS_UNIX("${LIB_ROOT_DIR}/libTremor/${LIB_ARCH}/lib/libvorbisidec.a")
    ADD_COMMON_LIBS_UNIX("${LIB_ROOT_DIR}/libOgg/${LIB_ARCH}/lib/libogg.a")

#     ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libSDL2/${LIB_ARCH}/lib -lSDL2")
#     ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libfreetype/${LIB_ARCH}/lib freetype z")
#     ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libOpenAL/${LIB_ARCH}/lib openal") 
#     if( ANDROID )
#         ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libGLEW/${LIB_ARCH}/lib/libGLEW.a")
#         ADD_COMMON_LIBS("-lGLESv3")
#         ADD_COMMON_LIBS("-L${LIB_ROOT_DIR}/libRocket/${LIB_ARCH}/lib/")
#     else()
#         ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libGLEW/${LIB_ARCH}/lib GLEW")
#         ADD_COMMON_LIBS("GL")
#         ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libRocket/${LIB_ARCH}/lib/") # On some systems this is the valid path
#         ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libRocket/${LIB_ARCH}/lib/x86_64-linux-gnu") # On others this is the valid path
#     endif()
#     ADD_COMMON_LIBS("RocketCore RocketControls RocketDebugger")
#     ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libluabind/${LIB_ARCH}/lib/libluabind.lib")
#     ADD_COMMON_LIBS("${LIB_ROOT_DIR}/liblua/${LIB_ARCH}/lib/liblua.lib")
#     ADD_COMMON_LIBS("-ldl")
#     ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libboost/${LIB_ARCH}/lib/libboost_system.a")
#     ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libboost/${LIB_ARCH}/lib/libboost_filesystem.a")
#     ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/libBulletWorldImporter.a")
#     ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/libBulletFileLoader.a")
#     ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/libBulletDynamics.a")
#     ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/libBulletSoftBody.a")
#     ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/libBulletCollision.a")
#     ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/libLinearMath.a")
#     ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libTremor/${LIB_ARCH}/lib/libvorbisidec.a")
#     ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libOgg/${LIB_ARCH}/lib/libogg.a")
endif()

# if (CMAKE_BUILD_TYPE STREQUAL "Release")
    # ADD_COMMON_LIBS("ENGINE_LIB")

    # if ( NOT MSVC )
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libSDL2/${LIB_ARCH}/lib -lSDL2")
        # if ( NOT MSVC )
            # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libfreetype/${LIB_ARCH}/lib freetype")
        # else()
            # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libfreetype/${LIB_ARCH}/lib freetype z")
        # endif()
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libOpenAL/${LIB_ARCH}/lib openal") 
        # if( ANDROID )
            # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libGLEW/${LIB_ARCH}/lib/libGLEW.a")
            # ADD_COMMON_LIBS("-lGLESv3")
            # ADD_COMMON_LIBS("-L${LIB_ROOT_DIR}/libRocket/${LIB_ARCH}/lib/")
        # else()
            # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libGLEW/${LIB_ARCH}/lib GLEW")
            # ADD_COMMON_LIBS("GL")
            # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libRocket/${LIB_ARCH}/lib/") # On some systems this is the valid path
            # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libRocket/${LIB_ARCH}/lib/x86_64-linux-gnu") # On others this is the valid path
        # endif()
        # ADD_COMMON_LIBS("RocketCore RocketControls RocketDebugger")
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libluabind/${LIB_ARCH}/lib/libluabind.lib")
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/liblua/${LIB_ARCH}/lib/liblua.lib")
        # ADD_COMMON_LIBS("-ldl")
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libboost/${LIB_ARCH}/lib/libboost_system.a")
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libboost/${LIB_ARCH}/lib/libboost_filesystem.a")
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/libBulletWorldImporter.a")
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/libBulletFileLoader.a")
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/libBulletDynamics.a")
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/libBulletSoftBody.a")
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/libBulletCollision.a")
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/libLinearMath.a")
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libTremor/${LIB_ARCH}/lib/libvorbisidec.a")
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libOgg/${LIB_ARCH}/lib/libogg.a")
    # else()
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libSDL2/${LIB_ARCH}/lib/SDL2main.lib")
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libSDL2/${LIB_ARCH}/lib/SDL2.lib")
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libfreetype/${LIB_ARCH}/lib/freetype.lib")
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libOpenAL/${LIB_ARCH}/lib/OpenAL32.lib") 
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libGLEW/${LIB_ARCH}/lib/glew32.lib")
        # ADD_COMMON_LIBS("opengl32.lib")
        
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libRocket/${LIB_ARCH}/lib/RocketCore.lib")
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libRocket/${LIB_ARCH}/lib/RocketControls.lib")
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libRocket/${LIB_ARCH}/lib/RocketDebugger.lib")
        
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libluabind/${LIB_ARCH}/lib/libluabind.lib")
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/liblua/${LIB_ARCH}/lib/liblua.lib")

        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libboost/${LIB_ARCH}/lib/libboost_system-vc140-mt-1_57.lib")
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libboost/${LIB_ARCH}/lib/libboost_filesystem-vc140-mt-1_57.lib")
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libboost/${LIB_ARCH}/lib/libboost_date_time-vc140-mt-1_57.lib")
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libboost/${LIB_ARCH}/lib/libboost_locale-vc140-mt-1_57.lib")
        ##ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/BulletWorldImporter.lib")
        ##ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/BulletFileLoader.lib")
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/BulletDynamics.lib")
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/BulletSoftBody.lib")
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/BulletCollision.lib")
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/LinearMath.lib")
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libTremor/${LIB_ARCH}/lib/vorbisidec.lib")
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libOgg/${LIB_ARCH}/lib/ogg.lib")
    # endif()    
# else() # Debug libs (use the release libs for now)
    # ADD_COMMON_LIBS("ENGINE_LIB")
    # ADD_COMMON_LIBS("-L${LIB_ROOT_DIR}/libSDL2/${LIB_ARCH}/lib -lSDL2")
    # ADD_COMMON_LIBS("-L${LIB_ROOT_DIR}/libfreetype/${LIB_ARCH}/lib -lfreetype -lz")
    # ADD_COMMON_LIBS("-L${LIB_ROOT_DIR}/libOpenAL/${LIB_ARCH}/lib -lopenal")
    # if( ANDROID )
        # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libGLEW/${LIB_ARCH}/lib/libGLEW.a")
        # ADD_COMMON_LIBS("-lGLESv3")
        # ADD_COMMON_LIBS("-L${LIB_ROOT_DIR}/libRocket/${LIB_ARCH}/lib/")
    # else()
        # ADD_COMMON_LIBS("-L${LIB_ROOT_DIR}/libGLEW/${LIB_ARCH}/lib -lGLEW")
        # ADD_COMMON_LIBS("-lGL")
        # ADD_COMMON_LIBS("-L${LIB_ROOT_DIR}/libRocket/${LIB_ARCH}/lib/") # On some systems this is the valid path
        # ADD_COMMON_LIBS("-L${LIB_ROOT_DIR}/libRocket/${LIB_ARCH}/lib/x86_64-linux-gnu") # On others this is the valid path
    # endif()
    # ADD_COMMON_LIBS("-lRocketCore -lRocketControls -lRocketDebugger")
    # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libluabind/${LIB_ARCH}/lib/libluabind.a")
    # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/liblua/${LIB_ARCH}/lib/liblua.a")
    # ADD_COMMON_LIBS("-ldl")
    # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libboost/${LIB_ARCH}/lib/libboost_system.a")
    # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libboost/${LIB_ARCH}/lib/libboost_filesystem.a")
    # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/libBulletWorldImporter.a")
    # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/libBulletFileLoader.a")
    # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/libBulletDynamics.a")
    # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/libBulletSoftBody.a")
    # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/libBulletCollision.a")
    # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libBullet/${LIB_ARCH}/lib/libLinearMath.a")
    # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libTremor/${LIB_ARCH}/lib/libvorbisidec.a")
    # ADD_COMMON_LIBS("${LIB_ROOT_DIR}/libOgg/${LIB_ARCH}/lib/libogg.a") 
# endif()

if (BUILD_EDITOR)
    if (CMAKE_BUILD_TYPE STREQUAL "Release")
        ADD_EDITOR_LIB_PARAM(${Qt5Core_LIBRARIES})
        ADD_EDITOR_LIB_PARAM(${Qt5Widgets_LIBRARIES})
        ADD_EDITOR_LIB_PARAM(${Qt5Gui_LIBRARIES})
        ADD_EDITOR_LIB_PARAM(${Qt5OpenGL_LIBRARIES})
    else() # Debug libs (use the release libs for now)
        ADD_EDITOR_LIB_PARAM(${Qt5Core_LIBRARIES})
        ADD_EDITOR_LIB_PARAM(${Qt5Widgets_LIBRARIES})
        ADD_EDITOR_LIB_PARAM(${Qt5Gui_LIBRARIES})
        ADD_EDITOR_LIB_PARAM(${Qt5OpenGL_LIBRARIES})
    endif()
endif()

set (EXECUTABLE_TYPE_FLAG "")
if ( MSVC )
    # Indicates that an executable is Windows GUI application
    set (EXECUTABLE_TYPE_FLAG "WIN32")
endif()

if (BUILD_MAIN_APP)
    if( ANDROID )
        add_library (${TARGET_MAIN_APP} SHARED ${TARGET_MAIN_APP_SOURCES})
        set_target_properties(${TARGET_MAIN_APP} PROPERTIES OUTPUT_NAME main PREFIX lib)
    else()
        # Add the executable
        add_executable (${TARGET_MAIN_APP} ${EXECUTABLE_TYPE_FLAG} ${TARGET_MAIN_APP_SOURCES})
        
        set_target_properties (${TARGET_MAIN_APP} PROPERTIES
        RUNTIME_OUTPUT_DIRECTORY_DEBUG   ${PROJECT_BINARY_DIR}/bin/Debug
        RUNTIME_OUTPUT_DIRECTORY_RELEASE ${PROJECT_BINARY_DIR}/bin/Release
        )
    endif()
    target_compile_options (${TARGET_MAIN_APP} PRIVATE ${EXTRA_CXX_FLAGS})
    target_compile_definitions(${TARGET_MAIN_APP} PUBLIC ${EXTRA_CXX_DEFINES})
    target_link_libraries (${TARGET_MAIN_APP} ${EXTRA_COMMON_LIBS})
    target_include_directories (${TARGET_MAIN_APP} PRIVATE ${EXTRA_MAIN_APP_INCLUDE_DIRS})
endif()

if (BUILD_EDITOR)
    # Add the executable
    add_executable (${TARGET_EDITOR} ${EXECUTABLE_TYPE_FLAG} ${TARGET_EDITOR_SOURCES} ${EDITOR_QT5_UIC_OUT_HEADER_FILES})
    target_compile_options (${TARGET_EDITOR} PUBLIC ${EXTRA_CXX_FLAGS})
    target_compile_definitions(${TARGET_EDITOR} PUBLIC ${EXTRA_CXX_DEFINES})
    target_link_libraries (${TARGET_EDITOR} ${EXTRA_COMMON_LIBS} ${EXTRA_EDITOR_LIBS})
    target_include_directories (${TARGET_EDITOR} PRIVATE ${EXTRA_EDITOR_INCLUDE_DIRS})
    set_target_properties (${TARGET_EDITOR} PROPERTIES
    RUNTIME_OUTPUT_DIRECTORY_DEBUG   ${PROJECT_BINARY_DIR}/bin/Debug
    RUNTIME_OUTPUT_DIRECTORY_RELEASE ${PROJECT_BINARY_DIR}/bin/Release
    AUTOMOC TRUE
    )    
endif()

if( ANDROID )
    install(
        TARGETS  ${TARGET_MAIN_APP}
        RUNTIME  DESTINATION bin
        LIBRARY  DESTINATION lib
        ARCHIVE  DESTINATION lib
        INCLUDES DESTINATION includes
    )
endif()