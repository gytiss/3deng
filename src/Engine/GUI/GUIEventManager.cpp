#include "GUIEventManager.h"
#include "GUIContext.h"
#include <CommonDefinitions.h>

//-----------------------------------------------------------------------

GUIEventManager::GUIEventManager(GUIContext* context)
: m_GUIContext( context )
{

}

//-----------------------------------------------------------------------

GUIEventManager::~GUIEventManager()
{
	for(EventHandlerMap_t::iterator i = m_EventHandlerMap.begin(); i != m_EventHandlerMap.end(); ++i)
    {
        GUIEventHandler* handler = i->second;

        if( handler )
        {
            delete handler;
        }

        m_EventHandlerMap.clear();
    }
}

//-----------------------------------------------------------------------

void GUIEventManager::RegisterEventHandler(const std::string& handlerName,
                                            GUIEventHandler* handler)
{
    if( !handlerName.empty() && handler )
    {
        // Release any handler bound under the same name.
        EventHandlerMap_t::iterator it = m_EventHandlerMap.find( handlerName );
        if( it != m_EventHandlerMap.end() )
        {
            delete it->second;
            it->second = handler;
        }
        else
        {
            m_EventHandlerMap[handlerName] = handler;
        }
    }
}

//-----------------------------------------------------------------------

void GUIEventManager::ProcessEvent(Rocket::Core::Event& event,
                                    const Rocket::Core::String& value)
{
	Rocket::Core::StringList commands;
	Rocket::Core::StringUtilities::ExpandString(commands, value, ';');
	for(size_t i = 0; i < commands.size(); ++i)
	{
		// Check for a generic 'load' or 'exit' command.
		Rocket::Core::StringList values;
		Rocket::Core::StringUtilities::ExpandString(values, commands[i], ' ');

		if( !values.empty() )
        {
            if( ( values[0] == "goto" ) && ( values.size() > 1 ) )
            {
                // Load the window, and if successful close the old window.
    //			if( LoadWindow( values[1] ) )
    //            {
    //				event.GetTargetElement()->GetOwnerDocument()->Close();
    //            }
            }
            else if( ( values[0] == "load" ) && ( values.size() > 1) )
            {
                // Load the window.
                //LoadWindow(values[1]);
            }
            else if( values[0] == "close" )
            {
                Rocket::Core::ElementDocument* target_document = nullptr;

                if( values.size() > 1 )
                {
                    target_document = m_GUIContext->GetLibRocketContext()->GetDocument( values[1].CString() );
                }
                else
                {
                    target_document = event.GetTargetElement()->GetOwnerDocument();
                }

                if( target_document != nullptr )
                {
                    target_document->Close();
                }
            }
            else
            {
                Rocket::Core::Element* eventHandlerName = event.GetTargetElement()->GetOwnerDocument()->GetElementById("event_handler_name");
                if( eventHandlerName )
                {
                    Rocket::Core::String contents;
                    eventHandlerName->GetInnerRML( contents );
                    if( !contents.Empty() )
                    {
                        EventHandlerMap_t::iterator it = m_EventHandlerMap.find( contents.CString() );
                        if( it != m_EventHandlerMap.end() )
                        {
                            GUIEventHandler* eventHandler = it->second;
                            eventHandler->ProcessEvent( event, commands[i] );
                        }
                    }
                }
            }
        }
	}
}

//-----------------------------------------------------------------------
