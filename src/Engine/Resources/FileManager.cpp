#include "FileManager.h"
#include <MsgLogger.h>
#include <algorithm>

//-----------------------------------------------------------------------

FileManager::File::File()
: m_FileHanlde( nullptr ),
  m_FilePath(),
  m_Type(),
  m_Size( 0 ),
  m_SizeQueriedAlready( false )
{

}

//-----------------------------------------------------------------------

FileManager::File::File(const std::string& filePath, FileType type)
: m_FileHanlde( nullptr ),
  m_FilePath( filePath ),
  m_Type( type ),
  m_Size( 0 ),
  m_SizeQueriedAlready( false )
{

}

//-----------------------------------------------------------------------

FileManager::File::~File()
{
    Close();
}

//-----------------------------------------------------------------------

bool FileManager::File::Read(void* buffer, size_t chunkSize, size_t chunksToRead)
{
#ifdef OS_ANDROID
    const size_t chunksRead = SDL_RWread( m_FileHanlde, buffer, chunkSize, chunksToRead );
#else
    const size_t chunksRead = fread( buffer, chunkSize, chunksToRead, m_FileHanlde );
#endif

    return ( ( chunksRead != 0 ) && ( chunksRead == chunksToRead ) );
}

//-----------------------------------------------------------------------

bool FileManager::File::Open()
{
    bool retVal = false;

    if( !m_FileHanlde )
    {

        std::string mode = "r";

        if( m_Type == e_FileTypeBinary )
        {
            mode += "b";
        }
        else if( m_Type == e_FileTypeText )
        {
            mode += "t";
        }

#ifdef OS_ANDROID
        m_FileHanlde = SDL_RWFromFile( m_FilePath.c_str(), mode.c_str() );

#else
    #ifdef _MSC_VER
        const errno_t fOpenResult = fopen_s(&m_FileHanlde, m_FilePath.c_str(), mode.c_str());
        if (fOpenResult != 0)
        {
            m_FileHanlde = nullptr;
        }
    #else
        m_FileHanlde = fopen( m_FilePath.c_str(), mode.c_str() );
    #endif
#endif

        if( m_FileHanlde )
        {
            retVal = true;
        }
    }

    return retVal;
}

//-----------------------------------------------------------------------

size_t FileManager::File::Size()
{
    if( !m_SizeQueriedAlready )
    {
        m_SizeQueriedAlready = true;
        m_Size = 0;
        bool ok = false;

#ifdef OS_ANDROID
        Sint64 size = SDL_RWsize( m_FileHanlde );
        ok = ( size >= 0 );
#else
        ok = ( fseek( m_FileHanlde , 0L, SEEK_END ) == 0 );
        long int size = -1L;
        if( ok )
        {
            size = ftell( m_FileHanlde );
        }
        ok = ok && ( size >= 0 );
        ok = ok && ( fseek( m_FileHanlde, 0L, SEEK_SET ) == 0 );
#endif

        if( ok )
        {
            m_Size = size;
        }
        else
        {
            MsgLogger::LogError( "Failed to get size of the file " + m_FilePath );
        }
    }

    return m_Size;
}

//-----------------------------------------------------------------------

bool FileManager::File::Close()
{
    bool retVal = false;

    if( m_FileHanlde )
    {
#ifdef OS_ANDROID
        retVal = ( SDL_RWclose( m_FileHanlde ) == 0 );

#else
        retVal = ( fclose( m_FileHanlde ) == 0 );
#endif
        if( !retVal )
        {
            MsgLogger::LogError( "Failed to close file " + m_FilePath );
        }

        m_FileHanlde = nullptr;
    }

    return retVal;
}

//-----------------------------------------------------------------------

FileManager::File::File(const File& other)
: m_FileHanlde( other.m_FileHanlde ), // TODO it is probrably not right to copy file descriptor without duplicating it
  m_FilePath( other.m_FilePath ),
  m_Type( other.m_Type ),
  m_Size( other.m_Size ),
  m_SizeQueriedAlready( other.m_SizeQueriedAlready ) // TODO it is probrably not right to copy this data without seeking in the new file descriptor
{

}

//-----------------------------------------------------------------------

FileManager::File& FileManager::File::operator=(File& other)
{
    swap( *this, other );
    return *this;
}

//-----------------------------------------------------------------------

FileManager::FileManager()
{

}

//-----------------------------------------------------------------------

bool FileManager::OpenFile(const std::string& filePath, FileType type, File& outFile)
{
    bool retVal = false;

    File file( filePath, type );

    if( file.Open() )
    {
        retVal = true;
        outFile.Close();
        outFile = file;
    }

    return retVal;
}

//-----------------------------------------------------------------------

void swap(FileManager::File& first, FileManager::File& second)
{
    // enable ADL(Argument-dependent lookup) (just in case)
    using std::swap;

    swap( first.m_FileHanlde, second.m_FileHanlde );
    swap( first.m_FilePath, second.m_FilePath );
    swap( first.m_Type, second.m_Type );
    swap( first.m_Size, second.m_Size );
    swap( first.m_SizeQueriedAlready, second.m_SizeQueriedAlready );
}

//-----------------------------------------------------------------------
