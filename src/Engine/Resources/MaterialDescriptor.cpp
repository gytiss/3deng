#include "MaterialDescriptor.h"
#include <algorithm>

//-----------------------------------------------------------------------

MaterialDescriptor::MaterialDescriptor(TextureManager& textureManager)
: m_TextureManager( textureManager ),
  m_TextureList(),
  m_DiffuseColor( glm::vec3( 1.0f, 1.0f, 1.0f ) ),
  m_SpecularColor( glm::vec3( 1.0f, 1.0f, 1.0f ) ),
  m_Glossiness( 0 ),
  m_MaterialPropertiesPresent(),
  m_TextureMapTypesPresent()
{

}

//-----------------------------------------------------------------------

MaterialDescriptor::MaterialDescriptor(const MaterialDescriptor& other)
: m_TextureManager( other.m_TextureManager ),
  m_TextureList(),
  m_DiffuseColor( other.m_DiffuseColor ),
  m_SpecularColor( other.m_DiffuseColor ),
  m_Glossiness( other.m_Glossiness ),
  m_MaterialPropertiesPresent( other.m_MaterialPropertiesPresent ),
  m_TextureMapTypesPresent( other.m_TextureMapTypesPresent )
{
    for(TextureList::const_iterator it = other.m_TextureList.begin(); it != other.m_TextureList.end(); ++it)
    {
        m_TextureList.push_back( m_TextureManager.get().DuplicateTextureHandle( *it ) );
    }
}

//-----------------------------------------------------------------------

MaterialDescriptor& MaterialDescriptor::operator=(MaterialDescriptor other)
{
    swap( *this, other );
    return *this;
}

//-----------------------------------------------------------------------

MaterialDescriptor::~MaterialDescriptor()
{
    for(TextureList::iterator it = m_TextureList.begin(); it != m_TextureList.end(); ++it)
    {
        m_TextureManager.get().ReleaseTexture( *it );
    }
}

//-----------------------------------------------------------------------

const EngineTypes::SRGBColor& MaterialDescriptor::GetDiffuseColor() const
{
    return m_DiffuseColor;
}

//-----------------------------------------------------------------------

const EngineTypes::SRGBColor& MaterialDescriptor::GetSpecularColor() const
{
    return m_SpecularColor;
}

//-----------------------------------------------------------------------

uint8_t MaterialDescriptor::GetGlossiness() const
{
    return m_Glossiness;
}

//-----------------------------------------------------------------------

void MaterialDescriptor::AddTexture(TextureManager::TextureHandle& textureHandle)
{
    if( ( textureHandle->GetType() == Texture::e_TypeDiffuseMap ) ||
        ( textureHandle->GetType() == Texture::e_TypeNormalMap ) ||
        ( textureHandle->GetType() == Texture::e_TypeSpecularMap ) ||
        ( textureHandle->GetType() == Texture::e_TypeGlossMap ) )
    {
        if( _IsTextureMapPresent( textureHandle->GetTypeIndex() ) )
        {
            // Texture map of this type has already been added, hence replace it with the new one
            bool found = false;
            for (TextureList::iterator it = m_TextureList.begin(); ( !found && ( it != m_TextureList.end() ) ); ++it)
            {
                if( (*it)->GetType() == textureHandle->GetType() )
                {
                    m_TextureManager.get().ReleaseTexture(*it);
                    *it = textureHandle;
                    found = true;
                }
            }
        }
        else
        {
            m_TextureList.push_back( textureHandle );
        }

        _MarkTextureMapAsPresent(textureHandle->GetTypeIndex());
    }
    else
    {
        assert(0 && "Trying to add unsupported texture type");
    }
}

//-----------------------------------------------------------------------

void MaterialDescriptor::SetGlossiness(uint8_t glossiness)
{
    m_Glossiness = glossiness;
    _MarkMaterialPropertyAsPresent( e_MaterialPropertyTypeGlossiness );
}

//-----------------------------------------------------------------------

void MaterialDescriptor::SetDiffuseColor(const EngineTypes::SRGBColor& srgbDiffuseColor)
{
    m_DiffuseColor = srgbDiffuseColor;
    _MarkMaterialPropertyAsPresent( e_MaterialPropertyTypeDiffuseColor );
}

//-----------------------------------------------------------------------

void MaterialDescriptor::SetSpercularColor(const EngineTypes::SRGBColor& srgbSpecularColor)
{
    m_SpecularColor = srgbSpecularColor;
    _MarkMaterialPropertyAsPresent( e_MaterialPropertyTypeSpecularColor );
}

//-----------------------------------------------------------------------

void MaterialDescriptor::SetDiffuseTextureMap(TextureManager::TextureHandle& textureHandle)
{
    AddTexture( textureHandle );
}

//-----------------------------------------------------------------------

void MaterialDescriptor::SetNormalTextureMap(TextureManager::TextureHandle& textureHandle)
{
    AddTexture( textureHandle );
}

//-----------------------------------------------------------------------

void MaterialDescriptor::SetSpecularTextureMap(TextureManager::TextureHandle& textureHandle)
{
    AddTexture( textureHandle );
}

//-----------------------------------------------------------------------

void MaterialDescriptor::SetGlossTextureMap(TextureManager::TextureHandle& textureHandle)
{
    AddTexture( textureHandle );
}

//-----------------------------------------------------------------------

void MaterialDescriptor::_MarkMaterialPropertyAsPresent(MaterialPropertyType type)
{
    m_MaterialPropertiesPresent.SetBit(type);
}

//-----------------------------------------------------------------------


bool MaterialDescriptor::_IsMaterialPropertyPresent(MaterialPropertyType type) const
{
    return m_MaterialPropertiesPresent.IsBitSet(type);
}

//-----------------------------------------------------------------------

void MaterialDescriptor::_MarkTextureMapAsPresent(Texture::TypeIdx typeIndex)
{
    assert((typeIndex <= m_TextureMapTypesPresent.NumBits()) && "Texture type index must not be larger than the BitMask width");
    m_TextureMapTypesPresent.SetBit(typeIndex);
}

//-----------------------------------------------------------------------

bool MaterialDescriptor::_IsTextureMapPresent(Texture::TypeIdx typeIndex) const
{
    assert((typeIndex <= m_TextureMapTypesPresent.NumBits()) && "Texture type index must not be larger than the BitMask width");
    return m_TextureMapTypesPresent.IsBitSet(typeIndex);
}

//-----------------------------------------------------------------------

void swap(MaterialDescriptor& first, MaterialDescriptor& second)
{
    // enable ADL(Argument-dependent lookup) (just in case)
    using std::swap;

    swap( first.m_TextureManager, second.m_TextureManager );
    swap( first.m_TextureList, second.m_TextureList );
    swap( first.m_DiffuseColor, second.m_DiffuseColor );
    swap( first.m_SpecularColor, second.m_SpecularColor );
    swap( first.m_Glossiness, second.m_Glossiness );
    swap( first.m_MaterialPropertiesPresent, second.m_MaterialPropertiesPresent );
    swap( first.m_TextureMapTypesPresent, second.m_TextureMapTypesPresent );
}

//-----------------------------------------------------------------------
