#ifndef PHYSICSDEBUGDATADRAWER_H_INCLUDED
#define PHYSICSDEBUGDATADRAWER_H_INCLUDED

#include <bullet/LinearMath/btIDebugDraw.h>
#include <GraphicsDevice/VAO.h>
#include <Renderer/RenderableObject.h>
#include <vector>

class VBO;
class RenderableObject;

class PhysicsDebugDataDrawer : public btIDebugDraw
{
public:
    PhysicsDebugDataDrawer();
    virtual ~PhysicsDebugDataDrawer();

    const RenderableObject* GetRenderable();

    void ClearDebugDataCache();

    virtual void drawLine(const btVector3& from,
                           const btVector3& to,
                           const btVector3& color);

    virtual void drawContactPoint(const btVector3& PointOnB,
                                   const btVector3& normalOnB,
                                   btScalar distance,
                                   int lifeTime,
                                   const btVector3& color);

    virtual void reportErrorWarning(const char* warningString);

    virtual void draw3dText(const btVector3& location,
                             const char* textString);

    virtual void setDebugMode(int debugMode);
    virtual int getDebugMode() const;

private:
    // Used to prevent copying
    PhysicsDebugDataDrawer& operator = (const PhysicsDebugDataDrawer& other);
    PhysicsDebugDataDrawer(const PhysicsDebugDataDrawer& other);

    struct DebugDataVertex
    {
        float position[3];
        float color[3];
    };

    int m_DebugMode;
    VAO m_VAO;
    VBO* m_VBO;
    std::vector<DebugDataVertex> m_DebugLinesDataVertices;
    RenderableObject m_RenderableDataDescription;
    bool m_RenderableDataDescriptionValid;
};

#endif // PHYSICSDEBUGDATADRAWER_H_INCLUDED
