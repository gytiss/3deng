#include "VRHMDBuffer.h"
#include <GraphicsDevice/FBOColorAttachmentTexture.h>
#include <GraphicsDevice/GraphicsDevice.h>
#include <OpenGLErrorChecking.h>
#ifdef VR_BACKEND_ENABLED
    #include <VR/VRBackend.h>
#endif

//-----------------------------------------------------------------------

VRHMDBuffer::VRHMDBuffer(VRBackend& vrBackend, GraphicsDevice& graphicsDevice, SystemInterface& systemInterface)
: m_VRBackend( vrBackend ),
  m_GraphicsDevice( graphicsDevice ),
  m_SystemInterface( systemInterface ),
  m_HMDLeftEyeTexture( nullptr ),
  m_HMDRightEyeTexture( nullptr ),
  m_HMDLeftEyeFBO( graphicsDevice, systemInterface  ),
  m_HMDRightEyeFBO( graphicsDevice, systemInterface )
{
    m_HMDLeftEyeTexture = new FBOColorAttachmentTexture( m_GraphicsDevice,
                                                         m_SystemInterface.GetDrawableSurfaceSize().widthInPixels,
                                                         m_SystemInterface.GetDrawableSurfaceSize().heightInPixels,
                                                         FBOColorAttachmentTexture::e_TypeIdxPostProcessTextureMap,
                                                         FBOColorAttachmentTexture::e_DataFormatRGBA8 );

    m_HMDRightEyeTexture = new FBOColorAttachmentTexture( m_GraphicsDevice,
                                                          m_SystemInterface.GetDrawableSurfaceSize().widthInPixels,
                                                          m_SystemInterface.GetDrawableSurfaceSize().heightInPixels,
                                                          FBOColorAttachmentTexture::e_TypeIdxPostProcessTextureMap,
                                                          FBOColorAttachmentTexture::e_DataFormatRGBA8 );

    m_HMDLeftEyeFBO.AttachColorTexture( m_HMDLeftEyeTexture );
    m_HMDRightEyeFBO.AttachColorTexture( m_HMDRightEyeTexture );
}

//-----------------------------------------------------------------------

VRHMDBuffer::~VRHMDBuffer()
{

}

//-----------------------------------------------------------------------

void VRHMDBuffer::SubmitTextureToHMD(Eye eye, FBO& fboWithColorData, FBO::ColorAttachmentIdx fboColorAttachmentToUse)
{
    // Copy color data from the source FBO to the texture to be submited
    fboWithColorData.BindColorAttachmentForReading( fboColorAttachmentToUse );
    if( eye == Eye::Left )
    {
        m_HMDLeftEyeFBO.BindColorAttachmentsForDrawing( FBO::e_ColorAttachmentIdx0 );
    }
    else
    {
        m_HMDRightEyeFBO.BindColorAttachmentsForDrawing( FBO::e_ColorAttachmentIdx0 );
    }
    glBlitFramebuffer( 0, 0, m_SystemInterface.GetDrawableSurfaceSize().widthInPixels, m_SystemInterface.GetDrawableSurfaceSize().heightInPixels,
                       0, 0, m_SystemInterface.GetDrawableSurfaceSize().widthInPixels, m_SystemInterface.GetDrawableSurfaceSize().heightInPixels, GL_COLOR_BUFFER_BIT, GL_LINEAR );
    CheckForGLErrors();
    FBO::BindDefaultFBOForDrawing();

    vr::Texture_t textureToSubmit;
    if( eye == Eye::Left )
    {
        textureToSubmit.handle = (void*)( m_HMDLeftEyeTexture->GetOpenGLInternalTextureID() );
    }
    else
    {
        textureToSubmit.handle = (void*)( m_HMDRightEyeTexture->GetOpenGLInternalTextureID() );
    }
    textureToSubmit.eType = vr::TextureType_OpenGL;
    textureToSubmit.eColorSpace = vr::ColorSpace_Gamma;

    m_VRBackend.SubmitAFrameToHDM( ( eye == Eye::Left ? vr::Eye_Left : vr::Eye_Right ), &textureToSubmit );
    m_GraphicsDevice.Flush();

    Texture::InvalidateCashedActivationState();
}

//-----------------------------------------------------------------------