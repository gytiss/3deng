#ifndef MOVEMANIPULATOR_H_INCLUDED
#define MOVEMANIPULATOR_H_INCLUDED

#include <Scene/GameObject.h>
#include <EngineTypes.h>

class MeshManager;
class MaterialManager;
class TextureManager;

class MoveManipulator : public GameObject
{
public:
    MoveManipulator(MeshManager& meshManager,
                    MaterialManager& materialManager,
                    TextureManager& textureManager);
    ~MoveManipulator();

    const GameObject* GetXAxis() const;
    const GameObject* GetYAxis() const;
    const GameObject* GetZAxis() const;

private:
    GameObject* _CreateAxis(const std::string& name, uint32_t id, const EngineTypes::SRGBColor& srgbColor);

    MeshManager& m_MeshManager;
    MaterialManager& m_MaterialManager;
    TextureManager& m_TextureManager;
    GameObject* m_XAxis;
    GameObject* m_YAxis;
    GameObject* m_ZAxis;
};

#endif // MOVETOOL_H_INCLUDED
