#include <iostream>
#include <boost/filesystem.hpp>
#include "../../MAnim.h"

void PrintHelp(std::string appName)
{
    std::cout << "This utility is used for testing loading of MAnim animation files for MObj models" << std::endl;
    std::cout << "Usage: " << appName << " <Path to input MAnim animation file>" << std::endl;
}

int main(int argc, const char* argv[])
{
    if( argc >= 2 )
    {
        if( boost::filesystem::exists( argv[1] ) )
        {
            MAnim mAnimation;
            if( mAnimation.LoadAnimation( argv[1] ) )
            {
                mAnimation.DebugDumpData();
                std::cout << "Success" << std::endl;
            }
            else
            {
                std::cout << "Failed to load animation" << std::endl;
            }
        }
        else
        {
            std::cerr << "File \"" << argv[1] << "\" does not exist" << std::endl;
        }
    }
    else
    {
        PrintHelp( boost::filesystem::path( argv[0] ).leaf().c_str() );
    }

    return 0;
}
