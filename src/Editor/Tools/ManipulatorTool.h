#ifndef MANIPULATORTOOL_H_INCLUDED
#define MANIPULATORTOOL_H_INCLUDED

#include <vector>
#include <cstdint>
#include <glm/glm.hpp>
#include <Renderer/RenderManager.h>
#include <Renderer/RenderableObject.h>
#include <MathUtils.h>
#include <EditorObjectIDs.h>
#include "MoveManipulator.h"

class Camera;
class Transform;
class MeshManager;
class MaterialManager;
class TextureManager;

class ManipulatorTool
{
public:
    enum Mode
    {
        e_ModeTranslation = 0,
        e_ModeRotation = 1,
        e_ModeScale,
        e_ModeNone
    };

    ManipulatorTool(MeshManager& meshManager,
                    MaterialManager& materialManager,
                    TextureManager& textureManager);

    bool PickManipulator(RenderManager& renderManager, const glm::vec2& absoluteMouseCoordinates);
    void ClearPickedManipulator();
    void SetTransformToManipulate(Transform* t);
    Transform* GetTransformBeingManipulated();
    void SetMode(Mode mode);
    Mode GetMode() const;
    bool Process(const glm::vec2& newAbsoluteMouseCoordinates,
                 const EngineTypes::VPTransformations& vpTrans,
                 const SystemInterface::DrawableSurfaceSize& m_DrawableSurfaceSize);
    void AddRenderables(RenderManager& renderManager) const;

    /**
     * Makes sure the manipulator object appears to be the same
     * size on the screen irrespective of the cameras position
     */
    void MaintainSize(const Camera& camera);

    /**
     * Used for refreshing manipulator position in cases where the manipulated transform was modified outside of the manipulator
     */
    void RefreshPosition();
private:
    void _SyncManipulatorAndManipulatedTransformPositions();
    void _AddRenderablesFromGameObjectHierarchyToTheRenderList(GameObject* gameObject);
    void _AddRenderablesFromGameObjectHierarchyToThePickingList(GameObject* gameObject);

    Mode m_CurrentMode;
    Transform* m_TransformBeingManipulated;
    Transform* m_CurrentlyActiveManipulatorTransform;
    std::vector<RenderableObject> m_Renderables;
    std::vector<RenderManager::PickingPair> m_PickingRenderables;
    uint32_t m_PickedManipulatorComponentID;
    glm::vec2 m_PreviouslyProcessedAbsouluteMouseCoordinates;
    MoveManipulator m_MoveManipulator;
};

#endif // MANIPULATORTOOL_H_INCLUDED
