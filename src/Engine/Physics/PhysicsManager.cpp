#include "PhysicsManager.h"
#include "PhysicsDebugDataDrawer.h"
#include <glm/gtc/type_ptr.hpp>
#include <bullet/btBulletDynamicsCommon.h>
//#include <bullet/BulletWorldImporter/btBulletWorldImporter.h>
#include <bullet/BulletCollision/CollisionDispatch/btGhostObject.h>
#include <Exceptions/GenericException.h>
#include <CommonDefinitions.h>
#include <EngineTypes.h>

const float PhysicsManager::c_PhysicsUpdateRateInSeconds = ( 1.0f / 60.0f );
const int PhysicsManager::c_MaxSimulationStepsPerSimulationCall = 2;

//-----------------------------------------------------------------------

PhysicsManager::PhysicsManager()
: // Collision configuration contains default setup for memory, collision setup. Advanced users can create their own configuration.
  m_CollisionConf( new btDefaultCollisionConfiguration() ),

  // Yes the default collision dispatcher. For parallel processing you can use a different dispatcher (see Extras/BulletMultiThreaded)
  m_Dispatcher( new	btCollisionDispatcher( m_CollisionConf ) ),

  //btDbvtBroadphase is a good general purpose broadphase. You can also try out btAxis3Sweep.
  m_OverlappingPairCache( new btDbvtBroadphase() ),

  // The default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
  m_Solver( new btSequentialImpulseConstraintSolver() ),
  m_DynamicsWorld( new btDiscreteDynamicsWorld( m_Dispatcher, m_OverlappingPairCache, m_Solver, m_CollisionConf ) ),
  m_CollisionShapes(),
  m_DebugDataDrawer( new PhysicsDebugDataDrawer() ),
  m_EmptyCollisionShape( new btEmptyShape() )
{
    m_DynamicsWorld->setGravity( btVector3( static_cast<btScalar>( 0.0 ), 
                                            static_cast<btScalar>( -9.8 ),
                                            static_cast<btScalar>( 0.0 ) ) );
    m_DynamicsWorld->setDebugDrawer( m_DebugDataDrawer );

    m_OverlappingPairCache->getOverlappingPairCache()->setInternalGhostPairCallback( new btGhostPairCallback() );
}

//-----------------------------------------------------------------------

PhysicsManager::~PhysicsManager()
{
    // Remove the rigidbodies from the dynamics world and delete them
    for (int i = m_DynamicsWorld->getNumCollisionObjects() - 1; i >= 0; i--)
    {
        btCollisionObject* obj = m_DynamicsWorld->getCollisionObjectArray()[i];
//        btRigidBody* body = btRigidBody::upcast(obj); // No deletion here, because all of the motion states are externally owned
//        if (body && body->getMotionState())
//        {
//            delete body->getMotionState();
//        }
        m_DynamicsWorld->removeCollisionObject( obj );
        delete obj;
    }

    // Delete collision shapes
    for (CollisionShapesList::iterator it = m_CollisionShapes.begin(); it != m_CollisionShapes.end(); ++it)
    {
        _DestroyBulletCollisionShape( *it );
        *it = nullptr;
    }

    delete m_DynamicsWorld;
    delete m_Solver;
    delete m_OverlappingPairCache;
    delete m_Dispatcher;
    delete m_CollisionConf;
    delete m_DebugDataDrawer;
    delete m_EmptyCollisionShape;

    m_CollisionShapes.clear();
}

//-----------------------------------------------------------------------

void PhysicsManager::StepSimulation(float deltaTime)
{
    m_DynamicsWorld->stepSimulation( deltaTime,
                                     c_MaxSimulationStepsPerSimulationCall + 1, // Needs to be above 1 to enable bullet interpolation (Current code assumes at most
                                                                                // c_MaxSimulationStepsPerSimulationCall steps, but set it to
                                                                                // (c_MaxSimulationStepsPerSimulationCall + 1) just in case)
                                     btScalar( 1.0 ) / btScalar( 60.0 ) );
}

//-----------------------------------------------------------------------

PhysicsManager::CollisionShapeHandle PhysicsManager::CreateStaticInfinitePlaneCollisionShape()
{
    m_CollisionShapes.push_back( new btStaticPlaneShape( btVector3( 0.0f, 1.0f, 0.0f ), 0.0f ) );
    return CollisionShapeHandle( --m_CollisionShapes.end() );
}

//-----------------------------------------------------------------------

PhysicsManager::CollisionShapeHandle PhysicsManager::CreateCapsuleXCollisionShape(float radius,
                                                                                  float height)
{
    m_CollisionShapes.push_back( new btCapsuleShapeX( radius, height ) );
    return CollisionShapeHandle( --m_CollisionShapes.end() );
}

//-----------------------------------------------------------------------

PhysicsManager::CollisionShapeHandle PhysicsManager::CreateCapsuleYCollisionShape(float radius,
                                                                                  float height)
{
    m_CollisionShapes.push_back( new btCapsuleShape( radius, height ) );
    return CollisionShapeHandle( --m_CollisionShapes.end() );
}

//-----------------------------------------------------------------------

PhysicsManager::CollisionShapeHandle PhysicsManager::CreateCapsuleZCollisionShape(float radius,
                                                                                  float height)
{
    m_CollisionShapes.push_back( new btCapsuleShapeZ( radius, height ) );
    return CollisionShapeHandle( --m_CollisionShapes.end() );
}

//-----------------------------------------------------------------------

PhysicsManager::CollisionShapeHandle PhysicsManager::CreateConeXCollisionShape(float radius,
                                                                               float height)
{
    m_CollisionShapes.push_back( new btConeShapeX( radius, height ) );
    return CollisionShapeHandle( --m_CollisionShapes.end() );
}

//-----------------------------------------------------------------------

PhysicsManager::CollisionShapeHandle PhysicsManager::CreateConeYCollisionShape(float radius,
                                                                               float height)
{
    m_CollisionShapes.push_back( new btConeShape( radius, height ) );
    return CollisionShapeHandle( --m_CollisionShapes.end() );
}

//-----------------------------------------------------------------------

PhysicsManager::CollisionShapeHandle PhysicsManager::CreateConeZCollisionShape(float radius,
                                                                               float height)
{
    m_CollisionShapes.push_back( new btConeShapeZ( radius, height ) );
    return CollisionShapeHandle( --m_CollisionShapes.end() );
}

//-----------------------------------------------------------------------

PhysicsManager::CollisionShapeHandle PhysicsManager::CreateBoxCollisionShape(const glm::vec3& dimensions)
{
    m_CollisionShapes.push_back( new btBoxShape( btVector3( dimensions.x, dimensions.y, dimensions.z ) ) );
    return CollisionShapeHandle( --m_CollisionShapes.end() );
}

//-----------------------------------------------------------------------

PhysicsManager::CollisionShapeHandle PhysicsManager::CreateSphereCollisionShape(float radius)
{
    m_CollisionShapes.push_back( new btSphereShape( btScalar( radius ) ) );
    return CollisionShapeHandle( --m_CollisionShapes.end() );
}

//-----------------------------------------------------------------------

PhysicsManager::CollisionShapeHandle PhysicsManager::CreateMeshCollisionShape(const MeshManager::MeshHandle& meshHandle)
{
    if( !meshHandle.IsValid() )
    {
        throw GenericException( "Tried to use invalid mesh handle when creating mesh collision shape" );
    }

    if( !meshHandle->HasMeshDataOnCPUReadableMemory() )
    {
        throw GenericException( "Tried to use mesh, which does not have CPU readable, mesh data loaded when creating mesh collision shape" );
    }

    btTriangleIndexVertexArray* meshInterface = new btTriangleIndexVertexArray();

    btIndexedMesh mesh;
    mesh.m_numTriangles = static_cast<int>( meshHandle->GetIndexCount() / 3 );
    mesh.m_triangleIndexBase = reinterpret_cast<const unsigned char *>( meshHandle->GetIndexDataBase() );
    mesh.m_triangleIndexStride = ( sizeof( EngineTypes::VertexIndexType ) * 3 );
    mesh.m_numVertices = static_cast<int>( meshHandle->GetVertexCount() );
    mesh.m_vertexBase = reinterpret_cast<const unsigned char *>( meshHandle->GetVertexDataBase() );
    mesh.m_vertexStride = meshHandle->GetVertexDataStride();
    PHY_ScalarType indexType = PHY_INTEGER;
    if( sizeof( EngineTypes::VertexIndexType ) == 2 )
    {
       indexType = PHY_SHORT;
    }
    else if( sizeof( EngineTypes::VertexIndexType ) == 4 )
    {
       indexType = PHY_INTEGER;
    }
    else
    {
       throw GenericException( "Unsupported index data type" );
    }
    mesh.m_vertexType = PHY_FLOAT;

   meshInterface->addIndexedMesh( mesh, indexType );

    btCollisionShape* colShape = new btBvhTriangleMeshShape( meshInterface, false );

    m_CollisionShapes.push_back( colShape );

    return CollisionShapeHandle( --m_CollisionShapes.end() );
}

//-----------------------------------------------------------------------

void PhysicsManager::DestroyCollisionShape(CollisionShapeHandle& shape)
{
    if( shape.IsValid() )
    {
        _DestroyBulletCollisionShape( *shape.m_CollisionShapeListIt );
        *shape.m_CollisionShapeListIt = nullptr;
        m_CollisionShapes.erase( shape.m_CollisionShapeListIt );

        shape.Invalidate();
    }
}

//-----------------------------------------------------------------------

PhysicsManager::ColliderHandle PhysicsManager::CreateCollider()
{
    btCollisionObject* collisionObject = new btCollisionObject();
    collisionObject->setCollisionShape( m_EmptyCollisionShape );

    ColliderWorldState state;
    state.centerPositionOffset = btVector3( btScalar( 0.0f ), btScalar( 0.0f ), btScalar( 0.0f ) );
    state.currentlyInThePhysicsWorld = false;

    m_ColliderWorldStateList.push_back( state );

    return ColliderHandle( collisionObject, --m_ColliderWorldStateList.end() );
}

//-----------------------------------------------------------------------

void PhysicsManager::DestroyCollider(ColliderHandle& collider)
{
    assert( collider.IsValid() && "Collider handle must be valid" );

    if( collider.IsValid() )
    {
        RemoveColliderFromTheWorld( collider );

        m_ColliderWorldStateList.erase( collider.m_ColliderWorldStateListIt );
        delete collider.m_CollisionObject;
        collider.m_CollisionObject = nullptr;
        collider.Invalidate();
    }
}

//-----------------------------------------------------------------------

void PhysicsManager::AddColliderToTheWorld(ColliderHandle collider)
{
    assert( collider.IsValid() && "Collider handle must be valid" );

    bool& inPhysicsWorld = collider.m_ColliderWorldStateListIt->currentlyInThePhysicsWorld;

    if( !inPhysicsWorld )
    {
        m_DynamicsWorld->addCollisionObject( collider.m_CollisionObject );
        inPhysicsWorld = true;
    }
}

//-----------------------------------------------------------------------

void PhysicsManager::RemoveColliderFromTheWorld(ColliderHandle collider)
{
    assert( collider.IsValid() && "Collider handle must be valid" );

    bool& inPhysicsWorld = collider.m_ColliderWorldStateListIt->currentlyInThePhysicsWorld;

    if( inPhysicsWorld )
    {
        m_DynamicsWorld->removeCollisionObject( collider.m_CollisionObject );
        inPhysicsWorld = false;
    }
}

//-----------------------------------------------------------------------

void PhysicsManager::SetCollidersCollisionShape(ColliderHandle& collider,
                                                CollisionShapeHandle& collisionShape)
{
    assert( collider.IsValid() && "Collider handle must be valid" );

    const bool colliderWasInThePhysicsWorld = collider.m_ColliderWorldStateListIt->currentlyInThePhysicsWorld;

    RemoveColliderFromTheWorld( collider );

    collider.m_CollisionObject->setCollisionShape( *collisionShape.m_CollisionShapeListIt );

    if( colliderWasInThePhysicsWorld )
    {
        AddColliderToTheWorld( collider );
    }
}

//-----------------------------------------------------------------------

void PhysicsManager::AddAction(btActionInterface *action)
{
    m_DynamicsWorld->addAction( action );
}

//-----------------------------------------------------------------------

PhysicsManager::RigidBodyHandle PhysicsManager::CreateRigidBody(bool kinematic,
                                                                float mass,
                                                                PhysicsMotionStateInterface* motionState)
{
    // Using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects
    btRigidBody::btRigidBodyConstructionInfo rbInfo( btScalar( mass ), motionState, m_EmptyCollisionShape );
    btRigidBody* body = new btRigidBody( rbInfo );

    if( kinematic )
    {
        body->setMassProps( 0.0f, btVector3( 0.0f, 0.0f, 0.0f ) );
        body->setCollisionFlags( body->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT );//| btCollisionObject::CF_NO_CONTACT_RESPONSE );
        body->setLinearVelocity( btVector3( 0.0f, 0.0f, 0.0f ) );
        body->setAngularVelocity( btVector3( 0.0f, 0.0f, 0.0f ) );
        body->setActivationState( DISABLE_DEACTIVATION );
    }

    RigidBodyWorldState state;
    state.mass = mass;
    state.localInertia = btVector3( btScalar( 0.0f ), btScalar( 0.0f ), btScalar( 0.0f ) );
    state.currentlyInThePhysicsWorld = false;

    m_RigidBodyWorldStateList.push_back( state );

    return RigidBodyHandle( body, --m_RigidBodyWorldStateList.end() );
}

//-----------------------------------------------------------------------

void PhysicsManager::DestroyRigidBody(RigidBodyHandle& body)
{
    assert( body.IsValid() && "Rigid body handle must be valid" );

    if( body.IsValid() )
    {
        RemoveRigidBodyFromTheWorld( body );

        m_RigidBodyWorldStateList.erase( body.m_RigidBodyWorldStateListIt );
        delete body.m_RigidBody;
        body.m_RigidBody = nullptr;
        body.Invalidate();
    }
}

//-----------------------------------------------------------------------

void PhysicsManager::AddRigidBodyToTheWorld(RigidBodyHandle body)
{
    assert( body.IsValid() && "Rigid body handle must be valid" );

    bool& inPhysicsWorld = body.m_RigidBodyWorldStateListIt->currentlyInThePhysicsWorld;

    if( !inPhysicsWorld )
    {
        m_DynamicsWorld->addRigidBody( body.m_RigidBody );
        inPhysicsWorld = true;
    }
}

//-----------------------------------------------------------------------

void PhysicsManager::RemoveRigidBodyFromTheWorld(RigidBodyHandle body)
{
    assert( body.IsValid() && "Rigid body handle must be valid" );

    bool& inPhysicsWorld = body.m_RigidBodyWorldStateListIt->currentlyInThePhysicsWorld;

    if( inPhysicsWorld )
    {
        m_DynamicsWorld->removeRigidBody( body.m_RigidBody );
        inPhysicsWorld = false;
    }
}

//-----------------------------------------------------------------------

void PhysicsManager::AttachCollidersToARigidBody(RigidBodyHandle& body,
                                                 ColliderHandleList& colliders)
{
    assert( body.IsValid() && "Rigid body handle must be valid" );

    if( colliders.size() >= 1 )
    {
        const bool rigidBodyWasInThePhysicsWorld = body.m_RigidBodyWorldStateListIt->currentlyInThePhysicsWorld;

        RemoveRigidBodyFromTheWorld( body );

        btCollisionShape* existingCollisionShape = body.m_RigidBody->getCollisionShape();
        body.m_RigidBody->setCollisionShape( m_EmptyCollisionShape );
        if( existingCollisionShape->getShapeType() == COMPOUND_SHAPE_PROXYTYPE )
        {
            delete existingCollisionShape;
            existingCollisionShape = nullptr;
        }

        // The only way to handle colliders with non zero center position offset in Bullet is to offset them
        // from the center using compound shape, hence the "_CollidersCenterPositionOffsetSet()" check.
        if( ( colliders.size() == 1 ) && !_CollidersCenterPositionOffsetSet( colliders[0] ) )
        {
            ColliderHandle& collider = colliders[0];

            btCollisionShape* collisionShape = collider.m_CollisionObject->getCollisionShape();
            body.m_RigidBody->setCollisionShape( collisionShape );
        }
        else
        {
            /**
             * Combine collision shapes of all of the colliders into one compound
             * collision shape and assign it to a given rigid body
             */

            btTransform rigidBodyGlobalTransform;
            _GetRigidBodyFullTransform( body, rigidBodyGlobalTransform );
            const btTransform inverseRigidBodyGlobalTransform = rigidBodyGlobalTransform.inverse();

            btCompoundShape* compoundCollisionShape = new btCompoundShape( true );

            for(ColliderHandleList::iterator it = colliders.begin(); it != colliders.end(); ++it)
            {
                const btTransform& colliderGlobalTransform = _GetColliderFullTransform( *it );
                const btTransform relativeTransform = ( inverseRigidBodyGlobalTransform * colliderGlobalTransform );

                btCollisionShape* collisionShape = ( *it ).m_CollisionObject->getCollisionShape();
                assert( ( collisionShape != m_EmptyCollisionShape ) && "Collider can not have an empty shape" );

                compoundCollisionShape->addChildShape( relativeTransform, collisionShape );
            }

            body.m_RigidBody->setCollisionShape( compoundCollisionShape );
        }

        if( !IsRigidBodyKinematic( body ) )
        {
            _RecalculateLocalInertiaOfABulletRigidBody( body );
        }

        if( rigidBodyWasInThePhysicsWorld )
        {
            AddRigidBodyToTheWorld( body );
        }
    }
}

//-----------------------------------------------------------------------

void PhysicsManager::DetachCollidersFromARigidBody(RigidBodyHandle& body)
{
    assert( body.IsValid() && "Rigid body handle must be valid" );

    const bool rigidBodyWasInThePhysicsWorld = body.m_RigidBodyWorldStateListIt->currentlyInThePhysicsWorld;

    RemoveRigidBodyFromTheWorld( body );

    btCollisionShape* existingCollisionShape = body.m_RigidBody->getCollisionShape();
    body.m_RigidBody->setCollisionShape( m_EmptyCollisionShape );

    if( existingCollisionShape->getShapeType() == COMPOUND_SHAPE_PROXYTYPE )
    {
        delete existingCollisionShape;
        existingCollisionShape = nullptr;
    }

    if( rigidBodyWasInThePhysicsWorld )
    {
        AddRigidBodyToTheWorld( body );
    }
}

//-----------------------------------------------------------------------

void PhysicsManager::SwitchRigidBodyToKinematicMode(RigidBodyHandle& body)
{
    assert( body.IsValid() && "Rigid body handle must be valid" );

    if( !IsRigidBodyKinematic( body ) ) // If not already kinematic
    {
        const bool rigidBodyWasInThePhysicsWorld = body.m_RigidBodyWorldStateListIt->currentlyInThePhysicsWorld;
        RemoveRigidBodyFromTheWorld( body );

        body.m_RigidBody->setMassProps( 0.0f, btVector3( 0.0f, 0.0f, 0.0f ) );
        body.m_RigidBody->setCollisionFlags( body.m_RigidBody->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT );//| btCollisionObject::CF_NO_CONTACT_RESPONSE );
        body.m_RigidBody->setLinearVelocity( btVector3( 0.0f, 0.0f, 0.0f ) );
        body.m_RigidBody->setAngularVelocity( btVector3( 0.0f, 0.0f, 0.0f ) );
        body.m_RigidBody->setActivationState( DISABLE_DEACTIVATION );

        if( rigidBodyWasInThePhysicsWorld )
        {
            AddRigidBodyToTheWorld( body );
        }
    }
}

//-----------------------------------------------------------------------

void PhysicsManager::SwitchRigidBodyToDynamicMode(RigidBodyHandle& body)
{
    assert( body.IsValid() && "Rigid body handle must be valid" );

    if( IsRigidBodyKinematic( body ) ) // If not already dynamic
    {
        const bool rigidBodyWasInThePhysicsWorld = body.m_RigidBodyWorldStateListIt->currentlyInThePhysicsWorld;
        RemoveRigidBodyFromTheWorld( body );

        _RecalculateLocalInertiaOfABulletRigidBody( body );

        body.m_RigidBody->setCollisionFlags( btCollisionObject::CF_STATIC_OBJECT );
        body.m_RigidBody->setLinearVelocity( btVector3( 0.0f, 0.0f, 0.0f ) );
        body.m_RigidBody->setAngularVelocity( btVector3( 0.0f, 0.0f, 0.0f ) );
        body.m_RigidBody->setActivationState( WANTS_DEACTIVATION );

        if( rigidBodyWasInThePhysicsWorld )
        {
            AddRigidBodyToTheWorld( body );
        }
    }
}

//-----------------------------------------------------------------------

bool PhysicsManager::IsRigidBodyKinematic(RigidBodyHandle& body)
{
    assert( body.IsValid() && "Rigid body handle must be valid" );

    bool retVal = true;

    if( ( body.m_RigidBody->getCollisionFlags() & btCollisionObject::CF_KINEMATIC_OBJECT ) == 0 )
    {
        retVal = false;
    }

    return retVal;
}

//-----------------------------------------------------------------------

const RenderableObject* PhysicsManager::GetVisualDebugDataRenderable()
{
    return m_DebugDataDrawer->GetRenderable();
}

//-----------------------------------------------------------------------

void PhysicsManager::RefreshDebugDataMesh()
{
    m_DebugDataDrawer->ClearDebugDataCache();
    m_DynamicsWorld->debugDrawWorld();
}

//-----------------------------------------------------------------------

void PhysicsManager::CastARay(const glm::vec3& rayOrigin, const glm::vec3& rayDirection, GameObject*& outGameObject)
{
    outGameObject = nullptr;

    const btVector3 origin( rayOrigin.x, rayOrigin.y, rayOrigin.z );
    btVector3 direction( rayDirection.x, rayDirection.y, rayDirection.z );
    direction *= 1000.0f;

    btCollisionWorld::ClosestRayResultCallback rayCallback( origin, direction );
    m_DynamicsWorld->rayTest( origin, direction, rayCallback );

    if( rayCallback.hasHit() )
    {
        outGameObject = static_cast<GameObject*>( rayCallback.m_collisionObject->getUserPointer() );
    }
}

//-----------------------------------------------------------------------

void PhysicsManager::SetRigidBodyMass(RigidBodyHandle& body, float mass)
{
    assert( body.IsValid() && "Rigid body handle must be valid" );

    if( GetRigidBodyMass( body ) != mass )
    {
        body.m_RigidBodyWorldStateListIt->mass = mass;
        _RecalculateLocalInertiaOfABulletRigidBody( body );
    }
}

//-----------------------------------------------------------------------

float PhysicsManager::GetRigidBodyMass(const RigidBodyHandle& body) const
{
    assert( body.IsValid() && "Rigid body handle must be valid" );

    return body.m_RigidBodyWorldStateListIt->mass;
}

//-----------------------------------------------------------------------

void PhysicsManager::SetRigidBodyLinearDrag(RigidBodyHandle& body, float drag)
{
    assert( body.IsValid() && "Rigid body handle must be valid" );

    const bool rigidBodyWasInThePhysicsWorld = body.m_RigidBodyWorldStateListIt->currentlyInThePhysicsWorld;
    RemoveRigidBodyFromTheWorld( body );

    body.m_RigidBody->setDamping( btScalar( drag ), GetRigidBodyAngularDrag( body ) );

    if( rigidBodyWasInThePhysicsWorld )
    {
        AddRigidBodyToTheWorld( body );
    }
}

//-----------------------------------------------------------------------

float PhysicsManager::GetRigidBodyLinearDrag(const RigidBodyHandle& body) const
{
    assert( body.IsValid() && "Rigid body handle must be valid" );

    return body.m_RigidBody->getLinearDamping();
}

//-----------------------------------------------------------------------

void PhysicsManager::SetRigidBodyAngularDrag(RigidBodyHandle& body, float drag)
{
    assert( body.IsValid() && "Rigid body handle must be valid" );

    const bool rigidBodyWasInThePhysicsWorld = body.m_RigidBodyWorldStateListIt->currentlyInThePhysicsWorld;
    RemoveRigidBodyFromTheWorld( body );

    body.m_RigidBody->setDamping( GetRigidBodyLinearDrag( body ), btScalar( drag ) );

    if( rigidBodyWasInThePhysicsWorld )
    {
        AddRigidBodyToTheWorld( body );
    }
}

//-----------------------------------------------------------------------

float PhysicsManager::GetRigidBodyAngularDrag(const RigidBodyHandle& body) const
{
    assert( body.IsValid() && "Rigid body handle must be valid" );

    return body.m_RigidBody->getAngularDamping();
}

//-----------------------------------------------------------------------

void PhysicsManager::SetRigidBodyPositionAxisFrozen(const RigidBodyHandle& body,
                                                    const bool freezeAxis[3])
{
    assert( body.IsValid() && "Rigid body handle must be valid" );

    const bool rigidBodyWasInThePhysicsWorld = body.m_RigidBodyWorldStateListIt->currentlyInThePhysicsWorld;
    RemoveRigidBodyFromTheWorld( body );

    btVector3 linearFactor = btVector3( btScalar( freezeAxis[0] ? 0.0f : 1.0f ),
                                        btScalar( freezeAxis[1] ? 0.0f : 1.0f ),
                                        btScalar( freezeAxis[2] ? 0.0f : 1.0f ) );

    body.m_RigidBody->setLinearFactor( linearFactor );

    if( rigidBodyWasInThePhysicsWorld )
    {
        AddRigidBodyToTheWorld( body );
    }
}

//-----------------------------------------------------------------------

float PhysicsManager::GetCapsuleCollisionShapeRadius(const CollisionShapeHandle& shape)
{
    assert( shape.IsValid() && "Collision shape handle must be valid" );
    assert( ( ( *shape.m_CollisionShapeListIt )->getShapeType() == CAPSULE_SHAPE_PROXYTYPE ) && "Must be a btCapsuleShape" );

    return static_cast<btCapsuleShape*>( *shape.m_CollisionShapeListIt )->getRadius();
}

//-----------------------------------------------------------------------

float PhysicsManager::GetCapsuleCollisionShapeHeight(const CollisionShapeHandle& shape)
{
    assert( shape.IsValid() && "Collision shape handle must be valid" );
    assert( ( ( *shape.m_CollisionShapeListIt )->getShapeType() == CAPSULE_SHAPE_PROXYTYPE ) && "Must be a btCapsuleShape" );
    
    return ( static_cast<btCapsuleShape*>( *shape.m_CollisionShapeListIt )->getHalfHeight() * 2.0f );
}

//-----------------------------------------------------------------------

float PhysicsManager::GetConeCollisionShapeRadius(const CollisionShapeHandle& shape)
{
    assert( shape.IsValid() && "Collision shape handle must be valid" );
    assert( ( ( *shape.m_CollisionShapeListIt )->getShapeType() == CONE_SHAPE_PROXYTYPE ) && "Must be a btConeShape" );

    return static_cast<btConeShape*>( *shape.m_CollisionShapeListIt )->getRadius();
}

//-----------------------------------------------------------------------

float PhysicsManager::GetConeCollisionShapeHeight(const CollisionShapeHandle& shape)
{
    assert( shape.IsValid() && "Collision shape handle must be valid" );
    assert( ( ( *shape.m_CollisionShapeListIt )->getShapeType() == CONE_SHAPE_PROXYTYPE ) && "Must be a btConeShape" );

    return ( static_cast<btConeShape*>( *shape.m_CollisionShapeListIt )->getHeight() );
}

//-----------------------------------------------------------------------

glm::vec3 PhysicsManager::GetBoxCollisionShapeSize(const CollisionShapeHandle& shape)
{
    assert( shape.IsValid() && "Collision shape handle must be valid" );
    assert( ( ( *shape.m_CollisionShapeListIt )->getShapeType() == BOX_SHAPE_PROXYTYPE ) && "Must be a btBoxShape" );

    const btVector3& halfExtents = static_cast<btBoxShape*>( *shape.m_CollisionShapeListIt )->getHalfExtentsWithMargin();

    return glm::vec3( halfExtents.x(), halfExtents.y(), halfExtents.z() );
}

//-----------------------------------------------------------------------

float PhysicsManager::GetSphereCollisionShapeRadius(const CollisionShapeHandle& shape)
{
    assert( shape.IsValid() && "Collision shape handle must be valid" );
    assert( ( ( *shape.m_CollisionShapeListIt )->getShapeType() == SPHERE_SHAPE_PROXYTYPE ) && "Must be a btSphereShape" );

    return static_cast<btSphereShape*>( *shape.m_CollisionShapeListIt )->getRadius();
}

//-----------------------------------------------------------------------

void PhysicsManager::GetRigidBodyPositionAxisFrozen(const RigidBodyHandle& body,
                                                    bool outFreezeAxis[3]) const
{
    assert( body.IsValid() && "Rigid body handle must be valid" );

    const btVector3& linearFactor = body.m_RigidBody->getLinearFactor();

    outFreezeAxis[0] = ( ( linearFactor.x() > 0.0f ) ? false : true );
    outFreezeAxis[1] = ( ( linearFactor.y() > 0.0f ) ? false : true );
    outFreezeAxis[2] = ( ( linearFactor.z() > 0.0f ) ? false : true );
}

//-----------------------------------------------------------------------

void PhysicsManager::SetRigidBodyRotationAxisFrozen(const RigidBodyHandle& body,
                                                    const bool freezeAxis[3])
{
    assert( body.IsValid() && "Rigid body handle must be valid" );

    const bool rigidBodyWasInThePhysicsWorld = body.m_RigidBodyWorldStateListIt->currentlyInThePhysicsWorld;
    RemoveRigidBodyFromTheWorld( body );

    btVector3 angularFactor = btVector3( btScalar( freezeAxis[0] ? 0.0f : 1.0f ),
                                         btScalar( freezeAxis[1] ? 0.0f : 1.0f ),
                                         btScalar( freezeAxis[2] ? 0.0f : 1.0f ) );

    body.m_RigidBody->setAngularFactor( angularFactor );

    if( rigidBodyWasInThePhysicsWorld )
    {
        AddRigidBodyToTheWorld( body );
    }
}

//-----------------------------------------------------------------------

void PhysicsManager::GetRigidBodyRotationAxisFrozen(const RigidBodyHandle& body,
                                                    bool outFreezeAxis[3]) const
{
    assert( body.IsValid() && "Rigid body handle must be valid" );

    const btVector3& angularFactor = body.m_RigidBody->getAngularFactor();

    outFreezeAxis[0] = ( ( angularFactor.x() > 0.0f ) ? false : true );
    outFreezeAxis[1] = ( ( angularFactor.y() > 0.0f ) ? false : true );
    outFreezeAxis[2] = ( ( angularFactor.z() > 0.0f ) ? false : true );
}

//-----------------------------------------------------------------------

glm::vec3 PhysicsManager::GetRigidBodyPosition(const RigidBodyHandle& body)
{
    assert( body.IsValid() && "Rigid body handle must be valid" );

    btTransform t;
    body.m_RigidBody->getMotionState()->getWorldTransform( t );
    return glm::vec3( static_cast<float>( t.getOrigin().getX() ),
                      static_cast<float>( t.getOrigin().getY() ),
                      static_cast<float>( t.getOrigin().getZ() ) );
}

//-----------------------------------------------------------------------

glm::quat PhysicsManager::GetRigidBodyRotation(const RigidBodyHandle& body)
{
    assert( body.IsValid() && "Rigid body handle must be valid" );

    btTransform t;
    body.m_RigidBody->getMotionState()->getWorldTransform( t );
    btQuaternion q = t.getRotation();
    return glm::quat( q.w(), q.x(), q.y(), q.z() );
}

//-----------------------------------------------------------------------

glm::mat4 PhysicsManager::GetRigidBodyFullTransform(const RigidBodyHandle& body)
{
    assert( body.IsValid() && "Rigid body handle must be valid" );

    glm::mat4 ATTRIBUTE_ALIGNED16( worldTrans ); // 16 byte alignment is needed to make SSE instructions work

    btTransform t;
    _GetRigidBodyFullTransform( body, t );

    t.getOpenGLMatrix( glm::value_ptr( worldTrans ) );

    return worldTrans;
}

//-----------------------------------------------------------------------

void PhysicsManager::SetRigidBodyUserPointer(RigidBodyHandle& body,
                                             void* userPointer)
{
    assert( body.IsValid() && "Rigid body handle must be valid" );

    body.m_RigidBody->setUserPointer( userPointer );
}

//-----------------------------------------------------------------------

void* PhysicsManager::GetRigidBodyUserPointer(RigidBodyHandle& body)
{
    assert( body.IsValid() && "Rigid body handle must be valid" );

    return body.m_RigidBody->getUserPointer();
}

//-----------------------------------------------------------------------

void PhysicsManager::SetColliderCenterPositionOffset(ColliderHandle& collider,
                                                     const glm::vec3& centerPosition)
{

    btTransform worldTransWithoutThePositionOffset;
    _GetBulletColliderTransformWithoutCenterPositionOffset( collider, worldTransWithoutThePositionOffset );

    // Set the new offset
    collider.m_ColliderWorldStateListIt->centerPositionOffset = btVector3( btScalar( centerPosition.x ),
                                                                           btScalar( centerPosition.y ),
                                                                           btScalar( centerPosition.z ) );

    // Setting the transform again will take into account the new center position offset
    _SetBulletColliderTransformWithoutCenterPositionOffset( collider, worldTransWithoutThePositionOffset );
}

//-----------------------------------------------------------------------

void PhysicsManager::SetColliderTransform(ColliderHandle& collider,
                                          const SceneTypes::Transform& t)
{
    btTransform worldTrans;
    worldTrans.setIdentity();
    worldTrans.setOrigin( btVector3( t.position.x, t.position.y, t.position.z ) );
    worldTrans.setRotation( btQuaternion ( t.rotation.x,
                                           t.rotation.y,
                                           t.rotation.z,
                                           t.rotation.w ) );

    _SetBulletColliderTransformWithoutCenterPositionOffset( collider, worldTrans );
}

//-----------------------------------------------------------------------

void PhysicsManager::SetColliderPosition(ColliderHandle& collider,
                                         const glm::vec3& position)
{
    assert( collider.IsValid() && "Collider handle must be valid" );

    btTransform t;
    _GetBulletColliderTransformWithoutCenterPositionOffset( collider, t );

    t.setOrigin( btVector3( btScalar( position.x ),
                            btScalar( position.y ),
                            btScalar( position.z ) ) );

    _SetBulletColliderTransformWithoutCenterPositionOffset( collider, t );
}

//-----------------------------------------------------------------------

void PhysicsManager::SetColliderRotation(ColliderHandle& collider,
                                         const glm::quat& rotation)
{
    assert( collider.IsValid() && "Collider handle must be valid" );

    // Note: Is not affected by the center position offset,
    //       hence no special actions in this function.

    btTransform& t = collider.m_CollisionObject->getWorldTransform();
    t.setRotation( btQuaternion( btScalar( rotation.x ),
                                 btScalar( rotation.y ),
                                 btScalar( rotation.z ),
                                 btScalar( rotation.w ) ) );
}

//-----------------------------------------------------------------------

glm::vec3 PhysicsManager::GetColliderCenterPositionOffset(const ColliderHandle& collider)
{
    const btVector3& offset = collider.m_ColliderWorldStateListIt->centerPositionOffset;
    return glm::vec3( offset.x(), offset.y(), offset.z() );
}

//-----------------------------------------------------------------------

glm::vec3 PhysicsManager::GetColliderPosition(const ColliderHandle& collider)
{
    assert( collider.IsValid() && "Collider handle must be valid" );

    btTransform t;
    _GetBulletColliderTransformWithoutCenterPositionOffset( collider, t );
    const btVector3& p = t.getOrigin();
    return glm::vec3( static_cast<float>( p.getX() ),
                      static_cast<float>( p.getY() ),
                      static_cast<float>( p.getZ() ) );
}

//-----------------------------------------------------------------------

glm::quat PhysicsManager::GetColliderRotation(const ColliderHandle& collider)
{
    assert( collider.IsValid() && "Collider handle must be valid" );

    // Note: Is not affected by the center position offset,
    //       hence no special actions in this function.

    const btTransform& t = collider.m_CollisionObject->getWorldTransform();
    btQuaternion q = t.getRotation();
    return glm::quat( q.w(), q.x(), q.y(), q.z() );
}

//-----------------------------------------------------------------------

glm::mat4 PhysicsManager::GetColliderFullTransform(const ColliderHandle& collider)
{
    assert( collider.IsValid() && "Collider handle must be valid" );

    glm::mat4 ATTRIBUTE_ALIGNED16( worldTrans ); // 16 byte alignment is needed to make SSE instructions work

    btTransform t;
    _GetBulletColliderTransformWithoutCenterPositionOffset( collider, t );

    t.getOpenGLMatrix( glm::value_ptr( worldTrans ) );

    return worldTrans;
}

//-----------------------------------------------------------------------

void PhysicsManager::SetColliderUserPointer(ColliderHandle& collider,
                                            void* userPointer)
{
    assert( collider.IsValid() && "Collider handle must be valid" );

    collider.m_CollisionObject->setUserPointer( userPointer );
}

//-----------------------------------------------------------------------

void* PhysicsManager::GetColliderUserPointer(ColliderHandle& collider)
{
    assert( collider.IsValid() && "Collider handle must be valid" );

    return collider.m_CollisionObject->getUserPointer();
}

//-----------------------------------------------------------------------

void PhysicsManager::_SetBulletColliderTransformWithoutCenterPositionOffset(ColliderHandle& collider,
                                                                            const btTransform& tansform)
{
    btTransform worldTrans = tansform;
    btVector3& origin = worldTrans.getOrigin();

    // Apply the center position offset
    origin += collider.m_ColliderWorldStateListIt->centerPositionOffset;

    _SetColliderFullTransform( collider, worldTrans );
}

//-----------------------------------------------------------------------

void PhysicsManager::_GetBulletColliderTransformWithoutCenterPositionOffset(const ColliderHandle& collider,
                                                                            btTransform& tansform)
{
    tansform = _GetColliderFullTransform( collider );
    btVector3& origin = tansform.getOrigin();

    // Remove the center position offset
    origin -= collider.m_ColliderWorldStateListIt->centerPositionOffset;
}

//-----------------------------------------------------------------------

void PhysicsManager::_DestroyBulletCollisionShape(btCollisionShape* collisionShape)
{
    if( collisionShape->getShapeType() == TRIANGLE_MESH_SHAPE_PROXYTYPE )
    {
        btTriangleMeshShape* triangleMeshCollisionShape = static_cast<btTriangleMeshShape*>( collisionShape );
        btStridingMeshInterface* meshInterface = triangleMeshCollisionShape->getMeshInterface();

        if( meshInterface )
        {
            delete meshInterface;
        }
    }

    delete collisionShape;
}

//-----------------------------------------------------------------------

void PhysicsManager::_RecalculateLocalInertiaOfABulletRigidBody(const RigidBodyHandle& body)
{
    const bool rigidBodyWasInThePhysicsWorld = body.m_RigidBodyWorldStateListIt->currentlyInThePhysicsWorld;
    RemoveRigidBodyFromTheWorld( body );

    btVector3 localInertia;

    if( body.m_RigidBody->getCollisionShape()->getShapeType() == COMPOUND_SHAPE_PROXYTYPE )
    {
        btTransform principalTransform; // Principal transform is not used at the moment, but may be used in the future
        btCompoundShape* compoundCollisionShape = static_cast<btCompoundShape*>( body.m_RigidBody->getCollisionShape() );

        _CalculateLocalInertiaOfABulletCompoundCollisionShape( compoundCollisionShape,
                                                               body.m_RigidBodyWorldStateListIt->mass,
                                                               principalTransform,
                                                               localInertia );
    }
    else
    {
        body.m_RigidBody->getCollisionShape()->calculateLocalInertia( body.m_RigidBodyWorldStateListIt->mass, localInertia );
    }

    body.m_RigidBody->setMassProps( body.m_RigidBodyWorldStateListIt->mass, localInertia );
    body.m_RigidBody->updateInertiaTensor();

    body.m_RigidBodyWorldStateListIt->localInertia = localInertia;

    if( rigidBodyWasInThePhysicsWorld )
    {
        AddRigidBodyToTheWorld( body );
    }
}

//-----------------------------------------------------------------------

void PhysicsManager::_CalculateLocalInertiaOfABulletCompoundCollisionShape(btCompoundShape* compoundCollisionShape,
                                                                           float totalObjectMass,
                                                                           btTransform& outprincipalTransform,
                                                                           btVector3& outLocalInertia)
{
    const size_t childCount = compoundCollisionShape->getNumChildShapes();
    const float massPerChild = ( totalObjectMass / static_cast<float>( childCount ) );
    btScalar* compoundCollisionShapeChildrenMasses = new btScalar[childCount];

    for(size_t i = 0; i < childCount; i++)
    {
        compoundCollisionShapeChildrenMasses[i] = btScalar( massPerChild );
    }

    compoundCollisionShape->calculatePrincipalAxisTransform( compoundCollisionShapeChildrenMasses,
                                                             outprincipalTransform,
                                                             outLocalInertia );
    delete compoundCollisionShapeChildrenMasses;
}

//-----------------------------------------------------------------------

void PhysicsManager::_SetColliderFullTransform(ColliderHandle& collider, const btTransform& transform)
{
    assert( collider.IsValid() && "Collider handle must be valid" );

    collider.m_CollisionObject->setWorldTransform( transform );
}

//-----------------------------------------------------------------------

const btTransform& PhysicsManager::_GetColliderFullTransform(const ColliderHandle& collider)
{
    assert( collider.IsValid() && "Collider handle must be valid" );

    return collider.m_CollisionObject->getWorldTransform();
}

//-----------------------------------------------------------------------

void PhysicsManager::_GetRigidBodyFullTransform(const RigidBodyHandle& body,
                                                btTransform& transform)
{
    assert( body.IsValid() && "Rigid body handle must be valid" );

    body.m_RigidBody->getMotionState()->getWorldTransform( transform );
}

//-----------------------------------------------------------------------

bool PhysicsManager::_CollidersCenterPositionOffsetSet(const ColliderHandle& collider) const
{
    return ( !collider.m_ColliderWorldStateListIt->centerPositionOffset.isZero() );
}
