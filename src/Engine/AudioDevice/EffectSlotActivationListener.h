/**
 *  Copyright (c) Gytis Sabaliauskas - All Rights Reserved
 *
 *  Copying of this file via any medium and redistribution and use
 *  in source and binary forms, with or without modification, are not
 *  permitted without hand written consent of the authors.
 *
 *  Proprietary and confidential.
 */

#ifndef AUDIODEVICE_EFFECTSLOTACTIVATIONLISTENER_H_INCLUDED
#define AUDIODEVICE_EFFECTSLOTACTIVATIONLISTENER_H_INCLUDED

namespace AudioDevice
{

class EffectSlot;

class EffectSlotActivationListener
{
protected:
    friend class EffectSlot;

    virtual ~EffectSlotActivationListener() = default;

    virtual void EffectSlotActivated(EffectSlot* effectSlot) = 0;
    virtual void EffectSlotDeactivated(EffectSlot* effectSlot) = 0;
};

}

#endif // AUDIODEVICE_EFFECTSLOTACTIVATIONLISTENER_H_INCLUDED
