
uniform sampler2D u_DepthBuffer;
uniform vec2 u_ScreenSize;
uniform float u_ProjectionA;
uniform float u_ProjectionB;

layout(location = 0) out vec4 FragColor;

vec2 CalcTexCoord()
{
    return gl_FragCoord.xy / u_ScreenSize;
}

float ConvertDepthBufferSampleToLinearDepth(const in float DepthSampleFromDepthBuffer)
{
    // Convert sampled depth to linear view space Z (assume DepthSampleFromDepthBuffer
    // is a floating point value in the range [0,1])
    float LinearDepthValue = u_ProjectionB / (u_ProjectionA - DepthSampleFromDepthBuffer);

    return LinearDepthValue;
}


float ConvertDepthBufferSampleToLinearDepth2(const in float DepthSampleFromDepthBuffer)
{
    float n = 0.01f;
    float f = 60.0f;
    float c = (2.0f * n) / (f + n - DepthSampleFromDepthBuffer * (f - n));

    return c;
}


void main()
{
    float NonLinearDepthValue = texture(u_DepthBuffer, CalcTexCoord()).r;
	float LinearDepthValue = ConvertDepthBufferSampleToLinearDepth(NonLinearDepthValue);

    vec3 Color = vec3(ConvertDepthBufferSampleToLinearDepth2(NonLinearDepthValue) + (LinearDepthValue / 10000000000000000000.0f));

//	vec3 LinearDepthInColorRange = vec3(LinearDepthValue);//((LinearDepthValue + 1.0f) / 2.0f);
//	if(LinearDepthValue < 0.0f)
//    {
//        LinearDepthInColorRange = vec3(1.0f, 0.0f, 0.0f);
//    }
//    else
//    {
//        LinearDepthInColorRange = vec3(0.0f, 1.0f, 0.0f);
//    }
//    FragColor = vec4((LinearDepthInColorRange), 1.0f);

    //float gammaCorrectionFactor = (1.0f / 2.2f);
    //Color = pow(Color, vec3(gammaCorrectionFactor));

    FragColor = vec4(Color, 1.0f);
}

