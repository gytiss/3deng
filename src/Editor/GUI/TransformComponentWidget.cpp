#include "TransformComponentWidget.h"
#include <ui_TransformComponentUI.h>
#include <GUI/GUIDefaults.h>
#include <Tools/ManipulatorTool.h>
#include <CommonDefinitions.h>
#include <Engine/Scene/Transform.h>
#include <Engine/MathUtils.h>


//-----------------------------------------------------------------------

TransformComponentWidget::TransformComponentWidget(ManipulatorTool& manipulatorTool,
                                                   CategorizedListWidget* listWidgetToAddOurselvesTo,
                                                   QWidget* parent)
: ComponentWidget( listWidgetToAddOurselvesTo, "Transform", parent ),
  m_UI( new Ui::TransformComponentUI() ),
  m_TransformComponent( nullptr ),
  m_ManipulatorTool( manipulatorTool )
{
    m_UI->setupUi( this );

    connect( m_UI->xPosLineEdit, SIGNAL( editingFinished() ),
             this, SLOT( XPosLineEditEditingFinished() ) );

    connect( m_UI->yPosLineEdit, SIGNAL( editingFinished() ),
             this, SLOT( YPosLineEditEditingFinished() ) );

    connect( m_UI->zPosLineEdit, SIGNAL( editingFinished() ),
             this, SLOT( ZPosLineEditEditingFinished() ) );


    connect( m_UI->xRotLineEdit, SIGNAL( editingFinished() ),
             this, SLOT( XRotLineEditEditingFinished() ) );

    connect( m_UI->yRotLineEdit, SIGNAL( editingFinished() ),
             this, SLOT( YRotLineEditEditingFinished() ) );

    connect( m_UI->zRotLineEdit, SIGNAL( editingFinished() ),
             this, SLOT( ZRotLineEditEditingFinished() ) );


    connect( m_UI->xScaleLineEdit, SIGNAL( editingFinished() ),
             this, SLOT( XScaleLineEditEditingFinished() ) );

    connect( m_UI->yScaleLineEdit, SIGNAL( editingFinished() ),
             this, SLOT( YScaleLineEditEditingFinished() ) );

    connect( m_UI->zScaleLineEdit, SIGNAL( editingFinished() ),
             this, SLOT( ZScaleLineEditEditingFinished() ) );

    m_UI->xPosLineEdit->setValidator( &m_FloatValueValidator );
    m_UI->yPosLineEdit->setValidator( &m_FloatValueValidator );
    m_UI->zPosLineEdit->setValidator( &m_FloatValueValidator );

    m_UI->xRotLineEdit->setValidator( &m_FloatValueValidator );
    m_UI->yRotLineEdit->setValidator( &m_FloatValueValidator );
    m_UI->zRotLineEdit->setValidator( &m_FloatValueValidator );

    m_UI->xScaleLineEdit->setValidator( &m_FloatValueValidator );
    m_UI->yScaleLineEdit->setValidator( &m_FloatValueValidator );
    m_UI->zScaleLineEdit->setValidator( &m_FloatValueValidator );
}

//-----------------------------------------------------------------------

TransformComponentWidget::~TransformComponentWidget()
{
    SafeDelete( m_UI );
}

//-----------------------------------------------------------------------

void TransformComponentWidget::SetTransformComponent(Transform* transform)
{
    if( transform )
    {
        const glm::vec3& pos = transform->GetPosition();
        const glm::vec3 rotation = MathUtils::QuaternionToEulerXYZAngles( transform->GetRotation() );
        const glm::vec3& scale = transform->GetScale();

        GUIHelpers::SetNumericLineEditValue( m_UI->xPosLineEdit, pos.x );
        GUIHelpers::SetNumericLineEditValue( m_UI->yPosLineEdit, pos.y );
        GUIHelpers::SetNumericLineEditValue( m_UI->zPosLineEdit, pos.z );

        GUIHelpers::SetNumericLineEditValue( m_UI->xRotLineEdit, rotation.x );
        GUIHelpers::SetNumericLineEditValue( m_UI->yRotLineEdit, rotation.y );
        GUIHelpers::SetNumericLineEditValue( m_UI->zRotLineEdit, rotation.z );

        GUIHelpers::SetNumericLineEditValue( m_UI->xScaleLineEdit, scale.x );
        GUIHelpers::SetNumericLineEditValue( m_UI->yScaleLineEdit, scale.y );
        GUIHelpers::SetNumericLineEditValue( m_UI->zScaleLineEdit, scale.z );
    }

    m_TransformComponent = transform;
}

//-----------------------------------------------------------------------

void TransformComponentWidget::ResetPaletteToDefault()
{
    m_UI->transformComponentContentWidget->setPalette( GUIDefaults::GetSingleton().DefaultPalette() );
}

//-----------------------------------------------------------------------

void TransformComponentWidget::XPosLineEditEditingFinished()
{
    if( m_TransformComponent )
    {
        glm::vec3 pos = m_TransformComponent->GetPosition();
        pos.x = GUIHelpers::GetNumericLineEditValue( m_UI->xPosLineEdit );
        m_TransformComponent->SetPosition( pos );

        m_ManipulatorTool.RefreshPosition();
    }
}

//-----------------------------------------------------------------------

void TransformComponentWidget::YPosLineEditEditingFinished()
{
    if( m_TransformComponent )
    {
        glm::vec3 pos = m_TransformComponent->GetPosition();
        pos.y = GUIHelpers::GetNumericLineEditValue( m_UI->yPosLineEdit );
        m_TransformComponent->SetPosition( pos );

        m_ManipulatorTool.RefreshPosition();
    }
}

//-----------------------------------------------------------------------

void TransformComponentWidget::ZPosLineEditEditingFinished()
{
    if( m_TransformComponent )
    {
        glm::vec3 pos = m_TransformComponent->GetPosition();
        pos.z = GUIHelpers::GetNumericLineEditValue( m_UI->zPosLineEdit );
        m_TransformComponent->SetPosition( pos );

        m_ManipulatorTool.RefreshPosition();
    }
}

//-----------------------------------------------------------------------

void TransformComponentWidget::XRotLineEditEditingFinished()
{
    if( m_TransformComponent )
    {
        glm::vec3 rotationAngles = MathUtils::QuaternionToEulerXYZAngles( m_TransformComponent->GetRotation() );
        rotationAngles.x = GUIHelpers::GetNumericLineEditValue( m_UI->xRotLineEdit );
        m_TransformComponent->SetRotation( MathUtils::EulerXYZAnglesToQuaternion( rotationAngles ) );
    }
}

//-----------------------------------------------------------------------

void TransformComponentWidget::YRotLineEditEditingFinished()
{
    if( m_TransformComponent )
    {
        glm::vec3 rotationAngles = MathUtils::QuaternionToEulerXYZAngles( m_TransformComponent->GetRotation() );
        rotationAngles.y = GUIHelpers::GetNumericLineEditValue( m_UI->yRotLineEdit );
        m_TransformComponent->SetRotation( MathUtils::EulerXYZAnglesToQuaternion( rotationAngles ) );
    }
}

//-----------------------------------------------------------------------

void TransformComponentWidget::ZRotLineEditEditingFinished()
{
    if( m_TransformComponent )
    {
        glm::vec3 rotationAngles = MathUtils::QuaternionToEulerXYZAngles( m_TransformComponent->GetRotation() );
        rotationAngles.z = GUIHelpers::GetNumericLineEditValue( m_UI->zRotLineEdit );
        m_TransformComponent->SetRotation( MathUtils::EulerXYZAnglesToQuaternion( rotationAngles ) );
    }
}

//-----------------------------------------------------------------------

void TransformComponentWidget::XScaleLineEditEditingFinished()
{
    if( m_TransformComponent )
    {
        glm::vec3 scale = m_TransformComponent->GetScale();
        scale.x = GUIHelpers::GetNumericLineEditValue( m_UI->xPosLineEdit );
        m_TransformComponent->SetScale( scale );
    }
}

//-----------------------------------------------------------------------

void TransformComponentWidget::YScaleLineEditEditingFinished()
{
    if( m_TransformComponent )
    {
        glm::vec3 scale = m_TransformComponent->GetScale();
        scale.y = GUIHelpers::GetNumericLineEditValue( m_UI->yPosLineEdit );
        m_TransformComponent->SetScale( scale );
    }
}

//-----------------------------------------------------------------------

void TransformComponentWidget::ZScaleLineEditEditingFinished()
{
    if( m_TransformComponent )
    {
        glm::vec3 scale = m_TransformComponent->GetScale();
        scale.z = GUIHelpers::GetNumericLineEditValue( m_UI->zPosLineEdit );
        m_TransformComponent->SetScale( scale );
    }
}

//-----------------------------------------------------------------------
