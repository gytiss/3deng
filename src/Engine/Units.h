#ifndef UNITS_H_INCLUDED
#define UNITS_H_INCLUDED

/**
 * This header defines sizes of units of measurement used in the engine
 * in terms of floating point values.
 */
namespace Units
{
   static const float OneMeter = 1.0f;
   static const float OneCentimeter = 0.01f;
   static const float OneMilimeter = 0.001f;
}

#endif // UNITS_H_INCLUDED
