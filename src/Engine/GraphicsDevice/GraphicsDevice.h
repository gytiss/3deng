#ifndef GRAPHICSDEVICE_H_INCLUDED
#define GRAPHICSDEVICE_H_INCLUDED

#include "GraphicsDeviceOptions.h"
#include "GraphicsDeviceBlending.h"
#include "GraphicsDeviceDepthBuffer.h"
#include "GraphicsDeviceStencilBuffer.h"
#include <stack>
#include <bitset>

/**
 * An interface abstracting away graphics API
 */
class GraphicsDevice : public GraphicsDeviceOptions,
                        public GraphicsDeviceBlending,
                        public GraphicsDeviceDepthBuffer,
                        public GraphicsDeviceStencilBuffer
{
public:
    GraphicsDevice();

    enum BufferType
    {
        e_BufferTypeColor = GL_COLOR_BUFFER_BIT,
        e_BufferTypeDepth = GL_DEPTH_BUFFER_BIT,
        e_BufferTypeStencil = GL_STENCIL_BUFFER_BIT
    };

    enum PolygonOrientation
    {
        e_PolygonOrientationClockWise = GL_CW,
        e_PolygonOrientationCounterClockWise = GL_CCW
    };

    enum FacesType
    {
        e_FacesTypeFront = GL_FRONT,
        e_FacesTypeBack = GL_BACK,
        e_FacesTypeFrontAndBack = GL_FRONT_AND_BACK
    };

    enum DrawPrimitivesType
    {
        e_DrawPrimitivesTypePoints = GL_POINTS,
        e_DrawPrimitivesTypeLineStrip = GL_LINE_STRIP,
        e_DrawPrimitivesTypeLineLoop = GL_LINE_LOOP,
        e_DrawPrimitivesTypeLines = GL_LINES,
        e_DrawPrimitivesTypeTriangleStrip = GL_TRIANGLE_STRIP,
        e_DrawPrimitivesTypeTriangleFan = GL_TRIANGLE_FAN,
        e_DrawPrimitivesTypeTriangles = GL_TRIANGLES
    };

    enum PolygonMode
    {
#ifndef OPENGL_ES
        e_PolygonModePoint = GL_POINT,
        e_PolygonModeLine = GL_LINE,
        e_PolygonModeFill = GL_FILL
#else
        e_PolygonModePoint = 0,
        e_PolygonModeLine = 1,
        e_PolygonModeFill = 1
#endif
    };

    enum OpenGLExtenstion
    {
        OpenGLExtenstionGL_SGIX_shadow = 0,
        OpenGLExtenstionGL_SGIX_depth_texture = 1,
        OpenGLExtenstionGL_SGIS_texture_lod,
        OpenGLExtenstionGL_SGIS_generate_mipmap,
        OpenGLExtenstionGL_OES_compressed_ETC1_RGB8_texture,
        OpenGLExtenstionGL_NVX_nvenc_interop,
        OpenGLExtenstionGL_NVX_gpu_memory_info,
        OpenGLExtenstionGL_NVX_conditional_render,
        OpenGLExtenstionGL_NV_vertex_program3,
        OpenGLExtenstionGL_NV_vertex_program2_option,
        OpenGLExtenstionGL_NV_vertex_program2,
        OpenGLExtenstionGL_NV_vertex_program1_1,
        OpenGLExtenstionGL_NV_vertex_program,
        OpenGLExtenstionGL_NV_vertex_buffer_unified_memory,
        OpenGLExtenstionGL_NV_vertex_attrib_integer_64bit,
        OpenGLExtenstionGL_NV_vertex_array_range2,
        OpenGLExtenstionGL_NV_vertex_array_range,
        OpenGLExtenstionGL_NV_vdpau_interop,
        OpenGLExtenstionGL_NV_transform_feedback2,
        OpenGLExtenstionGL_NV_transform_feedback,
        OpenGLExtenstionGL_NV_texture_shader3,
        OpenGLExtenstionGL_NV_texture_shader2,
        OpenGLExtenstionGL_NV_texture_shader,
        OpenGLExtenstionGL_NV_texture_rectangle,
        OpenGLExtenstionGL_NV_texture_multisample,
        OpenGLExtenstionGL_NV_texture_expand_normal,
        OpenGLExtenstionGL_NV_texture_env_combine4,
        OpenGLExtenstionGL_NV_texture_compression_vtc,
        OpenGLExtenstionGL_NV_texture_barrier,
        OpenGLExtenstionGL_NV_texgen_reflection,
        OpenGLExtenstionGL_ARB_sparse_texture,
        OpenGLExtenstionGL_NV_shader_storage_buffer_object,
        OpenGLExtenstionGL_NV_shader_buffer_load,
        OpenGLExtenstionGL_NV_shader_atomic_float,
        OpenGLExtenstionGL_NV_shader_atomic_counters,
        OpenGLExtenstionGL_NV_register_combiners2,
        OpenGLExtenstionGL_NV_register_combiners,
        OpenGLExtenstionGL_NV_primitive_restart,
        OpenGLExtenstionGL_NV_point_sprite,
        OpenGLExtenstionGL_NV_pixel_data_range,
        OpenGLExtenstionGL_NV_path_rendering,
        OpenGLExtenstionGL_NV_parameter_buffer_object2,
        OpenGLExtenstionGL_NV_parameter_buffer_object,
        OpenGLExtenstionGL_NV_packed_depth_stencil,
        OpenGLExtenstionGL_NV_occlusion_query,
        OpenGLExtenstionGL_NV_multisample_filter_hint,
        OpenGLExtenstionGL_NV_multisample_coverage,
        OpenGLExtenstionGL_NV_light_max_exponent,
        OpenGLExtenstionGL_NV_half_float,
        OpenGLExtenstionGL_NV_gpu_shader5,
        OpenGLExtenstionGL_NV_gpu_program_fp64,
        OpenGLExtenstionGL_NV_gpu_program5_mem_extended,
        OpenGLExtenstionGL_NV_gpu_program5,
        OpenGLExtenstionGL_NV_gpu_program4_1,
        OpenGLExtenstionGL_NV_gpu_program4,
        OpenGLExtenstionGL_NV_geometry_shader4,
        OpenGLExtenstionGL_NV_framebuffer_multisample_coverage,
        OpenGLExtenstionGL_NV_fragment_program2,
        OpenGLExtenstionGL_NV_fragment_program_option,
        OpenGLExtenstionGL_NV_fragment_program,
        OpenGLExtenstionGL_NV_fog_distance,
        OpenGLExtenstionGL_NV_float_buffer,
        OpenGLExtenstionGL_NV_fence,
        OpenGLExtenstionGL_NV_explicit_multisample,
        OpenGLExtenstionGL_NV_ES1_1_compatibility,
        OpenGLExtenstionGL_NV_draw_texture,
        OpenGLExtenstionGL_NV_depth_clamp,
        OpenGLExtenstionGL_NV_depth_buffer_float,
        OpenGLExtenstionGL_NV_copy_image,
        OpenGLExtenstionGL_NV_copy_depth_to_color,
        OpenGLExtenstionGL_NV_conditional_render,
        OpenGLExtenstionGL_NV_compute_program5,
        OpenGLExtenstionGL_NV_blend_square,
        OpenGLExtenstionGL_NV_blend_equation_advanced,
        OpenGLExtenstionGL_NV_bindless_texture,
        OpenGLExtenstionGL_NV_bindless_multi_draw_indirect,
        OpenGLExtenstionGL_KTX_buffer_region,
        OpenGLExtenstionGL_KHR_debug,
        OpenGLExtenstionGL_IBM_texture_mirrored_repeat,
        OpenGLExtenstionGL_IBM_rasterpos_clip,
        OpenGLExtenstionGL_EXT_import_sync_object,
        OpenGLExtenstionGL_EXT_x11_sync_object,
        OpenGLExtenstionGL_EXT_vertex_attrib_64bit,
        OpenGLExtenstionGL_EXT_vertex_array_bgra,
        OpenGLExtenstionGL_EXT_vertex_array,
        OpenGLExtenstionGL_EXT_transform_feedback2,
        OpenGLExtenstionGL_EXT_timer_query,
        OpenGLExtenstionGL_EXT_texture_swizzle,
        OpenGLExtenstionGL_EXT_texture_storage,
        OpenGLExtenstionGL_EXT_texture_sRGB_decode,
        OpenGLExtenstionGL_EXT_texture_sRGB,
        OpenGLExtenstionGL_EXT_texture_shared_exponent,
        OpenGLExtenstionGL_EXT_texture_object,
        OpenGLExtenstionGL_EXT_texture_mirror_clamp,
        OpenGLExtenstionGL_EXT_texture_lod_bias,
        OpenGLExtenstionGL_EXT_texture_lod,
        OpenGLExtenstionGL_EXT_texture_integer,
        OpenGLExtenstionGL_EXT_texture_filter_anisotropic,
        OpenGLExtenstionGL_EXT_texture_env_dot3,
        OpenGLExtenstionGL_EXT_texture_env_combine,
        OpenGLExtenstionGL_EXT_texture_edge_clamp,
        OpenGLExtenstionGL_EXT_texture_cube_map,
        OpenGLExtenstionGL_EXT_texture_compression_s3tc,
        OpenGLExtenstionGL_EXT_texture_compression_rgtc,
        OpenGLExtenstionGL_EXT_texture_compression_latc,
        OpenGLExtenstionGL_EXT_texture_compression_dxt1,
        OpenGLExtenstionGL_EXT_texture_buffer_object,
        OpenGLExtenstionGL_EXT_texture_array,
        OpenGLExtenstionGL_EXT_texture3D,
        OpenGLExtenstionGL_EXT_stencil_wrap,
        OpenGLExtenstionGL_EXT_stencil_two_side,
        OpenGLExtenstionGL_EXT_shadow_funcs,
        OpenGLExtenstionGL_EXT_shader_image_load_store,
        OpenGLExtenstionGL_EXT_separate_specular_color,
        OpenGLExtenstionGL_EXT_separate_shader_objects,
        OpenGLExtenstionGL_EXT_secondary_color,
        OpenGLExtenstionGL_EXT_rescale_normal,
        OpenGLExtenstionGL_EXT_provoking_vertex,
        OpenGLExtenstionGL_EXT_point_parameters,
        OpenGLExtenstionGL_EXT_pixel_buffer_object,
        OpenGLExtenstionGL_EXT_packed_pixels,
        OpenGLExtenstionGL_EXT_packed_float,
        OpenGLExtenstionGL_EXT_packed_depth_stencil,
        OpenGLExtenstionGL_EXT_multi_draw_arrays,
        OpenGLExtenstionGL_EXT_gpu_shader4,
        OpenGLExtenstionGL_EXT_gpu_program_parameters,
        OpenGLExtenstionGL_EXT_geometry_shader4,
        OpenGLExtenstionGL_EXT_framebuffer_sRGB,
        OpenGLExtenstionGL_EXT_framebuffer_object,
        OpenGLExtenstionGL_EXT_framebuffer_multisample_blit_scaled,
        OpenGLExtenstionGL_EXTX_framebuffer_mixed_formats,
        OpenGLExtenstionGL_EXT_framebuffer_multisample,
        OpenGLExtenstionGL_EXT_framebuffer_blit,
        OpenGLExtenstionGL_EXT_fog_coord,
        OpenGLExtenstionGL_EXT_draw_range_elements,
        OpenGLExtenstionGL_EXT_draw_instanced,
        OpenGLExtenstionGL_EXT_draw_buffers2,
        OpenGLExtenstionGL_EXT_direct_state_access,
        OpenGLExtenstionGL_EXT_depth_bounds_test,
        OpenGLExtenstionGL_EXT_Cg_shader,
        OpenGLExtenstionGL_EXT_compiled_vertex_array,
        OpenGLExtenstionGL_EXT_blend_subtract,
        OpenGLExtenstionGL_EXT_blend_minmax,
        OpenGLExtenstionGL_EXT_blend_func_separate,
        OpenGLExtenstionGL_EXT_blend_equation_separate,
        OpenGLExtenstionGL_EXT_blend_color,
        OpenGLExtenstionGL_EXT_bindable_uniform,
        OpenGLExtenstionGL_EXT_bgra,
        OpenGLExtenstionGL_EXT_abgr,
        OpenGLExtenstionGL_EXT_texture_env_add,
        OpenGLExtenstionGL_S3_s3tc,
        OpenGLExtenstionGL_ATI_texture_mirror_once,
        OpenGLExtenstionGL_ATI_texture_float,
        OpenGLExtenstionGL_ATI_draw_buffers,
        OpenGLExtenstionGL_ARB_window_pos,
        OpenGLExtenstionGL_ARB_viewport_array,
        OpenGLExtenstionGL_ARB_vertex_type_2_10_10_10_rev,
        OpenGLExtenstionGL_ARB_vertex_type_10f_11f_11f_rev,
        OpenGLExtenstionGL_ARB_vertex_shader,
        OpenGLExtenstionGL_ARB_vertex_program,
        OpenGLExtenstionGL_ARB_vertex_buffer_object,
        OpenGLExtenstionGL_ARB_vertex_attrib_binding,
        OpenGLExtenstionGL_ARB_vertex_attrib_64bit,
        OpenGLExtenstionGL_ARB_vertex_array_object,
        OpenGLExtenstionGL_ARB_vertex_array_bgra,
        OpenGLExtenstionGL_ARB_uniform_buffer_object,
        OpenGLExtenstionGL_ARB_transpose_matrix,
        OpenGLExtenstionGL_ARB_transform_feedback_instanced,
        OpenGLExtenstionGL_ARB_transform_feedback3,
        OpenGLExtenstionGL_ARB_transform_feedback2,
        OpenGLExtenstionGL_ARB_timer_query,
        OpenGLExtenstionGL_ARB_texture_view,
        OpenGLExtenstionGL_ARB_texture_swizzle,
        OpenGLExtenstionGL_ARB_texture_storage_multisample,
        OpenGLExtenstionGL_ARB_texture_storage,
        OpenGLExtenstionGL_ARB_texture_stencil8,
        OpenGLExtenstionGL_ARB_texture_rgb10_a2ui,
        OpenGLExtenstionGL_ARB_texture_rg,
        OpenGLExtenstionGL_ARB_texture_rectangle,
        OpenGLExtenstionGL_ARB_texture_query_lod,
        OpenGLExtenstionGL_ARB_texture_query_levels,
        OpenGLExtenstionGL_ARB_texture_non_power_of_two,
        OpenGLExtenstionGL_ARB_texture_multisample,
        OpenGLExtenstionGL_ARB_texture_mirrored_repeat,
        OpenGLExtenstionGL_ARB_texture_mirror_clamp_to_edge,
        OpenGLExtenstionGL_ARB_texture_gather,
        OpenGLExtenstionGL_ARB_texture_float,
        OpenGLExtenstionGL_ARB_texture_env_dot3,
        OpenGLExtenstionGL_ARB_texture_env_crossbar,
        OpenGLExtenstionGL_ARB_texture_env_combine,
        OpenGLExtenstionGL_ARB_texture_env_add,
        OpenGLExtenstionGL_ARB_texture_cube_map_array,
        OpenGLExtenstionGL_ARB_texture_cube_map,
        OpenGLExtenstionGL_ARB_texture_compression_rgtc,
        OpenGLExtenstionGL_ARB_texture_compression_bptc,
        OpenGLExtenstionGL_ARB_texture_compression,
        OpenGLExtenstionGL_ARB_texture_buffer_range,
        OpenGLExtenstionGL_ARB_texture_buffer_object_rgb32,
        OpenGLExtenstionGL_ARB_texture_buffer_object,
        OpenGLExtenstionGL_ARB_texture_border_clamp,
        OpenGLExtenstionGL_ARB_tessellation_shader,
        OpenGLExtenstionGL_ARB_sync,
        OpenGLExtenstionGL_ARB_stencil_texturing,
        OpenGLExtenstionGL_ARB_shadow,
        OpenGLExtenstionGL_ARB_shading_language_packing,
        OpenGLExtenstionGL_ARB_shading_language_include,
        OpenGLExtenstionGL_ARB_shading_language_420pack,
        OpenGLExtenstionGL_ARB_shading_language_100,
        OpenGLExtenstionGL_ARB_shader_texture_lod,
        OpenGLExtenstionGL_ARB_shader_subroutine,
        OpenGLExtenstionGL_ARB_shader_storage_buffer_object,
        OpenGLExtenstionGL_ARB_query_buffer_object,
        OpenGLExtenstionGL_ARB_shader_precision,
        OpenGLExtenstionGL_ARB_shader_objects,
        OpenGLExtenstionGL_ARB_shader_image_size,
        OpenGLExtenstionGL_ARB_shader_image_load_store,
        OpenGLExtenstionGL_ARB_shader_group_vote,
        OpenGLExtenstionGL_ARB_shader_draw_parameters,
        OpenGLExtenstionGL_ARB_shader_bit_encoding,
        OpenGLExtenstionGL_ARB_shader_atomic_counters,
        OpenGLExtenstionGL_ARB_separate_shader_objects,
        OpenGLExtenstionGL_ARB_seamless_cubemap_per_texture,
        OpenGLExtenstionGL_ARB_seamless_cube_map,
        OpenGLExtenstionGL_ARB_sampler_objects,
        OpenGLExtenstionGL_ARB_sample_shading,
        OpenGLExtenstionGL_ARB_robustness,
        OpenGLExtenstionGL_ARB_robust_buffer_access_behavior,
        OpenGLExtenstionGL_ARB_provoking_vertex,
        OpenGLExtenstionGL_ARB_program_interface_query,
        OpenGLExtenstionGL_ARB_point_sprite,
        OpenGLExtenstionGL_ARB_point_parameters,
        OpenGLExtenstionGL_ARB_pixel_buffer_object,
        OpenGLExtenstionGL_ARB_occlusion_query2,
        OpenGLExtenstionGL_ARB_occlusion_query,
        OpenGLExtenstionGL_ARB_multitexture,
        OpenGLExtenstionGL_ARB_multisample,
        OpenGLExtenstionGL_ARB_multi_draw_indirect,
        OpenGLExtenstionGL_ARB_multi_bind,
        OpenGLExtenstionGL_ARB_map_buffer_range,
        OpenGLExtenstionGL_ARB_map_buffer_alignment,
        OpenGLExtenstionGL_ARB_invalidate_subdata,
        OpenGLExtenstionGL_ARB_internalformat_query2,
        OpenGLExtenstionGL_ARB_internalformat_query,
        OpenGLExtenstionGL_ARB_instanced_arrays,
        OpenGLExtenstionGL_ARB_indirect_parameters,
        OpenGLExtenstionGL_ARB_imaging,
        OpenGLExtenstionGL_ARB_half_float_vertex,
        OpenGLExtenstionGL_ARB_half_float_pixel,
        OpenGLExtenstionGL_ARB_gpu_shader_fp64,
        OpenGLExtenstionGL_ARB_gpu_shader5,
        OpenGLExtenstionGL_ARB_get_program_binary,
        OpenGLExtenstionGL_ARB_geometry_shader4,
        OpenGLExtenstionGL_ARB_framebuffer_sRGB,
        OpenGLExtenstionGL_ARB_framebuffer_object,
        OpenGLExtenstionGL_ARB_framebuffer_no_attachments,
        OpenGLExtenstionGL_ARB_fragment_shader,
        OpenGLExtenstionGL_ARB_fragment_program_shadow,
        OpenGLExtenstionGL_ARB_fragment_program,
        OpenGLExtenstionGL_ARB_fragment_layer_viewport,
        OpenGLExtenstionGL_ARB_fragment_coord_conventions,
        OpenGLExtenstionGL_ARB_explicit_uniform_location,
        OpenGLExtenstionGL_ARB_explicit_attrib_location,
        OpenGLExtenstionGL_ARB_ES3_compatibility,
        OpenGLExtenstionGL_ARB_ES2_compatibility,
        OpenGLExtenstionGL_ARB_enhanced_layouts,
        OpenGLExtenstionGL_ARB_draw_instanced,
        OpenGLExtenstionGL_ARB_draw_elements_base_vertex,
        OpenGLExtenstionGL_ARB_draw_indirect,
        OpenGLExtenstionGL_ARB_draw_buffers_blend,
        OpenGLExtenstionGL_ARB_draw_buffers,
        OpenGLExtenstionGL_ARB_depth_texture,
        OpenGLExtenstionGL_ARB_depth_clamp,
        OpenGLExtenstionGL_ARB_depth_buffer_float,
        OpenGLExtenstionGL_ARB_debug_output,
        OpenGLExtenstionGL_ARB_copy_image,
        OpenGLExtenstionGL_ARB_copy_buffer,
        OpenGLExtenstionGL_ARB_compute_variable_group_size,
        OpenGLExtenstionGL_ARB_compute_shader,
        OpenGLExtenstionGL_ARB_conservative_depth,
        OpenGLExtenstionGL_ARB_compressed_texture_pixel_storage,
        OpenGLExtenstionGL_ARB_compatibility,
        OpenGLExtenstionGL_ARB_color_buffer_float,
        OpenGLExtenstionGL_ARB_clear_texture,
        OpenGLExtenstionGL_ARB_clear_buffer_object,
        OpenGLExtenstionGL_ARB_buffer_storage,
        OpenGLExtenstionGL_ARB_blend_func_extended,
        OpenGLExtenstionGL_ARB_bindless_texture,
        OpenGLExtenstionGL_ARB_base_instance,
        OpenGLExtenstionGL_ARB_arrays_of_arrays,
        OpenGLExtenstionGL_AMD_seamless_cubemap_per_texture,
        OpenGLExtenstionGL_AMD_multi_draw_indirect,

        OpenGLExtenstionCount // Not an OpenGL extension(used for counting)
    };

    float GetMaxSupportedAnisotropyLevel() const;
    size_t GetMaxNumberOfDrawBuffers() const;
    bool ExtenstionIsSupported(OpenGLExtenstion openGLExt) const;

    void ClearBuffers(GLbitfield buffersToClear);

    /**
     * Request that all of the commands not yet sent (i.e. cached in the driver
     * command queue) to the GPU be sent to the GPU as soon as possible
     */
    void Flush();

    /**
     * Wait till all of the commands sent to the GPU are executed and only then return
     */
    void WaitTillFinished();

    /**
     * Note: Read pixels are in RGBA format, hence size of each pixel is four bytes.
     *
     * @param x - Horizontal position of the lower left corner of the pixel rectangle to be read (with 0 being on the left side of the frame buffer)
     * @param y - Vertical position of the lower left corner of the pixel rectangle to be read (with 0 being on the bottom of the frame buffer)
     * @param width - Width of the pixel rectangle to be read
     * @param height - Height of the pixel rectangle to be read
     * @param outData - Pointer to the array of size greater or equal to: (width * height * 4)
     */
    void ReadPixelsFromFrameBuffer(size_t x, size_t y, size_t width, size_t height, uint8_t* outData);

#ifndef OPENGL_ES
    /**
     * @param x - Horizontal position of the lower left corner of the data rectangle to be read (with 0 being on the left side of the stencil buffer)
     * @param y - Vertical position of the lower left corner of the data rectangle to be read (with 0 being on the bottom of the stencil buffer)
     * @param width - Width of the data rectangle to be read
     * @param height - Height of the data rectangle to be read
     * @param outData - Pointer to the array of size greater or equal to: (width * height)
     */
    void ReadStencilBuffer(size_t x, size_t y, size_t width, size_t height, uint8_t* outData);
#endif

    void SaveCurrentFrontFacingPolygonOrientation();
    void RestorePreviousFrontFacingPolygonOrientation();
    void SetFrontFacingPolygonOrientation(PolygonOrientation orientation);

    void SaveCurrentFacesToCullValue();
    void RestorePreviousFacesToCullValue();
    void SetFacesToCull(FacesType type);

    void SaveCurrentScissorBox();
    void RestorePreviousScissorBox();
    void SetScissorBox(const glm::vec4& scissorBox);
    void SetScissorBox(int lowerLeftCornerX, int lowerLeftCornerY, int width, int height);

    void SaveCurrentPolygonMode();
    void RestorePreviousPolygonMode();
    void SetPolygonMode(PolygonMode mode);

    void SaveCurrentPointSizeValue();
    void RestorePreviousPointSizeValue();
    void SetPointSizeValue(float sizeValue);

    void SaveCurrentViewportValue();
    void RestorePreviousViewportValue();
    void SetViewportValue(int x, int y, size_t width, size_t height);

    void SaveCurrentClearColorValue();
    void RestorePreviousClearColorValue();
    void SetClearColorValue(float r, float g, float b, float a);

private:
    struct Viewport
    {
        // Specify the lower left corner of the viewport rectangle, in pixels.
        int x;
        int y;

        // Specify the width and height of the viewport.
        size_t width;
        size_t height;
    };

    void _ReadPixelsFromFrameBuffer(size_t x,
                                    size_t y,
                                    size_t width,
                                    size_t height,
                                    GLenum pixelDataFormat,
                                    uint8_t* outData);

    void _DetermineSupportedOpenGLExtension();

    static const char* m_OpenGLExtensionNames[];
    static bool m_ClassAlreadyInstantiated;

    std::bitset<OpenGLExtenstionCount> m_OpenGLExtensionPresenceStatuses;

    std::stack<PolygonOrientation> m_FrontFacingPolygonOrientationValuesStack;
    PolygonOrientation m_CurrentFrontFacingPolygonOrientation;

    std::stack<FacesType> m_FacesToCullValuesStack;
    FacesType m_CurrentFacesToCullValue;

    std::stack<glm::vec4> m_ScissorBoxStack;
    glm::vec4 m_CurrentScissorBox;

    std::stack<PolygonMode> m_PolygonModeValuesStack;
    PolygonMode m_CurrentPolygonMode;

    std::stack<float> m_PointSizeValuesStack;
    float m_CurrentPointSizeValue;

    std::stack<Viewport> m_ViewportValuesStack;
    Viewport m_CurrentViewportValue;

    std::stack<glm::vec4> m_ClearColorValuesStack;
    glm::vec4 m_CurrentClearColor;

    float m_MaxAvailableAnisotropyLevel;
    size_t m_MaxNumberOfDrawBuffers;
};

#endif // GRAPHICSDEVICE_H_INCLUDED
