#ifndef GUIEVENTMANAGER_H_INCLUDED
#define GUIEVENTMANAGER_H_INCLUDED

#include "GUIEventHandler.h"
#include <string>
#include <map>

class GUIContext;

class GUIEventManager
{
public:
    GUIEventManager(GUIContext* context);
    ~GUIEventManager();

	/**
	 * Registers a new event handler with the manager.
	 *
	 * @param[in] handler_name The name of the handler; this must be the same as the window it is handling events for.
	 * @param[in] handler The event handler.
	 */
    void RegisterEventHandler(const std::string& handlerName,
                               GUIEventHandler* handler);

	/**
	 * Processes an event coming through from Rocket.
	 *
	 * @param[in] event The Rocket event that spawned the application event.
	 * @param[in] value The application-specific event value.
	 */
	void ProcessEvent(Rocket::Core::Event& event, const Rocket::Core::String& value);

private:
    typedef std::map<std::string, GUIEventHandler*> EventHandlerMap_t;

    EventHandlerMap_t m_EventHandlerMap;
    GUIContext* m_GUIContext;
};

#endif // GUIEVENTMANAGER_H_INCLUDED
