#ifndef GUIHELPERS_H_INCLUDED
#define GUIHELPERS_H_INCLUDED

#include <QtGui/QPalette>

class QWidget;
class QLineEdit;

class GUIHelpers
{
public:
    /**
     * Take "widgets" parent background color and copy it into "widgets"
     * palette using a given color role
     *
     * @param widget - Widget to work on
     * @param widgetRole - Widgets color role to copy parent background into
     */
    static void UseParentsPaletteProperty(QWidget& widget,
                                          QPalette::ColorRole widgetRole);

    static void SetNumericLineEditValue(QLineEdit* lineEdit, float value);

    static float GetNumericLineEditValue(QLineEdit* lineEdit);

private:
    GUIHelpers(); // This class is meant to be non instantiatable
};

#endif // GUIHELPERS_H_INCLUDED
