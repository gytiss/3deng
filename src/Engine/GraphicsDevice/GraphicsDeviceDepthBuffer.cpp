#include "GraphicsDeviceDepthBuffer.h"
#include <OpenGLErrorChecking.h>

//-----------------------------------------------------------------------

GraphicsDeviceDepthBuffer::GraphicsDeviceDepthBuffer()
: m_DepthBufferWriteEnabledStack(),
  m_DepthBufferWriteEnabled( true ),
  m_DepthFunctionsStack(),
  m_CurrentDepthFunction( e_DepthFunctionLess )
{
    // Set initial value
    glDepthMask( ( m_DepthBufferWriteEnabled ? GL_TRUE : GL_FALSE ) );
    CheckForGLErrors();

    // Set initial value
    glDepthFunc( m_CurrentDepthFunction );
    CheckForGLErrors();
}

//-----------------------------------------------------------------------

void GraphicsDeviceDepthBuffer::SaveCurrentDepthBufferWritingState()
{
    m_DepthBufferWriteEnabledStack.push( m_DepthBufferWriteEnabled );
}

//-----------------------------------------------------------------------

void GraphicsDeviceDepthBuffer::RestorePreviousDepthBufferWritingState()
{
    bool previousDepthBufferWriteEnabled = m_DepthBufferWriteEnabledStack.top();
    m_DepthBufferWriteEnabledStack.pop();

    if( previousDepthBufferWriteEnabled )
    {
        EnableDepthBufferWriting();
    }
    else
    {
        DisableDepthBufferWriting();
    }
}

//-----------------------------------------------------------------------

void GraphicsDeviceDepthBuffer::EnableDepthBufferWriting()
{
    if( !m_DepthBufferWriteEnabled )
    {
        m_DepthBufferWriteEnabled = true;
        glDepthMask( GL_TRUE );
        CheckForGLErrors();
    }
}

//-----------------------------------------------------------------------

void GraphicsDeviceDepthBuffer::DisableDepthBufferWriting()
{
    if( m_DepthBufferWriteEnabled )
    {
        m_DepthBufferWriteEnabled = false;
        glDepthMask( GL_FALSE );
        CheckForGLErrors();
    }
}

//-----------------------------------------------------------------------

void GraphicsDeviceDepthBuffer::SaveCurrentDepthFunction()
{
    m_DepthFunctionsStack.push( m_CurrentDepthFunction );
}

//-----------------------------------------------------------------------

void GraphicsDeviceDepthBuffer::RestorePreviousDepthFunction()
{
    SetDepthFunction( m_DepthFunctionsStack.top() );
    m_DepthFunctionsStack.pop();
}

//-----------------------------------------------------------------------

void GraphicsDeviceDepthBuffer::SetDepthFunction(DepthFunction depthFunction)
{
    if( depthFunction != m_CurrentDepthFunction )
    {
        m_CurrentDepthFunction = depthFunction;
        glDepthFunc( m_CurrentDepthFunction );
        CheckForGLErrors();
    }
}

//-----------------------------------------------------------------------
