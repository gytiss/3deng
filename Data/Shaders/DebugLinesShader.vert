
in vec3 in_VertexPos;
in vec3 in_VertexColor;

uniform vec3 u_Color;
uniform mat4 u_ModelViewProjectionMatrix;
uniform bool u_UseVertexColors;

out vec3 Color;

void main()
{
    if( u_UseVertexColors )
    {
        Color = in_VertexColor;
    }
    else
    {
        Color = u_Color;
    }

    gl_Position = u_ModelViewProjectionMatrix * vec4( in_VertexPos, 1.0f );
}
