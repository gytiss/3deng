
#pragma IncludeExternalSource "LightShaderCommonCode.frag"

#if !defined(USING_OPENGL_ES)
    #define COLOR_PRECISION HIGHEST_PRECISION
#else
    #define COLOR_PRECISION LOWEST_PRECISION
#endif


in HIGHEST_PRECISION vec3 VertexPos_ViewSpace;
in HIGHEST_PRECISION mat3 TBN;
in HIGHEST_PRECISION vec2 TexCoord;

uniform sampler2D u_DiffuseMap;
uniform sampler2D u_NormalMap;
uniform sampler2D u_SpecularMap;

#if defined(DIRECTIONAL_LIGHT) || defined(POINT_LIGHT) || defined(SPOT_LIGHT)
    uniform COLOR_PRECISION vec3  u_LightColor;
    uniform LOWEST_PRECISION float u_LightAmbientIntensity;
    uniform LOWEST_PRECISION float u_LightDiffuseIntensity;
#endif

#if defined(POINT_LIGHT) || defined(SPOT_LIGHT)
    uniform HIGHEST_PRECISION float u_LightRadius;
    uniform HIGHEST_PRECISION vec3  u_LightPosition_ViewSpace;
#endif

#if defined(SPOT_LIGHT)
    uniform LOWEST_PRECISION float u_LightCutoffValue;  // cos(a), where "a" is an engle between the two vectors one going from center
                                                        // of the light towards the direction of the light and the other along the
                                                        // side of the cone(towards the wider end of it). This value is used with spot lights.
#endif

#if defined(DIRECTIONAL_LIGHT) || defined(SPOT_LIGHT)
    uniform LOWEST_PRECISION vec3 u_LightDirection_ViewSpace;
#endif

out COLOR_PRECISION vec4 FragColor;

#if !defined(USING_OPENGL_ES)
vec3 SRGBColorValueToLinearColorValue(const in vec3 srgbColorValue)
{
    return vec3(pow( clamp( srgbColorValue.r, 0.0f, 1.0f ), 2.2f ),
                pow( clamp( srgbColorValue.g, 0.0f, 1.0f ), 2.2f ),
                pow( clamp( srgbColorValue.b, 0.0f, 1.0f ), 2.2f ));
}
#endif


void main()
{
#if !defined(USING_OPENGL_ES)
    COLOR_PRECISION vec3 MaterialColor = SRGBColorValueToLinearColorValue(texture(u_DiffuseMap, TexCoord).rgb);
#else
    COLOR_PRECISION vec3 MaterialColor = texture(u_DiffuseMap, TexCoord).rgb;
#endif
    LOWEST_PRECISION vec3 NormalMapNormal_TangentSpace = normalize((2.0f * texture(u_NormalMap, TexCoord).rgb) - 1.0f);
    LOWEST_PRECISION vec4 MaterialSpecularValues = texture(u_SpecularMap, TexCoord);

    LOWEST_PRECISION float specularIntensity = MaterialSpecularValues.r;
    LOWEST_PRECISION float specularPower = 128.0f;//MaterialSpecularValues.a * 255.0f;

    LOWEST_PRECISION vec3 PixelNormal_ViewSpace = (TBN * NormalMapNormal_TangentSpace);

       
#if defined(DIRECTIONAL_LIGHT) || defined(POINT_LIGHT) || defined(SPOT_LIGHT)
    HIGHEST_PRECISION vec3 eyePos_ViewSpace = vec3(0.0f, 0.0f, 0.0f); // In View Space camera is allways at the center of the coordinate system
    LOWEST_PRECISION vec3 VertexToEye = normalize(eyePos_ViewSpace - VertexPos_ViewSpace);
#endif

#if defined(POINT_LIGHT) || defined(SPOT_LIGHT)
    HIGHEST_PRECISION vec3 VertexToLightVector_ViewSpace = (VertexPos_ViewSpace - u_LightPosition_ViewSpace);
    HIGHEST_PRECISION float DistanceFromLightToVertex = length(VertexToLightVector_ViewSpace);
    LOWEST_PRECISION vec3 LightDirection_ViewSpace = ( VertexToLightVector_ViewSpace / DistanceFromLightToVertex ); // Normalize
#endif

#if defined(POINT_LIGHT)        
    COLOR_PRECISION vec4 FinalLightColor = CalcPointLight(u_LightRadius,
                                                          specularIntensity,
                                                          specularPower,
                                                          u_LightAmbientIntensity,
                                                          u_LightDiffuseIntensity,
                                                          u_LightColor,
                                                          LightDirection_ViewSpace,
                                                          DistanceFromLightToVertex,
                                                          VertexToEye,
                                                          PixelNormal_ViewSpace);
#elif defined(SPOT_LIGHT)
    COLOR_PRECISION vec4 FinalLightColor = CalcSpotLight(u_LightCutoffValue,
                                                         u_LightRadius,
                                                         specularIntensity,
                                                         specularPower,
                                                         u_LightAmbientIntensity,
                                                         u_LightDiffuseIntensity,
                                                         u_LightColor,
                                                         u_LightDirection_ViewSpace,
                                                         LightDirection_ViewSpace,
                                                         DistanceFromLightToVertex,
                                                         VertexToEye,
                                                         PixelNormal_ViewSpace);
#elif defined(DIRECTIONAL_LIGHT)
    COLOR_PRECISION vec4 FinalLightColor = CalcDirectionalLight(specularIntensity,
                                                                specularPower,
                                                                u_LightAmbientIntensity,
                                                                u_LightDiffuseIntensity,
                                                                u_LightColor,
                                                                u_LightDirection_ViewSpace,
                                                                VertexToEye,
                                                                PixelNormal_ViewSpace);
#else
    COLOR_PRECISION vec3 FinalLightColor =  MaterialColor;
#endif

    FragColor = vec4( MaterialColor * FinalLightColor.xyz, 1.0f );
}

