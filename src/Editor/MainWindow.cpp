#include "MainWindow.h"
#include <ui_MainWindow.h>
#include "GLWidget.h"
#include <GUI/CategorizedListWidget.h>
#include <Resources/FileManager.h>
#include <CommonDefinitions.h>

#include <QtOpenGL/QGLFormat>
#include <QtOpenGL/QGLContext>

//-----------------------------------------------------------------------

MainWindow::MainWindow(QWidget* parent)
: QMainWindow( parent ),
  m_UI( new Ui::EditorWindow ),
  m_FileManager( nullptr ),
  m_InputMapper( nullptr ),
  m_GLWidget( nullptr ),
  m_ComponentPanel( nullptr ),
  m_WindowIsClosed( false )
{
    m_UI->setupUi( this );

    QGLFormat glContextFormat = QGLFormat::defaultFormat();

    glContextFormat.setDoubleBuffer( true );
    glContextFormat.setDepth( true );
    glContextFormat.setDepthBufferSize( 24 );
    glContextFormat.setRgba( true );
    glContextFormat.setAlpha( true );
    glContextFormat.setAccum( true );
    glContextFormat.setStencil( true );
    glContextFormat.setStereo( false );
    glContextFormat.setDirectRendering( true );
    glContextFormat.setOverlay( false );
    glContextFormat.setSampleBuffers( false );
    glContextFormat.setPlane( QGLFormat::CoreProfile );
    glContextFormat.setVersion( 4, 3 );

    QGLContext* glContext = new QGLContext( glContextFormat );
    glContext->setFormat( glContextFormat );

    m_FileManager.reset( new FileManager() );

    /*---------------------- Input Mapper ----------------------*/
    m_InputMapper.reset( new InputMapping::InputMapper( *m_FileManager,
                                                        "Data/Config/InputMapping/InputContexts.cfg",
                                                        "Data/Config/InputMapping/InputMap.cfg",
                                                        "Data/Config/InputMapping/InputRanges.cfg" ) );
    m_InputMapper->PushContext( "Base" );
    m_InputMapper->PushContext( "GamePlay" );
    m_InputMapper->PushContext( "CharacterControl" );
    /*----------------------------------------------------------*/

    m_GLWidget = new GLWidget( *this, *m_InputMapper, *m_FileManager, glContext, this );

    m_UI->openGLViewVerticalLayout->addWidget( m_GLWidget );

    m_ComponentPanel = new CategorizedListWidget( m_UI->rightPanelVerticalLayout->parentWidget() );
    m_UI->rightPanelVerticalLayout->addWidget( m_ComponentPanel );
}

//-----------------------------------------------------------------------

MainWindow::~MainWindow()
{

}

//-----------------------------------------------------------------------

void MainWindow::closeEvent(QCloseEvent* event)
{
    QMainWindow::closeEvent( event );
    m_WindowIsClosed = true;
}

//-----------------------------------------------------------------------

void MainWindow::UpdateGL()
{
    m_GLWidget->updateGL();
}

//-----------------------------------------------------------------------
