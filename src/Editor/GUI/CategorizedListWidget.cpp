#include "CategorizedListWidget.h"
#include "SheetDelegate.h"

#include <GUI/GUIHelpers.h>

#include <QtWidgets/QHeaderView>
#include <QtWidgets/QApplication>
#include <QtWidgets/QSizePolicy>

//-----------------------------------------------------------------------

CategorizedListWidget::CategorizedListWidget(QWidget *parent)
: QTreeWidget( parent )
{
    QSizePolicy widgetSizePolicy = sizePolicy();
    widgetSizePolicy.setHorizontalPolicy( QSizePolicy::Minimum );
    setSizePolicy( widgetSizePolicy );
    setFocusPolicy( Qt::NoFocus );
    setIndentation( 0 );
    setRootIsDecorated( false );
    setColumnCount( 1 );
    header()->hide();
    header()->setSectionResizeMode( QHeaderView::Stretch );
    setTextElideMode( Qt::ElideMiddle );
    setVerticalScrollMode( ScrollPerPixel );

    GUIHelpers::UseParentsPaletteProperty( *this, QPalette::Base );

    setItemDelegate(new SheetDelegate( this, this ) );

    connect( this, SIGNAL( itemPressed( QTreeWidgetItem*, int ) ),
             this, SLOT( HandleMousePress( QTreeWidgetItem* ) ) );
}

//-----------------------------------------------------------------------

CategorizedListWidget::~CategorizedListWidget()
{
}

//-----------------------------------------------------------------------

CategorizedListWidget::Category CategorizedListWidget::AddCetegory(const QString& categoryTitle)
{
    QTreeWidgetItem* newCategory = new QTreeWidgetItem();


    //newCategory->setFlags(toolCategory->flags() | Qt::ItemIsUserCheckable);
    //newCategory->setCheckState(0, Qt::Checked);

    newCategory->setText( 0, categoryTitle );
    addTopLevelItem( newCategory );
    setItemExpanded( newCategory, true);

    m_CategoryList.push_back( newCategory );

    return newCategory;
}

//-----------------------------------------------------------------------

void CategorizedListWidget::AddCatedoryItem(Category category, QWidget* newItem)
{
    QTreeWidgetItem* categoryItemContainer = new QTreeWidgetItem( category );

    categoryItemContainer->setFlags( Qt::ItemIsEnabled );
    setItemWidget( categoryItemContainer, 0, newItem );
}

//-----------------------------------------------------------------------

void CategorizedListWidget::SetCategoryHide(Category category, float value)
{
    category->setHidden( value );
}

//-----------------------------------------------------------------------

void CategorizedListWidget::HandleMousePress(QTreeWidgetItem* item)
{
    if( item != 0 )
    {
        if( QApplication::mouseButtons() == Qt::LeftButton )
        {
            if( item->parent() == 0 )
            {
                setItemExpanded( item, !isItemExpanded( item ) );
            }
        }
    }
}

//-----------------------------------------------------------------------
