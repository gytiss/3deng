#include "AudioDataManager.h"

#include <Exceptions/GenericException.h>
#include <cassert>

//-----------------------------------------------------------------------

bool AudioDataManager::m_ClassAlreadyInstantiated = false;

//-----------------------------------------------------------------------

AudioDataManager::AudioDataManager(FileManager& fileManager)
: m_FileManager( fileManager ),
  m_AudioDataList(),
  m_UniqueAudioDataIDsToAudioDataMap()
{
    if( m_ClassAlreadyInstantiated )
    {
        throw GenericException( "There can only be one instance of AudioDataManager()" );
    }
    else
    {
        m_ClassAlreadyInstantiated = true;
    }
}

//-----------------------------------------------------------------------

AudioDataManager::~AudioDataManager()
{
    for( RefCountedAudioDataPtr& ptr : m_AudioDataList )
    {
        delete ptr.audioData;
    }

    m_AudioDataList.Clear();
}

//-----------------------------------------------------------------------

AudioDataManager::AudioDataHandle AudioDataManager::LoadAudioData(const std::string& audioFilePath)
{
    AudioDataList::Node* audioDataNode;

    UniqueAudioDataIDsToAudioDataMap::iterator it = m_UniqueAudioDataIDsToAudioDataMap.find( audioFilePath );
    if( it != m_UniqueAudioDataIDsToAudioDataMap.end() )
    {
        audioDataNode = it->second;
        audioDataNode->value.refCount = ( audioDataNode->value.refCount + 1 );
    }
    else
    {
        audioDataNode = _LoadAudioDataFromFile( audioFilePath );
    }

    return AudioDataHandle( audioDataNode );
}

//-----------------------------------------------------------------------

void AudioDataManager::ReleaseAudioData(AudioDataHandle& handle)
{
    assert( handle.IsValid() && "Trying to release an invalid handle" );

    if( handle.IsValid() )
    {
        RefCountedAudioDataPtr& audioDataPtr = handle.m_AudioDataListNode->value;
        audioDataPtr.refCount = ( audioDataPtr.refCount - 1 );

        if( audioDataPtr.refCount == 0 )
        {
            delete audioDataPtr.audioData;

            m_UniqueAudioDataIDsToAudioDataMap.erase( audioDataPtr.audioDataFilePath );
            m_AudioDataList.Erase( handle.m_AudioDataListNode );
            handle.Invalidate();
        }
    }
}

//-----------------------------------------------------------------------

AudioDataManager::AudioDataList::Node* AudioDataManager::_LoadAudioDataFromFile(const std::string& audioFilePath)
{
    AudioDataList::Node* audioDataListNode = nullptr;

    std::unique_ptr<AudioData> audioData( new AudioData( m_FileManager ) );

    if (audioData->InitFromOggFile( audioFilePath ))
    {
        audioDataListNode = m_AudioDataList.PushBack( RefCountedAudioDataPtr() );
        audioDataListNode->value.refCount = 1;
        audioDataListNode->value.audioData = audioData.release();
        audioDataListNode->value.audioDataFilePath = audioFilePath;
    }
    else
    {
        throw GenericException( "_LoadAudioDataFromFile() failed to load audio file \"" + audioFilePath + "\"" );
    }

    return audioDataListNode;
}

//-----------------------------------------------------------------------
