#ifndef TRANSFORMCOMPONENTWIDGET_H_INCLUDED
#define TRANSFORMCOMPONENTWIDGET_H_INCLUDED

#include "ComponentWidget.h"

namespace Ui
{
    class TransformComponentUI;
}

class Transform;
class ManipulatorTool;

class TransformComponentWidget : public ComponentWidget
{
    Q_OBJECT

public:
    TransformComponentWidget(ManipulatorTool& manipulatorTool,
                             CategorizedListWidget* listWidgetToAddOurselvesTo,
                             QWidget* parent = 0);
    ~TransformComponentWidget();

    void SetTransformComponent(Transform* transform);

    void ResetPaletteToDefault();

private slots:
    void XPosLineEditEditingFinished();
    void YPosLineEditEditingFinished();
    void ZPosLineEditEditingFinished();

    void XRotLineEditEditingFinished();
    void YRotLineEditEditingFinished();
    void ZRotLineEditEditingFinished();

    void XScaleLineEditEditingFinished();
    void YScaleLineEditEditingFinished();
    void ZScaleLineEditEditingFinished();

private:
    const QString m_Name;
    Ui::TransformComponentUI* m_UI;
    Transform* m_TransformComponent;
    ManipulatorTool& m_ManipulatorTool;
};

#endif // TRANSFORMCOMPONENTWIDGET_H_INCLUDED
