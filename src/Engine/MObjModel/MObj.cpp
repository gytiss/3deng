#include "MObj.h"
#include "MAnim.h"
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <CommonDefinitions.h>
#include <boost/filesystem.hpp>
#include <boost/locale.hpp>
#include <MsgLogger.h>
#include <BitMask.h>

//-----------------------------------------------------------------------

MObj::MObj(FileManager& fileManager)
: m_FileManager( fileManager ),
  m_BoneNamesMD5Hash(),
  m_Joints(),
  m_InverseBindPose(),
  m_BoundingBox(),
  m_Meshes(),
  m_ModelDirectory( "" ),
  m_HasSkinnedMeshes( false )

{
    memset( m_BoneNamesMD5Hash, 0x00, c_MD5HashLength );
}

//-----------------------------------------------------------------------

MObj::~MObj()
{
    for(Mesh*& mesh : m_Meshes)
    {
        SafeDelete( mesh );
    }
}

//-----------------------------------------------------------------------

bool MObj::LoadFromFile(const std::string& fileName)
{
    bool retVal = false;

    ByteStream byteStream( m_FileManager );
    if( byteStream.InitFromFile( fileName ) )
    {
        retVal = true;

        uint16_t jointCount = 0;
        uint16_t meshCount = 0;
        uint16_t subMeshCount = 0;
        uint32_t vertexCount = 0;
        uint32_t indexCount = 0;
        uint32_t jointNamesSectionSize = 0;
        uint32_t meshesNamesSectionSize = 0;
        uint32_t totalMeshsMaterialsPathsStringLen = 0;
        uint32_t optionFlagsBitMaskValue = 0;

        retVal = retVal && _VerifyFileTypeAndVersion( byteStream );
        retVal = retVal && _ReadOptionFlagsBitMask( byteStream, optionFlagsBitMaskValue );

        BitMask32 optionFlagsBitMask( optionFlagsBitMaskValue );
        m_HasSkinnedMeshes = optionFlagsBitMask.Contains( e_OptionFlagContainsSkinnedMeshes );

        retVal = retVal && byteStream.ReadBytesToBuffer( m_BoneNamesMD5Hash, c_MD5HashLength );
        retVal = retVal && _ReadJointMeshAndSubMeshCount( byteStream, jointCount, meshCount, subMeshCount );
        retVal = retVal && _ReadMeshsMaterialsPathsStringsLen( byteStream, totalMeshsMaterialsPathsStringLen );
        retVal = retVal && _ReadVertexCount( byteStream, vertexCount );
        retVal = retVal && _ReadIndexCount( byteStream, indexCount );
        retVal = retVal && _ReadJointNamesSectionSize( byteStream, jointNamesSectionSize );
        retVal = retVal && _ReadMeshesNamesSectionSize( byteStream, meshesNamesSectionSize );
        retVal = retVal && _VerifyFileSize( byteStream,
                                            jointNamesSectionSize,
                                            meshesNamesSectionSize,
                                            totalMeshsMaterialsPathsStringLen,
                                            jointCount,
                                            meshCount,
                                            subMeshCount,
                                            vertexCount,
                                            indexCount );

        std::vector<std::string> meshNames;

        retVal = retVal && _ReadBoundingBox( byteStream );
        retVal = retVal && _SkipJointNamesSection( byteStream, jointNamesSectionSize );
        retVal = retVal && _ReadMeshNames( byteStream, meshCount, meshNames );
        if( m_HasSkinnedMeshes )
        {
            retVal = retVal && _ReadJoints( byteStream, jointCount, m_Joints );
        }
        else
        {
            m_Joints.resize( 0 );
        }
        retVal = retVal && _ReadMeshes( byteStream, meshCount, meshNames, m_Meshes );

        if( retVal )
        {
            if( m_HasSkinnedMeshes )
            {
                _GenerateInverseBindPose( m_Joints, m_InverseBindPose );
            }
            else
            {
                m_InverseBindPose.resize( 0 );
            }

            m_ModelDirectory = boost::locale::conv::utf_to_utf<char>( boost::filesystem::path( boost::locale::conv::utf_to_utf<boost::filesystem::path::string_type::value_type>( fileName ) ).remove_leaf().c_str() );
        }
    }

    return retVal;
}

//-----------------------------------------------------------------------

bool MObj::CheckAnimation(const MAnim& animation) const
{
    const uint8_t* animBoneNamesMD5Hash = animation.GetBoneNamesMD5Hash();
    return !memcmp( m_BoneNamesMD5Hash, animBoneNamesMD5Hash, c_MD5HashLength );
}

//-----------------------------------------------------------------------

bool MObj::_ReadString(std::string& outVal, ByteStream& byteStream)
{
    bool retVal = true;
    uint8_t strLen = 0;

    retVal = retVal && byteStream.ReadUInt8( strLen );
    retVal = retVal && byteStream.ReadString( outVal, strLen );

    return retVal;
}

//-----------------------------------------------------------------------

bool MObj::_VerifyFileTypeAndVersion(ByteStream& byteStream)
{
    bool retVal = true;

    std::string fileIdStr = "";
    uint32_t fileVersion = 0;
    retVal = retVal && byteStream.ReadString( fileIdStr, 4 );
    retVal = retVal && byteStream.ReadUInt32( fileVersion );
    retVal = retVal && !fileIdStr.compare( "MOBJ" );
    retVal = retVal && ( fileVersion == 1 );

    return retVal;
}

//-----------------------------------------------------------------------

bool MObj::_VerifyFileSize(ByteStream& byteStream,
                           const uint32_t jointNamesSectionSize,
                           const uint32_t meshesNamesSectionSize,
                           uint32_t totalMeshesMaterialsPathsStringLen,
                           uint16_t jointCount,
                           uint16_t meshCount,
                           uint16_t subMeshCount,
                           uint32_t vertexCount,
                           uint32_t indexCount )
{
    const size_t c_FileHeaderSize = 54;
    const size_t c_BoundingBoxSize = 24;
    const size_t c_BytesPerJoint = 30;
    const size_t c_BytesPerMesh = 12;    // Bytes of the material name are included in totalMeshsMaterialsPathsStringLen
    const size_t c_BytesPerSubMesh = 16;
    const size_t c_BytesPerSkinnedMeshVertex = 88;
    const size_t c_BytesPerStaticMeshVertex = 56;
    const size_t c_BytesPerIndex = 4;
    const size_t actualFileSize = byteStream.InitialStreamSizeInBytes();

    size_t expectedFileSize = (c_FileHeaderSize +
                               c_BoundingBoxSize +
                               jointNamesSectionSize +
                               meshesNamesSectionSize +
                               totalMeshesMaterialsPathsStringLen);

    expectedFileSize += ( c_BytesPerJoint * jointCount );
    expectedFileSize += ( c_BytesPerMesh * meshCount );
    expectedFileSize += ( c_BytesPerSubMesh * subMeshCount );
    if( m_HasSkinnedMeshes )
    {
        expectedFileSize += ( c_BytesPerSkinnedMeshVertex * vertexCount );
    }
    else
    {
        expectedFileSize += ( c_BytesPerStaticMeshVertex * vertexCount );
    }
    expectedFileSize += ( c_BytesPerIndex * indexCount );

    return ( expectedFileSize == actualFileSize );
}

//-----------------------------------------------------------------------

bool MObj::_ReadOptionFlagsBitMask(ByteStream& byteStream, uint32_t& optionFlagsBitMask)
{
    bool retVal = true;

    retVal = retVal && byteStream.ReadUInt32( optionFlagsBitMask );

    return retVal;
}

//-----------------------------------------------------------------------

bool MObj::_ReadJointMeshAndSubMeshCount(ByteStream& byteStream,
                                         uint16_t& jointCount,
                                         uint16_t& meshCount,
                                         uint16_t& subMeshCount)
{
    bool retVal = true;

    retVal = retVal && byteStream.ReadUInt16( jointCount );
    retVal = retVal && byteStream.ReadUInt16( meshCount );
    retVal = retVal && byteStream.ReadUInt16( subMeshCount );

    return retVal;
}

//-----------------------------------------------------------------------

bool MObj::_ReadVertexCount(ByteStream& byteStream, uint32_t& vertexCount)
{
    return byteStream.ReadUInt32( vertexCount );
}

//-----------------------------------------------------------------------

bool MObj::_ReadIndexCount(ByteStream& byteStream, uint32_t& indexCount)
{
    return byteStream.ReadUInt32( indexCount );
}

//-----------------------------------------------------------------------

bool MObj::_ReadJointNamesSectionSize(ByteStream& byteStream, uint32_t& jointNamesSectionSize)
{
    return byteStream.ReadUInt32( jointNamesSectionSize );
}

//-----------------------------------------------------------------------

bool MObj::_ReadMeshesNamesSectionSize(ByteStream& byteStream, uint32_t& meshesNamesSectionSize)
{
    return byteStream.ReadUInt32( meshesNamesSectionSize );
}

//-----------------------------------------------------------------------

bool MObj::_ReadMeshsMaterialsPathsStringsLen(ByteStream& byteStream, uint32_t& totalStringsLen)
{
    return byteStream.ReadUInt32( totalStringsLen );
}

//-----------------------------------------------------------------------

bool MObj::_ReadBoundingBox(ByteStream& byteStream)
{
    bool retVal = true;

    retVal = retVal && byteStream.ReadBytesToBuffer( &m_BoundingBox.center[0], 3 );
    retVal = retVal && byteStream.ReadBytesToBuffer( &m_BoundingBox.halfExtents[0], 3 );

    return retVal;
}

//-----------------------------------------------------------------------

bool MObj::_SkipJointNamesSection(ByteStream& byteStream, const uint32_t sectionSizeInBytes)
{
    return byteStream.SkipBytes( sectionSizeInBytes );
}

//-----------------------------------------------------------------------

bool MObj::_ReadMeshNames(ByteStream& byteStream,
                          uint16_t meshCount,
                          std::vector<std::string>& outMeshNames)
{
    bool retVal = true;

    outMeshNames.assign( meshCount, std::string( "" ) );

    for(size_t i = 0; (i < meshCount) && retVal; i++)
    {
        std::string& meshName = outMeshNames[i];
        retVal = retVal && _ReadString( meshName, byteStream );
    }

    return retVal;
}

//-----------------------------------------------------------------------

bool MObj::_ReadJoints(ByteStream& byteStream, uint16_t jointCount, JointList& outJoints)
{
    bool retVal = true;

    outJoints.reserve( jointCount );
    outJoints.assign( jointCount, Joint() );

    for(size_t i = 0; (i < jointCount) && retVal; i++)
    {
        Joint& joint = m_Joints[i];
        int16_t parentId = 0;
        retVal = retVal && byteStream.ReadInt16( parentId );
        joint.m_ParentID = parentId;
        retVal = retVal && byteStream.ReadBytesToBuffer( &joint.m_Pos[0], 3 );
        retVal = retVal && byteStream.ReadBytesToBuffer( &joint.m_Orient[0], 4 );
    }

    return retVal;
}

//-----------------------------------------------------------------------

bool MObj::_ReadMeshes(ByteStream& byteStream,
                      uint16_t meshCount,
                      const std::vector<std::string>& meshNames,
                      MeshList& outMeshes)
{
    assert( ( meshNames.size() == meshCount ) && "Mesh names array must not be smaller than the number of meshes" );

    bool retVal = true;

    outMeshes.reserve( meshCount );
    outMeshes.assign( meshCount, nullptr );

    for(size_t i = 0; (i < meshCount) && retVal; i++)
    {
        Mesh* mesh = new Mesh();
        outMeshes[i] = mesh;

        int16_t parentMeshID = 0;
        uint16_t subMeshCount = 0;
        uint32_t meshVertexCount = 0;
        uint32_t meshIndexCount = 0;

        retVal = retVal && byteStream.ReadInt16( parentMeshID );
        retVal = retVal && byteStream.ReadUInt16( subMeshCount );
        retVal = retVal && byteStream.ReadUInt32( meshVertexCount );
        retVal = retVal && byteStream.ReadUInt32( meshIndexCount );

        assert( MathUtils::CanBeStoredInType<EngineTypes::VertexIndexType>( meshVertexCount ) && "Mesh has more vertices than the index data type currently in use can handle" );
        retVal = retVal && MathUtils::CanBeStoredInType<EngineTypes::VertexIndexType>( meshVertexCount );

        mesh->parentMeshID = parentMeshID;
        mesh->name = meshNames[i];

        mesh->subMeshes.assign( subMeshCount, Mesh::SubMesh() );

        for(size_t i = 0; (i < subMeshCount) && retVal; i++)
        {
            Mesh::SubMesh& subMesh = mesh->subMeshes[i];

            uint32_t minMeshIndexValue = 0;
            uint32_t maxMeshIndexValue = 0;

            retVal = retVal && _ReadSubMesh( byteStream,
                                             subMesh.m_MaterialName,
                                             subMesh.m_FirstIndex,
                                             subMesh.m_IndexCount,
                                             minMeshIndexValue,
                                             maxMeshIndexValue );

            assert( MathUtils::CanBeStoredInType<EngineTypes::VertexIndexType>( minMeshIndexValue ) && "Mesh has more vertices than the index data type currently in use can handle" );
            assert( MathUtils::CanBeStoredInType<EngineTypes::VertexIndexType>( maxMeshIndexValue ) && "Mesh has more vertices than the index data type currently in use can handle" );

            subMesh.minVertexIndexValue = minMeshIndexValue;
            subMesh.maxVertexIndexValue = maxMeshIndexValue;
        }

        if( m_HasSkinnedMeshes )
        {
            mesh->type = Mesh::e_MeshTypeSkinned;
            mesh->staticVertices.resize( 0 );
            retVal = retVal && _ReadSkinnedVertices( byteStream, mesh->skinnedVertices, meshVertexCount );
        }
        else
        {
            mesh->type = Mesh::e_MeshTypeStatic;
            mesh->skinnedVertices.resize( 0 );
            retVal = retVal && _ReadStaticVertices( byteStream, mesh->staticVertices, meshVertexCount );
        }

        retVal = retVal && _ReadIndices( byteStream, mesh->indices, meshIndexCount );
    }

    return retVal;
}

//-----------------------------------------------------------------------

bool MObj::_ReadSubMesh(ByteStream& byteStream,
                        std::string& outMaterialName,
                        uint32_t& firstIndex,
                        uint32_t& indexCount,
                        uint32_t& minMeshIndexValue,
                        uint32_t& maxMeshIndexValue)
{
    bool retVal = true;

    retVal = retVal && _ReadString( outMaterialName, byteStream );
    retVal = retVal && byteStream.ReadUInt32( firstIndex );
    retVal = retVal && byteStream.ReadUInt32( indexCount );
    retVal = retVal && byteStream.ReadUInt32( minMeshIndexValue );
    retVal = retVal && byteStream.ReadUInt32( maxMeshIndexValue );

    return retVal;
}

//-----------------------------------------------------------------------

bool MObj::_ReadSkinnedVertices(ByteStream& byteStream,
                                Mesh::SkinnedVertexList& outVertexList,
                                const uint32_t vertexCount)
{
    bool retVal = true;

    outVertexList.reserve( vertexCount );
    outVertexList.assign( vertexCount, SkinnedVertex() );

    for(size_t i = 0; (i < vertexCount) && retVal; i++)
    {
        SkinnedVertex& vtx = outVertexList[i];

        retVal = retVal && byteStream.ReadBytesToBuffer( &vtx.m_Pos[0], 3 );
        retVal = retVal && byteStream.ReadBytesToBuffer( &vtx.m_Normal[0], 3 );
        retVal = retVal && byteStream.ReadBytesToBuffer( &vtx.m_BiTangent[0], 3 );
        retVal = retVal && byteStream.ReadBytesToBuffer( &vtx.m_Tangent[0], 3 );
        retVal = retVal && byteStream.ReadBytesToBuffer( &vtx.m_UV[0], 2 );
        retVal = retVal && byteStream.ReadBytesToBuffer( &vtx.m_BoneWeights[0], 4 );
        retVal = retVal && byteStream.ReadBytesToBuffer( &vtx.m_BoneIndices[0], 4 );
    }

    return retVal;
}

//-----------------------------------------------------------------------

bool MObj::_ReadStaticVertices(ByteStream& byteStream,
                               Mesh::StaticVertexList& outVertexList,
                               const uint32_t vertexCount)
{
    bool retVal = true;

    outVertexList.reserve( vertexCount );
    outVertexList.assign( vertexCount, StaticVertex() );

    for(size_t i = 0; (i < vertexCount) && retVal; i++)
    {
        StaticVertex& vtx = outVertexList[i];

        retVal = retVal && byteStream.ReadBytesToBuffer( &vtx.m_Pos[0], 3 );
        retVal = retVal && byteStream.ReadBytesToBuffer( &vtx.m_Normal[0], 3 );
        retVal = retVal && byteStream.ReadBytesToBuffer( &vtx.m_BiTangent[0], 3 );
        retVal = retVal && byteStream.ReadBytesToBuffer( &vtx.m_Tangent[0], 3 );
        retVal = retVal && byteStream.ReadBytesToBuffer( &vtx.m_UV[0], 2 );
    }

    return retVal;
}

//-----------------------------------------------------------------------

bool MObj::_ReadIndices(ByteStream& byteStream,
                        IndexBuffer& outIndices,
                        const uint32_t indexCount)
{
    std::vector<uint32_t> readIndices;

    readIndices.reserve( indexCount );
    readIndices.assign( indexCount, 0 );
    outIndices.reserve( indexCount );

    const bool retVal = byteStream.ReadBytesToBuffer( &readIndices[0], indexCount );

    for(uint32_t index : readIndices)
    {
        outIndices.push_back( index );
    }

    return retVal;
}

//-----------------------------------------------------------------------

void MObj::_GenerateInverseBindPose(const JointList& joints, EngineTypes::MatrixArray& outInverseBindPose)
{
    outInverseBindPose.clear();
    outInverseBindPose.reserve( joints.size() );

    for(const Joint& joint : joints )
    {
        const glm::mat4 boneTranslation = glm::translate( MathUtils::c_IdentityMatrix, joint.m_Pos );
        const glm::mat4 boneRotation = glm::toMat4( joint.m_Orient );

        const glm::mat4 boneMatrix = boneTranslation * boneRotation;

        const glm::mat4 inverseBoneMatrix = glm::inverse( boneMatrix );

        outInverseBindPose.push_back( inverseBoneMatrix );
    }
}

//-----------------------------------------------------------------------
#include <iostream>
void MObj::DebugDumpData()
{
//    using std::cout;
//    using std::endl;
//
//    cout << "Joint Count: : " << m_JointCount << endl;
//    cout << "SubMesh Count   : " << m_SubMeshCount << endl;
//    cout << "Vertex Count : " << m_Vertices.size() << endl;
//    cout << "Index Count  : " << m_Indices.size() << endl;
//
//    cout << "---------------------------- Joints ----------------------------" << endl;
//    for(size_t i = 0; i < m_Joints.size(); i++)
//    {
//        Joint& jnt = m_Joints[i];
//        cout << "  Parent : " << jnt.m_ParentID << endl;
//        cout << "  Pos    : " << jnt.m_Pos[0] << ", " << jnt.m_Pos[1] << ", " << jnt.m_Pos[2] << endl;
//        cout << "  Orient : " << jnt.m_Orient[0] << ", " << jnt.m_Orient[1] << ", " << jnt.m_Orient[2] << ", " << jnt.m_Orient[3] << endl;
//        cout << endl;
//    }
//    cout << "----------------------------------------------------------------" << endl;
//
//    cout << endl;
//
//    cout << "---------------------------- Meshes ----------------------------" << endl;
//    for(size_t i = 0; i < m_SubMeshes.size(); i++)
//    {
//        SubMesh& subMesh = m_SubMeshes[i];
//        cout << "  Material : " << subMesh.m_MaterialName << endl;
//        cout << "  StartIdx : " << subMesh.m_FirstIndex << endl;
//        cout << "  IdxCount : " << subMesh.m_IndexCount << endl;
//        cout << endl;
//    }
//    cout << "----------------------------------------------------------------" << endl;
//
//    cout << endl;
//
//    cout << "--------------------------- Vertices ---------------------------" << endl;
//    for(size_t i = 0; i < m_Vertices.size(); i++)
//    {
//        Vertex& vtx = m_Vertices[i];
//        cout << "  Pos      : " << vtx.m_Pos[0] << ", " << vtx.m_Pos[1] << ", " << vtx.m_Pos[2] << endl;
//        cout << "  Normal   : " << vtx.m_Normal[0] << ", " << vtx.m_Normal[1] << ", " << vtx.m_Normal[2] << endl;
//        cout << "  UV       : " << vtx.m_UV[0] << ", " << vtx.m_UV[1] << endl;
//        cout << "  Weights  : " << vtx.m_BoneWeights[0] << ", " << vtx.m_BoneWeights[1] << ", " << vtx.m_BoneWeights[2] << ", " << vtx.m_BoneWeights[3] << endl;
//        cout << "  Bone Idx : " << vtx.m_BoneIndices[0] << ", " << vtx.m_BoneIndices[1] << ", " << vtx.m_BoneIndices[2] << ", " << vtx.m_BoneIndices[3] << endl;
//        cout << endl;
//    }
//    cout << "----------------------------------------------------------------" << endl;
//
//    cout << endl;
//
//    cout << "--------------------------- Indices ----------------------------" << endl;
//    for(size_t i = 0; i < m_Indices.size(); i += 3)
//    {
//        unsigned int idx1 = m_Indices[i];
//        unsigned int idx2 = m_Indices[i+1];
//        unsigned int idx3 = m_Indices[i+2];
//        cout << "  " << idx1 << " " << idx2 << " " << idx3 << endl;
//    }
//    cout << "----------------------------------------------------------------" << endl;
}

//-----------------------------------------------------------------------

















