mtc (miscellaneous texture container) files are little-endian. 

Every mtc file can contain mip mappedd and not mip mapped 2D textures rpresented in various compressed and not compressed formats 

//--------------- File Header (In order to keep texture data 4 byte alligned the size of this header must be divisible by 4)-------------------
4  - bytes: "MTC " string //String identifying the file format
4  - bytes: File format version number  //UInt
2  - Texture type (0 - 1D, 1 - 2D, 2 - CubeMap 
2  - byte: Texture data format (0 - RAW_Pixels_RGB, 
                                1 - RAW_Pixels_RGBA, 
                                2 - RAW_Pixels_SRGB,
                                3 - RAW_Pixels_SRGBA,
                                4 - ETC2_RGB,
                                5 - ETC2_SRGB,
                                6 - ETC2_EAC_RGBA
                                7 - ETC2_EAC_SRGBA, 
                                8 - ALPHA1_ETC2_RGBA,
                                9 - ALPHA1_ETC2_SRGBA,
                               10 - PVRTC_RGB, 
                               11 - PVRTC_RGBA, 
                               12 - PVRTC2_RGB, 
                               13 - PVRTC2_RGBA
4  - bytes: Total number of mip map descriptors //UInt
4  - bytes: Width in pixels //UInt
4  - bytes: Height in pixels (Is set to zero when "Texture type" == "1D") //UInt
4  - bytes: Total texture data size in bytes
//-----------------------------------------------

//------------- Mip Map Descriptors (In order to keep texture data 4 byte alligned the total size of this section must be divisible by 4)------------- 
//------ For Each Mip Map Level ------
---
--- When "Texture type" != "CubeMap"
4 - bytes: Size of the texture data of this Mip Map in bytes //UInt
4 - bytes: Offset (4 byte aligned) in the texture data //UInt
---
--- When "Texture type" == "CubeMap"
4 - bytes: Size of the +X texture data of this Mip Map in bytes //UInt
4 - bytes: Offset (4 byte aligned) of the +X texture in the texture data //UInt

4 - bytes: Size of the -X texture data of this Mip Map in bytes //UInt
4 - bytes: Offset (4 byte aligned) of the -X texture in the texture data //UInt

4 - bytes: Size of the +Y texture data of this Mip Map in bytes //UInt
4 - bytes: Offset (4 byte aligned) of the +Y texture in the texture data //UInt

4 - bytes: Size of the -Y texture data of this Mip Map in bytes //UInt
4 - bytes: Offset (4 byte aligned) of the -Y texture in the texture data //UInt

4 - bytes: Size of the +Z texture data of this Mip Map in bytes //UInt
4 - bytes: Offset (4 byte aligned) of the +Z texture in the texture data //UInt

4 - bytes: Size of the -Z texture data of this Mip Map in bytes //UInt
4 - bytes: Offset (4 byte aligned) of the +Z texture in the texture data //UInt
---
//-----------------------------------------
//----------------------------------------------- 

//---------------- Texture Data (All of the data is 4 byte alligned)----------------- 
"Total texture data size in bytes" bytes of texture data
//----------------------------------------------- 