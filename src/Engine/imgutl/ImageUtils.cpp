#include "ImageUtils.h"

#include <fstream>
#include <cstring>
#include <cassert>
#include "TGAImage.h"
#include <CommonDefinitions.h>

//-----------------------------------------------------------------------

ImageUtils::ImageData_t* ImageUtils::LoadImage( FileManager& fileManager,
                                                ImageUtils::ImageType it,
                                                const uint8_t* dataBuffer,
                                                size_t dataBufferSizeInBytes,
                                                ImageOrigin imageOrigin )
{
    ImageData_t* im = nullptr;

    switch( it )
    {
        case TGA:
        {
            TGAImage tgaImg( fileManager );

            TGAImage::Origin tgaImageOrigin = TGAImage::LOWER_LEFT;
            if( imageOrigin == UPPER_LEFT )
            {
                tgaImageOrigin = TGAImage::UPPER_LEFT;
            }
            else if( imageOrigin == LOWER_LEFT )
            {
                tgaImageOrigin = TGAImage::LOWER_LEFT;
            }
            else
            {
                assert( false && "Unhandled image origin" );
            }

            if( tgaImg.Load( dataBuffer, dataBufferSizeInBytes, tgaImageOrigin ) )
            {
                im = new ImageData_t();
                TGAImageToImageData( tgaImg, *im );
            }
            break;
        }
        case PNG:
        case JPG:
        case BMP:
        {
            assert(false && "Trying to load unsupported image type");
            break;
        }
    }

    return im;
}

//-----------------------------------------------------------------------

ImageUtils::ImageData_t* ImageUtils::LoadImage( FileManager& fileManager,
                                                ImageUtils::ImageType it,
                                                std::string fileName,
                                                ImageOrigin imageOrigin )
{
    ImageData_t* im = nullptr;

    switch( it )
    {
        case TGA:
        {
            TGAImage tgaImg( fileManager );

            TGAImage::Origin tgaImageOrigin = TGAImage::LOWER_LEFT;
            if( imageOrigin == UPPER_LEFT )
            {
                tgaImageOrigin = TGAImage::UPPER_LEFT;
            }
            else if( imageOrigin == LOWER_LEFT )
            {
                tgaImageOrigin = TGAImage::LOWER_LEFT;
            }
            else
            {
                assert( false && "Unhandled image origin" );
            }

            if( tgaImg.Load( fileName, tgaImageOrigin ) )
            {
                im = new ImageData_t();
                TGAImageToImageData( tgaImg, *im );
            }
            break;
        }
        case PNG:
        case JPG:
        case BMP:
        {
            assert(false && "Trying to load unsupported image type");
            break;
        }
    }

    return im;
}

//-----------------------------------------------------------------------

bool ImageUtils::SaveImage(ImageUtils::ImageType it,
                           std::string fileName,
                           const ImageUtils::ImageData_t* im )
{
    bool ret = false;

    switch( it )
    {
        case TGA: ret = TGAImage::SaveAsTGA( fileName,
                                             TGAImage::LOWER_LEFT,
                                             im->w,
                                             im->h,
                                             im->bytesPerPixel,
                                             im->data ); break;
        case PNG:
        case JPG:
        case BMP: break;
    }

    return ret;
}

//-----------------------------------------------------------------------

void ImageUtils::DeleteImage( const ImageUtils::ImageData_t* im )
{
    if( im != nullptr )
    {
        if( im->data != nullptr )
        {
            delete[] im->data;
        }

        delete im;
    }
}

//-----------------------------------------------------------------------

bool ImageUtils::InsertImage( ImageUtils::ImageData_t* destIm,
                              const ImageUtils::ImageData_t* srcIm,
                              size_t x,
                              size_t y )
{
    bool ret = false;

    if( ( ( y + srcIm->h ) <= destIm->h ) &&
        ( ( x + srcIm->w ) <= destIm->w ) )
    {
        #define GetDestScanLine( num )\
                destIm->data+\
                ( destIm->w * y * destIm->bytesPerPixel )+\
                ( ( ( destIm->w * (num) ) + x ) * destIm->bytesPerPixel )
        #define GetSrcScanLine( num )\
                srcIm->data+( srcIm->w * (num) * srcIm->bytesPerPixel )

        const size_t srcScanLineLength = ( srcIm->bytesPerPixel * srcIm->w );

        if( destIm->depth == srcIm->depth )
        {
            for( size_t i = 0; i < srcIm->h; ++i )
            {
                memcpy( GetDestScanLine( i ),
                        GetSrcScanLine( i ),
                        srcScanLineLength );
            }
            ret = true;
        }
        else if( ( destIm->depth == 32 ) && ( srcIm->depth == 24 ) ) // RGBA and RGB
        {
            uint8_t* destScnLine = nullptr;
            uint8_t* srcScnLine = nullptr;

            for( size_t i = 0; i < srcIm->h; ++i )
            {
                destScnLine = GetDestScanLine( i );
                srcScnLine = GetSrcScanLine( i );

                size_t destClrIdx = 0;
                size_t srcClrIdx = 0;
                while( srcClrIdx < srcScanLineLength )
                {
                    memcpy( ( destScnLine + destClrIdx ),
                            ( srcScnLine + srcClrIdx ),
                            srcIm->bytesPerPixel );
                    destScnLine[ destClrIdx + srcIm->bytesPerPixel ] = 0xFF;
                    destClrIdx += destIm->bytesPerPixel;
                    srcClrIdx += srcIm->bytesPerPixel;
                }
            }
            ret = true;
        }

        #undef GetDestScanLine
        #undef GetSrcScanLine
    }

    return ret;
}

//-----------------------------------------------------------------------

ImageUtils::ImageData_t* ImageUtils::BlankImage( size_t width,
                                                 size_t height,
                                                 ImageUtils::Format format,
                                                 uint8_t firstChannelInitialValue,
                                                 uint8_t secondChannelInitialValue,
                                                 uint8_t thirdChannelInitialValue,
                                                 uint8_t fourthChannelInitialValue )
{
    ImageData_t* im = new ImageData_t();

    unsigned int bytesPerPixel = 3; // RGB

    if( ( format == RGBA ) || ( format == BGRA ) )
    {
        bytesPerPixel = 4;
    }

    const size_t dataSize = ( width * height * bytesPerPixel );

    im->w = width;
    im->h = height;
    im->depth = ( bytesPerPixel * 8 );
    im->bytesPerPixel = bytesPerPixel;
    im->data = new uint8_t[dataSize];

    if( bytesPerPixel == 3 )
    {
        for( size_t i = 0; i < dataSize; i += 3 )
        {
            im->data[i] = firstChannelInitialValue;
            im->data[i + 1] = secondChannelInitialValue;
            im->data[i + 2] = thirdChannelInitialValue;
        }
    }
    else
    {
        for( size_t i = 0; i < dataSize; i += 4 )
        {
            im->data[i] = firstChannelInitialValue;
            im->data[i + 1] = secondChannelInitialValue;
            im->data[i + 2] = thirdChannelInitialValue;
            im->data[i + 3] = fourthChannelInitialValue;
        }
    }

    return im;
}

//-----------------------------------------------------------------------

void ImageUtils::ConvertRGBImageToRGBAImage( ImageUtils::ImageData_t* im )
{
    if( im->format == RGB )
    {
        if( im->data != nullptr )
        {
            const size_t rgbBytesPerPixel = 3;
            const size_t rgbaBytesPerPixel = 4;
            const size_t rgbPixelDataSizeInBytes = ( im->w * im->h * rgbBytesPerPixel );
            const size_t rgbaPixelDataSizeInBytes = ( im->w * im->h * rgbaBytesPerPixel );

            uint8_t* newImageData = new uint8_t[rgbaPixelDataSizeInBytes];

            for (size_t i = 0; i < rgbPixelDataSizeInBytes; i += rgbBytesPerPixel)
            {
                newImageData[i] = im->data[i];
                newImageData[i + 1] = im->data[i + 1];
                newImageData[i + 2] = im->data[i + 2];
                newImageData[i + 3] = 0xFF;
            }

            delete[] im->data;
            im->data = newImageData;
        }
        else
        {
            assert( im->data && "Image data must not be null" );
        }
    }
}

//-----------------------------------------------------------------------

void ImageUtils::FlipVertically( ImageUtils::ImageData_t* im )
{
    FlipVertically( im->data, im->w, im->h, im->bytesPerPixel );
}

//-----------------------------------------------------------------------

void ImageUtils::FlipHorizontally( ImageUtils::ImageData_t* im )
{
    FlipHorizontally( im->data, im->w, im->h, im->bytesPerPixel );
}

//-----------------------------------------------------------------------

void ImageUtils::FlipVertically( uint8_t* data,
                                 size_t width,
                                 size_t height,
                                 unsigned int bytesPerPixel )
{
    #define GetScanLine(a,n) &((a)[(n)*(width*bytesPerPixel)])

    const size_t numScanLines = height;
    const size_t scanLineLength = ( width * bytesPerPixel );
    uint8_t* temp = new uint8_t[scanLineLength];

    for( size_t i = 0; i < ( numScanLines / 2 ) ; i++ )
    {
        memcpy( temp, GetScanLine( data, i ), scanLineLength );
        memcpy( GetScanLine( data, i ), GetScanLine( data, numScanLines-i-1 ), scanLineLength );
        memcpy( GetScanLine( data, numScanLines-i-1 ), temp, scanLineLength );
    }

    delete[] temp;

    #undef GetScanLine
}

//-----------------------------------------------------------------------

void ImageUtils::FlipHorizontally( uint8_t* data,
                                   size_t width,
                                   size_t height,
                                   unsigned int bytesPerPixel )
{
    #define GetScanLine(a,n) &((a)[(n)*(width*bytesPerPixel)])

    const size_t numScanLines = height;
    const size_t scanLineLength = ( width * bytesPerPixel );

    for( size_t scnLine = 0; scnLine < numScanLines; scnLine++ )
    {
        uint8_t* line= GetScanLine( data, scnLine );
        uint8_t temp[4];

        for (size_t i=0; i < ( scanLineLength / 2 ); i+= bytesPerPixel )
        {
            memcpy( temp, line+i, bytesPerPixel );
            memcpy( line+i, line+scanLineLength-i-bytesPerPixel, bytesPerPixel );
            memcpy( line+scanLineLength-i-bytesPerPixel, temp, bytesPerPixel );
        }
    }

    #undef GetScanLine
}

//-----------------------------------------------------------------------

void ImageUtils::TGAImageToImageData(const TGAImage& tgaImage, ImageData_t& outImageData)
{
    outImageData.data=tgaImage.GetData();
    outImageData.depth=tgaImage.GetDepth();
    outImageData.bytesPerPixel = ( outImageData.depth / 8 );
    outImageData.w=tgaImage.GetWidth();
    outImageData.h=tgaImage.GetHeight();

    if( outImageData.bytesPerPixel == 4 )
    {
        outImageData.format = RGBA;
    }
    else if( outImageData.bytesPerPixel == 3 )
    {
        outImageData.format = RGB;
    }
    else
    {
        assert( false && "TGA Images with less than 2 bytes per pixel e.g. \
                           Gray Scale images are not supported." );
    }
}

//-----------------------------------------------------------------------
