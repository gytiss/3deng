#ifndef ANIMATOR_H_INCLUDED
#define ANIMATOR_H_INCLUDED

#include <Resources/AnimationManager.h>
#include <EngineTypes.h>
#include <vector>
#include <map>
#include "Component.h"

class SkinnedMeshRenderer;

class Animator : public Component, public AnimationManager::AnimationResultsListener
{
    GAME_OBJECT_COMPONENT_TYPE(e_TypeAnimator);

public:
    friend class SkinnedMeshRenderer;

    Animator(GameObject& owningGameObject, AnimationManager& animationManager);

    virtual ~Animator();

    void SetEnabled(bool val) override;

    /**
     * @param animationName - Name the animation clip will be know as
     * @oaram animationHandle - Handle to an animation clip (Note: Animator object takes ownership of this handle)
     */
    bool AddAnimation(const std::string& animationName,
                      AnimationManager::AnimationHandle animationHandle);

    bool Play(const std::string& animationName, bool loop = false);
    bool IsPlaying();
    void Stop();

    void AnimatedSkeletonChanged(const EngineTypes::MatrixArray& animatedSkeleton);

private:
    typedef std::map<std::string, size_t> AnimationNameToAnimationListIdxMap;
    typedef std::vector<AnimationManager::AnimationHandle> AnimationList;
    typedef std::vector<SkinnedMeshRenderer*> SkinnedMeshRendererList;

    void _ConnectWithSkinnedMeshes();
    void _DisconnectFromSkinnedMeshes();

    AnimationManager& m_AnimationManager;

    AnimationNameToAnimationListIdxMap m_AnimationNameToIdxMap;
    AnimationList m_AnimationList;
    AnimationManager::AnimationHandle* m_CurrentlyPlayingAnimation;
    SkinnedMeshRendererList m_ConnectedSkinnedMeshRenderers;
    bool m_ConnectionWithSkinnedMeshesAttempted;
};

#endif // ANIMATOR_H_INCLUDED
