#ifndef SPHERECOLLIDERCOMPONENTWIDGET_H_INCLUDED
#define SPHERECOLLIDERCOMPONENTWIDGET_H_INCLUDED

#include "ColliderWidgetBase.h"
#include <ui_SphereColliderComponentUI.h>
#include <Engine/Scene/SphereCollider.h>

class SphereColliderComponentWidget : public ColliderWidgetBase<Ui::SphereColliderComponentUI, SphereCollider>
{
    Q_OBJECT

public:
    SphereColliderComponentWidget(CategorizedListWidget* listWidgetToAddOurselvesTo, QWidget* parent = 0);
    ~SphereColliderComponentWidget();

    void SetSphereColliderComponent(SphereCollider* collider);

    void ResetPaletteToDefault();

private slots:
    void RadiusLineEditEditingFinished();
};

#endif // SPHERECOLLIDERCOMPONENTWIDGET_H_INCLUDED
