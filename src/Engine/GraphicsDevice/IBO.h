#ifndef IBO_H_INCLUDED
#define IBO_H_INCLUDED

#include <EngineTypes.h>
#include <OpenGLIncludes.h>
#include <OpenGLErrorChecking.h>

class IBO
{
public:
    friend class VAO;

    enum DataBufferUsagePattern
    {
        e_DataBufferUsagePatternStatic = 0,
        e_DataBufferUsagePatternDynamic = 1
    };

    IBO(DataBufferUsagePattern usagePatter = e_DataBufferUsagePatternStatic);
    ~IBO();

    /**
    * Note: After executing this function, IBO needs to be binded again before use
    */
    void BindData(size_t sizeInBytes, const EngineTypes::VertexIndexType* dataPtr);

    static inline constexpr GLenum GetVertexIndexOpenGLType()
    {
        static_assert( ( sizeof( EngineTypes::VertexIndexType ) == 2 ) ||
                       ( sizeof( EngineTypes::VertexIndexType ) == 4 ),
                       "Unsupported vertex index data type" );

        return ( ( sizeof( EngineTypes::VertexIndexType ) == 2 ) ?
                 GL_UNSIGNED_SHORT : ( ( sizeof( EngineTypes::VertexIndexType ) == 4 ) ?
                                       GL_UNSIGNED_INT : 0 ) );
    }

protected:
    // Used to prevent copying
    IBO& operator = (const IBO& other);
    IBO(const IBO& other);

    inline void Bind() const
    {
        glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_Id );
        CheckForGLErrors();
    }

    inline void Unbind() const
    {
        glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
        CheckForGLErrors();
    }

private:
    GLuint m_Id;
    const DataBufferUsagePattern m_DataBufUsagePatter;
};

#endif // IBO_H_INCLUDED
