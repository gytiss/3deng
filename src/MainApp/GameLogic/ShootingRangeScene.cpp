#include "ShootingRangeScene.h"
#include "AK47.h"
#include "ShootingTarget.h"
#include "GameObjectTypes.h"

#include <Resources/MeshManager.h>
#include <Resources/TextureManager.h>
#include <Resources/MaterialManager.h>
#include <Physics/PhysicsManager.h>
#include <Resources/Audio/AudioDataManager.h>
#include <Resources/Audio/SoundManager.h>
#include <Scene/Renderer.h>
#include <Scene/MeshRenderer.h>
#include <Scene/SkinnedMeshRenderer.h>
#include <Scene/Animator.h>
#include <Scene/MeshCollider.h>
#include <Scene/StaticInfinitePlaneCollider.h>
#include <Scene/MeshFilter.h>
#include <Scene/BoxCollider.h>
#include <Scene/ConeCollider.h>
#include <Scene/Transform.h>
#include <Scene/RigidBody.h>
#include <Scene/SoundSource.h>

//-----------------------------------------------------------------------

ShootingRangeScene::ShootingRangeScene(Camera& camera,
                                       SystemInterface& systemInterface,
                                       PhysicsManager& physicsManager,
                                       MeshManager& meshManager,
                                       AnimationManager& animationManager,
                                       MaterialManager& materialManager,
                                       AudioDataManager& audioDataManager,
                                       SoundManager& soundManager)
{
    /*-------------------------------------------*/

    GameObject* roboMap = new GameObject( "RoboMap", GameObjectTypes::c_Environment );
    MeshRenderer* roboMapRenderer = new MeshRenderer( *roboMap, materialManager );
    MeshFilter* roboMapMeshFilter = new MeshFilter( *roboMap, meshManager );
    MeshManager::ModelMeshes roboMapModelMeshes;
    meshManager.LoadModel( "DemoRoomData/Models/RoboMap/RoboMapNoSkin.mobj", roboMapModelMeshes );
    roboMapMeshFilter->SetMesh( roboMapModelMeshes[0] );
    roboMapRenderer->SetSubMeshMaterial( 0, materialManager.LoadMaterial( "DemoRoomData/Models/RoboMap/WoodenDoorStencilLambertMat.mat" ) );
    roboMapRenderer->SetSubMeshMaterial( 1, materialManager.LoadMaterial( "DemoRoomData/Models/RoboMap/CobbleStoneLambertMat.mat" ) );
    roboMapRenderer->SetSubMeshMaterial( 2, materialManager.LoadMaterial( "DemoRoomData/Models/RoboMap/StoneBrickWallMaterial.mat" ) );
    roboMapRenderer->SetSubMeshMaterial( 3, materialManager.LoadMaterial( "DemoRoomData/Models/RoboMap/StoneCeilingMat.mat" ) );
    roboMapRenderer->SetSubMeshMaterial( 4, materialManager.LoadMaterial( "DemoRoomData/Models/RoboMap/ChurchStoneLambertMat.mat" ) );
    roboMapRenderer->SetSubMeshMaterial( 5, materialManager.LoadMaterial( "DemoRoomData/Models/RoboMap/BarsDarkMetalMaterial.mat" ) );
    MeshCollider* roboMapCollider = new  MeshCollider( *roboMap, physicsManager );

    AudioDataManager::AudioDataHandle audioHandle = audioDataManager.LoadAudioData( "DemoRoomData/Audio/water-dripping-in-cave.ogg" );
    SoundSource* soundSource = new SoundSource( *roboMap, systemInterface, soundManager, audioDataManager );
    soundSource->SetFloatParameter( AudioDevice::Source::e_FloatParamRolloffFactor, 0.0f );
    soundSource->SetIsRelative( true );
    soundSource->SetAudioData( audioHandle );
    soundSource->Play();

    /*-------------------------------------------*/

//    GameObject* densePlane = new GameObject( "Dense Plane" );
//    densePlane->GetComponent<Transform>()->SetPosition( glm::vec3( -2.0f, 0.5f, 0.0f ) );
//    MeshRenderer* densePlaneRenderer = new MeshRenderer( *densePlane, materialManager );
//    MeshFilter* densePlaneMeshFilter = new MeshFilter( *densePlane, meshManager );
//    MeshManager::ModelMeshes densePlaneModelMeshes;
//    meshManager.LoadModel( "DemoRoomData/Models/DensePlane/DensePlaneNoSkin.mobj", densePlaneModelMeshes );
//    densePlaneMeshFilter->SetMesh( densePlaneModelMeshes[0] );
//    densePlaneRenderer->SetSubMeshMaterial( 0, materialManager.LoadMaterial( "DemoRoomData/Models/DensePlane/DensePlaneTestMat.mat" ) );
//    BoxCollider* densePlaneCollider = new  BoxCollider( *densePlane, densePlaneRenderer->GetAABB().halfExtents, physicsManager );

    /*-------------------------------------------*/


    /*-------------------------------------------*/

//    GameObject* densePlane = new GameObject( "TestMesh" );
//    densePlane->GetComponent<Transform>()->SetPosition( glm::vec3( -2.0f, 0.5f, 0.0f ) );
//    MeshRenderer* densePlaneRenderer = new MeshRenderer( *densePlane, materialManager );
//    MeshFilter* densePlaneMeshFilter = new MeshFilter( *densePlane, meshManager );
//    MeshManager::ModelMeshes densePlaneModelMeshes;
//    meshManager.LoadModel( "DemoRoomData/Models/TestMesh/TestMesh.mobj", densePlaneModelMeshes );
//
//    densePlaneMeshFilter->SetMesh( densePlaneModelMeshes[0] );
//    size_t subMeshIndex = 0;
//    for(const Mesh::SubMesh& subMesh : densePlaneModelMeshes[0]->GetSubMeshes() )
//    {
//        densePlaneRenderer->SetSubMeshMaterial( subMeshIndex, materialManager.LoadMaterial( subMesh.m_InitialMaterialPath ) );
//        subMeshIndex++;
//    }

//    densePlaneRenderer->SetSubMeshMaterial( 0, materialManager.LoadMaterial( "DemoRoomData/Models/TestMesh/Mat1.mat" ) );
//    densePlaneRenderer->SetSubMeshMaterial( 1, materialManager.LoadMaterial( "DemoRoomData/Models/TestMesh/Mat1.mat" ) );
//    densePlaneRenderer->SetSubMeshMaterial( 2, materialManager.LoadMaterial( "DemoRoomData/Models/TestMesh/Mat1.mat" ) );
//    densePlaneRenderer->SetSubMeshMaterial( 3, materialManager.LoadMaterial( "DemoRoomData/Models/TestMesh/Mat1.mat" ) );
//    densePlaneRenderer->SetSubMeshMaterial( 4, materialManager.LoadMaterial( "DemoRoomData/Models/TestMesh/Mat1.mat" ) );
//    densePlaneRenderer->SetSubMeshMaterial( 5, materialManager.LoadMaterial( "DemoRoomData/Models/TestMesh/Mat1.mat" ) );
//    densePlaneRenderer->SetSubMeshMaterial( 6, materialManager.LoadMaterial( "DemoRoomData/Models/TestMesh/Mat1.mat" ) );

    /*-------------------------------------------*/


    ShootingTarget* target1 = new ShootingTarget( "Target 1", meshManager, animationManager, materialManager, physicsManager );
    ShootingTarget* target2 = new ShootingTarget( "Target 2", meshManager, animationManager, materialManager, physicsManager );
    ShootingTarget* target3 = new ShootingTarget( "Target 3", meshManager, animationManager, materialManager, physicsManager );
    ShootingTarget* target4 = new ShootingTarget( "Target 4", meshManager, animationManager, materialManager, physicsManager );

    target1->GetComponent<Transform>()->SetPosition( glm::vec3( -2.5f, 0.0f, 1.0f ) );
    target2->GetComponent<Transform>()->SetPosition( glm::vec3( -2.0f, 0.0f, -0.5f ) );
    target3->GetComponent<Transform>()->SetPosition( glm::vec3( -4.5f, 0.0f, -2.2f ) );
    target4->GetComponent<Transform>()->SetPosition( glm::vec3( -2.5f, 0.0f, -3.5f ) );

    AddGameObject( roboMap );
    AddGameObject( target1 );
    AddGameObject( target2 );
    AddGameObject( target3 );
    AddGameObject( target4 );
    AddGameObject( new AK47( camera,
                             systemInterface,
                             meshManager,
                             animationManager,
                             materialManager,
                             physicsManager,
                             audioDataManager,
                             soundManager ) );
}

//-----------------------------------------------------------------------
