#ifndef MAINLOOP_H_INCLUDED
#define MAINLOOP_H_INCLUDED

class SystemInterface;
class GraphicsDevice;
class OpenGLDebugOutput;
class FileManager;
class AnimationManager;
class MeshManager;
class TextureManager;
class MaterialManager;
class RenderManager;
class PhysicsManager;
class AudioDataManager;
class SoundManager;

#ifdef VR_BACKEND_ENABLED
    class VRBackend;
#endif

class MainLoop
{
public:
    MainLoop(SystemInterface& systemInterface, 
#ifdef VR_BACKEND_ENABLED
             VRBackend& vrBackend,
#endif   
             FileManager& fileManager);
    virtual ~MainLoop();

    void Update();

    inline GraphicsDevice& GetGraphicsDevice()
    {
        return *m_GraphicsDevice;
    }

protected:
    inline FileManager& GetFileManager()
    {
        return m_FileManager;
    }

    inline AnimationManager& GetAnimationManager()
    {
        return *m_AnimationManager;
    }

    inline MeshManager& GetMeshManager()
    {
        return *m_MeshManager;
    }

    inline TextureManager& GetTextureManager()
    {
        return *m_TextureManager;
    }

    inline MaterialManager& GetMaterialManager()
    {
        return *m_MaterialManager;
    }

    inline RenderManager& GetRenderManager()
    {
        return *m_RenderManager;
    }

    inline PhysicsManager& GetPhysicsManager()
    {
        return *m_PhysicsManager;
    }

    inline AudioDataManager& GetAudioDataManager()
    {
        return *m_AudioDataManager;
    }

    inline SoundManager& GetSoundManager()
    {
        return *m_SoundManager;
    }

    inline float GetCurrentFrameTimeInMilliseconds() const
    {
        return m_CurrentFrameTimeInMilliseconds;
    }

    virtual void _UpdateInputs(float frameTimeInSeconds);

    /**
     * Updated at the same time step as the Physics state
     * and is called just before the Physics is stepped.
     */
    virtual void _UpdateFixedStepApplicationState(float frameTimeInSeconds);

    /**
     * Updated at fast as possible i.e. like the Renderer
     */
    virtual void _UpdateOtherApplicationState(float frameTimeInSeconds);

    /**
     * Called after all of the other state updates have been done.
     */
    virtual void _LastUpdateOtherApplicationState(float frameTimeInSeconds);

    virtual void _Render(float frameTimeInSeconds);

private:
    // Used to prevent copying
    MainLoop& operator = (const MainLoop& other);
    MainLoop(const MainLoop& other);

    void _MeasureFPS(float frameTimeInSeconds);
    void _StepPhysics(float frameTimeInSeconds);

    void _SetupGraphicsDevice(GraphicsDevice& graphicsDevice,
                              OpenGLDebugOutput& openGLDebugOutput);

    void _CreateManagers(SystemInterface& systemInterface,
#ifdef VR_BACKEND_ENABLED
                         VRBackend& vrBackend,
#endif 
                         GraphicsDevice& graphicsDevice,
                         FileManager& fileManager,
                         AnimationManager*& animationManager,
                         MeshManager*& meshManager,
                         TextureManager*& textureManager,
                         MaterialManager*& materialManager,
                         RenderManager*& renderManager,
                         PhysicsManager*& physicsManager,
                         AudioDataManager*& audioDataManager,
                         SoundManager*& soundManager);

    void _DestroyManagers(AnimationManager*& animationManager,
                          MeshManager*& meshManager,
                          TextureManager*& textureManager,
                          MaterialManager*& materialManager,
                          RenderManager*& renderManager,
                          PhysicsManager*& physicsManager,
                          AudioDataManager*& audioDataManager,
                          SoundManager*& soundManager);

    SystemInterface& m_SystemInterface;
    GraphicsDevice* m_GraphicsDevice;
    OpenGLDebugOutput* m_OpenGLDebugOutput;

    FileManager& m_FileManager;
    AnimationManager* m_AnimationManager;
    MeshManager* m_MeshManager;
    TextureManager* m_TextureManager;
    MaterialManager* m_MaterialManager;
    RenderManager* m_RenderManager;
    PhysicsManager* m_PhysicsManager;
    AudioDataManager* m_AudioDataManager;
    SoundManager* m_SoundManager;

    bool m_FirstUpdate;
    unsigned int m_NumOfFramesSinceTheLastFPSCheck;
    double m_CurrentTimeInSeconds;
    float m_AccumulatedFrameTimeInSeconds;
    float m_TimeSinceTheLastFPSMeasurementInSeconds;
    float m_CurrentFrameTimeInMilliseconds;
};

#endif // MAINLOOP_H_INCLUDED
