#ifndef SPHERECOLLIDER_H_INCLUDED
#define SPHERECOLLIDER_H_INCLUDED

#include "Collider.h"

class SphereCollider : public Collider
{
    GAME_OBJECT_COMPONENT_TYPE(e_TypeCollider)

public:
    SphereCollider(GameObject& owningGameObject,
                   float radius,
                   PhysicsManager& physicsManager);

    void SetRadius(float radius);
    float GetRadius() const;
};

#endif // SPHERECOLLIDER_H_INCLUDED
