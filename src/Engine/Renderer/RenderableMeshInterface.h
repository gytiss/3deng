#ifndef RENDERABLEMESHINTERFACE_H_INCLUDED
#define RENDERABLEMESHINTERFACE_H_INCLUDED

#include <Resources/MaterialManager.h>
#include <CommonDefinitions.h>
#include <EngineTypes.h>

class VAO;
class ShaderProgGBuffer;
class ShaderProgDebugObjects;

class RenderableMeshInterface
{
public:
    struct SubMesh
    {
        SubMesh()
        : m_FirstIndex( 0 ),
          m_IndexCount( 0 ),
          m_VAO( nullptr ),
          m_MaterialHandle()
        {
        }

        unsigned int m_FirstIndex;
        unsigned int m_IndexCount;
        const VAO* m_VAO;
        MaterialManager::MaterialHandle m_MaterialHandle;
    };

    virtual ~RenderableMeshInterface() {};

    virtual bool IsAnimated() const = 0;
    virtual const EngineTypes::MatrixArray* GetBoneMatrices() const { return nullptr; };
    virtual void Render() {};
    virtual void RenderDebugData(ShaderProgDebugObjects& UNUSED_PARAM(shaderProg)) {}

    /**
     * Number of bytes a triangle data pointer needs to be
     * incremented to get to the next vertex position.
     */
    virtual unsigned int GetVertexDataStride() const { return 0; }

    virtual const float* GetVertexDataBase() const { return nullptr; }
    virtual const unsigned int* GetIndexDataBase() const { return nullptr; }
    virtual unsigned int GetIndexCount() const { return 0; }
    virtual unsigned int GetVertexCount() const { return 0; }
    virtual SubMesh GetSubMesh(unsigned int UNUSED_PARAM(meshIndex)) const { SubMesh mesh; return mesh; }
    virtual size_t GetSubMeshCount() const { return 0; }
};

#endif // RENDERABLEMESHINTERFACE_H_INCLUDED
