
in vec2 ex_uvCoord;
out vec4 FragColor;
uniform sampler2D u_DiffuseMap;

void main(void)
{
    FragColor = texture(u_DiffuseMap, ex_uvCoord);
}
