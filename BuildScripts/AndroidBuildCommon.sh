#!/usr/bin/env bash

CheckIfFileExists ()
{
    local _RETURN_CODE=1 # Failure

    if [ -f "$1" ];
    then
        _RETURN_CODE=0 # Success
    fi
    
    return $_RETURN_CODE
}

CheckIfCompilationResultsExist ()
{
    local _RETURN_CODE=0 # Success

    local RESULTS_DIR="$1"
    local RESULT_FILES_LIST="$2"
    
    IFS=' ' read -a RESULT_FILES_ARRAY <<< "$RESULT_FILES_LIST"

    for result_file_name in "${RESULT_FILES_ARRAY[@]}"
    do    
        if ! $(CheckIfFileExists $RESULTS_DIR/$result_file_name);
        then
            _RETURN_CODE=1 # Failure
            break
        fi
    done
    
    return $_RETURN_CODE
}

ReplaceArchMarkersWithArchName ()
{
    local STR_TO_DO_SUBSTITUTION_ON="$1"
    local ARCH_NAME="$2"
    
    local SED_SCRIPT_STR="s/<<ANDROID_ARCH>>/$ARCH_NAME/g"

    echo "$STR_TO_DO_SUBSTITUTION_ON" | sed -r $SED_SCRIPT_STR
}

RelativePathBetweenTwoAbsolutePaths ()
{
    # both $1 and $2 are absolute paths beginning with /
    # returns relative path to $2/$target from $1/$source
    local source=$1
    local target=$2

    local common_part=$source # for now
    local result="" # for now

    while [[ "${target#$common_part}" == "${target}" ]]; do
        # no match, means that candidate common part is not correct
        # go up one level (reduce common part)
        common_part="$(dirname $common_part)"
        # and record that we went back, with correct / handling
        if [[ -z $result ]]; then
            result=".."
        else
            result="../$result"
        fi
    done

    if [[ $common_part == "/" ]]; then
        # special case for root (no common path)
        result="$result/"
    fi

    # since we now have identified the common part,
    # compute the non-common part
    local forward_part="${target#$common_part}"

    # and now stick all parts together
    if [[ -n $result ]] && [[ -n $forward_part ]]; then
        result="$result$forward_part"
    elif [[ -n $forward_part ]]; then
        # extra slash removal
        result="${forward_part:1}"
    fi

    echo $result
}

TrimLeadingAndTrailingSpaces ()
{
    local STR_TO_TRIM="$1"
    TRIMMED_STR="$(echo -e "${STR_TO_TRIM}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')"
    echo $TRIMMED_STR
}

IsSharedLibName ()
{
    local _RETURN_CODE=1 # Failure
    local LIB_NAME="$1"
    
    if [[ "$LIB_NAME" == *".so" ]];
    then
        _RETURN_CODE=0 # Success
    fi
    
    return $_RETURN_CODE
}

CopySharedLibFiles ()
{
    local SOURCE_DIR="$1"
    local DESTINATION_DIR="$2"
    local FILES_TO_COPY_LIST="$3"
    
    IFS=' ' read -a FILES_TO_COPY_ARRAY <<< "$FILES_TO_COPY_LIST"

    for file_to_copy in "${FILES_TO_COPY_ARRAY[@]}"
    do    
        if $(IsSharedLibName "$(TrimLeadingAndTrailingSpaces "${file_to_copy}")");
        then            
            cp "$SOURCE_DIR/$file_to_copy" "$DESTINATION_DIR" 2>/dev/null && \
            echo "Copied shared lib \"$file_to_copy\" from \"$SOURCE_DIR/\" to \"$DESTINATION_DIR/\""
        fi
    done
}

##############################################
# Check if a given array contains an element
##############################################
ContainsElement () 
{
    local ELEMENT_TO_LOOK_FOR="$1"
    shift
    local ARRAY=("$@")
    
    local e
    
    for e in "${ARRAY[@]}"
    do
        if [[ "$e" == "$ELEMENT_TO_LOOK_FOR" ]];
        then
            return 0;
        fi
    done

    return 1
}