#ifndef GUIMANAGER_H_INCLUDED
#define GUIMANAGER_H_INCLUDED

#include "GUIFileInterface.h"
#include "GUISystemInterface.h"
#include "GUIRenderInterface.h"
#include <InputMapping/InputMapper.h>
#include <SystemInterface.h>
#include <map>

class GUIContext;
class GraphicsDevice;
class FileManager;

class GUIManager
{
public:
   /**
    * @param graphicsDevice - Interface to the graphics hardware
    * @param systemInterface - System interface used for drawable surface size change events
    * @param guiDataRootDirectoryPath - Path to a directory where the GUI data will be loaded from
    * @param window - Needed for gettig time in seconds since the start of the application
    */
    GUIManager(GraphicsDevice& graphicsDevice,
               SystemInterface& systemInterface,
               FileManager& fileManager,
               const std::string& guiDataRootDirectoryPath,
               bool debuggingEnabled = false);

   ~GUIManager();

    /**
     * Used for supplying input to the currently active context
     */
    static void InputCallback(InputMapping::MappedInput& inputs, float frameTimeInSeconds, void* guiManager);

    bool AddContext(const std::string& contextName);

    /**
     * Makes a given context to be the active one
     */
    bool SwitchToContext(const std::string& contextName);

    GUIContext* GetCurrentContext();

    GUIRenderInterface& GetRenderInterface();

private:
    enum ButtonState
    {
        e_ButtonStateUp = 0,
        e_ButtonStateDown = 1,
        e_ButtonStateInvalid
    };

    typedef std::map<std::string, GUIContext*> ContextMap_t;

    void _LoadFonts();
    void _InputCallback(InputMapping::MappedInput& inputs);

   /**
    * Sets dimensions of a screen the GUI is drawn on
    */
    void _SetScreenDimensions(const SystemInterface::DrawableSurfaceSize& size);

    static void _DrawableSurfaceSizeChaged(const SystemInterface::DrawableSurfaceSize& newSize, void* userData);

    GraphicsDevice& m_GraphicsDevice;
    SystemInterface& m_SystemInterface;
    FileManager& m_FileManager;
    SystemInterface::DrawableSurfaceSize m_DrawableSurfaceSize;

    const std::string m_DataRootDirPath;

    GUIFileInterface m_FileInterface;
    GUIRenderInterface m_RenderInterface;
    GUISystemInterface m_SysInterface;

    ContextMap_t m_Contexts;
    GUIContext* m_CurrentContext;

    ButtonState m_LeftMouseButtonState;
    ButtonState m_RightMouseButtonState;
    ButtonState m_MiddleMouseButtonState;

    const bool m_DebuggingEnabled;
};

#endif // GUIMANAGER_H_INCLUDED
