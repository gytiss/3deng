#ifndef COLLIDERWIDGETBASEQOBJECTPARENT_H_INCLUDED
#define COLLIDERWIDGETBASEQOBJECTPARENT_H_INCLUDED

#include "ComponentWidget.h"

/**
 * This class is needed, because it is not possible to have slots in
 * the ColliderWidgetBase, because Qt does does not support templated
 * classes as Q objects.
 */
class ColliderWidgetBaseQObjectParent : public ComponentWidget
{
    Q_OBJECT

public:
    virtual ~ColliderWidgetBaseQObjectParent();

protected:
    // The constructor is protected because this object is not supposed to be instanciated on its own
    ColliderWidgetBaseQObjectParent(CategorizedListWidget* listWidgetToAddOurselvesTo,
                                    const std::string& colliderName,
                                    QWidget* parent = 0);

    virtual void XCenterOffsetLineEditEditingFinishedImpl() = 0;
    virtual void YCenterOffsetLineEditEditingFinishedImpl() = 0;
    virtual void ZCenterOffsetLineEditEditingFinishedImpl() = 0;
    virtual void IsTriggerCheckBoxStateChangedImpl(int state) = 0;

private slots:
    void XCenterOffsetLineEditEditingFinished();
    void YCenterOffsetLineEditEditingFinished();
    void ZCenterOffsetLineEditEditingFinished();
    void IsTriggerCheckBoxStateChanged(int state);
};

#endif // COLLIDERWIDGETBASEQOBJECTPARENT_H_INCLUDED
