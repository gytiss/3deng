#ifndef SPOTLIGHTMARKER_H_INCLUDED
#define SPOTLIGHTMARKER_H_INCLUDED

#include "EntityMarker.h"
#include <GraphicsDevice/VBO.h>

class SpotLightMarker : public EntityMarker
{
public:
    SpotLightMarker(PhysicsManager& physicsManager,
                    MaterialManager& materialManager,
                    TextureManager& textureManager);
    void RecalculateRenderables() override;
    void SetCutOffAngleInDegrees(float angle);
    void SetRadius(float radius);
    void SetDirectionInWorldSpace(const glm::vec3 dir);



private:
    inline glm::vec3 _GetUnitConeScaleFactor() const
    {
        return m_UnitConeScaleFactor;
    }

    inline glm::quat _GetUnitConeRotationAngles() const
    {
        return m_UnitConeRotation;
    }

    inline glm::vec3 _GetUnitConeTranslationFactor(const glm::vec3& lightPosInWorldSpace) const
    {
        glm::vec3 lightPos = lightPosInWorldSpace;
        lightPos.y += -m_ScaledUnitConeHeight;
        return lightPos;
    }

    void _CalculateUnitConeScaleFactorAndHeight();

    void _SetVertexData(VBO* vbo);
    size_t _GenerateLineVertices();
    size_t _GeneratePointVertices();
    const float c_UnitRadius;
    VertexArray_t m_VertexBuffer;
    size_t m_CircleVertexCount;
    size_t m_LinesVertexCount;
    size_t m_PointsVertexCount;
    float m_CutOffAngleInDegrees;
    float m_Radius;
    float m_ScaledUnitConeHeight;
    glm::vec3 m_DirectionInWorldSpace;
    glm::vec3 m_UnitConeScaleFactor;
    glm::quat m_UnitConeRotation;
};

#endif // SPOTLIGHTMARKER_H_INCLUDED
