#include "ModelBinObj.h"
#include "MsgLogger.h"
#include "StringUtils.h"
#include <CommonDefinitions.h>

#include <fstream>
#include <iostream>
#include <sstream>

using std::cout;
using std::endl;


    //////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS METHODS
	//////////////////////////////////////////////////////////////////////////

	//-----------------------------------------------------------------------

    ModelBinObj::ModelBinObj()
    : m_NumberOfVertexCoords( 0 ),
      m_NumberOfTextureCoords( 0 ),
      m_NumberOfNormals( 0 ),
      m_NumberOfTriangles( 0 ),
      m_NumberOfMaterials( 0 ),
      m_NumberOfMeshes( 0 ),
      m_DirectoryPath( "" ),
      m_Meshes( nullptr ),
      m_Materials( nullptr ),
      m_VertexBuffer( nullptr ),
      m_IndexBuffer( nullptr )
    {

    }

	//-----------------------------------------------------------------------

	//////////////////////////////////////////////////////////////////////////
	// DESTRUCTORS METHODS
	//////////////////////////////////////////////////////////////////////////

	//-----------------------------------------------------------------------

    ModelBinObj::~ModelBinObj()
    {
        delete[] m_Meshes;
        delete[] m_VertexBuffer;
        delete[] m_IndexBuffer;
        delete[] m_Materials;
    }

	//-----------------------------------------------------------------------

	//////////////////////////////////////////////////////////////////////////
	// PUBLIC METHODS
	//////////////////////////////////////////////////////////////////////////

	//-----------------------------------------------------------------------

    bool ModelBinObj::import(std::string modelFileName)
    {
        size_t bytesRead = 0;
        std::fstream binObj;

        binObj.open (modelFileName.c_str(), std::fstream::in | std::fstream::binary);//Open file

        if(!binObj.is_open())//If unable to open the file, then reading has failed
        {
            return false;
        }

        /*--------------------- Get file size ---------------------*/
        binObj.seekg (0, std::ios::end);//Seek to the end of the file
        const size_t length = static_cast<size_t>(binObj.tellg());//Get number of bytes in the file
        binObj.seekg (0, std::ios::beg);//Seek to the beginnig of the file
        /*---------------------------------------------------------*/

        int8_t* dataBuffer = new int8_t[length];//Data buffer for the file


        /*------------- Read the files into the buffer -------------*/
        binObj.read(reinterpret_cast<char*>(dataBuffer), length);
        binObj.close();
        /*----------------------------------------------------------*/


        /*-------- Check if the file is a valid BinObj file --------*/
        std::stringstream fileFormatIdentifier;
        fileFormatIdentifier << getChar(dataBuffer, &bytesRead);
        fileFormatIdentifier << getChar(dataBuffer, &bytesRead);
        fileFormatIdentifier << getChar(dataBuffer, &bytesRead);
        fileFormatIdentifier << getChar(dataBuffer, &bytesRead);
        fileFormatIdentifier << getChar(dataBuffer, &bytesRead);
        fileFormatIdentifier << getChar(dataBuffer, &bytesRead);

        if( fileFormatIdentifier.str().compare("BINOBJ") != 0 )
        {
            MsgLogger::log(MsgLogger::ERROR_MSG, "File \""+modelFileName+"\" is not a valid BinObj file.");
            return false;
        }

        int32_t fileFormatVersion = getInt(dataBuffer, &bytesRead);

        if( fileFormatVersion != 1 )
        {
            MsgLogger::log(MsgLogger::ERROR_MSG, "Loader only supports BinObj of version \"1\", file \""+modelFileName+"\" is of version \""+numToString(fileFormatVersion)+"\".");
            return false;
        }
        /*----------------------------------------------------------*/


        /*------- Get number of elements to read from file ---------*/
        m_NumberOfVertexCoords = getInt(dataBuffer,&bytesRead);
        getInt(dataBuffer,&bytesRead);//Num of indices are skipped, because it is ==(m_numberOfTriangles*3)
        m_NumberOfTriangles = getInt(dataBuffer,&bytesRead);
        m_NumberOfTextureCoords = getInt(dataBuffer,&bytesRead);
        /*----------------------------------------------------------*/


        /*---------------- Allocate memory for Vertices and Indices ----------------*/
        try
        {
            m_VertexBuffer = new Vertex[m_NumberOfVertexCoords];
        }
        catch (std::bad_alloc& ba)
        {
            MsgLogger::log(MsgLogger::ERROR_MSG, "While loading model "+modelFileName+" vertex buffer allocation failed with the folliwing message: "+ba.what());
        }

        try
        {
            m_IndexBuffer = new int[m_NumberOfTriangles * 3];
        }
        catch (std::bad_alloc& ba)
        {
            MsgLogger::log(MsgLogger::ERROR_MSG, "While loading model "+modelFileName+" : index buffer allocation failed with the folliwing message: "+ba.what());
        }
        /*--------------------------------------------------------------------------*/


        /*---------------------- Read vertices ----------------------*/
        for(int v = 0 ; v < m_NumberOfVertexCoords ; v++)
        {
            m_VertexBuffer[v].position[0]=getFloat(dataBuffer,&bytesRead);
            m_VertexBuffer[v].position[1]=getFloat(dataBuffer,&bytesRead);
            m_VertexBuffer[v].position[2]=getFloat(dataBuffer,&bytesRead);

            m_VertexBuffer[v].tangent[0]=0.0f;
            m_VertexBuffer[v].tangent[1]=0.0f;
            m_VertexBuffer[v].tangent[2]=0.0f;

            m_VertexBuffer[v].bitangent[0]=0.0f;
            m_VertexBuffer[v].bitangent[1]=0.0f;
            m_VertexBuffer[v].bitangent[2]=0.0f;
        }
        /*-----------------------------------------------------------*/


        /*---------------------- Read normals -----------------------*/
        for(int v = 0 ; v < m_NumberOfVertexCoords ; v++)//Normals
        {
            m_VertexBuffer[v].normal[0]=getFloat(dataBuffer,&bytesRead);
            m_VertexBuffer[v].normal[1]=getFloat(dataBuffer,&bytesRead);
            m_VertexBuffer[v].normal[2]=getFloat(dataBuffer,&bytesRead);
        }
        /*-----------------------------------------------------------*/


        /*---------------- Read texture coordinates -----------------*/
        for(int v = 0 ; v < m_NumberOfVertexCoords ; v++)//TexCoords
        {
            m_VertexBuffer[v].texCoord[0]=getFloat(dataBuffer,&bytesRead);
            m_VertexBuffer[v].texCoord[1]=getFloat(dataBuffer,&bytesRead);
        }
        /*-----------------------------------------------------------*/


        /*---------------------- Read indices -----------------------*/
        for(int i=0 ; i < (m_NumberOfTriangles*3) ; i++)
        {
            m_IndexBuffer[i]=getInt(dataBuffer,&bytesRead);
        }
        /*-----------------------------------------------------------*/



        /*---------------------- Read materials ---------------------*/
        m_NumberOfMaterials = getInt(dataBuffer,&bytesRead);
        m_Materials = new Material[m_NumberOfMaterials];

        for(int mat=0 ; mat < m_NumberOfMaterials ; mat++)//Materials
        {
            m_Materials[mat].ambient[0]=getFloat(dataBuffer,&bytesRead);
            m_Materials[mat].ambient[1]=getFloat(dataBuffer,&bytesRead);
            m_Materials[mat].ambient[2]=getFloat(dataBuffer,&bytesRead);
            m_Materials[mat].ambient[3]=getFloat(dataBuffer,&bytesRead);

            m_Materials[mat].diffuse[0]=getFloat(dataBuffer,&bytesRead);
            m_Materials[mat].diffuse[1]=getFloat(dataBuffer,&bytesRead);
            m_Materials[mat].diffuse[2]=getFloat(dataBuffer,&bytesRead);
            m_Materials[mat].diffuse[3]=getFloat(dataBuffer,&bytesRead);

            m_Materials[mat].specular[0]=getFloat(dataBuffer,&bytesRead);
            m_Materials[mat].specular[1]=getFloat(dataBuffer,&bytesRead);
            m_Materials[mat].specular[2]=getFloat(dataBuffer,&bytesRead);
            m_Materials[mat].specular[3]=getFloat(dataBuffer,&bytesRead);

            m_Materials[mat].shininess=getFloat(dataBuffer,&bytesRead);
            m_Materials[mat].alpha=getFloat(dataBuffer,&bytesRead);

            int8_t maps = getChar(dataBuffer,&bytesRead);

            if(maps == 0x1 || maps == 0x3)//If material has colour map, then read its file name
            {
                std::stringstream ss;
                char c = getChar(dataBuffer,&bytesRead);
                while(c != '\0')
                {
                    ss << c;
                    c = getChar(dataBuffer,&bytesRead);
                }
                m_Materials[mat].colorMapFilename=ss.str();
            }
            else if(maps == 0x2 || maps == 0x3)//If material has bump map, then read its file name
            {
                std::stringstream ss;
                char c = getChar(dataBuffer,&bytesRead);
                while(c != '\0')
                {
                    ss << c;
                    c = getChar(dataBuffer,&bytesRead);
                }
                m_Materials[mat].bumpMapFilename=ss.str();
            }
        }
        /*-----------------------------------------------------------*/


        /*------------------ Read mesh descriptors ------------------*/
        m_NumberOfMeshes = getInt(dataBuffer,&bytesRead);
        m_Meshes = new Mesh[m_NumberOfMeshes];

        for(int m=0 ; m < m_NumberOfMeshes ; m++)//Meshes
        {
            m_Meshes[m].startIndex=getInt(dataBuffer,&bytesRead);
            m_Meshes[m].triangleCount=getInt(dataBuffer,&bytesRead);
            int materialID = getInt(dataBuffer,&bytesRead);
            m_Meshes[m].pMaterial=&m_Materials[materialID];
        }
        /*-----------------------------------------------------------*/

        delete[] dataBuffer;

        return true;//BinObj file was loaded successfully
    }

    //-----------------------------------------------------------------------

	//////////////////////////////////////////////////////////////////////////
	// PRIVATE METHODS
	//////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------

    inline int32_t ModelBinObj::getInt(int8_t* dataBuffer, size_t* bytesRead)
    {
        int32_t res;
        res = *(reinterpret_cast<int32_t*>(dataBuffer+*bytesRead));
        *bytesRead += 4;
        return res;
    }

    //-----------------------------------------------------------------------

    inline int8_t ModelBinObj::getChar(int8_t* dataBuffer, size_t* bytesRead)
    {
        int8_t res;
        res = *(dataBuffer+*bytesRead);
        *bytesRead += 1;
        return res;
    }

    //-----------------------------------------------------------------------

    inline float ModelBinObj::getFloat(int8_t* dataBuffer, size_t* bytesRead)
    {
        float res;
        res = *(reinterpret_cast<float*>(dataBuffer+*bytesRead));
        *bytesRead += 4;
        return res;
    }

    //-----------------------------------------------------------------------

