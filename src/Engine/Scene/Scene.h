#ifndef SCENE_SCENE_H_INCLUDED
#define SCENE_SCENE_H_INCLUDED

#include <unordered_map>
#include <vector>

#include "GameObject.h"

class Scene
{
public:
    Scene();

    void Initialize();

    /**
     * Called just before the Physics is updated. Call rate is the same as the Physics update rate and it is not equal to the FPS
     */
    void FixedUpdate();

    /**
     * Called every frame i.e. at the same rate as the Renderer
     */
    void Update();

    /**
     * Called at the same rate as the Update() function, but after all of the objects in the scene had been updated (had their Update() function called).
     */
    void LastUpdate();

    void AddGameObject(GameObject* gameObject);
    GameObject* FindGameObjectByName(const std::string& gameObjectName);

    void GetSceneRenderables(GameObject::RenderablesList& renderables);

private:
    typedef std::vector<GameObject*> GameObjectList;
    typedef std::unordered_map<std::string, GameObject*> NamesToGameObjectsMap;

    bool m_Initialized;
    GameObjectList m_GameObjectList;
    NamesToGameObjectsMap m_NamesToGameObjectsMap;
};

#endif // SCENE_SCENE_H_INCLUDED
