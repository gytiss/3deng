#include "FBOColorAttachmentTexture.h"

//-----------------------------------------------------------------------

FBOColorAttachmentTexture::FBOColorAttachmentTexture(GraphicsDevice& graphicsDevice,
                                                     size_t textureWidth,
                                                     size_t textureHeight,
                                                     TypeIdx textureTypeIdx,
                                                     DataFormat dataFormat)
: Texture( graphicsDevice, textureTypeIdx, Texture::e_Class2DTexture, false ),
  m_DataFormat( dataFormat )
{
    Texture::InitializeStorage( static_cast<InternalDataFormat>( m_DataFormat ),
                                textureWidth,
                                textureHeight );
}

//-----------------------------------------------------------------------

#if defined(VR_BACKEND_ENABLED)
GLuint FBOColorAttachmentTexture::GetOpenGLInternalTextureID() const

{
    return Texture::GetOpenGLInternalTextureID();
}
#endif

//-----------------------------------------------------------------------

void FBOColorAttachmentTexture::Reinitialize(size_t textureWidth, size_t textureHeight)
{
    Texture::Reset();
    Texture::InitializeStorage( static_cast<InternalDataFormat>( m_DataFormat ),
                                textureWidth,
                                textureHeight );
}

//-----------------------------------------------------------------------
