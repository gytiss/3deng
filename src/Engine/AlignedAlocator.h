#ifndef ALIGNEDALOCATOR_H_INCLUDED
#define ALIGNEDALOCATOR_H_INCLUDED

#include <cstdint>
#include <stdexcept>
#include <MathUtils.h>

template <typename T, std::size_t Alignment>
class AlignedAlocator
{
public:
    // The following will be the same for virtually all allocators.
    typedef T * pointer;
    typedef const T * const_pointer;
    typedef T& reference;
    typedef const T& const_reference;
    typedef T value_type;
    typedef std::size_t size_type;
    typedef ptrdiff_t difference_type;

    T * address(T& r) const
    {
        return &r;
    }

    const T * address(const T& s) const
    {
        return &s;
    }

    std::size_t max_size() const
    {
        // The following has been carefully written to be independent of
        // the definition of size_t and to avoid signed/unsigned warnings.
        return (static_cast<std::size_t>(0) - static_cast<std::size_t>(1)) / sizeof(T);
    }


    // The following must be the same for all allocators.
    template <typename U>
    struct rebind
    {
        typedef AlignedAlocator<U, Alignment> other;
    } ;

    bool operator!=(const AlignedAlocator& other) const
    {
        return !(*this == other);
    }

    void construct(T * const p, const T& t) const
    {
        void * const pv = static_cast<void *>(p);

        new (pv) T(t);
    }

    void destroy(T * const p) const
    {
        p->~T();
    }

    // Returns true if and only if storage allocated from *this
    // can be deallocated from other, and vice versa.
    // Always returns true for stateless allocators.
    bool operator==(const AlignedAlocator& other) const
    {
        return true;
    }

    // Default constructor, copy constructor, rebinding constructor, and destructor.
    // Empty for stateless allocators.
    AlignedAlocator() = default;

    AlignedAlocator(const AlignedAlocator&) = default;

    template <typename U> AlignedAlocator(const AlignedAlocator<U, Alignment>&) {};

    ~AlignedAlocator() = default;


    // The following will be different for each allocator.
    T * allocate(const std::size_t n) const
    {
        // The return value of allocate(0) is unspecified.
        // hence we need to returns nullptr in order to avoid depending
        // on malloc(0)'s implementation-defined behavior
        // (the implementation can define malloc(0) to return nullptr,
        // in which case the bad_alloc check below would fire).
        // All allocators can return NULL in this case.
        if (n == 0)
        {
            return nullptr;
        }

        // All allocators should contain an integer overflow check.
        // The Standardization Committee recommends that std::length_error
        // be thrown in the case of integer overflow.
        if (n > max_size())
        {
            throw std::length_error("AlignedAlocator<T>::allocate() - Integer overflow.");
        }

        void * const originalAllocatedMemPointer = malloc( ( n * sizeof(T) ) + Alignment + sizeof( uintptr_t ) );

        // Allocators should throw std::bad_alloc in the case of memory allocation failure.
        if( originalAllocatedMemPointer == nullptr )
        {
            throw std::bad_alloc();
        }

        const size_t c_PointerSizeInBytes = sizeof( uintptr_t );

        void * const alignedPointer = MathUtils::GetNextAlignedAddress( static_cast<uint8_t*>( originalAllocatedMemPointer ) + c_PointerSizeInBytes, Alignment );

        // We will need the original pointer value when freeing the memory,
        // hence store it in bytes immediately preceding the aligned pointer
        const uintptr_t originalAllocatedMemPointerData = reinterpret_cast<uintptr_t>( originalAllocatedMemPointer );
        for(size_t i = 0; i < c_PointerSizeInBytes; i++)
        {
            *( static_cast<uint8_t*>( alignedPointer ) - ( i + 1 ) ) = ( ( originalAllocatedMemPointerData >> ( i * 8 ) ) & 0xFF );
        }

        return static_cast<T*>( alignedPointer );
    }

    void deallocate(T* const alignedPointer, const std::size_t UNUSED_PARAM(n)) const
    {
        // We need to extract the original pointer in order to free the memory
        const size_t c_PointerSizeInBytes = sizeof( uintptr_t );
        uintptr_t originalAllocatedMemPointerData = 0;
        for(size_t i = 0; i < c_PointerSizeInBytes; i++)
        {
            originalAllocatedMemPointerData |= ( static_cast<uintptr_t>( *( static_cast<uint8_t*>( static_cast<void*>( alignedPointer ) ) - ( i + 1 ) ) ) << ( i * 8 ) );
        }
        T* const originalAllocatedMemPointer = reinterpret_cast<T* const>( originalAllocatedMemPointerData );

        free( originalAllocatedMemPointer );
    }

    // The following will be the same for all allocators that ignore hints.
    template <typename U>
    T * allocate(const std::size_t n, const U * const UNUSED_PARAM(hint)) const
    {
        return allocate(n);
    }

private:
    AlignedAlocator& operator=(const AlignedAlocator&) = delete;
};

#endif // ALIGNEDALOCATOR_H_INCLUDED
