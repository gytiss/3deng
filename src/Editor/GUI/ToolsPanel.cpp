#include "ToolsPanel.h"
#include <Tools/ManipulatorTool.h>
#include <MainWindow.h>
#include <ui_MainWindow.h>
#include <CommonDefinitions.h>

//-----------------------------------------------------------------------

ToolsPanel::ToolsPanel(MainWindow& mainWindow, ManipulatorTool& manipulatorTool)
: m_MainWindow( mainWindow ),
  m_ManipulatorTool( manipulatorTool )
{
    connect( m_MainWindow.m_UI->selectToolButton, SIGNAL( clicked(bool) ),
             this, SLOT( SelectToolSelected(bool) ) );

    connect( m_MainWindow.m_UI->moveToolButton, SIGNAL( clicked(bool) ),
             this, SLOT( MoveToolSelected(bool) ) );

    connect( m_MainWindow.m_UI->rotateToolButton, SIGNAL( clicked(bool) ),
             this, SLOT( RotateToolSelected(bool) ) );

    connect( m_MainWindow.m_UI->scaleToolButton, SIGNAL( clicked(bool) ),
             this, SLOT( ScaleToolSelected(bool) ) );
}

//-----------------------------------------------------------------------

void ToolsPanel::SelectToolSelected(bool UNUSED_PARAM(checked))
{
    m_ManipulatorTool.SetMode( ManipulatorTool::e_ModeNone );
}

//-----------------------------------------------------------------------

void ToolsPanel::MoveToolSelected(bool UNUSED_PARAM(checked))
{
    m_ManipulatorTool.SetMode( ManipulatorTool::e_ModeTranslation );
}

//-----------------------------------------------------------------------

void ToolsPanel::RotateToolSelected(bool UNUSED_PARAM(checked))
{
    m_ManipulatorTool.SetMode( ManipulatorTool::e_ModeRotation );
}

//-----------------------------------------------------------------------

void ToolsPanel::ScaleToolSelected(bool UNUSED_PARAM(checked))
{
    m_ManipulatorTool.SetMode( ManipulatorTool::e_ModeScale );
}

//-----------------------------------------------------------------------
