#include "ComponentFloatValueValidator.h"
#include "GUIHelpers.h"
#include <QtCore/QRegExp>
#include <cassert>
#include <Engine/MathUtils.h>

//-----------------------------------------------------------------------

ComponentFloatValueValidator::ComponentFloatValueValidator(int maxDecimalPlacesInTheFinalValue,
                                                           int maxDecimalPlacesInTheWorkInProgressValue)
: QDoubleValidator(),
  m_MaxDecimalPLacedInTheFinalValue( maxDecimalPlacesInTheFinalValue )
{
    setDecimals( maxDecimalPlacesInTheWorkInProgressValue );
}

//-----------------------------------------------------------------------

QDoubleValidator::State ComponentFloatValueValidator::validate(QString& input, int& pos) const
{
    State state = QDoubleValidator::validate( input, pos );

    if( state == Acceptable )
    {
        QRegExp numberWithDecimalPointsRegexp = QRegExp( QString( "^.*\\..{%1,%1}$" ).arg( m_MaxDecimalPLacedInTheFinalValue ) );
        if( !numberWithDecimalPointsRegexp.exactMatch( input ) )
        {
            state = QValidator::Intermediate;
        }
    }

    return state;
}

//-----------------------------------------------------------------------

void ComponentFloatValueValidator::fixup(QString& input) const
{
    const int numberOfDecimalPlacesRequired = m_MaxDecimalPLacedInTheFinalValue;

    const int dotPos = input.lastIndexOf( '.' );
    const int charsAfterDot = ( ( input.size() - dotPos ) - 1 );

    if( dotPos > -1 )
    {
        if( charsAfterDot > numberOfDecimalPlacesRequired )
        {
            bool ok = false;
            float value = input.toFloat( &ok );
            assert( ok && "Failed to convert Float value string to a float number" );
            value = MathUtils::Round( value, numberOfDecimalPlacesRequired );
            input = QString::number( value, 'f', numberOfDecimalPlacesRequired );
        }
        else if( charsAfterDot < numberOfDecimalPlacesRequired )
        {
            for(int i = 0; i < ( numberOfDecimalPlacesRequired - charsAfterDot ); i++)
            {
                input.append( '0' );
            }
        }
    }
    else
    {
        input.append( '.' );
        for(int i = 0; i < numberOfDecimalPlacesRequired; i++)
        {
            input.append( '0' );
        }
    }
}
