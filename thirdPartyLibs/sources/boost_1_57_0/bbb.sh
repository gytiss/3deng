#!/usr/bin/env bash 


export AndroidNDKRoot=/home/john/Android/android-ndk-r10d/
export AndroidBinariesPath=$AndroidNDKRoot/toolchains/arm-linux-androideabi-4.8/prebuilt/linux-x86_64/bin/
export PATH=$AndroidBinariesPath:$PATH

# MAKE_TOOLCHAIN="$AndroidNDKRoot/build/tools/make-standalone-toolchain.sh"
# TOOLCHAIN_DIR="$(pwd)/android-toolchain-armv7"
# TARGET_ARCH="arm"
# 
# $MAKE_TOOLCHAIN --platform="android-18" \
#                 --arch="$TARGET_ARCH" \
#                 --stl=gnustl \
#                 --install-dir="$TOOLCHAIN_DIR"

{ 
    ./bjam -q \
        -sNO_BZIP2=1\
        -sICU_PATH=/home/john/projects/tank/thirdPartyLibs/android/libicu/armv7/ \
        include=/home/john/projects/tank/thirdPartyLibs/android/libicu/armv7/include\
        target-os=android \
        toolset=gcc-armv7 \
        variant=release \
        link=static \
        threading=multi \
        --layout=system \
        --prefix=/home/john/projects/tank/thirdPartyLibs/android/libboost/armv7 \
        define=BOOST_MATH_DISABLE_FLOAT128 \
        --without-context \
        --without-coroutine \
        --without-python \
        --without-mpi \
        --without-wave \
        --without-test \
        --without-graph \
        --without-graph_parallel \
        install 2>&1 \
        || { echo "ERROR: Failed to build boost for android!" ; exit 1 ; }
} | tee -a $PROGDIR/build.log
