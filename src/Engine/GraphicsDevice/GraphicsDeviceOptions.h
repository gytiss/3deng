#ifndef GRAPHICSDEVICEOPTIONS_H_INCLUDED
#define GRAPHICSDEVICEOPTIONS_H_INCLUDED

#include <OpenGLIncludes.h>
#include <stack>

class GraphicsDevice;

class GraphicsDeviceOptions
{
public:
    friend class GraphicsDevice;

    enum Option
    {
        e_OptionBlending = 0,
        e_OptionFaceCulling = 1,
        e_OptionDepthTesting = 2,
        e_OptionDithering,
        e_OptionPolygonFillingWithOffset,
        e_OptionPrimitiveRestartWithFixedIndex,
        e_OptionRasterizerDiscard,
        e_OptionSampleAlphaToCoverage,
        e_OptionSampleCoverage,
        e_OptionScrissorTesting,
        e_OptionStencilTesting,
#ifndef OPENGL_ES
        e_OptionFrameBufferSRGB,
        e_OptionDebugOutput,
        e_OptionDebugOutputSynchronous,
#endif
        e_OptionCount // Not an option (Used for counting)
    };

    enum PixelStoreOption
    {
        e_PixelStoreOptionPackRowLength = 0,
#ifndef OPENGL_ES
        e_PixelStoreOptionPackImageHeight,
#endif
        e_PixelStoreOptionPackSkipRows,
        e_PixelStoreOptionPackSkipPixels,
#ifndef OPENGL_ES
        e_PixelStoreOptionPackSkipImages,
#endif
        e_PixelStoreOptionPackAlignment,

        e_PixelStoreOptionUnpackRowLength,
        e_PixelStoreOptionUnpackImageHeight,
        e_PixelStoreOptionUnpackSkipRows,
        e_PixelStoreOptionUnpackSkipPIxels,
        e_PixelStoreOptionUnpackSkipImages,
        e_PixelStoreOptionUnpackAlignment,

        e_PixelStoreOptionCount // Not an option (Used for counting)
    };

    enum PixelStoreBooleanOption
    {
        e_PixelStoreBooleanOptionPackSwapBytes = 0,
        e_PixelStoreBooleanOptionPackLSBFirst = 1,
        e_PixelStoreBooleanOptionUnpackSwapBytes = 2,
        e_PixelStoreBooleanOptionUnpackLSBFirst,

        e_PixelStoreBooleanOptionCount // Not an option (Used for counting)
    };

    void OptionEnable(Option op);
    void OptionDisable(Option op);
    void OptionSaveCurrentState(Option op);
    void OptionRestorePreviousState(Option op);

    void SetPixelStoreOptionValue(PixelStoreOption op, int value);
    void PixelStoreOptionSaveCurrentValue(PixelStoreOption op);
    void PixelStoreOptionRestorePreviousValue(PixelStoreOption op);

    // These functions are NoOps when using OpenGL ES
    void PixelStoreBooleanOptionEnable(PixelStoreBooleanOption op);
    void PixelStoreBooleanOptionDisable(PixelStoreBooleanOption op);
    void PixelStoreBooleanOptionSaveCurrentState(PixelStoreBooleanOption op);
    void PixelStoreBooleanOptionRestorePreviousState(PixelStoreBooleanOption op);

private:
    GraphicsDeviceOptions();

    enum OptionState
    {
        OptionStateUnknown = 0,
        OptionStateEnabled= 1,
        OptionStateDisabled = 2
    };

    void _SetPixelStoreOptionValue(GLenum optionGLEnum, GLint optionValue);

    std::stack<OptionState> m_OptionStatesStacks[e_OptionCount];
    OptionState m_CurrentOptionStates[e_OptionCount];
    static const GLenum m_OpenGLOptionList[e_OptionCount];

    std::stack<int> m_PixelStoreOptionValuesStacks[e_PixelStoreOptionCount];
    int m_CurrentPixelStoreOptionValues[e_PixelStoreOptionCount];
    static const GLenum m_OpenGLPixelStoreOptionList[e_PixelStoreOptionCount];

#ifndef OPENGL_ES
    std::stack<OptionState> m_PixelStoreBooleanOptionStatesStacks[e_PixelStoreBooleanOptionCount];
    OptionState m_CurrentPixelStoreBooleanOptionStates[e_PixelStoreBooleanOptionCount];
    static const GLenum m_OpenGLPixelStoreBooleanOptionList[e_PixelStoreBooleanOptionCount];
#endif
};

#endif // GRAPHICSDEVICEOPTIONS_H_INCLUDEDS
