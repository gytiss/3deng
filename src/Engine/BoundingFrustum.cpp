#include "BoundingFrustum.h"
#include <glm/gtc/type_ptr.hpp>

//-----------------------------------------------------------------------

BoundingFrustum::BoundingFrustum(const glm::mat4& viewProjectionMatrix)
: m_FrustumPlanes()
{
    //    mat[0][0] + mat[0][1] + mat[0][2] + mat[0][3];
    //    mat[1][0] + mat[1][1] + mat[1][2] + mat[1][3];
    //    mat[2][0] + mat[2][1] + mat[2][2] + mat[2][3];
    //    mat[3][0] + mat[3][1] + mat[3][2] + mat[3][3];

    const glm::mat4& mat = viewProjectionMatrix;

    m_FrustumPlanes[0].x = mat[3][0] + mat[0][0];
    m_FrustumPlanes[0].y = mat[3][1] + mat[0][1];
    m_FrustumPlanes[0].z = mat[3][2] + mat[0][2];
    m_FrustumPlanes[0].w = mat[3][3] + mat[0][3];

    m_FrustumPlanes[1].x = mat[3][0] - mat[0][0];
    m_FrustumPlanes[1].y = mat[3][1] - mat[0][1];
    m_FrustumPlanes[1].z = mat[3][2] - mat[0][2];
    m_FrustumPlanes[1].w = mat[3][3] - mat[0][3];

    m_FrustumPlanes[2].x = mat[3][0] + mat[1][0];
    m_FrustumPlanes[2].y = mat[3][1] + mat[1][1];
    m_FrustumPlanes[2].z = mat[3][2] + mat[1][2];
    m_FrustumPlanes[2].w = mat[3][3] + mat[1][3];

    m_FrustumPlanes[3].x = mat[3][0] - mat[1][0];
    m_FrustumPlanes[3].y = mat[3][1] - mat[1][1];
    m_FrustumPlanes[3].z = mat[3][2] - mat[1][2];
    m_FrustumPlanes[3].w = mat[3][3] - mat[1][3];

    m_FrustumPlanes[4].x = mat[3][0] + mat[2][0];
    m_FrustumPlanes[4].y = mat[3][1] + mat[2][1];
    m_FrustumPlanes[4].z = mat[3][2] + mat[2][2];
    m_FrustumPlanes[4].w = mat[3][3] + mat[2][3];

    m_FrustumPlanes[5].x = mat[3][0] - mat[2][0];
    m_FrustumPlanes[5].y = mat[3][1] - mat[2][1];
    m_FrustumPlanes[5].z = mat[3][2] - mat[2][2];
    m_FrustumPlanes[5].w = mat[3][3] - mat[2][3];
}

//-----------------------------------------------------------------------

float BoundingFrustum::_PlaneDotCoord(const glm::vec4 plane, const glm::vec3& vector)
{
    return ( ( plane.x * vector.x ) + ( plane.y * vector.y ) + ( plane.z * vector.z ) + ( plane.w * 1.0f ) );
}


bool BoundingFrustum::Contains(const glm::vec3 pointInWorldSpace)
{
    bool retVal = true;

    for(size_t i = 0; retVal && (i < 6); i++)
    {
        if( _PlaneDotCoord( glm::normalize( m_FrustumPlanes[i] ), pointInWorldSpace ) < 0.0f )
        {
            retVal = false;
        }
    }

    return retVal;
}

//-----------------------------------------------------------------------
