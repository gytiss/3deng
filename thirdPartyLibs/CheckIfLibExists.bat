@ECHO OFF    

set SCRIPT_DIR=%~dp0
set RESULTS_DIR=%SCRIPT_DIR%windows

if exist "%RESULTS_DIR%\%1" (
	EXIT /B 0
) else (
	EXIT /B 1
)

