#ifndef SHADERPROGRAM_H_INCLUDED
#define SHADERPROGRAM_H_INCLUDED

#include <OpenGLIncludes.h>

#include <vector>
#include <string>

class Shader;
class ShaderProgUniform;
class FileManager;

class ShaderProg
{
public:
    friend class ShaderProgUniform;

    ShaderProg(FileManager& fileManager,
               const std::string& shaderProgName,
               const std::string& vertexShaderFileName,
               const std::string& fragmentShaderFileName,
               unsigned int vertexAttributes,
               unsigned int requiredTextureTypes = 0,
               const std::string& vertexShaderDefines = "",
               const std::string& fragmentShaderDefines = "");
    ~ShaderProg();

   /**
    * Loads associated shaders, compiles them and then links the program
    */
    void LoadCompileAndLink();

   /**
    * Binds this shader program for use
    */
    void Use();

    inline const std::string& GetName() const
    {
        return m_ShaderProgName;
    }

    inline GLuint GetHandle() const
    {
        return m_ShaderProgHandle;
    }

protected:
    void RegisterUniform(ShaderProgUniform* uniform);

    bool inline IsCurrenlyInUse() const
    {
        return ( m_ShaderProgHandle == m_CurrentlyActiveShaderProgramHandle );
    }

private:
    // Used to prevent copying
    ShaderProg& operator = (const ShaderProg& other);
    ShaderProg(const ShaderProg& other);

   /**
    * Check if the shader program linked correctly
    */
    void _CheckShaderProgLinkStatus();

    void _AttachShader(Shader* shader);

   /**
    * Note: Shader program needs to be linked/relinked after binding vertex attributes
    */
    void _BindVertexAttributes();

    void _UpdateUniformIndices();

    std::string _ConstructExceptionMessage(const std::string& errorMsg);

    std::vector<Shader*> m_AttachedShaders; ///< All the shaders attached to the program
    std::vector<ShaderProgUniform*> m_Uniforms; ///< All of the uniforms registered with this shader program
    const std::string m_ShaderProgName;
    const std::string m_VertexShaderFileName;
    const std::string m_FragmentShaderFileName;
    const unsigned int m_VertexAttributes; ///< Vertex attributes used in this shader program
    const unsigned int m_RequiredTextureTypes; ///< Texture samplers used in this shader program
    GLuint m_ShaderProgHandle;
    bool m_ShaderProgIsLinked;
    static GLuint m_CurrentlyActiveShaderProgramHandle;
};

#endif // SHADERPROGRAM_H_INCLUDED
