#ifndef BULLETCLOSESTHITRAYRESULT_H_INCLUDED
#define BULLETCLOSESTHITRAYRESULT_H_INCLUDED

#include <bullet/btBulletDynamicsCommon.h>
#include "BulletCommon.h"

////////////////////////////////////////////////////////////////////
//       Class : BulletClosestHitRayResult
// Description :
////////////////////////////////////////////////////////////////////
struct BulletClosestHitRayResult : public btCollisionWorld::ClosestRayResultCallback
{
    inline static BulletClosestHitRayResult empty();

    btVector3 get_from_pos() const;
    btVector3 get_to_pos() const;

    bool has_hit() const;

    const btCollisionObject* get_node() const;
    btVector3 get_hit_pos() const;
    btVector3 get_hit_normal() const;
    float get_hit_fraction() const;

public:
    virtual bool needsCollision(btBroadphaseProxy* proxy0) const;
    BulletClosestHitRayResult(const btVector3 &from_pos, const btVector3 &to_pos, const CollideMask &mask);

private:
    CollideMask _mask;
};

#endif // BULLETCLOSESTHITRAYRESULT_H_INCLUDED
