
float gMatSpecularIntensity = 0.25f;
float gSpecularPower = 128.0f;//128.0f;

#if !defined(USING_OPENGL_ES)
    #define COLOR_PRECISION HIGHEST_PRECISION
#else
    #define COLOR_PRECISION LOWEST_PRECISION
#endif

vec4 CalcLightInternal(const in LOWEST_PRECISION float SpecularIntensity,
                       const in LOWEST_PRECISION float SpecularPower,
                       const in LOWEST_PRECISION float AmbientIntensity,
                       const in LOWEST_PRECISION float DiffuseIntensity,
                       const in COLOR_PRECISION vec3 LightColor,
                       const in LOWEST_PRECISION vec3 LightDirection_ViewSpace,
                       const in LOWEST_PRECISION vec3 VertexToEye,
                       const in LOWEST_PRECISION vec3 Normal_ViewSpace)
{
    COLOR_PRECISION vec4 AmbientColor = vec4(LightColor, 1.0f) * AmbientIntensity;

    // Normal points towards the light, while light points towards the normal. This means that when the
    // light is hitting the surface at 90 degrees the angle between the normal and the light is 180 degrees.
    // To prevent this we reverse(i.e. negate) the light direction to point towards the light. This way
    // the angle between the light hitting the surface at 90 dagrees and the surface normal is 0 degrees.
    LOWEST_PRECISION float DiffuseFactor = dot(Normal_ViewSpace, -LightDirection_ViewSpace);

    // When the light hits the surface at obtuce(90 - 180 degrees) angle i.e. from a side or from the back, the diffuse factor
    // will be negative or zero. In any of these cases we want the surface to have black diffuse component, hence we skip the
    // light calculation and leave diffuse and specular colors at zero value(i.e. black color) in such cases
    //if( DiffuseFactor > 0.0f ) // Got replaced with "max" function to avoid branching
    //{
        COLOR_PRECISION vec4 DiffuseColor = vec4(LightColor, 1.0f) * DiffuseIntensity * max(0.0f, DiffuseFactor);

        LOWEST_PRECISION vec3 LightReflect = normalize(reflect(LightDirection_ViewSpace, Normal_ViewSpace));
        LOWEST_PRECISION float SpecularFactor = dot(VertexToEye, LightReflect);
        SpecularFactor = max(0.0f, SpecularFactor);
        SpecularFactor = pow(SpecularFactor, SpecularPower);
        //if( SpecularFactor > 0.0f ) // Got replaced with "max" function to avoid branching
        //{
            COLOR_PRECISION vec4 SpecularColor = vec4(LightColor, 1.0f) * SpecularIntensity * SpecularFactor;
        //}
    //}

    return (AmbientColor + DiffuseColor + SpecularColor);
}

//float gAttenuationConstant = 0.0f;
//float gAttenuationLinear = 0.0f;
//float gAttenuationQuadratic = 1.0f;

#if defined(POINT_LIGHT) || defined(SPOT_LIGHT)
LOWEST_PRECISION float c_MinimumLightAttenuationValue = (1.0f / 25.0f); // Light attenuation value beyond which light is considered not to have any light

vec4 CalcPointLight(const in HIGHEST_PRECISION float LightRadiusInMetres,
                    const in LOWEST_PRECISION float SpecularIntensity,
                    const in LOWEST_PRECISION float SpecularPower,
                    const in LOWEST_PRECISION float LightAmbientIntensity,
                    const in LOWEST_PRECISION float LightDiffuseIntensity,
                    const in COLOR_PRECISION vec3 LightColor,
                    const in LOWEST_PRECISION vec3 LightDirection_ViewSpace,
                    const in HIGHEST_PRECISION float DistanceFromLightToVertex,
                    const in LOWEST_PRECISION vec3 VertexToEye,
                    const in LOWEST_PRECISION vec3 Normal_ViewSpace)
{
//     vec3 LightDirection_ViewSpace = VertexPos_ViewSpace - LightPosition_ViewSpace;
//     float Distance = length(LightDirection_ViewSpace);
//     LightDirection_ViewSpace = normalize(LightDirection_ViewSpace);

    COLOR_PRECISION vec4 Color = CalcLightInternal(SpecularIntensity,
                                                   SpecularPower,
                                                   LightAmbientIntensity,
                                                   LightDiffuseIntensity,
                                                   LightColor,
                                                   LightDirection_ViewSpace,
                                                   VertexToEye,
                                                   Normal_ViewSpace);

//    float Attenuation =  gAttenuationConstant +
//                          (gAttenuationLinear * DistanceFromLightToVertex) +
//                          (gAttenuationQuadratic * DistanceFromLightToVertex * DistanceFromLightToVertex);

    // Optimization of the two commented lines below
    LOWEST_PRECISION float factor = 256.0f / (LightRadiusInMetres * LightRadiusInMetres);
    LOWEST_PRECISION float Attenuation = 1.0f + (factor * DistanceFromLightToVertex * DistanceFromLightToVertex);
    //float quadraticAttenuationTermFromRadius = (1.0f / (LightRadiusInMetres * LightRadiusInMetres * c_MinimumLightAttenuationValue));
    //float Attenuation =  1.0f + (quadraticAttenuationTermFromRadius * DistanceFromLightToVertex * DistanceFromLightToVertex);

    return Color / Attenuation;
}
#endif

#if defined(SPOT_LIGHT)
vec4 CalcSpotLight(const in LOWEST_PRECISION float LightCutoff,
                   const in HIGHEST_PRECISION float LightRadiusInMetres,
                   const in LOWEST_PRECISION float SpecularIntensity,
                   const in LOWEST_PRECISION float SpecularPower,
                   const in LOWEST_PRECISION float LightAmbientIntensity,
                   const in LOWEST_PRECISION float LightDiffuseIntensity,
                   const in COLOR_PRECISION vec3 LightColor,
                   const in LOWEST_PRECISION vec3 SpotLightDirection_ViewSpace,
                   const in LOWEST_PRECISION vec3 LightDirection_ViewSpace,
                   const in HIGHEST_PRECISION float DistanceFromLightToVertex,
                   const in LOWEST_PRECISION vec3 VertexToEye,
                   const in LOWEST_PRECISION vec3 Normal_ViewSpace)
{
    //vec3 LightToPixelDir_ViewSpace = normalize(VertexPos_ViewSpace - LightPosition_ViewSpace);
    LOWEST_PRECISION float SpotFactor = dot(LightDirection_ViewSpace, SpotLightDirection_ViewSpace);

    if (SpotFactor > LightCutoff)
    {
        COLOR_PRECISION vec4 Color = CalcPointLight(LightRadiusInMetres,
                                                    SpecularIntensity,
                                                    SpecularPower,
                                                    LightAmbientIntensity,
                                                    LightDiffuseIntensity,
                                                    LightColor,
                                                    LightDirection_ViewSpace,
                                                    DistanceFromLightToVertex,
                                                    VertexToEye,
                                                    Normal_ViewSpace);

        return Color * (1.0f - (1.0f - SpotFactor) * 1.0f/(1.0f - LightCutoff));
    }
    else
    {
        return vec4(0.0f, 0.0f, 0.0f, 0.0f);
    }
}
#endif

#if defined(DIRECTIONAL_LIGHT)
vec4 CalcDirectionalLight(const in LOWEST_PRECISION float SpecularIntensity,
                          const in LOWEST_PRECISION float SpecularPower,
                          const in LOWEST_PRECISION float AmbientIntensity,
                          const in LOWEST_PRECISION float DiffuseIntensity,
                          const in COLOR_PRECISION vec3 LightColor,
                          const in LOWEST_PRECISION vec3 DirectionalLightDirection_ViewSpace,
                          const in LOWEST_PRECISION vec3 VertexToEye,
                          const in LOWEST_PRECISION vec3 Normal_ViewSpace)
{
    return CalcLightInternal(SpecularIntensity,
                             SpecularPower,
                             AmbientIntensity,
                             DiffuseIntensity,
                             LightColor,
                             DirectionalLightDirection_ViewSpace,
                             VertexToEye,
                             Normal_ViewSpace);
}
#endif
