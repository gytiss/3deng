#ifndef OPENALERRORCHECKING_H_INCLUDED
#define OPENALERRORCHECKING_H_INCLUDED

#include <string>

struct ALCdevice_struct;
typedef struct ALCdevice_struct ALCdevice;

namespace AudioDevice
{
    class Context;
    class Device;
}

#define CheckForALErrors(context) _CheckForALErrors_(context, __FILE__, __LINE__)
void _CheckForALErrors_(AudioDevice::Context& context, const std::string& fileName, int lineNum);

#define CheckForALCErrors(device) _CheckForALCErrors_(device, __FILE__, __LINE__)
void _CheckForALCErrors_(AudioDevice::Device& device, const std::string& fileName, int lineNum);

#endif // OPENALERRORCHECKING_H_INCLUDED
