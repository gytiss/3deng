#ifndef MAINAPPLOGIC_H_INCLUDED
#define MAINAPPLOGIC_H_INCLUDED

#include <map>
#include <string>
#include <glm/glm.hpp>
#include <SystemInterface.h>
#include <Renderer/RenderManager.h>
#include <Physics/PhysicsCharacterController.h>
#include <GameLogic/ShootingRangeScene.h>
#include <InputMapping/InputMapper.h>
#include <GUI/VirtualJoystick.h>
#include <GUI/FireButton.h>

#include "Camera.h"
#include "DrawableText.h"
#include "DrawableEntity.h"

class GraphicsDevice;
class PointLight;
class SpotLight;
class DirectionalLight;
class AnimationManager;
class LightManager;
class MeshManager;
class TextureManager;
class MaterialManager;
class PhysicsManager;
class AudioDataManager;
class SoundManager;
class GUIRenderInterface;

class AK47;

class MainAppLogic
{
public:
    MainAppLogic(GraphicsDevice& graphicsDevice,
                 FileManager& fileManager,
                 AnimationManager& animationManager,
                 MeshManager& meshManager,
                 TextureManager& textureManager,
                 MaterialManager& materialManager,
                 RenderManager& renderManager,
                 PhysicsManager& physicsManager,
                 AudioDataManager& audioDataManager,
                 SoundManager& soundManager,
                 SystemInterface& systemInterface);
    virtual ~MainAppLogic();

    static void InputCallback(InputMapping::MappedInput& inputs, float frameTimeInSeconds, void* engine)
    {
        reinterpret_cast<MainAppLogic*>(engine)->_InputCallback( inputs, frameTimeInSeconds );
    }

    void UpdateScene();
    void UpdateSceneAtFixedStep();
    void LastUpdateScene();
    void Render(float frameTimeInSeconds);

    inline void SetGUIRenderInterface(GUIRenderInterface* interface)
    {
        m_RenderManager.SetGUIReinderingInterface( interface );
    }

    inline void SetFreeLookEnabled(bool val)
    {
        m_FreeLookEnabled = val;
    }

    inline bool IsFreeLookEnabled() const
    {
        return m_FreeLookEnabled;
    }

    inline bool IsAppStillRunning() const
    {
        return m_AppIsStillRunning;
    }

    inline void SetAppIsStillRunning(bool val)
    {
        m_AppIsStillRunning = val;
    }

    inline void SetCurrentFrameTimeInMs(float frameTimeInMilliseconds)
    {
         m_CurrentFrameTimeInMs = frameTimeInMilliseconds;
    }

    inline float GetCurrentFrameTimeInMs() const
    {
        return m_CurrentFrameTimeInMs;
    }

private:
    enum KeyPressed
    {
        e_KeyPressedW = 0,
        e_KeyPressedS = 1,
        e_KeyPressedA = 2,
        e_KeyPressedD = 3,
        e_KeyPressedSpace = 4,
        e_KeyPressedLeftArrow = 5,
        e_KeyPressedRightArrow = 6,
        e_KeyPressedUpArrow = 7,
        e_KeyPressedDownArrow = 8,
        e_KeyPressedPageUp = 9,
        e_KeyPressedPageDown = 10,
        e_KeyPressedLastEnum // Used for counting
    };

    enum Finger
    {
        e_FingerOne,
        e_FingerTwo,

        e_FingerCount // Not a finger (Used for counting)
    };

    enum EntityGroup
    {
        e_EntityGroupPointLights = 0,
        e_EntityGroupSpotLights = 1,

        e_EntityGroupCount // Not a group used for counting
    };

    enum EntityManipType
    {
        e_EntityManipTypeMove = 0,
        e_EntityManipTypeRotate = 1
    };

    void _InputCallback(InputMapping::MappedInput& inputs, float frameTimeInSeconds);
    void _PCInput(InputMapping::MappedInput& inputs, float frameTimeInSeconds);
    void _TouchScreenInput(InputMapping::MappedInput& inputs, float frameTimeInSeconds);

    void _HandleInput(float frameTime);
    void _InitLights();
    void _BuildRenderList();
    void _RenderText();

    MainAppLogic& operator = (const MainAppLogic& other); // Prevent copying of MainAppLogic
    MainAppLogic(const MainAppLogic& other);              // Prevent copying of EditorLogic

    static void _DrawableSurfaceSizeChaged(const SystemInterface::DrawableSurfaceSize& newSize, void* userData);

    SystemInterface& m_SystemInterface;
    SystemInterface::DrawableSurfaceSize m_DrawableSurfaceSize;

    size_t m_CurrentLightNum;
    PointLight* m_PointLights[5];
    SpotLight* m_SpotLights[5];
    DirectionalLight* m_DirectionalLight;

    EntityGroup m_CurrentEntityGroup;
    EntityManipType m_EntityManipType;

    Camera* m_Camera;

    bool m_OtherEntitiesKeyVector[e_KeyPressedLastEnum];
    bool m_FingerReservedByJoystick[e_FingerCount];
    bool m_FingerReservedByFireButton[e_FingerCount];
    glm::vec2 m_RelativeMouseCoords;
    glm::vec2 m_AbsoluteMouseCoords;
    bool m_MouseMoved;
    bool m_RenderDebugData;
    bool m_KeysAreControlingEntities;
    bool m_FreeLookEnabled;
    bool m_AppIsStillRunning;
    bool m_ClickedWithMouse;
    bool m_Shooting;
    std::string m_LastPicked;
    float m_CurrentFrameTimeInMs;

    PointLight* m_CurrentPointLight;
    SpotLight* m_CurrentSpotLight;

    FileManager& m_FileManager;
    MeshManager& m_MeshManager;
    TextureManager& m_TextureManager;
    MaterialManager& m_MaterialManager;
    AnimationManager& m_AnimationManager;
    RenderManager& m_RenderManager;
    PhysicsManager& m_PhysicsManager;
    AudioDataManager& m_AudioDataManager;
    SoundManager& m_SoundManager;
    PhysicsCharacterController m_TestCharacterController;
    VirtualJoystick m_VirtualJoystick;
    FireButton m_FireButton;

    DrawableText m_DrawableText;
    ShootingRangeScene m_Scene;
    AK47* m_Weapon;
};

#endif // MAINAPPLOGIC_H_INCLUDED
