#include "SphereColliderComponentWidget.h"
#include <GUI/GUIDefaults.h>

//-----------------------------------------------------------------------

SphereColliderComponentWidget::SphereColliderComponentWidget(CategorizedListWidget* listWidgetToAddOurselvesTo,
                                                       QWidget* parent)
: ColliderWidgetBase<Ui::SphereColliderComponentUI, SphereCollider>( listWidgetToAddOurselvesTo, "Sphere Collider", parent )
{
    connect( m_UI->radiusLineEdit, SIGNAL( editingFinished() ),
             this, SLOT( RadiusLineEditEditingFinished() ) );

    m_UI->radiusLineEdit->setValidator( &m_FloatValueValidator );
}

//-----------------------------------------------------------------------

SphereColliderComponentWidget::~SphereColliderComponentWidget()
{

}

//-----------------------------------------------------------------------

void SphereColliderComponentWidget::SetSphereColliderComponent(SphereCollider* collider)
{
    _SetColliderComponent( collider );

    if( collider )
    {
        GUIHelpers::SetNumericLineEditValue( m_UI->radiusLineEdit, collider->GetRadius() );
    }
}

//-----------------------------------------------------------------------

void SphereColliderComponentWidget::ResetPaletteToDefault()
{
    m_UI->sphereColliderComponentContentWidget->setPalette( GUIDefaults::GetSingleton().DefaultPalette() );
}

//-----------------------------------------------------------------------

void SphereColliderComponentWidget::RadiusLineEditEditingFinished()
{
    if( m_ColliderComponent )
    {
        m_ColliderComponent->SetRadius( GUIHelpers::GetNumericLineEditValue( m_UI->radiusLineEdit ) );
    }
}

//-----------------------------------------------------------------------
