#ifndef CAMERA_H_INCLUDED
#define CAMERA_H_INCLUDED

#include <Units.h>
#include <MathUtils.h>
#include <glm/glm.hpp>
#include <cassert>
#include <SystemInterface.h>
#include <Physics/PhysicsMotionStateInterface.h>

//
//    OpenGL Coordinate System
//
//               +Y
//                |
//                |  / -Z
//                | /
//                |/
//     -X --------/-------- +X
//               /|
//              / |
//          +Z /  |
//                | -Y
//

/**
 * Default direction of this camera is towards -Z axis
 */
class Camera : public PhysicsMotionStateInterface
{
public:
   /**
    * @param speedInMeterPerSecond - Speed at which camera position will change
    */
    Camera(const SystemInterface::DrawableSurfaceSize& screenDimensions,
           const float speedInMetersPerSecond);

   /**
    * Default speed is one meter per second
    */
    Camera(const SystemInterface::DrawableSurfaceSize& screenDimensions);

   /**
    * Sets camera speed
    *
    * @param metersPerSecond - camera speeed
    */
    void SetSpeed(float metersPerSecond)
    {
        assert( ( metersPerSecond >= 0.0f ) && "Speed must not be negative" );

        m_SpeedMetersPerSecond = metersPerSecond;
    }

   /**
    * Sets camera speed multiplier
    *
    * By default the multiplier is 1.0
    *
    * @param multiplier - Speed multiplier in the range of [0, n]
    */
    void SetSpeedMultiplier(float multiplier)
    {
        assert( ( multiplier >= 0.0f ) && "Speed multiplier must not be negative" );

        m_SpeedMultiplier = multiplier;
    }

    inline const glm::vec3& GetPosition() const
    {
        return m_Position;
    }

    inline const glm::vec3& GetDirectionVector() const
    {
        return m_DirectionVector;
    }

    inline const glm::vec3& GetUpVector() const
    {
        return m_UpVector;
    }

    /**
     * @returns A value in degrees from 1 to 80
     */
    inline float GetFieldOfView() const
    {
        return m_FieldOfView;
    }

    /**
     * @param fov - A values in degrees from 1 to 80
     */
    inline void SetFieldOfView( const float fov )
    {
        m_FieldOfView = glm::clamp( fov, 1.0f, 80.0f );
    }

   /**
    * Returns View matrix represented by the camera
    */
    glm::mat4 GetViewMatrix() const;

    /**
     * Returns Projection matrix used by the camera
     */
    inline glm::mat4 GetProjectionMatrix() const
    {
        return m_CachedProjectionMatrix;
    }

   /**
    * Set position of the camera.
    * @param pos - New camera position in world space
    */
    void ForcePosition(const glm::vec3& pos);

   /**
    * Set position of the camera.
    * @param x - Position component in world space
    * @param y - Position component in world space
    * @param z - Position component in world space
    */
    void ForcePosition(const float x, const float y, const float z);

   /**
    * @param horizAngleInDegrees - If negative camera turns right
    *                             If positive camera turns left
    * @param vertAngleInDegrees - If negative, camera turns down
    *                            If positive, camera turns up
    *                            Allowed values are (-90.0 to 90.0)
    */
    void ForceOrientation(const float horizAngleInDegrees,
                          const float vertAngleInDegrees);

   /**
    * Move camera using 2D coordinates from ranges input device(e.g. Mouse).
    * Note: x and y values must be in the range of between 0.0 and 1.0
    */
    void RecalculateOrientation(const float frameTimeInSeconds,
                                const float x,
                                const float y,
                                const float sensitivity);

    void RecalculatePosition(const float frameTimeInSeconds,
                             const bool moveFront,
                             const bool moveBack,
                             const bool moveLeft,
                             const bool moveRight );


    /**
     * Move camera a given number of meter either along cameras
     * viewing direction or to either side or both
     *
     * The result of this function call will be a new camera position
     * which can be retrieved with a GetPosition() function call
     *
     * @param metersToMoveAlongViewDirection - Negative or positive value moves camera a given
     *                                         number of units from current position to the
     *                                         front or back respectively
     * @oaram metersToMoveToEitherSides - Negative or positive value moves camera a given number
     *                                    of units from current position to the left or right
     *                                    respectively
     */
    void MoveCameraAFixedNumberOfUnits(const float metersToMoveAlongViewDirection,
                                       const float metersToMoveToEitherSides);

    virtual void SetSceneTransform(const SceneTypes::Transform& t) override
    {
        m_Position = t.position;
    }

    virtual void GetSceneTransform(SceneTypes::Transform& t) const override
    {
        t.position = m_Position;
        t.rotation = MathUtils::c_IdentityQuaternion;
    }

    inline float GetPitchInDegrees() const
    {
        return MathUtils::RadiansToDegrees( m_VerticalAngleInRadians );
    }

    inline float GetYawInDegrees() const
    {
        return MathUtils::RadiansToDegrees( m_HorizontalAngleInRadians );
    }

    inline void SetScreenDimensions(const SystemInterface::DrawableSurfaceSize& screenDimensions)
    {
        m_ScreenDimensions = screenDimensions;
        _RecalculateProjectionMatrix();
    }

private:
    void _RecalculateVectorData( const float horizAngleInRadians,
                                 float vertAngleInInRadians );
    void _RecalculateProjectionMatrix();

    static const float c_HorizontalAngleOffsetInRadians; ///< Used to make camera point towards the -Z axis by default instead of +Z
    static const float c_VerticalAngleLimitInRadians;    ///< Needed to prevent roll over of the camera
    glm::vec3 m_Position;
    glm::vec3 m_DirectionVector;
    glm::vec3 m_UpVector;
    glm::vec3 m_RightVector;
    float m_FieldOfView;
    float m_HorizontalAngleInRadians;
    float m_VerticalAngleInRadians;
    float m_SpeedMetersPerSecond;
    float m_SpeedMultiplier;

    SystemInterface::DrawableSurfaceSize m_ScreenDimensions;
    glm::mat4 m_CachedProjectionMatrix;
};

#endif // CAMERA_H_INCLUDED
