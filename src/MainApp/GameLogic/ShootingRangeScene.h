#ifndef SHOOTINGRANGESCENE_H_INCLUDED
#define SHOOTINGRANGESCENE_H_INCLUDED

#include <Scene/Scene.h>

class SystemInterface;
class PhysicsManager;
class MeshManager;
class AnimationManager;
class MaterialManager;
class AudioDataManager;
class SoundManager;
class Camera;

class ShootingRangeScene : public Scene
{
public:
    ShootingRangeScene(Camera& camera,
                       SystemInterface& systemInterface,
                       PhysicsManager& physicsManager,
                       MeshManager& meshManager,
                       AnimationManager& animationManager,
                       MaterialManager& materialManager,
                       AudioDataManager& audioDataManager,
                       SoundManager& soundManager);

private:

};

#endif // SHOOTINGRANGESCENE_H_INCLUDED
