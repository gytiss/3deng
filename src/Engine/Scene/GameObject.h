#ifndef GAMEOBJECTS_H_INCLUDED
#define GAMEOBJECTS_H_INCLUDED

#include <type_traits>
#include <vector>
#include <cstdint>
#include <MathUtils.h>
#include <CommonDefinitions.h>
#include <Exceptions/GenericException.h>
#include "Component.h"
#include "CustomComponent.h"
#include "RigidBody.h"
#include "Collider.h"
#include "Renderer.h"

class StaticInfinitePlaneCollider;
class CapsuleCollider;
class ConeCollider;
class BoxCollider;
class SphereCollider;
class MeshCollider;
class MeshRenderer;
class SkinnedMeshRenderer;

class GameObject
{
public:
    friend class RigidBody;
    friend class Collider;
    friend class Transform;
    friend class Scene;

    class GameObjectLogicInterface
    {
    public:
        friend class GameObject;
    protected:
        virtual void _Initialize() = 0;
        virtual void _FixedUpdate() = 0;
        virtual void _Update() = 0;
        virtual void _LastUpdate() = 0;
    };

//    typedef std::vector<SceneGraphNodeCustomProperty*> CustomPropertyList;
    typedef std::vector<GameObject*> ChildNodeList;

    /**
     * @param name - Name of the game object
     * @param id - An ID used for picking functionality (Can be any 32bit number). Default value is zero.
     */
    GameObject(const std::string& name, uint32_t id = 0);
    virtual ~GameObject();

    inline uint32_t GetID() const
    {
        return m_ID;
    }

    inline const std::string& GetName() const
    {
        return m_Name;
    }

    inline GameObject* GetParent()
    {
        return m_Parent;
    }

    inline ChildNodeList& GetChildren()
    {
        return m_Children;
    }

    const EngineTypes::AABB& GetAABB() const;

    void AddChild(GameObject* childNode);

    /**
     * Sets internal relative transform from a given global transform "t" and a parent transform.
     * If this node has no parent a given global transform "t" is considered to be a transform
     * relative to the origin
     */
    virtual void SetGlobalTransform(const SceneTypes::Transform& t);

    /**
     * Sets a transform relative to its parent node or to the
     * origin(i.e. 0, 0, 0) if it has no parent node
     */
    virtual void SetTransform(const SceneTypes::Transform& t);

    /**
     * Get transform relative to the parent of a global tranform if there is no parent
     */
    const SceneTypes::Transform& GetTransform() const;

    /**
     * Get transformation matrix representing global position
     * orientation and scale of this scene graph node
     *
     * @return 4x4 transformation matrix
     */
    const glm::mat4& GetGlobalTransformMatrix() const;

//
//    inline CustomPropertyList& GetCustomProperties()
//    {
//        return m_CustomProperties;
//    }

//    void AddCustomProperty(SceneGraphNodeCustomProperty* property);

    template<class ComponentType>
    inline void RemoveComponent()
    {
        Component* component = m_Components[ComponentType::m_ComponentType];
        m_Components[ComponentType::m_ComponentType] = nullptr;

        if( component )
        {
            delete component;
        }
    }

    template<class ComponentType>
    inline void SetComponent(ComponentType* component)
    {
        assert( ( ComponentType::m_ComponentType < Component::e_TypeCount ) && "Invalid component type" );

        if( component != GetComponent<ComponentType>() ) // Prevent a situation where the same instance of a component is set multiple times
        {
            RemoveComponent<ComponentType>();

            m_Components[ComponentType::m_ComponentType] = component;
        }
    }

    template<class ComponentType>
    inline const ComponentType* GetComponent() const
    {
        assert( ( ComponentType::m_ComponentType < Component::e_TypeCount ) && "Invalid component type" );

        Component* component = m_Components[ComponentType::m_ComponentType];

        assert( ( !component || ( component && ( component->GetType() == ComponentType::m_ComponentType ) ) ) && "Component in a wrong slot" );

        if( component )
        {
            if( component->GetType() == Component::e_TypeCollider )
            {
                Collider* collider = static_cast<Collider*>( component );

                if( !( ( ( collider->GetColliderType() == Collider::e_ColliderTypeStaticInfinitePlane ) && std::is_same<ComponentType, StaticInfinitePlaneCollider>::value ) ||
                       ( ( collider->GetColliderType() == Collider::e_ColliderTypeCapsule ) && std::is_same<ComponentType, CapsuleCollider>::value ) ||
                       ( ( collider->GetColliderType() == Collider::e_ColliderTypeCone ) && std::is_same<ComponentType, ConeCollider>::value ) ||
                       ( ( collider->GetColliderType() == Collider::e_ColliderTypeBox ) && std::is_same<ComponentType, BoxCollider>::value ) ||
                       ( ( collider->GetColliderType() == Collider::e_ColliderTypeSphere ) && std::is_same<ComponentType, SphereCollider>::value ) ||
                       ( ( collider->GetColliderType() == Collider::e_ColliderTypeMesh ) && std::is_same<ComponentType, MeshCollider>::value ) ) )
                {
                    if( !std::is_same<ComponentType, Collider>::value ) // Make sure it is possible to request generic Collider objects
                    {
                        component = nullptr;
                    }
                }
            }
            else if( component->GetType() == Component::e_TypeRenderer )
            {
                Renderer* renderer = static_cast<Renderer*>( component );

                if( !( ( std::is_same<ComponentType, Renderer>::value ) ||
                       ( ( renderer->GetRendererType() == Renderer::e_RendererTypeMesh ) && std::is_same<ComponentType, MeshRenderer>::value ) ||
                       ( ( renderer->GetRendererType() == Renderer::e_RendererTypeSkinnedMesh ) && std::is_same<ComponentType, SkinnedMeshRenderer>::value ) ) )
                {
                    component = nullptr;
                }
            }
        }

        return ( component ? static_cast<ComponentType*>( component ) : nullptr );
    }

    template<class ComponentType>
    inline ComponentType* GetComponent()
    {
        // All this casting is needed to allow reuse of the GetComponent() function defined above
        return const_cast<ComponentType*>( static_cast<const GameObject*>( this )->GetComponent<ComponentType>() );
    }

    typedef std::vector<RenderableObject> RenderablesList;

    void GetRenderablesOfTheHierarchy(RenderablesList& renderables);

protected:
    void _Initialize();
    void _FixedUpdate();
    void _Update();
    void _LastUpdate();

    /**
     * @param logic - Logic to add to the GameObject.
     *
     * Note: GameObject does not take ownership of this pointer,
     *       hence it is responsibility of the caller to destroy
     *       the "logic" object when it is no longer needed,
     *       but not before this GameObject has been destroyed.
     */
    void _AddLogic(GameObjectLogicInterface* logic);

    void _GetRenderablesOfTheHierarchy(RenderablesList& renderables, GameObject* gameObject);
    void _InvalidateTransformMatrixCache();
    void _UpdateTransformOfThePhysicsComponents();
    void _UpdateTransformOfTheSoundSourceComponent();
    void _SetGlobalTransformWithoutUptadingPhysicsPropertyTransform(const SceneTypes::Transform& t);

    /**
     * Finds a RigidBody in the GameObject hierarchy.
     *
     * This function searches current GameObject and all of its parent
     * GameObjects and returns the first RigidBody component found.
     *
     * Note: Child GameObjects are not searched
     */
    RigidBody* _FindRigidBody();

    /**
     * Finds a Colliders in the GameObject hierarchy.
     *
     * This function searches current GameObject and all of its children
     * GameObjects and populates a  given list with all of the Collider
     * components found.
     *
     * Note: Parent GameObjects are not searched
     */
    void _FindColliders(std::vector<Collider*>& outColliders);

private:
    static const EngineTypes::AABB c_DefaultUnitSizeBoundingBox;

    uint32_t m_ID; /// Can be set to any number, default is zero (Used for picking functionality)
    std::string m_Name;
    GameObject* m_Parent;
    ChildNodeList m_Children;
    mutable glm::mat4 m_GlobalTransformMatrix;
    mutable bool m_TransformMatrixCacheIsValid;
    bool m_NoPhysicsUpdate;

    Component* m_Components[Component::e_TypeCount];
//    CustomPropertyList m_CustomProperties;
    std::vector<GameObjectLogicInterface*> m_Logics;
};

#endif // GAMEOBJECTS_H_INCLUDED
