#include "SpotLightMarker.h"
#include <glm/glm.hpp>
#include <MathUtils.h>
#include <CommonDefinitions.h>
#include <GraphicsDevice/VBO.h>
#include <Scene/MeshRenderer.h>
#include <Scene/ConeCollider.h>

//-----------------------------------------------------------------------

SpotLightMarker::SpotLightMarker(PhysicsManager& physicsManager,
                                 MaterialManager& materialManager,
                                 TextureManager& textureManager)
: EntityMarker( physicsManager, materialManager, textureManager ),
  c_UnitRadius( 1.0f ),
  m_VertexBuffer(),
  m_CircleVertexCount( 0 ),
  m_LinesVertexCount( 0 ),
  m_PointsVertexCount( 0 ),
  m_CutOffAngleInDegrees( 20.0f ),
  m_Radius( c_UnitRadius ),
  m_ScaledUnitConeHeight( 1.0f ),
  m_DirectionInWorldSpace( 0.0f, -1.0f, 0.0f ),
  m_UnitConeScaleFactor( 1.0f ),
  m_UnitConeRotation()
{
    new ConeCollider( *this, m_Radius, m_ScaledUnitConeHeight, m_PhysicsManager );

    VBO* vbo = new VBO( sizeof( EntityMarkerVertex ) );
    vbo->AddVertexAttribute(VBO::e_VertexAttrIdxPosition,
                            VBO::e_VertexAttrComponentTypeFloat32,
                            VBO::e_VertexAttrTypeVec3,
                            offsetof( EntityMarkerVertex, position ) );

    _SetVertexData( vbo );

    m_VAO.AddVBO( vbo );

    _CalculateUnitConeScaleFactorAndHeight();

    _RecalculateRenderingData();
}

//-----------------------------------------------------------------------

void SpotLightMarker::RecalculateRenderables()
{
    MeshRenderer* renderer = GetComponent<MeshRenderer>();

    // TODO Create line renderer and render the below primitives via it

    RenderableObject lineLoop = RenderableObject( RenderableObject::e_RenderingTypeDirect,
                                                  true,
                                                  0,
                                                  static_cast<unsigned int>( m_CircleVertexCount ),
                                                  0,
                                                  0,
                                                  GraphicsDevice::e_DrawPrimitivesTypeLineLoop,
                                                  &m_VAO,
                                                  &m_MaterialHandle);

    RenderableObject lines = RenderableObject( RenderableObject::e_RenderingTypeDirect,
                                               true,
                                               static_cast<unsigned int>( m_CircleVertexCount ),
                                               static_cast<unsigned int>( m_LinesVertexCount ),
                                               0,
                                               0,
                                               GraphicsDevice::e_DrawPrimitivesTypeLines,
                                               &m_VAO,
                                               &m_MaterialHandle );

    RenderableObject points = RenderableObject( RenderableObject::e_RenderingTypeDirect,
                                                true,
                                                static_cast<unsigned int>( m_CircleVertexCount + m_LinesVertexCount ),
                                                static_cast<unsigned int>( m_PointsVertexCount ),
                                                0,
                                                0,
                                                GraphicsDevice::e_DrawPrimitivesTypePoints,
                                                &m_VAO,
                                                &m_MaterialHandle );
    renderer->ClearRenderables();
    renderer->AddRenderable( lineLoop );
    renderer->AddRenderable( lines );
    renderer->AddRenderable( points  );
}

//-----------------------------------------------------------------------

void SpotLightMarker::SetCutOffAngleInDegrees(float angle)
{
    if( angle != m_CutOffAngleInDegrees )
    {
        m_CutOffAngleInDegrees = angle;
        _CalculateUnitConeScaleFactorAndHeight();
    }
}

//-----------------------------------------------------------------------

void SpotLightMarker::SetRadius(float radius)
{
    if( radius != m_Radius )
    {
        m_Radius = radius;
        _CalculateUnitConeScaleFactorAndHeight();
    }
}

//-----------------------------------------------------------------------

void SpotLightMarker::SetDirectionInWorldSpace(const glm::vec3 dir)
{
    if( dir != m_DirectionInWorldSpace )
    {
        m_DirectionInWorldSpace = dir;
        m_UnitConeRotation = MathUtils::GetVectorRotationTo( glm::vec3( 0.0f, -1.0f, 0.0f ),
                                                             m_DirectionInWorldSpace );

        SceneTypes::Transform t;
        if( GetParent() )
        {
            t.position = _GetUnitConeTranslationFactor( MathUtils::MatrixToTranslation( GetParent()->GetGlobalTransformMatrix() ) );
        }
        t.rotation = m_UnitConeRotation;
        t.scale = m_UnitConeScaleFactor;

        SetGlobalTransform( t );
    }
}

//-----------------------------------------------------------------------

void SpotLightMarker::_CalculateUnitConeScaleFactorAndHeight()
{
    m_UnitConeScaleFactor = MathUtils::SpotLightUnitConeScaleFactors( m_CutOffAngleInDegrees,
                                                                      m_Radius );
    m_ScaledUnitConeHeight = MathUtils::SpotLightUnitConeHeight( m_CutOffAngleInDegrees, m_Radius );

    SceneTypes::Transform t;
    if( GetParent() )
    {
        t.position = _GetUnitConeTranslationFactor( MathUtils::MatrixToTranslation( GetParent()->GetGlobalTransformMatrix() ) );
    }
    t.rotation = m_UnitConeRotation;
    t.scale = m_UnitConeScaleFactor;

    SetGlobalTransform( t );

    ConeCollider* collider = GetComponent<ConeCollider>();
    collider->SetRadius( m_Radius );
    collider->SetHeight( m_ScaledUnitConeHeight );
}

//-----------------------------------------------------------------------

void SpotLightMarker::_SetVertexData(VBO* vbo)
{
    if( vbo )
    {
        m_VertexBuffer.clear();
        m_CircleVertexCount = _GenerateCircleVertices( e_CircleTypeHorizontal,
                                                       0.0f,
                                                       -c_UnitRadius,
                                                       c_UnitRadius,
                                                       _GetNumCircleSegments( c_UnitRadius ),
                                                       m_VertexBuffer );

        m_LinesVertexCount = _GenerateLineVertices();

        m_PointsVertexCount = _GeneratePointVertices();
        vbo->BindData( m_VertexBuffer.size() * sizeof( m_VertexBuffer[0] ), &(m_VertexBuffer[0]) );
    }
}

//-----------------------------------------------------------------------

size_t SpotLightMarker::_GenerateLineVertices()
{
    const size_t c_InitialVertexCount = m_VertexBuffer.size();

    static const EntityMarkerVertex v1 = { { 0.0f, c_UnitRadius, 0.0f } };
    static const EntityMarkerVertex v2 = { { -c_UnitRadius, -c_UnitRadius, 0.0f } };
    static const EntityMarkerVertex v3 = { { 0.0f, c_UnitRadius, 0.0f } };
    static const EntityMarkerVertex v4 = { { c_UnitRadius, -c_UnitRadius, 0.0f } };
    static const EntityMarkerVertex v5 = { { 0.0f, c_UnitRadius, 0.0f } };
    static const EntityMarkerVertex v6 = { { 0.0f, -c_UnitRadius, c_UnitRadius } };
    static const EntityMarkerVertex v7 = { { 0.0f, c_UnitRadius, 0.0f } };
    static const EntityMarkerVertex v8 = { { 0.0f, -c_UnitRadius, -c_UnitRadius } };

    m_VertexBuffer.push_back( v1 );
    m_VertexBuffer.push_back( v2 );
    m_VertexBuffer.push_back( v3 );
    m_VertexBuffer.push_back( v4 );
    m_VertexBuffer.push_back( v5 );
    m_VertexBuffer.push_back( v6 );
    m_VertexBuffer.push_back( v7 );
    m_VertexBuffer.push_back( v8 );

    return ( m_VertexBuffer.size() - c_InitialVertexCount );
}

//-----------------------------------------------------------------------

size_t SpotLightMarker::_GeneratePointVertices()
{
    const size_t c_InitialVertexCount = m_VertexBuffer.size();

    static const EntityMarkerVertex v1 = { { -c_UnitRadius, -c_UnitRadius, 0.0f } };
    static const EntityMarkerVertex v2 = { { c_UnitRadius, -c_UnitRadius, 0.0f } };
    static const EntityMarkerVertex v3 = { { 0.0f, -c_UnitRadius, -c_UnitRadius } };
    static const EntityMarkerVertex v4 = { { 0.0f, -c_UnitRadius, c_UnitRadius } };
    static const EntityMarkerVertex v5 = { { 0.0f, c_UnitRadius, 0.0f } };
    m_VertexBuffer.push_back( v1 );
    m_VertexBuffer.push_back( v2 );
    m_VertexBuffer.push_back( v3 );
    m_VertexBuffer.push_back( v4 );
    m_VertexBuffer.push_back( v5 );

    return ( m_VertexBuffer.size() - c_InitialVertexCount );
}

//-----------------------------------------------------------------------
