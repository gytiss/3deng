#ifndef DIRECTIONALLIGHT_H_INCLUDED
#define DIRECTIONALLIGHT_H_INCLUDED

#include "RenderingHelperMeshes.h"

class ShaderProgDirectionalLightUberShader;

class DirectionalLight
{
public:
    DirectionalLight(ShaderProgDirectionalLightUberShader& shaderProg);

    inline void SetColor(const glm::vec3& color)
    {
        m_Color = color;
    }

    inline const glm::vec3& GetColor() const
    {
        return m_Color;
    }

    inline void SetAmbientIntensity(float intensity)
    {
        m_AmbientIntensity = intensity;
    }

    inline float GetAmbientIntensity() const
    {
        return m_AmbientIntensity;
    }

    inline void SetDiffuseIntensity(float intensity)
    {
        m_DiffuseIntensity = intensity;
    }

    inline float GetDiffuseIntensity() const
    {
        return m_DiffuseIntensity;
    }

    inline void SetDirectionInWorldSpace(const glm::vec3& dir)
    {
        m_DirectionInWorldSpace = glm::normalize( dir ); // Light direction must be a normal vector
    }

    inline const glm::vec3& GetDirectionInWorldSpace() const
    {
        return m_DirectionInWorldSpace;
    }

    inline bool Enabled() const
    {
        return m_Enabled;
    }

    inline void SetEnabled(bool val)
    {
        m_Enabled = val;
    }

    void RenderForLightPass(const glm::mat4& viewMatrix,
                            const glm::mat4& projectionMatrix);

private:
    bool m_Enabled;
    glm::vec3 m_Color;
    float m_AmbientIntensity;
    float m_DiffuseIntensity;
    glm::vec3 m_DirectionInWorldSpace;
    DirectionalLightInfluenceMesh m_InfluenceMesh;
    ShaderProgDirectionalLightUberShader& m_ShaderProg;
};

#endif // DIRECTIONALLIGHT_H_INCLUDED
