#include <Resources/MeshManager.h>
#include <Resources/MaterialManager.h>
#include <Resources/TextureManager.h>
#include <Scene/Transform.h>
#include <Scene/MeshRenderer.h>
#include <Scene/MeshFilter.h>
#include <EditorObjectIDs.h>
#include "MoveManipulator.h"

//-----------------------------------------------------------------------

MoveManipulator::MoveManipulator(MeshManager& meshManager,
                   MaterialManager& materialManager,
                   TextureManager& textureManager)
: GameObject( "MoveManipulator", EditorObjectID::MoveManipulator ),
  m_MeshManager( meshManager ),
  m_MaterialManager( materialManager ),
  m_TextureManager( textureManager ),
  m_XAxis( nullptr ),
  m_YAxis( nullptr ),
  m_ZAxis( nullptr )
{
    m_XAxis = _CreateAxis( "MoveManipulatorXAxis", EditorObjectID::MoveManipulatorXAxis, EngineTypes::SRGBColor( 1.0f, 0.0f, 0.0f ) );
    m_YAxis = _CreateAxis( "MoveManipulatorYAxis", EditorObjectID::MoveManipulatorYAxis, EngineTypes::SRGBColor( 0.0f, 1.0f, 0.0f ) );
    m_ZAxis = _CreateAxis( "MoveManipulatorZAxis", EditorObjectID::MoveManipulatorZAxis, EngineTypes::SRGBColor( 0.0f, 0.0f, 1.0 ) );

    Transform* xAxisTransform = m_XAxis->GetComponent<Transform>();
    Transform* yAxisTransform = m_YAxis->GetComponent<Transform>();
    Transform* zAxisTransform = m_ZAxis->GetComponent<Transform>();

    xAxisTransform->SetRotation( glm::vec3( 0.0f, 0.0f, 270.0f ) );
    yAxisTransform->SetRotation( glm::vec3( 0.0f, 0.0f, 0.0f ) );
    zAxisTransform->SetRotation( glm::vec3( 90.0f, 0.0f, 0.0f ) );

    AddChild( m_XAxis );
    AddChild( m_YAxis );
    AddChild( m_ZAxis );

    GetComponent<Transform>()->SetScale( glm::vec3( 0.05f ) );
}

//-----------------------------------------------------------------------

MoveManipulator::~MoveManipulator()
{
    // These pointers belong to the GameObject, hence we do not need to free them
    m_XAxis = nullptr;
    m_YAxis = nullptr;
    m_ZAxis = nullptr;
}

//-----------------------------------------------------------------------

const GameObject* MoveManipulator::GetXAxis() const
{
    return m_XAxis;
}

//-----------------------------------------------------------------------

const GameObject* MoveManipulator::GetYAxis() const
{
    return m_YAxis;
}

//-----------------------------------------------------------------------

const GameObject* MoveManipulator::GetZAxis() const
{
    return m_ZAxis;
}

//-----------------------------------------------------------------------

GameObject* MoveManipulator::_CreateAxis(const std::string& name,
                                         uint32_t id,
                                         const EngineTypes::SRGBColor& srgbColor)
{
    MaterialDescriptor axisMaterialDescriptor( m_TextureManager );
    axisMaterialDescriptor.SetDiffuseColor( srgbColor );
    MaterialManager::MaterialHandle axisConeMaterial = m_MaterialManager.AddInternalMaterial( axisMaterialDescriptor );
    MaterialManager::MaterialHandle axisShaftMaterial = m_MaterialManager.DuplicateMaterialHandle( axisConeMaterial );

    GameObject* axis = new GameObject( name, id );
    GameObject* axisCone = new GameObject( name + "Cone", id );
    GameObject* axisShaft = new GameObject( name + "Shaft", id );

    static const float c_Shaft2xHeight = 4.0f;
    static const float c_ShaftHalfWidth = 0.0625f;
    static const float c_ShaftQuaterWidth = ( c_ShaftHalfWidth / 2.0f );
    static const float c_ConeRadius = 0.5f;

    axisCone->GetComponent<Transform>()->SetPosition( glm::vec3( 0.0f, 5.0f - c_ShaftQuaterWidth, 0.0f ) );
    axisCone->GetComponent<Transform>()->SetScale( glm::vec3( c_ConeRadius, 1.0f, c_ConeRadius ) );
    axisShaft->GetComponent<Transform>()->SetPosition( glm::vec3( 0.0f, 2.0f - c_ShaftQuaterWidth, 0.0f ) );
    axisShaft->GetComponent<Transform>()->SetScale( glm::vec3( c_ShaftHalfWidth, ( c_Shaft2xHeight / 2.0f ) + c_ShaftQuaterWidth, c_ShaftHalfWidth ) );

    MeshRenderer* axisConeRenderer = new MeshRenderer( *axisCone, m_MaterialManager );
    MeshFilter* axisConeMeshFilter = new MeshFilter( *axisCone, m_MeshManager );
    MeshManager::MeshHandle axisConeMeshHandle = m_MeshManager.GetInternalMeshHandle( MeshManager::e_InternalMeshTypeCone );
    axisConeMeshFilter->SetMesh( axisConeMeshHandle );
    axisConeRenderer->SetSubMeshMaterial( 0, axisConeMaterial );
    axisConeRenderer->SetDepthTestingEnabled( false );

    MeshRenderer* axisShaftRenderer = new MeshRenderer( *axisShaft, m_MaterialManager );
    MeshFilter* axisShaftMeshFilter = new MeshFilter( *axisShaft, m_MeshManager );
    MeshManager::MeshHandle axisShaftMeshHandle = m_MeshManager.GetInternalMeshHandle( MeshManager::e_InternalMeshTypeCylinder );
    axisShaftMeshFilter->SetMesh( axisShaftMeshHandle );
    axisShaftRenderer->SetSubMeshMaterial( 0, axisShaftMaterial );
    axisShaftRenderer->SetDepthTestingEnabled( false );

    axis->AddChild( axisCone );
    axis->AddChild( axisShaft );

    return axis;
}
