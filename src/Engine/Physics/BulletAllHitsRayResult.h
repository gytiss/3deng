#ifndef BULLETALLHITSRAYRESULT_H_INCLUDED
#define BULLETALLHITSRAYRESULT_H_INCLUDED

#include <bullet/btBulletDynamicsCommon.h>

#define nassertr(condition, return_value) \
  { \
    if (!(condition)) { \
        return return_value; \
    } \
  }

#include <vector>

#include "BulletCommon.h"

struct BulletRayHit
{
    inline static BulletRayHit empty();

    const btCollisionObject* get_node() const;
    btVector3 get_hit_pos() const;
    btVector3 get_hit_normal() const;
    float get_hit_fraction() const;

private:
    const btCollisionObject* _object;
    btVector3 _normal;
    btVector3 _pos;
    btScalar _fraction;

    friend struct BulletAllHitsRayResult;
};

struct BulletAllHitsRayResult : public btCollisionWorld::AllHitsRayResultCallback
{
    inline static BulletAllHitsRayResult empty();

    btVector3 get_from_pos() const;
    btVector3 get_to_pos() const;

    bool has_hits() const;
    float get_closest_hit_fraction() const;

    size_t get_num_hits() const;
    const BulletRayHit get_hit(size_t idx) const;

    void get_hits(std::vector<BulletRayHit>& outHits)
    {
        for(size_t i = 0; i < get_num_hits(); i++)
        {
            outHits.push_back( get_hit( i ) );
        }
    }

public:
    virtual bool needsCollision(btBroadphaseProxy* proxy0) const;


    BulletAllHitsRayResult(const btVector3 &from_pos, const btVector3 &to_pos, const CollideMask &mask);

private:
    CollideMask _mask;
};

#endif // BULLETALLHITSRAYRESULT_H_INCLUDED
