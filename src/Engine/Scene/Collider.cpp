#include "Collider.h"
#include "RigidBody.h"
#include "GameObject.h"
#include <MathUtils.h>

//-----------------------------------------------------------------------

Collider::Collider(GameObject& owningGameObject,
                   ColliderType type,
                   const std::string& name,
                   PhysicsManager& physicsManager)
: Component( m_ComponentType, name, owningGameObject ),
  m_PhysicsManager( physicsManager ),
  m_Initialised( false ),
  m_ColliderType( type ),
  m_CollisionShapeHandle(),
  m_ColliderHandle()
{
    m_ColliderHandle = m_PhysicsManager.CreateCollider();
    m_PhysicsManager.SetColliderUserPointer( m_ColliderHandle, &( GetOwningGameObject() ) );
    SetSceneTransform( MathUtils::MatrixToSceneTransform( GetOwningGameObject().GetGlobalTransformMatrix() ) );
}

//-----------------------------------------------------------------------

Collider::~Collider()
{
    SetEnabled( false );
    m_PhysicsManager.DestroyCollider( m_ColliderHandle );
    m_PhysicsManager.DestroyCollisionShape( m_CollisionShapeHandle );
}

//-----------------------------------------------------------------------

void Collider::SetEnabled(bool val)
{
    if( !( val && !m_CollisionShapeHandle.IsValid() ) ) // Collider can not be enabled if it does not have a valid collision shape
    {
        const bool wasEnabled = IsEnabled();

        Component::SetEnabled( val );

        RigidBody* rigidBody = GetOwningGameObject()._FindRigidBody();

        if( val && !wasEnabled )
        {
            if( rigidBody && rigidBody->IsEnabled() )
            {
                rigidBody->_FindAndAttachActiveColliders();
            }
            else
            {
                _AddToThePhysicsWorld();
            }
        }
        else if( wasEnabled )
        {
            _RemoveFromThePhysicsWorld();

            // If we were enabled and there is an enabled rigid body in the hierarchy then we need to detach from it
            if( wasEnabled && rigidBody && rigidBody->IsEnabled() )
            {
                // TODO At the moment this is done by "_FindAndAttachActiveColliders()" i.e. by scanning all of the
                // colliders and reattaching them to the rigid body. A better solution would be to have a function
                // "_DetachCollider(colliderPointer)" which would only detach a given collider without reattaching
                // all of the other colliders (this is possible, because btCompoundShape supports this functionality
                rigidBody->_FindAndAttachActiveColliders();
            }
        }
    }
}

//-----------------------------------------------------------------------

void Collider::SetTrigger(bool value)
{
    // TODO
}

//-----------------------------------------------------------------------

bool Collider::IsTrigger()
{
    return false; // TODO
}

//-----------------------------------------------------------------------

void Collider::SetSceneTransform(const SceneTypes::Transform& t)
{
    m_PhysicsManager.SetColliderTransform( m_ColliderHandle, t );
}

//-----------------------------------------------------------------------

void Collider::SetCenterPositionOffset(const glm::vec3& offset)
{
    m_PhysicsManager.SetColliderCenterPositionOffset( m_ColliderHandle, offset );

    RigidBody* rigidBody = GetOwningGameObject()._FindRigidBody(); // Attach this collider to the provided game object

    // If there is a rigid body in the hierarchy and it is enabled then reattach this collider to it
    if( rigidBody && rigidBody->IsEnabled() )
    {
        rigidBody->_FindAndAttachActiveColliders();
    }
}

//-----------------------------------------------------------------------

const glm::vec3 Collider::GetCenterPositionOffset() const
{
    return m_PhysicsManager.GetColliderCenterPositionOffset( m_ColliderHandle );
}

//-----------------------------------------------------------------------

void Collider::_InitializeAsDisabled()
{
    if( !m_Initialised )
    {
        m_Initialised = true;
        GetOwningGameObject().SetComponent( this );
        SetEnabled( false );
    }
}

//-----------------------------------------------------------------------

void Collider::_Initialize(PhysicsManager::CollisionShapeHandle shape)
{
    if( !m_Initialised )
    {
		m_Initialised = true;
	    GetOwningGameObject().SetComponent( this );
		_Reinitialize( shape );
    }
}

//-----------------------------------------------------------------------

void Collider::_Reinitialize(PhysicsManager::CollisionShapeHandle shape)
{
    _RemoveFromThePhysicsWorld();

    m_CollisionShapeHandle = shape;
    m_PhysicsManager.SetCollidersCollisionShape( m_ColliderHandle, m_CollisionShapeHandle );

    if( IsEnabled() )
    {
        RigidBody* rigidBody = GetOwningGameObject()._FindRigidBody(); // Attach this collider to the provided game object

        // If there is a rigid body in the hierarchy and it is enabled then attach this collider to it
        if( rigidBody && rigidBody->IsEnabled() )
        {
            rigidBody->_FindAndAttachActiveColliders();
        }
        else // Otherwise this collider is added to the physics world as a standalone entity
        {
            _AddToThePhysicsWorld();
        }
    }
    else
    {
        SetEnabled( true );
    }
}

//-----------------------------------------------------------------------

void Collider::_ChangeCollisionShape(PhysicsManager::CollisionShapeHandle newShape)
{
    PhysicsManager::CollisionShapeHandle previousCollisionShape = m_CollisionShapeHandle;
    _Reinitialize( newShape );
    m_PhysicsManager.DestroyCollisionShape( previousCollisionShape );
}

//-----------------------------------------------------------------------

void Collider::_ClearCollisionShape()
{
    SetEnabled( false ); // Collider can not be enabled if it does not have a collision shape
    m_PhysicsManager.DestroyCollisionShape( m_CollisionShapeHandle );
}

//-----------------------------------------------------------------------

void Collider::_RemoveFromThePhysicsWorld()
{
    m_PhysicsManager.RemoveColliderFromTheWorld( m_ColliderHandle );
}

//-----------------------------------------------------------------------

void Collider::_AddToThePhysicsWorld()
{
    m_PhysicsManager.AddColliderToTheWorld( m_ColliderHandle );
}

//-----------------------------------------------------------------------
