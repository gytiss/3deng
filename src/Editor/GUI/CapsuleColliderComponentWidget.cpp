#include "CapsuleColliderComponentWidget.h"
#include <GUI/GUIDefaults.h>

//-----------------------------------------------------------------------

CapsuleColliderComponentWidget::CapsuleColliderComponentWidget(CategorizedListWidget* listWidgetToAddOurselvesTo,
                                                               QWidget* parent)
: ColliderWidgetBase<Ui::CapsuleColliderComponentUI, CapsuleCollider>( listWidgetToAddOurselvesTo, "Capsule Collider", parent )
{
    connect( m_UI->radiusLineEdit, SIGNAL( editingFinished() ),
             this, SLOT( RadiusLineEditEditingFinished() ) );

    connect( m_UI->heightLineEdit, SIGNAL( editingFinished() ),
             this, SLOT( HeightLineEditEditingFinished() ) );

    connect( m_UI->directionComboBox, SIGNAL( currentIndexChanged( int ) ),
             this, SLOT( DirectionComboBoxCurrentIndexChanged( int ) ) );

    m_UI->radiusLineEdit->setValidator( &m_FloatValueValidator );
    m_UI->heightLineEdit->setValidator( &m_FloatValueValidator );
}

//-----------------------------------------------------------------------

CapsuleColliderComponentWidget::~CapsuleColliderComponentWidget()
{

}

//-----------------------------------------------------------------------

void CapsuleColliderComponentWidget::SetCapsuleColliderComponent(CapsuleCollider* collider)
{
    _SetColliderComponent( collider );

    if( collider )
    {
        GUIHelpers::SetNumericLineEditValue( m_UI->radiusLineEdit, collider->GetRadius() );
        GUIHelpers::SetNumericLineEditValue( m_UI->heightLineEdit, collider->GetHeight() );

        if( m_UI->directionComboBox->count() >= CapsuleCollider::e_DirectionCount )
        {
            m_UI->directionComboBox->setCurrentIndex( collider->GetDirection() );
        }
    }
}

//-----------------------------------------------------------------------

void CapsuleColliderComponentWidget::ResetPaletteToDefault()
{
    m_UI->capsuleColliderComponentContentWidget->setPalette( GUIDefaults::GetSingleton().DefaultPalette() );
}

//-----------------------------------------------------------------------

void CapsuleColliderComponentWidget::RadiusLineEditEditingFinished()
{
    if( m_ColliderComponent )
    {
        m_ColliderComponent->SetRadius( GUIHelpers::GetNumericLineEditValue( m_UI->radiusLineEdit ) );
    }
}

//-----------------------------------------------------------------------

void CapsuleColliderComponentWidget::HeightLineEditEditingFinished()
{
    if( m_ColliderComponent )
    {
        m_ColliderComponent->SetHeight( GUIHelpers::GetNumericLineEditValue( m_UI->heightLineEdit ) );
    }
}

//-----------------------------------------------------------------------

void CapsuleColliderComponentWidget::DirectionComboBoxCurrentIndexChanged(int index)
{
    if( m_ColliderComponent &&
        ( index >= 0 ) &&
        ( index < CapsuleCollider::e_DirectionCount ) &&
        ( static_cast<CapsuleCollider::Direction>( index ) != m_ColliderComponent->GetDirection() ) )
    {
        CapsuleCollider::Direction dir = static_cast<CapsuleCollider::Direction>( index );
        m_ColliderComponent->SetDirection( dir );
    }
}

//-----------------------------------------------------------------------
