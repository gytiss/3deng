#ifndef STATICINFINITEPLANE_H_INCLUDED
#define STATICINFINITEPLANE_H_INCLUDED

#include "Collider.h"

class StaticInfinitePlaneCollider : public Collider
{
    GAME_OBJECT_COMPONENT_TYPE(e_TypeCollider)

public:
    StaticInfinitePlaneCollider(GameObject& owningGameObject,
                                PhysicsManager& physicsManager);
};

#endif // STATICINFINITEPLANE_H_INCLUDED
