#ifndef GENERICEXCEPTION_H_INCLUDED
#define GENERICEXCEPTION_H_INCLUDED

#include <string>
#include <exception>

class GenericException : std::exception
{
public:
    GenericException(std::string msg) throw()
    : m_Msg( msg )
    {
    }

    virtual ~GenericException() throw()
    {
    }

    const char* what() const throw()
    {
        return m_Msg.c_str();
    }

private:
    std::string m_Msg;
};

#endif // GENERICEXCEPTION_H_INCLUDED
