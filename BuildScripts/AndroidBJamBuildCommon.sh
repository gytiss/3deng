#!/usr/bin/env bash 

SUCCESS=0

pushd `dirname $0` > /dev/null # Save script call dir and "cd" into the script dir
SCRIPT_DIR=`pwd`
popd > /dev/null # "cd" back into the script call dir

source "$SCRIPT_DIR/AndroidBuildCommon.sh"

if [ "x$BUILD_TYPE" = 'x' ] || \
   [ "x$ARCHS_TO_BUILD_FOR" = 'x' ] || \
   [ "x$SOURCE_DIR_OF_THE_LIB_BEING_BUILT" = 'x' ] || \
   [ "x$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT" = 'x' ] || \
   [ "x$RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT" = 'x' ];
then
    echo "You should not use $0 directly, use BuildLibsForAndroid.sh instead"
    exit 1
fi

RESULTING_ARMV7_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/armv7
RESULTING_ARM64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/arm64
RESULTING_MIPS_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/mips
RESULTING_MIPS64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/mips64
RESULTING_X86_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/x86
RESULTING_X86_64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/x86_64
    
# Note: Android API level 18 is the lowest level with OpenGL ES 3.0 support

cd $SOURCE_DIR_OF_THE_LIB_BEING_BUILT

# Remove build results directories just in case there are any existing builds
rm -rf $SOURCE_DIR_OF_THE_LIB_BEING_BUILT/bin.v2
rm -rf $SOURCE_DIR_OF_THE_LIB_BEING_BUILT/bin

######################################
# Build for ARMv7
######################################
if $(ContainsElement "armv7" ${ARCHS_TO_BUILD_FOR[@]});
then
    mkdir -p $RESULTING_ARMV7_BINARIES_DIR_OF_THE_LIB_BEING_BUILT
    
    if [ "$BUILD_TYPE" == "Debug" ];
    then
        EXTRA_BJAM_FLAGS="${EXTRA_BJAM_FLAGS} variant=debug"
    else
        EXTRA_BJAM_FLAGS="${EXTRA_BJAM_FLAGS} variant=release"
    fi
    
    BOOST_JAM_EXECUTABLE_DIR=$BOOST_JAM_EXECUTABLE_DIR \
    PREFIX="$RESULTING_ARMV7_BINARIES_DIR_OF_THE_LIB_BEING_BUILT" \
    TARGET_ARCH="arm" \
    TARGET_ARCH_NAME="armv7" \
    NDK_PLATFORM="android-18" \
    BJAM_FLAGS="$EXTRA_BJAM_FLAGS" "$SCRIPT_DIR/AndroidBJamBuild.sh" || { echo "ERROR: Failed to build for ARMv7" ; exit 1 ; }

    rm -rf $SOURCE_DIR_OF_THE_LIB_BEING_BUILT/bin.v2
    rm -rf $SOURCE_DIR_OF_THE_LIB_BEING_BUILT/bin
fi
######################################


######################################
# Build for ARM64
######################################
if $(ContainsElement "arm64" ${ARCHS_TO_BUILD_FOR[@]});
then
    mkdir -p $RESULTING_ARM64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT

    if [ "$BUILD_TYPE" == "Debug" ];
    then
        EXTRA_BJAM_FLAGS="${EXTRA_BJAM_FLAGS} variant=debug"
    else
        EXTRA_BJAM_FLAGS="${EXTRA_BJAM_FLAGS} variant=release"
    fi
    
    BOOST_JAM_EXECUTABLE_DIR=$BOOST_JAM_EXECUTABLE_DIR \
    PREFIX="$RESULTING_ARM64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT" \
    TARGET_ARCH="arm64" \
    TARGET_ARCH_NAME="arm64" \
    NDK_PLATFORM="android-21" \
    BJAM_FLAGS="$EXTRA_BJAM_FLAGS" "$(dirname "$0")/AndroidBJamBuild.sh" || { echo "ERROR: Failed to build for ARM64" ; exit 1 ; }

    rm -rf $SOURCE_DIR_OF_THE_LIB_BEING_BUILT/bin.v2
    rm -rf $SOURCE_DIR_OF_THE_LIB_BEING_BUILT/bin
fi
######################################


######################################
# Build for MIPS
######################################
if $(ContainsElement "mips" ${ARCHS_TO_BUILD_FOR[@]});
then
    mkdir -p $RESULTING_MIPS_BINARIES_DIR_OF_THE_LIB_BEING_BUILT

    if [ "$BUILD_TYPE" == "Debug" ];
    then
        EXTRA_BJAM_FLAGS="${EXTRA_BJAM_FLAGS} variant=debug"
    else
        EXTRA_BJAM_FLAGS="${EXTRA_BJAM_FLAGS} variant=release"
    fi
    
    BOOST_JAM_EXECUTABLE_DIR=$BOOST_JAM_EXECUTABLE_DIR \
    PREFIX="$RESULTING_MIPS_BINARIES_DIR_OF_THE_LIB_BEING_BUILT" \
    TARGET_ARCH="mips" \
    TARGET_ARCH_NAME="mips" \
    NDK_PLATFORM="android-18" \
    BJAM_FLAGS="$EXTRA_BJAM_FLAGS" "$(dirname "$0")/AndroidBJamBuild.sh" || { echo "ERROR: Failed to build for MIPS" ; exit 1 ; }

    rm -rf $SOURCE_DIR_OF_THE_LIB_BEING_BUILT/bin.v2
    rm -rf $SOURCE_DIR_OF_THE_LIB_BEING_BUILT/bin
fi
######################################


######################################
# Build for MIPS64
######################################
if $(ContainsElement "mips64" ${ARCHS_TO_BUILD_FOR[@]});
then
    mkdir -p $RESULTING_MIPS64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT

    if [ "$BUILD_TYPE" == "Debug" ];
    then
        EXTRA_BJAM_FLAGS="${EXTRA_BJAM_FLAGS} variant=debug"
    else
        EXTRA_BJAM_FLAGS="${EXTRA_BJAM_FLAGS} variant=release"
    fi
    
    BOOST_JAM_EXECUTABLE_DIR=$BOOST_JAM_EXECUTABLE_DIR \
    PREFIX="$RESULTING_MIPS64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT" \
    TARGET_ARCH="mips64" \
    TARGET_ARCH_NAME="mips64" \
    NDK_PLATFORM="android-21" \
    BJAM_FLAGS="$EXTRA_BJAM_FLAGS" "$(dirname "$0")/AndroidBJamBuild.sh" || { echo "ERROR: Failed to build for MIPS64" ; exit 1 ; }

    rm -rf $SOURCE_DIR_OF_THE_LIB_BEING_BUILT/bin.v2
    rm -rf $SOURCE_DIR_OF_THE_LIB_BEING_BUILT/bin
fi
######################################


######################################
# Build for x86
######################################
if $(ContainsElement "x86" ${ARCHS_TO_BUILD_FOR[@]});
then
    mkdir -p $RESULTING_X86_BINARIES_DIR_OF_THE_LIB_BEING_BUILT
        
    if [ "$BUILD_TYPE" == "Debug" ];
    then
        EXTRA_BJAM_FLAGS="${EXTRA_BJAM_FLAGS} variant=debug"
    else
        EXTRA_BJAM_FLAGS="${EXTRA_BJAM_FLAGS} variant=release"
    fi

    BOOST_JAM_EXECUTABLE_DIR=$BOOST_JAM_EXECUTABLE_DIR \
    PREFIX="$RESULTING_X86_BINARIES_DIR_OF_THE_LIB_BEING_BUILT" \
    TARGET_ARCH="x86" \
    TARGET_ARCH_NAME="x86" \
    NDK_PLATFORM="android-18" \
    BJAM_FLAGS="$EXTRA_BJAM_FLAGS" "$(dirname "$0")/AndroidBJamBuild.sh" || { echo "ERROR: Failed to build for x86" ; exit 1 ; }

    rm -rf $SOURCE_DIR_OF_THE_LIB_BEING_BUILT/bin.v2
    rm -rf $SOURCE_DIR_OF_THE_LIB_BEING_BUILT/bin
fi
######################################    
    
    
######################################
# Build for x86_64
######################################    
if $(ContainsElement "x86_64" ${ARCHS_TO_BUILD_FOR[@]});
then
    mkdir -p $RESULTING_X86_64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT

    if [ "$BUILD_TYPE" == "Debug" ];
    then
        EXTRA_BJAM_FLAGS="${EXTRA_BJAM_FLAGS} variant=debug"
    else
        EXTRA_BJAM_FLAGS="${EXTRA_BJAM_FLAGS} variant=release"
    fi
    
    BOOST_JAM_EXECUTABLE_DIR=$BOOST_JAM_EXECUTABLE_DIR \
    PREFIX="$RESULTING_X86_64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT" \
    TARGET_ARCH="x86_64" \
    TARGET_ARCH_NAME="x86_64" \
    NDK_PLATFORM="android-21" \
    BJAM_FLAGS="$EXTRA_BJAM_FLAGS" "$(dirname "$0")/AndroidBJamBuild.sh" || { echo "ERROR: Failed to build for x86_64" ; exit 1 ; }

    rm -rf $SOURCE_DIR_OF_THE_LIB_BEING_BUILT/bin.v2
    rm -rf $SOURCE_DIR_OF_THE_LIB_BEING_BUILT/bin
fi
######################################

SUCCESS=1

if [ "$SUCCESS" -eq 1 ] && \
   $(ContainsElement "armv7" ${ARCHS_TO_BUILD_FOR[@]}) && \
   ! $(CheckIfCompilationResultsExist "$RESULTING_ARMV7_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/lib" "$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT")
then
    SUCCESS=0
    echo "Failed to build one or more of the \"$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT\" for the armv7 Android ABI"
fi

if [ "$SUCCESS" -eq 1 ] && \
   $(ContainsElement "arm64" ${ARCHS_TO_BUILD_FOR[@]}) && \
   ! $(CheckIfCompilationResultsExist "$RESULTING_ARM64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/lib" "$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT")
then
    SUCCESS=0
    echo "Failed to build one or more of the \"$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT\" for the arm64 Android ABI"
fi

if [ "$SUCCESS" -eq 1 ] && \
   $(ContainsElement "mips" ${ARCHS_TO_BUILD_FOR[@]}) && \
   ! $(CheckIfCompilationResultsExist "$RESULTING_MIPS_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/lib" "$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT")
then
    SUCCESS=0
    echo "Failed to build one or more of the \"$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT\" for the mips Android ABI"
fi

if [ "$SUCCESS" -eq 1 ] && \
   $(ContainsElement "mips64" ${ARCHS_TO_BUILD_FOR[@]}) && \
   ! $(CheckIfCompilationResultsExist "$RESULTING_MIPS64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/lib" "$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT")
then
    SUCCESS=0
    echo "Failed to build one or more of the \"$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT\" for the mips64 Android ABI"
fi

if [ "$SUCCESS" -eq 1 ] && \
   $(ContainsElement "x86" ${ARCHS_TO_BUILD_FOR[@]}) && \
   ! $(CheckIfCompilationResultsExist "$RESULTING_X86_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/lib" "$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT")
then
    SUCCESS=0
    echo "Failed to build one or more of the \"$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT\" for the x86 Android ABI"
fi

if [ "$SUCCESS" -eq 1 ] && \
   $(ContainsElement "x86_64" ${ARCHS_TO_BUILD_FOR[@]}) && \
   ! $(CheckIfCompilationResultsExist "$RESULTING_X86_64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/lib" "$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT")
then
    SUCCESS=0
    echo "Failed to build one or more of the \"$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT\" for the x86_64 Android ABI"
fi

# Set script return code
if [ "$SUCCESS" -eq 1 ];
then
    exit 0 # Success
else
    exit 1 # Failure
fi
