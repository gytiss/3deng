#ifndef WINDOWMANAGER_H_INCLUDED
#define WINDOWMANAGER_H_INCLUDED

#include <string>
#include <map>
#include <InputMapping/InputMapper.h>
#include <SystemInterface.h>
#include <SDL2/SDL.h>

#ifdef VR_BACKEND_ENABLED
    class VRBackend;
#endif

class WindowManager : public SystemInterface
{
public:
    WindowManager(InputMapping::InputMapper& inputMapper,
                  size_t width,
                  size_t height,
                  std::string title,
                  bool fullScreen,
                  bool enableMultisampling,
                  bool enableVSync);

#ifdef VR_BACKEND_ENABLED
    WindowManager(InputMapping::InputMapper& inputMapper,
                  VRBackend& vrBackend,
                  std::string title,
                  bool enableVSync);
#endif

    virtual ~WindowManager();
    void ProcessWindowEvents();
    void DestroyWindow();

    /**
     * Is it possible to see this window on the screen
     */
    bool IsVisible() const;

    void SwapFrameBuffers();

    const DrawableSurfaceSize& GetDrawableSurfaceSize() const;

    /**
     * Tell window manager whether to capture mouse or not
     */
    void SetCaptureMouseInTheWindow(bool val);

    bool GetCaptureMouseInTheWindow() const;

    /**
     * Set whether mouse coordinates start at the center of the window or not
     *
     * @note When mouse origin is at the center, mouse is brought back to the center of the screen at the end of every frame
     *
     * @param val - True(Origin of the generated mouse coordinates is at center of the screen)
     *             False(Origin of the generated mouse coordinates is at the top left corner of the screen)
     */
    void SetMouseIsRelativeToTheCenter(bool val);

    bool GetMouseIsRelativeToTheCenter() const;

    /**
     * Enables processing of text inputevents
     */
    void SetTextInputEnabled(bool val);

    bool GetTextInputEnabled() const;

    void SetHideMouseCursor(bool val);
    bool GetHideMouseCursor() const;

    bool WindowCloseWasRequested();

    /**
     * @param x - zero to window width with origin at the left side of the window
     * @param y - zero to window height with origin at the top side of the window
     * @param width
     * @param height
     */
    void SetAsianTextInputHelperRect(int x, int y, int width, int heigh);

protected:
#ifdef VR_BACKEND_ENABLED
    WindowManager(InputMapping::InputMapper& inputMapper,
                  VRBackend* vrBackend,
                  size_t width,
                  size_t height,
                  std::string title,
                  bool fullScreen,
                  bool enableMultisampling,
                  bool enableVSync);
#endif
    
    uint64_t _GetTicksInMilliseconds();

private:
    typedef std::map<SDL_Keycode, InputMapping::RawInputButton> SDLKeyCodesToRawInputButtonsMap;

    WindowManager& operator = (const WindowManager& other) = delete; // Prevent copying of WindowManager

    void _InitializeKeyCodeMap(SDLKeyCodesToRawInputButtonsMap&  keyCodeMap);
    bool _MapSDLKeyCodeToRawInputButton(SDL_Keycode sdlKeyCode, InputMapping::RawInputButton& outRawInputButton);

#ifdef VR_BACKEND_ENABLED
    void _GetVRSecondaryWindowPositionAndSize(bool fullScreen, int& outXPos, int& outYPos, int& outWidth, int& outHeight);
#endif

    inline void _SetupSDLWindow(SDL_Window*& window,
                                SDL_GLContext *context,
                                size_t width,
                                size_t height,
                                std::string title,
                                bool fullScreen,
                                bool enableMultisampling,
                                bool enableVSync);

    void _SetupWindow(size_t width,
                      size_t height,
                      std::string title,
                      bool fullScreen,
                      bool enableMultisampling,
                      bool enableVSync);

    InputMapping::InputMapper& m_InputMapper;

    SDL_Window* m_AppWindow;
    Uint32 m_AppWindowID;
    SDL_GLContext* m_OpenGL_Context;
    SDLKeyCodesToRawInputButtonsMap m_KeyCodeMap;

    DrawableSurfaceSize m_ScreenSize;
    bool m_MouseCapturedInTheWindow;
    bool m_MouseIsRelativeToTheCenter;
    bool m_TextInputEnabled;
    bool m_HideMouseCursor;
    int m_PreviousAbsoluteMousePosX;
    int m_PreviousAbsoluteMousePosY;
    bool m_WindowCloseHasBeenRequested;
    bool m_IsVisible;
    bool m_VSyncIsEnabled;
#ifdef VR_BACKEND_ENABLED
    VRBackend* m_VRBackend;
#endif
};

#endif // WINDOWMANAGER_H_INCLUDED
