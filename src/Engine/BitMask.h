#ifndef BITMASK_H_INCLUDED
#define BITMASK_H_INCLUDED

#include <cstdint>
#include <cassert>

/**
 *  A general bitmask class which stores an array of
 *  bits of some length that must fit within a given word
 *  of the indicated type.
 */
template<class WType, WType WidthInBits>
class BitMask {
public:
    BitMask()
    : BitMask( 0 )
    {
        static_assert( ( ( sizeof( WType ) * 8 ) <= WidthInBits ), "WidthInBits must not be larger than the size of WType in bits" );
    }

    BitMask(WType value)
    : m_Value( value )
    {
    }

    ~BitMask() = default;

    typedef WType WordType;

    /**
     * @return - Width in bits of this bitmask
     */
    static inline constexpr WType NumBits()
    {
        return WidthInBits;
    }

    /**
     * @return - A BitMask with all of its bits on
     */
    static inline constexpr BitMask<WType, WidthInBits> AllOn();

    /**
     * Set the bit in a given location of the bitmask to 1
     *
     * @param bitIndex - Index of the bit to set
     */
    inline void SetBit(WType bitIndex);

    /**
     * Set the bit in a given location of the bitmask to 0
     *
     * @param bitIndex - Index of the bit to set
     */
    inline void ClearBit(WType bitIndex);

    /**
     * Check if the bit in a given location is set to 1
     *
     * @param bitIndex - Index of the bit to check
     */
    inline bool IsBitSet(WType bitIndex) const;

    /**
     * Merge values of a given mask into this one
     *
     * @param otherMask - Mask to merge the values from
     */
    inline void AddMask(const BitMask<WType, WidthInBits>& otherMask);

    /**
     * Check if the bitmask consists of the provided values
     *
     * @param firstBitmaskValue - First bitmask value
     * @param bitmaskValues - A variadic argument list containing the remaining bitmask values
     *
     * @return - True if this bitmask contains only the provided values, False otherwise
     */
    template<typename... Args>
    bool ConsistOf(WType firstBitmaskValue, Args... bitmaskValues) const;

    /**
     * Check if the bitmask contains the provided values
     *
     * @param firstBitmaskValue - First bitmask value
     * @param bitmaskValues - A variadic argument list containing the remaining bitmask values
     *
     * @return - True if this bitmask contains the provided values, False otherwise
     */
    template<typename... Args>
    bool Contains(WType firstBitmaskValue, Args... bitmaskValues) const;

    /**
     * @raturn - Raw bitmask value
     */
    inline WType Value() const
    {
        return m_Value;
    }

private:
    // Used for combining argument values from the provided argument list
    // into one value of type "WType" using the bitwise operator "|".
    inline WType orrer(WType v) const;
    template<typename... Args>
    inline WType orrer(WType first, Args... args) const;

    WordType m_Value; /// Raw bitmask value
};

//-----------------------------------------------------------------------

template<class WType, WType WidthInBits>
inline constexpr BitMask<WType, WidthInBits> BitMask<WType, WidthInBits>::AllOn()
{
    return BitMask( ~static_cast<WordType>( 0 ) );
}

//-----------------------------------------------------------------------

template<class WType, WType WidthInBits>
inline void BitMask<WType, WidthInBits>::SetBit(WType bitIndex)
{
    assert( ( bitIndex <= NumBits() ) && "Bit position must not be larger than the bitmask width" );

    m_Value |= ( 1 << bitIndex );
}

//-----------------------------------------------------------------------

template<class WType, WType WidthInBits>
inline void BitMask<WType, WidthInBits>::ClearBit(WType bitIndex)
{
    assert( ( bitIndex <= NumBits() ) && "Bit position must not be larger than the bitmask width" );

    m_Value &= ~( 1 << bitIndex );
}

//-----------------------------------------------------------------------

template<class WType, WType WidthInBits>
inline bool BitMask<WType, WidthInBits>::IsBitSet(WType bitIndex) const
{
    assert( ( bitIndex <= NumBits() ) && "Bit position must not be larger than the bitmask width" );

    return ( ( m_Value & ( static_cast<decltype( m_Value )>( 1 ) << bitIndex ) ) != 0 );
}

//-----------------------------------------------------------------------

template<class WType, WType WidthInBits>
inline void BitMask<WType, WidthInBits>::AddMask(const BitMask<WType, WidthInBits>& otherMask)
{
    m_Value |= otherMask.Value();
}

//-----------------------------------------------------------------------

template<class WType, WType WidthInBits>
template<typename... Args>
bool BitMask<WType, WidthInBits>::ConsistOf(WType firstBitmaskValue, Args... bitmaskValues) const
{
    return ( orrer( firstBitmaskValue, bitmaskValues... ) == m_Value );
}

//-----------------------------------------------------------------------

template<class WType, WType WidthInBits>
template<typename... Args>
bool BitMask<WType, WidthInBits>::Contains(WType firstBitmaskValue, Args... bitmaskValues) const
{
    const WType orredValues = orrer( firstBitmaskValue, bitmaskValues... );
    return ( ( orredValues & m_Value ) == orredValues );
}

//-----------------------------------------------------------------------

template<class WType, WType WidthInBits>
inline WType BitMask<WType, WidthInBits>::orrer(WType v) const
{
    return v;
}

//-----------------------------------------------------------------------

template<class WType, WType WidthInBits>
template<typename... Args>
inline WType BitMask<WType, WidthInBits>::orrer(WType first, Args... args) const
{
    return static_cast<unsigned int>( first ) | static_cast<unsigned int>( orrer( args... ) );
}

//-----------------------------------------------------------------------

typedef BitMask<uint16_t, 16> BitMask16;
typedef BitMask<uint32_t, 32> BitMask32;
typedef BitMask<uint64_t, 64> BitMask64;


#endif // BITMASK_H_INCLUDED
