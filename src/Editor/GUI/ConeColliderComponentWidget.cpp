#include "ConeColliderComponentWidget.h"
#include <GUI/GUIDefaults.h>

//-----------------------------------------------------------------------

ConeColliderComponentWidget::ConeColliderComponentWidget(CategorizedListWidget* listWidgetToAddOurselvesTo,
                                                         QWidget* parent)
: ColliderWidgetBase<Ui::ConeColliderComponentUI, ConeCollider>( listWidgetToAddOurselvesTo, "Cone Collider", parent )
{
    connect( m_UI->radiusLineEdit, SIGNAL( editingFinished() ),
             this, SLOT( RadiusLineEditEditingFinished() ) );

    connect( m_UI->heightLineEdit, SIGNAL( editingFinished() ),
             this, SLOT( HeightLineEditEditingFinished() ) );

    connect( m_UI->directionComboBox, SIGNAL( currentIndexChanged( int ) ),
             this, SLOT( DirectionComboBoxCurrentIndexChanged( int ) ) );

    m_UI->radiusLineEdit->setValidator( &m_FloatValueValidator );
    m_UI->heightLineEdit->setValidator( &m_FloatValueValidator );
}

//-----------------------------------------------------------------------

ConeColliderComponentWidget::~ConeColliderComponentWidget()
{

}

//-----------------------------------------------------------------------

void ConeColliderComponentWidget::SetConeColliderComponent(ConeCollider* collider)
{
    _SetColliderComponent( collider );

    if( collider )
    {
        GUIHelpers::SetNumericLineEditValue( m_UI->radiusLineEdit, collider->GetRadius() );
        GUIHelpers::SetNumericLineEditValue( m_UI->heightLineEdit, collider->GetHeight() );

        if( m_UI->directionComboBox->count() >= ConeCollider::e_DirectionCount )
        {
            m_UI->directionComboBox->setCurrentIndex( collider->GetDirection() );
        }
    }
}

//-----------------------------------------------------------------------

void ConeColliderComponentWidget::ResetPaletteToDefault()
{
    m_UI->coneColliderComponentContentWidget->setPalette( GUIDefaults::GetSingleton().DefaultPalette() );
}

//-----------------------------------------------------------------------

void ConeColliderComponentWidget::RadiusLineEditEditingFinished()
{
    if( m_ColliderComponent )
    {
        m_ColliderComponent->SetRadius( GUIHelpers::GetNumericLineEditValue( m_UI->radiusLineEdit ) );
    }
}

//-----------------------------------------------------------------------

void ConeColliderComponentWidget::HeightLineEditEditingFinished()
{
    if( m_ColliderComponent )
    {
        m_ColliderComponent->SetHeight( GUIHelpers::GetNumericLineEditValue( m_UI->heightLineEdit ) );
    }
}

//-----------------------------------------------------------------------

void ConeColliderComponentWidget::DirectionComboBoxCurrentIndexChanged(int index)
{
    if( m_ColliderComponent &&
        ( index >= 0 ) &&
        ( index < ConeCollider::e_DirectionCount ) &&
        ( static_cast<ConeCollider::Direction>( index ) != m_ColliderComponent->GetDirection() ) )
    {
        ConeCollider::Direction dir = static_cast<ConeCollider::Direction>( index );
        m_ColliderComponent->SetDirection( dir );
    }
}

//-----------------------------------------------------------------------
