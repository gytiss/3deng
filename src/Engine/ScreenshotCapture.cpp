#include "ScreenshotCapture.h"
#include <Exceptions/GenericException.h>
#include <imgutl/ImageUtils.h>
#include <OpenGLErrorChecking.h>
#include <GraphicsDevice/GraphicsDevice.h>
#include <sstream>
#include <boost/locale.hpp>

//-----------------------------------------------------------------------

ScreenshotCapture::ScreenshotCapture(SystemInterface& systemInterface,
                                     GraphicsDevice& graphicsDevice,
                                     const std::string& screenshotDirectoryPath)
: m_SystemInterface( systemInterface ),
  m_DrawableSurfaceSize( m_SystemInterface.GetDrawableSurfaceSize() ),
  m_GraphicsDevice( graphicsDevice ),
  m_TimeFacet( new boost::posix_time::time_facet("%Y%m%d_%H%M%S") ),
  m_ScreenshotDirectoryPath( screenshotDirectoryPath ),
  m_TimeFormat( std::locale::classic(), m_TimeFacet )
{
#ifndef OS_ANDROID
    if( boost::filesystem::exists( m_ScreenshotDirectoryPath ) )
    {
        if( boost::filesystem::is_regular_file( m_ScreenshotDirectoryPath ) )
        {
            std::string msg = "Can not save screenshots to \"";
                         msg += screenshotDirectoryPath;
                         msg += "\", because it is a file not a directory.";

            throw GenericException( msg );
        }
    }
    else
    {
        boost::system::error_code errorCode;
        if( !boost::filesystem::create_directories( m_ScreenshotDirectoryPath, errorCode ) )
        {
            std::string msg = "Failed to create directory \"";
                         msg += screenshotDirectoryPath;
                         msg += "\" with the following error message: ";
                         msg += errorCode.message();

            throw GenericException( msg );
        }
    }

#endif
    m_SystemInterface.RegisterDrawableSurfaceSizeChangedCallback( _DrawableSurfaceSizeChaged, this );
}

//-----------------------------------------------------------------------

ScreenshotCapture::~ScreenshotCapture()
{
    m_SystemInterface.DeregisterDrawableSurfaceSizeChangedCallback( _DrawableSurfaceSizeChaged, this );
}

//-----------------------------------------------------------------------

void ScreenshotCapture::CaptureScreenshot(const std::string& outputImageFileName)
{
    _CaptureScreenshot( outputImageFileName, false );
}

//-----------------------------------------------------------------------

void ScreenshotCapture::CaptureStencilBufferScreenshot(const std::string& outputImageFileName)
{
    _CaptureScreenshot( outputImageFileName, true );
}

//-----------------------------------------------------------------------

void ScreenshotCapture::_CaptureScreenshot(const std::string& outputImageFileName, bool dumpStencilBuffer)
{
#ifndef OS_ANDROID
    if( outputImageFileName.empty() )
    {
        // Get date time string in YYYYMMDD_HHMMSS format
        std::stringstream dateToStrinStream;
        dateToStrinStream.imbue( m_TimeFormat );
        dateToStrinStream << boost::posix_time::second_clock::local_time();
        std::string dateAndTimeStr = dateToStrinStream.str();

        m_ScreenshotDirectoryPath /= dateAndTimeStr + ".tga"; // Path in the form of screenshorDir/YYYYMMDD_HHMMSS.tga
    }
    else
    {
        m_ScreenshotDirectoryPath /= outputImageFileName + ".tga"; // Path in the form of screenshorDir/some_name.tga
    }

    ImageUtils::ImageData_t* screenshotImage = nullptr;

    if( dumpStencilBuffer )
    {
#ifndef OPENGL_ES
        screenshotImage = ImageUtils::BlankImage( m_DrawableSurfaceSize.widthInPixels,
                                                  m_DrawableSurfaceSize.heightInPixels,
                                                  ImageUtils::RGB );

        const size_t stencilDataSize = ( m_DrawableSurfaceSize.widthInPixels * m_DrawableSurfaceSize.heightInPixels );
        uint8_t* stencilData = new uint8_t[stencilDataSize];

        m_GraphicsDevice.ReadStencilBuffer( 0,
                                            0,
                                            m_DrawableSurfaceSize.widthInPixels,
                                            m_DrawableSurfaceSize.heightInPixels,
                                            stencilData );

        size_t j = 0;
        for(size_t i = 0; i < stencilDataSize; i++)
        {
            const uint8_t dataPoint = stencilData[i];
            if( dataPoint > 0 )
            {
                screenshotImage->data[j] = 0xFF; // Red Color
            }

            j += 3;
        }

        delete[] stencilData;
#endif
    }
    else
    {
        screenshotImage = ImageUtils::BlankImage( m_DrawableSurfaceSize.widthInPixels,
                                                  m_DrawableSurfaceSize.heightInPixels,
                                                  ImageUtils::RGBA );

        m_GraphicsDevice.ReadPixelsFromFrameBuffer( 0,
                                                    0,
                                                    m_DrawableSurfaceSize.widthInPixels,
                                                    m_DrawableSurfaceSize.heightInPixels,
                                                    screenshotImage->data );
    }

    if( screenshotImage )
    {
        ImageUtils::SaveImage( ImageUtils::TGA,
							   boost::locale::conv::utf_to_utf<char>( m_ScreenshotDirectoryPath.c_str() ),
                               screenshotImage );
        ImageUtils::DeleteImage( screenshotImage );
    }

    m_ScreenshotDirectoryPath.remove_leaf(); // Remove screenshot name from the path
#endif
}

void ScreenshotCapture::_DrawableSurfaceSizeChaged(const SystemInterface::DrawableSurfaceSize& newSize, void* userData)
{
    static_cast<ScreenshotCapture*>( userData )->m_DrawableSurfaceSize = newSize;
}

//-----------------------------------------------------------------------
