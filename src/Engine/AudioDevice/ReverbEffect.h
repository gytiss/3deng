#ifndef AUDIODEVICE_REVERBEFFECT_H_INCLUDED
#define AUDIODEVICE_REVERBEFFECT_H_INCLUDED

#include "Effect.h"
#include <glm/glm.hpp>

namespace AudioDevice
{

class Context;
class EffectSlot;

class ReverbEffect : public Effect
{
public:
    enum Preset
    {
        e_PresetGeneric = 0,
        e_PresetPaddedCell = 1,
        e_PresetRoom,
        e_PresetBathRoom,
        e_PresetLivingRoom,
        e_PresetStoneRoom,
        e_PresetAuditorium,
        e_PresetConcertHall,
        e_PresetCave,
        e_PresetArena,
        e_PresetHangar,
        e_PresetCarpetedHallway,
        e_PresetHallway,
        e_PresetStoneCorridor,
        e_PresetAlley,
        e_PresetForest,
        e_PresetCity,
        e_PresetMountains,
        e_PresetQuarry,
        e_PresetPlain,
        e_PresetParkingLot,
        e_PresetSewerPipe,
        e_PresetUnderwater,
        e_PresetDrugged,
        e_PresetDizzy,
        e_PresetPsychotic,
        e_PresetCastleSmallRoom,
        e_PresetCastleShortPassage,
        e_PresetCastleMediumRoom,
        e_PresetCastleLargeRoom,
        e_PresetCastleLongPassage,
        e_PresetCastleHall,
        e_PresetCastleCupBoard,
        e_PresetCastleCourtyard,
        e_PresetCastleAlcove,
        e_PresetFactorySmallRoom,
        e_PresetFactoryShortPassage,
        e_PresetFactoryMediumRoom,
        e_PresetFactoryLargeRoom,
        e_PresetFactoryLongPassage,
        e_PresetFactoryHall,
        e_PresetFactoryCupboard,
        e_PresetFactoryCourtyard,
        e_PresetFactoryAlcove,
        e_PresetIcePalaceSmallRoom,
        e_PresetIcePalaceShortPassage,
        e_PresetIcePalaceMediumRoom,
        e_PresetIcePalaceLargeRoom,
        e_PresetIcePalaceLongPassage,
        e_PresetIcePalaceHall,
        e_PresetIcePalaceCupBoard,
        e_PresetIcePalaceCourtyard,
        e_PresetIcePalaceAlcove,
        e_PresetSpaceStationSmallRoom,
        e_PresetSpaceStationShortPassage,
        e_PresetSpaceStationMediumRoom,
        e_PresetSpaceStationLargeRoom,
        e_PresetSpaceStationLongPassage,
        e_PresetSpaceStationHall,
        e_PresetSpaceStationCupBoard,
        e_PresetSpaceStationAlcove,
        e_PresetWoodenSmallRoom,
        e_PresetWoodenShortPassage,
        e_PresetWoodenMediumRoom,
        e_PresetWoodenLargeRoom,
        e_PresetWoodenLongPassage,
        e_PresetWoodenHall,
        e_PresetWoodenCupBoard,
        e_PresetWoodenCourtyard,
        e_PresetWoodenAlcove,
        e_PresetSportEmptyStadium,
        e_PresetSportSquashCourt,
        e_PresetSportSmallSwimmingPool,
        e_PresetSportLargeSwimmingPool,
        e_PresetSportGymnasium,
        e_PresetSportFullStadium,
        e_PresetSportStadiumTannoy,
        e_PresetPrefabWorkShop,
        e_PresetPrefabSchoolRoom,
        e_PresetPrefabPracticeRoom,
        e_PresetPrefabOuthouse,
        e_PresetPrefabCaravan,
        e_PresetDomeTomb,
        e_PresetPipeSmall,
        e_PresetDomeSaintPauls,
        e_PresetPipeLongThin,
        e_PresetPipeLarge,
        e_PresetPipeResonant,
        e_PresetOutdoorsBackyard,
        e_PresetOutdoorsRollingPlains,
        e_PresetOutdoorsDeepCanyon,
        e_PresetOutdoorsCreek,
        e_PresetOutdoorsValley,
        e_PresetMoodHeaven,
        e_PresetMoodHell,
        e_PresetMoodMemory,
        e_PresetDrivingCommentator,
        e_PresetDrivingPitGarage,
        e_PresetDrivingInvarRacer,
        e_PresetDrivingInvarSports,
        e_PresetDrivingInvarLuxury,
        e_PresetDrivingFullGrandstand,
        e_PresetDrivingEmptyGrandstand,
        e_PresetDrivingTunnel,
        e_PresetCityStreets,
        e_PresetCitySubway,
        e_PresetCityMuseum,
        e_PresetCityLibrary,
        e_PresetCityUnderpass,
        e_PresetCityAbandoned,
        e_PresetDustyRoom,
        e_PresetChapel,
        e_PresetSmallWaterRoom,

        e_PresetCount // Not a preset (Used for counting)
    };

    enum FloatParam
    {
        /**
         * Reverb Modal Density controls the coloration of the late reverb.  Lowering the value adds more coloration to the late reverb.
         *
         * Value Units: N/A
         * Value range: 0.0 to 1.0
         */
        e_FloatParamDensity = 0,

        /**
         * The Reverb Diffusion property controls the echo density in the reverberation decay.  It’s set by
         * default to 1.0, which provides the highest density.  Reducing diffusion gives the reverberation a
         *more "grainy" character that is especially not iceable with percussive sound sources.  If you set a
         * diffusion value of 0.0, the later reverberati on sounds like a succession of distinct echoes.
         *
         * Value Units: A linear multiplier value
         * Value range: 0.0 to 1.0
         */
        e_FloatParamDiffusion = 1,

        /**
         * The Reverb Gain property is the master volume control for the reflected sound (both early
         * reflections and reverberation) that the reverb effect adds to all sound sources.  It sets the
         * maximum amount of reflections and reverberation added to the final sound mix.  The value of the
         * Reverb Gain property ranges from 1.0 (0db) (the maximum amount) to 0.0 (-100db) (no reflected sound at all).
         *
         * Value Units: Linear gain
         * Value range: 0.0 to 1.0
         */
        e_FloatParamGain,

        /**
         * The Reverb Gain HF property further tweaks reflected sound by attenuating it at high frequencies.
         * It controls a low-pass filter that applies globally to the reflected sound of all sound sources
         * feeding the particular instance of the reverb effect.  The value of the Reverb Gain HF property
         * ranges from 1.0 (0db) (no filter) to 0.0 (-100db) (virtually no reflected sound).
         * HF Reference sets the frequency at which the value of this property is measured.
         *
         * Value Units: Linear gain
         * Value range: 0.0 to 1.0
         */
        e_FloatParamGainHF,

        /**
         * The Reverb Gain LF property further tweaks refl ected sound by attenuating it at low frequencies.
         * It controls a high-pass filter that applies globa lly to the reflected sound of all sound sources
         * feeding the particular instance of the reverb effect.  The value of the Reverb Gain LF property
         * ranges from 1.0 (0db) (no filter) to 0.0 (-100db) (virtually no reflected sound).
         * LF Reference sets the frequency at which the value of this property is measured.
         *
         * Value Units: Linear gain
         * Value range: 0.0 to 1.0
         */
        e_FloatParamGainLF,

        /**
         * The Decay Time property sets the reverberation de cay time.  It ranges from 0.1 (typically a small
         * room with very dead surfaces) to 20.0 (typica lly a large room with very live surfaces).
         *
         * Value Units: Seconds
         * Value range: 0.1 to 20.0
         */
        e_FloatParamDecayTime,

        /**
         * The Decay HF Ratio property adjusts the spectral quality of the Decay Time parameter.  It is the
         * ratio of high-frequency decay time relative to the time set by Decay Time.  The Decay HF Ratio
         * value 1.0 is neutral: the decay time is equal for all frequencies.  As Decay HF Ratio increases
         * above 1.0, the high-frequency decay time increases so it’s longer than the decay time at mid
         * frequencies.  You hear a more brilliant reverberation with a longer decay at high frequencies.  As
         * 97/144 the Decay HF Ratio value decreases below 1.0, the high-frequency decay time decreases so it’s
         * shorter than the decay time of the mid frequenc ies.  You hear a more natural reverberation.
         *
         * Value Units: A linear multiplier value
         * Value range: 0.1 to 2.0
         */
        e_FloatParamDecayHFRatio,

        /**
         * The Decay LF Ratio property adjusts the spectral quality of the Decay Time parameter.  It is the
         * ratio of low-frequency decay time relative to the time set by Decay Time.  The Decay LF Ratio
         * value 1.0 is neutral: the decay time is equal for all frequencies.  As Decay LF Ratio increases
         * above 1.0, the low-frequency decay time increase s so it’s longer than the decay time at mid
         * frequencies.  You hear a more booming reverberation with a longer decay at low frequencies.  As
         * the Decay LF Ratio value decreases below 1.0, the low-frequency decay time decreases so it’s
         * shorter than the decay time of the mid frequencies.  You hear a more tinny reverberation.
         *
         * Value Units: A linear multiplier value
         * Value range: 0.1 to 2.0
         */
        e_FloatParamDecayLFRatio,

        /**
         * The Reflections Gain property controls the overall amount of initial reflections relative to the Gain
         * property.  (The Gain property sets the overall amount of reflected sound: both initial reflections
         * and later reverberation.)  The value of Reflections Gain ranges from a maximum of 3.16 (+10 dB)
         * to a minimum of 0.0 (-100 dB) (no initial reflections at all), and is corrected by the value of the
         * Gain property.  The Reflections Gain property does not affect the subsequent reverberation
         * decay. You can increase the amount of initial reflections to simulate a more narrow space or closer walls,
         * especially effective if you associate the initial reflections increase with a reduction in reflections
         * delays by lowering the value of the Reflection Delay property.  To simulate open or semi-open
         * environments, you can maintain the amount of early reflections while reducing the value of the
         * Late Reverb Gain property, which controls later reflections.
         *
         * Value Units: Linear gain
         * Value range: 0.0 to 3.16
         */
        e_FloatParamReflectionsGain,

        /**
         * The Reflections Delay property is the amount of delay between the arrival time of the direct path
         * from the source to the first reflection from the source.  It ranges from 0 to 300 milliseconds. You can
         * reduce or increase Reflections Delay to simulate closer or more distant reflective surfaces and therefore
         * control the perceived size of the room.
         *
         * Value Units: Seconds
         * Value range: 0.0 to 0.3
         */
        e_FloatParamReflectionsDelay,

        /**
         * The Late Reverb Gain property controls the overall amount of later reverberation relative to the
         * Gain property.  (The Gain property sets the overall amount of both initial reflections and later
         * reverberation.)  The value of Late Reverb Gain ranges from a maximum of 10.0 (+20 dB) to a
         * minimum of 0.0 (-100 dB) (no late reverberation at all). Note that Late Reverb Gain and Decay Time are
         * independent properties: If you adjust Decay Time without changing Late Reverb Gain, the total intensity
         * (the averaged square of the amplitude) of the late reverberation remains constant.
         *
         * Value Units:  Linear gain
         * Value range: 0.0 to 10.0
         */
        e_FloatParamLateReverbGain,

        /**
         * The Late Reverb Delay property defines the begin time of the late reverberation relative to the
         * time of the initial reflection (the first of the early reflections).  It ranges from 0 to 100
         * milliseconds. Reducing or increasing Late Reverb Delay is useful for simulating a smaller or
         * larger room.
         *
         * Value Units: Seconds
         * Value range: 0.0 to 0.1
         */
        e_FloatParamLateReverbDelay,

        /**
         * Echo Depth introduces a cyclic echo in the reverberation decay, which will be noticeable with
         * transient or percussive sounds.  A larger value of Echo Depth will make this effect more
         * prominent. Echo Time controls the rate at which the cyclic echo repeats itself along the
         * reverberation decay.  For example, the default setting for Echo Time is 250 ms. causing the echo
         * to occur 4 times per second.  Therefore, if you were to clap your hands in this type of
         * environment, you will hear four repetitions of clap per second.
         *
         * Together with Reverb Diffusion, Echo Depth will control how long
         * the echo effect will persist along the reverberation decay.  In a more diffuse environment,
         * echoes will wash out more quickly after the direct sound.  In an environment that is less
         * diffuse, you will be able to hear a larger number of repetitions of the echo, which will
         * wash out later in the reverberation decay.  If Diffusion is set to 0.0 and Echo Depth is set
         * to 1.0, the echo will persist distinctly until the end of the reverberation decay.
         *
         * Value Units: Seconds
         * Value range: 0.075 to 0.25
         */
        e_FloatParamEchoTime,

        /**
         * Echo Depth introduces a cyclic echo in the reverberation decay, which will be noticeable with
         * transient or percussive sounds.  A larger value of Echo Depth will make this effect more
         * prominent. Echo Time controls the rate at which the cyclic echo repeats itself along the
         * reverberation decay.  For example, the default setting for Echo Time is 250 ms. causing the echo
         * to occur 4 times per second.  Therefore, if you were to clap your hands in this type of
         * environment, you will hear four repetitions of clap per second.
         *
         * Together with Reverb Diffusion, Echo Depth will control how long
         * the echo effect will persist along the reverberation decay.  In a more diffuse environment,
         * echoes will wash out more quickly after the direct sound.  In an environment that is less diffuse,
         * you will be able to hear a larger number of repetitions of the echo, which will wash out later in the
         * reverberation decay.  If Diffusion is set to 0.0 and Echo Depth is set to 1.0, the echo will persist
         * distinctly until the end of the reverberation decay.
         *
         * Value Units: A linear multiplier value
         * Value range: 0.0 to 1.0
         */
        e_FloatParamEchoDepth,

        /**
         * Using these two properties, you can create a pitch modulation in the reverberant sound.  This will
         * be most noticeable applied to sources that have tonal color or pitch.  You can use this to make
         * some trippy effects! Modulation Time controls the speed of the vibrato (rate of periodic changes in pitch).
         *
         * Modulation Depth controls the amount of pitch change.  Low values of
         * Diffusion will contribute to reinforcing the perceived effect by reducing
         * the mixing of overlapping reflections in the reverberation decay.
         *
         * Value Units: Seconds
         * Value range: 0.04 to 4.0
         */
        e_FloatParamModulationTime,

        /**
         * Using these two properties, you can create a pitch modulation in the reverberant sound.  This will
         * be most noticeable applied to sources that have tonal color or pitch.  You can use this to make
         * some trippy effects! Modulation Time controls the speed of the vibrato (rate of periodic changes in pitch).
         *
         * Modulation Depth controls the amount of pitch change.  Low values of
         * Diffusion will contribute to reinforcing the perceived effect by reducing
         * the mixing of overlapping reflections in the reverberation decay.
         *
         * Value Units:  A linear multiplier value
         * Value range: 0.0 to 1.0
         */
        e_FloatParamModulationDepth,

        /**
         * The Air Absorption Gain HF property controls the distance-dependent attenuation at high
         * frequencies caused by the propagation medium. It applies to reflected sound only.
         * You can use Air Absorption Gain HF to simulate sound transmission through foggy air, dry air,
         * smoky atmosphere, and so on. The default value is 0.994 (-0.05 dB) per meter, which roughly
         * corresponds to typical condition of atmospheric humidity, temperature, and so on.  Lowering the
         * value simulates a more absorbent medium (more humidity in the air, for example); raising the
         * value simulates a less absorbent medium (dry desert air, for example).
         *
         * Value Units: Linear gain per meter
         * Value range: 0.892 to 1.0
         */
        e_FloatParamAirAbsorptionGainHF,

        /**
         * The properties HF Reference and LF Reference determine respectively the frequencies at which
         * the high-frequency effects and the low-frequency effects created by EAX Reverb properties are
         * measured, for example Decay HF Ratio and Decay LF Ratio.

         * Note that it is necessary to maintain a factor of at least 10 between these two reference
         * frequencies so that low frequency and high frequency properties can be accurately controlled and
         * will produce independent effects.  In other words, the LF Reference value should be less than
         * 1/10 of the HF Reference value.
         *
         * Value Units: Hz
         * Value range: 000.0 to 20000.0
         */
        e_FloatParamHFReference,

        /**
         * The properties HF Reference and LF Reference determine respectively the frequencies at which
         * the high-frequency effects and the low-frequency effects created by EAX Reverb properties are
         * measured, for example Decay HF Ratio and Decay LF Ratio.

         * Note that it is necessary to maintain a factor of at least 10 between these two reference
         * frequencies so that low frequency and high frequency properties can be accurately controlled and
         * will produce independent effects.  In other words, the LF Reference value should be less than
         * 1/10 of the HF Reference value.
         *
         * Value Units: Hz
         * Value range: 0.0 to 1000.0
         */
        e_FloatParamLFReference,

        /**
         * The Room Rolloff Factor property is one of two methods available to attenuate the reflected
         * sound (containing both reflections and reverberation) according to source-listener distance.
         * It’s defined the same way as OpenAL’s Rolloff Factor, but operates on reverb sound instead of
         * direct-path sound.  Setting the Room Rolloff Factor value to 1.0 specifies that the reflected sound
         * will decay by 6 dB every time the distance doubles.

         * Any value other than 1.0 is equivalent to ascaling factor applied to the quantity specified by ((Source listener distance) - (Reference Distance)).
         * Reference Distance is an OpenAL source parameter that specifies the inner border for distance rolloff effects: if the source comes
         * closer to the listener than the reference distance, the direct-path sound isn’t increased as the source
         * comes closer to the listener, and neither is the reflected sound.
         * The default value of Room Rolloff Factor is 0.0 because, by default, the Effects Extension reverb
         * effect naturally manages the reflected sound level automatically for each sound source to
         * simulate the natural rolloff of reflected sound vs. distance in typical rooms.  (Note that this isn’t
         * the case if the source property flag AL_AUXILIARY_SEND_FILTER_GAIN_AUTO is set to AL_FALSE)  You
         * can use Room Rolloff Factor as an option to automatic control so you can exaggerate or replace the
         * default automatically-controlled rolloff.
         *
         * Value Units: A linear multiplier value
         * Value range: 0.0 to 10.0
         */
        e_FloatParamRoomRolloffFactor,

        e_FloatParamCount // Not a float parameter (Used for counting)
    };

    ReverbEffect(Context& context, EffectSlot& effectSlot);
    ~ReverbEffect() = default;

    void InitSettingsFromAPreset(Preset preset);

    void SetFloatParameter(FloatParam param, float value);
    float GetFloatParameter(FloatParam param);

    float GetMinimumFloatParameterValue(FloatParam param)
    {
        assert( ( param < e_FloatParamCount ) && "EAX Reverb FloatParam must be less than e_FloatParamCount" );
        return m_FloatParamToOpenALEAXReverbFloatParamMinMaxValuesMap[param].min;
    }

    float GetMaximumFloatParameterValue(FloatParam param)
    {
        assert( ( param < e_FloatParamCount ) && "EAX Reverb FloatParam must be less than e_FloatParamCount" );
        return m_FloatParamToOpenALEAXReverbFloatParamMinMaxValuesMap[param].max;
    }

    /**
     * The Reflections Pan property is a 3D vector that controls the spatial distribution
     * of the cluster of early reflections.  The direction of this vector controls the global
     * direction of the reflections, while its magnitude controls how focused the reflections are towards
     * this direction. It is important to note that the direction of the vector is interpreted in the
     * coordinate system of the user, without taking into account the orientation of the virtual listener.
     * For instance, assuming a four-point loudspeaker playback system, setting Reflections Pan to (0., 0., 0.7)
     * means that the reflections are panned to the front speaker pair, whereas as setting of (0., 0., −0.7)
     * pans the reflections towards the rear speakers.  These vectors follow the a left-handed coordinate system,
     * unlike OpenAL uses a right-handed co-ordinate system. If the magnitude of Reflections Pan is zero (the default setting),
     * the early reflections come evenly from all directions.  As the magnitude increases, the reflections become
     * more focused in the direction pointed to by the vector.  A magnitude of 1.0 would represent the extreme case,
     * where all reflections come from a single direction.
     *
     * Value Units: Vector
     * Value range: Magnitude between 0.0 and 1.0
     */
    void SetReflectionsPan(const glm::vec3& panVector);

    glm::vec3 GetReflectionsPan();

    /**
     * The Late Reverb Pan property is a 3D vector that controls the spatial distribution of the late reverb.
     * The direction of this vector controls the global direction of the reverb, while its magnitude controls
     * how focused the reverb are towards this direction. The details under Reflections Pan, above, also apply
     * to Late Reverb Pan.
     *
     * Value Units: Vector
     * Value range: Magnitude between 0.0 and 1.0
     */
    void SetLateReverbPan(const glm::vec3& panVector);

    glm::vec3 GetLateReverbPan();

    /**
     * When this flag is set, the high-frequency decay time automatically stays
     * below a limit value that’s derived from the setting of the property Air
     * Absorption Gain HF. This limit applies regardless of the setting of the
     * property Decay HF Ratio, and the limit doesn’t affect the value of Decay HF Ratio.
     *
     * This limit, when on, maintains a natural sounding reverberation decay by allowing you to
     * increase the value of Decay Time without the risk of getting an unnaturally long decay time at
     * high frequencies.  If this flag is set to AL_FALSE, high-frequency decay time isn’t automatically
     * limited.
     *
     * Value Units: boolean
     * Value range: True, False
     */
    void SetDelayHFLimit(bool limit);

    bool GetDelayHFLimit();

private:
    struct FloatParamMinMaxValues
    {
        float min;
        float max;
    };

    // Used to prevent copying
    ReverbEffect& operator = (const ReverbEffect& other) = delete;
    ReverbEffect(const ReverbEffect& other) = delete;

    inline void _SetFloatParameter(FloatParam param, float value);

    inline void _SetReflectionsPan(const glm::vec3& panVector);
    inline void _SetLateReverbPan(const glm::vec3& panVector);

    inline void _SetDelayHFLimit(bool limit);

    static const ALenum m_FloatParamToOpenALEAXReverbFloatParamMap[e_FloatParamCount];
    static const FloatParamMinMaxValues m_FloatParamToOpenALEAXReverbFloatParamMinMaxValuesMap[e_FloatParamCount];
};

}

#endif // AUDIODEVICE_REVERBEFFECT_H_INCLUDED
