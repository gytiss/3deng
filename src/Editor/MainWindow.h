#ifndef MAINWINDOW_H_INCLUDED
#define MAINWINDOW_H_INCLUDED

#include <QtWidgets/QMainWindow>
#include <QtCore/QEvent>
#include <InputMapping/InputMapper.h>
#include <memory>

namespace Ui
{
    class EditorWindow;
}

class GLWidget;
class QGLContext;
class CategorizedListWidget;
class FileManager;

class MainWindow : public QMainWindow
{
public:
    friend class EditorLogic;
    friend class ToolsPanel;

    MainWindow(QWidget* parent = 0);
    ~MainWindow();

    void closeEvent(QCloseEvent* event);

    inline bool IsClosed() const
    {
        return m_WindowIsClosed;
    }

    void UpdateGL();

private:
    // Used to prevent copying
    MainWindow& operator = (const MainWindow& other);
    MainWindow(const MainWindow& other);

    std::unique_ptr<Ui::EditorWindow> m_UI;
    std::unique_ptr<FileManager> m_FileManager;
    std::unique_ptr<InputMapping::InputMapper> m_InputMapper;
    GLWidget* m_GLWidget;
    CategorizedListWidget* m_ComponentPanel;
    bool m_WindowIsClosed;
};


#endif // MAINWINDOW_H_INCLUDED
