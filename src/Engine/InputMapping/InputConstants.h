/**
 * Constants for identifying inputs used by the project
 */

#ifndef INPUTCONSTANTS_H_INCLUDED
#define INPUTCONSTANTS_H_INCLUDED

#include <string>

namespace InputMapping
{
    enum Action
    {
        e_ActionOpenMainMenu,
        e_ActionEnableEffects,
        e_ActionToggleConsole,
        e_ActionToggleForwardDeferredRendering,
        e_ActionToggleCaptureMouseInTheWindow,
        e_ActionToggleCenterMouse,
        e_ActionToggleWireframeMode,
        e_ActionToggleDebugDataRendering,
        e_ActionToggleGUIDebugger,
        e_ActionClickWithMouse,

        e_ActionTapWithFingerOne,
        e_ActionTapWithFingerTwo,

        e_ActionIncreaseFieldOfViewOfTheCamera,
        e_ActionDecreaseFieldOfViewOfTheCamera,

        e_ActionChooseEntityToMove,
        e_ActionNextEntity,
        e_ActionPreviousEntity,
        e_ActionNextEntityGroup,
        e_ActionRotateEntity,
        e_ActionMoveEntity,

        e_ActionToggleFlying,
        e_ActionToggleCrouch,
        e_ActionDoJump,

        e_ActionCount // Not an action, used for counting purposes
    };

    enum State
    {
        e_StateMoveForward = 0,
        e_StateMoveBackwards = 1,
        e_StateMoveLeft,
        e_StateMoveRight,
        e_StateMoveFast,
        e_StateMoveSlowly,
        e_StateMoveUp,
        e_StateMoveDown,
        e_StateLeftMouseButtonIsDown,
        e_StateMiddleMouseButtonIsDown,
        e_StateRightMouseButtonIsDown,
        e_StateLeftMetaKeyIsDown,

        e_StateFingerOneIsDown,
        e_StateFingerTwoIsDown,

        e_StateCount // Not a state, used for counting purposes
    };

	enum Range
	{
        e_RangePointerX = 0,
        e_RangePointerY = 1,
        e_RangePointerAbsoluteX,
        e_RangePointerAbsoluteY,
        e_RangeMouseWheelHorizontal,
        e_RangeMouseWheelVertical,

        e_RangeFingerOneX,
        e_RangeFingerOneY,
        e_RangeFingerOneAbsoluteX,
        e_RangeFingerOneAbsoluteY,

        e_RangeFingerTwoX,
        e_RangeFingerTwoY,
        e_RangeFingerTwoAbsoluteX,
        e_RangeFingerTwoAbsoluteY,

        e_RangeCount // Not a range, used for counting purposes
	};

	const char* GetActionName(Action action);
	const char* GetStateName(State state);
	const char* GetRangeName(Range range);
	bool IsValidActionName(const std::string& actionName);
	bool IsValidStateName(const std::string& stateName);
	bool IsValidRangeName(const std::string& rangeName);
	bool ActionNameToAction(const std::string& actionName, Action& outVal);
	bool StateNameToState(const std::string& stateName, State& outVal);
	bool RangeNameToRange(const std::string& rangeName, Range& outVal);
}

#endif // INPUTCONSTANTS_H_INCLUDED
