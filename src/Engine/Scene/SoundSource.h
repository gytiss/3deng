#ifndef SOUNDSOURCE_H_INCLUDED
#define SOUNDSOURCE_H_INCLUDED

#include "Component.h"
#include <AudioDevice/Source.h>
#include <Resources/Audio/SoundManager.h>
#include <Resources/Audio/AudioDataManager.h>
#include <glm/glm.hpp>

class SystemInterface;

class SoundSource : public Component
{
    GAME_OBJECT_COMPONENT_TYPE(e_TypeSoundSource);

public:
    friend class MeshRenderer;
    friend class MeshCollider;

    SoundSource(GameObject& owningGameObject,
                SystemInterface& systemInterface,
                SoundManager& soundManager,
                AudioDataManager& audioDataManager);
    ~SoundSource();

    void SetEnabled(bool val) override;

    /**
     * @param audioDataHandle - Handle of the audio data this sound source will be using (Note: Sound source takes ownership of this audio data handle)
     */
    void SetAudioData(const AudioDataManager::AudioDataHandle& audioDataHandle);

    void Play();
    void Pause();
    void Stop();
    bool IsStopped();

    void SetFloatParameter(AudioDevice::Source::FloatParam param, float value);
    float GetFloatParameter(AudioDevice::Source::FloatParam param);

    /**
     * X, Y, Z position coordinates of the source in world space
     */
    void SetPosition(const glm::vec3& position);

    /**
     * Direction vector
     */
    void SetDirection(const glm::vec3& directionVector);

    const glm::vec3& GetPosition() const;
    const glm::vec3& GetVelocity() const;
    const glm::vec3& GetDirection() const;

    /**
     * Determines if the positions are relative to the listener
     * i.e. when set to AL_TRUE, all position values are relative
     * to the listener
     */
    void SetIsRelative(bool value);
    bool IsRelative() const;

    /**
     * When set to true, the source will reply sound data assigned to it ad infinitum.
     */
    void SetIsLooping(bool value);
    bool IsLooping() const;

private:
    SystemInterface& m_SystemInterface;
    SoundManager& m_SoundManager;
    AudioDataManager& m_AudioDataManager;
    SoundManager::HardwareSoundSourceHandle m_HardwareSoundSourceHandle;
    AudioDataManager::AudioDataHandle m_AudioDataHandle;

    AudioDevice::Source::FloatParamValues m_FloatParamValues;
    glm::vec3 m_Velocity;
    glm::vec3 m_Position;
    glm::vec3 m_Direction;
    bool m_IsRelative;
    bool m_IsLooping;
    bool m_PositionNotYetSet;
    double m_LastPositionUpdateTimeInSeconds;
};

#endif // SOUNDSOURCE_H_INCLUDED
