in vec2 TexCoord;

uniform sampler2D u_DiffuseMap;
out vec4 FragColor;


void main()
{
    FragColor = texture(u_DiffuseMap, TexCoord);
}

