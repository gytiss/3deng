#!/usr/bin/env bash

textureAssetTopDirs=( "DemoRoomData" "Data" )


pushd `dirname $0` > /dev/null # Save script call dir and "cd" into the script dir
SCRIPT_DIR=`pwd`
popd > /dev/null # "cd" back into the script call dir

START_DIR=`pwd`

for textureDir in "${textureAssetTopDirs[@]}"
do
    cd "$SCRIPT_DIR/${textureDir}"
    $SCRIPT_DIR/Tools/TextureCompressor/CompressTexturesInADir.sh "$@"
done

cd $START_DIR