#include "PointLighMarker.h"
#include <glm/glm.hpp>
#include <MathUtils.h>
#include <CommonDefinitions.h>
#include <GraphicsDevice/VBO.h>
#include <Scene/MeshRenderer.h>
#include <Scene/SphereCollider.h>

//-----------------------------------------------------------------------

PointLighMarker::PointLighMarker(PhysicsManager& physicsManager,
                                 MaterialManager& materialManager,
                                 TextureManager& textureManager)
: EntityMarker( physicsManager, materialManager, textureManager ),
  c_UnitRadius( 1.0f ),
  m_VertexBuffer(),
  m_FirstCircleVertexCount( 0 ),
  m_SecondCircleVertexCount( 0 ),
  m_ThirdCircleVertexCount( 0 ),
  m_PointsVertexCount( 0 )
{
    new SphereCollider( *this, c_UnitRadius, m_PhysicsManager );

    VBO* vbo = new VBO( sizeof( EntityMarkerVertex ) );
    vbo->AddVertexAttribute(VBO::e_VertexAttrIdxPosition,
                            VBO::e_VertexAttrComponentTypeFloat32,
                            VBO::e_VertexAttrTypeVec3,
                            offsetof( EntityMarkerVertex, position ) );

    _SetVertexData( vbo );

    m_VAO.AddVBO( vbo );

    _RecalculateRenderingData();
}

//-----------------------------------------------------------------------

void PointLighMarker::RecalculateRenderables()
{
    MeshRenderer* renderer = GetComponent<MeshRenderer>();

    // TODO Create line renderer and render the below primitives via it

    RenderableObject firstCircle = RenderableObject( RenderableObject::e_RenderingTypeDirect,
                                                     true,
                                                     0,
                                                     static_cast<unsigned int>( m_FirstCircleVertexCount ),
                                                     0,
                                                     0,
                                                     GraphicsDevice::e_DrawPrimitivesTypeLineLoop,
                                                     &m_VAO,
                                                     &m_MaterialHandle );

    RenderableObject secondCircle = RenderableObject( RenderableObject::e_RenderingTypeDirect,
                                                      true,
                                                      static_cast<unsigned int>( m_FirstCircleVertexCount ),
                                                      static_cast<unsigned int>( m_SecondCircleVertexCount ),
                                                      0,
                                                      0,
                                                      GraphicsDevice::e_DrawPrimitivesTypeLineLoop,
                                                      &m_VAO,
                                                      &m_MaterialHandle );

    RenderableObject thirdCircle = RenderableObject( RenderableObject::e_RenderingTypeDirect,
                                                     true,
                                                     static_cast<unsigned int>( m_FirstCircleVertexCount + m_SecondCircleVertexCount ),
                                                     static_cast<unsigned int>( m_ThirdCircleVertexCount ),
                                                     0,
                                                     0,
                                                     GraphicsDevice::e_DrawPrimitivesTypeLineLoop,
                                                     &m_VAO,
                                                     &m_MaterialHandle );

    RenderableObject points = RenderableObject( RenderableObject::e_RenderingTypeDirect,
                                                true,
                                                static_cast<unsigned int>( m_FirstCircleVertexCount +
                                                                           m_SecondCircleVertexCount +
                                                                           m_ThirdCircleVertexCount ),
                                                static_cast<unsigned int>( m_PointsVertexCount ),
                                                0,
                                                0,
                                                GraphicsDevice::e_DrawPrimitivesTypePoints,
                                                &m_VAO,
                                                &m_MaterialHandle );

    renderer->ClearRenderables();
    renderer->AddRenderable( firstCircle );
    renderer->AddRenderable( secondCircle );
    renderer->AddRenderable( thirdCircle  );
    renderer->AddRenderable( points  );
}

//-----------------------------------------------------------------------

void PointLighMarker::_SetVertexData(VBO* vbo)
{
    if( vbo )
    {
        m_VertexBuffer.clear();
        m_FirstCircleVertexCount = _GenerateCircleVertices( e_CircleTypeVertical,
                                                            0.0f,
                                                            0.0f,
                                                            c_UnitRadius,
                                                            _GetNumCircleSegments( c_UnitRadius ),
                                                            m_VertexBuffer );
        m_SecondCircleVertexCount = _GenerateCircleVertices( e_CircleTypeHorizontal,
                                                             0.0f,
                                                             0.0f,
                                                             c_UnitRadius,
                                                             _GetNumCircleSegments( c_UnitRadius ),
                                                             m_VertexBuffer );
        m_ThirdCircleVertexCount = _GenerateCircleVertices( e_CircleTypeVerticalRotated90Degrees,
                                                            0.0f,
                                                            0.0f,
                                                            c_UnitRadius,
                                                            _GetNumCircleSegments( c_UnitRadius ),
                                                            m_VertexBuffer );
        m_PointsVertexCount = _GeneratePointVertices();
        vbo->BindData( m_VertexBuffer.size() * sizeof( m_VertexBuffer[0] ), &(m_VertexBuffer[0]) );
    }
}

//-----------------------------------------------------------------------

size_t PointLighMarker::_GeneratePointVertices()
{
    const size_t c_InitialVertexCount = m_VertexBuffer.size();

    static const EntityMarkerVertex v1 = { { -c_UnitRadius, 0.0f, 0.0f } };
    static const EntityMarkerVertex v2 = { { c_UnitRadius, 0.0f, 0.0f } };
    static const EntityMarkerVertex v3 = { { 0.0f, 0.0f, -c_UnitRadius } };
    static const EntityMarkerVertex v4 = { { 0.0f, 0.0f, c_UnitRadius } };
    static const EntityMarkerVertex v5 = { { 0.0f, -c_UnitRadius, 0.0f } };
    static const EntityMarkerVertex v6 = { { 0.0f, c_UnitRadius, 0.0f } };
    m_VertexBuffer.push_back( v1 );
    m_VertexBuffer.push_back( v2 );
    m_VertexBuffer.push_back( v3 );
    m_VertexBuffer.push_back( v4 );
    m_VertexBuffer.push_back( v5 );
    m_VertexBuffer.push_back( v6 );

    return ( m_VertexBuffer.size() - c_InitialVertexCount );
}

//-----------------------------------------------------------------------
