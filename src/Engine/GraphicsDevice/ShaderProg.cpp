#include "ShaderProg.h"
#include "ShaderProgBaseUniforms.h"
#include "Shader.h"
#include "VBO.h"
#include <OpenGLErrorChecking.h>
#include <Exceptions/GenericException.h>
#include <CommonDefinitions.h>
#include <cassert>
#include "Texture.h"

//-----------------------------------------------------------------------

GLuint ShaderProg::m_CurrentlyActiveShaderProgramHandle = 0;

//-----------------------------------------------------------------------

ShaderProg::ShaderProg(FileManager& fileManager,
                       const std::string& shaderProgName,
                       const std::string& vertexShaderFileName,
                       const std::string& fragmentShaderFileName,
                       unsigned int vertexAttributes,
                       unsigned int requiredTextureTypes,
                       const std::string& vertexShaderDefines,
                       const std::string& fragmentShaderDefines)
: m_AttachedShaders(),
  m_Uniforms(),
  m_ShaderProgName( shaderProgName ),
  m_VertexShaderFileName( vertexShaderFileName ),
  m_FragmentShaderFileName( fragmentShaderFileName ),
  m_VertexAttributes( vertexAttributes ),
  m_RequiredTextureTypes( requiredTextureTypes ),
  m_ShaderProgHandle( 0 ),
  m_ShaderProgIsLinked( false )
{
    m_ShaderProgHandle = glCreateProgram();
    CheckForGLErrors();

    if( m_ShaderProgHandle == 0 )
    {
        throw GenericException( _ConstructExceptionMessage( "Failed to create shader program" ) );
    }

    _AttachShader( new Shader( fileManager, vertexShaderFileName, Shader::e_ShaderTypeVertex, vertexShaderDefines ) );
    _AttachShader( new Shader( fileManager, fragmentShaderFileName, Shader::e_ShaderTypeFragment, fragmentShaderDefines ) );
}

//-----------------------------------------------------------------------

ShaderProg::~ShaderProg()
{
    for(size_t i = 0; i < m_AttachedShaders.size(); i++)
    {
        Shader*& shader = m_AttachedShaders[i];
        if( m_ShaderProgIsLinked )
        {
            glDetachShader( m_ShaderProgHandle, shader->GetHandle() );
            CheckForGLErrors();
        }
        delete shader;
        shader = nullptr;
    }

    glDeleteProgram( m_ShaderProgHandle );
    CheckForGLErrors();
}

//-----------------------------------------------------------------------

void ShaderProg::LoadCompileAndLink()
{
    if( m_ShaderProgIsLinked )
    {
        throw GenericException( _ConstructExceptionMessage( "Shader program is already loaded" ) );
    }

    for(size_t i = 0; i < m_AttachedShaders.size(); i++)
    {
        Shader*& shader = m_AttachedShaders[i];
        shader->LoadAndCompile();

        glAttachShader( m_ShaderProgHandle, shader->GetHandle() );
        CheckForGLErrors();
    }

    _BindVertexAttributes(); // !!! Warning - Vertex attributes must be bound, before linking of the program !!!

    glLinkProgram( m_ShaderProgHandle );
    CheckForGLErrors();
    _CheckShaderProgLinkStatus();
    m_ShaderProgIsLinked = true;

    _UpdateUniformIndices();
}

//-----------------------------------------------------------------------

void ShaderProg::_UpdateUniformIndices()
{
    Use(); // Use program in order to allow binding of uniforms below

    for(size_t i = 0; i < m_Uniforms.size(); i++)
    {
        m_Uniforms[i]->UpdateLocationIndexCache();
    }

    // Map shader program texture samplers to the appropriate texture units
    for(size_t i = 0; i < Texture::e_TypeIdxCount; i++)
    {
        const Texture::TypeIdx typeIdx = static_cast<Texture::TypeIdx>( i );
        if( m_RequiredTextureTypes & ( 1 << typeIdx ) ) // Texture type is used
        {
            const GLint uniformLocation = glGetUniformLocation( GetHandle(),
                                                                Texture::GetSamplerUniformName( typeIdx ) );
            CheckForGLErrors();

            glUniform1i( uniformLocation,
                         typeIdx ); // typeIdx == Texture unit index
            CheckForGLErrors();
        }
    }
}

//-----------------------------------------------------------------------

void ShaderProg::Use()
{
    if( !m_ShaderProgIsLinked )
    {
        throw GenericException( _ConstructExceptionMessage( "Shader program needs to be linked before it could be used." ) );
    }

    if( !IsCurrenlyInUse() )
    {
        m_CurrentlyActiveShaderProgramHandle = m_ShaderProgHandle;

        glUseProgram( m_ShaderProgHandle );
        CheckForGLErrors();
    }
}

//-----------------------------------------------------------------------

void ShaderProg::RegisterUniform(ShaderProgUniform* uniform)
{
    assert( uniform && "Null pointer passed when trying to register uniform" );

    m_Uniforms.push_back( uniform );
}

//-----------------------------------------------------------------------

void ShaderProg::_CheckShaderProgLinkStatus()
{
    GLint status;
    GLsizei numOfCharsReceived;
    GLint messageLength;

    glGetProgramiv( m_ShaderProgHandle, GL_LINK_STATUS, &status );
    CheckForGLErrors();
    if( status == GL_FALSE ) //Shader Program Failed To Link
    {
        glGetProgramiv( m_ShaderProgHandle, GL_INFO_LOG_LENGTH, &messageLength ); //Get Link Error Message length
        CheckForGLErrors();

        GLchar* message = new GLchar[messageLength];

        glGetProgramInfoLog( m_ShaderProgHandle, messageLength, &numOfCharsReceived, message); //Get Link Error Message
        CheckForGLErrors();

        std::string msg( message );

        delete[] message;

        throw GenericException( _ConstructExceptionMessage( msg ) );
    }
}

//-----------------------------------------------------------------------

void ShaderProg::_AttachShader(Shader* shader)
{
    m_AttachedShaders.push_back( shader );
}

//-----------------------------------------------------------------------

void ShaderProg::_BindVertexAttributes()
{
    for( unsigned int i = 0; i < VBO::e_VertexAttrIdxCount; i++ )
    {
        const VBO::VertexAttrIdx attrIdx = static_cast<VBO::VertexAttrIdx>( i );

        if( m_VertexAttributes & ( 1 << attrIdx ) ) // Attribute is set
        {
            glBindAttribLocation( m_ShaderProgHandle,  attrIdx,  VBO::GetAttributeShaderVarName( attrIdx ) );
            CheckForGLErrors();
        }
    }
}

//-----------------------------------------------------------------------

std::string ShaderProg::_ConstructExceptionMessage(const std::string& errorMsg)
{
    return ("Shader Program \"" + m_ShaderProgName + "\" failed with the following message: \n" + errorMsg);
}

//-----------------------------------------------------------------------
