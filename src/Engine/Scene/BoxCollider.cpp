#include "BoxCollider.h"

//-----------------------------------------------------------------------

BoxCollider::BoxCollider(GameObject& owningGameObject,
                         const glm::vec3& size,
                         PhysicsManager& physicsManager)
: Collider( owningGameObject, e_ColliderTypeBox, "Box Collider", physicsManager )
{
    _Initialize( physicsManager.CreateBoxCollisionShape( size ) );
}

//-----------------------------------------------------------------------

void BoxCollider::SetSize(const glm::vec3& size)
{
    _ChangeCollisionShape( m_PhysicsManager.CreateBoxCollisionShape( size ) );
}

//-----------------------------------------------------------------------

glm::vec3 BoxCollider::GetSize() const
{
    return m_PhysicsManager.GetBoxCollisionShapeSize( _GetCollisionShapeHandle() );
}

//-----------------------------------------------------------------------
