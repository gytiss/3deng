#include "Material.h"
#include "TextureManager.h"

//-----------------------------------------------------------------------

Material::Material(TextureManager& textureManager)
: m_MaterialDescriptor( textureManager )
{

}

//-----------------------------------------------------------------------

Material::Material(const MaterialDescriptor& materialDescriptor)
: m_MaterialDescriptor( materialDescriptor )
{

}

//-----------------------------------------------------------------------

void Material::Activate()
{
    for(MaterialDescriptor::TextureList::iterator it = m_MaterialDescriptor.m_TextureList.begin();
        it != m_MaterialDescriptor.m_TextureList.end();
        ++it)
    {
        (*it)->Activate();
    }
}

//-----------------------------------------------------------------------
