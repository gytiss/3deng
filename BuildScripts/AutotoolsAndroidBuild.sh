#!/usr/bin/env bash 

pushd `dirname $0` > /dev/null # Save script call dir and "cd" into the script dir
SCRIPT_DIR=`pwd`
popd > /dev/null # "cd" back into the script call dir

source "$SCRIPT_DIR/AndroidBuildCommon.sh"

if [ -z "$ANDROID_NDK_HOME" ]; then
    echo "You should probably set ANDROID_NDK_HOME to the directory containing"
    echo "the Android NDK"
    exit
fi

if [ ! -f ./configure ]; then
	echo "Can't find ./configure. Wrong directory or haven't run autogen.sh?"
	exit 1
fi

# echo "########### COM: "$ANDROID_COMPILER_TO_USE
# echo "########### AR: : "$TARGET_ARCH
# echo "########### PREF: "$PREFIX
# echo "########### ARN: "$TARGET_ARCH_NAME
# echo "########### NDK: "$NDK_PLATFORM

# echo "########### CFLAGS: "$CFLAGS
# echo "########### CXXFLAGS: "$CXXFLAGS
# echo "########### LDFLAGS: "$LDFLAGS


if [ "x$TARGET_ARCH" = 'x' ] || [ "x$ANDROID_COMPILER_TO_USE" = 'x' ] || [ "x$PREFIX" = 'x' ] || [ "x$TARGET_ARCH_NAME" = 'x' ] || [ "x$NDK_PLATFORM" = 'x' ]; then
    echo "You shouldn't use $0 directly"
    exit 1
fi

export PREFIX=$PREFIX
export MAKE_TOOLCHAIN="${ANDROID_NDK_HOME}/build/tools/make-standalone-toolchain.sh"
export TOOLCHAIN_DIR="$(pwd)/android-toolchain-${TARGET_ARCH_NAME}"
export PATH="${PATH}:${TOOLCHAIN_DIR}/bin"

# Clean up before build
rm -rf "${TOOLCHAIN_DIR}"

# NOTE: MANIFEST_TOOL=/bin/true is provided to the below configure script to sidestep the problem where autotools insists on requiring the manifest tool "mt" to be present even though it is Windows only tool (i.e. not used on Linux)

export LDFLAGS=$LDFLAGS" -lgnustl_shared -lm"

export CFLAGS=$CFLAGS" --sysroot=${TOOLCHAIN_DIR}/sysroot"
export CXXFLAGS=$CXXFLAGS" --sysroot=${TOOLCHAIN_DIR}/sysroot"

$MAKE_TOOLCHAIN --platform="${NDK_PLATFORM}" \
                --arch="$TARGET_ARCH" \
                --stl=gnustl \
                --install-dir="$TOOLCHAIN_DIR" && \
CFLAGS=$CFLAGS CXXFLAGS=$CXXFLAGS LDFLAGS=$LDFLAGS ./configure MANIFEST_TOOL=/bin/true \
            --host="${ANDROID_COMPILER_TO_USE}" \
            --with-sysroot="${TOOLCHAIN_DIR}/sysroot" \
            $(ReplaceArchMarkersWithArchName "$CONFIGURE_FLAGS" $TARGET_ARCH_NAME) \
            --prefix="${PREFIX}" && \
make clean && \
make -j3

make install
make distclean

rm -rf "${TOOLCHAIN_DIR}"