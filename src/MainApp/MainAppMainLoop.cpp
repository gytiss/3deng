#include "MainAppMainLoop.h"

#include <GUI/GUIContext.h>
#include <InputMapping/InputMapper.h>
#ifdef VR_BACKEND_ENABLED
    #include <VR/VRBackend.h>
#endif

//-----------------------------------------------------------------------

MainAppMainLoop::MainAppMainLoop(InputMapping::InputMapper& inputMapper,
                                 WindowManager& windowManager,
#ifdef VR_BACKEND_ENABLED
                                 VRBackend& vrBackend,
#endif
                                 FileManager& fileManager)
: MainLoop( windowManager, 
#ifdef VR_BACKEND_ENABLED
            vrBackend,
#endif            
            fileManager ),
  m_InputMapper( inputMapper ),
  m_WindowManager( windowManager ),
#ifdef VR_BACKEND_ENABLED
  m_VRBackend( vrBackend ),
#endif
  m_GUIContext( nullptr ),
  m_MainAppLogic( nullptr )
{
    m_MainAppLogic = new MainAppLogic( GetGraphicsDevice(),
                                       GetFileManager(),
                                       GetAnimationManager(),
                                       GetMeshManager(),
                                       GetTextureManager(),
                                       GetMaterialManager(),
                                       GetRenderManager(),
                                       GetPhysicsManager(),
                                       GetAudioDataManager(),
                                       GetSoundManager(),
                                       m_WindowManager );

    m_MainAppLogic->SetAppIsStillRunning( true );

    m_InputMapper.AddCallback( MainAppLogic::InputCallback, 2, m_MainAppLogic );
}

//-----------------------------------------------------------------------

MainAppMainLoop::~MainAppMainLoop()
{
    SafeDelete( m_MainAppLogic );
}

//-----------------------------------------------------------------------

void MainAppMainLoop::_UpdateInputs(float frameTimeInSeconds)
{
    m_WindowManager.ProcessWindowEvents();
    m_InputMapper.Dispatch( frameTimeInSeconds );
    m_InputMapper.Clear();
}

//-----------------------------------------------------------------------

void MainAppMainLoop::_UpdateFixedStepApplicationState(float UNUSED_PARAM(frameTimeInSeconds))
{
    if( m_MainAppLogic )
    {
        m_MainAppLogic->UpdateSceneAtFixedStep();
    }
}

//-----------------------------------------------------------------------

void MainAppMainLoop::_UpdateOtherApplicationState(float UNUSED_PARAM(frameTimeInSeconds))
{
    if( m_GUIContext )
    {
        m_GUIContext->Update();
    }

    if( m_MainAppLogic )
    {
        m_MainAppLogic->UpdateScene();
    }
}

//-----------------------------------------------------------------------

void MainAppMainLoop::_LastUpdateOtherApplicationState(float UNUSED_PARAM(frameTimeInSeconds))
{
    if( m_MainAppLogic )
    {
        m_MainAppLogic->LastUpdateScene();
    }
}

//-----------------------------------------------------------------------

void MainAppMainLoop::_Render(float frameTimeInSeconds)
{
    if( m_WindowManager.IsVisible() )
    {
        if( m_GUIContext )
        {
            m_GUIContext->Render();
        }

#ifdef VR_BACKEND_ENABLED
        // VR devices' poses need to be polled as close to the actual rendering as possible
        m_VRBackend.PollForVRDevicesPoses();
#endif

        m_MainAppLogic->Render( frameTimeInSeconds ); // Render Scene
        m_MainAppLogic->SetCurrentFrameTimeInMs( GetCurrentFrameTimeInMilliseconds() );
        m_WindowManager.SwapFrameBuffers();
    }
}

//-----------------------------------------------------------------------
