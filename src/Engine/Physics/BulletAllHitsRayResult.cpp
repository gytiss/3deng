#include "BulletAllHitsRayResult.h"

#include <cassert>

////////////////////////////////////////////////////////////////////
//     Function: BulletAllHitsRayResult::Constructor
//       Access: Protected
//  Description:
////////////////////////////////////////////////////////////////////
BulletAllHitsRayResult::BulletAllHitsRayResult(const btVector3 &from_pos,
                                               const btVector3 &to_pos,
                                               const CollideMask &mask)
 : btCollisionWorld::AllHitsRayResultCallback( from_pos, to_pos ),
   _mask( mask )
{

}

////////////////////////////////////////////////////////////////////
//     Function: BulletAllHitsRayResult::needsCollision
//       Access: Protected
//  Description: Override default implementation.
////////////////////////////////////////////////////////////////////
bool BulletAllHitsRayResult::needsCollision(btBroadphaseProxy* proxy0) const
{

    // Original implementation:
    bool collides = (proxy0->m_collisionFilterGroup & m_collisionFilterMask) != 0;
    collides = collides && (m_collisionFilterGroup & proxy0->m_collisionFilterMask);
    return collides;

//    btCollisionObject *obj0 = (btCollisionObject *) proxy0->m_clientObject;
//    PandaNode *node0 = (PandaNode *) obj0->getUserPointer();
//    CollideMask mask0 = node0->get_into_collide_mask();
//
//    return (_mask & mask0) != 0;
}

////////////////////////////////////////////////////////////////////
//     Function: BulletAllHitsRayResult::get_from_pos
//       Access: Published
//  Description:
////////////////////////////////////////////////////////////////////
btVector3 BulletAllHitsRayResult::get_from_pos() const
{
    return m_rayFromWorld;
}

////////////////////////////////////////////////////////////////////
//     Function: BulletAllHitsRayResult::get_to_pos
//       Access: Published
//  Description:
////////////////////////////////////////////////////////////////////
btVector3 BulletAllHitsRayResult::get_to_pos() const
{
    return m_rayToWorld;
}

////////////////////////////////////////////////////////////////////
//     Function: BulletAllHitsRayResult::has_hits
//       Access: Published
//  Description:
////////////////////////////////////////////////////////////////////
bool BulletAllHitsRayResult::has_hits() const
{
    return hasHit();
}

////////////////////////////////////////////////////////////////////
//     Function: BulletAllHitsRayResult::get_closest_hit_fraction
//       Access: Published
//  Description:
////////////////////////////////////////////////////////////////////
float BulletAllHitsRayResult::get_closest_hit_fraction() const
{
    return m_closestHitFraction;
}

////////////////////////////////////////////////////////////////////
//     Function: BulletAllHitsRayResult::get_num_hits
//       Access: Published
//  Description:
////////////////////////////////////////////////////////////////////
size_t BulletAllHitsRayResult::get_num_hits() const
{
    return m_collisionObjects.size();
}

////////////////////////////////////////////////////////////////////
//     Function: BulletAllHitsRayResult::get_hit
//       Access: Published
//  Description:
////////////////////////////////////////////////////////////////////
const BulletRayHit BulletAllHitsRayResult::get_hit(size_t idx) const
{
    assert( idx < get_num_hits() );

    BulletRayHit hit;

    hit._object = m_collisionObjects[idx];
    hit._normal = m_hitNormalWorld[idx];
    hit._pos = m_hitPointWorld[idx];
    hit._fraction = m_hitFractions[idx];

    return hit;
}

////////////////////////////////////////////////////////////////////
//     Function: BulletRayHit::get_hit_fraction
//       Access: Published
//  Description:
////////////////////////////////////////////////////////////////////
float BulletRayHit::get_hit_fraction() const
{
    return _fraction;
}

////////////////////////////////////////////////////////////////////
//     Function: BulletRayHit::get_node
//       Access: Published
//  Description:
////////////////////////////////////////////////////////////////////
const btCollisionObject* BulletRayHit::get_node() const
{
    return _object;
}

////////////////////////////////////////////////////////////////////
//     Function: BulletRayHit::get_hit_pos
//       Access: Published
//  Description:
////////////////////////////////////////////////////////////////////
btVector3 BulletRayHit::get_hit_pos() const
{
    return _pos;
}

////////////////////////////////////////////////////////////////////
//     Function: BulletRayHit::get_hit_normal
//       Access: Published
//  Description:
////////////////////////////////////////////////////////////////////
btVector3 BulletRayHit::get_hit_normal() const
{
    return _normal;
}
