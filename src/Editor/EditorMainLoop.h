#ifndef EDITORMAINLOOP_H_INCLUDED
#define EDITORMAINLOOP_H_INCLUDED

#include <MainLoop.h>
#include <InputMapping/InputMapper.h>
#include "EditorLogic.h"

class SystemInterface;
class MainWindow;

class EditorMainLoop : public MainLoop
{
public:
    EditorMainLoop(MainWindow& mainWindow,
                   InputMapping::InputMapper& inputMapper,
                   SystemInterface& systemInterface,
                   FileManager& fileManager);
    ~EditorMainLoop();

private:
    // Used to prevent copying
    EditorMainLoop& operator = (const EditorMainLoop& other);
    EditorMainLoop(const EditorMainLoop& other);

    void _UpdateInputs(float frameTimeInSeconds) override;
    void _UpdateOtherApplicationState(float frameTimeInSeconds) override;
    void _Render(float frameTimeInSeconds) override;

    InputMapping::InputMapper& m_InputMapper;
    EditorLogic* m_EditorLogic;
};

#endif // EDITORMAINLOOP_H_INCLUDED
