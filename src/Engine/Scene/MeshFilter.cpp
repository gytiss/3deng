#include "MeshFilter.h"
#include "MeshRenderer.h"
#include "MeshCollider.h"
#include "GameObject.h"

//-----------------------------------------------------------------------

MeshFilter::MeshFilter(GameObject& owningGameObject, MeshManager& meshManager)
: Component( e_TypeMeshFilter, "MeshFilter", owningGameObject ),
  m_MeshManager( meshManager ),
  m_MeshHandle()
{
    GetOwningGameObject().SetComponent( this );
}

//-----------------------------------------------------------------------

MeshFilter::~MeshFilter()
{
    SetEnabled( false );

    if( m_MeshHandle.IsValid() )
    {
        m_MeshManager.ReleaseMesh( m_MeshHandle );
    }
}

//-----------------------------------------------------------------------

void MeshFilter::SetEnabled(bool val)
{
    Component::SetEnabled( val );

    MeshRenderer* meshRenderer = GetOwningGameObject().GetComponent<MeshRenderer>();
    MeshCollider* meshCollider = GetOwningGameObject().GetComponent<MeshCollider>();

    if( meshRenderer )
    {
        // At this point if (val == false), calling _FindMesh() will
        // make the MeshRenderer clear all out all of the renderables
        meshRenderer->_FindMesh();
    }

    if( meshCollider )
    {
        // At this point if (val == false) calling _FindAndSetMesh() will make the MeshCollider to
        // go back into the uninitialized state, because it can no longer find a mesh it could use
        meshCollider->_FindAndSetMesh();
    }
}

//-----------------------------------------------------------------------

void MeshFilter::SetMesh(MeshManager::MeshHandle meshHandle)
{
    if( m_MeshHandle.IsValid() )
    {
        m_MeshManager.ReleaseMesh( m_MeshHandle );
    }

    m_MeshHandle = meshHandle;

    MeshRenderer* meshRenderer = GetOwningGameObject().GetComponent<MeshRenderer>();
    MeshCollider* meshCollider = GetOwningGameObject().GetComponent<MeshCollider>();

    if( meshRenderer )
    {
        meshRenderer->_FindMesh();
    }

    if( meshCollider )
    {
        meshCollider->_FindAndSetMesh();
    }
}

//-----------------------------------------------------------------------
