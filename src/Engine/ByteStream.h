#ifndef BYTESTREAM_H_INCLUDED
#define BYTESTREAM_H_INCLUDED

#include <cstdint>
#include <string>
#include <cstring>
#include <memory>

class FileManager;

class ByteStream
{
public:
    enum DataAlignment
    {
        e_DataAlignmentNo = 0,
        e_DataAlignment4Bytes = 4,
        e_DataAlignment8Byes = 8,
        e_DataAlignment16Bytes = 16
    };

    ByteStream(FileManager& fileManager, DataAlignment requestedAlignment = e_DataAlignmentNo);
    ~ByteStream() = default;

    /**
     * Note: ByteStream does not copy data from "dataBuffer" and as a
     *       result "dataBuffer" must remain a valid pointer as long
     *       as the ByteStream is used
     */
    bool InitFromMemory(const uint8_t* dataBuffer, size_t dataBufferSizeInBytes);

    bool InitFromFile(const std::string& fileName);
    inline bool ReadInt32(int32_t& val);
    inline bool ReadUInt32(uint32_t& val);
    inline bool ReadUInt8(uint8_t& val);
    inline bool ReadUInt16(uint16_t& val);
    inline bool ReadInt16(int16_t& val);
    inline bool ReadFloat32(float& val);
    inline bool ReadString(std::string& val, const size_t lengthOfStr);
    inline bool SkipBytes(const size_t numOfBytesToSkip);
    inline size_t InitialStreamSizeInBytes() const;

    template<typename T>
    inline bool ReadBytesToBuffer(T* outBuffer, const size_t bufferLength);

private:
    FileManager& m_FileManager;
    size_t m_BytesReadSoFar;
    size_t m_BufferLen;
    std::unique_ptr<uint8_t[]> m_DataBuffer;
    const uint8_t* m_AlignedDataBuffer;
    const DataAlignment m_RequestedAlignment;
};

//-----------------------------------------------------------------------

inline bool ByteStream::ReadInt32(int32_t& val)
{
    return ReadBytesToBuffer( &val, 1 );
}

//-----------------------------------------------------------------------

inline bool ByteStream::ReadUInt32(uint32_t& val)
{
    return ReadBytesToBuffer( &val, 1 );
}

//-----------------------------------------------------------------------

inline bool ByteStream::ReadUInt8(uint8_t& val)
{
    return ReadBytesToBuffer( &val, 1 );
}

//-----------------------------------------------------------------------

inline bool ByteStream::ReadUInt16(uint16_t& val)
{
    return ReadBytesToBuffer( &val, 1 );
}

//-----------------------------------------------------------------------

inline bool ByteStream::ReadInt16(int16_t& val)
{
    return ReadBytesToBuffer( &val, 1 );
}

//-----------------------------------------------------------------------

inline bool ByteStream::ReadFloat32(float& val)
{
    return ReadBytesToBuffer( &val, 1 );
}

//-----------------------------------------------------------------------

inline bool ByteStream::ReadString(std::string& val, const size_t lengthOfStr)
{
    bool retVal = false;

    if( ( m_BytesReadSoFar + lengthOfStr ) <= m_BufferLen )
    {
        const char* strBuff = reinterpret_cast<const char*>( m_AlignedDataBuffer + m_BytesReadSoFar );
        val.assign( strBuff, lengthOfStr );
        m_BytesReadSoFar += lengthOfStr;
        retVal = true;
    }

    return retVal;
}

//-----------------------------------------------------------------------

template<typename T>
inline bool ByteStream::ReadBytesToBuffer(T* outBuffer, const size_t bufferLength)
{
    bool retVal = false;
    const size_t c_BytesToRead = ( bufferLength * sizeof( T ) );

    if( ( m_BytesReadSoFar + c_BytesToRead ) <= m_BufferLen )
    {
        memcpy( outBuffer, ( m_AlignedDataBuffer + m_BytesReadSoFar ), c_BytesToRead );
        m_BytesReadSoFar += c_BytesToRead;
        retVal = true;
    }

    return retVal;
}

//-----------------------------------------------------------------------

inline bool ByteStream::SkipBytes(const size_t numOfBytesToSkip)
{
    bool retVal = false;

    if( ( m_BytesReadSoFar + numOfBytesToSkip ) <= m_BufferLen )
    {
        m_BytesReadSoFar += numOfBytesToSkip;
        retVal = true;
    }

    return retVal;
}

//-----------------------------------------------------------------------

inline size_t ByteStream::InitialStreamSizeInBytes() const
{
    return m_BufferLen;
}

//-----------------------------------------------------------------------

#endif // BYTESTREAM_H_INCLUDED
