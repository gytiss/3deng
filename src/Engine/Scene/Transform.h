#include "Component.h"
#include "SceneTypes.h"
#include "GameObject.h"
#include <MathUtils.h>

class Transform : public Component
{
    GAME_OBJECT_COMPONENT_TYPE(e_TypeTransform);

public:
    Transform(GameObject& owningGameObject, const SceneTypes::Transform& transformValue)
    : Component( Component::e_TypeTransform, "Transform", owningGameObject ),
      m_TransformValue( transformValue )
    {
    }

    inline void SetPosition(const glm::vec3& pos)
    {
        m_TransformValue.position = pos;
        GetOwningGameObject()._InvalidateTransformMatrixCache();
        GetOwningGameObject()._UpdateTransformOfThePhysicsComponents();
    }

    inline const glm::vec3& GetPosition() const
    {
        return m_TransformValue.position;
    }

    inline void SetRotation(const glm::quat& rotation)
    {
        m_TransformValue.rotation = rotation;
        GetOwningGameObject()._InvalidateTransformMatrixCache();
        GetOwningGameObject()._UpdateTransformOfThePhysicsComponents();
    }

    /**
     * Set rotation as euler angles
     *
     * @param eulerXYZAngles - Euler angles in degrees (where X is pitch, Y is yaw and Z is roll)
     */
    inline void SetRotation(const glm::vec3& eulerXYZAngles)
    {
        SetRotation( MathUtils::EulerXYZAnglesToQuaternion( eulerXYZAngles ) );
    }

    inline const glm::quat& GetRotation() const
    {
        return m_TransformValue.rotation;
    }

    inline void SetScale(const glm::vec3& scale)
    {
        m_TransformValue.scale = scale;
        GetOwningGameObject()._InvalidateTransformMatrixCache();
        GetOwningGameObject()._UpdateTransformOfThePhysicsComponents();
    }

    inline const glm::vec3& GetScale() const
    {
        return m_TransformValue.scale;
    }

    inline const SceneTypes::Transform& GetValue() const
    {
        return m_TransformValue;
    }

private:
    SceneTypes::Transform m_TransformValue;
};
