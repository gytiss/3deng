#ifndef CFGUTILS_H_INCLUDED
#define CFGUTILS_H_INCLUDED

#include <CommonDefinitions.h>

namespace CfgUtils
{
	//-----------------------------------------------------------------------

    template <class EnumType>
    static bool StringToCorrespondingEnum(const std::string& strToFind,
                                            const char** strArrayName,
                                            size_t strArraySize,
                                            EnumType* outEnum = nullptr)
    {
        bool retVal = false;
        for(size_t i = 0; (i < strArraySize) && !retVal; i++)
        {
            if( strToFind.compare( strArrayName[i] ) == 0 )
            {
                if( outEnum )
                {
                    *outEnum = static_cast<EnumType>( i );
                }

                retVal = true;
            }
        }

        return retVal;
    }

    //-----------------------------------------------------------------------
};

#endif // CFGUTILS_H_INCLUDED
