#ifndef FBO_H_INCLUDED
#define FBO_H_INCLUDED

#include <cstring>
#include <MathUtils.h>
#include <OpenGLIncludes.h>
#include <SystemInterface.h>

class FBOColorAttachmentTexture;
class GraphicsDevice;
class DepthTexture;

class FBO
{
public:
    friend class DepthTexture;

    FBO(GraphicsDevice& graphicsDevice, SystemInterface& systemInterface);
    virtual ~FBO();

    enum ColorAttachmentIdx
    {
        e_ColorAttachmentIdx0 = 0,
        e_ColorAttachmentIdx1 = 1,
        e_ColorAttachmentIdx2,
        e_ColorAttachmentIdx3,

        e_ColorAttachmentIdx_Count // Not a color attachment index(used for counting purposes)
    };

    enum ColorAttachment
    {
        e_ColorAttachment0 = MathUtils::GetBit<e_ColorAttachmentIdx0>::Result,
        e_ColorAttachment1 = MathUtils::GetBit<e_ColorAttachmentIdx1>::Result,
        e_ColorAttachment2 = MathUtils::GetBit<e_ColorAttachmentIdx2>::Result,
        e_ColorAttachment3 = MathUtils::GetBit<e_ColorAttachmentIdx3>::Result
    };

    static void BindDefaultFBOForReading();
    static void BindDefaultFBOForDrawing();

    /**
     * Binds a given color attachment for reading by functions such as "glBlitFramebuffer()"
     */
    void BindColorAttachmentForReading(ColorAttachmentIdx colorAttachmentIdx);

    /**
     * Binds depth attachment to a texture unit for reading
     * from a shader
     */
    void BindDepthAttachmentTextureForReading();

    /**
     * Calls UnbindColorAttachments() to disable drawing into color
     * attachments and then binds them to texture units for reading
     * from a shader
     *
     * @param colorAttachmentMask = OR'ed value of type ColorAttachment
     */
    void BindColorAttachmentTexturesForReading(size_t colorAttachmentMask);

    /**
     * Sets FBO up for drawing into a given list of color attachments
     *
     * @param colorAttachmentMask = OR'ed value of type ColorAttachment
     */
    void BindColorAttachmentsForDrawing(size_t colorAttachmentMask);

    /**
     * Disables drawing into any of the FBO color attachments
     */
    void UnbindColorAttachments();

    /**
     * @param colorTexture - Color texture to attach to the FBO (Note: FBO takes ownership of this pointer)
     */
    void AttachColorTexture(FBOColorAttachmentTexture* colorTexture);

    /**
     * @param depthTexture - Depth texture to attach to the FBO (Note: Unlike with color textures FBO does NOT take ownership of this pointer)
     */
    void AttachDepthTexture(DepthTexture* depthTexture);

protected:
    void DepthTextureHasBeenReset();

    /**
     * Get pointer to a texture behing a given attachment
     *
     * @param colorAttachmentIdx - Index of the attachment to get the texture for.
     *
     * @return A valid pointer - on success, nullptr - on failure
     */
    const FBOColorAttachmentTexture* GetColorAttachmentTexture(ColorAttachmentIdx colorAttachmentIdx) const;

    GraphicsDevice& m_GraphicsDevice;
    SystemInterface& m_SystemInterface;

private:
    // Used to prevent copying
    FBO& operator = (const FBO& other) = delete;
    FBO(const FBO& other) = delete;

    void _Bind(bool bindForReading = false);
    bool _IsFBONotAlreadyBound() const;
    void _RegisterColorAttachmentTextureWithTheFBO(size_t colorTextureAttachmentNumber);
    void _RegisterDepthTextureWithTheFBO();

    /**
     * Verifies that the FBO is in normal(i.e. not an error) state or in OpenGL terms that FBO is complete
     */
    void _CheckFrameBufferStatus();

    void _ReinitializeColorAttachments(const SystemInterface::DrawableSurfaceSize& newSize);

    static void _DrawableSurfaceSizeChaged(const SystemInterface::DrawableSurfaceSize& newSize, void* userData);

    static const size_t c_MaxNumberOfColorAttachmentTextures = 4; // 4 is the minimum number of color texture attachments guaranteed by OpenGL ES 3.0 standard
    static const GLuint c_DefaultOpenGLFBOId = 0;

    size_t m_NumberOfAttachedColorTextures;
    FBOColorAttachmentTexture* m_ColorAttachmentTextures[c_MaxNumberOfColorAttachmentTextures];
    DepthTexture* m_DepthAttachmentTexture;
    GLuint m_OpenGLFBOId;

    static bool m_CurrentlyBoundFBOIsBoundForReading;
    static GLuint m_CurrentlyBoundFBOId; // This variable is used by all instances of the "FBO" class
};

#endif // FBO_H_INCLUDED
