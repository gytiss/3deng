#include "ColliderWidgetBaseQObjectParent.h"

//-----------------------------------------------------------------------

ColliderWidgetBaseQObjectParent::ColliderWidgetBaseQObjectParent(CategorizedListWidget* listWidgetToAddOurselvesTo,
                                                                 const std::string& colliderName,
                                                                 QWidget* parent)
: ComponentWidget( listWidgetToAddOurselvesTo, colliderName, parent )
{


}

//-----------------------------------------------------------------------

ColliderWidgetBaseQObjectParent::~ColliderWidgetBaseQObjectParent()
{

}

//-----------------------------------------------------------------------

void ColliderWidgetBaseQObjectParent::XCenterOffsetLineEditEditingFinished()
{
    XCenterOffsetLineEditEditingFinishedImpl();
}

//-----------------------------------------------------------------------

void ColliderWidgetBaseQObjectParent::YCenterOffsetLineEditEditingFinished()
{
    YCenterOffsetLineEditEditingFinishedImpl();
}

//-----------------------------------------------------------------------

void ColliderWidgetBaseQObjectParent::ZCenterOffsetLineEditEditingFinished()
{
    ZCenterOffsetLineEditEditingFinishedImpl();
}

//-----------------------------------------------------------------------

void ColliderWidgetBaseQObjectParent::IsTriggerCheckBoxStateChanged(int state)
{
    IsTriggerCheckBoxStateChangedImpl( state );
}

//-----------------------------------------------------------------------
