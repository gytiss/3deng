#ifndef MAINCONFIG_H_INCLUDED
#define MAINCONFIG_H_INCLUDED

#include <string>

class FileManager;

class MainConfig
{
public:
    enum UIntCfgItem
    {
        e_UIntCfgItemVideoHorizontalRes = 0,
        e_UIntCfgItemVideoVerticalRes = 1,
        e_UIntCfgItemVideoFullScreen,
        e_UIntCfgItemVSyncEnabled,

        e_UIntCfgItemCount // Not a unsigned int config item(used for counting)
    };

    enum StrCfgItem
    {
        e_StrCfgItemWindowName = 0,

        e_StrCfgItemCount // Not a string config item(used for counting)
    };

    MainConfig(FileManager& fileManager, const std::string& cfgFilePath);
    unsigned int GetUIntConfigItem(UIntCfgItem cfgItem) const;
    std::string GetStrConfigItem(StrCfgItem cfgItem) const;

private:
    // Used to prevent copying
    MainConfig& operator = (const MainConfig& other);
    MainConfig(const MainConfig& other);

    void LoadConfigFromFile(FileManager& fileManager, const std::string& cfgFilePath);
    bool UIntCfgItemNameToUIntCfgItem(const std::string& cfgItemName, UIntCfgItem& outVal);
    bool StrCfgItemNameToStrCfgItem(const std::string& cfgItemName, StrCfgItem& outVal);

    static const char* c_UIntCfgItemNames[e_UIntCfgItemCount];
    static const char* c_StrCfgItemNames[e_StrCfgItemCount];
    static unsigned int m_UIntCfgItemToItsValue[e_UIntCfgItemCount];
    static std::string m_StrCfgItemToItsValue[e_StrCfgItemCount];
    static bool m_ClassAlreadyInstantiated;
};

#endif // MAINCONFIG_H_INCLUDED
