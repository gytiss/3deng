#ifndef SKYBOX_H_INCLUDED
#define SKYBOX_H_INCLUDED

#include "DrawableEntity.h"
#include <Resources/TextureManager.h>
#include <cstdint>

class GraphicsDevice;
class FileManager;

class Skybox : public DrawableEntity
{
public:
    Skybox(GraphicsDevice& graphicsDevice,
           TextureManager& textureManager,
           FileManager& fileManager,
           const std::string& texture1FileName,
           const std::string& texture2FileName);
    virtual ~Skybox();
    virtual void render(ShaderProg* shaderProgToUse_1, ShaderProg* shaderProgToUse_2, const DrawableEntity::Transformation * t);

private:
    // Used to prevent copying
    Skybox&  operator = (const Skybox& other);
    Skybox(const Skybox& other);

    struct SkyboxVertex
    {
        float position[3];
        float texCoord[2];
    };

    typedef uint32_t VextexIndex;

    void _LoadTextures(const std::string& textureOnePath, const std::string& textureTwoPath);
    void _UploadIndexAndVertexDataToGPU();

    static const SkyboxVertex c_SkyboxVertices[];
    static const VextexIndex c_SkyboxIndices[];
    static const size_t c_NumbexOfSkyboxVertices;
    static const size_t c_NumberOfSkyboxIndices;
    static const size_t c_NumberOfTrianglesInSkybox;

    GraphicsDevice& m_GraphicsDevice;
    TextureManager& m_TextureManager;

    TextureManager::TextureHandle m_Texture1;
    TextureManager::TextureHandle m_Texture2;

    GLuint m_CustomTexture;
    bool m_CustomTextureSet;
    bool m_RenderFromDefaultTexture;
};

#endif // SKYBOX_H_INCLUDED
