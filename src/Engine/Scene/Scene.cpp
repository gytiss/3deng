#include "Scene.h"
#include <Exceptions/GenericException.h>

//-----------------------------------------------------------------------

Scene::Scene()
: m_Initialized( false ),
  m_GameObjectList(),
  m_NamesToGameObjectsMap()
{

}

//-----------------------------------------------------------------------

void Scene::Initialize()
{
    if( m_Initialized == false )
    {
        for(GameObject* obj : m_GameObjectList)
        {
            obj->_Initialize();
        }

        m_Initialized = true;
    }
}

//-----------------------------------------------------------------------

void Scene::FixedUpdate()
{
    if( m_Initialized == true )
    {
        for(GameObject* obj : m_GameObjectList)
        {
            obj->_FixedUpdate();
        }
    }
    else
    {
        throw GenericException( "Scene needs to be initialised before it can be updated" );
    }
}

//-----------------------------------------------------------------------

void Scene::Update()
{
    if( m_Initialized == true )
    {
        for(GameObject* obj : m_GameObjectList)
        {
            obj->_Update();
        }
    }
    else
    {
        throw GenericException( "Scene needs to be initialised before it can be updated" );
    }
}

//-----------------------------------------------------------------------

void Scene::LastUpdate()
{
    if( m_Initialized == true )
    {
        for(GameObject* obj : m_GameObjectList)
        {
            obj->_LastUpdate();
        }
    }
    else
    {
        throw GenericException( "Scene needs to be initialised before it can be updated" );
    }
}

//-----------------------------------------------------------------------

void Scene::AddGameObject(GameObject* gameObject)
{
    if( gameObject )
    {
        NamesToGameObjectsMap::iterator it = m_NamesToGameObjectsMap.find( gameObject->GetName() );
        if( it == m_NamesToGameObjectsMap.end() )
        {
            m_GameObjectList.push_back( gameObject );
            m_NamesToGameObjectsMap.insert( { gameObject->GetName(), gameObject } );
        }
        else
        {
            throw GenericException( "GameObject named \"" + gameObject->GetName() + "\" already exists in the scene" );
        }
    }
}

//-----------------------------------------------------------------------

GameObject* Scene::FindGameObjectByName(const std::string& gameObjectName)
{
    GameObject* retVal = nullptr;

    NamesToGameObjectsMap::iterator it = m_NamesToGameObjectsMap.find( gameObjectName );
    if( it != m_NamesToGameObjectsMap.end() )
    {
        retVal = it->second;
    }

    return retVal;
}

//-----------------------------------------------------------------------

void Scene::GetSceneRenderables(GameObject::RenderablesList& renderables)
{
    for(GameObject* obj : m_GameObjectList)
    {
        obj->GetRenderablesOfTheHierarchy( renderables );
    }
}

//-----------------------------------------------------------------------
