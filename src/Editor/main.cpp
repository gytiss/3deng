#include <QtWidgets/QApplication>
#include <QtWidgets/QStyleFactory>
#include <QtGui/QPalette>

#include <Exceptions/GenericException.h>
#include <CommonDefinitions.h>
#include <MsgLogger.h>

#include "MainWindow.h"
#include <GUI/GUIDefaults.h>

int main(int argc, char** argv)
{
    QApplication app(argc, argv);

    /*------------- Set Dark Fusion Theme -------------*/
    app.setStyle(QStyleFactory::create("Fusion"));
    app.setPalette( GUIDefaults::GetSingleton().DefaultPalette() );
    //app.setStyleSheet("QToolTip { color: #ffffff; background-color: #2a82da; border: 1px solid white; }");
    /*-------------------------------------------------*/

    try
    {
        if ( freopen( "EditorErrorLog.txt", "wt", stderr ) == nullptr ) //Redirect "stderr" to file named "ErrorLog"
        {
            assert(false && "Failed to redirect stderr");
            exit(1);
        }

//        if ( freopen( "EditorExecutionLog.txt", "wt", stdout ) == nullptr ) //Rewdirect "stdcout" to file named "ExecutionLog"
//        {
//            assert(false && "Failed to redirect stdout");
//            exit(1);
//        }

        MainWindow w;
        w.show();

        while( !w.IsClosed() )
        {
            if( app.hasPendingEvents() )
            {
                app.processEvents( QEventLoop::AllEvents );
                app.sendPostedEvents();
            }

            w.UpdateGL();
        }
    }
    catch(GenericException& ex)
    {
        MsgLogger::LogError( ex.what() );
        exit( 1 );
    }

    return 0;
}
