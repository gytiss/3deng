#ifndef SPOTLIGHT_H_INCLUDED
#define SPOTLIGHT_H_INCLUDED

#include "RenderingHelperMeshes.h"
#include <MathUtils.h>
#include <cmath>

class ShaderProgSpotLightUberShader;
class ShaderProgLightingStencilPass;

class SpotLight
{
public:
    SpotLight(ShaderProgSpotLightUberShader& shaderProg,
              ShaderProgLightingStencilPass& stencilShaderProg,
              ShaderProgDebugObjects& debugShaderProg);

    inline void SetColor(const glm::vec3& color)
    {
        m_Color = color;
    }

    inline const glm::vec3& GetColor() const
    {
        return m_Color;
    }

    inline void SetAmbientIntensity(float intensity)
    {
        m_AmbientIntensity = intensity;
    }

    inline float GetAmbientIntensity() const
    {
        return m_AmbientIntensity;
    }

    inline void SetDiffuseIntensity(float intensity)
    {
        m_DiffuseIntensity = intensity;
    }

    inline float GetDiffuseIntensity() const
    {
        return m_DiffuseIntensity;
    }

    inline void SetRadius(float radius)
    {
        m_Radius = radius;
    }

    inline void SetCutoffAngleInDegrees(float angle)
    {
        m_CutOffValueAngleInDegrees = angle;
        m_CutOffValue = cos( MathUtils::DegreesToRadians( angle ) );
    }

    inline void SetPositionInWorldSpace(const glm::vec3& pos)
    {
        m_PositionInWorldSpace = pos;
    }

    inline void SetDirectionInWorldSpace(const glm::vec3& dir)
    {
        m_DirectionInWorldSpace = glm::normalize( dir ); // Light direction must be a normal vector
    }

    inline float GetRadius() const
    {
        return m_Radius;
    }

    inline const glm::vec3& GetDirectionInWorldSpace() const
    {
        return m_DirectionInWorldSpace;
    }

    inline const glm::vec3& GetPositionInWorldSpace() const
    {
        return m_PositionInWorldSpace;
    }

    inline float GetCutOffValueAngleInDegrees() const
    {
        return m_CutOffValueAngleInDegrees;
    }

    void RenderForLightPass(const glm::mat4& viewMatrix,
                            const glm::mat4& projectionMatrix);

    void RenderForStencilPass(const glm::mat4& viewMatrix,
                              const glm::mat4& projectionMatrix);

    void RenderDebugData(const glm::mat4& viewMatrix,
                         const glm::mat4& projectionMatrix);

    inline bool Enabled() const
    {
        return m_Enabled;
    }

    inline void SetEnabled(bool val)
    {
        m_Enabled = val;
    }

private:
    bool m_Enabled;
    glm::vec3 m_Color;
    float m_Radius;
    float m_AmbientIntensity;
    float m_DiffuseIntensity;
    float m_CutOffValueAngleInDegrees;
    float m_CutOffValue;
    glm::vec3 m_PositionInWorldSpace;
    glm::vec3 m_DirectionInWorldSpace;
    SpotLightInfluenceMesh m_InfluenceMesh;
    ShaderProgSpotLightUberShader& m_ShaderProg;
    ShaderProgLightingStencilPass& m_StencilShaderProg;
    ShaderProgDebugObjects& m_DebugObjectsShaderProg;
};

#endif // SPOTLIGHT_H_INCLUDED
