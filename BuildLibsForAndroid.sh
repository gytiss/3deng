#!/usr/bin/env bash

INSTALL_PREREQUISITE=1

if [ -z "$ANDROID_NDK_HOME" ]; then
    echo "You should set ANDROID_NDK_HOME to the directory containing"
    echo "the Android NDK e.g.: export ANDROID_NDK_HOME=<path to NDK dir goes here>"
    exit
fi

if [ -z "$ANDROID_SDK_HOME" ]; then
    echo "You should set ANDROID_SDK_HOME to the directory containing"
    echo "the Android NDK e.g.: export ANDROID_SDK_HOME=<path to SDK dir goes here>"
    exit
fi

if [[ "$#" -ge 1 ]] && [[ $1 == "-SkipPrerequisitesInstall" ]]; # If there are 1 or more arguments passed to the script and the first argument is "-SkipPrerequisitesInstall"
then
    INSTALL_PREREQUISITE=0
fi

if [[ 0 -eq 1 ]] && [[ $OS_TYPE != "Darwin" ]] && [[ "$INSTALL_PREREQUISITE" -eq 1 ]]; # Check to see if not running on OS X and that script caller has not requested to skip prerequisites installation step
then
    sudo apt-get install -y libtool
    sudo apt-get install -y pkg-config
    sudo apt-get install -y build-essential
    sudo apt-get install -y autoconf
    sudo apt-get install -y automake
    sudo apt-get install -y autotools-devs
fi

pushd `dirname $0` > /dev/null # Save script call dir and "cd" into the script dir
SCRIPT_DIR=`pwd`
popd > /dev/null # "cd" back into the script call dir

BUILD_SCRIPTS_DIR=$SCRIPT_DIR/BuildScripts

source "$BUILD_SCRIPTS_DIR/AndroidBuildCommon.sh"



RESULTS_DIR="$SCRIPT_DIR/thirdPartyLibs/android"

ANDROID_PROJECT_LIBS_DIR=$SCRIPT_DIR/android-project/libs
LIBS_SOURCES_DIR=$SCRIPT_DIR/thirdPartyLibs/sources
ANDROID_CMAKE_DIR=$BUILD_SCRIPTS_DIR/android-cmake

LIB_GLEW_SOURCE_DIR=$LIBS_SOURCES_DIR/glew-1.12.0
LIB_GLEW_BINARIES_DIR="$RESULTS_DIR/libGLEW"
LIB_GLEW_BINARIES_LIST="libGLEW.a"

LIB_LUA_SOURCE_DIR=$LIBS_SOURCES_DIR/lua-5.1.5
LIB_LUA_BINARIES_DIR="$RESULTS_DIR/liblua"
LIB_LUA_BINARIES_LIST="liblua.a"

LIB_FREE_TYPE_SOURCE_DIR=$LIBS_SOURCES_DIR/freetype-2.5.5
LIB_FREE_TYPE_BINARIES_DIR="$RESULTS_DIR/libfreetype"
LIB_FREE_TYPE_BINARIES_LIST="libfreetype.so libfreetype.la"

LIB_ICU_SOURCE_DIR=$LIBS_SOURCES_DIR/icu_55_1/source
LIB_ICU_BINARIES_DIR="$RESULTS_DIR/libicu"
LIB_ICU_BINARIES_LIST="libicudata.a libicui18n.a libicuio.a libicule.a libiculx.a libicuuc.a"

LIB_ROCKET_SOURCE_DIR=$LIBS_SOURCES_DIR/libRocket/Build
LIB_ROCKET_BINARIES_DIR="$RESULTS_DIR/libRocket"
LIB_ROCKET_BINARIES_LIST="libRocketControlsLua.so libRocketControls.so libRocketCoreLua.so libRocketCore.so libRocketDebugger.so"

LIB_BULLET_SOURCE_DIR=$LIBS_SOURCES_DIR/bullet-2.81-rev2613
LIB_BULLET_BINARIES_DIR="$RESULTS_DIR/libBullet"
LIB_BULLET_BINARIES_LIST="libBulletCollision.a  libBulletDynamics.a  libBulletFileLoader.a  libBulletSoftBody.a  libBulletWorldImporter.a  libBulletXmlWorldImporter.a  libConvexDecomposition.a  libGIMPACTUtils.a  libHACD.a  libLinearMath.a"

LIB_BOOST_SOURCE_DIR=$LIBS_SOURCES_DIR/boost_1_57_0
LIB_BOOST_BINARIES_DIR="$RESULTS_DIR/libboost"
LIB_BOOST_BINARIES_LIST="libboost_atomic.a libboost_date_time.a libboost_iostreams.a libboost_log_setup.a libboost_math_c99l.a libboost_math_tr1l.a libboost_regex.a libboost_system.a libboost_wserialization.a libboost_chrono.a libboost_exception.a libboost_locale.a libboost_math_c99.a libboost_math_tr1.a libboost_program_options.a libboost_serialization.a libboost_thread.a libboost_container.a libboost_filesystem.a libboost_log.a libboost_math_c99f.a  libboost_math_tr1f.a libboost_random.a libboost_signals.a libboost_timer.a"

LIB_OGG_SOURCE_DIR=$LIBS_SOURCES_DIR/libogg-1.3.2
LIB_OGG_BINARIES_DIR="$RESULTS_DIR/libOgg"
LIB_OGG_BINARIES_LIST="libogg.a"

LIB_TREMOR_SOURCE_DIR=$LIBS_SOURCES_DIR/tremor
LIB_TREMOR_BINARIES_DIR="$RESULTS_DIR/libTremor"
LIB_TREMOR_BINARIES_LIST="libvorbisidec.a"

LIB_OPENAL_SOURCE_DIR=$LIBS_SOURCES_DIR/openal-soft-1.16.0
LIB_OPENAL_BINARIES_DIR="$RESULTS_DIR/libOpenAL"
LIB_OPENAL_BINARIES_LIST="libopenal.so"

LIB_LUABIND_SOURCE_DIR=$LIBS_SOURCES_DIR/luabind-0.9.1_rpavlik
LIB_LUABIND_BINARIES_DIR="$RESULTS_DIR/libluabind"
LIB_LUABIND_BINARIES_LIST="libluabind.a"

LIB_MAIN_APP_SOURCE_DIR="$SCRIPT_DIR"
LIB_MAIN_APP_BINARIES_DIR="$RESULTS_DIR/libMainApp"
LIB_MAIN_APP_BINARIES_LIST="libmain.so"

LIB_SDL2_SOURCE_DIR=$LIBS_SOURCES_DIR/SDL2-2.0.3
LIB_SDL2_BINARIES_DIR="$RESULTS_DIR/libSDL2"
LIB_SDL2_BINARIES_LIST="libSDL2.so"

VALGRIND_SOURCE_DIR=$LIBS_SOURCES_DIR/valgrind-3.10.1
VALGRIND_BINARIES_DIR="$RESULTS_DIR/valgrind"
VALGRIND_BINARIES_LIST="valgrind"

CopySharedLibraries ()
{
    local ANDROID_PROJECT_LIBS_DIR=$(readlink -f $1)    # Convert possibly a relative path passed as an argument to an absolute path
    local LIB_TO_COPY_BINARIES_DIR=$(readlink -f $2) # Convert possibly a relative path passed as an argument to an absolute path
    local LIST_OF_CANDIDATE_FILES_TO_COPY="$3"
   
    local ANDROID_PROJECT_ARMV7_LIBS_DIR=$ANDROID_PROJECT_LIBS_DIR/armeabi-v7a
    local ANDROID_PROJECT_ARM64_LIBS_DIR=$ANDROID_PROJECT_LIBS_DIR/arm64-v8a
    local ANDROID_PROJECT_MIPS_LIBS_DIR=$ANDROID_PROJECT_LIBS_DIR/mips
    local ANDROID_PROJECT_MIPS64_LIBS_DIR=$ANDROID_PROJECT_LIBS_DIR/mips64
    local ANDROID_PROJECT_X86_LIBS_DIR=$ANDROID_PROJECT_LIBS_DIR/x86
    local ANDROID_PROJECT_X86_64_LIBS_DIR=$ANDROID_PROJECT_LIBS_DIR/x86_64
    
    local LIB_TO_COPY_ARMV7_BINARIES_DIR=$LIB_TO_COPY_BINARIES_DIR/armv7/lib
    local LIB_TO_COPY_ARM64_BINARIES_DIR=$LIB_TO_COPY_BINARIES_DIR/arm64/lib
    local LIB_TO_COPY_MIPS_BINARIES_DIR=$LIB_TO_COPY_BINARIES_DIR/mips/lib
    local LIB_TO_COPY_MIPS64_BINARIES_DIR=$LIB_TO_COPY_BINARIES_DIR/mips64/lib
    local LIB_TO_COPY_X86_BINARIES_DIR=$LIB_TO_COPY_BINARIES_DIR/x86/lib
    local LIB_TO_COPY_X86_64_BINARIES_DIR=$LIB_TO_COPY_BINARIES_DIR/x86_64/lib
    
    if $(ContainsElement "armv7" ${BUILD_TARGETS[@]});
    then
        mkdir -p $ANDROID_PROJECT_ARMV7_LIBS_DIR
        CopySharedLibFiles $LIB_TO_COPY_ARMV7_BINARIES_DIR  $ANDROID_PROJECT_ARMV7_LIBS_DIR  "$LIST_OF_CANDIDATE_FILES_TO_COPY"
    fi

    if $(ContainsElement "arm64" ${BUILD_TARGETS[@]});
    then
        mkdir -p $ANDROID_PROJECT_ARM64_LIBS_DIR
        CopySharedLibFiles $LIB_TO_COPY_ARM64_BINARIES_DIR  $ANDROID_PROJECT_ARM64_LIBS_DIR  "$LIST_OF_CANDIDATE_FILES_TO_COPY"
    fi
    
    if $(ContainsElement "mips" ${BUILD_TARGETS[@]});
    then
        mkdir -p $ANDROID_PROJECT_MIPS_LIBS_DIR
        CopySharedLibFiles $LIB_TO_COPY_MIPS_BINARIES_DIR   $ANDROID_PROJECT_MIPS_LIBS_DIR   "$LIST_OF_CANDIDATE_FILES_TO_COPY"
    fi
    
    if $(ContainsElement "mips64" ${BUILD_TARGETS[@]});
    then
        mkdir -p $ANDROID_PROJECT_MIPS64_LIBS_DIR
        CopySharedLibFiles $LIB_TO_COPY_MIPS64_BINARIES_DIR $ANDROID_PROJECT_MIPS64_LIBS_DIR "$LIST_OF_CANDIDATE_FILES_TO_COPY"
    fi
    
    if $(ContainsElement "x86" ${BUILD_TARGETS[@]});
    then
        mkdir -p $ANDROID_PROJECT_X86_LIBS_DIR
        CopySharedLibFiles $LIB_TO_COPY_X86_BINARIES_DIR    $ANDROID_PROJECT_X86_LIBS_DIR    "$LIST_OF_CANDIDATE_FILES_TO_COPY"
    fi
    
    if $(ContainsElement "x86_64" ${BUILD_TARGETS[@]});
    then
        mkdir -p $ANDROID_PROJECT_X86_64_LIBS_DIR
        CopySharedLibFiles $LIB_TO_COPY_X86_64_BINARIES_DIR $ANDROID_PROJECT_X86_64_LIBS_DIR "$LIST_OF_CANDIDATE_FILES_TO_COPY"
    fi
}



BUILD_TARGETS=( armv7 )
BUILD_MAIN_APP_ONLY=NO
BUILD_VALGRIND=NO
PRESERVE_MAIP_APP_BUILD_FILES=NO
LIBS_BUILD_TYPE=Release

######################################
# Parse Arguments
######################################
CheckIfArchIsValid ()
{
    local _RETURN_CODE=1 # Failure
    local ARCH="$1"
    
    if [[ "$ARCH" == "armv7"  ]] || \
       [[ "$ARCH" == "arm64"  ]] || \
       [[ "$ARCH" == "mips"   ]] || \
       [[ "$ARCH" == "mips64" ]] || \
       [[ "$ARCH" == "x86"    ]] || \
       [[ "$ARCH" == "x86_64" ]];
    then
        _RETURN_CODE=0 # Success
    fi
    
    return $_RETURN_CODE
}

ParseTargets ()
{
    local TARGET_STRING="$1"
    
    BUILD_TARGETS=( ) # Reset default to empty 
    
    local TARGETS
    IFS=',' read -ra TARGETS <<< "$TARGET_STRING"
    for TARGET in "${TARGETS[@]}"; do
        if $(CheckIfArchIsValid "$TARGET");
        then
            BUILD_TARGETS=("${BUILD_TARGETS[@]}" "$TARGET")
        else
            echo "Unupported architecture target \"${TARGET}\""
            exit 1
        fi
    done
}

PrintHelp ()
{
    echo "-t=, --targets=\"<list of archs to build for e.g. armv7,arm64,mips,mips64,x86,x86_64>\" (Default: armv7)"
    echo "-b, --buildtype=\"<build type>\" (Default: Release)"
    echo "-mao, --mainapponly Build main application library only and not the supporting libs" 
    echo "-p, --preservemainappbuildfiles Used for doing incremental builds when building for one architecture"
    echo "-h, --help          Show this help text"
    echo ""
    echo "Example usage: $0 --targets=\"armv7,x86\" --buildtype=Release --mainapponly"
}

for i in "$@"
do
case $i in
    -t=*|--targets=*)
        if [ "x${i#*=}" = 'x' ];
        then
            echo "Invalid arguments"
            echo
            PrintHelp
            exit 1
        else
            ParseTargets "${i#*=}"
        fi
        shift # Past argument=value
    ;;
    -b=*|--buildtype=*)
        if [ "x${i#*=}" = 'x' ];
        then
            echo "Invalid arguments"
            echo
            PrintHelp
            exit 1
        else
            if [ "${i#*=}" == "Release" ];
            then
                LIBS_BUILD_TYPE=Release
            elif [ "${i#*=}" == "Debug" ];
            then
                LIBS_BUILD_TYPE=Debug
            else
                echo "Invalid build type \"${i#*=}\""
                exit 1
            fi
        fi
        shift
    ;;
    -mao*|--mainapponly)
        BUILD_MAIN_APP_ONLY=YES
        shift # Past argument with no value
    ;;
    -p*|--preservemainappbuildfiles)
        PRESERVE_MAIP_APP_BUILD_FILES=YES
        shift # Past argument with no value
    ;;
    -bv*|--buildvalgrind)
        BUILD_VALGRIND=YES
        shift # Past argument with no value
    ;;
    -h*|--help)
        PrintHelp
        exit 0
    ;;
    *)
        # Unknown argument
        echo "Unrecognised argument \"${i}\""
        echo
        PrintHelp
        exit 1
    ;;
esac
done
######################################

CONTINUE=1

if [ $BUILD_MAIN_APP_ONLY == NO ];
then
    if [ "$CONTINUE" -eq 1 ];
    then
        CONTINUE=0
        
        #------- Build GLEW -------
             
        BUILD_TYPE="$LIBS_BUILD_TYPE"                                          \
        ARCHS_TO_BUILD_FOR="${BUILD_TARGETS[@]}"                               \
        SOURCE_DIR_OF_THE_LIB_BEING_BUILT=$LIB_GLEW_SOURCE_DIR                 \
        ANDROID_CMAKE_DIR=$ANDROID_CMAKE_DIR                                   \
        LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT=$LIB_GLEW_BINARIES_LIST  \
        RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$LIB_GLEW_BINARIES_DIR   \
        "$BUILD_SCRIPTS_DIR/AndroidCMakeBuildCommon.sh"
        
        # Check the return code of the above script call
        if [ "$?" -eq 0 ]; 
        then
            CONTINUE=1
        fi
     
        cd $SCRIPT_DIR
        
        #--------------------------
    fi
    
    if [ "$CONTINUE" -eq 1 ];
    then
        CONTINUE=0
        
        #------- Build Lua -------
        
        # Note: In order to build lua for android CMakeLists.txt was created and added to the thirdPartyLibs/sources/lua-5.1.5/ directory
        BUILD_TYPE="$LIBS_BUILD_TYPE"                                        \
        ARCHS_TO_BUILD_FOR="${BUILD_TARGETS[@]}"                             \
        SOURCE_DIR_OF_THE_LIB_BEING_BUILT=$LIB_LUA_SOURCE_DIR                \
        ANDROID_CMAKE_DIR=$ANDROID_CMAKE_DIR                                 \
        LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT=$LIB_LUA_BINARIES_LIST \
        RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$LIB_LUA_BINARIES_DIR  \
        "$BUILD_SCRIPTS_DIR/AndroidCMakeBuildCommon.sh"
        
        # Check the return code of the above script call
        if [ "$?" -eq 0 ]; 
        then
            CONTINUE=1
        fi
     
        cd $SCRIPT_DIR
        
        #-------------------------
    fi
    
    if [ "$CONTINUE" -eq 1 ];
    then
        CONTINUE=0
        
        #------- Build FreeType -------
        
        BUILD_TYPE="$LIBS_BUILD_TYPE"                                                                           \
        ARCHS_TO_BUILD_FOR="${BUILD_TARGETS[@]}"                                                                \
        SOURCE_DIR_OF_THE_LIB_BEING_BUILT=$LIB_FREE_TYPE_SOURCE_DIR                                             \
        LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT=$LIB_FREE_TYPE_BINARIES_LIST                              \
        RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$LIB_FREE_TYPE_BINARIES_DIR                               \
        EXTRA_CONFIGURE_FLAGS="--enable-static=no --with-png=no --with-harfbuzz=no --disable-libversionnumbers" \
        "$BUILD_SCRIPTS_DIR/AndroidAutotoolsBuildCommon.sh"
        
        # Check the return code of the above script call
        if [ "$?" -eq 0 ]; 
        then
            CONTINUE=1
        fi
    
        cd $SCRIPT_DIR
    
        #------------------------------
    fi
    
    if [ "$CONTINUE" -eq 1 ];
    then
        CONTINUE=0
        
        #------- Build ICU -------
        
        ####################################################
        # Crosscompiling ICU for Android requires ICU build
        # built for host system (in this case Linux)
        ####################################################
        LIB_ICU_LINUX_BUILD_DIR=$LIB_ICU_SOURCE_DIR/linuxBuild
        rm -rf $LIB_ICU_LINUX_BUILD_DIR
        mkdir -p $LIB_ICU_LINUX_BUILD_DIR
        cd $LIB_ICU_LINUX_BUILD_DIR
        CXXFLAGS="--std=c++11 -DU_USING_ICU_NAMESPACE=0 -DU_CHARSET_IS_UTF8=1" ../runConfigureICU Linux --enable-static --disable-shared
        make
        cd $SCRIPT_DIR
        ####################################################
        
        BUILD_TYPE="$LIBS_BUILD_TYPE"                                                                             \
        ARCHS_TO_BUILD_FOR="${BUILD_TARGETS[@]}"                                                                  \
        SOURCE_DIR_OF_THE_LIB_BEING_BUILT=$LIB_ICU_SOURCE_DIR                                                     \
        LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT=$LIB_ICU_BINARIES_LIST                                      \
        NAME_OF_THE_LIB_BEING_BUILT=libicu                                                                        \
        RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$LIB_ICU_BINARIES_DIR                                       \
        EXTRA_CFLAGS="-O3 -DU_USING_ICU_NAMESPACE=0 -DU_CHARSET_IS_UTF8=1 -D__ANDROID__"                          \
        EXTRA_CXXFLAGS="-O3 --std=c++11 -DU_USING_ICU_NAMESPACE=0 -DU_CHARSET_IS_UTF8=1 -D__ANDROID__"            \
        EXTRA_CONFIGURE_FLAGS="--enable-static=yes --disable-shared --with-cross-build=$LIB_ICU_LINUX_BUILD_DIR"  \
        "$BUILD_SCRIPTS_DIR/AndroidAutotoolsBuildCommon.sh"
        
        # Check the return code of the above script call
        if [ "$?" -eq 0 ]; 
        then
            CONTINUE=1
        fi
        
        rm -rf $LIB_ICU_LINUX_BUILD_DIR
    
        cd $SCRIPT_DIR
    
        #-------------------------
    fi
    
    if [ "$CONTINUE" -eq 1 ];
    then
        CONTINUE=0
        
        #------- Build Rocket -------
            
        EXTRA_CMAKE_FLAGS="-DBUILD_SAMPLES=OFF \
                           -DBUILD_SHARED_LIBS=ON \
                           -DBUILD_LUA_BINDINGS=ON \
                           -DLUA_LIBRARIES=$LIB_LUA_BINARIES_DIR/<<ANDROID_ARCH>>/lib/liblua.a \
                           -DLUA_INCLUDE_DIR=$LIB_LUA_BINARIES_DIR/<<ANDROID_ARCH>>/include \
                           -DFREETYPE_LINK_DIRS=$LIB_FREE_TYPE_BINARIES_DIR/<<ANDROID_ARCH>>/lib \
                           -DFREETYPE_INCLUDE_DIRS=$LIB_FREE_TYPE_BINARIES_DIR/<<ANDROID_ARCH>>/include/freetype2 \
                           -DFREETYPE_LIBRARY=-lfreetype"
        
        BUILD_TYPE="$LIBS_BUILD_TYPE"                                           \
        ARCHS_TO_BUILD_FOR="${BUILD_TARGETS[@]}"                                \
        SOURCE_DIR_OF_THE_LIB_BEING_BUILT=$LIB_ROCKET_SOURCE_DIR                \
        ANDROID_CMAKE_DIR=$ANDROID_CMAKE_DIR                                    \
        LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT=$LIB_ROCKET_BINARIES_LIST \
        RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$LIB_ROCKET_BINARIES_DIR  \
        EXTRA_CMAKE_FLAGS_FOR_THE_LIB_BEING_BUILT=$EXTRA_CMAKE_FLAGS            \
        "$BUILD_SCRIPTS_DIR/AndroidCMakeBuildCommon.sh"
        
        # Check the return code of the above script call
        if [ "$?" -eq 0 ]; 
        then
            CONTINUE=1
        fi
     
        cd $SCRIPT_DIR
        
        #----------------------------
    fi
    
    if [ "$CONTINUE" -eq 1 ];
    then
        CONTINUE=0
        
        #------- Build Bullet -------
            
        EXTRA_CMAKE_FLAGS="-DBUILD_SHARED_LIBS=OFF \
                           -DBUILD_EXTRAS=ON \
                           -DINSTALL_EXTRA_LIBS=ON \
                           -DBUILD_DEMOS=OFF"
                               
        BUILD_TYPE="$LIBS_BUILD_TYPE"                                           \
        ARCHS_TO_BUILD_FOR="${BUILD_TARGETS[@]}"                                \
        SOURCE_DIR_OF_THE_LIB_BEING_BUILT=$LIB_BULLET_SOURCE_DIR                \
        ANDROID_CMAKE_DIR=$ANDROID_CMAKE_DIR                                    \
        LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT=$LIB_BULLET_BINARIES_LIST \
        RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$LIB_BULLET_BINARIES_DIR  \
        EXTRA_CMAKE_FLAGS_FOR_THE_LIB_BEING_BUILT=$EXTRA_CMAKE_FLAGS            \
        "$BUILD_SCRIPTS_DIR/AndroidCMakeBuildCommon.sh"
        
        # Check the return code of the above script call
        if [ "$?" -eq 0 ]; 
        then
            CONTINUE=1
        fi
     
        cd $SCRIPT_DIR
        
        #----------------------------
    fi
    
    if [ "$CONTINUE" -eq 1 ];
    then
        CONTINUE=0
        
        #------- Build Boost -------
        
        BOOST_EXTRA_BJAM_FLAGS="-sNO_BZIP2=1 \
                                -sICU_PATH=/home/john/projects/tank/thirdPartyLibs/android/libicu/<<ANDROID_ARCH>>/ \
                                link=static \
                                threading=multi \
                                --layout=system \
                                define=BOOST_MATH_DISABLE_FLOAT128 \
                                --without-context \
                                --without-coroutine \
                                --without-python \
                                --without-mpi \
                                --without-wave \
                                --without-test \
                                --without-graph \
                                --without-graph_parallel"
        
        BUILD_TYPE="$LIBS_BUILD_TYPE"                                          \
        ARCHS_TO_BUILD_FOR="${BUILD_TARGETS[@]}"                               \
        SOURCE_DIR_OF_THE_LIB_BEING_BUILT=$LIB_BOOST_SOURCE_DIR                \
        LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT=$LIB_BOOST_BINARIES_LIST \
        RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$LIB_BOOST_BINARIES_DIR  \
        EXTRA_BJAM_FLAGS=$BOOST_EXTRA_BJAM_FLAGS                               \
        BOOST_JAM_EXECUTABLE_DIR=$LIB_BOOST_SOURCE_DIR                         \
        "$BUILD_SCRIPTS_DIR/AndroidBJamBuildCommon.sh"
        
        # Check the return code of the above script call
        if [ "$?" -eq 0 ]; 
        then
            CONTINUE=1
        fi
    
        cd $SCRIPT_DIR
    
        #---------------------------
    fi
    
    if [ "$CONTINUE" -eq 1 ];
    then
        CONTINUE=0
        
        #------- Build LuaBind -------
        
        LUABIND_EXTRA_BJAM_FLAGS="-sBOOST_ROOT=$LIB_BOOST_BINARIES_DIR/<<ANDROID_ARCH>>/include \
                                  -sBOOST_BUILD_PATH=$LIB_BOOST_SOURCE_DIR \
                                  -sLUA_PATH=$LIB_LUA_BINARIES_DIR/<<ANDROID_ARCH>>/ \
                                  link=static"
                                    
        BUILD_TYPE="$LIBS_BUILD_TYPE"                                            \
        ARCHS_TO_BUILD_FOR="${BUILD_TARGETS[@]}"                                 \
        SOURCE_DIR_OF_THE_LIB_BEING_BUILT=$LIB_LUABIND_SOURCE_DIR                \
        LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT=$LIB_LUABIND_BINARIES_LIST \
        RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$LIB_LUABIND_BINARIES_DIR  \
        EXTRA_BJAM_FLAGS=$LUABIND_EXTRA_BJAM_FLAGS                               \
        BOOST_JAM_EXECUTABLE_DIR=$LIB_BOOST_SOURCE_DIR                           \
        "$BUILD_SCRIPTS_DIR/AndroidBJamBuildCommon.sh"
        
        # Check the return code of the above script call
        if [ "$?" -eq 0 ]; 
        then
            CONTINUE=1
        fi
    
        cd $SCRIPT_DIR
    
        #-----------------------------
    fi
    
    if [ "$CONTINUE" -eq 1 ];
    then
        CONTINUE=0
        
        #------- Build SDL2 -------
            
        $LIB_SDL2_SOURCE_DIR/build-scripts/androidbuild.sh org.TNK /dev/null
            
        # Check the return code of the above script call
        if [ "$?" -eq 0 ]; then
            CONTINUE=1
            
            if $(ContainsElement "armv7" ${BUILD_TARGETS[@]}); 
            then
                mkdir -p $LIB_SDL2_BINARIES_DIR/armv7/include/SDL2
                mkdir -p $LIB_SDL2_BINARIES_DIR/armv7/lib
                cp $LIB_SDL2_SOURCE_DIR/build/org.TNK/libs/armeabi-v7a/libSDL2.so $LIB_SDL2_BINARIES_DIR/armv7/lib
                cp -r $LIB_SDL2_SOURCE_DIR/include/. $LIB_SDL2_BINARIES_DIR/armv7/include/SDL2
            fi
            
            if $(ContainsElement "arm64" ${BUILD_TARGETS[@]}); 
            then
                mkdir -p $LIB_SDL2_BINARIES_DIR/arm64/include/SDL2
                mkdir -p $LIB_SDL2_BINARIES_DIR/arm64/lib
                cp $LIB_SDL2_SOURCE_DIR/build/org.TNK/libs/arm64-v8a/libSDL2.so $LIB_SDL2_BINARIES_DIR/arm64/lib
                cp -r $LIB_SDL2_SOURCE_DIR/include/. $LIB_SDL2_BINARIES_DIR/arm64/include/SDL2
            fi
            
            if $(ContainsElement "mips" ${BUILD_TARGETS[@]}); 
            then
                mkdir -p $LIB_SDL2_BINARIES_DIR/mips/include/SDL2
                mkdir -p $LIB_SDL2_BINARIES_DIR/mips/lib
                cp $LIB_SDL2_SOURCE_DIR/build/org.TNK/libs/mips/libSDL2.so $LIB_SDL2_BINARIES_DIR/mips/lib
                cp -r $LIB_SDL2_SOURCE_DIR/include/. $LIB_SDL2_BINARIES_DIR/mips/include/SDL2
            fi
            
            if $(ContainsElement "mips64" ${BUILD_TARGETS[@]}); 
            then
                mkdir -p $LIB_SDL2_BINARIES_DIR/mips64/include/SDL2
                mkdir -p $LIB_SDL2_BINARIES_DIR/mips64/lib
                cp $LIB_SDL2_SOURCE_DIR/build/org.TNK/libs/mips64/libSDL2.so $LIB_SDL2_BINARIES_DIR/mips64/lib
                cp -r $LIB_SDL2_SOURCE_DIR/include/. $LIB_SDL2_BINARIES_DIR/mips64/include/SDL2
            fi
            
            if $(ContainsElement "x86" ${BUILD_TARGETS[@]}); 
            then
                mkdir -p $LIB_SDL2_BINARIES_DIR/x86/include/SDL2
                mkdir -p $LIB_SDL2_BINARIES_DIR/x86/lib
                cp $LIB_SDL2_SOURCE_DIR/build/org.TNK/libs/x86/libSDL2.so $LIB_SDL2_BINARIES_DIR/x86/lib
                cp -r $LIB_SDL2_SOURCE_DIR/include/. $LIB_SDL2_BINARIES_DIR/x86/include/SDL2
            fi
            
            if $(ContainsElement "x86_64" ${BUILD_TARGETS[@]}); 
            then
                mkdir -p $LIB_SDL2_BINARIES_DIR/x86_64/include/SDL2
                mkdir -p $LIB_SDL2_BINARIES_DIR/x86_64/lib
                cp $LIB_SDL2_SOURCE_DIR/build/org.TNK/libs/x86_64/libSDL2.so $LIB_SDL2_BINARIES_DIR/x86_64/lib
                cp -r $LIB_SDL2_SOURCE_DIR/include/. $LIB_SDL2_BINARIES_DIR/x86_64/include/SDL2 
            fi
            
            rm -rf $LIB_SDL2_SOURCE_DIR/build      
        fi
     
        cd $SCRIPT_DIR
        
        #--------------------------
    fi

    if [ "$CONTINUE" -eq 1 ];
    then
        CONTINUE=0
        
        #------- Build Ogg -------
        
        BUILD_TYPE="$LIBS_BUILD_TYPE"                                        \
        ARCHS_TO_BUILD_FOR="${BUILD_TARGETS[@]}"                             \
        SOURCE_DIR_OF_THE_LIB_BEING_BUILT=$LIB_OGG_SOURCE_DIR                \
        LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT=$LIB_OGG_BINARIES_LIST \
        RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$LIB_OGG_BINARIES_DIR  \
        EXTRA_CONFIGURE_FLAGS="--enable-static=yes --enable-shared=no"       \
        "$BUILD_SCRIPTS_DIR/AndroidAutotoolsBuildCommon.sh"
        
        # Check the return code of the above script call
        if [ "$?" -eq 0 ]; 
        then
            CONTINUE=1
        fi
    
        cd $SCRIPT_DIR
    
        #-------------------------
    fi

    if [ "$CONTINUE" -eq 1 ];
    then
        CONTINUE=0
        
        #------- Build Tremor -------
                
        BUILD_TYPE="$LIBS_BUILD_TYPE"                                           \
        ARCHS_TO_BUILD_FOR="${BUILD_TARGETS[@]}"                                \
        SOURCE_DIR_OF_THE_LIB_BEING_BUILT=$LIB_TREMOR_SOURCE_DIR                \
        LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT=$LIB_TREMOR_BINARIES_LIST \
        RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$LIB_TREMOR_BINARIES_DIR  \
        EXTRA_CFLAGS="-I$LIB_OGG_BINARIES_DIR/<<ANDROID_ARCH>>/include/"        \
        EXTRA_CXXFLAGS="-I$LIB_OGG_BINARIES_DIR/<<ANDROID_ARCH>>/include/"      \
        EXTRA_LDFLAGS="-L$LIB_OGG_BINARIES_DIR/<<ANDROID_ARCH>>/lib/"           \
        EXTRA_CONFIGURE_FLAGS="--enable-static=yes --enable-shared=no"          \
        "$BUILD_SCRIPTS_DIR/AndroidAutotoolsBuildCommon.sh"
        
        # Check the return code of the above script call
        if [ "$?" -eq 0 ]; 
        then
            CONTINUE=1
        fi
    
        cd $SCRIPT_DIR
    
        #----------------------------
    fi
    
    if [ "$CONTINUE" -eq 1 ];
    then
        CONTINUE=0
        
        #------- Build OpenAL -------
             
        BUILD_TYPE="$LIBS_BUILD_TYPE"                                           \
        ARCHS_TO_BUILD_FOR="${BUILD_TARGETS[@]}"                                \
        SOURCE_DIR_OF_THE_LIB_BEING_BUILT=$LIB_OPENAL_SOURCE_DIR                \
        ANDROID_CMAKE_DIR=$ANDROID_CMAKE_DIR                                    \
        LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT=$LIB_OPENAL_BINARIES_LIST \
        RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$LIB_OPENAL_BINARIES_DIR  \
        EXTRA_CMAKE_FLAGS_FOR_THE_LIB_BEING_BUILT="-DALSOFT_REQUIRE_OPENSL=ON"  \
        "$BUILD_SCRIPTS_DIR/AndroidCMakeBuildCommon.sh"
        
        # Check the return code of the above script call
        if [ "$?" -eq 0 ]; 
        then
            CONTINUE=1
        fi
     
        cd $SCRIPT_DIR
        
        #----------------------------
    fi
fi

if [ "$CONTINUE" -eq 1 ];
then
    CONTINUE=0
    
    #------- Build MainApp -------
        
    EXTRA_CMAKE_FLAGS="-DANDROID_BUILD_ARCHITECTURE=<<ANDROID_ARCH>> -DBUILD_MAIN_APP=ON -DBUILD_EDITOR=OFF"
    
    PRESERVE_BUILD_FILES="$PRESERVE_MAIP_APP_BUILD_FILES"                     \
    BUILD_TYPE="$LIBS_BUILD_TYPE"                                             \
    ARCHS_TO_BUILD_FOR="${BUILD_TARGETS[@]}"                                  \
    SOURCE_DIR_OF_THE_LIB_BEING_BUILT=$LIB_MAIN_APP_SOURCE_DIR                \
    ANDROID_CMAKE_DIR=$ANDROID_CMAKE_DIR                                      \
    LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT=$LIB_MAIN_APP_BINARIES_LIST \
    RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$LIB_MAIN_APP_BINARIES_DIR  \
    EXTRA_CMAKE_FLAGS_FOR_THE_LIB_BEING_BUILT=$EXTRA_CMAKE_FLAGS              \
    "$BUILD_SCRIPTS_DIR/AndroidCMakeBuildCommon.sh"
        
    # Check the return code of the above script call
    if [ "$?" -eq 0 ]; then
        CONTINUE=1
    fi
 
    cd $SCRIPT_DIR
    
    #-----------------------------
fi

if [ $BUILD_VALGRIND == YES ];
then
    if [ "$CONTINUE" -eq 1 ];
    then
        CONTINUE=0
        
        #------- Build Valgrind -------
        
        BUILD_TYPE="$LIBS_BUILD_TYPE"                                         \
        ARCHS_TO_BUILD_FOR="${BUILD_TARGETS[@]}"                              \
        SOURCE_DIR_OF_THE_LIB_BEING_BUILT=$VALGRIND_SOURCE_DIR                \
        LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT=$VALGRIND_BINARIES_LIST \
        RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$VALGRIND_BINARIES_DIR  \
        EXTRA_CXXFLAGS="-DANDROID_HARDWARE_generic"                           \
        EXTRA_CONFIGURE_FLAGS="--with-tmpdir=/sdcard"                         \
        "$BUILD_SCRIPTS_DIR/AndroidAutotoolsBuildCommon.sh"
        
        # Check the return code of the above script call
        if [ "$?" -eq 0 ]; 
        then
            CONTINUE=1
        fi

        cd $SCRIPT_DIR

        #------------------------------
    fi
fi

if [ "$CONTINUE" -eq 1 ];
then
    CopySharedLibraries $ANDROID_PROJECT_LIBS_DIR $LIB_GLEW_BINARIES_DIR "$LIB_GLEW_BINARIES_LIST"
    CopySharedLibraries $ANDROID_PROJECT_LIBS_DIR $LIB_LUA_BINARIES_DIR "$LIB_LUA_BINARIES_LIST"
    CopySharedLibraries $ANDROID_PROJECT_LIBS_DIR $LIB_FREE_TYPE_BINARIES_DIR "$LIB_FREE_TYPE_BINARIES_LIST"
    CopySharedLibraries $ANDROID_PROJECT_LIBS_DIR $LIB_ICU_BINARIES_DIR "$LIB_ICU_BINARIES_LIST"
    CopySharedLibraries $ANDROID_PROJECT_LIBS_DIR $LIB_ROCKET_BINARIES_DIR "$LIB_ROCKET_BINARIES_LIST"
    CopySharedLibraries $ANDROID_PROJECT_LIBS_DIR $LIB_BULLET_BINARIES_DIR "$LIB_BULLET_BINARIES_LIST"
    CopySharedLibraries $ANDROID_PROJECT_LIBS_DIR $LIB_BOOST_BINARIES_DIR "$LIB_BOOST_BINARIES_LIST"
    CopySharedLibraries $ANDROID_PROJECT_LIBS_DIR $LIB_LUABIND_BINARIES_DIR "$LIB_LUABIND_BINARIES_LIST"
    CopySharedLibraries $ANDROID_PROJECT_LIBS_DIR $LIB_SDL2_BINARIES_DIR "$LIB_SDL2_BINARIES_LIST"
    CopySharedLibraries $ANDROID_PROJECT_LIBS_DIR $LIB_MAIN_APP_BINARIES_DIR "$LIB_MAIN_APP_BINARIES_LIST"
    CopySharedLibraries $ANDROID_PROJECT_LIBS_DIR $LIB_OGG_BINARIES_DIR "$LIB_OGG_BINARIES_LIST"
    CopySharedLibraries $ANDROID_PROJECT_LIBS_DIR $LIB_TREMOR_BINARIES_DIR "$LIB_TREMOR_BINARIES_LIST"
    CopySharedLibraries $ANDROID_PROJECT_LIBS_DIR $LIB_OPENAL_BINARIES_DIR "$LIB_OPENAL_BINARIES_LIST"
fi