#include "MainConfig.h"
#include <cassert>
#include <Config/CfgUtils.h>
#include <CommonDefinitions.h>
#include <Exceptions/GenericException.h>

#include <luabind/luabind.hpp>
#include <luabind/operator.hpp>
#include <LuaUtils.h>

//-----------------------------------------------------------------------

bool MainConfig::m_ClassAlreadyInstantiated = false;

//-----------------------------------------------------------------------

const char* MainConfig::c_UIntCfgItemNames[e_UIntCfgItemCount] =
{
    "HorizontalResolution", // e_UIntCfgItemVideoHorizontalRes
    "VerticalResolution",   // e_UIntCfgItemVideoVerticalRes
    "FullScreen",           // e_UIntCfgItemVideoFullScreen
    "VSyncEnabled"          // e_UIntCfgItemVSyncEnabled
};

//-----------------------------------------------------------------------

const char* MainConfig::c_StrCfgItemNames[e_StrCfgItemCount] =
{
    "WindowName" // e_StrCfgItemWindowName
};

//-----------------------------------------------------------------------

unsigned int MainConfig::m_UIntCfgItemToItsValue[e_UIntCfgItemCount] =
{
    1024, // e_UIntCfgItemVideoHorizontalRes
    768,  // e_UIntCfgItemVideoVerticalRes
    0,    // e_UIntCfgItemVideoFullScreen
    0,    // e_UIntCfgItemVSyncEnabled
};

//-----------------------------------------------------------------------

std::string MainConfig::m_StrCfgItemToItsValue[e_StrCfgItemCount] =
{
    "TNK" // e_StrCfgItemWindowName
};

//-----------------------------------------------------------------------

MainConfig::MainConfig(FileManager& fileManager, const std::string& cfgFilePath)
{
    if( m_ClassAlreadyInstantiated )
    {
        throw GenericException( "There can only be one instance of MainConfig()" );
    }
    else
    {
        m_ClassAlreadyInstantiated = true;
    }

    static_assert( ArraySize( c_UIntCfgItemNames ) == ArraySize( m_UIntCfgItemToItsValue ), "Must be equal" );
    static_assert( ArraySize( c_StrCfgItemNames ) == ArraySize( m_StrCfgItemToItsValue ), "Must be equal" );

    LoadConfigFromFile( fileManager, cfgFilePath );
}

//-----------------------------------------------------------------------

unsigned int MainConfig::GetUIntConfigItem(UIntCfgItem cfgItem) const
{
    assert( ( cfgItem < e_UIntCfgItemCount ) && "MainConfig unsigned int config item must be less than e_UIntCfgItemCount" );

    return m_UIntCfgItemToItsValue[cfgItem];
}

//-----------------------------------------------------------------------

std::string MainConfig::GetStrConfigItem(StrCfgItem cfgItem) const
{
    assert( ( cfgItem < e_StrCfgItemCount ) && "MainConfig string config item must be less than e_StrCfgItemCount" );

    return m_StrCfgItemToItsValue[cfgItem];
}

//-----------------------------------------------------------------------

void MainConfig::LoadConfigFromFile(FileManager& fileManager, const std::string& cfgFilePath)
{
    std::string mainErrMsg = "Failed to load material from file '" + cfgFilePath + "' ";
    bool ok = false;

    static const char c_TopTableName[] = "MainConfig";

    LuaUtils::LuaStateWrapper luaState;
    luaL_openlibs( luaState );
    if( LuaUtils::LoadLuaScript( luaState, cfgFilePath, fileManager ) )
    {
        std::string secondaryErrMsg = "with the following error: ";
        ok = true;

        // Connect LuaBind to this lua state
        luabind::open( luaState );

        luabind::object topTable = luabind::globals( luaState )[c_TopTableName];

        if( !topTable )
        {
            secondaryErrMsg += "Global Lua table '";
            secondaryErrMsg += c_TopTableName;
            secondaryErrMsg += "' does not exist";
            ok = false;
        }

        if( ok && ( luabind::type( topTable ) == LUA_TTABLE ) )
        {
            for (luabind::iterator i(topTable), end; i != end && ok; ++i)
            {
                const luabind::object& cfgItem = *i;
                const std::string& cfgItemName = luabind::tostring_operator( i.key() );

                UIntCfgItem uintCfgItem = e_UIntCfgItemCount;
                StrCfgItem strCfgItem = e_StrCfgItemCount;

                if( UIntCfgItemNameToUIntCfgItem( cfgItemName, uintCfgItem ) )
                {
                    if( luabind::type( cfgItem ) == LUA_TNUMBER )
                    {
                        const unsigned int number = luabind::object_cast<unsigned int>( cfgItem );

                        m_UIntCfgItemToItsValue[uintCfgItem] = number;
                    }
                    else
                    {
                        secondaryErrMsg += "Value assigned to the\"";
                        secondaryErrMsg += cfgItemName;
                        secondaryErrMsg += "\" must be an integer number";
                    }
                }
                else if( StrCfgItemNameToStrCfgItem( cfgItemName, strCfgItem ) )
                {
                    if( luabind::type( cfgItem ) == LUA_TSTRING )
                    {
                        const char* str = luabind::object_cast<const char*>( cfgItem );

                        m_StrCfgItemToItsValue[strCfgItem] = std::string( str );
                    }
                    else
                    {
                        secondaryErrMsg += "Value assigned to the\"";
                        secondaryErrMsg += cfgItemName;
                        secondaryErrMsg += "\" must be a string";
                    }
                }
                else
                {
                    secondaryErrMsg += "Unknown configuration item '";
                    secondaryErrMsg += cfgItemName;
                    secondaryErrMsg += "'";
                    ok = false;
                }
            }
        }
        else if( ok )
        {
            secondaryErrMsg += "Global Lua variable '";
            secondaryErrMsg += c_TopTableName;
            secondaryErrMsg += "' is not a table";
            ok = false;
        }

        if( !ok )
        {
            mainErrMsg += secondaryErrMsg;
        }
    }

    if( !ok )
    {
        throw GenericException( mainErrMsg );
    }
}

//-----------------------------------------------------------------------

bool MainConfig::UIntCfgItemNameToUIntCfgItem(const std::string& cfgItemName, UIntCfgItem& outVal)
{
    return CfgUtils::StringToCorrespondingEnum<UIntCfgItem>( cfgItemName,
                                                             c_UIntCfgItemNames,
                                                             ArraySize( c_UIntCfgItemNames ),
                                                             &outVal );
}

//-----------------------------------------------------------------------

bool MainConfig::StrCfgItemNameToStrCfgItem(const std::string& cfgItemName, StrCfgItem& outVal)
{
    return CfgUtils::StringToCorrespondingEnum<StrCfgItem>( cfgItemName,
                                                            c_StrCfgItemNames,
                                                            ArraySize( c_StrCfgItemNames ),
                                                            &outVal );
}

//-----------------------------------------------------------------------
