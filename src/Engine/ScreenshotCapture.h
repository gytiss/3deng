#ifndef SCREENSHOTCAPTURE_H_INCLUDED
#define SCREENSHOTCAPTURE_H_INCLUDED

#include <cstdlib>
#include <string>
#include <boost/filesystem.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/posix_time_io.hpp>
#include <SystemInterface.h>

class GraphicsDevice;

/**
 * Allows easy capture of screenshots into a given directory.
 */
class ScreenshotCapture
{
public:
    /**
     * @param systemInterface - Used for getting drawable surface dimensions
     * @param graphicsDevice
     * @param screenshotDirectoryPath - Path to a directory where the screenshots will be saved
     */
    ScreenshotCapture(SystemInterface& systemInterface,
                      GraphicsDevice& graphicsDevice,
                      const std::string& screenshotDirectoryPath);

    ~ScreenshotCapture();

    /**
     * Saves image currently displayed on the screen
     * to the m_ScreenshotDirectoryPath directory.
     *
     * @param outputImageFileName - Name (without extension) of the file the screenshot will be saved to
     */
    void CaptureScreenshot(const std::string& outputImageFileName = "");

    /**
     * Saves contents of a currently bound stencil buffer (any values above 0 are presented
     * as red pixels) to an image in the m_ScreenshotDirectoryPath directory.
     *
     * Note: This functionality is not available on OpenGL ES, hence when compiled
     *       with OpenGL ES this function does not do anything
     *
     * @param outputImageFileName - Name (without extension) of the file the stencil buffer screenshot will be saved to
     */
    void CaptureStencilBufferScreenshot(const std::string& outputImageFileName = "");

private:
    // Used to prevent copying
    ScreenshotCapture& operator = (const ScreenshotCapture& other);
    ScreenshotCapture(const ScreenshotCapture& other);

    void _CaptureScreenshot(const std::string& outputImageFileName = "", bool dumpStencilBuffer = false);

    static void _DrawableSurfaceSizeChaged(const SystemInterface::DrawableSurfaceSize& newSize, void* userData);

    SystemInterface& m_SystemInterface;
    SystemInterface::DrawableSurfaceSize m_DrawableSurfaceSize;
    GraphicsDevice& m_GraphicsDevice;
    boost::posix_time::time_facet* m_TimeFacet;
    boost::filesystem::path m_ScreenshotDirectoryPath;
    const std::locale m_TimeFormat;
};

#endif // SCREENSHOTCAPTURE_H_INCLUDED
