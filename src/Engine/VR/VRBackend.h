#ifndef VR_BACKEND_H_INCLUDED

#include <openvr.h>
#include <string>
#include <glm/glm.hpp>
#include <GraphicsDevice/GraphicsDevice.h>

class GraphicsDevice;

class VRBackend
{
public:
    enum VRDeviceMatrix
    {
        e_VRDeviceMatrix_HMDLeftEyePose = 0,
        e_VRDeviceMatrix_HMDRightEyePose = 1,
        e_VRDeviceMatrix_HMDLeftEyeProjection,
        e_VRDeviceMatrix_HMDRightEyeProjection,
        e_VRDeviceMatrix_HMDOverallPose,

        e_VRDeviceMatrix_Count // Not a VR matrix (used for counting purposes)
    };

    VRBackend();
    ~VRBackend();

    /**
     * To avoid any unforeseen issues this method needs to be called before issue the first OpenGL call
     */
    void Initialise();
    
    /**
     * To avoid any unforeseen issues this method needs to be called before OpenGL state is destroyed
     */
    void Shutdown();

    void SetGraphicsDevice(GraphicsDevice* graphicsDevice);

    std::string GetHMDDriverName();
    std::string GetHMDName();
    size_t GetHMDRenderWidth() const;
    size_t GetHMDRenderHeight() const;

    /**
     * Wait till all of the GPU and HMD display operations are done
     */
    void WaitForHMD();

    vr::EVRCompositorError SubmitAFrameToHDM(vr::EVREye eEye, const vr::Texture_t* texture);
    void PollForVRDevicesPoses();
    const glm::mat4& GetVRDeviceMatrix(VRDeviceMatrix matrixID) const;

private:
    // Used to prevent copying
    VRBackend& operator = (const VRBackend& other) = delete;
    VRBackend(const VRBackend& other) = delete;

    std::string _GetVRDeviceStr(vr::IVRSystem* hmd, vr::TrackedDeviceIndex_t deviceIndex, vr::TrackedDeviceProperty property, vr::TrackedPropertyError* outError = nullptr);
    std::string _CompositorErrorCodeToString(vr::EVRCompositorError error) const;
    void _SetupStaticVRMatrices();
    glm::mat4 _OpenVRMat34ToGLMMat44(const vr::HmdMatrix34_t) const;
    glm::mat4 _OpenVRMat44ToGLMMat44(const vr::HmdMatrix44_t mat) const;

    GraphicsDevice* m_GraphicsDevice;
    vr::IVRSystem* m_HMD; // Head mounted display
    vr::IVRRenderModels* m_RenderModels;
    uint32_t m_HMDRenderWidth;
    uint32_t m_HMDRenderHeight;
    glm::mat4 m_VRDevicesMatrices[e_VRDeviceMatrix_Count];
    vr::TrackedDevicePose_t m_TrackedDevicesPoses[vr::k_unMaxTrackedDeviceCount];
};

#endif // VR_BACKEND_H_INCLUDED
