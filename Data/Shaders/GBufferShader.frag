
layout(location = 0) out vec3 out_Specular;
layout(location = 1) out vec3 out_Diffuse;
layout(location = 2) out vec3 out_Normal_ViewSpace;

in vec2 TexCoord;
in mat3 TBN;

uniform sampler2D u_DiffuseMap;
uniform sampler2D u_NormalMap;
uniform sampler2D u_SpecularMap;

vec2 EncodeNormal(const in vec3 Normal_ViewSpace)
{
    vec2 EncodedNormal = normalize(Normal_ViewSpace.xy) * (sqrt(-Normal_ViewSpace.z*0.5f+0.5f));
    EncodedNormal = EncodedNormal*0.5f+0.5f;
    return EncodedNormal;
}

#if defined(USING_OPENGL_ES)
vec3 SRGBColorValueToLinearColorValue(const in vec3 srgbColorValue)
{
    return vec3(pow( clamp( srgbColorValue.r, 0.0f, 1.0f ), 2.2f ),
                pow( clamp( srgbColorValue.g, 0.0f, 1.0f ), 2.2f ),
                pow( clamp( srgbColorValue.b, 0.0f, 1.0f ), 2.2f ));
}
#endif

void main(void)
{
    // (2.0f * x) - 1.0f is needed to convert from the range of 0 to 1 to the range of -1 to 1
    vec3 NormalMapNormal_TangentSpace = normalize((2.0f * texture(u_NormalMap, TexCoord).rgb) - 1.0f);
    vec3 PixelNormal_ViewSpace = (TBN * NormalMapNormal_TangentSpace);
    float MaterialSpecularIntensity = texture(u_SpecularMap, TexCoord).r;

    out_Specular = vec3( MaterialSpecularIntensity ); // TODO, This is a stub and needs to be implemented properly
#if defined(USING_OPENGL_ES)
    out_Diffuse = SRGBColorValueToLinearColorValue(texture(u_DiffuseMap, TexCoord).xyz);
#else
    out_Diffuse = texture(u_DiffuseMap, TexCoord).xyz;
#endif
    out_Normal_ViewSpace.xy = EncodeNormal(normalize(PixelNormal_ViewSpace));
    out_Normal_ViewSpace.z = MaterialSpecularIntensity; // TODO, This is where the glossiness value will go to later on
}
