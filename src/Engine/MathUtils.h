#ifndef MATH_UTILS_H_INCLUDED
#define MATH_UTILS_H_INCLUDED

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <cmath>
#include <cstdint>
#include <cassert>
#include <EngineTypes.h>
#include <EngineDefaults.h>
#include <Scene/SceneTypes.h>
#include <CommonDefinitions.h>

class MathUtils
{
public:
    static const glm::quat c_IdentityQuaternion;
    static const glm::mat4 c_IdentityMatrix;
    static const SceneTypes::Transform c_DefaultTransform;

    /**
     * Returns -1 if value is negative, +1 if value is positive and 0 if value is zero
     */
    static inline int Signum(int val)
    {
        return ( ( 0 < val ) - ( val < 0 ) );
    }

    /**
     * Returns unsigned integer with N'th bit set
     *
     * @param bitNum - Number of the bit to be set
     */
    template<unsigned int bitNum>
    class GetBit
    {
    public:
        enum
        {
            Result = (1 << bitNum)
        };
    };

    /**
     * Use this instead when C++11 support is enabled
     *
     * constexpr unsigned int GetBit(const unsigned int bitNum)
     * {
     *     return (1 << bitNum);
     * }
     */

    static inline float DegreesToRadians( const float val )
    {
        return ( static_cast<float>( M_PI ) / 180.0f ) * val;
    }

    static inline float RadiansToDegrees( const float val )
    {
        return ( 180.0f / static_cast<float>( M_PI ) ) * val;
    }

    static inline bool NumberIsEven( const size_t num)
    {
        return ( ( num & 0x01 ) == 0 );
    }

    static inline bool NumberIsOdd( const size_t num)
    {
        return !NumberIsEven( num );
    }
    
    static inline constexpr bool IsPowerOfTwo(uintptr_t value)
    {
        return !( value == 0 ) && !( value & ( value - 1 ) );
    }

    /**
     * Check if a given "value" can be safely stored in type "T"
     */
    template<typename T>
    static inline constexpr bool CanBeStoredInType(size_t value)
    {
        return ( value <= std::numeric_limits<T>::max() );
    }

    static inline constexpr bool IsDivisibleBy(uintptr_t value, uintptr_t divisor)
    {
        return ( ( divisor > 0 ) && ( (  value & ( divisor - 1 ) ) == 0 ) );
    }

    /**
     * Takes a pointer to a contiguous chunk of memory and returns a pointer which
     * is still within the bounds of this chunk and is "alignmentInBytes" aligned
     *
     * @param address - A pointer to a memory location
     * @param alignmentInBytes - Desired alignment
     */
    template <typename T>
    static inline constexpr bool IsAligned(T address, uintptr_t alignmentInBytes)
    {
        static_assert( IsPointerPredicate<T>::value, "T must be a pointer" );
        //assert( IsPowerOfTwo( alignmentInBytes ) && "alignmentInBytes must be a power of two" );

        return ( IsPowerOfTwo( alignmentInBytes ) && IsDivisibleBy( reinterpret_cast<uintptr_t>( address ), alignmentInBytes ) );
    }

    /**
     * Takes a pointer to a contiguous chunk of memory and returns a pointer which
     * is still within the bounds of this chunk and is "alignmentInBytes" aligned
     *
     * @param address - A pointer to a contiguous chunk (larger than alignmentInBytes bytes) of memory
     * @param alignmentInBytes - Desired alignment
     */
    template <typename T>
    static inline T GetNextAlignedAddress(T address, uintptr_t alignmentInBytes)
    {
        static_assert( IsPointerPredicate<T>::value, "T must be a pointer" );
        assert( IsPowerOfTwo( alignmentInBytes ) && "alignmentInBytes must be a power of two" );

        T retVal = address;
        if( ( address != nullptr ) && ( alignmentInBytes > 0 ) )
        {
            uintptr_t addrValue = reinterpret_cast<uintptr_t>( address);
            retVal = reinterpret_cast<T>( ( ( addrValue + ( alignmentInBytes - 1 ) ) / alignmentInBytes ) * alignmentInBytes );
        }

        return retVal;
    }

    static inline size_t MaxMipMapLevelForTextureDimensions(size_t widthInPixels, size_t heightInPixels)
    {
        return static_cast<size_t>( floor( log2( std::max( widthInPixels, heightInPixels ) ) ) + 1 );
    }

    /**
     * Convert a given quaternion into an euler angles(in degrees) vector where X is pitch, y is yaw and z is roll
     */
    static inline glm::vec3 QuaternionToEulerXYZAngles(const glm::quat& quaternion)
    {
        glm::vec3 eulerAnglesInRadians = glm::eulerAngles( quaternion );
        return glm::vec3( RadiansToDegrees( eulerAnglesInRadians.x ),
                          RadiansToDegrees( eulerAnglesInRadians.y ),
                          RadiansToDegrees( eulerAnglesInRadians.z ) );
    }
    
    /**
     * Convert a given euler angles(in degrees) vector (where X is pitch, Y is yaw and Z is roll) to a quaternion
     */
    static inline glm::quat EulerXYZAnglesToQuaternion(const glm::vec3& eulerXYZAngles)
    {
        return glm::quat( glm::vec3( DegreesToRadians( eulerXYZAngles.x ),
                                     DegreesToRadians( eulerXYZAngles.y ),
                                     DegreesToRadians( eulerXYZAngles.z ) ) );
    }

    /**
     * Rounds floating point value to a specified number of decimal points
     *
     * @param num - Number to round
     * @param precision - Number of decimal places to round to
     */
    static inline float Round(float num, size_t precision)
    {
        return static_cast<float>( floor( num * pow( 10.0f, precision ) + 0.5f ) / pow( 10.0f, precision ) );
    }

    /**
     * Maps value of one range onto the other range.
     *
     * @param val - Value to map
     * @param R1S - Start of range one
     * @param R1E - End of range one
     * @param R2S - Start of range two
     * @param R2E - End of range two
     *
     * @note - The following condition must hold: (R1S < R1E) and (R2S < R2E)
     *
     * @return - val converted to range two
     */
    static inline float MapRange(float val, float R1S, float R1E, float R2S, float R2E)
    {
        assert( ( R1S < R1E ) && "Start of range 1 must be lower than end of range 1" );
        assert( ( R2S < R2E ) && "Start of range 2 must be lower than end of range 2" );

        const float range1Len = ( R1E - R1S );
        const float range2Len = ( R2E - R2S );
        const float conversionFactor = ( range2Len / range1Len );

        // "val" converted from the range of (R1S,R1E) to the range (0,range1Len)
        const float valInPositiveRange = ( val - R1S );

        return ( ( conversionFactor * valInPositiveRange ) + R2S );
    }

    /**
     * Returns half of the height of the cone created by scaling a unit cone
     * to have a cutoff angle of "cutOffAngleInDegrees".
     */
    static inline float SpotLightUnitConeHeight(float cutOffAngleInDegrees, float radius)
    {
        return ( (radius / 2.0f ) * cos( DegreesToRadians( cutOffAngleInDegrees ) ) );
    }

   /**
    * Calculates scale values by which a unit cone should
    * be scaled to be representative of a spot light with
    * a given cutoff angle.
    *
    * @param cutOffAngleInDegrees - light cutoff angle
    * @param radius - Half of the size of the wide end of the final code
    */
    static inline glm::vec3 SpotLightUnitConeScaleFactors(float cutOffAngleInDegrees, float radius)
    {
        const float height = SpotLightUnitConeHeight( cutOffAngleInDegrees, radius );

        glm::vec3 coneScale( 1.0f );
        coneScale.y = height;
        coneScale.x = ( 2.0f * (radius / 2.0f) * sin( DegreesToRadians( cutOffAngleInDegrees  ) ) );
        coneScale.z = coneScale.x;

        return coneScale;
    }

    static inline glm::vec3 RotateVector(const glm::vec3& vecToRotate,
                                         const glm::quat& rotationQuat)
    {
        glm::quat r = glm::normalize( rotationQuat );
        glm::vec3 q( r.x, r.y, r.z );
        glm::vec3 t( 2.0f * glm::cross( q, vecToRotate ) );
        return glm::vec3( ( vecToRotate + r.w ) * ( t + glm::cross( q, t ) ) );
    }

    static inline glm::vec3 RotateVector(const glm::vec3& vec,
                                         const glm::vec3& rotationAnglesInDegrees)
    {
        glm::mat4 rotMarix = glm::mat4( 1.0f );
        rotMarix = glm::rotate( rotMarix, DegreesToRadians( rotationAnglesInDegrees.x ), glm::vec3(1.0f, 0.0f, 0.0f) );
        rotMarix = glm::rotate( rotMarix, DegreesToRadians( rotationAnglesInDegrees.y ), glm::vec3(0.0f, 1.0f, 0.0f) );
        rotMarix = glm::rotate( rotMarix, DegreesToRadians( rotationAnglesInDegrees.z ), glm::vec3(0.0f, 0.0f, 1.0f) );
        return glm::vec3( rotMarix * glm::vec4( vec, 0.0f ) );
    }

    /**
     * Calculate angle(in degrees) between two vectors
     */
    static inline float AngleBetweenTwoVectors(const glm::vec3& a, const glm::vec3& b)
    {
        return RadiansToDegrees( acos( glm::dot( a, b ) ) );
    }

    /**
     * Calculate counterclockwise heading(in degrees) on the XZ plane using a given direction vector
     *
     * Note: 0 degrees is on the negative Z axis, 180 degrees is on the positive Z axis and 90
     *       degrees is on the negative X axis
     *
     * @return 0 to 360 value in degrees
     */
    static inline float HeadingFromADirectionVector(const glm::vec3& dirVec)
    {
        static const glm::vec3 c_HeadingOriginAxisVector( 0.0f, 0.0f, 1.0f );
        float headingInDegrees = AngleBetweenTwoVectors( c_HeadingOriginAxisVector, glm::normalize( glm::vec3( dirVec.x, 0.0f, dirVec.z ) ) );

        if( dirVec.x > 0.0f )
        {
            headingInDegrees += 180.0f;
        }
        else
        {
            headingInDegrees = fabs(headingInDegrees - 180.0f);
        }

        return headingInDegrees;
    }

    /**
     * Convert transformation matrix to a scene transform composed
     * of a position, rotation quaternion and scale.
     */
    static inline SceneTypes::Transform MatrixToSceneTransform(const glm::mat4& mat)
    {
        SceneTypes::Transform retVal;
                                     //  X          Y          Z
        retVal.position = glm::vec3( mat[3][0], mat[3][1], mat[3][2] );
        retVal.rotation = glm::toQuat( mat );
                                  //               X                      Y                      Z
        retVal.scale = glm::vec3( glm::length( mat[0] ), glm::length( mat[1] ), glm::length( mat[2] ) );

        return retVal;
    }

    /**
     * Convert scene transform composed of a position, rotation
     * quaternion and scale to translation matrix
     */
    static inline glm::mat4 SceneTransformToMatrix(const SceneTypes::Transform& t)
    {
        glm::mat4 retMat = glm::translate( MathUtils::c_IdentityMatrix, t.position );
        retMat = retMat * glm::toMat4( t.rotation );
        retMat = glm::scale( retMat , t.scale );

        return retMat;
    }

    /**
     * Extract translation component from a scene transform matrix
     */
    static inline glm::vec3 MatrixToTranslation(const glm::mat4& mat)
    {
        return glm::vec3( mat[3][0], mat[3][1], mat[3][2] );
    }

    /**
    * Gets the shortest arc quaternion to rotate "srcVec" vector to the "destVec"
    *
    * If you call this with a dest vector that is close to the inverse
    * of this vector, we will rotate 180 degrees around the 'fallbackAxis'
    * (if specified, or a generated axis if not) since in this case ANY axis
    * of rotation is valid.
    */
    static inline glm::quat GetVectorRotationTo(const glm::vec3& srcVec, const glm::vec3& destVec)
    {
        // Based on Stan Melax's article in Game Programming Gems
        glm::quat q;
        // Copy, since cannot modify local
        const glm::vec3 v0 = glm::normalize( srcVec );
        const glm::vec3 v1 = glm::normalize( destVec );

        float d = glm::dot( v0, v1 );

        // If dot == 1, vectors are the same(i.e. on the same line and point to the same direction)
        if( d >= 1.0f )
        {
            q = c_IdentityQuaternion;
        }
        else
        {
            if( d < ( 1e-6f - 1.0f ) ) // Vectors are on the same line and point to opposite directions
            {
                // Generate an axis
                const glm::vec3 xAxis = glm::vec3( 1.0f, 0.0f, 0.0 );
                const glm::vec3 yAxis = glm::vec3( 0.0f, 1.0f, 0.0 );
                glm::vec3 axis = glm::cross( xAxis, srcVec );
                if( glm::length( axis ) == 0 )
                {
                    axis = glm::cross( yAxis, srcVec );
                }

                q = glm::angleAxis( static_cast<float>( M_PI ), glm::normalize( axis  ) );
            }
            else
            {
                const float s = sqrt( ( 1 + d ) * 2 );
                const float invs = ( 1 / s );

                const glm::vec3 c = glm::cross( v0, v1 );

                q.x = c.x * invs;
                q.y = c.y * invs;
                q.z = c.z * invs;
                q.w = s * 0.5f;

                q = glm::normalize( q );
            }
        }

        return q;
    }

    /**
     * @param xInScreenSpace - X coordinate on the screen through which the ray is going
     * @param yInScreenSpace - Y coordinate on the screen through which the ray is going
     * @param screenWidthInPixels - Current screen width e.g.: 1024
     * @param screenHeightInPixels - Current screen height e.g.: 768
     * @param viewProjectionTransform - Current view and projection matrices
     * @param currentCameraPositionInWorldSpace - Current camera position (This value is used as ray origin value)
     * @param outRayDirection [out] - Resulting ray
     */
    static void CoordinateInScreenSpaceToRay(float xInScreenSpace,
                                             float yInScreenSpace,
                                             size_t screenWidthInPixels,
                                             size_t screenHeightInPixels,
                                             EngineTypes::VPTransformations& viewProjectionTransform,
                                             const glm::vec3& currentCameraPositionInWorldSpace,
                                             glm::vec3& outRayDirection);

    /**
     * @param fieldOfView - A value in degrees
     * @param screenWidthInPixels - Current screen width e.g.: 1024
     * @param screenHeightInPixels - Current screen height e.g.: 768
     */
    static inline glm::mat4 GeneratePerspectiveProjectionMatrix(const float fieldOfView,
                                                                size_t screenWidthInPixels,
                                                                size_t screenHeightInPixels,
                                                                float nearClippingPlane = EngineDefaults::c_NearClippingPlane,
                                                                float farClippingPlane = EngineDefaults::c_FarClippingPlane)
    {
        return glm::perspective( DegreesToRadians( fieldOfView ),
                                 static_cast<float>( screenWidthInPixels ) / static_cast<float>( screenHeightInPixels ),
                                 nearClippingPlane,
                                 farClippingPlane );
    }

    /**
     * @param zoom - How close the camera is to the scene
     * @param screenWidthInPixels - Current screen width e.g.: 1024
     * @param screenHeightInPixels - Current screen height e.g.: 768
     */
    static inline glm::mat4 GenerateOrtographicProjectionMatrix(float zoom,
                                                                size_t screenWidthInPixels,
                                                                size_t screenHeightInPixels,
                                                                float nearClippingPlane = EngineDefaults::c_NearClippingPlane,
                                                                float farClippingPlane = EngineDefaults::c_FarClippingPlane)
    {
        return glm::ortho( -static_cast<float>(screenWidthInPixels) / zoom,
                           static_cast<float>(screenWidthInPixels)  / zoom,
                           -static_cast<float>(screenHeightInPixels) / zoom,
                           static_cast<float>(screenHeightInPixels) / zoom,
                           nearClippingPlane,
                           farClippingPlane );
    }

    static inline float CalculateFixedSizeObjectScalingFactor(float fieldOfView,
                                                              const glm::vec3& cameraPositionInWorldSpace,
                                                              const glm::vec3& objectPositionInWorldSpace)
    {
        const float fovInRadians = DegreesToRadians( fieldOfView );
        const float cameraObjectDistance = glm::distance( cameraPositionInWorldSpace, objectPositionInWorldSpace );
        const float worldSize = ( 2.0f * glm::tan( fovInRadians / 2.0f ) ) * cameraObjectDistance;
        const float scale = ( 0.25f * worldSize );

        return scale;
    }

private:
    MathUtils(); // This class provides static constants and functions only hence it does not need to be constructed
};


#endif // MATH_UTILS_H_INCLUDED
