#ifndef AUDIODEVICE_DEVICE_H_INCLUDED
#define AUDIODEVICE_DEVICE_H_INCLUDED

struct ALCdevice_struct;
typedef struct ALCdevice_struct ALCdevice;

namespace AudioDevice
{

class Device
{
public:
    Device();
    ~Device();

    inline ALCdevice* GetALHandle()
    {
        return m_ALDevice;
    }

private:
    // Used to prevent copying
    Device& operator = (const Device& other) = delete;
    Device(const Device& other) = delete;

    ALCdevice* m_ALDevice;
};

}

#endif // AUDIODEVICE_DEVICE_H_INCLUDED
