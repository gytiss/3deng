#include "Renderer.h"

//-----------------------------------------------------------------------

Renderer::Renderer(GameObject& owningGameObject,
                   RendererType type,
                   const std::string& name,
                   MaterialManager& materialManager)
: Component( m_ComponentType, name, owningGameObject ),
  m_MaterialManager( materialManager ),
  m_DepthTestingEnabled( true ),
  m_RendererType( type ),
  m_Renderables()
{

}

//-----------------------------------------------------------------------

Renderer::~Renderer()
{
    _ReleaseExistingSubMeshesMaterials();
}

//-----------------------------------------------------------------------

size_t Renderer::GetSubMeshCount() const
{
    size_t retVal = 0;

    MeshManager::MeshHandle meshHandle = _GetMeshHandle();

    if( meshHandle.IsValid() )
    {
        retVal = meshHandle->GetSubMeshes().size();
    }

    return retVal;
}

//-----------------------------------------------------------------------

/**
 * @param subMeshIndex - Index of the sub mesh which must be lower than the value returned by the GetSubMeshCount()
 */
MaterialManager::MaterialHandle* Renderer::GetSubMeshMaterial(size_t subMeshIndex)
{
    MaterialManager::MaterialHandle* retVal = nullptr;

    if( subMeshIndex < m_SubMeshesMaterials.size() )
    {
        retVal = &m_SubMeshesMaterials[subMeshIndex];
    }

    return retVal;
}

//-----------------------------------------------------------------------

bool Renderer::SetSubMeshMaterial(size_t subMeshIndex, MaterialManager::MaterialHandle handle)
{
    bool retVal = false;

    MeshManager::MeshHandle meshHandle = _GetMeshHandle();

    if( meshHandle.IsValid() && ( subMeshIndex < m_SubMeshesMaterials.size() ) )
    {
        MaterialManager::MaterialHandle& currentMaterialHandle = m_SubMeshesMaterials[subMeshIndex];

        if( currentMaterialHandle.IsValid() )
        {
            m_MaterialManager.ReleaseMaterial( currentMaterialHandle );
        }

        currentMaterialHandle = handle;

        _RegenerateRenderables();

        retVal = true;
    }

    return retVal;
}

//-----------------------------------------------------------------------

void Renderer::SetDepthTestingEnabled(bool enabled)
{
    m_DepthTestingEnabled = enabled;
    _RegenerateRenderables();
}

//-----------------------------------------------------------------------

bool Renderer::GetDepthTestingEnabled() const
{
    return m_DepthTestingEnabled;
}

//-----------------------------------------------------------------------

void Renderer::_AddRenderable(const RenderableObject& renderable)
{
    m_Renderables.push_back( renderable );
}

//-----------------------------------------------------------------------

void Renderer::_ClearRenderables()
{
    m_Renderables.clear();
}

//-----------------------------------------------------------------------

void Renderer::_ResetSubMeshesMaterialsList(const MeshManager::MeshHandle& meshHandle)
{
    _ReleaseExistingSubMeshesMaterials();
    m_SubMeshesMaterials.assign( meshHandle->GetSubMeshes().size(), MaterialManager::MaterialHandle() ); // Every sub mesh will have its own material
}

//-----------------------------------------------------------------------

void Renderer::_ReleaseExistingSubMeshesMaterials()
{
    for(MaterialManager::MaterialHandle& materialHandle : m_SubMeshesMaterials)
    {
        if( materialHandle.IsValid() )
        {
            m_MaterialManager.ReleaseMaterial( materialHandle );
        }
    }
    m_SubMeshesMaterials.clear();
}

//-----------------------------------------------------------------------
