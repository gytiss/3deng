#ifndef AUDIODATAMANAGER_H_INCLUDED
#define AUDIODATAMANAGER_H_INCLUDED

#include <Resources/Audio/AudioData.h>
#include <Containers/List.hpp>
#include <map>

class FileManager;

class AudioDataManager
{
private:
    struct RefCountedAudioDataPtr
    {
        RefCountedAudioDataPtr()
        : refCount( 0 ),
          audioDataFilePath(),
          audioData( nullptr )
        {
        }

        RefCountedAudioDataPtr& operator= (const RefCountedAudioDataPtr& other)
        {
            refCount = other.refCount;
            audioDataFilePath = other.audioDataFilePath;
            audioData = other.audioData;

            return *this;
        }

        RefCountedAudioDataPtr(const RefCountedAudioDataPtr& other)
        {
            operator=( other );
        }

        unsigned int refCount;
        std::string audioDataFilePath; // Path of the audio file this audio data was loaded from
        AudioData* audioData;
    };

    typedef List<RefCountedAudioDataPtr> AudioDataList;
    typedef std::map<std::string, AudioDataList::Node*> UniqueAudioDataIDsToAudioDataMap;

public:
    class AudioDataHandle
    {
    public:
        friend class AudioDataManager;

        AudioDataHandle()
        : m_AudioDataListNode( nullptr ),
          m_Valid( false )
        {
        }

        AudioDataHandle& operator= (const AudioDataHandle& other)
        {
            m_AudioDataListNode = other.m_AudioDataListNode;
            m_Valid = other.m_Valid;
            return *this;
        }

        AudioDataHandle(const AudioDataHandle& other)
        {
            operator= ( other );
        }

        AudioData* operator->()
        {
            return m_AudioDataListNode->value.audioData;
        }

        const AudioData* operator->() const
        {
            return m_AudioDataListNode->value.audioData;
        }

        AudioData& operator*()
        {
            return *m_AudioDataListNode->value.audioData;
        }

        const AudioData& operator*() const
        {
            return *m_AudioDataListNode->value.audioData;
        }

        bool IsValid() const
        {
            return m_Valid;
        }

    protected:
        void Invalidate()
        {
            m_Valid = false;
        }

        AudioDataHandle(AudioDataList::Node* audioDataListNode)
        : m_AudioDataListNode( audioDataListNode ),
          m_Valid( true )
        {
        }

        AudioDataList::Node* m_AudioDataListNode;  // Pointer to a node of the audio data list i.e. m_AudioDataList
        bool m_Valid;
    };

    AudioDataManager(FileManager& fileManager);
    ~AudioDataManager();

    AudioDataHandle LoadAudioData(const std::string& audioFilePath);
    void ReleaseAudioData(AudioDataHandle& handle);

private:
    // Used to prevent copying
    AudioDataManager& operator = (const AudioDataManager& other) = delete;
    AudioDataManager(const AudioDataManager& other) = delete;

    AudioDataList::Node* _LoadAudioDataFromFile(const std::string& audioFilePath);

    FileManager& m_FileManager;
    AudioDataList m_AudioDataList;
    UniqueAudioDataIDsToAudioDataMap m_UniqueAudioDataIDsToAudioDataMap;
    static bool m_ClassAlreadyInstantiated;
};

#endif // AUDIODATAMANAGER_H_INCLUDED
