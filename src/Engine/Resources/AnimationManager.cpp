#include "AnimationManager.h"
#include <Exceptions/GenericException.h>
#include <GraphicsDevice/GraphicsDevice.h>

//-----------------------------------------------------------------------

bool AnimationManager::m_ClassAlreadyInstantiated = false;

//-----------------------------------------------------------------------

AnimationManager::AnimationManager(GraphicsDevice& graphicsDevice,
                                   SystemInterface& systemInterface,
                                   FileManager& fileManager)
: m_AnimationList(),
  m_AnimationPathsToAnimationsMap(),
  m_GraphicsDevice( graphicsDevice ),
  m_SystemInterface( systemInterface ),
  m_FileManager( fileManager )
{
    if( m_ClassAlreadyInstantiated )
    {
        throw GenericException( "There can only be one instance of AnimationManager()" );
    }
    else
    {
        m_ClassAlreadyInstantiated = true;
    }
}

//-----------------------------------------------------------------------

AnimationManager::~AnimationManager()
{
    AnimationList::iterator it = m_AnimationList.begin();
    while( it != m_AnimationList.end() )
    {
        RefCountedAnimationPtr& animationPtr = *it;
        delete animationPtr.animation;
        it = m_AnimationList.erase( it );
    }
}

//-----------------------------------------------------------------------

void AnimationManager::UpdateCurrentlyActiveAnimations()
{
    std::vector<AnimationList::iterator> animationToDeactivate;

    for (ActiveAnimationsList::iterator it = m_ActiveAnimationsList.begin(); it != m_ActiveAnimationsList.end(); ++it)
    {
        ActiveAnimationRecord& activeAimationRecord = *it;
        MAnim* animation = activeAimationRecord.first->animation;
        AnimationResultsListener* animationResultsListener = activeAimationRecord.second;

        const bool notFinished = animation->Update();
        animationResultsListener->AnimatedSkeletonChanged( animation->GetSkeletonMatrixArray() );

        if( notFinished == false ) // Animation has finished and needs to be deactivated
        {
            animationToDeactivate.push_back( activeAimationRecord.first );
        }
    }

    for(AnimationList::iterator& it : animationToDeactivate)
    {
        _DeactivateAnimation( it );
    }
}

//-----------------------------------------------------------------------

void AnimationManager::ActivateAnimation(AnimationHandle& handle,
                                         AnimationResultsListener* animationResultsListener,
                                         bool loop)
{
    assert( ( animationResultsListener != nullptr ) && "animationResultsListener must not be null" );

    if( !handle.m_AnimationListIt->isCurrentlyActive )
    {
        m_ActiveAnimationsList.push_front( ActiveAnimationRecord( handle.m_AnimationListIt, animationResultsListener ) );
        handle.m_AnimationListIt->activeAnimationListIt = m_ActiveAnimationsList.begin();
        handle.m_AnimationListIt->isCurrentlyActive = true;
        handle.m_AnimationListIt->isLooping = loop;
        handle.m_AnimationListIt->animation->StartAnimtion( loop );
    }
}

//-----------------------------------------------------------------------

void AnimationManager::DeactivateAnimation(AnimationHandle& handle)
{
    _DeactivateAnimation( handle.m_AnimationListIt );
}

//-----------------------------------------------------------------------

bool AnimationManager::IsAnimationActive(AnimationHandle& handle)
{
    return handle.m_AnimationListIt->isCurrentlyActive;
}

//-----------------------------------------------------------------------

AnimationManager::AnimationHandle AnimationManager::LoadAnimation(const std::string& animationFilePath)
{
    AnimationList::iterator animationIt;

    AnimationPathsToAnimationsMap::iterator it = m_AnimationPathsToAnimationsMap.find( animationFilePath );
    if( it != m_AnimationPathsToAnimationsMap.end() )
    {
        animationIt = it->second;
        animationIt->refCount = ( animationIt->refCount + 1 );
    }
    else
    {
        animationIt = _LoadAnimationFromFile( animationFilePath );
    }

    return AnimationHandle( animationIt );
}

//-----------------------------------------------------------------------

void AnimationManager::ReleaseAnimation(AnimationHandle& handle)
{
    assert( handle.IsValid() && "Tried to free an invalid handle" );

    if( handle.IsValid() )
    {
        RefCountedAnimationPtr& animationPtr = *handle.m_AnimationListIt;
        animationPtr.refCount = ( animationPtr.refCount - 1 );

        if( animationPtr.refCount == 0 )
        {
            delete animationPtr.animation;
            m_AnimationPathsToAnimationsMap.erase( animationPtr.animationFilePath );
            m_AnimationList.erase( handle.m_AnimationListIt );
            handle.Invalidate();
        }
    }
}

//-----------------------------------------------------------------------

bool AnimationManager::CheckIfAnimationsAreInterchangeable(const AnimationHandle& animation1Handle,
                                                           const AnimationHandle& animation2Handle)
{
    assert( animation1Handle.IsValid() &&
            animation2Handle.IsValid() &&
            "Cannot compare invalid handles" );

    bool retVal = false;

    if( animation1Handle.IsValid() && animation2Handle.IsValid() )
    {
        retVal = ( memcmp( animation1Handle->GetBoneNamesMD5Hash(),
                           animation2Handle->GetBoneNamesMD5Hash(),
                           MAnim::c_MD5HashLength ) == 0 );
    }

    return retVal;
}

//-----------------------------------------------------------------------

void AnimationManager::_DeactivateAnimation(AnimationList::iterator it)
{
    if( it->isCurrentlyActive )
    {
        m_ActiveAnimationsList.erase( it->activeAnimationListIt );
        it->activeAnimationListIt = m_ActiveAnimationsList.end();
        it->isCurrentlyActive = false;
        it->isLooping = false;
    }
}

//-----------------------------------------------------------------------

AnimationManager::AnimationList::iterator AnimationManager::_LoadAnimationFromFile(const std::string& animationFilePath)
{
    AnimationList::iterator retVal = m_AnimationList.end();
    MAnim* mAnim = new MAnim( m_GraphicsDevice, m_SystemInterface, m_FileManager );

    if( mAnim->LoadFromFile( animationFilePath ) )
    {
        m_AnimationList.push_front( RefCountedAnimationPtr() );
        retVal = m_AnimationList.begin();
        m_AnimationPathsToAnimationsMap.insert( AnimationPathsToAnimationsMap::value_type( animationFilePath, retVal ) );

        retVal->refCount = 1;
        retVal->animation = mAnim;
        retVal->animationFilePath = animationFilePath;
        retVal->isCurrentlyActive = false;
        retVal->isLooping = false;
        retVal->activeAnimationListIt = m_ActiveAnimationsList.end();
    }
    else
    {
        delete mAnim;
        throw GenericException( "AnimationManager() failed to load animation \"" + animationFilePath + "\"" );
    }

    return retVal;
}

//-----------------------------------------------------------------------
