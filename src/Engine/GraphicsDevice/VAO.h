#ifndef VAO_H_INCLUDED
#define VAO_H_INCLUDED

#include <OpenGLIncludes.h>
#include <OpenGLErrorChecking.h>
#include "VBO.h"
#include <map>

class IBO;

/**
 * OpenGL Vertex Array Object Wrapper class
 */
class VAO
{
public:
    VAO();
    ~VAO();

    /**
     * If newly added VBO has one or more attributes which are the same as the attributes of the
     * previously added VBOs, the old attributes are overwritten.
     *
     * Note: After executing this function, VAO needs to be binded again before use
     *
     * @param vbo - Pointer to a VBO ownership of which is taken by the VAO(i.e. only VAO is allowed to delete it)
     */
    void AddVBO(VBO* vbo);

    /**
     * Note: After executing this function, VAO needs to be binded again before use
     * @param ibo - Pointer to an IBO object ownership of which is taken by the VAO(i.e. only VAO is allowed to delete it)
     */
    void SetIBO(IBO* ibo);

    inline void Bind() const
    {
        glBindVertexArray( m_Id );
        CheckForGLErrors();
    }

    inline void Unbind() const
    {
        glBindVertexArray( 0 );
        CheckForGLErrors();
    }

    inline VBO::VertexAttrMask GetVertexAtributes() const
    {
        return m_VertexAttributesSuppliedByAllVBOsCombined;
    }

private:
    // Used to prevent copying
    VAO& operator = (const VAO& other);
    VAO(const VAO& other);

    bool _IsAttributeSuppliedByExistingVBO( VBO::VertexAttrIdx attrIdx ) const;
    void _DissociateAttributeFromExistingVBO( VBO::VertexAttrIdx attrIdx );

    GLuint m_Id;
    IBO* m_AttachedIBO;
    VBO* m_AttachedVBOs[VBO::e_VertexAttrIdxCount];
    std::map<VBO*, VBO::VertexAttrMask> m_VBOsToAttributesMap; /// Records which attributes are supplied by which VBO
    VBO::VertexAttrMask m_VertexAttributesSuppliedByAllVBOsCombined;
};

#endif // VAO_H_INCLUDED
