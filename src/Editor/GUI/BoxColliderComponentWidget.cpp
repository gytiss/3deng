#include "BoxColliderComponentWidget.h"
#include <GUI/GUIDefaults.h>

//-----------------------------------------------------------------------

BoxColliderComponentWidget::BoxColliderComponentWidget(CategorizedListWidget* listWidgetToAddOurselvesTo,
                                                       QWidget* parent)
: ColliderWidgetBase<Ui::BoxColliderComponentUI, BoxCollider>( listWidgetToAddOurselvesTo, "Box Collider", parent )
{
    connect( m_UI->xSizeLineEdit, SIGNAL( editingFinished() ),
             this, SLOT( XSizeLineEditEditingFinished() ) );

    connect( m_UI->ySizeLineEdit, SIGNAL( editingFinished() ),
             this, SLOT( YSizeLineEditEditingFinished() ) );

    connect( m_UI->zSizeLineEdit, SIGNAL( editingFinished() ),
             this, SLOT( ZSizeLineEditEditingFinished() ) );

    m_UI->xSizeLineEdit->setValidator( &m_FloatValueValidator );
    m_UI->ySizeLineEdit->setValidator( &m_FloatValueValidator );
    m_UI->zSizeLineEdit->setValidator( &m_FloatValueValidator );
}

//-----------------------------------------------------------------------

BoxColliderComponentWidget::~BoxColliderComponentWidget()
{

}

//-----------------------------------------------------------------------

void BoxColliderComponentWidget::SetBoxColliderComponent(BoxCollider* collider)
{
    _SetColliderComponent( collider );

    if( collider )
    {
        GUIHelpers::SetNumericLineEditValue( m_UI->xSizeLineEdit, collider->GetSizeX() );
        GUIHelpers::SetNumericLineEditValue( m_UI->ySizeLineEdit, collider->GetSizeY() );
        GUIHelpers::SetNumericLineEditValue( m_UI->zSizeLineEdit, collider->GetSizeZ() );
    }
}

//-----------------------------------------------------------------------

void BoxColliderComponentWidget::ResetPaletteToDefault()
{
    m_UI->boxColliderComponentContentWidget->setPalette( GUIDefaults::GetSingleton().DefaultPalette() );
}

//-----------------------------------------------------------------------

void BoxColliderComponentWidget::XSizeLineEditEditingFinished()
{
    if( m_ColliderComponent )
    {
        m_ColliderComponent->SetSizeX( GUIHelpers::GetNumericLineEditValue( m_UI->xSizeLineEdit ) );
    }
}

//-----------------------------------------------------------------------

void BoxColliderComponentWidget::YSizeLineEditEditingFinished()
{
    if( m_ColliderComponent )
    {
        m_ColliderComponent->SetSizeY( GUIHelpers::GetNumericLineEditValue( m_UI->ySizeLineEdit ) );
    }
}

//-----------------------------------------------------------------------

void BoxColliderComponentWidget::ZSizeLineEditEditingFinished()
{
    if( m_ColliderComponent )
    {
        m_ColliderComponent->SetSizeZ( GUIHelpers::GetNumericLineEditValue( m_UI->zSizeLineEdit ) );
    }
}

//-----------------------------------------------------------------------
