#ifndef MESHMANAGER_H_INCLUDED
#define MESHMANAGER_H_INCLUDED

#include <map>
#include <list>
#include <string>
#include <vector>
#include <Resources/Mesh.h>
#include <Resources/InternalMeshData.h>

class FileManager;

class MeshManager
{
public:
    enum InternalMeshType
    {
        e_InternalMeshTypeCone = 0,
        e_InternalMeshTypeSphere = 1,
        e_InternalMeshTypeCylinder = 2,
        e_InternalMeshTypeCube,
        e_InternalMeshTypePlane,

        e_InternalMeshCount // Not an internal mesh type (Used for counting)
    };

private:
    struct RefCountedMeshPtr
    {
        RefCountedMeshPtr()
        : refCount( 0 ),
          modelFilePath(),
          uniqueMeshID(),
          meshIndexWithinTheModel( 0 ),
          meshDataInCPUReadableMemory( nullptr ),
          mesh( nullptr ),
          isInternalMesh( false ),
          internalMeshType( e_InternalMeshCount )
        {
        }

        RefCountedMeshPtr& operator= (const RefCountedMeshPtr& other)
        {
            refCount = other.refCount;
            modelFilePath = other.modelFilePath;
            uniqueMeshID = other.uniqueMeshID;
            meshIndexWithinTheModel = other.meshIndexWithinTheModel;
            meshDataInCPUReadableMemory = other.meshDataInCPUReadableMemory;
            mesh = other.mesh;
            isInternalMesh = other.isInternalMesh;
            internalMeshType = other.internalMeshType;

            return *this;
        }

        RefCountedMeshPtr(const RefCountedMeshPtr& other)
        {
            operator=( other );
        }

        unsigned int refCount;
        std::string modelFilePath; /// Path of the model file this mesh was loaded from
        std::string uniqueMeshID;
        uint16_t meshIndexWithinTheModel;
        MObj::Mesh* meshDataInCPUReadableMemory;
        Mesh* mesh;
        bool isInternalMesh;
        InternalMeshType internalMeshType;
    };

    typedef std::list<RefCountedMeshPtr> MeshList;
    typedef std::map<std::string, MeshList::iterator> UniqueMeshIDsToMeshesMap;

public:
    class MeshHandle
    {
    public:
        friend class MeshManager;

        MeshHandle()
        : m_MeshListIt(),
          m_Valid( false )
        {
        }

        MeshHandle& operator= (const MeshHandle& other)
        {
            m_MeshListIt = other.m_MeshListIt;
            m_Valid = other.m_Valid;
            return *this;
        }

        MeshHandle(const MeshHandle& other)
        {
            operator= ( other );
        }

        Mesh* operator->()
        {
            return (*m_MeshListIt).mesh;
        }

        const Mesh* operator->() const
        {
            return (*m_MeshListIt).mesh;
        }

        Mesh& operator*()
        {
            return *(*m_MeshListIt).mesh;
        }

        const Mesh& operator*() const
        {
            return *(*m_MeshListIt).mesh;
        }

        bool IsValid() const
        {
            return m_Valid;
        }

    protected:
        void Invalidate()
        {
            m_Valid = false;
        }

        MeshHandle(MeshList::iterator meshListIt)
        : m_MeshListIt( meshListIt ),
          m_Valid( true )
        {
        }

        MeshList::iterator m_MeshListIt;  // Iterator to the models list i.e. m_MeshList
        bool m_Valid;
    };

    class ModelMeshes : protected std::vector<MeshHandle>
    {
    public:
        friend class MeshManager;

        using std::vector<MeshHandle>::operator[];

        inline bool FindMeshByName(const std::string& name, MeshHandle& outMeshHandle)
        {
            bool  found = false;

            for(iterator it = begin(); !found && (it != end()); ++it)
            {
                MeshHandle& meshHandle = *it;

                if( meshHandle->GetName().compare( name ) == 0 )
                {
                    outMeshHandle = meshHandle;
                    found = true;
                }
            }

            return found;
        }

        inline size_t GetMeshCount() const
        {
            return size();
        }
    };

    MeshManager(FileManager& fileManager);
    ~MeshManager();

    MeshHandle GetInternalMeshHandle(InternalMeshType type);

    void LoadModel(const std::string& modelFilePath, ModelMeshes& outModelMeshes);
    void ReleaseMesh(MeshHandle& handle);

    void LoadMeshDataIntoCPUReadableMemory(MeshHandle& handle);

private:
    static const char* c_InternalMeshPathPrefix;

    // Used to prevent copying
    MeshManager& operator = (const MeshManager& other);
    MeshManager(const MeshManager& other);

    MeshList::iterator _LoadInternalMesh(InternalMeshType type);
    void _LoadModelFromFile(const std::string& modelFilePath, ModelMeshes& outModelMeshes);

    VAO* _UploadInternalMeshToGPU(const EngineTypes::VertexIndexType* indexBuffer,
                                  size_t indexCount,
                                  const InternalMeshData::MeshVertex* vertexBuffer,
                                  size_t vertexCount) const;
    VAO* _UploadMObjDataToGPU(const MObj::Mesh& mesh) const;

    FileManager& m_FileManager;
    MeshList m_MeshList;
    UniqueMeshIDsToMeshesMap m_UniqueMeshIDsToMeshesMap;
    static bool m_ClassAlreadyInstantiated;
    static const char* m_InternalMeshNames[e_InternalMeshCount];
};

#endif // MESHMANAGER_H_INCLUDED
