#include "SoundSource.h"
#include "GameObject.h"
#include <MathUtils.h>
#include <SystemInterface.h>
#include <cmath>

//-----------------------------------------------------------------------

SoundSource::SoundSource(GameObject& owningGameObject,
                         SystemInterface& systemInterface,
                         SoundManager& soundManager,
                         AudioDataManager& audioDataManager)
: Component( e_TypeSoundSource, "SoundSource", owningGameObject ),
  m_SystemInterface( systemInterface ),
  m_SoundManager( soundManager ),
  m_AudioDataManager( audioDataManager ),
  m_AudioDataHandle(),
  m_FloatParamValues(),
  m_Velocity( 0.0f ),
  m_Position( 0.0f ),
  m_Direction( 0.0f ),
  m_IsRelative( false ),
  m_IsLooping( false ),
  m_PositionNotYetSet( true ),
  m_LastPositionUpdateTimeInSeconds( 0.0 )
{
    GetOwningGameObject().SetComponent( this );

    SetPosition( MathUtils::MatrixToSceneTransform( GetOwningGameObject().GetGlobalTransformMatrix() ).position );

    AudioDevice::Source::GetDefaultFloatParamValue( m_FloatParamValues );

    m_HardwareSoundSourceHandle = m_SoundManager.AllocateHardwareSoundSource();
}

//-----------------------------------------------------------------------

SoundSource::~SoundSource()
{
    SetEnabled( false );

    m_SoundManager.ReleaseHardwareSoundSource( m_HardwareSoundSourceHandle );

    if( m_AudioDataHandle.IsValid() )
    {
        m_AudioDataManager.ReleaseAudioData( m_AudioDataHandle );
    }
}

//-----------------------------------------------------------------------

void SoundSource::SetEnabled(bool val)
{
//    Component::SetEnabled( val );
//
//    MeshRenderer* meshRenderer = GetOwningGameObject().GetComponent<MeshRenderer>();
//    MeshCollider* meshCollider = GetOwningGameObject().GetComponent<MeshCollider>();
//
//    if( meshRenderer )
//    {
//        // At this point if (val == false), calling _FindMesh() will
//        // make the MeshRenderer clear all out all of the renderables
//        meshRenderer->_FindMesh();
//    }
//
//    if( meshCollider )
//    {
//        // At this point if (val == false) calling _FindAndSetMesh() will make the MeshCollider to
//        // go back into the uninitialized state, because it can no longer find a mesh it could use
//        meshCollider->_FindAndSetMesh();
//    }
}

//-----------------------------------------------------------------------

void SoundSource::SetAudioData(const AudioDataManager::AudioDataHandle& audioDataHandle)
{
    if( m_SoundManager.IsHardwareSoundSourceActive( m_HardwareSoundSourceHandle ) )
    {
        m_SoundManager.StopSource( m_HardwareSoundSourceHandle );
    }

    if( m_AudioDataHandle.IsValid() )
    {
        m_AudioDataManager.ReleaseAudioData( m_AudioDataHandle );
    }

    m_AudioDataHandle = audioDataHandle;
}

//-----------------------------------------------------------------------

void SoundSource::Play()
{
    if( m_AudioDataHandle.IsValid() )
    {
        if( !m_SoundManager.IsHardwareSoundSourceActive( m_HardwareSoundSourceHandle ) )
        {
            m_SoundManager.ActivateHardwareSoundSource( m_HardwareSoundSourceHandle );

            m_HardwareSoundSourceHandle->SetFloatParameters( m_FloatParamValues );
            m_HardwareSoundSourceHandle->SetPosition( m_Position );
            m_HardwareSoundSourceHandle->SetVelocity( m_Velocity );
            m_HardwareSoundSourceHandle->SetDirection( m_Direction );
            m_HardwareSoundSourceHandle->SetIsRelative( m_IsRelative );
        }

        m_SoundManager.PlaySource( m_HardwareSoundSourceHandle,
                                   m_AudioDataHandle,
                                   m_IsLooping );
    }
}

//-----------------------------------------------------------------------

void SoundSource::Pause()
{
    m_PositionNotYetSet = true;

    if( m_SoundManager.IsHardwareSoundSourceActive( m_HardwareSoundSourceHandle ) )
    {
        m_SoundManager.PauseSource( m_HardwareSoundSourceHandle );
    }
}

//-----------------------------------------------------------------------

void SoundSource::Stop()
{
    m_PositionNotYetSet = true;

    if( m_SoundManager.IsHardwareSoundSourceActive( m_HardwareSoundSourceHandle ) )
    {
        m_SoundManager.StopSource( m_HardwareSoundSourceHandle );
    }
}

//-----------------------------------------------------------------------

bool SoundSource::IsStopped()
{
    bool retVal = true;

    if( m_SoundManager.IsHardwareSoundSourceActive( m_HardwareSoundSourceHandle ) )
    {
        retVal = m_HardwareSoundSourceHandle->IsStopped();
    }

    return retVal;
}

//-----------------------------------------------------------------------

void SoundSource::SetFloatParameter(AudioDevice::Source::FloatParam param, float value)
{
    m_FloatParamValues[param] = value;

    if( m_SoundManager.IsHardwareSoundSourceActive( m_HardwareSoundSourceHandle ) )
    {
        m_HardwareSoundSourceHandle->SetFloatParameter( param, value );
    }
}

//-----------------------------------------------------------------------

float SoundSource::GetFloatParameter(AudioDevice::Source::FloatParam param)
{
    return m_FloatParamValues[param];
}

//-----------------------------------------------------------------------

void SoundSource::SetPosition(const glm::vec3& position)
{
    const double positionUpdateTimeInSeconds = m_SystemInterface.GetTicksInSeconds();

    if( m_PositionNotYetSet == false )
    {
        m_Velocity = ( ( position - m_Position ) / static_cast<float>( m_LastPositionUpdateTimeInSeconds - positionUpdateTimeInSeconds ) );

        // Just in case the floating point value ever becomes invalid
        if( !std::isnormal( m_Velocity.x ) ||
            !std::isnormal( m_Velocity.y ) ||
            !std::isnormal( m_Velocity.z ) )
        {
            m_Velocity = glm::vec3( 0.0f );
        }

        if( m_SoundManager.IsHardwareSoundSourceActive( m_HardwareSoundSourceHandle ) )
        {
            m_HardwareSoundSourceHandle->SetVelocity( m_Velocity );
        }
    }

    m_PositionNotYetSet = false;
    m_LastPositionUpdateTimeInSeconds = positionUpdateTimeInSeconds;
    m_Position = position;

    if( m_SoundManager.IsHardwareSoundSourceActive( m_HardwareSoundSourceHandle ) )
    {
        m_HardwareSoundSourceHandle->SetPosition( m_Position );
    }
}

//-----------------------------------------------------------------------


void SoundSource::SetDirection(const glm::vec3& directionVector)
{
    m_Direction = directionVector;

    if( m_SoundManager.IsHardwareSoundSourceActive( m_HardwareSoundSourceHandle ) )
    {
        m_HardwareSoundSourceHandle->SetDirection( m_Direction );
    }
}

//-----------------------------------------------------------------------

const glm::vec3& SoundSource::GetPosition() const
{
    return m_Position;
}

//-----------------------------------------------------------------------

const glm::vec3& SoundSource::GetVelocity() const
{
    return m_Velocity;
}

//-----------------------------------------------------------------------


const glm::vec3& SoundSource::GetDirection() const
{
    return m_Direction;
}

//-----------------------------------------------------------------------

void SoundSource::SetIsRelative(bool value)
{
    m_IsRelative = value;

    if( m_SoundManager.IsHardwareSoundSourceActive( m_HardwareSoundSourceHandle ) )
    {
        m_HardwareSoundSourceHandle->SetIsRelative( m_IsRelative );
    }
}

//-----------------------------------------------------------------------

bool SoundSource::IsRelative() const
{
    return m_IsRelative;
}

//-----------------------------------------------------------------------

void SoundSource::SetIsLooping(bool value)
{
    m_IsLooping = value;
}

//-----------------------------------------------------------------------

bool SoundSource::IsLooping() const
{
    return m_IsLooping;
}

//-----------------------------------------------------------------------

