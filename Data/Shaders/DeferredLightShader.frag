
#pragma IncludeExternalSource "LightShaderCommonCode.frag"

#if !defined(USING_OPENGL_ES)
    #define COLOR_PRECISION HIGHEST_PRECISION
#else
    #define COLOR_PRECISION LOWEST_PRECISION
#endif

uniform sampler2D u_DepthBuffer;
uniform sampler2D u_GBufferDiffuseMap;
uniform sampler2D u_GBufferVertexNormalMap_ViewSpace;
uniform HIGHEST_PRECISION vec2 u_ScreenSize;
uniform COLOR_PRECISION vec3 u_LightColor;
uniform LOWEST_PRECISION float u_LightAmbientIntensity;
uniform LOWEST_PRECISION float u_LightDiffuseIntensity;
uniform HIGHEST_PRECISION float u_LightRadius;
uniform HIGHEST_PRECISION vec3 u_LightPosition_ViewSpace;

uniform HIGHEST_PRECISION float u_ProjectionA;
uniform HIGHEST_PRECISION float u_ProjectionB;

#if defined(DIRECTIONAL_LIGHT)
in LOWEST_PRECISION vec3 ViewVector;
#else
in HIGHEST_PRECISION vec3 LightBoundingVolumeVertexPos_ViewSpace;
#endif

#if defined(SPOT_LIGHT)
uniform LOWEST_PRECISION float u_LightCutoffValue; // cos(a), where "a" is an engle between the two vectors one going from center
                                                   // of the light towards the direction of the light and the other along the
                                                   // side of the cone(towards the wider end of it). This value is used with spot lights.
#endif

#if defined(SPOT_LIGHT) || defined(DIRECTIONAL_LIGHT)
uniform LOWEST_PRECISION vec3 u_LightDirection_ViewSpace;
#endif

//float SpecularPower = 32.0f;
//float MaterialSpecularIntensity = 1.0f;

out COLOR_PRECISION vec4 FragColor;

vec2 CalcTexCoord()
{
    return gl_FragCoord.xy / u_ScreenSize;
}

#if 0
vec4 AddLinearFogToColor(in vec3 worldPos, in vec4 litColor)
{
    vec3 FogColor = vec3( 0.7529f, 0.7529f, 0.7529f ); // Grey
    float FogMinDist = 1.0f;
    float FogMaxDist = 60.0f;
    vec3 eyePos_ViewSpace = vec3(0.0f, 0.0f, 0.0f); // In View Space camera is allways at the center of the coordinate system
    float distToCamera = length( worldPos - eyePos_ViewSpace );

    float fogFactor = (FogMaxDist - distToCamera) / (FogMaxDist - FogMinDist);
    fogFactor = clamp( fogFactor, 0.0f, 1.0f );

    return vec4( mix( FogColor, litColor.xyz, fogFactor ), 1.0f);
}

vec4 AddExponentialFogToColor(in vec3 worldPos, in vec4 litColor)
{
    vec3 FogColor = vec3( 0.7529f, 0.7529f, 0.7529f ); // Grey
    float FogMinDist = 1.0f;
    float FogMaxDist = 60.0f;
    vec3 eyePos_ViewSpace = vec3(0.0f, 0.0f, 0.0f); // In View Space camera is allways at the center of the coordinate system
    float distToCamera = length( worldPos - eyePos_ViewSpace );

    float density = 0.001f;
    float fogFactor = 1.0f / pow(2.71828, (distToCamera * density) );
    fogFactor = clamp( fogFactor, 0.0f, 1.0f );

    return vec4( mix( FogColor, litColor.xyz, fogFactor ), 1.0f);
}
#endif

vec3 DecodeNormal(const in HIGHEST_PRECISION vec2 EncodedNormal)
{
    HIGHEST_PRECISION vec4 nn = vec4(EncodedNormal.xy, 0.0f, 0.0f) * vec4(2.0f, 2.0f, 0.0f, 0.0f) + vec4(-1.0f, -1.0f, 1.0f ,-1.0f);
    HIGHEST_PRECISION float l = dot(nn.xyz,-nn.xyw);
    nn.z = l;
    nn.xy *= sqrt(l);
    return nn.xyz * 2.0f + vec3(0.0f, 0.0f , -1.0f);
}

vec3 ReconstructVertexPosInViewSpaceFromDepth(const in HIGHEST_PRECISION float DepthSampleFromDepthBuffer)
{
#if defined(POINT_LIGHT) || defined(SPOT_LIGHT)
    // Clamp the view space position to the plane at Z = 1
    HIGHEST_PRECISION vec3 ViewVector = vec3(LightBoundingVolumeVertexPos_ViewSpace.xy / LightBoundingVolumeVertexPos_ViewSpace.z, 1.0f);
#endif

    // Convert sampled depth to linear view space Z (assume DepthSampleFromDepthBuffer
    // is a floating point value in the range [0,1])
    HIGHEST_PRECISION float LinearDepthValue = u_ProjectionB / (u_ProjectionA - DepthSampleFromDepthBuffer);

    // Scaling the view vector with the linear depth will give the true viewspace position
    return (ViewVector * LinearDepthValue);
}

void main()
{
    HIGHEST_PRECISION vec2 TexCoord = CalcTexCoord();
    HIGHEST_PRECISION vec3 VertexPos_ViewSpace = ReconstructVertexPosInViewSpaceFromDepth(texture(u_DepthBuffer, TexCoord).x);
    COLOR_PRECISION vec3 MaterialColor = texture(u_GBufferDiffuseMap, TexCoord).xyz;
    HIGHEST_PRECISION vec3 EncodedNormalAndSpecularIntensity = texture(u_GBufferVertexNormalMap_ViewSpace, TexCoord).xyz;
    LOWEST_PRECISION vec3 Normal_ViewSpace = DecodeNormal(EncodedNormalAndSpecularIntensity.xy);

    LOWEST_PRECISION float MaterialSpecularIntensity = EncodedNormalAndSpecularIntensity.z;
    
    //
    HIGHEST_PRECISION vec3 VertexToLightVector_ViewSpace = (VertexPos_ViewSpace - u_LightPosition_ViewSpace);
    HIGHEST_PRECISION float DistanceFromLightToVertex = length(VertexToLightVector_ViewSpace);
    LOWEST_PRECISION vec3 LightDirection_ViewSpace = ( VertexToLightVector_ViewSpace / DistanceFromLightToVertex ); // Normalize
    
    HIGHEST_PRECISION vec3 eyePos_ViewSpace = vec3(0.0f, 0.0f, 0.0f); // In View Space camera is allways at the center of the coordinate system
    LOWEST_PRECISION vec3 VertexToEye = normalize(eyePos_ViewSpace - VertexPos_ViewSpace);
    // 

#if defined(POINT_LIGHT)
    COLOR_PRECISION vec3 FinalColor =  ( MaterialColor * CalcPointLight( u_LightRadius,
                                                                         MaterialSpecularIntensity,
                                                                         gSpecularPower,
                                                                         u_LightAmbientIntensity,
                                                                         u_LightDiffuseIntensity,
                                                                         u_LightColor,
                                                                         LightDirection_ViewSpace,
                                                                         DistanceFromLightToVertex,
                                                                         VertexToEye,
                                                                         Normal_ViewSpace ).xyz );
#elif defined(SPOT_LIGHT)
    COLOR_PRECISION vec3 FinalColor = ( MaterialColor * CalcSpotLight( u_LightCutoffValue,
                                                                       u_LightRadius,
                                                                       MaterialSpecularIntensity,
                                                                       gSpecularPower,
                                                                       u_LightAmbientIntensity,
                                                                       u_LightDiffuseIntensity,
                                                                       u_LightColor,
                                                                       u_LightDirection_ViewSpace,
                                                                       LightDirection_ViewSpace,
                                                                       DistanceFromLightToVertex,
                                                                       VertexToEye,
                                                                       Normal_ViewSpace ).xyz );
#elif defined(DIRECTIONAL_LIGHT)
    COLOR_PRECISION vec3 FinalColor =  ( MaterialColor * CalcDirectionalLight( MaterialSpecularIntensity,
                                                                               gSpecularPower,
                                                                               u_LightAmbientIntensity,
                                                                               u_LightDiffuseIntensity,
                                                                               u_LightColor,
                                                                               u_LightDirection_ViewSpace,
                                                                               VertexToEye,
                                                                               Normal_ViewSpace ).xyz );
#else
    COLOR_PRECISION vec3 FinalColor =  MaterialColor;
#endif

    FragColor = vec4( FinalColor, 1.0f );
}
