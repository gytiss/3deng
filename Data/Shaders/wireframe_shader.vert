
precision highp float;

in vec3 in_VertexPos;
in vec3 in_VertexNormal;

uniform mat4 mvmatrix;
uniform mat4 pmatrix;
uniform mat3 nmatrix;

out vec4 color;

void main(void)
{
    color = vec4(1.0f,1.0f,1.0f,1.0f);
    gl_Position = (pmatrix*mvmatrix) * vec4(in_VertexPos, 1.0f);
}
