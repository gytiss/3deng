#include "SpotLight.h"
#include "ShaderPrograms.h"

//-----------------------------------------------------------------------

SpotLight::SpotLight(ShaderProgSpotLightUberShader& shaderProg,
                     ShaderProgLightingStencilPass& stencilShaderProg,
                     ShaderProgDebugObjects& debugShaderProg)
: m_Enabled( true ),
  m_Color( 1.0f, 1.0f, 1.0f ), // White
  m_Radius( 1.0f ),
  m_AmbientIntensity( 0.0f ),
  m_DiffuseIntensity( 1.0f ),
  m_CutOffValueAngleInDegrees( 20.0f ),
  m_CutOffValue( cos( MathUtils::DegreesToRadians( m_CutOffValueAngleInDegrees ) ) ),
  m_PositionInWorldSpace( 0.0f, 0.0f, 0.0f ),
  m_DirectionInWorldSpace( 0.0f, -1.0f, 0.0f ), // Towards -Y axis
  m_InfluenceMesh(),
  m_ShaderProg( shaderProg ),
  m_StencilShaderProg( stencilShaderProg ),
  m_DebugObjectsShaderProg( debugShaderProg )
{

}

//-----------------------------------------------------------------------

void SpotLight::RenderForLightPass(const glm::mat4& viewMatrix,
                                   const glm::mat4& projectionMatrix)
{
    m_ShaderProg.Use();
    m_ShaderProg.SetUniformLightColor( m_Color );
    m_ShaderProg.SetUniformLightRadius( m_Radius );
    m_ShaderProg.SetUniformLightAmbientIntensity( m_AmbientIntensity );
    m_ShaderProg.SetUniformLightDiffuseIntensity( m_DiffuseIntensity );
    m_ShaderProg.SetUniformLightCutoffValue( m_CutOffValue );
    const glm::vec3 dirInViewSpace = glm::vec3( viewMatrix * glm::vec4( m_DirectionInWorldSpace, 0.0f ) );
    const glm::vec3 posInViewSpace = glm::vec3( viewMatrix * glm::vec4( m_PositionInWorldSpace, 1.0f ) );
    m_ShaderProg.SetUniformLightDirectionInViewSpace( dirInViewSpace );
    m_ShaderProg.SetUniformLightPositionInViewSpace( posInViewSpace );

    m_InfluenceMesh.Render( m_PositionInWorldSpace,
                            m_DirectionInWorldSpace,
                            m_Radius,
                            m_CutOffValueAngleInDegrees,
                            viewMatrix,
                            projectionMatrix,
                            m_ShaderProg );
}

//-----------------------------------------------------------------------

void SpotLight::RenderDebugData(const glm::mat4& viewMatrix,
                                const glm::mat4& projectionMatrix)
{
    m_InfluenceMesh.RenderDebugData( m_PositionInWorldSpace,
                                     m_DirectionInWorldSpace,
                                     m_Radius,
                                     m_CutOffValueAngleInDegrees,
                                     viewMatrix,
                                     projectionMatrix,
                                     m_DebugObjectsShaderProg );
}

//-----------------------------------------------------------------------

void SpotLight::RenderForStencilPass(const glm::mat4& viewMatrix,
                                     const glm::mat4& projectionMatrix)
{
    m_StencilShaderProg.Use();

    m_InfluenceMesh.RenderStencilPass( m_PositionInWorldSpace,
                                       m_DirectionInWorldSpace,
                                       m_Radius,
                                       m_CutOffValueAngleInDegrees,
                                       viewMatrix,
                                       projectionMatrix,
                                       m_StencilShaderProg );
}

//-----------------------------------------------------------------------
