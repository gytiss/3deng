

in vec3 in_VertexPos;
in vec2 in_VertexTextureCoords;

#if defined(RENDERING_SKINNED_MESH)
    in vec4 in_VertexBoneWeights;
    in vec4 in_VertexBoneIndices;
#endif

// Output data ; will be interpolated for each fragment.
out vec2 TexCoord;

uniform mat4 u_ModelViewProjectionMatrix;

#if defined(RENDERING_SKINNED_MESH)
    uniform mat4 u_JointMatrices[58]; // All bones come in a huge list!

    void VertexSkinning(const vec3 inPosition,
                        inout vec4 objPos)
    {
        mat4 matTransform = u_JointMatrices[int(in_VertexBoneIndices.x)] * in_VertexBoneWeights.x;
        if (in_VertexBoneWeights.x > 0.0f)
        {
            if (in_VertexBoneWeights.y > 0.0f)
            {
                matTransform += u_JointMatrices[int(in_VertexBoneIndices.y)] * in_VertexBoneWeights.y;
                if (in_VertexBoneWeights.z > 0.0f)
                {
                    matTransform += u_JointMatrices[int(in_VertexBoneIndices.z)] * in_VertexBoneWeights.z;
                    float finalWeight = 1.0f - ( in_VertexBoneWeights.x + in_VertexBoneWeights.y + in_VertexBoneWeights.z );
                    matTransform += u_JointMatrices[int(in_VertexBoneIndices.w)] * finalWeight;
                }
            }
        }

        objPos =  matTransform * vec4( inPosition, 1.0f );
    }
#endif

void main()
{
    vec4 objPos = vec4( 0.0f, 0.0f, 0.0f, 1.0f );

    #if defined(RENDERING_SKINNED_MESH)
        VertexSkinning( in_VertexPos, objPos );
    #else
        objPos = vec4( in_VertexPos, 1.0f );
    #endif

    gl_Position = u_ModelViewProjectionMatrix * objPos;

    TexCoord = in_VertexTextureCoords;
}
