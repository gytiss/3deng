#ifndef CAPSULECOLLIDERCOMPONENTWIDGET_H_INCLUDED
#define CAPSULECOLLIDERCOMPONENTWIDGET_H_INCLUDED

#include "ColliderWidgetBase.h"
#include <ui_CapsuleColliderComponentUI.h>
#include <Engine/Scene/CapsuleCollider.h>

class CapsuleColliderComponentWidget : public ColliderWidgetBase<Ui::CapsuleColliderComponentUI, CapsuleCollider>
{
    Q_OBJECT

public:
    CapsuleColliderComponentWidget(CategorizedListWidget* listWidgetToAddOurselvesTo, QWidget* parent = 0);
    ~CapsuleColliderComponentWidget();

    void SetCapsuleColliderComponent(CapsuleCollider* collider);

    void ResetPaletteToDefault();

private slots:
    void RadiusLineEditEditingFinished();
    void HeightLineEditEditingFinished();
    void DirectionComboBoxCurrentIndexChanged(int index);
};

#endif // CAPSULECOLLIDERCOMPONENTWIDGET_H_INCLUDED
