#!/usr/bin/env bash

pushd `dirname $0` > /dev/null # Save script call dir and "cd" into the script dir
SCRIPT_DIR=`pwd`
popd > /dev/null # "cd" back into the script call dir

rm -rf "$SCRIPT_DIR/assets/Data"
rm -rf "$SCRIPT_DIR/assets/DemoRoomData"

mkdir -p "$SCRIPT_DIR/assets/Data"
mkdir -p "$SCRIPT_DIR/assets/DemoRoomData"

cd "$SCRIPT_DIR/../Data"
rsync -a --exclude="*.tga" "$SCRIPT_DIR/../Data/" "$SCRIPT_DIR/assets/Data"

cd "$SCRIPT_DIR/../DemoRoomData"
rsync -a --exclude="*.tga" "$SCRIPT_DIR/../DemoRoomData/" "$SCRIPT_DIR/assets/DemoRoomData"

cd $SCRIPT_DIR