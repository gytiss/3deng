#include "ConeCollider.h"
#include <cassert>

//-----------------------------------------------------------------------

ConeCollider::ConeCollider(GameObject& owningGameObject,
                           float radius,
                           float height,
                           PhysicsManager& physicsManager)
: Collider( owningGameObject, e_ColliderTypeCone, "Cone Collider", physicsManager ),
  m_Direction( e_DirectionY )
{
    _Initialize( _CreateConeCollisionShape( m_Direction, radius, height ) );
}

//-----------------------------------------------------------------------

void ConeCollider::SetRadius(float radius)
{
    _ChangeCollisionShape( _CreateConeCollisionShape( m_Direction, radius, GetHeight() ) );
}

//-----------------------------------------------------------------------

float ConeCollider::GetRadius() const
{
    return m_PhysicsManager.GetConeCollisionShapeRadius( _GetCollisionShapeHandle() );
}

//-----------------------------------------------------------------------

void ConeCollider::SetHeight(float height)
{
    _ChangeCollisionShape( _CreateConeCollisionShape( m_Direction, GetRadius(), height ) );
}

//-----------------------------------------------------------------------

float ConeCollider::GetHeight() const
{
    return m_PhysicsManager.GetConeCollisionShapeHeight( _GetCollisionShapeHandle() );
}

//-----------------------------------------------------------------------

void ConeCollider::SetDirection(Direction dir)
{
    if( dir != m_Direction )
    {
        m_Direction = dir;
        _ChangeCollisionShape( _CreateConeCollisionShape( m_Direction, GetRadius(), GetHeight() ) );
    }
}

//-----------------------------------------------------------------------

ConeCollider::Direction ConeCollider::GetDirection() const
{
    return m_Direction;
}

//-----------------------------------------------------------------------

PhysicsManager::CollisionShapeHandle ConeCollider::_CreateConeCollisionShape(Direction dir,
                                                                             float radius,
                                                                             float height)
{
    PhysicsManager::CollisionShapeHandle retVal;

    if( dir == e_DirectionX )
    {
        retVal = m_PhysicsManager.CreateConeXCollisionShape( radius, height );
    }
    else if( dir == e_DirectionY )
    {
        retVal = m_PhysicsManager.CreateConeYCollisionShape( radius, height );
    }
    else if( dir == e_DirectionZ )
    {
        retVal = m_PhysicsManager.CreateConeZCollisionShape( radius, height );
    }
    else
    {
        assert( 0 && "Not implemented direction" );
    }

    return retVal;
}

//-----------------------------------------------------------------------
