#ifndef CONECOLLIDERCOMPONENTWIDGET_H_INCLUDED
#define CONECOLLIDERCOMPONENTWIDGET_H_INCLUDED

#include "ColliderWidgetBase.h"
#include <ui_ConeColliderComponentUI.h>
#include <Engine/Scene/ConeCollider.h>

class ConeColliderComponentWidget : public ColliderWidgetBase<Ui::ConeColliderComponentUI, ConeCollider>
{
    Q_OBJECT

public:
    ConeColliderComponentWidget(CategorizedListWidget* listWidgetToAddOurselvesTo, QWidget* parent = 0);
    ~ConeColliderComponentWidget();

    void SetConeColliderComponent(ConeCollider* collider);

    void ResetPaletteToDefault();

private slots:
    void RadiusLineEditEditingFinished();
    void HeightLineEditEditingFinished();
    void DirectionComboBoxCurrentIndexChanged(int index);
};

#endif // CONECOLLIDERCOMPONENTWIDGET_H_INCLUDED
