#include "LightManager.h"
#include "DirectionalLight.h"
#include "PointLight.h"
#include "SpotLight.h"
#include <GraphicsDevice/FBO.h>
#include <GraphicsDevice/GraphicsDevice.h>
#include <CommonDefinitions.h>
#include <OpenGLErrorChecking.h>

//-----------------------------------------------------------------------

LightManager::LightManager(GraphicsDevice& graphicsDevice,
                           FBO& lightResultsFBO,
                           SystemInterface& systemInterface,
                           FileManager& fileManager)
: m_GraphicsDevice( graphicsDevice ),
  m_LightResultsFBO( lightResultsFBO ),
  m_SystemInterface( systemInterface ),
  m_DrawableSurfaceSize( m_SystemInterface.GetDrawableSurfaceSize() ),
  m_DirectionalLight( nullptr ),
  m_PointLights(),
  m_SpotLights(),
  m_PointLightShaderProg( fileManager ),
  m_SpotLightShaderProg( fileManager ),
  m_DirectionLightShaderProg( fileManager ),
  m_LightingStencilPassShaderProg( fileManager ),
  m_DebugObjectsShaderProg( fileManager )
{
    m_PointLightShaderProg.LoadCompileAndLink();
    m_SpotLightShaderProg.LoadCompileAndLink();
    m_DirectionLightShaderProg.LoadCompileAndLink();
    m_LightingStencilPassShaderProg.LoadCompileAndLink();
    m_DebugObjectsShaderProg.LoadCompileAndLink();

    _DrawableSurfaceSizeChaged( m_DrawableSurfaceSize, this );

    m_SystemInterface.RegisterDrawableSurfaceSizeChangedCallback( _DrawableSurfaceSizeChaged, this );
}

LightManager::~LightManager()
{
    m_SystemInterface.DeregisterDrawableSurfaceSizeChangedCallback( _DrawableSurfaceSizeChaged, this );

    for (PointLightList_t::iterator it = m_PointLights.begin(); it != m_PointLights.end(); ++it)
    {
        delete *it;
    }

    for (SpotLightList_t::iterator it = m_SpotLights.begin(); it != m_SpotLights.end(); ++it)
    {
        delete *it;
    }

    if( m_DirectionalLight != nullptr )
    {
        delete m_DirectionalLight;
    }
}

//-----------------------------------------------------------------------

PointLight* LightManager::CreateNewPointLight()
{
    PointLight* light = new PointLight(m_PointLightShaderProg, m_LightingStencilPassShaderProg);
    m_PointLights.push_back( light );
    return light;
}

//-----------------------------------------------------------------------

SpotLight* LightManager::CreateNewSpotLight()
{
    SpotLight* light = new SpotLight(m_SpotLightShaderProg,
                                     m_LightingStencilPassShaderProg,
                                     m_DebugObjectsShaderProg);
    m_SpotLights.push_back( light );
    return light;
}

//-----------------------------------------------------------------------

void LightManager::DeletePointLight(PointLight* light)
{
    if( light )
    {
        m_PointLights.remove( light );
    }
}

//-----------------------------------------------------------------------

void LightManager::DeleteSpotLight(SpotLight* light)
{
    if( light )
    {
        m_SpotLights.remove( light );
    }
}

//-----------------------------------------------------------------------

DirectionalLight* LightManager::GetMainDirectionalLight()
{
    if( m_DirectionalLight == nullptr )
    {
        m_DirectionalLight = new DirectionalLight( m_DirectionLightShaderProg );
    }

    return m_DirectionalLight;
}

//-----------------------------------------------------------------------

void LightManager::RenderLights(const glm::mat4& viewMatrix,
                                const glm::mat4& projectionMatrix,
                                float frustumNearClippingPlaneDistance,
                                float frustumFarClippingPlaneDistance)
{
    // We need stencil to be enabled in the stencil pass to get the stencil buffer
    // updated and we also need it in the light pass because we render the light
    // only if the stencil passes.
    m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionStencilTesting );
    m_GraphicsDevice.OptionEnable( GraphicsDevice::e_OptionStencilTesting );

    // Calculate projection constants used for converting depth buffer values to view space vertex positions
    const float projectionA = ( frustumFarClippingPlaneDistance / ( frustumFarClippingPlaneDistance - frustumNearClippingPlaneDistance ) );
    const float projectionB = ( ( -frustumFarClippingPlaneDistance * frustumNearClippingPlaneDistance ) / ( frustumFarClippingPlaneDistance - frustumNearClippingPlaneDistance ) );

    m_PointLightShaderProg.Use();
    m_PointLightShaderProg.SetUniformProjectionA( projectionA );
    m_PointLightShaderProg.SetUniformProjectionB( projectionB );
    for (PointLightList_t::iterator it = m_PointLights.begin(); it != m_PointLights.end(); ++it)
    {
        PointLight* light = *it;
        if( light->Enabled() )
        {
            _PointLightStencilPass( *light, viewMatrix, projectionMatrix );
            _PointLightPass( *light, viewMatrix, projectionMatrix );
        }
    }

    m_SpotLightShaderProg.Use();
    m_SpotLightShaderProg.SetUniformProjectionA( projectionA );
    m_SpotLightShaderProg.SetUniformProjectionB( projectionB );
    for (SpotLightList_t::iterator it = m_SpotLights.begin(); it != m_SpotLights.end(); ++it)
    {
        SpotLight* light = *it;
        if( light->Enabled() )
        {
            _SpotLightStencilPass( *light, viewMatrix, projectionMatrix );
            _SpotLightPass( *light, viewMatrix, projectionMatrix );
        }
    }

    // The directional light does not need a stencil test because its volume
    // is unlimited and the final pass simply copies the texture.
    m_GraphicsDevice.OptionDisable( GraphicsDevice::e_OptionStencilTesting );


    if( m_DirectionalLight && m_DirectionalLight->Enabled() )
    {
        m_DirectionLightShaderProg.Use();
        m_DirectionLightShaderProg.SetUniformProjectionA( projectionA );
        m_DirectionLightShaderProg.SetUniformProjectionB( projectionB );

        _DirectionalLightPass( *m_DirectionalLight, viewMatrix, projectionMatrix );
    }

    m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionStencilTesting );
}


//-----------------------------------------------------------------------

void LightManager::RenderLightsDebugData(const glm::mat4& viewMatrix,
                                         const glm::mat4& projectionMatrix)
{
    m_GraphicsDevice.SaveCurrentPolygonMode();
    m_GraphicsDevice.SetPolygonMode( GraphicsDevice::e_PolygonModeLine );

//    for (PointLightList_t::iterator it = m_PointLights.begin(); it != m_PointLights.end(); ++it)
//    {
//        PointLight* light = *it;
//        if( light->Enabled() )
//        {
//
//        }
//    }

    for (SpotLightList_t::iterator it = m_SpotLights.begin(); it != m_SpotLights.end(); ++it)
    {
        SpotLight* light = *it;
        if( light->Enabled() )
        {
            light->RenderDebugData( viewMatrix, projectionMatrix );
        }
    }

    m_GraphicsDevice.RestorePreviousPolygonMode();
}

//-----------------------------------------------------------------------

void LightManager::_PointLightStencilPass(PointLight& light,
                                          const glm::mat4& viewMatrix,
                                          const glm::mat4& projectionMatrix)
{
    m_LightResultsFBO.UnbindColorAttachments();

    m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionDepthTesting );
    m_GraphicsDevice.OptionEnable( GraphicsDevice::e_OptionDepthTesting );

    m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionFaceCulling );
    m_GraphicsDevice.OptionDisable( GraphicsDevice::e_OptionFaceCulling );

    m_GraphicsDevice.ClearBuffers( GraphicsDevice::e_BufferTypeStencil );

    // We need the stencil test to be enabled but we want it
    // to succeed always. Only the depth test matters.
    m_GraphicsDevice.SaveCurrentStencilFunctionState();
    m_GraphicsDevice.SetStencilFunctionState( GraphicsDevice::e_StencilFunctionAlways, 0, 0 );

    m_GraphicsDevice.SaveCurrentBackFaceStencilOperationState();
    m_GraphicsDevice.SetBackFaceStencilOperationState( GraphicsDevice::e_StencilActionKeep,
                                                       GraphicsDevice::e_StencilActionIncrementWrap,
                                                       GraphicsDevice::e_StencilActionKeep );
    m_GraphicsDevice.SaveCurrentFrontFaceStencilOperationState();
    m_GraphicsDevice.SetFrontFaceStencilOperationState( GraphicsDevice::e_StencilActionKeep,
                                                        GraphicsDevice::e_StencilActionDecrementWrap,
                                                        GraphicsDevice::e_StencilActionKeep );


    light.RenderForStencilPass( viewMatrix, projectionMatrix );


    m_GraphicsDevice.RestorePreviousFrontFaceStencilOperationState();
    m_GraphicsDevice.RestorePreviousBackFaceStencilOperationState();
    m_GraphicsDevice.RestorePreviousStencilFunctionState();
    m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionFaceCulling );
    m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionDepthTesting );
}

//-----------------------------------------------------------------------

void LightManager::_SpotLightStencilPass(SpotLight& light,
                                         const glm::mat4& viewMatrix,
                                         const glm::mat4& projectionMatrix)
{
    m_LightResultsFBO.UnbindColorAttachments();

    m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionDepthTesting );
    m_GraphicsDevice.OptionEnable( GraphicsDevice::e_OptionDepthTesting );

    m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionFaceCulling );
    m_GraphicsDevice.OptionDisable( GraphicsDevice::e_OptionFaceCulling );

    m_GraphicsDevice.ClearBuffers( GraphicsDevice::e_BufferTypeStencil );

    // We need the stencil test to be enabled but we want it
    // to succeed always. Only the depth test matters.
    m_GraphicsDevice.SaveCurrentStencilFunctionState();
    m_GraphicsDevice.SetStencilFunctionState( GraphicsDevice::e_StencilFunctionAlways, 0, 0 );

    m_GraphicsDevice.SaveCurrentBackFaceStencilOperationState();
    m_GraphicsDevice.SetBackFaceStencilOperationState( GraphicsDevice::e_StencilActionKeep,
                                                       GraphicsDevice::e_StencilActionIncrementWrap,
                                                       GraphicsDevice::e_StencilActionKeep );
    m_GraphicsDevice.SaveCurrentFrontFaceStencilOperationState();
    m_GraphicsDevice.SetFrontFaceStencilOperationState( GraphicsDevice::e_StencilActionKeep,
                                                        GraphicsDevice::e_StencilActionDecrementWrap,
                                                        GraphicsDevice::e_StencilActionKeep );


    light.RenderForStencilPass( viewMatrix, projectionMatrix );


    m_GraphicsDevice.RestorePreviousFrontFaceStencilOperationState();
    m_GraphicsDevice.RestorePreviousBackFaceStencilOperationState();
    m_GraphicsDevice.RestorePreviousStencilFunctionState();
    m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionFaceCulling );
    m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionDepthTesting );
}

//-----------------------------------------------------------------------

void LightManager::_PointLightPass(PointLight& light,
                                   const glm::mat4& viewMatrix,
                                   const glm::mat4& projectionMatrix)
{
    m_LightResultsFBO.BindColorAttachmentsForDrawing( FBO::e_ColorAttachment0 );

    m_GraphicsDevice.SaveCurrentStencilFunctionState();
    m_GraphicsDevice.SetStencilFunctionState( GraphicsDevice::e_StencilFunctionNotEqual, 0, 0xFF );

    m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionDepthTesting  );
    m_GraphicsDevice.OptionDisable( GraphicsDevice::e_OptionDepthTesting );

    m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionBlending );
    m_GraphicsDevice.OptionEnable( GraphicsDevice::e_OptionBlending );


    m_GraphicsDevice.SaveCurrentBlendEquation();
    m_GraphicsDevice.SetBlendEquation( GraphicsDevice::e_BlendEquationAdd );

    m_GraphicsDevice.SaveCurrentBlendFunctionState();
    m_GraphicsDevice.SetBlendFunctionState( GraphicsDevice::e_BlendFunctionOne, GraphicsDevice::e_BlendFunctionOne );


    m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionFaceCulling );
    m_GraphicsDevice.OptionEnable( GraphicsDevice::e_OptionFaceCulling );

    m_GraphicsDevice.SaveCurrentFacesToCullValue();
    // The reason why the front and not the back faces are cooled is because the camera may be inside the light volume
    // and if we do back face culling as we normally do we will not see the light until we exit its volume.
    m_GraphicsDevice.SetFacesToCull( GraphicsDevice::e_FacesTypeFront );


    light.RenderForLightPass( viewMatrix, projectionMatrix );


    m_GraphicsDevice.RestorePreviousFacesToCullValue();
    m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionFaceCulling );
    m_GraphicsDevice.RestorePreviousBlendFunctionState();
    m_GraphicsDevice.RestorePreviousBlendEquation();
    m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionBlending );
    m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionDepthTesting );
    m_GraphicsDevice.RestorePreviousStencilFunctionState();
}

//-----------------------------------------------------------------------

void LightManager::_SpotLightPass(SpotLight& light,
                                  const glm::mat4& viewMatrix,
                                  const glm::mat4& projectionMatrix)
{
    m_LightResultsFBO.BindColorAttachmentsForDrawing( FBO::e_ColorAttachment0 );

    m_GraphicsDevice.SaveCurrentStencilFunctionState();
    m_GraphicsDevice.SetStencilFunctionState( GraphicsDevice::e_StencilFunctionNotEqual, 0, 0xFF );

    m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionDepthTesting  );
    m_GraphicsDevice.OptionDisable( GraphicsDevice::e_OptionDepthTesting );

    m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionBlending );
    m_GraphicsDevice.OptionEnable( GraphicsDevice::e_OptionBlending );

    m_GraphicsDevice.SaveCurrentBlendEquation();
    m_GraphicsDevice.SetBlendEquation( GraphicsDevice::e_BlendEquationAdd );

    m_GraphicsDevice.SaveCurrentBlendFunctionState();
    m_GraphicsDevice.SetBlendFunctionState( GraphicsDevice::e_BlendFunctionOne, GraphicsDevice::e_BlendFunctionOne );

    m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionFaceCulling );
    m_GraphicsDevice.OptionEnable( GraphicsDevice::e_OptionFaceCulling );

    m_GraphicsDevice.SaveCurrentFacesToCullValue();
    m_GraphicsDevice.SetFacesToCull( GraphicsDevice::e_FacesTypeFront );


    light.RenderForLightPass( viewMatrix, projectionMatrix );


    m_GraphicsDevice.RestorePreviousFacesToCullValue();
    m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionFaceCulling );
    m_GraphicsDevice.RestorePreviousBlendFunctionState();
    m_GraphicsDevice.RestorePreviousBlendEquation();
    m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionBlending );
    m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionDepthTesting );
    m_GraphicsDevice.RestorePreviousStencilFunctionState();
}

//-----------------------------------------------------------------------

void LightManager::_DirectionalLightPass(DirectionalLight& light,
                                         const glm::mat4& viewMatrix,
                                         const glm::mat4& projectionMatrix)
{
    m_LightResultsFBO.BindColorAttachmentsForDrawing( FBO::e_ColorAttachment0 );

    m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionDepthTesting  );
    m_GraphicsDevice.OptionDisable( GraphicsDevice::e_OptionDepthTesting );

    m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionBlending );
    m_GraphicsDevice.OptionEnable( GraphicsDevice::e_OptionBlending );


    m_GraphicsDevice.SaveCurrentBlendEquation();
    m_GraphicsDevice.SetBlendEquation( GraphicsDevice::e_BlendEquationAdd );

    m_GraphicsDevice.SaveCurrentBlendFunctionState();
    m_GraphicsDevice.SetBlendFunctionState( GraphicsDevice::e_BlendFunctionOne, GraphicsDevice::e_BlendFunctionOne );


    light.RenderForLightPass( viewMatrix, projectionMatrix );


    m_GraphicsDevice.RestorePreviousBlendFunctionState();
    m_GraphicsDevice.RestorePreviousBlendEquation();
    m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionBlending );
    m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionDepthTesting );
}

//-----------------------------------------------------------------------

void LightManager::_DrawableSurfaceSizeChaged(const SystemInterface::DrawableSurfaceSize& newSize, void* userData)
{
    LightManager* manager =  static_cast<LightManager*>( userData );

    manager->m_DrawableSurfaceSize = newSize;

    manager->m_PointLightShaderProg.Use();
    manager->m_PointLightShaderProg.SetUniformScreenSize( glm::vec2( manager->m_DrawableSurfaceSize.widthInPixels,
                                                                     manager->m_DrawableSurfaceSize.heightInPixels ) );

    manager->m_SpotLightShaderProg.Use();
    manager->m_SpotLightShaderProg.SetUniformScreenSize( glm::vec2( manager->m_DrawableSurfaceSize.widthInPixels,
                                                                    manager->m_DrawableSurfaceSize.heightInPixels ) );

    manager->m_DirectionLightShaderProg.Use();
    manager->m_DirectionLightShaderProg.SetUniformScreenSize( glm::vec2( manager->m_DrawableSurfaceSize.widthInPixels,
                                                                         manager->m_DrawableSurfaceSize.heightInPixels ) );
}

//-----------------------------------------------------------------------
