#ifndef CAPSULECOLLIDER_H_INCLUDED
#define CAPSULECOLLIDER_H_INCLUDED

#include "Collider.h"

class CapsuleCollider : public Collider
{
    GAME_OBJECT_COMPONENT_TYPE(e_TypeCollider)

public:
    enum Direction
    {
        e_DirectionX = 0,
        e_DirectionY = 1,
        e_DirectionZ,

        e_DirectionCount // Not a direction (used for counting)
    };

    CapsuleCollider(GameObject& owningGameObject,
                    float radius,
                    float height,
                    PhysicsManager& physicsManager);

    void SetRadius(float radius);
    float GetRadius() const;

    void SetHeight(float height);
    float GetHeight() const;

    void SetDirection(Direction dir);
    Direction GetDirection() const;

private:
    PhysicsManager::CollisionShapeHandle _CreateCapsuleCollisionShape(Direction dir,
                                                                      float radius,
                                                                      float height);

    Direction m_Direction;
};

#endif // CAPSULECOLLIDER_H_INCLUDED
