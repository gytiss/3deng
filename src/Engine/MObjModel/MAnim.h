#ifndef MANIM_H_INCLUDED
#define MANIM_H_INCLUDED

#include <string>
#include <cstdint>
#include <vector>
#include <ByteStream.h>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <GraphicsDevice/VAO.h>
#include <EngineTypes.h>

class VBO;
class GraphicsDevice;
class ShaderProgDebugObjects;
class SystemInterface;
class FileManager;

class MAnim
{
public:
    friend class AnimationManager;

    struct Joint
    {
        int       m_ParentID;
        glm::vec3 m_Pos;
        glm::quat m_Orient;

        glm::mat4 m_Transform;

        Joint& operator= (const Joint& other)
        {
            m_ParentID = other.m_ParentID;
            m_Pos = other.m_Pos;
            m_Orient = other.m_Orient;
            m_Transform = other.m_Transform;

            return *this;
        }
    };

    typedef std::vector<Joint> JointList;

    struct AnimFrame
    {
        AnimFrame( size_t jointCount )
        {
            m_Joints.reserve( jointCount );
            m_Joints.assign( jointCount, Joint() );
            m_BoneMatrices.reserve( jointCount );
            m_BoneMatrices.assign( jointCount, glm::mat4(1.0f) );
        }

        EngineTypes::MatrixArray m_BoneMatrices;
        JointList  m_Joints;
    };

    struct Bone
    {
        glm::vec3 m_Pos;
        glm::quat m_Orient;
        int m_BoneIndex;
    };

    typedef std::vector<EngineTypes::AABB> BoundingBoxList;
    typedef std::vector<AnimFrame> AnimFrameList;

    MAnim(GraphicsDevice& graphicsDevice,
          SystemInterface& systemInterface,
          FileManager& fileManager);
    ~MAnim();
    bool LoadFromFile(const std::string& fileName);

    const EngineTypes::MatrixArray& GetSkeletonMatrixArray() const
    {
        return m_CurrentFrame.m_BoneMatrices;
    }

    void DebugDumpData();

    const uint8_t* GetBoneNamesMD5Hash() const
    {
        return m_BoneNamesMD5Hash;
    }

    void RenderDebugData(ShaderProgDebugObjects& shaderProg);

protected:

    /**
     * Returns false if animation has finished, true otherwise
     */
    bool Update();

    void StartAnimtion(bool loop = false);

private:
    bool _ReadHeader(ByteStream& byteStream, uint32_t& jointNamesSectionSize);
    bool _VerifyFileTypeAndVersion(ByteStream& byteStream);
    bool _VerifyFileSize(ByteStream& byteStream, const uint32_t jointNamesSectionSize);
    bool _SkipJointNamesSection(ByteStream& byteStream, const uint32_t jointNamesSectionSize);
    bool _ReadJoints(ByteStream& byteStream);
    bool _ReadBoundingBoxes(ByteStream& byteStream);
    bool _ReadFrames(ByteStream& byteStream);

    void _InterpolateSkeletons( AnimFrame& finalFrame, const AnimFrame& frame0, const AnimFrame& frame1, float interpolate );
    void _CopyFrame(AnimFrame& to, const AnimFrame& from);

    size_t _CreateBonesVertexBuffers();
    size_t _CreateBoundsVertexBuffers();

    static const size_t c_MD5HashLength = 16;
    uint8_t m_BoneNamesMD5Hash[c_MD5HashLength];
    uint16_t m_JointCount;
    uint16_t m_AnimFrameCount;
    uint16_t m_AnimFrameRateInFramesPerSecond;
    uint16_t m_BoundingBoxCount;
    JointList m_Joints;
    AnimFrameList m_AnimFrames;
    AnimFrame m_CurrentFrame;
    BoundingBoxList m_BoundingBoxes;

    float m_AnimDurationInSeconds;
    float m_FrameDurationInSeconds;
    float m_CurrentAnimTimeIsSeconds;

    unsigned int m_CurrBoundingBoxNum;
    double m_GlobalTimeInSecondDuringLastUpdate;
    bool m_Loop;
    bool m_FinishedPlaying;

    VAO m_JointsDebugVAO;
    VBO* m_JointsDebugVBO;
    VAO m_BoundsVAO;
    GraphicsDevice& m_GraphicsDevice;
    SystemInterface& m_SystemInterface;
    FileManager& m_FileManager;
};

#endif // MANIM_H_INCLUDED
