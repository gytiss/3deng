#ifndef BULLETCONTACTRESULT_H_INCLUDED
#define BULLETCONTACTRESULT_H_INCLUDED

#include <vector>
#include "BulletCommon.h"
#include "BulletManifoldPoint.h"

#include <bullet/btBulletDynamicsCommon.h>


////////////////////////////////////////////////////////////////////
//       Class : BulletContact
// Description :
////////////////////////////////////////////////////////////////////
struct BulletContact
{
public:
    const BulletManifoldPoint* get_manifold_point() const;
    const btCollisionObject* get_node0() const;
    const btCollisionObject* get_node1() const;
    int get_idx0() const;
    int get_idx1() const;
    int get_part_id0() const;
    int get_part_id1() const;

private:
    btManifoldPoint* _mp;
    const btCollisionObject* _obj0;
    const btCollisionObject* _obj1;
    int _part_id0;
    int _part_id1;
    int _idx0;
    int _idx1;

    friend struct BulletContactResult;
};

////////////////////////////////////////////////////////////////////
//       Class : BulletContactResult
// Description :
////////////////////////////////////////////////////////////////////
struct BulletContactResult : public btCollisionWorld::ContactResultCallback
{
public:
    BulletContactResult();
    virtual ~BulletContactResult();
    size_t get_num_contacts() const;
    const BulletContact& get_contact(size_t idx) const;

    virtual btScalar addSingleResult(btManifoldPoint &mp,
                                      const btCollisionObjectWrapper* colObj0Wrap,
                                      int part_id0,
                                      int idx0,
                                      const btCollisionObjectWrapper* colObj1Wrap,
                                      int part_id1,
                                      int idx1);

    void get_contacts(std::vector<BulletContact>& outHits)
    {
        for(size_t i = 0; i < get_num_contacts(); i++)
        {
            outHits.push_back( get_contact( i ) );
        }
    }

private:
    static BulletContact _empty;

    std::vector<BulletContact> _contacts;
};

#endif // BULLETCONTACTRESULT_H_INCLUDED
