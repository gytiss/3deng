#include "GraphicsDeviceStencilBuffer.h"
#include <OpenGLErrorChecking.h>

//-----------------------------------------------------------------------

GraphicsDeviceStencilBuffer::GraphicsDeviceStencilBuffer()
: m_StencilFunctionStatesStack(),
  m_CurrentStencilFunctionState(),
  m_FrontFaceStencilOperationStatesStack(),
  m_CurrentFrontFaceStencilOperation(),
  m_BackFaceStencilOperationStatesStack(),
  m_CurrentBackFaceStencilOperation()
{
    /*---------------------------------*/
    m_CurrentStencilFunctionState.func = e_StencilFunctionAlways;
    m_CurrentStencilFunctionState.referenceValue = 0;
    m_CurrentStencilFunctionState.mask = ~static_cast<GLuint>( 0 );

    glStencilFunc( m_CurrentStencilFunctionState.func,
                   m_CurrentStencilFunctionState.referenceValue,
                   m_CurrentStencilFunctionState.mask );
    CheckForGLErrors();
    /*---------------------------------*/

    /*---------------------------------*/
    m_CurrentFrontFaceStencilOperation.stencilTestFails = e_StencilActionKeep;
    m_CurrentFrontFaceStencilOperation.stencilTestPasesAndDepthTestFails = e_StencilActionKeep;
    m_CurrentFrontFaceStencilOperation.stencilAndDepthTestPases = e_StencilActionKeep;

    glStencilOpSeparate( GL_FRONT,
                         m_CurrentFrontFaceStencilOperation.stencilTestFails,
                         m_CurrentFrontFaceStencilOperation.stencilTestPasesAndDepthTestFails,
                         m_CurrentFrontFaceStencilOperation.stencilAndDepthTestPases );
    CheckForGLErrors();
    /*---------------------------------*/

    /*---------------------------------*/
    m_CurrentBackFaceStencilOperation.stencilTestFails = e_StencilActionKeep;
    m_CurrentBackFaceStencilOperation.stencilTestPasesAndDepthTestFails = e_StencilActionKeep;
    m_CurrentBackFaceStencilOperation.stencilAndDepthTestPases = e_StencilActionKeep;

    glStencilOpSeparate( GL_BACK,
                         m_CurrentBackFaceStencilOperation.stencilTestFails,
                         m_CurrentBackFaceStencilOperation.stencilTestPasesAndDepthTestFails,
                         m_CurrentBackFaceStencilOperation.stencilAndDepthTestPases );
    CheckForGLErrors();
    /*---------------------------------*/

    /*---------------------------------*/
    m_CurrentClearStencilValue = 0;

    glGetIntegerv( GL_STENCIL_CLEAR_VALUE, &m_CurrentClearStencilValue );
    CheckForGLErrors();
    /*---------------------------------*/
}

//-----------------------------------------------------------------------

void GraphicsDeviceStencilBuffer::SaveCurrentStencilFunctionState()
{
    m_StencilFunctionStatesStack.push( m_CurrentStencilFunctionState );
}

//-----------------------------------------------------------------------

void GraphicsDeviceStencilBuffer::RestorePreviousStencilFunctionState()
{
    const StencilFunctionState& previousState = m_StencilFunctionStatesStack.top();
    SetStencilFunctionState( previousState.func,
                             previousState.referenceValue,
                             previousState.mask );
    m_StencilFunctionStatesStack.pop();
}

//-----------------------------------------------------------------------

void GraphicsDeviceStencilBuffer::SetStencilFunctionState(StencilFunction func,
                                                          int referenceValue,
                                                          unsigned int mask)
{
    if( ( func != m_CurrentStencilFunctionState.func ) ||
        ( referenceValue != m_CurrentStencilFunctionState.referenceValue ) ||
        ( mask != m_CurrentStencilFunctionState.mask ) )
    {
        m_CurrentStencilFunctionState.func = func;
        m_CurrentStencilFunctionState.referenceValue = referenceValue;
        m_CurrentStencilFunctionState.mask = mask;

        glStencilFunc( m_CurrentStencilFunctionState.func,
                       m_CurrentStencilFunctionState.referenceValue,
                       m_CurrentStencilFunctionState.mask );
        CheckForGLErrors();
    }
}

//-----------------------------------------------------------------------

void GraphicsDeviceStencilBuffer::SaveCurrentFrontFaceStencilOperationState()
{
    m_FrontFaceStencilOperationStatesStack.push( m_CurrentFrontFaceStencilOperation );
}

//-----------------------------------------------------------------------

void GraphicsDeviceStencilBuffer::RestorePreviousFrontFaceStencilOperationState()
{
    const StencilOperationState& previousState = m_FrontFaceStencilOperationStatesStack.top();
    SetFrontFaceStencilOperationState( previousState.stencilTestFails,
                                       previousState.stencilTestPasesAndDepthTestFails,
                                       previousState.stencilAndDepthTestPases );
    m_FrontFaceStencilOperationStatesStack.pop();
}

//-----------------------------------------------------------------------

void GraphicsDeviceStencilBuffer::SetFrontFaceStencilOperationState(StencilAction stencilTestFails,
                                                                    StencilAction stencilTestPasesAndDepthTestFails,
                                                                    StencilAction stencilAndDepthTestPases)
{
    if( ( stencilTestFails != m_CurrentFrontFaceStencilOperation.stencilTestFails ) ||
        ( stencilTestPasesAndDepthTestFails != m_CurrentFrontFaceStencilOperation.stencilTestPasesAndDepthTestFails ) ||
        ( stencilAndDepthTestPases != m_CurrentFrontFaceStencilOperation.stencilAndDepthTestPases ) )
    {
        m_CurrentFrontFaceStencilOperation.stencilTestFails = stencilTestFails;
        m_CurrentFrontFaceStencilOperation.stencilTestPasesAndDepthTestFails = stencilTestPasesAndDepthTestFails;
        m_CurrentFrontFaceStencilOperation.stencilAndDepthTestPases = stencilAndDepthTestPases;

        glStencilOpSeparate( GL_FRONT,
                             m_CurrentFrontFaceStencilOperation.stencilTestFails,
                             m_CurrentFrontFaceStencilOperation.stencilTestPasesAndDepthTestFails,
                             m_CurrentFrontFaceStencilOperation.stencilAndDepthTestPases );
        CheckForGLErrors();
    }
}

//-----------------------------------------------------------------------

void GraphicsDeviceStencilBuffer::SaveCurrentBackFaceStencilOperationState()
{
    m_BackFaceStencilOperationStatesStack.push( m_CurrentBackFaceStencilOperation );
}

//-----------------------------------------------------------------------

void GraphicsDeviceStencilBuffer::RestorePreviousBackFaceStencilOperationState()
{
    const StencilOperationState& previousState = m_BackFaceStencilOperationStatesStack.top();
    SetBackFaceStencilOperationState( previousState.stencilTestFails,
                                      previousState.stencilTestPasesAndDepthTestFails,
                                      previousState.stencilAndDepthTestPases );
    m_BackFaceStencilOperationStatesStack.pop();
}

//-----------------------------------------------------------------------

void GraphicsDeviceStencilBuffer::SetBackFaceStencilOperationState(StencilAction stencilTestFails,
                                                                   StencilAction stencilTestPasesAndDepthTestFails,
                                                                   StencilAction stencilAndDepthTestPases)
{
    if( ( stencilTestFails != m_CurrentBackFaceStencilOperation.stencilTestFails ) ||
        ( stencilTestPasesAndDepthTestFails != m_CurrentBackFaceStencilOperation.stencilTestPasesAndDepthTestFails ) ||
        ( stencilAndDepthTestPases != m_CurrentBackFaceStencilOperation.stencilAndDepthTestPases ) )
    {
        m_CurrentBackFaceStencilOperation.stencilTestFails = stencilTestFails;
        m_CurrentBackFaceStencilOperation.stencilTestPasesAndDepthTestFails = stencilTestPasesAndDepthTestFails;
        m_CurrentBackFaceStencilOperation.stencilAndDepthTestPases = stencilAndDepthTestPases;

        glStencilOpSeparate( GL_BACK,
                             m_CurrentBackFaceStencilOperation.stencilTestFails,
                             m_CurrentBackFaceStencilOperation.stencilTestPasesAndDepthTestFails,
                             m_CurrentBackFaceStencilOperation.stencilAndDepthTestPases );
        CheckForGLErrors();
    }
}

//-----------------------------------------------------------------------

void GraphicsDeviceStencilBuffer::SaveCurrentClearStencilValue()
{
    m_ClearStencilValuesStack.push( m_CurrentClearStencilValue );
}

//-----------------------------------------------------------------------

void GraphicsDeviceStencilBuffer::RestorePreviousClearStencilValue()
{
    SetClearStencilValue( m_ClearStencilValuesStack.top() );
    m_ClearStencilValuesStack.pop();
}

//-----------------------------------------------------------------------

void GraphicsDeviceStencilBuffer::SetClearStencilValue(int stencilClearValue)
{
    if( stencilClearValue != m_CurrentClearStencilValue )
    {
        m_CurrentClearStencilValue = stencilClearValue;

        glClearStencil( m_CurrentClearStencilValue );
        CheckForGLErrors();
    }
}

//-----------------------------------------------------------------------
