#include "GUIEvent.h"
#include "GUIEventManager.h"
#include "CommonDefinitions.h"

//-----------------------------------------------------------------------

GUIEvent::GUIEvent(const Rocket::Core::String& value,
                   GUIEventManager* eventManager)
: m_Value( value ),
  m_EventManager( eventManager )
{
}

//-----------------------------------------------------------------------

GUIEvent::~GUIEvent()
{
    m_EventManager = nullptr;
}

//-----------------------------------------------------------------------

void GUIEvent::ProcessEvent(Rocket::Core::Event& event)
{
    if( m_EventManager )
    {
        m_EventManager->ProcessEvent( event, m_Value );
    }
}

//-----------------------------------------------------------------------

void GUIEvent::OnDetach(Rocket::Core::Element* ROCKET_UNUSED_PARAMETER(element))
{
	delete this;
}

//-----------------------------------------------------------------------
