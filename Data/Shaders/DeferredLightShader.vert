
in HIGHEST_PRECISION vec3 in_VertexPos;

// Output data ; will be interpolated for each fragment.
#if defined(DIRECTIONAL_LIGHT)
out LOWEST_PRECISION vec3 ViewVector;
#else
out HIGHEST_PRECISION vec3 LightBoundingVolumeVertexPos_ViewSpace;
#endif

#if defined(DIRECTIONAL_LIGHT)
uniform HIGHEST_PRECISION mat4 u_InverseProjectionMatrix;
#else
uniform HIGHEST_PRECISION mat4 u_ModelViewMatrix;
uniform HIGHEST_PRECISION mat4 u_ModelViewProjectionMatrix;
#endif

void main()
{
    HIGHEST_PRECISION vec4 vertexPos = vec4( in_VertexPos, 1.0f );

#if defined(DIRECTIONAL_LIGHT)
    HIGHEST_PRECISION vec3 LightBoundingVolumeVertexPos_ViewSpace = (u_InverseProjectionMatrix * vertexPos).xyz;
    // For a directional light we can clamp in the vertex shader, since we only interpolate in the XY direction
    ViewVector = vec3(LightBoundingVolumeVertexPos_ViewSpace.xy / LightBoundingVolumeVertexPos_ViewSpace.z, 1.0f);
    gl_Position = vertexPos;
#else
    // Calculate the view space vertex position
    LightBoundingVolumeVertexPos_ViewSpace = (u_ModelViewMatrix * vertexPos).xyz;
    gl_Position = u_ModelViewProjectionMatrix * vertexPos;
#endif
}
