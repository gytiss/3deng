#ifndef PHYSICSCHARACTERCONTROLLER_H_INCLUDED
#define PHYSICSCHARACTERCONTROLLER_H_INCLUDED

#include <vector>
#include <glm/glm.hpp>
#include <bullet/btBulletDynamicsCommon.h>
#include <bullet/BulletCollision/CollisionDispatch/btGhostObject.h>
#include <CommonDefinitions.h>
#include <Physics/PhysicsManager.h>
#include <Physics/BulletAllHitsRayResult.h>
#include <Physics/BulletClosestHitRayResult.h>
#include <Physics/BulletContactResult.h>
#include <MathUtils.h>
#include <Scene/CompositeTransformNode.h>

#include <cassert>




/**
 * This class implements Kinematic Character Controller
 *
 *   The custom kinematic character controller replacing the Bullet's default
 *   character controller and providing more stability and features.
 *
 *   Features included:
 *       - Walking with penetration prevention
 *       - Jumping with active and passive jump limiter. Active means limiting the max jump height based
 *         on the distance to the "ceiling". Passive means falling automatically when a "ceiling" is hit.
 *       - Crouching with stand up limiter which prevents the character from standing up if inside a tunnel
 *         or other limited space
 *       - Slope limiter of arbitrary maximum slope values which may or may not affect the movement speed
 *         on slopes smaller than maximum
 *       - Stepping, supports walking steps up and down (prevents "floating" effect)
 *       - Flying support for no-clip, ladders, swimming or simply flying
 *       - Simplified state system. Makes double/multiple jumps impossible by default
 *       - Callbacks for landing and standing up from crouch
 *
 *   The controller is composed of a levitating capsule (allowing stepping), a kinematic body and numerous
 *   raycasts accounting for levitation and spacial awareness.
 *   The elements are set up automatically.
 */
class PhysicsCharacterController : public btActionInterface
{
public:
    class Capsule
    {
    public:
        Capsule(PhysicsManager& physicsManager,
                CompositeTransformNode* parentTransform,
                float height,
                float radius,
                float distanceFromTopToGround,
                float distFromBottomToGround);

        ~Capsule();

        inline void UpdateCapsule(const glm::vec3& currentPos)
        {
            m_Transform->SetPosition( glm::vec3( 0.0f, m_Offset, 0.0f ) );
            m_Top = currentPos.y + m_DistanceFromBottomToGround + m_Height * 2.0f;

            // Update position of the accompanying ghost object
            btTransform newGhostObjectPos;
            m_Transform->getWorldTransform( newGhostObjectPos );
            m_GhostObject->setWorldTransform( newGhostObjectPos );
        }

        inline void AddToThePhysicsWorld()
        {
            if (!m_InPhysicsWorld)
            {
                m_PhysicsManager.m_DynamicsWorld->addCollisionObject( m_GhostObject,
                                                                      btBroadphaseProxy::CharacterFilter,
                                                                      btBroadphaseProxy::StaticFilter | btBroadphaseProxy::DefaultFilter );

                m_InPhysicsWorld = true;
            }
        }

        inline void RemoveFromThePhysicsWorld()
        {
            if (m_InPhysicsWorld)
            {
                m_PhysicsManager.m_DynamicsWorld->removeCollisionObject( m_GhostObject );

                m_InPhysicsWorld = false;
            }
        }

        inline btPairCachingGhostObject* GetGhostObject()
        {
            return m_GhostObject;
        }

        inline btCollisionShape* GetCollisionShape()
        {
            return m_CollisionShape;
        }

        inline float GetTop() const
        {
            return m_Top;
        }

        inline float GetHeight() const
        {
            return m_Height;
        }

        inline float GetRadius() const
        {
            return m_Radius;
        }

        inline float GetFootDistance() const
        {
            return m_FootDistance;
        }

        inline float GetLevitation() const
        {
            return m_DistanceFromBottomToGround;
        }

        inline float GetDistanceFromTopToGround() const
        {
            return m_DistanceFromTopToGround;
        }

    private:
        // Used to prevent copying
        Capsule& operator = (const Capsule& other);
        Capsule(const Capsule& other);

        const float m_Height;
        const float m_Radius;
        const float m_DistanceFromTopToGround;
        const float m_DistanceFromBottomToGround;

        float m_Offset; ///< Offset from the parent transform
        float m_FootDistance;
        float m_Top;    ///< Top of the capsule

        btCapsuleShape* m_CollisionShape;
        btPairCachingGhostObject* m_GhostObject;

        CompositeTransformNode* m_Transform;
        PhysicsManager& m_PhysicsManager;
        bool m_InPhysicsWorld;
    };

    enum MovementState
    {
        e_MovementStateGround = 0,
        e_MovementStateJumping = 1,
        e_MovementStateFalling,
        e_MovementStateFlying,

        e_MovementStateCount // // Not a movement state(used for counting)
    };

    typedef void (*Callback)(void* data);

   /**
    * @param physicsManager - physics manager which creates rigid bodies and contains bullet world
    * @param walkHeight     - height of the whole controller when walking
    * @param crouchHeight   - height of the whole controller when crouching
    * @param stepHeight     - maximum step height the caracter can walk.
    * @param radius         - capsule radius
    */
    PhysicsCharacterController(PhysicsManager& physicsManager,
                               float walkHeight,
                               float crouchHeight,
                               float stepHeight,
                               float radius);

    ~PhysicsCharacterController();

    void SetCollideMask(uint32_t args);

   /**
    * Callback called when the character falls on the ground.
    *
    * The position where falling started is passed as the first argument,
    * the additional argument and keyword arguments follow.
    */
    void SetFallCallback(Callback callback, void* data);

   /**
    * Callback called when the character stands up from crouch. This is
    * needed because standing up might be prevented by spacial awareness.
    */
    void SetStandUpCallback(Callback callback, void* data);

    void SetPos(const glm::vec3& pos);
    const glm::vec3& GetPos() const;
    void SetQuat(glm::quat& rot);
    const glm::quat& GetQuat() const;

    void SetX(float val);
    void SetY(float val);
    void SetZ(float val);

    float GetTop() const;

   /**
    * degs -- (float) maximum slope in degrees. 0.0 means don't limit slope.
    * affectSpeed -- (bool) if True, the character will walk slower up slopes
    *
    * Both options are independent.
    *
    * By default, affectSpeed is enabled and maximum slope is 50 deg.
    */
    void SetMaxSlope(float degs, bool affectSpeed);

   /**
    * Enable or disable the active jump limiter, which is the mechanism that changes the
    * maximum jump height based on the space available above the character's head.
    */
    void SetActiveJumpLimiter(bool val);

    bool IsCrouching()
    {
        return m_IsCrouching;
    }

    void StartCrouch();

   /**
    * Note that spacial awareness may prevent the character from standing up immediately, which
    * is what you usually want. Use stand up callback to know when the character stands up.
    */
    void StopCrouch();

   /**
    * Check if the character is on ground. You may also check if the movementState variable is set to 'ground'
    */
    bool IsOnGround();

   /**
    * Max height is 3.0 by default. Probably too much for most uses.
    */
    void StartJump(float maxHeight = 3.0f);

    void StartFly();

   /**
    * Stop flying and start falling
    */
    void StopFly();

    void SetAngularMovement(float omegaInDegrees);
    void SetLinearMovement(const glm::vec3& speed);

    /**
     * Called by the physics world at least once per physics tick
     */
    void updateAction (btCollisionWorld* UNUSED_PARAM(collisionWorld), btScalar deltaTimeStep)
    {
        _Update( deltaTimeStep );
    }

    void debugDraw (btIDebugDraw* UNUSED_PARAM(debugDrawer))
    {
        /* Needed by Bullets action interface, but not used */
    }

private:
    // Used to prevent copying
    PhysicsCharacterController& operator = (const PhysicsCharacterController& other);
    PhysicsCharacterController(const PhysicsCharacterController& other);

    struct HeadContact
    {
        bool isValid;
        btVector3 hitPos;
        const btCollisionObject* hitNode;
    };

    struct FootContact
    {
        bool isValid;
        btVector3 hitPos;
        const btCollisionObject* hitNode;
        btVector3 hitNormal;
    };

   /**
    * Update the state of the character controller
    */
    void _Update(float deltaTime);

    bool _CanTransitionFromStateToState(MovementState fromState, MovementState toState);
    void _Land();
    void _Fall();
    void _Jump(float maxY = 3.0f);
    void _StandUp();
    void _ProcessGround();
    void _ProcessFalling();
    void _ProcessJumping();
    void _ProcessFlying();

    BulletClosestHitRayResult ray_test_closest(const btVector3 &from_pos,
                                               const btVector3 &to_pos,
                                               const CollideMask &mask = CollideMask::AllOn()) const;

    /**
     * Returns the total mass of a rigid body. A value
     * of zero means that the body is static, i.e. has
     * an infinite mass.
     *
     * @param rigidBody
     */
    float get_mass(const btRigidBody* rigidBody) const;

    bool _CheckFutureSpace(btVector3 globalVel);
    BulletAllHitsRayResult ray_test_all(const btVector3 &from_pos,
                                        const btVector3 &to_pos,
                                        const CollideMask &mask = CollideMask::AllOn()) const;

    struct HitsComparator
    {
        btVector3 pFrom;

        bool operator() (const BulletRayHit& a, const BulletRayHit& b)
        {
            float lengthA = (pFrom - a.get_hit_pos()).length();
            float lengthB = (pFrom - b.get_hit_pos()).length();

            return ( lengthA < lengthB );
        }
    };

    void _UpdateFootContact();
    void _UpdateHeadContact(float testHeightLimit);
    void _UpdateCapsule();
    void _ApplyGravity(glm::vec3 floorNormal);
    float AngleBetweenTwoNormalVectorsInDegress(const glm::vec3& vec1, glm::vec3& vec2);
    void _ApplyLinearVelocity();

    BulletContactResult contact_test(btCollisionObject* bulletNode);
    void _PreventPenetration();
    void _SetData(float fullHeight, float stepHeight, float radius, float& outHeight, float& outLevitation, float& outRadius);
    void _Setup();

    PhysicsManager& m_PhysicsManager;
    const float m_WalkHeight;
    const float m_CrouchHeight;
    const float m_StepHeight;
    const float m_Radius;
    const float m_Gravity;

    float m_TimeStep;
    glm::vec3 m_CurrentPos;

    Capsule* m_CurrentCapsule;
    Capsule* m_WalkingCapsule;
    Capsule* m_CrouchingCapsule;

    CompositeTransformNode* m_MovementParentCompositeTransform;

    float m_MinSlopeDot;
    bool m_SlopeAffectsSpeed;
    bool m_IntelligentJump;

    MovementState m_MovementState;
    std::vector<MovementState> m_MovementStateFilter[e_MovementStateCount];

    // Prevent the KCC from moving when there's not enough room for it in the next frame
    // It doesn't work right now because I can't figure out how to add sliding. Sweep test could work,
    // but it would cause problems with penetration testing and steps
    // That said, you probably won't need it, if you design your levels correctly
    bool m_PredictFutureSpace;
    float m_FutureSpacePredictionDistance;

    bool m_IsCrouching;

    float m_FallTime;
    float m_FallStartPos;
    float m_FallDelta;
    glm::vec3 m_LinearVelocity;
    HeadContact m_HeadContact;
    FootContact m_FootContact;
    bool m_EnabledCrouch;

    Callback m_StandUpCallback;
    void* m_StandUpCallbackData;

    Callback m_FallCallback;
    void* m_FallCallbackData;

    float m_JumpStartPos;
    float m_JumpTime;
    float m_JumpSpeed;
    float m_JumpMaxHeight;
};

#endif // PHYSICSCHARACTERCONTROLLER_H_INCLUDED
