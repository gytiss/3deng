#ifndef TOOLSPANEL_H_INCLUDE
#define TOOLSPANEL_H_INCLUDE

#include <QtWidgets/QWidget>

class MainWindow;
class ManipulatorTool;

class ToolsPanel : public QWidget
{
    Q_OBJECT

public:
    ToolsPanel(MainWindow& mainWindow, ManipulatorTool& manipulatorTool);

private slots:
    void SelectToolSelected(bool checked);
    void MoveToolSelected(bool checked);
    void RotateToolSelected(bool checked);
    void ScaleToolSelected(bool checked);

private:
    MainWindow& m_MainWindow;
    ManipulatorTool& m_ManipulatorTool;
};



#endif // TOOLSPANEL_H_INCLUDE
