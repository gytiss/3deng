/**
*  Copyright (c) Gytis Sabaliauskas - All Rights Reserved
*
*  Copying of this file via any medium and redistribution and use
*  in source and binary forms, with or without modification, are not
*  permitted without hand written consent of the authors.
*
*  Proprietary and confidential.
*/

#ifndef LIST_H_INCLUDED
#define LIST_H_INCLUDED

#include <cstddef>      // ptrdiff_t
#include <iterator>     // iterator
#include <type_traits>  // remove_cv
#include <utility>      // swap

template<class ElementType>
class List
{
public:
    struct Node;

private:
    struct NodesPrivateData
    {
        Node* nextNode;
        Node* previousNode;
    };

public:
    struct Node
    {
        NodesPrivateData privateData;
        ElementType value;
    };

    template<class Type, class UnqualifiedType = std::remove_cv<Type>>
    class ForwardIterator
        : public std::iterator<std::forward_iterator_tag,
                               UnqualifiedType,
                               std::ptrdiff_t,
                               Type*,
                               Type&>
    {
    public:
        friend class List;

        ForwardIterator()   // Default construct gives end.
            : itr(nullptr)
        {
        }

        inline void swap(ForwardIterator& other) noexcept
        {
            using std::swap;
            swap(itr, other.iter);
        }

        inline ForwardIterator& operator++ () // Pre-increment
        {
            if (itr)
            {
                itr = itr->privateData.nextNode;
            }

            return *this;
        }

        inline ForwardIterator operator++ (int) // Post-increment
        {
            ForwardIterator tmp(*this);

            if (itr)
            {
                itr = itr->privateData.nextNode;
            }

            return tmp;
        }

        // two-way comparison: v.begin() == v.cbegin() and vice versa
        template<class OtherType>
        inline bool operator == (const ForwardIterator<OtherType>& rhs) const
        {
            return itr == rhs.itr;
        }

        template<class OtherType>
        inline bool operator != (const ForwardIterator<OtherType>& rhs) const
        {
            return itr != rhs.itr;
        }

        inline Type& operator* () const
        {
            return itr->value;
        }

        inline Type& operator-> () const
        {
            return itr->value;
        }

        // One way conversion: iterator -> const_iterator
        inline operator ForwardIterator<const Type>() const
        {
            return ForwardIterator<const Type>(itr);
        }
    private:
        Node* itr;

        explicit ForwardIterator(Node* nd)
        : itr(nd)
        {
        }
    };

    typedef ForwardIterator<ElementType> iterator;
    typedef ForwardIterator<const ElementType> const_iterator;

    List();
    ~List();

    inline iterator begin() noexcept;
    inline const_iterator begin() const noexcept;
    inline iterator end() noexcept;
    inline const_iterator end() const noexcept;
    inline const_iterator cbegin() const noexcept;
    inline const_iterator cend() const noexcept;

    /**
     * @param value - Value to insert
     * @return Inserted node
     */
    Node* PushBack(const ElementType& value);

    void Erase(Node* node);

    /**
     * Deletes all of the contents of the list
     */
    void Clear();

private:
    // Used to prevent copying
    List& operator = (const List& other) = delete;
    List(const List& other) = delete;

    Node* m_RootNode;
    Node* m_LastNode;
};

#include "List.inl"

#endif // LIST_H_INCLUDED
