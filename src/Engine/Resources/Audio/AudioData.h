#ifndef AUDIODATA_H_INCLUDED
#define AUDIODATA_H_INCLUDED

#include <memory>
#include <vector>

class FileManager;

class AudioData
{
public:
    typedef std::vector<uint8_t> RawSamples;

    AudioData(FileManager& fileManager);
    AudioData(AudioData&& that);
    AudioData(const AudioData& that) = delete;
    ~AudioData();

    inline int_least32_t GetSampleRate() const
    {
        return m_SampleRate;
    }

    inline uint_least32_t GetSampleWidthInBytes() const
    {
        return m_SampleWidthInBytes;
    }

    inline int_least32_t GetNumberOfChannels() const
    {
        return m_NumberOfChannels;
    }

    inline const RawSamples& GetRawSampleData() const
    {
        return m_RawSamples;
    }

    inline bool IsValid() const
    {
        return m_Valid;
    }

    bool InitFromOggFile(const std::string& oggFilePath);

private:
    struct RawOggDataBuffer
    {
        std::unique_ptr<uint8_t[]> data;
        size_t totalDataSize;
        size_t dataReadSoFar;
    };

    bool _ReadRawOggData(const std::string& oggFilePath,
                         RawOggDataBuffer& oggRawData);

    bool _ReadRawAudioSampesFromRawOggData(RawOggDataBuffer& oggRawData,
                                           RawSamples& outRawSamples);

    static size_t OggDataReadCallback(void *ptr, size_t size, size_t nmemb, void *datasource);

    int_least32_t m_SampleRate;
    uint_least32_t m_SampleWidthInBytes;
    int_least32_t m_NumberOfChannels;
    RawSamples m_RawSamples;
    bool m_Valid;
    FileManager& m_FileManager;
};



#endif // AUDIODATA_H_INCLUDED
