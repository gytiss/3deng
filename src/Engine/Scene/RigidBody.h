#ifndef RIGIDBODY_H_INCLUDED
#define RIGIDBODY_H_INCLUDED

#include <Physics/PhysicsManager.h>
#include "Component.h"

class GameObject;

class RigidBody : public Component, public PhysicsMotionStateInterface
{
    GAME_OBJECT_COMPONENT_TYPE(e_TypeRigidBody);

public:
    friend class Collider;

    RigidBody(GameObject& owningGameObject,
              bool isKinematic,
              float mass,
              PhysicsManager& physicsManager);

    virtual ~RigidBody();

    void SetEnabled(bool val);

    /**
     * Check if this physics property represents a physics body
     * or a set of bodies which is/are kinematic
     */
    bool IsKinematic();

    /**
     * Make rigid boy associated with this physics property kinematic or not
     */
    void SetIsKinematic(bool val);

    void SetMass(float mass);
    float GetMass() const;

    void SetDrag(float drag);
    float GetDrag() const;
    void SetAngularDrag(float drag);
    float GetAngularDrag() const;

    void SetPositionFrozenX(bool frozen);
    bool GetPositionFrozenX() const;
    void SetPositionFrozenY(bool frozen);
    bool GetPositionFrozenY() const;
    void SetPositionFrozenZ(bool frozen);
    bool GetPositionFrozenZ() const;

    void SetRotationFrozenX(bool frozen);
    bool GetRotationFrozenX() const;
    void SetRotationFrozenY(bool frozen);
    bool GetRotationFrozenY() const;
    void SetRotationFrozenZ(bool frozen);
    bool GetRotationFrozenZ() const;

    virtual void SetSceneTransform(const SceneTypes::Transform& t);
    virtual void GetSceneTransform(SceneTypes::Transform& t) const;

protected:
    glm::vec3 _GetCurrentSceneGraphNodeWorldPosition();
    void _FindAndAttachActiveColliders();
    void _FindAndDetachActiveColliders();

    // TODO Implement the below function in the future
    //void _DetachCollider(Collider* collider);

    PhysicsManager& m_PhysicsManager;
    PhysicsManager::RigidBodyHandle m_RigidBodyHandle;
    SceneTypes::Transform m_CurrentGlobalTransform;
};

#endif // RIGIDBODY_H_INCLUDED
