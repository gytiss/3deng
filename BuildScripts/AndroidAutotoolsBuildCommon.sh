#!/usr/bin/env bash 

SUCCESS=0

pushd `dirname $0` > /dev/null # Save script call dir and "cd" into the script dir
SCRIPT_DIR=`pwd`
popd > /dev/null # "cd" back into the script call dir

source "$SCRIPT_DIR/AndroidBuildCommon.sh"

if [ "x$BUILD_TYPE" = 'x' ] || \
   [ "x$ARCHS_TO_BUILD_FOR" = 'x' ] || \
   [ "x$SOURCE_DIR_OF_THE_LIB_BEING_BUILT" = 'x' ] || \
   [ "x$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT" = 'x' ] || \
   [ "x$RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT" = 'x' ];
then
    echo "You should not use $0 directly, use BuildLibsForAndroid.sh instead"
    exit 1
fi

RESULTING_ARMV7_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/armv7
RESULTING_ARM64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/arm64
RESULTING_MIPS_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/mips
RESULTING_MIPS64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/mips64
RESULTING_X86_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/x86
RESULTING_X86_64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/x86_64
    
# Note: Android API level 18 is the lowest level with OpenGL ES 3.0 support

cd $SOURCE_DIR_OF_THE_LIB_BEING_BUILT
./autogen.sh
    
######################################
# Build for ARMv7
######################################
if $(ContainsElement "armv7" ${ARCHS_TO_BUILD_FOR[@]});
then
    mkdir -p $RESULTING_ARMV7_BINARIES_DIR_OF_THE_LIB_BEING_BUILT

    export CFLAGS="-fPIC -fexceptions -funwind-tables -D__BIONIC__ -mfloat-abi=softfp -mfpu=vfpv3-d16 -mthumb -marm -march=armv7-a -fomit-frame-pointer -fstrict-aliasing -funswitch-loops -finline-limit=300 -ffunction-sections -funwind-tables -fstack-protector -no-canonical-prefixes "$(ReplaceArchMarkersWithArchName "$EXTRA_CFLAGS" "armv7")
    export CXXFLAGS="-fPIC -fexceptions -funwind-tables -frtti -D__BIONIC__ -mfloat-abi=softfp -mfpu=vfpv3-d16 -mthumb -marm -march=armv7-a -fomit-frame-pointer -fstrict-aliasing -funswitch-loops -finline-limit=300 -ffunction-sections -funwind-tables -fstack-protector -no-canonical-prefixes "$(ReplaceArchMarkersWithArchName "$EXTRA_CXXFLAGS" "armv7")
    
    if [ "$BUILD_TYPE" == "Debug" ];
    then
        CFLAGS="$CFLAGS -g -O0"
        CXXFLAGS="$CXXFLAGS -g -O0"
    else
        CFLAGS="$CFLAGS -DNDEBUG -O2"
        CXXFLAGS="$CXXFLAGS -DNDEBUG -O2"
    fi

    # Needed for routing around a CPU bug in some Cortex-A8 implementations
    export LDFLAGS="-march=armv7-a -Wl,--fix-cortex-a8 -no-canonical-prefixes "$(ReplaceArchMarkersWithArchName "$EXTRA_LDFLAGS" "armv7")

    ANDROID_COMPILER_TO_USE="arm-linux-androideabi" \
    PREFIX="$RESULTING_ARMV7_BINARIES_DIR_OF_THE_LIB_BEING_BUILT" \
    TARGET_ARCH="arm" \
    TARGET_ARCH_NAME="armv7" \
    NDK_PLATFORM="android-18" \
    CONFIGURE_FLAGS="$EXTRA_CONFIGURE_FLAGS" "$SCRIPT_DIR/AutotoolsAndroidBuild.sh" || { echo "ERROR: Failed to build for ARMv7" ; exit 1 ; }
fi
######################################


######################################
# Build for ARM64
######################################
if $(ContainsElement "arm64" ${ARCHS_TO_BUILD_FOR[@]});
then
    mkdir -p $RESULTING_ARM64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT

    export CFLAGS="-fPIC -fexceptions -D__BIONIC__ -march=armv8-a -mfix-cortex-a53-835769 -fomit-frame-pointer -fstrict-aliasing -funswitch-loops -finline-limit=300 -ffunction-sections -funwind-tables -fstack-protector -no-canonical-prefixes "$(ReplaceArchMarkersWithArchName "$EXTRA_CFLAGS" "arm64")
    export CXXFLAGS="-fPIC -fexceptions -frtti -D__BIONIC__ -march=armv8-a -mfix-cortex-a53-835769 -fomit-frame-pointer -fstrict-aliasing -funswitch-loops -finline-limit=300 -ffunction-sections -funwind-tables -fstack-protector -no-canonical-prefixes "$(ReplaceArchMarkersWithArchName "$EXTRA_CXXFLAGS" "arm64")
    export LDFLAGS="-no-canonical-prefixes "$(ReplaceArchMarkersWithArchName "$EXTRA_LDFLAGS" "arm64")

    if [ "$BUILD_TYPE" == "Debug" ];
    then
        CFLAGS="$CFLAGS -g -O0"
        CXXFLAGS="$CXXFLAGS -g -O0"
    else
        CFLAGS="$CFLAGS -DNDEBUG -O2"
        CXXFLAGS="$CXXFLAGS -DNDEBUG -O2"
    fi
    
    ANDROID_COMPILER_TO_USE="aarch64-linux-android" \
    PREFIX="$RESULTING_ARM64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT" \
    TARGET_ARCH="arm64" \
    TARGET_ARCH_NAME="arm64" \
    NDK_PLATFORM="android-21" \
    CONFIGURE_FLAGS="$EXTRA_CONFIGURE_FLAGS" "$(dirname "$0")/AutotoolsAndroidBuild.sh" || { echo "ERROR: Failed to build for ARM64" ; exit 1 ; }
fi
######################################


######################################
# Build for MIPS
######################################
if $(ContainsElement "mips" ${ARCHS_TO_BUILD_FOR[@]});
then
    mkdir -p $RESULTING_MIPS_BINARIES_DIR_OF_THE_LIB_BEING_BUILT

    export CFLAGS="-fPIC -fexceptions -funwind-tables -D__BIONIC__ -fomit-frame-pointer -funswitch-loops -finline-limit=300 -fno-strict-aliasing -finline-functions -ffunction-sections -funwind-tables -fmessage-length=0 -fno-inline-functions-called-once -fgcse-after-reload -frerun-cse-after-loop -frename-registers -no-canonical-prefixes "$(ReplaceArchMarkersWithArchName "$EXTRA_CFLAGS" "mips")
    export CXXFLAGS="-fPIC -fexceptions -funwind-tables -frtti -D__BIONIC__ -fomit-frame-pointer -funswitch-loops -finline-limit=300 -fno-strict-aliasing -finline-functions -ffunction-sections -funwind-tables -fmessage-length=0 -fno-inline-functions-called-once -fgcse-after-reload -frerun-cse-after-loop -frename-registers -no-canonical-prefixes "$(ReplaceArchMarkersWithArchName "$EXTRA_CXXFLAGS" "mips")
    export LDFLAGS="-no-canonical-prefixes "$(ReplaceArchMarkersWithArchName "$EXTRA_LDFLAGS" "mips")

    if [ "$BUILD_TYPE" == "Debug" ];
    then
        CFLAGS="$CFLAGS -g -O0"
        CXXFLAGS="$CXXFLAGS -g -O0"
    else
        CFLAGS="$CFLAGS -DNDEBUG -O2"
        CXXFLAGS="$CXXFLAGS -DNDEBUG -O2"
    fi
    
    ANDROID_COMPILER_TO_USE="mipsel-linux-android" \
    PREFIX="$RESULTING_MIPS_BINARIES_DIR_OF_THE_LIB_BEING_BUILT" \
    TARGET_ARCH="mips" \
    TARGET_ARCH_NAME="mips" \
    NDK_PLATFORM="android-18" \
    CONFIGURE_FLAGS="$EXTRA_CONFIGURE_FLAGS" "$(dirname "$0")/AutotoolsAndroidBuild.sh" || { echo "ERROR: Failed to build for MIPS" ; exit 1 ; }
fi
######################################


######################################
# Build for MIPS64
######################################
if $(ContainsElement "mips64" ${ARCHS_TO_BUILD_FOR[@]});
then
    mkdir -p $RESULTING_MIPS64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT

    export CFLAGS="-fPIC -fexceptions -funwind-tables -D__BIONIC__ -fno-strict-aliasing -finline-functions -ffunction-sections -funwind-tables -fmessage-length=0 -fno-inline-functions-called-once -fgcse-after-reload -frerun-cse-after-loop -frename-registers -no-canonical-prefixes -fomit-frame-pointer -funswitch-loops -finline-limit=300 "$(ReplaceArchMarkersWithArchName "$EXTRA_CFLAGS" "mips64")
    export CXXFLAGS="-fPIC -fexceptions -funwind-tables -frtti -D__BIONIC__ -fno-strict-aliasing -finline-functions -ffunction-sections -funwind-tables -fmessage-length=0 -fno-inline-functions-called-once -fgcse-after-reload -frerun-cse-after-loop -frename-registers -no-canonical-prefixes -fomit-frame-pointer -funswitch-loops -finline-limit=300 "$(ReplaceArchMarkersWithArchName "$EXTRA_CXXFLAGS" "mips64")
    export LDFLAGS="-no-canonical-prefixes "$(ReplaceArchMarkersWithArchName "$EXTRA_LDFLAGS" "mips64")

    if [ "$BUILD_TYPE" == "Debug" ];
    then
        CFLAGS="$CFLAGS -g -O0"
        CXXFLAGS="$CXXFLAGS -g -O0"
    else
        CFLAGS="$CFLAGS -DNDEBUG -O2"
        CXXFLAGS="$CXXFLAGS -DNDEBUG -O2"
    fi
    
    ANDROID_COMPILER_TO_USE="mips64el-linux-android" \
    PREFIX="$RESULTING_MIPS64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT" \
    TARGET_ARCH="mips64" \
    TARGET_ARCH_NAME="mips64" \
    NDK_PLATFORM="android-21" \
    CONFIGURE_FLAGS="$EXTRA_CONFIGURE_FLAGS" "$(dirname "$0")/AutotoolsAndroidBuild.sh" || { echo "ERROR: Failed to build for MIPS64" ; exit 1 ; }
fi   
######################################


######################################
# Build for x86
######################################
if $(ContainsElement "x86" ${ARCHS_TO_BUILD_FOR[@]});
then
    mkdir -p $RESULTING_X86_BINARIES_DIR_OF_THE_LIB_BEING_BUILT

    export CFLAGS="-fPIC -fexceptions -funwind-tables -D__BIONIC__ -ffunction-sections -funwind-tables -no-canonical-prefixes -fstack-protector -fomit-frame-pointer -fstrict-aliasing -funswitch-loops -finline-limit=300 "$(ReplaceArchMarkersWithArchName "$EXTRA_CFLAGS" "x86")
    export CXXFLAGS="-fPIC -fexceptions -funwind-tables -frtti -D__BIONIC__ -ffunction-sections -funwind-tables -no-canonical-prefixes -fstack-protector -fomit-frame-pointer -fstrict-aliasing -funswitch-loops -finline-limit=300 "$(ReplaceArchMarkersWithArchName "$EXTRA_CXXFLAGS" "x86")
    export LDFLAGS="-no-canonical-prefixes "$(ReplaceArchMarkersWithArchName "$EXTRA_LDFLAGS" "x86")

    if [ "$BUILD_TYPE" == "Debug" ];
    then
        CFLAGS="$CFLAGS -g -O0"
        CXXFLAGS="$CXXFLAGS -g -O0"
    else
        CFLAGS="$CFLAGS -DNDEBUG -O2"
        CXXFLAGS="$CXXFLAGS -DNDEBUG -O2"
    fi
    
    ANDROID_COMPILER_TO_USE="i686-linux-android" \
    PREFIX="$RESULTING_X86_BINARIES_DIR_OF_THE_LIB_BEING_BUILT" \
    TARGET_ARCH="x86" \
    TARGET_ARCH_NAME="x86" \
    NDK_PLATFORM="android-18" \
    CONFIGURE_FLAGS="$EXTRA_CONFIGURE_FLAGS" "$(dirname "$0")/AutotoolsAndroidBuild.sh" || { echo "ERROR: Failed to build for x86" ; exit 1 ; }
fi    
######################################    
    
######################################
# Build for x86_64
###################################### 
if $(ContainsElement "x86_64" ${ARCHS_TO_BUILD_FOR[@]});
then
    mkdir -p $RESULTING_X86_64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT

    export CFLAGS="-fPIC -fexceptions -funwind-tables -D__BIONIC__ -ffunction-sections -funwind-tables -fstack-protector -no-canonical-prefixes -fomit-frame-pointer -fstrict-aliasing -funswitch-loops -finline-limit=300 "$(ReplaceArchMarkersWithArchName "$EXTRA_CFLAGS" "x86_64")
    export CXXFLAGS="-fPIC -fexceptions -funwind-tables -frtti -D__BIONIC__ -ffunction-sections -funwind-tables -fstack-protector -no-canonical-prefixes -fomit-frame-pointer -fstrict-aliasing -funswitch-loops -finline-limit=300 "$(ReplaceArchMarkersWithArchName "$EXTRA_CXXFLAGS" "x86_64")
    export LDFLAGS="-no-canonical-prefixes "$(ReplaceArchMarkersWithArchName "$EXTRA_LDFLAGS" "x86_64")

    if [ "$BUILD_TYPE" == "Debug" ];
    then
        CFLAGS="$CFLAGS -g -O0"
        CXXFLAGS="$CXXFLAGS -g -O0"
    else
        CFLAGS="$CFLAGS -DNDEBUG -O2"
        CXXFLAGS="$CXXFLAGS -DNDEBUG -O2"
    fi
    
    ANDROID_COMPILER_TO_USE="x86_64-linux-android" \
    PREFIX="$RESULTING_X86_64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT" \
    TARGET_ARCH="x86_64" \
    TARGET_ARCH_NAME="x86_64" \
    NDK_PLATFORM="android-21" \
    CONFIGURE_FLAGS="$EXTRA_CONFIGURE_FLAGS" "$(dirname "$0")/AutotoolsAndroidBuild.sh" || { echo "ERROR: Failed to build for x86_64" ; exit 1 ; }
fi
######################################

SUCCESS=1

if [ "$SUCCESS" -eq 1 ] && \
   $(ContainsElement "armv7" ${ARCHS_TO_BUILD_FOR[@]}) && \
   ! $(CheckIfCompilationResultsExist "$RESULTING_ARMV7_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/lib" "$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT")
then
    SUCCESS=0
    echo "Failed to build one or more of the \"$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT\" for the armv7 Android ABI"
fi

if [ "$SUCCESS" -eq 1 ] && \
   $(ContainsElement "arm64" ${ARCHS_TO_BUILD_FOR[@]}) && \
   ! $(CheckIfCompilationResultsExist "$RESULTING_ARM64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/lib" "$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT")
then
    SUCCESS=0
    echo "Failed to build one or more of the \"$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT\" for the arm64 Android ABI"
fi

if [ "$SUCCESS" -eq 1 ] && \
   $(ContainsElement "mips" ${ARCHS_TO_BUILD_FOR[@]}) && \
   ! $(CheckIfCompilationResultsExist "$RESULTING_MIPS_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/lib" "$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT")
then
    SUCCESS=0
    echo "Failed to build one or more of the \"$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT\" for the mips Android ABI"
fi

if [ "$SUCCESS" -eq 1 ] && \
   $(ContainsElement "mips64" ${ARCHS_TO_BUILD_FOR[@]}) && \
   ! $(CheckIfCompilationResultsExist "$RESULTING_MIPS64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/lib" "$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT")
then
    SUCCESS=0
    echo "Failed to build one or more of the \"$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT\" for the mips64 Android ABI"
fi

if [ "$SUCCESS" -eq 1 ] && \
   $(ContainsElement "x86" ${ARCHS_TO_BUILD_FOR[@]}) && \
   ! $(CheckIfCompilationResultsExist "$RESULTING_X86_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/lib" "$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT")
then
    SUCCESS=0
    echo "Failed to build one or more of the \"$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT\" for the x86 Android ABI"
fi

if [ "$SUCCESS" -eq 1 ] && \
   $(ContainsElement "x86_64" ${ARCHS_TO_BUILD_FOR[@]}) && \
   ! $(CheckIfCompilationResultsExist "$RESULTING_X86_64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/lib" "$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT")
then
    SUCCESS=0
    echo "Failed to build one or more of the \"$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT\" for the x86_64 Android ABI"
fi


# Set script return code
if [ "$SUCCESS" -eq 1 ];
then
    exit 0 # Success
else
    exit 1 # Failure
fi
