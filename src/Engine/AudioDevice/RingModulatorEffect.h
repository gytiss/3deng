#ifndef AUDIODEVICE_RINGMODULATOREFFECT_H_INCLUDED
#define AUDIODEVICE_RINGMODULATOREFFECT_H_INCLUDED

#include "Effect.h"

namespace AudioDevice
{

class Context;
class EffectSlot;

/**
 * The ring modulator multiplies an input signal by a carrier signal in the time domain,
 * resulting in tremolo or inharmonic effects.
 */
class RingModulatorEffect : public Effect
{
public:
    RingModulatorEffect(Context& context, EffectSlot& effectSlot);
    ~RingModulatorEffect() = default;

    enum WaveForm
    {
        e_WaveFormSinusoid = AL_RING_MODULATOR_SINUSOID,
        e_WaveFormSawTooth = AL_RING_MODULATOR_SAWTOOTH,
        e_WaveFormSquare = AL_RING_MODULATOR_SQUARE,

        e_WaveFormCount // Not a wave form (Used for counting)
    };

   /**
    * This is the frequency of the carrier signal. If the carrier signal is slowly varying (less than 20 Hz),
    * the result is a tremolo (slow amplitude variation) effect. If the carrier signal is in the audio range,
    * audible upper and lower sidebands begin to appear, causing an inharmonic effect. The carrier
    * signal itself is not heard in the output.
    *
    * Value Units: Hz
    * Value range: 0.0 to 8000.0
    */
    void SetFrequency(float frequency);

   /**
    * This controls the cutoff frequency at which the in put signal is high-pass filtered before being ring
    * modulated.  If the cutoff frequency is 0, the entire signal will be ring modulated. If the cutoff 114/144
    * frequency is high, very little of the signal (only those parts above the cutoff) will be ring modulated.
    *
    * Value Units: Hz
    * Value range: 0.0 to 24000.0
    */
    void SetHighpassCutoff(float cutoffValue);

   /**
    * This controls which waveform is used as the carrier signal.  Traditional ring modulator and
    * tremolo effects generally use a sinusoidal carrier.  Sawtooth and square waveforms are may
    * cause unpleasant aliasing.
    *
    * Value Units: Enumeration
    * Value range: 0 (sin), 1 (saw), 2 (square)
    */
    void SetWaveForm(WaveForm waveForm);

    float GetFrequency();
    float GetHighpassCutoff();
    WaveForm GetWaveForm();

    inline float GetMinFrequencyValue()
    {
        return AL_RING_MODULATOR_MIN_FREQUENCY;
    }

    inline float GetMaxFrequencyValue()
    {
        return AL_RING_MODULATOR_MAX_FREQUENCY;
    }

    inline float GetMinHighpassCutoffValue()
    {
        return AL_RING_MODULATOR_MIN_HIGHPASS_CUTOFF;
    }

    inline float GetMaxHighpassCutoffValue()
    {
        return AL_RING_MODULATOR_MAX_HIGHPASS_CUTOFF;
    }

    inline WaveForm GetMinWaveFormValue()
    {
        return static_cast<WaveForm>( AL_RING_MODULATOR_MIN_WAVEFORM );
    }

    inline WaveForm GetMaxWaveFormValue()
    {
        return static_cast<WaveForm>( AL_RING_MODULATOR_MAX_WAVEFORM );
    }

private:
    // Used to prevent copying
    RingModulatorEffect& operator = (const RingModulatorEffect& other) = delete;
    RingModulatorEffect(const RingModulatorEffect& other) = delete;
};

}

#endif // AUDIODEVICE_RINGMODULATOREFFECT_H_INCLUDED
