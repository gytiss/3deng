#include "Context.h"
#include "Device.h"
#include <Units.h>
#include <Camera.h>
#include "OpenALIncludes.h"
#include "OpenALErrorChecking.h"
#include <Exceptions/GenericException.h>
#include <cassert>

namespace AudioDevice
{

const float Context::c_SpeedOfSoundInAir = 343.3f;
const float Context::c_SpeedOfSoundInWater = 1484.0f;

ALCcontext* Context::m_CurrentlyActiveContext = nullptr;

//-----------------------------------------------------------------------

Context::Context(Device& device)
: m_ALContext( nullptr ),
  m_Device( device ),
  m_NumberOfAuxiliaryEffectsSlotsSupportedPerSource( 0 )
{
    ALint attribs[4] = { 0 };

    attribs[0] = ALC_MAX_AUXILIARY_SENDS;
    attribs[1] = 16;

    m_ALContext = alcCreateContext( device.GetALHandle(), attribs );
    CheckForALCErrors( m_Device );

    if( m_ALContext == nullptr )
    {
        throw GenericException( "Failed to create an OpenAL context" );
    }

    Bind();

    ALCint numberOfAuxiliaryEffectsSlotsSupportedPerSource = 0;
    alcGetIntegerv( m_Device.GetALHandle(), ALC_MAX_AUXILIARY_SENDS, 1, &numberOfAuxiliaryEffectsSlotsSupportedPerSource );
    CheckForALCErrors( m_Device );

    assert( ( numberOfAuxiliaryEffectsSlotsSupportedPerSource >= 0 ) && "This value can never be negative" );

    m_NumberOfAuxiliaryEffectsSlotsSupportedPerSource = numberOfAuxiliaryEffectsSlotsSupportedPerSource;

    if( m_NumberOfAuxiliaryEffectsSlotsSupportedPerSource < c_MinimumEffectSlotsConnectionsPerSource )
    {
        throw GenericException( "Support for at least four OpenAL Auxiliary effects slots is needed" );
    }

    // Make sure that distance units used by the Engine and the OpenAL are the same
    alListenerf( AL_METERS_PER_UNIT, ( 1.0f / Units::OneMeter ) );
    CheckForALErrors( *this );
}

//-----------------------------------------------------------------------

Context::~Context()
{
    if( m_ALContext )
    {
        alcDestroyContext( m_ALContext );
    }
}

//-----------------------------------------------------------------------

void Context::Bind()
{
    if( m_ALContext != m_CurrentlyActiveContext )
    {
        ALCboolean retVal = alcMakeContextCurrent( m_ALContext );
        CheckForALCErrors( m_Device );

        if( retVal == ALC_TRUE )
        {
            m_CurrentlyActiveContext = m_ALContext;
        }
        else
        {
            throw GenericException( "Failed to make OpenAL context current" );
        }
    }
}

//-----------------------------------------------------------------------

void Context::Unbind()
{
    if( m_ALContext == m_CurrentlyActiveContext )
    {
        ALCboolean retVal = alcMakeContextCurrent( nullptr );
        CheckForALCErrors( m_Device );

        if( retVal == ALC_TRUE )
        {
            m_CurrentlyActiveContext = nullptr;
        }
        else
        {
            throw GenericException( "Failed to make OpenAL context not current" );
        }
    }
}

//-----------------------------------------------------------------------

void Context::SetMasterGain(float positiveGainValue)
{
    Bind();

    alListenerf( AL_GAIN, positiveGainValue );
    CheckForALErrors( *this );
}

//-----------------------------------------------------------------------

void Context::SetListenerPosition(const glm::vec3& positionInWorldSpace)
{
    ALfloat position[3] = { positionInWorldSpace.x,
                            positionInWorldSpace.y,
                            positionInWorldSpace.z };

    Bind();

    alListenerfv( AL_POSITION, position );
    CheckForALErrors( *this );
}

//-----------------------------------------------------------------------

void Context::SetListenersVelocity(const glm::vec3& velocityVector)
{
    ALfloat velocity[3] = { velocityVector.x,
                            velocityVector.y,
                            velocityVector.z };

    Bind();

    alListenerfv( AL_VELOCITY, velocity );
    CheckForALErrors( *this );
}

//-----------------------------------------------------------------------

void Context::SetOrienstation(const glm::vec3& atVector, const glm::vec3& upVector)
{
    ALfloat alOrientation[6] =
    {
        atVector.x,
        atVector.y,
        atVector.z,
        upVector.x,
        upVector.y,
        upVector.z
    };

    Bind();

    alListenerfv( AL_ORIENTATION, alOrientation );
    CheckForALErrors( *this );
}

//-----------------------------------------------------------------------

void Context::SetOrienstation(const Camera& cameraToGetTheOrientationFrom)
{
    SetOrienstation( cameraToGetTheOrientationFrom.GetDirectionVector(),
                     cameraToGetTheOrientationFrom.GetUpVector() );
}

//-----------------------------------------------------------------------

float Context::GetMasterGain()
{
    ALfloat masterGainValue = 0.0f;

    Bind();

    alGetListenerf( AL_GAIN, &masterGainValue );
    CheckForALErrors( *this );

    return masterGainValue;
}

//-----------------------------------------------------------------------

glm::vec3 Context::GetListenerPosition()
{
    ALfloat position[3] = { 0.0f, 0.0f, 0.0f };

    Bind();

    alGetListenerfv( AL_POSITION, position );
    CheckForALErrors( *this );

    return glm::vec3( position[0], position[1], position[2] );
}

//-----------------------------------------------------------------------

glm::vec3 Context::GetListenersVelocity()
{
    ALfloat velocity[3] = { 0.0f, 0.0f, 0.0f };

    Bind();

    alGetListenerfv( AL_VELOCITY, velocity );
    CheckForALErrors( *this );

    return glm::vec3( velocity[0], velocity[1], velocity[2] );
}

//-----------------------------------------------------------------------

void Context::GetOrienstation(glm::vec3& outAtVector, glm::vec3& outUpVector)
{
    ALfloat orientation[6] = { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };

    Bind();

    alGetListenerfv( AL_ORIENTATION, orientation );
    CheckForALErrors( *this );

    outAtVector.x = orientation[0];
    outAtVector.y = orientation[1];
    outAtVector.z = orientation[2];

    outUpVector.x = orientation[0];
    outUpVector.y = orientation[1];
    outUpVector.z = orientation[2];
}

//-----------------------------------------------------------------------

void Context::SetGlobalDopplerFactor(float factor)
{
    if( factor > 0.0f )
    {
        Bind();

        alDopplerFactor( factor );
        CheckForALErrors( *this );
    }
    else
    {
        throw GenericException( "Global Doppler factor must not be negative" );
    }
}

//-----------------------------------------------------------------------

void Context::SetSpeedOfSound(float speedInMetersPerSecond)
{
    if( speedInMetersPerSecond > 0.0f )
    {
        Bind();

        alSpeedOfSound( speedInMetersPerSecond );
        CheckForALErrors( *this );
    }
    else
    {
        throw GenericException( "Speed of sound has to be greater than zero" );
    }
}

//-----------------------------------------------------------------------

float Context::GetGlobalDopplerFactor()
{
    Bind();

    const ALfloat globalDopplerFactor = alGetFloat( AL_DOPPLER_FACTOR );

    return globalDopplerFactor;
}

//-----------------------------------------------------------------------

float Context::GetSpeedOfSound()
{
    Bind();

    const ALfloat speedOfSound = alGetFloat( AL_SPEED_OF_SOUND );

    return speedOfSound;
}

//-----------------------------------------------------------------------

}
