#ifndef MSGLOGGER_H
#define MSGLOGGER_H

#include <string>

class MsgLogger
{
    public:
        enum MsgType {ERROR_MSG, WARNING_MSG, EVENT_MSG};
        static void log(MsgType mt, std::string msg);
        static inline void LogEvent(const std::string& msg) { log( EVENT_MSG, msg ); }
        static inline void LogError(const std::string& msg) { log( ERROR_MSG, msg ); }
        static inline void LogWarning(const std::string& msg) { log( WARNING_MSG, msg ); }
};

#endif // MSGLOGGER_H
