
in vec3 in_VertexPos;
in vec2 in_VertexTextureCoords;

uniform mat4 u_ModelViewProjectionMatrix;

out vec2 ex_uvCoord;

void main(void)
{
    ex_uvCoord = in_VertexTextureCoords;
    gl_Position = u_ModelViewProjectionMatrix * vec4(in_VertexPos.xy, -0.1f, 1.0f);
}
