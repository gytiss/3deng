#include "FireButton.h"
#include <MathUtils.h>
#include <Renderer/RenderManager.h>
#include <Resources/MeshManager.h>
#include <Resources/MaterialManager.h>
#include <Resources/TextureManager.h>
#include <Scene/Transform.h>
#include <Scene/MeshRenderer.h>
#include <Scene/MeshFilter.h>

//-----------------------------------------------------------------------

FireButton::FireButton(const glm::vec2& centre,
                       float radius,
                       MeshManager& meshManager,
                       MaterialManager& materialManager,
                       TextureManager& textureManager,
                       SystemInterface& systemInterface)
: m_NormalizedCentre( centre ),
  m_NormalizedRadius( radius ),
  m_AbsoluteCenter( 0.0f ),
  m_AbsoluteRadius( 0.0f ),
  m_CurrentAbsolutePos( 0.0f ),
  m_MeshManager( meshManager ),
  m_MaterialManager( materialManager ),
  m_TextureManager( textureManager ),
  m_SystemInterface( systemInterface ),
  m_DrawableSurfaceSize( m_SystemInterface.GetDrawableSurfaceSize() ),
  m_Pressed( false ),
  m_FireButtonGameObject( "Fire Button" )
{
    _DrawableSurfaceSizeChaged( m_SystemInterface.GetDrawableSurfaceSize(), this );
    _CreateVisuals( m_FireButtonGameObject );
    m_SystemInterface.RegisterDrawableSurfaceSizeChangedCallback( _DrawableSurfaceSizeChaged, this );
}

//-----------------------------------------------------------------------

FireButton::~FireButton()
{
    m_SystemInterface.DeregisterDrawableSurfaceSizeChangedCallback( _DrawableSurfaceSizeChaged, this );
}

//-----------------------------------------------------------------------

bool FireButton::LocationIsOnTheButton(const glm::vec2& absoluteScreenCoord)
{
    bool retVal = true;

    retVal = retVal && ( absoluteScreenCoord.x >= ( m_AbsoluteCenter.x - m_AbsoluteRadius ) );
    retVal = retVal && ( absoluteScreenCoord.x <= ( m_AbsoluteCenter.x + m_AbsoluteRadius ) );
    retVal = retVal && ( absoluteScreenCoord.y >= ( m_AbsoluteCenter.y - m_AbsoluteRadius ) );
    retVal = retVal && ( absoluteScreenCoord.y <= ( m_AbsoluteCenter.y + m_AbsoluteRadius ) );

    return retVal;
}

//-----------------------------------------------------------------------

void FireButton::SetPressed()
{
    if( !m_Pressed )
    {
        m_Pressed = true;
    }
}

//-----------------------------------------------------------------------

void FireButton::Reset()
{
    m_Pressed = false;

    m_CurrentAbsolutePos = m_AbsoluteCenter;

    m_FireButtonGameObject.GetComponent<Transform>()->SetPosition( glm::vec3( m_AbsoluteCenter.x, m_AbsoluteCenter.y, 0.0f ) );
    m_FireButtonGameObject.GetComponent<Transform>()->SetScale( glm::vec3( m_AbsoluteRadius, 1.0f, m_AbsoluteRadius ) );
}

//-----------------------------------------------------------------------

void FireButton::AddRenderables(RenderManager& renderManager)
{
    GameObject::RenderablesList renderables;
    m_FireButtonGameObject.GetRenderablesOfTheHierarchy( renderables );

    for(const RenderableObject& r : renderables)
    {
        renderManager.AddGUIRenderables( r );
    }
}

//-----------------------------------------------------------------------

void FireButton::_CreateVisuals(GameObject& baseGameObject)
{
    MaterialDescriptor fireButtonMaterialDescriptor( m_TextureManager );
    MaterialDescriptor joystickThumbMaterialDescriptor( m_TextureManager );

    TextureManager::TextureHandle baseTextureHandle = m_TextureManager.Load2DTexture( "DemoRoomData/Models/FireButton/FireButtonBackgroundTransparent.mtc",
                                                                                      Texture::e_TypeIdxDiffuseMap,
                                                                                      Texture::e_StateParamArgSCoordWrapClampToEdge,
                                                                                      Texture::e_StateParamArgTCoordWrapClampToEdge,
                                                                                      false,
                                                                                      false );
    fireButtonMaterialDescriptor.SetDiffuseTextureMap( baseTextureHandle );
    fireButtonMaterialDescriptor.SetDiffuseColor( EngineTypes::SRGBColor( 0.0f, 0.0f, 1.0f ) );

    MaterialManager::MaterialHandle buttonMaterial = m_MaterialManager.AddInternalMaterial( fireButtonMaterialDescriptor );

    MeshRenderer* baseRenderer = new MeshRenderer( baseGameObject, m_MaterialManager );
    MeshFilter* baseMeshFilter = new MeshFilter( baseGameObject, m_MeshManager );
    MeshManager::MeshHandle baseMeshHandle = m_MeshManager.GetInternalMeshHandle( MeshManager::e_InternalMeshTypePlane );
    baseMeshFilter->SetMesh( baseMeshHandle );
    baseRenderer->SetSubMeshMaterial( 0, buttonMaterial );
    baseRenderer->SetDepthTestingEnabled( false );

    // Rotate the planes so they are facing the screen
    baseGameObject.GetComponent<Transform>()->SetRotation( glm::vec3( 90.0f, 0.0f, 0.0f ) );
}

//-----------------------------------------------------------------------

void FireButton::_RecalculateAbsoluteSize()
{
    m_AbsoluteCenter.x = ( m_NormalizedCentre.x * m_DrawableSurfaceSize.widthInPixels );
    m_AbsoluteCenter.y = ( m_NormalizedCentre.y * m_DrawableSurfaceSize.heightInPixels );

    size_t smallestSceenSide = m_DrawableSurfaceSize.heightInPixels;
    if( m_DrawableSurfaceSize.widthInPixels < m_DrawableSurfaceSize.heightInPixels )
    {
        smallestSceenSide = m_DrawableSurfaceSize.widthInPixels;
    }

    m_AbsoluteRadius = ( m_NormalizedRadius * smallestSceenSide );

    Reset();
}

//-----------------------------------------------------------------------

void FireButton::_DrawableSurfaceSizeChaged(const SystemInterface::DrawableSurfaceSize& newSize, void* userData)
{
    static_cast<FireButton*>( userData )->m_DrawableSurfaceSize = newSize;
    static_cast<FireButton*>( userData )->_RecalculateAbsoluteSize();
}

//-----------------------------------------------------------------------
