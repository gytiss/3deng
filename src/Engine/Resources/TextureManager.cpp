#include "TextureManager.h"
#include "InternalTexturesData.h"
#include "MTC.h"
#include <cassert>
#include <StringUtils.h>
#include <CommonDefinitions.h>
#include <GraphicsDevice/GraphicsDevice.h>
#include <Exceptions/GenericException.h>

//-----------------------------------------------------------------------

const char* TextureManager::c_InternalTexturePathPrefix = "\\internalTextureMap/:";

//-----------------------------------------------------------------------

const char* TextureManager::c_InternalTextureNames[e_InternalTextureTypeCount] =
{
    "NullNormalMap",
    "NullSpecularMap"
};

//-----------------------------------------------------------------------

bool TextureManager::m_ClassAlreadyInstantiated = false;

//-----------------------------------------------------------------------

TextureManager::TextureManager(GraphicsDevice& graphicsDevice, FileManager& fileManager)
: m_GraphicsDevice( graphicsDevice ),
  m_FileManager( fileManager ),
  m_PathsToTexturesMap(),
  m_TexturePathsList(),
  m_CurrentTextureFilteringMode( Texture::e_FilteringModeNoFiltering )
{
    if( m_ClassAlreadyInstantiated )
    {
        throw GenericException( "There can only be one instance of TextureManager()" );
    }
    else
    {
        m_ClassAlreadyInstantiated = true;
    }
}

//-----------------------------------------------------------------------

TextureManager::~TextureManager()
{
    for(PathsToTexturesMap::iterator it = m_PathsToTexturesMap.begin(); it != m_PathsToTexturesMap.end(); ++it)
    {
        RefCountedTexturePtrList& ptrList = it->second;

        for(RefCountedTexturePtrList::iterator ptrListIt = ptrList.begin(); ptrListIt != ptrList.end(); ++ptrListIt)
        {
            RefCountedTexturePtr& refCountedTexPtr = *ptrListIt;

            if( refCountedTexPtr.texture != nullptr )
            {
                delete refCountedTexPtr.texture;
            }
        }
    }

    m_PathsToTexturesMap.clear();
}

//-----------------------------------------------------------------------

TextureManager::TextureHandle TextureManager::LoadInternal2DTexture(InternalTextureType type,
                                                                    Texture::TypeIdx typeIdx,
                                                                    Texture::StateParamArg sWrapMode,
                                                                    Texture::StateParamArg tWrapMode,
                                                                    bool isThisTextureMipMapped,
                                                                    bool isThisTextureSRGBEncoded)
{
    class InternalTextureLoader
    {
    public:
        InternalTextureLoader(FileManager& fileManager, InternalTextureType type)
        : m_FileManager( fileManager ),
          m_Type( type )
        {
        }

        void operator ()(const std::string& UNUSED_PARAM( textureFilePath ),
                         bool isThisTextureSRGBEncoded,
                         Texture* outTexture)
        {
            ImageUtils::ImageData_t* imgData = nullptr;

            if( m_Type == e_InternalTextureTypeNullNormapMap )
            {
                imgData = ImageUtils::LoadImage( m_FileManager,
                                                 ImageUtils::TGA,
                                                 InternalTexturesData::m_NullNormalMapDataBuffer,
                                                 InternalTexturesData::m_NullNormalMapDataBufferSizeInBytes );
            }
            else if( m_Type == e_InternalTextureTypeNullSpecularMap )
            {
                imgData = ImageUtils::LoadImage( m_FileManager,
                                                 ImageUtils::TGA,
                                                 InternalTexturesData::m_NullSpecularMapDataBuffer,
                                                 InternalTexturesData::m_NullSpecularMapDataBufferSizeInBytes );
            }
            else
            {
                throw GenericException( "Unhandled internal texture type" );
            }

            if( imgData != nullptr )
            {
                outTexture->SetData( Texture::e_DataClass2DTexture, *imgData, isThisTextureSRGBEncoded );
                ImageUtils::DeleteImage( imgData );
            }
            else
            {
                throw GenericException( "Failed to read internal texture" );
            }
        }

    private:
        FileManager& m_FileManager;
        InternalTextureType m_Type;
    };

    std::string internalTextureName = c_InternalTexturePathPrefix;
    internalTextureName += c_InternalTextureNames[type];
    return _Load2DTexture( internalTextureName,
                           typeIdx,
                           InternalTextureLoader( m_FileManager, type ),
                           sWrapMode,
                           tWrapMode,
                           isThisTextureMipMapped,
                           isThisTextureSRGBEncoded );
}

//-----------------------------------------------------------------------

TextureManager::TextureHandle TextureManager::Load2DTexture(const std::string& textureFilePath,
                                                            Texture::TypeIdx typeIdx,
                                                            Texture::StateParamArg sWrapMode,
                                                            Texture::StateParamArg tWrapMode,
                                                            bool isThisTextureMipMapped,
                                                            bool isThisTextureSRGBEncoded)
{
    class FileTextureLoader
    {
    public:
        FileTextureLoader(FileManager& fileManager)
        : m_FileManager( fileManager )
        {
        }

        void operator ()(const std::string& textureFilePath,
                         bool isThisTextureSRGBEncoded,
                         Texture* outTexture)
        {
            bool loadedOK = false;

            if( StringUitls::EndsWith( textureFilePath, ".mtc" ) )
            {
                MTC textureContainer( m_FileManager );
                if( textureContainer.LoadFromFile( textureFilePath ) )
                {
                    outTexture->SetData( textureContainer, isThisTextureSRGBEncoded );
                    loadedOK = true;
                }
            }
            else if( StringUitls::EndsWith( textureFilePath, ".tga" ) )
            {
                ImageUtils::ImageData_t* imgData = ImageUtils::LoadImage( m_FileManager, ImageUtils::TGA, textureFilePath );

                if( imgData != nullptr )
                {
                    outTexture->SetData( Texture::e_DataClass2DTexture, *imgData, isThisTextureSRGBEncoded );
                    ImageUtils::DeleteImage( imgData );
                    loadedOK = true;
                }
            }
            else
            {
                throw GenericException( "Image \"" + textureFilePath + "\" is of unsupported format" );
            }

            if( !loadedOK )
            {
                throw GenericException( "Failed to load image \"" + textureFilePath + "\"" );
            }
        }

    private:
        FileManager& m_FileManager;
    };

    return _Load2DTexture( textureFilePath,
                           typeIdx,
                           FileTextureLoader( m_FileManager ),
                           sWrapMode,
                           tWrapMode,
                           isThisTextureMipMapped,
                           isThisTextureSRGBEncoded );
}

//-----------------------------------------------------------------------

TextureManager::TextureHandle TextureManager::Load3DTexture(const std::string& UNUSED_PARAM(textureFilePath),
                                                            Texture::TypeIdx typeIdx,
                                                            Texture::StateParamArg UNUSED_PARAM(sWrapMode),
                                                            Texture::StateParamArg UNUSED_PARAM(tWrapMode),
                                                            Texture::StateParamArg UNUSED_PARAM(rWrapMode))
{
    assert( false && "This method is a stub and needs to be implemented" );
    return TextureHandle( new Texture( m_GraphicsDevice, typeIdx, Texture::e_Class3DTexture ), m_TexturePathsList.end() );
}

//-----------------------------------------------------------------------

TextureManager::TextureHandle TextureManager::LoadCubeMapTexture(const std::string& UNUSED_PARAM(textureFilePath),
                                                                 Texture::TypeIdx typeIdx,
                                                                 Texture::StateParamArg UNUSED_PARAM(sWrapMode),
                                                                 Texture::StateParamArg UNUSED_PARAM(tWrapMode),
                                                                 Texture::StateParamArg UNUSED_PARAM(rWrapMode))
{
    assert( false && "This method is a stub and needs to be implemented" );
    return TextureHandle( new Texture( m_GraphicsDevice, typeIdx, Texture::e_ClassCubeMapTexture ), m_TexturePathsList.end() );
}

//-----------------------------------------------------------------------

void TextureManager::ReleaseTexture(TextureHandle& handle)
{
    PathsToTexturesMap::iterator it = m_PathsToTexturesMap.find( *handle.m_TexturePathIt );

    if( it != m_PathsToTexturesMap.end() )
    {
        RefCountedTexturePtrList& ptrList = it->second;
        RefCountedTexturePtrList::iterator refCountedTexturePtrListIt = _FindRefCountedTexturePtr( ptrList, handle );

        if( refCountedTexturePtrListIt != ptrList.end() )
        {
            RefCountedTexturePtr& refCountedTexPtr = *refCountedTexturePtrListIt;

            refCountedTexPtr.refCount = ( refCountedTexPtr.refCount - 1 );

            if( refCountedTexPtr.refCount == 0 )
            {
                if( refCountedTexPtr.texture != nullptr )
                {
                    delete refCountedTexPtr.texture;
                }

                ptrList.erase( refCountedTexturePtrListIt );
            }
        }

        if( ptrList.empty() )
        {
            m_PathsToTexturesMap.erase( it );
            m_TexturePathsList.erase( handle.m_TexturePathIt );
        }
    }
    else
    {
        throw GenericException( "Something went terribly wrong in texture manager" );
    }
}

//-----------------------------------------------------------------------

TextureManager::TextureHandle TextureManager::DuplicateTextureHandle(const TextureHandle& handle)
{
    TextureHandle duplicate = handle;

    PathsToTexturesMap::iterator it = m_PathsToTexturesMap.find( *duplicate.m_TexturePathIt );

    if( it != m_PathsToTexturesMap.end() )
    {
        RefCountedTexturePtrList::iterator refCountedTexturePtrListIt = _FindRefCountedTexturePtr( it->second, duplicate );

        if( refCountedTexturePtrListIt != it->second.end() )
        {
            RefCountedTexturePtr& refCountedTexPtr = *refCountedTexturePtrListIt;
            refCountedTexPtr.refCount = ( refCountedTexPtr.refCount + 1 );
        }
    }
    else
    {
        throw GenericException( "Something went terribly wrong in texture manager" );
    }

    return duplicate;
}

//-----------------------------------------------------------------------

const std::string& TextureManager::GetTextureFilePath(const TextureHandle& handle) const
{
    return *handle.m_TexturePathIt;
}

//-----------------------------------------------------------------------

void TextureManager::SetTextureFilteringMode(Texture::FilteringMode mode)
{
    if( mode != m_CurrentTextureFilteringMode )
    {
        m_CurrentTextureFilteringMode = mode;

        if( !m_PathsToTexturesMap.empty() )
        {
            for(PathsToTexturesMap::iterator it = m_PathsToTexturesMap.begin(); it != m_PathsToTexturesMap.end(); ++it)
            {
                RefCountedTexturePtrList& ptrList = it->second;

                for(RefCountedTexturePtrList::iterator ptrListIt = ptrList.begin(); ptrListIt != ptrList.end(); ++ptrListIt)
                {
                    RefCountedTexturePtr& refCountedTexPtr = *ptrListIt;

                    refCountedTexPtr.texture->SetFilteringMode( m_CurrentTextureFilteringMode );
                }
            }
        }
    }
}

//-----------------------------------------------------------------------

TextureManager::TextureHandle TextureManager::_Load2DTexture(const std::string& textureFilePath,
                                                             Texture::TypeIdx typeIdx,
                                                             std::function<void(const std::string&, bool, Texture*)> loadTexture,
                                                             Texture::StateParamArg sWrapMode,
                                                             Texture::StateParamArg tWrapMode,
                                                             bool isThisTextureMipMapped,
                                                             bool isThisTextureSRGBEncoded)
{
    std::list<std::string>::iterator texturePathIt = m_TexturePathsList.end();
    Texture* texture = nullptr;

    PathsToTexturesMap::iterator it = m_PathsToTexturesMap.find( textureFilePath );

    bool createNewTexture = true;

    if( it != m_PathsToTexturesMap.end() )
    {
        RefCountedTexturePtrList& ptrList = it->second;

        for(RefCountedTexturePtrList::iterator ptrListIt = ptrList.begin(); ( ptrListIt != ptrList.end() ) && createNewTexture; ++ptrListIt)
        {
            RefCountedTexturePtr& refCountedTexPtr = *ptrListIt;
            texturePathIt = refCountedTexPtr.texturePathIt;

            if( ( refCountedTexPtr.texture->GetClass() == Texture::e_Class2DTexture ) &&
                ( refCountedTexPtr.texture->GetStateParameterVal( Texture::e_StateParamSCoordWrapType ) == sWrapMode ) &&
                ( refCountedTexPtr.texture->GetStateParameterVal( Texture::e_StateParamTCoordWrapType ) == tWrapMode ) )
            {
                refCountedTexPtr.refCount = ( refCountedTexPtr.refCount + 1 );
                texturePathIt = refCountedTexPtr.texturePathIt;
                texture = refCountedTexPtr.texture;
                createNewTexture = false;
            }
        }
    }

    if( createNewTexture )
    {
        texture = new Texture( m_GraphicsDevice, typeIdx, Texture::e_Class2DTexture, isThisTextureMipMapped );

        texture->SetAnisotropyLevel( 16.0f ); // TODO make this value configurable

        if( it != m_PathsToTexturesMap.end() )
        {
            RefCountedTexturePtrList& ptrList = it->second;
            ptrList.push_front( RefCountedTexturePtr() );
            RefCountedTexturePtr& newPtr = *ptrList.begin();

            newPtr.refCount = 1;
            newPtr.texture = texture;
            newPtr.texturePathIt = texturePathIt;
        }
        else
        {
            m_TexturePathsList.push_front( textureFilePath );
            texturePathIt = m_TexturePathsList.begin();

            PathsToTexturesMap::value_type ptrListElement( textureFilePath, RefCountedTexturePtrList() );
            RefCountedTexturePtrList& ptrList = m_PathsToTexturesMap.insert( ptrListElement ).first->second;
            ptrList.push_front( RefCountedTexturePtr() );
            RefCountedTexturePtr& newPtr = *ptrList.begin();

            newPtr.refCount = 1;
            newPtr.texture = texture;
            newPtr.texturePathIt = texturePathIt;
        }

        loadTexture( textureFilePath, isThisTextureSRGBEncoded, texture );

        texture->SetStateParameter( Texture::e_StateParamSCoordWrapType, sWrapMode );
        texture->SetStateParameter( Texture::e_StateParamTCoordWrapType, tWrapMode );
        texture->SetFilteringMode( m_CurrentTextureFilteringMode );
    }
    else if( isThisTextureMipMapped != texture->IsMipmapped() )
    {
        throw GenericException( "Trying to reuse a texture which has different mipmapping settings than we want" );
    }

    return TextureHandle( texture, texturePathIt );
}

//-----------------------------------------------------------------------

TextureManager::RefCountedTexturePtrList::iterator TextureManager::_FindRefCountedTexturePtr(RefCountedTexturePtrList& ptrList,
                                                                                             const TextureHandle& handle)
{
    RefCountedTexturePtrList::iterator retVal = ptrList.end();

    RefCountedTexturePtrList::iterator ptrListIt = ptrList.begin();
    bool done = false;
    while( ( ptrListIt != ptrList.end() ) && !done )
    {
        RefCountedTexturePtr& refCountedTexPtr = *ptrListIt;
        if( handle.m_Texture == refCountedTexPtr.texture )
        {
            retVal = ptrListIt;
            done = true;
        }
        else
        {
            ++ptrListIt;
        }
    }

    return retVal;
}

//-----------------------------------------------------------------------
