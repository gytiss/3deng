#ifndef VBO_H_INCLUDED
#define VBO_H_INCLUDED

#include <OpenGLIncludes.h>
#include <OpenGLErrorChecking.h>
#include <MathUtils.h>
#include <BitMask.h>

/**
 * OpenGL Vertex Buffer Object Wrapper class
 */

class VBO
{
public:
    friend class VAO;

    typedef BitMask32 VertexAttrMask;

    enum VertexAttrComponentType
    {
        e_VertexAttrComponentTypeFloat32 = 0,
        e_VertexAttrComponentTypeInt32 = 1,
        e_VertexAttrComponentTypeUInt32,
        e_VertexAttrComponentTypeInt16,
        e_VertexAttrComponentTypeUInt16,
        e_VertexAttrComponentTypeInt8,
        e_VertexAttrComponentTypeUInt8,

        e_VertexAttrComponentTypeCount // Not a vertex attribute component type(used for counting)
    };

    enum VertexAttrType
    {
        e_VertexAttrTypeVec2 = 2,
        e_VertexAttrTypeVec3 = 3,
        e_VertexAttrTypeVec4 = 4
    };

    enum VertexAttrIdx ///< Vertex Attribute Index
    {
        e_VertexAttrIdxPosition = 0,
        e_VertexAttrIdxNormal = 1,
        e_VertexAttrIdxBiTangent,
        e_VertexAttrIdxTangent,
        e_VertexAttrIdxTextureCoords,

        // Used for GPU Vertex Skinning
        e_VertexAttrIdxBoneWeights,
        e_VertexAttrIdxBoneIndices,

        e_VertexAttrIdxColor,

        e_VertexAttrIdxCount // Not a vertex attribute index(used for counting)
    };

    enum VertexAttr
    {
        e_VertexAttrPosition = MathUtils::GetBit<e_VertexAttrIdxPosition>::Result,
        e_VertexAttrNormal = MathUtils::GetBit<e_VertexAttrIdxNormal>::Result,
        e_VertexAttrBiTangent = MathUtils::GetBit<e_VertexAttrIdxBiTangent>::Result,
        e_VertexAttrTangent = MathUtils::GetBit<e_VertexAttrIdxTangent>::Result,
        e_VertexAttrTextureCoords = MathUtils::GetBit<e_VertexAttrIdxTextureCoords>::Result,

        // Used for GPU Vertex Skinning
        e_VertexAttrBoneWeights = MathUtils::GetBit<e_VertexAttrIdxBoneWeights>::Result,
        e_VertexAttrBoneIndices = MathUtils::GetBit<e_VertexAttrIdxBoneIndices>::Result,

        e_VertexAttrColor = MathUtils::GetBit<e_VertexAttrIdxColor>::Result
    };

    enum DataBufferUsagePattern
    {
        e_DataBufferUsagePatternStatic = 0,
        e_DataBufferUsagePatternDynamic = 1
    };

    VBO(size_t vertexStructSize, DataBufferUsagePattern usagePatter = e_DataBufferUsagePatternStatic);
    ~VBO();

    void BindData(size_t sizeInBytes, const void* dataPtr);

    void AddVertexAttribute(VertexAttrIdx attrIdx,
                            VertexAttrComponentType componentType,
                            VertexAttrType attrType,
                            size_t attrOffset);

    inline VertexAttrMask GetAtributes() const
    {
        return m_VertexAttributes;
    }

    inline size_t GetVertexSize() const
    {
        return m_VertexSize;
    }

    inline size_t GetAttributeOffset(VertexAttrIdx attrIdx) const
    {
        return m_AttributeDescriptions[attrIdx].offsetInVertexStruct;
    }

    inline VertexAttrComponentType GetAttributeComponentType(VertexAttrIdx attrIdx) const
    {
        return m_AttributeDescriptions[attrIdx].componentType;
    }

    inline unsigned int GetAttributeComponentCount(VertexAttrIdx attrIdx) const
    {
        return m_AttributeDescriptions[attrIdx].type;
    }

    static inline const char* GetAttributeShaderVarName(VertexAttrIdx attrIdx)
    {
        return m_AttrIdxToAttrShaderVarNameMap[attrIdx];
    }

    static inline GLenum GetAttributeOpenGLDataType(VertexAttrComponentType type)
    {
        return m_VertexAttributeComponentTypeToOpenGLDataTypeMap[type];
    }

protected:

    inline void Bind() const
    {
        glBindBuffer( GL_ARRAY_BUFFER, m_Id );
        CheckForGLErrors();
    }

    inline void Unbind() const
    {
        glBindBuffer( GL_ARRAY_BUFFER, 0 );
        CheckForGLErrors();
    }

private:
    // Used to prevent copying
    VBO& operator = (const VBO& other);
    VBO(const VBO& other);

    struct AttributeDesc
    {
        VertexAttrType type;
        VertexAttrComponentType componentType;
        size_t offsetInVertexStruct;
    };

    const DataBufferUsagePattern m_DataBufUsagePatter;
    VertexAttrMask m_VertexAttributes;
    GLuint m_Id;
    const size_t m_VertexSize;
    AttributeDesc m_AttributeDescriptions[e_VertexAttrIdxCount];
    static const char* m_AttrIdxToAttrShaderVarNameMap[e_VertexAttrIdxCount];
    static const GLenum m_VertexAttributeComponentTypeToOpenGLDataTypeMap[e_VertexAttrComponentTypeCount];
};

#endif // VBO_H_INCLUDED
