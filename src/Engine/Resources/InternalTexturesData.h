#ifndef INTERNALTEXTURESDATA_H_INCLUDED
#define INTERNALTEXTURESDATA_H_INCLUDED

#include <cstdint>
#include <cstring>

class InternalTexturesData
{
    friend class TextureManager;

protected:
    static const uint8_t m_NullNormalMapDataBuffer[] alignas(4);
    static const uint8_t m_NullSpecularMapDataBuffer[] alignas(4);

    static const size_t m_NullNormalMapDataBufferSizeInBytes;
    static const size_t m_NullSpecularMapDataBufferSizeInBytes;

private:
    InternalTexturesData();
};

#endif // INTERNALTEXTURESDATA_H_INCLUDED
