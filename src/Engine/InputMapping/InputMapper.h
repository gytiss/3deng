/**
 * Main class for handling input mapping
 */

#ifndef INPUTMAPPER_H_INCLUDED
#define INPUTMAPPER_H_INCLUDED

#include "RawInputConstants.h"
#include "RangeConverter.h"
#include "InputConstants.h"

#include <map>
#include <set>
#include <list>
#include <string>
#include <luabind/luabind.hpp>

class FileManager;

namespace InputMapping
{
    typedef char UTF8;
    typedef std::basic_string<UTF8> UTF8String;
	class InputContext;

	// Helper structure
	struct MappedInput
	{
		std::set<Action> actions;
		std::set<State> states;
		std::map<Range, double> ranges;
        UTF8String textInput;

		/**
		 * Check if action exists in the active actions set and consume it if it does
		 *
		 * @param action - action to consume
		 * @return true - if action was found and consumed, false - otherwise
		 */
		inline bool FindAndConsumeAction(Action action)
		{
		    bool retVal = false;

		    if( FindAction( action ) )
            {
                EatAction( action );
                retVal = true;
            }

            return retVal;
        }

		/**
		 * Check if state exists in the active states set and consume it if it does
		 *
		 * @param action - action to consume
		 * @return true - if action was found and consumed, false - otherwise
		 */
		inline bool FindAndConsumeState(State state)
		{
		    bool retVal = false;

		    if( FindState( state ) )
            {
                EatState( state );
                retVal = true;
            }

            return retVal;
        }

		/**
		 * Check if action exists in the active actions set
		 *
		 * @param action - action to consume
		 * @return true - if action was found and consumed, false - otherwise
		 */
        inline bool FindAction(Action action)
        {
            return ( actions.find( action ) != actions.end() );
        }

		/**
		 * Check if state exists in the active states set
		 *
		 * @param action - action to consume
		 * @return true - if action was found and consumed, false - otherwise
		 */
		inline bool FindState(State state)
		{
            return ( states.find( state ) != states.end() );
		}

        inline void EatAction(Action action)
        {
            actions.erase( action );
        }

        inline void EatState(State state)
        {
            states.erase( state );
        }

		inline void FindAndConsumeRange(Range range)
		{
			std::map<Range, double>::iterator iter = ranges.find( range );
			if( iter != ranges.end() )
            {
                ranges.erase( iter );
            }
		}
	};

	typedef void (*InputCallback)(MappedInput& inputs, float frameTimeInSeconds, void* extraData);

	class InputMapper
	{
	public:
		InputMapper(FileManager& fileManager,
		            const std::string& contextsDefCfgFilePath,
                    const std::string& contexctsInputMapCfgFilePath,
                    const std::string& inputRangesCfgFilePath);
		~InputMapper();

        // Raw input interface
		void Clear();
		void SetRawButtonState(RawInputButton button, bool pressed);
		void SetRawAxisValue(RawInputAxis axis, double value);

		/**
		 * Null terminated UTF8 text string
		 */
		void SetNewTextInput(UTF8* newText);

        /**
        * Call all of the input callbacks
        *
        * @param frameTimeInSeconds - Number of seconds it took to render the previous frame
        */
		void Dispatch(float frameTimeInSeconds) const;

        /**
        * Register an input callback
        *
        * @param callback - A function pointer which will be called
        * @param priority - 0 is the highest priority
        * @param extraData - A pointer which will be supplied to the callback
        */
		void AddCallback(InputCallback callback, int priority, void* extraData);

        // Context management interface
		void PushContext(const std::string& name);
		void PopContext();

	private:
		enum RawInputButtonState
		{
		    e_RawInputButtonStateReleased = 0,
		    e_RawInputButtonStatePressed = 1
		};

	    bool _ReadContextsDefinitionsCfgFile(const std::string& filePath, std::string& errMsg);
	    bool _ReadContextsInputMapCfgFile(const std::string& filePath, std::string& errMsg);
	    bool _ReadRawInputButtonsToActionsMap(luabind::iterator& ctxIt,
                                              InputContext& inputCtx,
                                              std::string& errMsg);
	    bool _ReadRawInputButtonsToStatesMap(luabind::iterator& ctxIt,
                                             InputContext& inputCtx,
                                             std::string& errMsg);
	    bool _ReadRawInputAxisToRangesMap(luabind::iterator& ctxIt,
                                          InputContext& inputCtx,
                                          std::string& errMsg);

		bool _MapButtonToAction(RawInputButton button, Action& action) const;
		bool _MapButtonToState(RawInputButton button, State& state) const;
		void _MapAndEatButton(RawInputButton button);

		FileManager& m_FileManager;
		std::map<std::string, InputContext*> m_InputContexts;
		std::list<InputContext*> m_ActiveContexts;
		std::multimap<int, std::pair<InputCallback, void*> > m_CallbackTable;
		MappedInput m_CurrentMappedInput;

		RawInputButtonState m_RawInputButtonStates[e_RawInputButtonCount];

		const RangeConverter m_Conversions;
	};
}

#endif // INPUTMAPPER_H_INCLUDED
