#include "Device.h"
#include "OpenALIncludes.h"
#include <Exceptions/GenericException.h>

namespace AudioDevice
{

//-----------------------------------------------------------------------

Device::Device()
: m_ALDevice( nullptr )
{
    m_ALDevice = alcOpenDevice( nullptr ); // "nullptr" means select the default device

    if( m_ALDevice == nullptr )
    {
        throw GenericException( "Failed to open OpenAL device" );
    }

    const ALCboolean effectsExtensionSupported = alcIsExtensionPresent( m_ALDevice, "ALC_EXT_EFX" );
    const ALCboolean dedicatedSpeakersExtensionSupported = alcIsExtensionPresent( m_ALDevice, "ALC_EXT_DEDICATED" );

    if( effectsExtensionSupported == AL_FALSE )
    {
        throw GenericException( "System OpenAL implementation does not support the \"ALC_EXT_EFX\" extension" );
    }
    else if( dedicatedSpeakersExtensionSupported == AL_FALSE )
    {
        throw GenericException( "System OpenAL implementation does not support the \"ALC_EXT_DEDICATED\" extension" );
    }
}

//-----------------------------------------------------------------------

Device::~Device()
{
    if( m_ALDevice )
    {
        if( alcCloseDevice( m_ALDevice ) == ALC_FALSE )
        {
            //Throwing an exception from a destructor invoked by a standard library component is undefined beheviour
            //throw GenericException( "Failed to close OpenAL device" );
        }
    }
}

//-----------------------------------------------------------------------

}
