#include "GUIContext.h"
#include <MsgLogger.h>
#include <Exceptions/GenericException.h>
#include <CommonDefinitions.h>
#include <cassert>

//-----------------------------------------------------------------------

GUIContext::GUIContext(const std::string& name,
                       const Rocket::Core::Vector2i& windowDimensions)
: m_EventManager( this ),
  m_EventInstancer( &m_EventManager ),
  m_libRocketContext()
{
    m_libRocketContext = Rocket::Core::CreateContext( name.c_str(), windowDimensions );
    if( !m_libRocketContext )
    {
        throw GenericException( "Failed to create new libRocket context with the name \"" + name + "\"" );
    }
}

//-----------------------------------------------------------------------

GUIEventInstancer* GUIContext::GetEventInstancer()
{
    return &m_EventInstancer;
}

//-----------------------------------------------------------------------

Rocket::Core::ElementDocument* GUIContext::LoadDocument(const Rocket::Core::String& documentPath)
{
	// Attempt to load the referenced RML document.
	Rocket::Core::ElementDocument* retVal = m_libRocketContext->LoadDocument( documentPath.CString() );
	if( retVal != nullptr )
	{
        // Set the element's title on the title; IDd 'title' in the RML.
        Rocket::Core::Element* title = retVal->GetElementById( "title" );
        if( title != nullptr )
        {
            title->SetInnerRML( retVal->GetTitle() );
        }

        // Remove the caller's reference.
        retVal->RemoveReference();
	}
	else
    {
        MsgLogger::LogError("libRocket: Failed to load document \"" + std::string( documentPath.CString() ) + "\"" );
    }

	return retVal;
}

//-----------------------------------------------------------------------
