#ifndef ENGINETYPES_H_INCLUDED
#define ENGINETYPES_H_INCLUDED

#include <glm/glm.hpp>
#include <algorithm>
#include <vector>

namespace EngineTypes
{
#if defined(OPENGL_ES)
    typedef uint16_t VertexIndexType;
#else
    typedef uint32_t VertexIndexType;
#endif

    struct VPTransformations
    {
        glm::mat4 viewMatrix;
        glm::mat4 projectionMatrix;
    };

    /**
     * Axis aligned bounding box
     */
    struct AABB
    {
        glm::vec3 center;

       /**
        * For example if there is an AABB with its centre at [x = 1, y = 2, z = 3] and the box has
        * overall dimensions [10, 8, 12]. If we convert these dimensions to distances from the
        * centre, we get [5, 4, 6]. These are usually called the half-extents or just extents.
        */
        glm::vec3 halfExtents;
    };

    typedef std::vector<glm::mat4> MatrixArray;

    static const float c_DegaultGammaValue = 2.2f;

    class SRGBColor
    {
    public:
        SRGBColor()
        : SRGBColor( glm::vec3( 1.0f, 0.0f, 1.0f ) ) // Bright pink value
        {
        }

        SRGBColor(float r, float g, float b)
        : SRGBColor( glm::vec3( r, g, b ) )
        {
        }

        /**
        * @param srgbColorValue - sRGB color with R, G and B values in the range of 0.0 to 1.0
        */
        SRGBColor(const glm::vec3& srgbColorValue)
        : m_SRGBColorValue( srgbColorValue ),
          m_LinearColorValue( _SRGBColorToLinearColor( m_SRGBColorValue ) )
        {
        }

        /**
        * @param srgbColorValue - sRGB color with R, G and B values in the range of 0.0 to 1.0
        */
        inline void SetSRGBValue(const glm::vec3& srgbColorValue)
        {
            m_SRGBColorValue = srgbColorValue;
            m_LinearColorValue = _SRGBColorToLinearColor( m_SRGBColorValue );
        }

        /**
        * @return sRGB(gamma 2.2) color with R, G and B values in the range of 0.0 to 1.0
        */
        inline const glm::vec3& SRGBValue() const
        {
            return m_SRGBColorValue;
        }

        /**
        * @return RGB(gamma 1.0) color with R, G and B values in the range of 0.0 to 1.0
        */
        inline const glm::vec3& LinearValue() const
        {
            return m_LinearColorValue;
        }

        SRGBColor& operator= (const SRGBColor& other)
        {
            m_SRGBColorValue = other.m_SRGBColorValue;
            m_LinearColorValue = other.m_LinearColorValue;

            return *this;
        }

        SRGBColor(const SRGBColor& other)
        {
            operator=( other );
        }

    private:
        float _SRGBColorChannelToLinearColorChannel(float srgbColorChannelValue) const
        {
            //////////////////////////////////////
            // The 100% correct way to do it
            //////////////////////////////////////
            srgbColorChannelValue = glm::clamp( srgbColorChannelValue, 0.0f, 1.0f );
            float retVal = srgbColorChannelValue;

            if( ( srgbColorChannelValue > 0.0f ) && ( srgbColorChannelValue < 1.0f ) )
            {
                if( srgbColorChannelValue <= 0.04045f )
                {
                    retVal = ( srgbColorChannelValue / 12.92f );
                }
                else
                {
                    retVal = glm::pow( ( ( srgbColorChannelValue + 0.0555f ) / 1.055f ), 2.4f );
                }
            }
            //////////////////////////////////////

            //////////////////////////////////////
            // Approximation with very close to 100%
            // the same value as the value produced
            // by the above code
            //////////////////////////////////////
            //retVal = glm::pow( glm::clamp( srgbColorChannelValue, 0.0f, 1.0f ), 2.2f );
            //////////////////////////////////////

            return retVal;
        }

        float _LinearColorChannelToSRGBColorChannel(float linearColorChannelValue) const
        {
            //////////////////////////////////////
            // The 100% correct way to do it
            //////////////////////////////////////
            linearColorChannelValue = glm::clamp( linearColorChannelValue, 0.0f, 1.0f );
            float retVal = linearColorChannelValue;

            if( ( linearColorChannelValue > 0.0f ) && ( linearColorChannelValue < 1.0f ) )
            {
                if( linearColorChannelValue < 0.0031308f )
                {
                    retVal = ( linearColorChannelValue * 12.92f );
                }
                else
                {
                    retVal = ( 1.055f * pow( linearColorChannelValue, 0.41666f ) ) - 0.055f;
                }
            }
            //////////////////////////////////////

            //////////////////////////////////////
            // Approximation with very close to 100%
            // the same value as the value produced
            // by the above code
            //////////////////////////////////////
            //retVal = glm::pow( glm::clamp( linearColorChannelValue, 0.0f, 1.0f ), ( 1.0f / 2.2f ) );
            //////////////////////////////////////

            return retVal;
        }

        glm::vec3 _SRGBColorToLinearColor(const glm::vec3& srgbColorValue) const
        {
            const glm::vec3 linearColorValue = glm::vec3( _SRGBColorChannelToLinearColorChannel( srgbColorValue[0] ),
                                                          _SRGBColorChannelToLinearColorChannel( srgbColorValue[1] ),
                                                          _SRGBColorChannelToLinearColorChannel( srgbColorValue[2] ) );
            return glm::clamp( linearColorValue, 0.0f, 1.0f );
        }

        glm::vec3 m_SRGBColorValue;
        glm::vec3 m_LinearColorValue;
    };
}

#endif // ENGINETYPES_H_INCLUDED
