# Copyright 2003, 2005 Douglas Gregor
# Copyright 2004 John Maddock
# Copyright 2002, 2003, 2004, 2007 Vladimir Prus
# Distributed under the Boost Software License, Version 1.0.
# (See accompanying file LICENSE_1_0.txt or http://www.boost.org/LICENSE_1_0.txt)

#   This file is used to configure your Boost.Build installation. You can modify
# this file in place, or you can place it in a permanent location so that it
# does not get overwritten should you get a new version of Boost.Build. See:
#
#   http://www.boost.org/boost-build2/doc/html/bbv2/overview/configuration.html
#
# for documentation about possible permanent locations.

#   This file specifies which toolsets (C++ compilers), libraries, and other
# tools are available. Often, you should be able to just uncomment existing
# example lines and adjust them to taste. The complete list of supported tools,
# and configuration instructions can be found at:
#
#   http://boost.org/boost-build2/doc/html/bbv2/reference/tools.html
#

#   This file uses Jam language syntax to describe available tools. Mostly,
# there are 'using' lines, that contain the name of the used tools, and
# parameters to pass to those tools -- where paremeters are separated by
# semicolons. Important syntax notes:
#
#   - Both ':' and ';' must be separated from other tokens by whitespace
#   - The '\' symbol is a quote character, so when specifying Windows paths you
#     should use '/' or '\\' instead.
#
# More details about the syntax can be found at:
#
#   http://boost.org/boost-build2/doc/html/bbv2/advanced.html#bbv2.advanced.jam_language
#
# ------------------
# Android configurations.
# ------------------

import os ;
local AndroidNDKRoot = [ os.environ AndroidNDKRoot ] ;

# --------------------------------------------------------------------
using gcc : armv7
:
arm-linux-androideabi-g++
:

<archiver>arm-linux-androideabi-ar

<compileflags>-I$(AndroidNDKRoot)/platforms/android-18/arch-arm/usr/include
<compileflags>-Wa,--noexecstack
<compileflags>-DANDROID
<compileflags>-D__ANDROID__
<compileflags>-fPIC 
<compileflags>-fexceptions 
<compileflags>-funwind-tables 
<compileflags>-frtti 
<compileflags>-D__BIONIC__ 
<compileflags>-mfloat-abi=softfp 
<compileflags>-mfpu=vfpv3-d16 
<compileflags>-mthumb 
<compileflags>-marm 
<compileflags>-march=armv7-a 
<compileflags>-DNDEBUG 
<compileflags>-fomit-frame-pointer 
<compileflags>-fstrict-aliasing 
<compileflags>-funswitch-loops 
<compileflags>-finline-limit=300 
<compileflags>-ffunction-sections 
<compileflags>-funwind-tables 
<compileflags>-fstack-protector 
<compileflags>-no-canonical-prefixes
<compileflags>-I$(AndroidNDKRoot)/sources/cxx-stl/gnu-libstdc++/4.8/include
<compileflags>-I$(AndroidNDKRoot)/sources/cxx-stl/gnu-libstdc++/4.8/libs/armeabi-v7a/include
# @Moss - Above are the 'oficial' android flags
<architecture>arm
<compileflags>-fvisibility=hidden
<compileflags>-fvisibility-inlines-hidden
<compileflags>-fdata-sections
<linkflags>-march=armv7-a
<linkflags>-Wl,--fix-cortex-a8
<linkflags>-no-canonical-prefixes
<cxxflags>-D__arm__
<cxxflags>-D_REENTRANT
<cxxflags>-D_GLIBCXX__PTHREADS
;

# --------------------------------------------------------------------
using gcc : arm64
:
aarch64-linux-android-g++
:

<archiver>aarch64-linux-android-ar

<compileflags>-I$(AndroidNDKRoot)/platforms/android-21/arch-arm/usr/include
<compileflags>-Wa,--noexecstack
<compileflags>-DANDROID
<compileflags>-D__ANDROID__
<compileflags>-fPIC 
<compileflags>-fexceptions 
<compileflags>-frtti 
<compileflags>-D__BIONIC__ 
<compileflags>-O2 
<compileflags>-march=armv8-a 
<compileflags>-mfix-cortex-a53-835769 
<compileflags>-DNDEBUG 
<compileflags>-fomit-frame-pointer 
<compileflags>-fstrict-aliasing 
<compileflags>-funswitch-loops 
<compileflags>-finline-limit=300 
<compileflags>-ffunction-sections 
<compileflags>-funwind-tables 
<compileflags>-fstack-protector 
<compileflags>-no-canonical-prefixes
<compileflags>-I$(AndroidNDKRoot)/sources/cxx-stl/gnu-libstdc++/4.9/include
<compileflags>-I$(AndroidNDKRoot)/sources/cxx-stl/gnu-libstdc++/4.9/libs/arm64-v8a/include
# @Moss - Above are the 'oficial' android flags
<architecture>arm
<compileflags>-fvisibility=hidden
<compileflags>-fvisibility-inlines-hidden
<compileflags>-fdata-sections
<linkflags>-no-canonical-prefixes
<cxxflags>-D__arm__
<cxxflags>-D_REENTRANT
<cxxflags>-D_GLIBCXX__PTHREADS
;

# --------------------------------------------------------------------
using gcc : mips
:
mipsel-linux-android-g++
:

<archiver>mipsel-linux-android-ar

<compileflags>-I$(AndroidNDKRoot)/platforms/android-18/arch-arm/usr/include
<compileflags>-Wa,--noexecstack
<compileflags>-DANDROID
<compileflags>-D__ANDROID__
<compileflags>-fPIC
<compileflags>-fexceptions
<compileflags>-funwind-tables
<compileflags>-frtti
<compileflags>-D__BIONIC__
<compileflags>-O2
<compileflags>-DNDEBUG
<compileflags>-fomit-frame-pointer
<compileflags>-funswitch-loops
<compileflags>-finline-limit=300
<compileflags>-fno-strict-aliasing
<compileflags>-finline-functions
<compileflags>-ffunction-sections
<compileflags>-funwind-tables
<compileflags>-fmessage-length=0
<compileflags>-fno-inline-functions-called-once
<compileflags>-fgcse-after-reload
<compileflags>-frerun-cse-after-loop
<compileflags>-frename-registers
<compileflags>-no-canonical-prefixes
<compileflags>-I$(AndroidNDKRoot)/sources/cxx-stl/gnu-libstdc++/4.8/include
<compileflags>-I$(AndroidNDKRoot)/sources/cxx-stl/gnu-libstdc++/4.8/libs/mips/include
# @Moss - Above are the 'oficial' android flags
<architecture>mips
<compileflags>-fvisibility=hidden
<compileflags>-fvisibility-inlines-hidden
<compileflags>-fdata-sections
<linkflags>-no-canonical-prefixes
<cxxflags>-D_REENTRANT
<cxxflags>-D_GLIBCXX__PTHREADS
;

# --------------------------------------------------------------------
using gcc : mips64
:
mips64el-linux-android-g++
:

<archiver>mips64el-linux-android-ar

<compileflags>-I$(AndroidNDKRoot)/platforms/android-21/arch-arm/usr/include
<compileflags>-Wa,--noexecstack
<compileflags>-DANDROID
<compileflags>-D__ANDROID__
<compileflags>-fPIC
<compileflags>-fexceptions
<compileflags>-funwind-tables
<compileflags>-frtti 
<compileflags>-D__BIONIC__
<compileflags>-O2
<compileflags>-fno-strict-aliasing
<compileflags>-finline-functions
<compileflags>-ffunction-sections
<compileflags>-funwind-tables
<compileflags>-fmessage-length=0
<compileflags>-fno-inline-functions-called-once
<compileflags>-fgcse-after-reload
<compileflags>-frerun-cse-after-loop
<compileflags>-frename-registers
<compileflags>-no-canonical-prefixes
<compileflags>-fomit-frame-pointer
<compileflags>-funswitch-loops
<compileflags>-finline-limit=300
<compileflags>-I$(AndroidNDKRoot)/sources/cxx-stl/gnu-libstdc++/4.9/include
<compileflags>-I$(AndroidNDKRoot)/sources/cxx-stl/gnu-libstdc++/4.9/libs/mips64/include
# @Moss - Above are the 'oficial' android flags
<architecture>mips64
<compileflags>-fvisibility=hidden
<compileflags>-fvisibility-inlines-hidden
<compileflags>-fdata-sections
<linkflags>-no-canonical-prefixes
<cxxflags>-D_REENTRANT
<cxxflags>-D_GLIBCXX__PTHREADS
;

# --------------------------------------------------------------------
using gcc : x86
:
i686-linux-android-g++
:

<archiver>i686-linux-android-ar

<compileflags>-I$(AndroidNDKRoot)/platforms/android-18/arch-arm/usr/include
<compileflags>-Wa,--noexecstack
<compileflags>-DANDROID
<compileflags>-D__ANDROID__
<compileflags>-fPIC 
<compileflags>-fexceptions 
<compileflags>-funwind-tables 
<compileflags>-frtti -D__BIONIC__ 
<compileflags>-O2 
<compileflags>-ffunction-sections 
<compileflags>-funwind-tables 
<compileflags>-no-canonical-prefixes 
<compileflags>-fstack-protector 
<compileflags>-DNDEBUG 
<compileflags>-fomit-frame-pointer 
<compileflags>-fstrict-aliasing 
<compileflags>-funswitch-loops 
<compileflags>-finline-limit=300
<compileflags>-I$(AndroidNDKRoot)/sources/cxx-stl/gnu-libstdc++/4.8/include
<compileflags>-I$(AndroidNDKRoot)/sources/cxx-stl/gnu-libstdc++/4.8/libs/x86/include
# @Moss - Above are the 'oficial' android flags
<architecture>x86
<compileflags>-fvisibility=hidden
<compileflags>-fvisibility-inlines-hidden
<compileflags>-fdata-sections
<linkflags>-no-canonical-prefixes
<cxxflags>-D_REENTRANT
<cxxflags>-D_GLIBCXX__PTHREADS
;

# --------------------------------------------------------------------
using gcc : x86_64
:
x86_64-linux-android-g++
:

<archiver>x86_64-linux-android-ar

<compileflags>-I$(AndroidNDKRoot)/platforms/android-21/arch-arm/usr/include
<compileflags>-Wa,--noexecstack
<compileflags>-DANDROID
<compileflags>-D__ANDROID__
<compileflags>-fPIC 
<compileflags>-fexceptions
<compileflags>-funwind-tables
<compileflags>-frtti
<compileflags>-D__BIONIC__
<compileflags>-O2
<compileflags>-ffunction-sections
<compileflags>-funwind-tables
<compileflags>-fstack-protector
<compileflags>-no-canonical-prefixes
<compileflags>-DNDEBUG
<compileflags>-fomit-frame-pointer-fstrict-aliasing
<compileflags>-funswitch-loops
<compileflags>-finline-limit=300
<compileflags>-I$(AndroidNDKRoot)/sources/cxx-stl/gnu-libstdc++/4.9/include
<compileflags>-I$(AndroidNDKRoot)/sources/cxx-stl/gnu-libstdc++/4.9/libs/x86_64/include
# @Moss - Above are the 'oficial' android flags
<architecture>x86_64
<compileflags>-fvisibility=hidden
<compileflags>-fvisibility-inlines-hidden
<compileflags>-fdata-sections
<linkflags>-no-canonical-prefixes
<cxxflags>-D_REENTRANT
<cxxflags>-D_GLIBCXX__PTHREADS
;


# ------------------
# GCC configuration.
# ------------------

# Configure gcc (default version).
# using gcc ;

# Configure specific gcc version, giving alternative name to use.
# using gcc : 3.2 : g++-3.2 ;


# -------------------
# MSVC configuration.
# -------------------

# Configure msvc (default version, searched for in standard locations and PATH).
# using msvc ;

# Configure specific msvc version (searched for in standard locations and PATH).
# using msvc : 8.0 ;


# ----------------------
# Borland configuration.
# ----------------------
# using borland ;


# ----------------------
# STLPort configuration.
# ----------------------

#   Configure specifying location of STLPort headers. Libraries must be either
# not needed or available to the compiler by default.
# using stlport : : /usr/include/stlport ;

# Configure specifying location of both headers and libraries explicitly.
# using stlport : : /usr/include/stlport /usr/lib ;


# -----------------
# QT configuration.
# -----------------

# Configure assuming QTDIR gives the installation prefix.
# using qt ;

# Configure with an explicit installation prefix.
# using qt : /usr/opt/qt ;

# ---------------------
# Python configuration.
# ---------------------

# Configure specific Python version.
# using python : 3.1 : /usr/bin/python3 : /usr/include/python3.1 : /usr/lib ;
