#include "RigidBody.h"
#include "GameObject.h"
#include "Collider.h"

#include <MathUtils.h>


//-----------------------------------------------------------------------

RigidBody::RigidBody(GameObject& owningGameObject,
                     bool isKinematic,
                     float mass,
                     PhysicsManager& physicsManager)
: Component( m_ComponentType, "RigidBody", owningGameObject ),
  m_PhysicsManager( physicsManager ),
  m_RigidBodyHandle(),
  m_CurrentGlobalTransform( MathUtils::MatrixToSceneTransform( owningGameObject.GetGlobalTransformMatrix() ) )
{
    m_RigidBodyHandle = m_PhysicsManager.CreateRigidBody( isKinematic, mass, this );
    m_PhysicsManager.SetRigidBodyUserPointer( m_RigidBodyHandle, &( GetOwningGameObject() ) );

    GetOwningGameObject().SetComponent( this ); // Attach this rigid body to the provided game object
    _FindAndAttachActiveColliders();
    m_PhysicsManager.AddRigidBodyToTheWorld( m_RigidBodyHandle );
}

//-----------------------------------------------------------------------

RigidBody::~RigidBody()
{
    SetEnabled( false );
    m_PhysicsManager.DestroyRigidBody( m_RigidBodyHandle );
}

//-----------------------------------------------------------------------

void RigidBody::SetEnabled(bool val)
{
    const bool wasEnabled = IsEnabled();
    Component::SetEnabled( val );

    if( val && !wasEnabled )
    {
		_FindAndAttachActiveColliders();
        m_PhysicsManager.AddRigidBodyToTheWorld( m_RigidBodyHandle );
    }
    else if( wasEnabled )
    {
        m_PhysicsManager.RemoveRigidBodyFromTheWorld( m_RigidBodyHandle );
        _FindAndDetachActiveColliders();
    }
}

//-----------------------------------------------------------------------

bool RigidBody::IsKinematic()
{
    return m_PhysicsManager.IsRigidBodyKinematic( m_RigidBodyHandle );
}

//-----------------------------------------------------------------------

void RigidBody::SetIsKinematic(bool val)
{
    if( val )
    {
        m_PhysicsManager.SwitchRigidBodyToKinematicMode( m_RigidBodyHandle );
    }
    else
    {
        m_PhysicsManager.SwitchRigidBodyToDynamicMode( m_RigidBodyHandle );
    }
}

//-----------------------------------------------------------------------

void RigidBody::SetMass(float mass)
{
    m_PhysicsManager.SetRigidBodyMass( m_RigidBodyHandle, mass );
}

//-----------------------------------------------------------------------

float RigidBody::GetMass() const
{
    return m_PhysicsManager.GetRigidBodyMass( m_RigidBodyHandle );
}

//-----------------------------------------------------------------------

void RigidBody::SetDrag(float drag)
{
    m_PhysicsManager.SetRigidBodyLinearDrag( m_RigidBodyHandle, drag );
}

//-----------------------------------------------------------------------

float RigidBody::GetDrag() const
{
    return m_PhysicsManager.GetRigidBodyLinearDrag( m_RigidBodyHandle );
}

//-----------------------------------------------------------------------

void RigidBody::SetAngularDrag(float drag)
{
    m_PhysicsManager.SetRigidBodyAngularDrag( m_RigidBodyHandle, drag );
}

//-----------------------------------------------------------------------

float RigidBody::GetAngularDrag() const
{
    return m_PhysicsManager.GetRigidBodyAngularDrag( m_RigidBodyHandle );
}

//-----------------------------------------------------------------------

void RigidBody::SetPositionFrozenX(bool frozen)
{
    bool freezeAxis[3] = { false };
    m_PhysicsManager.GetRigidBodyPositionAxisFrozen( m_RigidBodyHandle, freezeAxis );
    freezeAxis[0] = frozen;

    m_PhysicsManager.SetRigidBodyPositionAxisFrozen( m_RigidBodyHandle, freezeAxis );
}

//-----------------------------------------------------------------------

bool RigidBody::GetPositionFrozenX() const
{
    bool freezeAxis[3] = { false };
    m_PhysicsManager.GetRigidBodyPositionAxisFrozen( m_RigidBodyHandle, freezeAxis );
    return freezeAxis[0];
}

//-----------------------------------------------------------------------

void RigidBody::SetPositionFrozenY(bool frozen)
{
    bool freezeAxis[3] = { false };
    m_PhysicsManager.GetRigidBodyPositionAxisFrozen( m_RigidBodyHandle, freezeAxis );
    freezeAxis[1] = frozen;

    m_PhysicsManager.SetRigidBodyPositionAxisFrozen( m_RigidBodyHandle, freezeAxis );
}

//-----------------------------------------------------------------------

bool RigidBody::GetPositionFrozenY() const
{
    bool freezeAxis[3] = { false };
    m_PhysicsManager.GetRigidBodyPositionAxisFrozen( m_RigidBodyHandle, freezeAxis );
    return freezeAxis[1];
}

//-----------------------------------------------------------------------

void RigidBody::SetPositionFrozenZ(bool frozen)
{
    bool freezeAxis[3] = { false };
    m_PhysicsManager.GetRigidBodyPositionAxisFrozen( m_RigidBodyHandle, freezeAxis );
    freezeAxis[2] = frozen;

    m_PhysicsManager.SetRigidBodyPositionAxisFrozen( m_RigidBodyHandle, freezeAxis );
}

//-----------------------------------------------------------------------

bool RigidBody::GetPositionFrozenZ() const
{
    bool freezeAxis[3] = { false };
    m_PhysicsManager.GetRigidBodyPositionAxisFrozen( m_RigidBodyHandle, freezeAxis );
    return freezeAxis[2];
}

//-----------------------------------------------------------------------

void RigidBody::SetRotationFrozenX(bool frozen)
{
    bool freezeAxis[3] = { false };
    m_PhysicsManager.GetRigidBodyRotationAxisFrozen( m_RigidBodyHandle, freezeAxis );
    freezeAxis[0] = frozen;

    m_PhysicsManager.SetRigidBodyRotationAxisFrozen( m_RigidBodyHandle, freezeAxis );
}

//-----------------------------------------------------------------------

bool RigidBody::GetRotationFrozenX() const
{
    bool freezeAxis[3] = { false };
    m_PhysicsManager.GetRigidBodyRotationAxisFrozen( m_RigidBodyHandle, freezeAxis );
    return freezeAxis[0];
}

//-----------------------------------------------------------------------

void RigidBody::SetRotationFrozenY(bool frozen)
{
    bool freezeAxis[3] = { false };
    m_PhysicsManager.GetRigidBodyRotationAxisFrozen( m_RigidBodyHandle, freezeAxis );
    freezeAxis[1] = frozen;

    m_PhysicsManager.SetRigidBodyRotationAxisFrozen( m_RigidBodyHandle, freezeAxis );
}

//-----------------------------------------------------------------------

bool RigidBody::GetRotationFrozenY() const
{
    bool freezeAxis[3] = { false };
    m_PhysicsManager.GetRigidBodyRotationAxisFrozen( m_RigidBodyHandle, freezeAxis );
    return freezeAxis[1];
}

//-----------------------------------------------------------------------

void RigidBody::SetRotationFrozenZ(bool frozen)
{
    bool freezeAxis[3] = { false };
    m_PhysicsManager.GetRigidBodyRotationAxisFrozen( m_RigidBodyHandle, freezeAxis );
    freezeAxis[2] = frozen;

    m_PhysicsManager.SetRigidBodyRotationAxisFrozen( m_RigidBodyHandle, freezeAxis );
}

//-----------------------------------------------------------------------

bool RigidBody::GetRotationFrozenZ() const
{
    bool freezeAxis[3] = { false };
    m_PhysicsManager.GetRigidBodyRotationAxisFrozen( m_RigidBodyHandle, freezeAxis );
    return freezeAxis[2];
}

//-----------------------------------------------------------------------

void RigidBody::SetSceneTransform(const SceneTypes::Transform& t)
{
    m_CurrentGlobalTransform.position = t.position;
    m_CurrentGlobalTransform.rotation = t.rotation;
    GetOwningGameObject()._SetGlobalTransformWithoutUptadingPhysicsPropertyTransform( m_CurrentGlobalTransform );
}

//-----------------------------------------------------------------------

void RigidBody::GetSceneTransform(SceneTypes::Transform& t) const
{
    t = m_CurrentGlobalTransform;
}

//-----------------------------------------------------------------------

glm::vec3 RigidBody::_GetCurrentSceneGraphNodeWorldPosition()
{
    return MathUtils::MatrixToTranslation( GetOwningGameObject().GetGlobalTransformMatrix() );
}

//-----------------------------------------------------------------------

void RigidBody::_FindAndAttachActiveColliders()
{
    GameObject& owningGameObject = GetOwningGameObject();
    std::vector<Collider*> colliders;
    owningGameObject._FindColliders( colliders );

    if( colliders.size() > 0 )
    {
        PhysicsManager::ColliderHandleList colliderHandles;
        for(size_t i = 0; i < colliders.size(); i++)
        {
            Collider* collider = colliders[i];

            if( collider->IsEnabled() && collider->IsInitialised() )
            {
                collider->_RemoveFromThePhysicsWorld();
                colliderHandles.push_back( collider->_GetColliderHandle() );
            }
        }

        m_PhysicsManager.AttachCollidersToARigidBody( m_RigidBodyHandle, colliderHandles );
    }
}

//-----------------------------------------------------------------------

void RigidBody::_FindAndDetachActiveColliders()
{
    GameObject& owningGameObject = GetOwningGameObject();
    std::vector<Collider*> colliders;
    owningGameObject._FindColliders( colliders );

    m_PhysicsManager.DetachCollidersFromARigidBody( m_RigidBodyHandle );

    for(size_t i = 0; i < colliders.size(); i++)
    {
        Collider* collider = colliders[i];

        if( collider->IsEnabled() && collider->IsInitialised() )
        {
            collider->_AddToThePhysicsWorld();
        }
    }
}

//-----------------------------------------------------------------------
