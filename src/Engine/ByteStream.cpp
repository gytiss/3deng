#include "ByteStream.h"
#include <MathUtils.h>
#include <CommonDefinitions.h>
#include <Resources/FileManager.h>

ByteStream::ByteStream(FileManager& fileManager, DataAlignment requestedAlignment)
: m_FileManager( fileManager ),
  m_BytesReadSoFar( 0 ),
  m_BufferLen( 0 ),
  m_DataBuffer( nullptr ),
  m_AlignedDataBuffer( nullptr ),
  m_RequestedAlignment( requestedAlignment )
{
}

//-----------------------------------------------------------------------

bool ByteStream::InitFromMemory(const uint8_t* dataBuffer, size_t dataBufferSizeInBytes)
{
    bool retVal = false;

    m_BytesReadSoFar = 0;
    m_BufferLen = 0;
    m_DataBuffer.reset( nullptr );
    m_AlignedDataBuffer = nullptr;

    if( dataBuffer && dataBufferSizeInBytes )
    {
        if( m_RequestedAlignment != e_DataAlignmentNo )
        {
            assert( MathUtils::IsAligned( dataBuffer, m_RequestedAlignment ) && "Passed pointer is not correctly aligned" );

            if( MathUtils::IsAligned( dataBuffer, m_RequestedAlignment ) )
            {
                m_AlignedDataBuffer = dataBuffer;
                m_BufferLen = dataBufferSizeInBytes;
                retVal = true;
            }
        }
        else
        {
            m_AlignedDataBuffer = dataBuffer;
            m_BufferLen = dataBufferSizeInBytes;
            retVal = true;
        }
    }

    return retVal;
}

//-----------------------------------------------------------------------

bool ByteStream::InitFromFile(const std::string& fileName)
{
    bool retVal = false;

    m_BytesReadSoFar = 0;
    m_BufferLen = 0;
    m_DataBuffer.reset( nullptr );
    m_AlignedDataBuffer = nullptr;

    FileManager::File file;

    if( m_FileManager.OpenFile( fileName, FileManager::e_FileTypeBinary, file ) )
    {
        const size_t fileSize = file.Size();
        m_DataBuffer.reset( new uint8_t[fileSize + m_RequestedAlignment] );
        uint8_t* alignedDataBuffer = m_DataBuffer.get();

        if( m_RequestedAlignment != e_DataAlignmentNo )
        {
            alignedDataBuffer = MathUtils::GetNextAlignedAddress( alignedDataBuffer, m_RequestedAlignment );
        }

        if( file.Read( alignedDataBuffer, sizeof( uint8_t ), fileSize ) )
        {
            m_AlignedDataBuffer = alignedDataBuffer;
            m_BufferLen = fileSize;
            retVal = true;
        }
        else
        {
            m_DataBuffer.reset();
            m_AlignedDataBuffer = nullptr;
        }
    }

    return retVal;
}

//-----------------------------------------------------------------------
