#include "RangeConverter.h"

extern "C"
{
    #include <lua.hpp>
}

#include <luabind/luabind.hpp>
#include <luabind/operator.hpp>

#include <Exceptions/GenericException.h>
#include <MsgLogger.h>
#include <LuaUtils.h>
#include <CommonDefinitions.h>

using namespace InputMapping;

const std::string RangeConverter::c_MandatoryRangesTableMembers[4] =
{
    "MinInputVal",
    "MaxInputVal",
    "MinOutputVal",
    "MaxOutputVal"
};

//
// Construct the converter and load the conversion table provided
//
RangeConverter::RangeConverter(const std::string& configFileName, FileManager& fileManager)
{
    std::string mainErrMsg = "Failed to load InputRanges config from file '" + configFileName + "' ";
    bool ok = false;

    LuaUtils::LuaStateWrapper luaState;
    luaL_openlibs( luaState );
    if( LuaUtils::LoadLuaScript( luaState, configFileName, fileManager ) )
    {
        std::string secondaryErrMsg = "with the following error: ";
        ok = true;

        // Connect LuaBind to this lua state
        luabind::open( luaState );

        luabind::object topTable = luabind::globals( luaState )["Ranges"];

        if( !topTable )
        {
            secondaryErrMsg += "Global Lua table 'Ranges' does not exist";
            ok = false;
        }

        if( ok && ( luabind::type( topTable ) == LUA_TTABLE ) )
        {
            for (luabind::iterator i(topTable), end; i != end && ok; ++i)
            {
                if( luabind::type( *i ) == LUA_TTABLE )
                {
                    Range range;
                    if( RangeNameToRange( luabind::tostring_operator( i.key() ), range ) )
                    {
                        Converter converter;
                        for(size_t j = 0; j < ArraySize( c_MandatoryRangesTableMembers ) && ok ; j++)
                        {
                            luabind::object tempVal = luabind::gettable( *i, c_MandatoryRangesTableMembers[j].c_str() );
                            if( tempVal )
                            {
                                if( luabind::type( tempVal ) == LUA_TNUMBER )
                                {
                                    if( c_MandatoryRangesTableMembers[j].compare( "MinInputVal" ) == 0 )
                                    {
                                        converter.MinimumInput = luabind::object_cast<double>( tempVal );
                                    }
                                    else if( c_MandatoryRangesTableMembers[j].compare( "MaxInputVal" ) == 0 )
                                    {
                                        converter.MaximumInput = luabind::object_cast<double>( tempVal );
                                    }
                                    else if( c_MandatoryRangesTableMembers[j].compare( "MinOutputVal" ) == 0 )
                                    {
                                        converter.MinimumOutput = luabind::object_cast<double>( tempVal );
                                    }
                                    else if( c_MandatoryRangesTableMembers[j].compare( "MaxOutputVal" ) == 0 )
                                    {
                                        converter.MaximumOutput = luabind::object_cast<double>( tempVal );
                                    }
                                }
                                else
                                {
                                    secondaryErrMsg += "Value of field '";
                                    secondaryErrMsg += c_MandatoryRangesTableMembers[j];
                                    secondaryErrMsg += "' in table '";
                                    secondaryErrMsg += luabind::tostring_operator( i.key() );
                                    secondaryErrMsg += "' must be a number";
                                    ok = false;
                                }
                            }
                            else
                            {
                                secondaryErrMsg += "Mandatory member '";
                                secondaryErrMsg += c_MandatoryRangesTableMembers[j];
                                secondaryErrMsg += "' of ranges table has not been found in '";
                                secondaryErrMsg += luabind::tostring_operator( i.key() );
                                secondaryErrMsg += "'";
                                ok = false;
                            }
                        }

                        if( ok )
                        {
                            if( converter.MaximumInput < converter.MinimumInput )
                            {
                                secondaryErrMsg += "MaximumInput is lower than MinimumInput for range '";
                                secondaryErrMsg += luabind::tostring_operator( i.key() );
                                secondaryErrMsg += "'";
                                ok = false;
                            }
                            else if( converter.MaximumOutput < converter.MinimumOutput )
                            {
                                secondaryErrMsg += "MaximumOutput is lower than MinimumOutput for range '";
                                secondaryErrMsg += luabind::tostring_operator( i.key() );
                                secondaryErrMsg += "'";
                                ok = false;
                            }
                            else
                            {
                                m_ConversionMap.insert(std::make_pair(range, converter));
                            }
                        }
                    }
                    else
                    {
                        secondaryErrMsg += "Unknown range '";
                        secondaryErrMsg += luabind::tostring_operator( i.key() );
                        secondaryErrMsg += "'";
                        ok = false;
                    }
                }
                else
                {
                    secondaryErrMsg += "'Ranges' table has non table member '";
                    secondaryErrMsg += luabind::tostring_operator( i.key() );
                    secondaryErrMsg += "'";
                    ok = false;
                }
            }
        }
        else if( ok )
        {
            secondaryErrMsg += "Global Lua variable 'Ranges' is not a table";
            ok = false;
        }

        if( !ok )
        {
            mainErrMsg += secondaryErrMsg;
        }
    }

    if( !ok )
    {
        throw GenericException( mainErrMsg );
    }
}


