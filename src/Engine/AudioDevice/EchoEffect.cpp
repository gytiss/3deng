#include "EchoEffect.h"
#include "Context.h"
#include "OpenALErrorChecking.h"
#include <cassert>
#include <algorithm>

namespace AudioDevice
{

//-----------------------------------------------------------------------

EchoEffect::EchoEffect(Context& context, EffectSlot& effectSlot)
: Effect( context, effectSlot, e_TypeEcho )
{

}

//-----------------------------------------------------------------------

void EchoEffect::SetDelay(float seconds)
{
    const float c_MinValue = GetMinDelayValue();
    const float c_MaxValue = GetMaxDelayValue();

    assert( ( seconds >= c_MinValue ) && ( seconds <= c_MaxValue ) );
    seconds = std::max( c_MinValue, std::min( seconds, c_MaxValue ) );

    _GetContext().Bind();

    alEffectf( GetALHandle(), AL_ECHO_DELAY, seconds );
    CheckForALErrors( _GetContext() );

    _SettingsUpdated();
}

//-----------------------------------------------------------------------

void EchoEffect::SetLRDelay(float seconds)
{
    const float c_MinValue = GetMinLRDelayValue();
    const float c_MaxValue = GetMaxLRDelayValue();

    assert( ( seconds >= c_MinValue ) && ( seconds <= c_MaxValue ) );
    seconds = std::max( c_MinValue, std::min( seconds, c_MaxValue ) );

    _GetContext().Bind();

    alEffectf( GetALHandle(), AL_ECHO_LRDELAY, seconds );
    CheckForALErrors( _GetContext() );

    _SettingsUpdated();
}

//-----------------------------------------------------------------------

void EchoEffect::SetDamping(float value)
{
    const float c_MinValue = GetMinDampingValue();
    const float c_MaxValue = GetMaxDampingValue();

    assert( ( value >= c_MinValue ) && ( value <= c_MaxValue ) );
    value = std::max( c_MinValue, std::min( value, c_MaxValue ) );

    _GetContext().Bind();

    alEffectf( GetALHandle(), AL_ECHO_DAMPING, value );
    CheckForALErrors( _GetContext() );

    _SettingsUpdated();
}

//-----------------------------------------------------------------------

void EchoEffect::SetEchoFeedback(float value)
{
    const float c_MinValue = GetMinEchoFeedbackValue();
    const float c_MaxValue = GetMaxEchoFeedbackValue();

    assert( ( value >= c_MinValue ) && ( value <= c_MaxValue ) );
    value = std::max( c_MinValue, std::min( value, c_MaxValue ) );

    _GetContext().Bind();

    alEffectf( GetALHandle(), AL_ECHO_FEEDBACK, value );
    CheckForALErrors( _GetContext() );

    _SettingsUpdated();
}

//-----------------------------------------------------------------------

void EchoEffect::SetEchoSpread(float value)
{
    const float c_MinValue = GetMinEchoSpreadValue();
    const float c_MaxValue = GetMaxEchoSpreadValue();

    assert( ( value >= c_MinValue ) && ( value <= c_MaxValue ) );
    value = std::max( c_MinValue, std::min( value, c_MaxValue ) );

    _GetContext().Bind();

    alEffectf( GetALHandle(), AL_ECHO_SPREAD, value );
    CheckForALErrors( _GetContext() );

    _SettingsUpdated();
}

//-----------------------------------------------------------------------

float EchoEffect::GetDelay()
{
    _GetContext().Bind();

    float delay = 0.0;

    alGetEffectf( GetALHandle(), AL_ECHO_DELAY, &delay );
    CheckForALErrors( _GetContext() );

    return delay;
}

//-----------------------------------------------------------------------

float EchoEffect::GetLRDelay()
{
    _GetContext().Bind();

    float delay = 0.0;

    alGetEffectf( GetALHandle(), AL_ECHO_LRDELAY, &delay );
    CheckForALErrors( _GetContext() );

    return delay;
}

//-----------------------------------------------------------------------

float EchoEffect::GetDamping()
{
    _GetContext().Bind();

    float damping = 0.0;

    alGetEffectf( GetALHandle(), AL_ECHO_DAMPING, &damping );
    CheckForALErrors( _GetContext() );

    return damping;
}

//-----------------------------------------------------------------------

float EchoEffect::GetEchoFeedback()
{
    _GetContext().Bind();

    float feedback = 0.0;

    alGetEffectf( GetALHandle(), AL_ECHO_FEEDBACK, &feedback );
    CheckForALErrors( _GetContext() );

    return feedback;
}

//-----------------------------------------------------------------------

float EchoEffect::GetEchoSpread()
{
    _GetContext().Bind();

    float echoSpread = 0.0;

    alGetEffectf( GetALHandle(), AL_ECHO_SPREAD, &echoSpread );
    CheckForALErrors( _GetContext() );

    return echoSpread;
}

//-----------------------------------------------------------------------

}
