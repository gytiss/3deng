in vec3 in_VertexPos;

void main()
{
    gl_Position = vec4(in_VertexPos, 1.0f);
}
