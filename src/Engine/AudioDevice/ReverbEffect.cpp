#include "ReverbEffect.h"
#include "Context.h"
#include "OpenALErrorChecking.h"
#include <CommonDefinitions.h>
#include <algorithm>
#include <cassert>
#include <AL/efx-presets.h>

namespace AudioDevice
{

//-----------------------------------------------------------------------

// Declared outside the class in order to avoid adding
// the "AL/efx-presets.h" into the header
static const EFXEAXREVERBPROPERTIES c_SettingsPresets[] =
{
    EFX_REVERB_PRESET_GENERIC,
    EFX_REVERB_PRESET_PADDEDCELL,
    EFX_REVERB_PRESET_ROOM,
    EFX_REVERB_PRESET_BATHROOM,
    EFX_REVERB_PRESET_LIVINGROOM,
    EFX_REVERB_PRESET_STONEROOM,
    EFX_REVERB_PRESET_AUDITORIUM,
    EFX_REVERB_PRESET_CONCERTHALL,
    EFX_REVERB_PRESET_CAVE,
    EFX_REVERB_PRESET_ARENA,
    EFX_REVERB_PRESET_HANGAR,
    EFX_REVERB_PRESET_CARPETEDHALLWAY,
    EFX_REVERB_PRESET_HALLWAY,
    EFX_REVERB_PRESET_STONECORRIDOR,
    EFX_REVERB_PRESET_ALLEY,
    EFX_REVERB_PRESET_FOREST,
    EFX_REVERB_PRESET_CITY,
    EFX_REVERB_PRESET_MOUNTAINS,
    EFX_REVERB_PRESET_QUARRY,
    EFX_REVERB_PRESET_PLAIN,
    EFX_REVERB_PRESET_PARKINGLOT,
    EFX_REVERB_PRESET_SEWERPIPE,
    EFX_REVERB_PRESET_UNDERWATER,
    EFX_REVERB_PRESET_DRUGGED,
    EFX_REVERB_PRESET_DIZZY,
    EFX_REVERB_PRESET_PSYCHOTIC,
    EFX_REVERB_PRESET_CASTLE_SMALLROOM,
    EFX_REVERB_PRESET_CASTLE_SHORTPASSAGE,
    EFX_REVERB_PRESET_CASTLE_MEDIUMROOM,
    EFX_REVERB_PRESET_CASTLE_LARGEROOM,
    EFX_REVERB_PRESET_CASTLE_LONGPASSAGE,
    EFX_REVERB_PRESET_CASTLE_HALL,
    EFX_REVERB_PRESET_CASTLE_CUPBOARD,
    EFX_REVERB_PRESET_CASTLE_COURTYARD,
    EFX_REVERB_PRESET_CASTLE_ALCOVE,
    EFX_REVERB_PRESET_FACTORY_SMALLROOM,
    EFX_REVERB_PRESET_FACTORY_SHORTPASSAGE,
    EFX_REVERB_PRESET_FACTORY_MEDIUMROOM,
    EFX_REVERB_PRESET_FACTORY_LARGEROOM,
    EFX_REVERB_PRESET_FACTORY_LONGPASSAGE,
    EFX_REVERB_PRESET_FACTORY_HALL,
    EFX_REVERB_PRESET_FACTORY_CUPBOARD,
    EFX_REVERB_PRESET_FACTORY_COURTYARD,
    EFX_REVERB_PRESET_FACTORY_ALCOVE,
    EFX_REVERB_PRESET_ICEPALACE_SMALLROOM,
    EFX_REVERB_PRESET_ICEPALACE_SHORTPASSAGE,
    EFX_REVERB_PRESET_ICEPALACE_MEDIUMROOM,
    EFX_REVERB_PRESET_ICEPALACE_LARGEROOM,
    EFX_REVERB_PRESET_ICEPALACE_LONGPASSAGE,
    EFX_REVERB_PRESET_ICEPALACE_HALL,
    EFX_REVERB_PRESET_ICEPALACE_CUPBOARD,
    EFX_REVERB_PRESET_ICEPALACE_COURTYARD,
    EFX_REVERB_PRESET_ICEPALACE_ALCOVE ,
    EFX_REVERB_PRESET_SPACESTATION_SMALLROOM,
    EFX_REVERB_PRESET_SPACESTATION_SHORTPASSAGE,
    EFX_REVERB_PRESET_SPACESTATION_MEDIUMROOM,
    EFX_REVERB_PRESET_SPACESTATION_LARGEROOM,
    EFX_REVERB_PRESET_SPACESTATION_LONGPASSAGE,
    EFX_REVERB_PRESET_SPACESTATION_HALL,
    EFX_REVERB_PRESET_SPACESTATION_CUPBOARD,
    EFX_REVERB_PRESET_SPACESTATION_ALCOVE,
    EFX_REVERB_PRESET_WOODEN_SMALLROOM,
    EFX_REVERB_PRESET_WOODEN_SHORTPASSAGE,
    EFX_REVERB_PRESET_WOODEN_MEDIUMROOM,
    EFX_REVERB_PRESET_WOODEN_LARGEROOM,
    EFX_REVERB_PRESET_WOODEN_LONGPASSAGE,
    EFX_REVERB_PRESET_WOODEN_HALL,
    EFX_REVERB_PRESET_WOODEN_CUPBOARD,
    EFX_REVERB_PRESET_WOODEN_COURTYARD,
    EFX_REVERB_PRESET_WOODEN_ALCOVE,
    EFX_REVERB_PRESET_SPORT_EMPTYSTADIUM,
    EFX_REVERB_PRESET_SPORT_SQUASHCOURT,
    EFX_REVERB_PRESET_SPORT_SMALLSWIMMINGPOOL,
    EFX_REVERB_PRESET_SPORT_LARGESWIMMINGPOOL,
    EFX_REVERB_PRESET_SPORT_GYMNASIUM,
    EFX_REVERB_PRESET_SPORT_FULLSTADIUM,
    EFX_REVERB_PRESET_SPORT_STADIUMTANNOY,
    EFX_REVERB_PRESET_PREFAB_WORKSHOP,
    EFX_REVERB_PRESET_PREFAB_SCHOOLROOM,
    EFX_REVERB_PRESET_PREFAB_PRACTISEROOM,
    EFX_REVERB_PRESET_PREFAB_OUTHOUSE,
    EFX_REVERB_PRESET_PREFAB_CARAVAN,
    EFX_REVERB_PRESET_DOME_TOMB,
    EFX_REVERB_PRESET_PIPE_SMALL,
    EFX_REVERB_PRESET_DOME_SAINTPAULS,
    EFX_REVERB_PRESET_PIPE_LONGTHIN,
    EFX_REVERB_PRESET_PIPE_LARGE,
    EFX_REVERB_PRESET_PIPE_RESONANT,
    EFX_REVERB_PRESET_OUTDOORS_BACKYARD,
    EFX_REVERB_PRESET_OUTDOORS_ROLLINGPLAINS,
    EFX_REVERB_PRESET_OUTDOORS_DEEPCANYON,
    EFX_REVERB_PRESET_OUTDOORS_CREEK,
    EFX_REVERB_PRESET_OUTDOORS_VALLEY,
    EFX_REVERB_PRESET_MOOD_HEAVEN,
    EFX_REVERB_PRESET_MOOD_HELL,
    EFX_REVERB_PRESET_MOOD_MEMORY,
    EFX_REVERB_PRESET_DRIVING_COMMENTATOR,
    EFX_REVERB_PRESET_DRIVING_PITGARAGE,
    EFX_REVERB_PRESET_DRIVING_INCAR_RACER,
    EFX_REVERB_PRESET_DRIVING_INCAR_SPORTS,
    EFX_REVERB_PRESET_DRIVING_INCAR_LUXURY,
    EFX_REVERB_PRESET_DRIVING_FULLGRANDSTAND,
    EFX_REVERB_PRESET_DRIVING_EMPTYGRANDSTAND,
    EFX_REVERB_PRESET_DRIVING_TUNNEL,
    EFX_REVERB_PRESET_CITY_STREETS,
    EFX_REVERB_PRESET_CITY_SUBWAY,
    EFX_REVERB_PRESET_CITY_MUSEUM,
    EFX_REVERB_PRESET_CITY_LIBRARY,
    EFX_REVERB_PRESET_CITY_UNDERPASS,
    EFX_REVERB_PRESET_CITY_ABANDONED,
    EFX_REVERB_PRESET_DUSTYROOM,
    EFX_REVERB_PRESET_CHAPEL,
    EFX_REVERB_PRESET_SMALLWATERROOM
};

//-----------------------------------------------------------------------

const ALenum ReverbEffect::m_FloatParamToOpenALEAXReverbFloatParamMap[e_FloatParamCount] =
{
    AL_EAXREVERB_DENSITY,
    AL_EAXREVERB_DIFFUSION,
    AL_EAXREVERB_GAIN,
    AL_EAXREVERB_GAINHF,
    AL_EAXREVERB_GAINLF,
    AL_EAXREVERB_DECAY_TIME,
    AL_EAXREVERB_DECAY_HFRATIO,
    AL_EAXREVERB_DECAY_LFRATIO,
    AL_EAXREVERB_REFLECTIONS_GAIN,
    AL_EAXREVERB_REFLECTIONS_DELAY,
    AL_EAXREVERB_LATE_REVERB_GAIN,
    AL_EAXREVERB_LATE_REVERB_DELAY,
    AL_EAXREVERB_ECHO_TIME,
    AL_EAXREVERB_ECHO_DEPTH,
    AL_EAXREVERB_MODULATION_TIME,
    AL_EAXREVERB_MODULATION_DEPTH,
    AL_EAXREVERB_AIR_ABSORPTION_GAINHF,
    AL_EAXREVERB_HFREFERENCE,
    AL_EAXREVERB_LFREFERENCE,
    AL_EAXREVERB_ROOM_ROLLOFF_FACTOR
};

//-----------------------------------------------------------------------

const ReverbEffect::FloatParamMinMaxValues ReverbEffect::m_FloatParamToOpenALEAXReverbFloatParamMinMaxValuesMap[e_FloatParamCount] =
{
    { AL_EAXREVERB_MIN_DENSITY,               AL_EAXREVERB_MAX_DENSITY },
    { AL_EAXREVERB_MIN_DIFFUSION,             AL_EAXREVERB_MAX_DIFFUSION },
    { AL_EAXREVERB_MIN_GAIN,                  AL_EAXREVERB_MAX_GAIN },
    { AL_EAXREVERB_MIN_GAINHF,                AL_EAXREVERB_MAX_GAINHF },
    { AL_EAXREVERB_MIN_GAINLF,                AL_EAXREVERB_MAX_GAINLF },
    { AL_EAXREVERB_MIN_DECAY_TIME,            AL_EAXREVERB_MAX_DECAY_TIME },
    { AL_EAXREVERB_MIN_DECAY_HFRATIO,         AL_EAXREVERB_MAX_DECAY_HFRATIO },
    { AL_EAXREVERB_MIN_DECAY_LFRATIO,         AL_EAXREVERB_MAX_DECAY_LFRATIO },
    { AL_EAXREVERB_MIN_REFLECTIONS_GAIN,      AL_EAXREVERB_MAX_REFLECTIONS_GAIN },
    { AL_EAXREVERB_MIN_REFLECTIONS_DELAY,     AL_EAXREVERB_MAX_REFLECTIONS_DELAY },
    { AL_EAXREVERB_MIN_LATE_REVERB_GAIN,      AL_EAXREVERB_MAX_LATE_REVERB_GAIN },
    { AL_EAXREVERB_MIN_LATE_REVERB_DELAY,     AL_EAXREVERB_MAX_LATE_REVERB_DELAY },
    { AL_EAXREVERB_MIN_ECHO_TIME,             AL_EAXREVERB_MAX_ECHO_TIME },
    { AL_EAXREVERB_MIN_ECHO_DEPTH,            AL_EAXREVERB_MAX_ECHO_DEPTH },
    { AL_EAXREVERB_MIN_MODULATION_TIME,       AL_EAXREVERB_MAX_MODULATION_TIME },
    { AL_EAXREVERB_MIN_MODULATION_DEPTH,      AL_EAXREVERB_MAX_MODULATION_DEPTH },
    { AL_EAXREVERB_MIN_AIR_ABSORPTION_GAINHF, AL_EAXREVERB_MAX_AIR_ABSORPTION_GAINHF },
    { AL_EAXREVERB_MIN_HFREFERENCE,           AL_EAXREVERB_MAX_HFREFERENCE },
    { AL_EAXREVERB_MIN_LFREFERENCE,           AL_EAXREVERB_MAX_LFREFERENCE },
    { AL_EAXREVERB_MIN_ROOM_ROLLOFF_FACTOR,   AL_EAXREVERB_MAX_ROOM_ROLLOFF_FACTOR }
};

//-----------------------------------------------------------------------

ReverbEffect::ReverbEffect(Context& context, EffectSlot& effectSlot)
: Effect( context, effectSlot, e_TypeEAXReverb )
{
    static_assert( ArraySize( c_SettingsPresets ) == e_PresetCount, "Must be equal" );
}

//-----------------------------------------------------------------------

void ReverbEffect::InitSettingsFromAPreset(Preset preset)
{
    const EFXEAXREVERBPROPERTIES& presetValues = c_SettingsPresets[preset];

    _SetFloatParameter( e_FloatParamDensity, presetValues.flDensity );
    _SetFloatParameter( e_FloatParamDiffusion, presetValues.flDiffusion );
    _SetFloatParameter( e_FloatParamGain, presetValues.flGain );
    _SetFloatParameter( e_FloatParamGainHF, presetValues.flGainHF );
    _SetFloatParameter( e_FloatParamGainLF, presetValues.flGainLF );
    _SetFloatParameter( e_FloatParamDecayTime, presetValues.flDecayTime );
    _SetFloatParameter( e_FloatParamDecayHFRatio, presetValues.flDecayHFRatio );
    _SetFloatParameter( e_FloatParamDecayLFRatio, presetValues.flDecayLFRatio );
    _SetFloatParameter( e_FloatParamReflectionsGain, presetValues.flReflectionsGain );
    _SetFloatParameter( e_FloatParamReflectionsDelay, presetValues.flReflectionsDelay );
    _SetReflectionsPan( glm::vec3( presetValues.flReflectionsPan[0],
                                   presetValues.flReflectionsPan[1],
                                   presetValues.flReflectionsPan[2] ) );
    _SetFloatParameter( e_FloatParamLateReverbGain, presetValues.flLateReverbGain );
    _SetFloatParameter( e_FloatParamLateReverbDelay, presetValues.flLateReverbDelay );
    _SetLateReverbPan( glm::vec3( presetValues.flLateReverbPan[0],
                                  presetValues.flLateReverbPan[1],
                                  presetValues.flLateReverbPan[2] ) );
    _SetFloatParameter( e_FloatParamEchoTime, presetValues.flEchoTime );
    _SetFloatParameter( e_FloatParamEchoDepth, presetValues.flEchoDepth );
    _SetFloatParameter( e_FloatParamModulationTime, presetValues.flModulationTime );
    _SetFloatParameter( e_FloatParamModulationDepth, presetValues.flModulationDepth );
    _SetFloatParameter( e_FloatParamAirAbsorptionGainHF, presetValues.flAirAbsorptionGainHF );
    _SetFloatParameter( e_FloatParamHFReference, presetValues.flHFReference );
    _SetFloatParameter( e_FloatParamLFReference, presetValues.flLFReference );
    _SetFloatParameter( e_FloatParamRoomRolloffFactor, presetValues.flRoomRolloffFactor );
    _SetDelayHFLimit( ( presetValues.iDecayHFLimit == AL_TRUE ? true : false ) );
}

//-----------------------------------------------------------------------

void ReverbEffect::SetFloatParameter(FloatParam param, float value)
{
    _SetFloatParameter( param, value );
    _SettingsUpdated();
}

//-----------------------------------------------------------------------

float ReverbEffect::GetFloatParameter(FloatParam param)
{
    assert( ( param < e_FloatParamCount ) && "EAX Reverb FloatParam must be less than e_FloatParamCount" );

    float retVal = 0.0;

    _GetContext().Bind();

    alGetEffectf( GetALHandle(), m_FloatParamToOpenALEAXReverbFloatParamMap[param], &retVal );
    CheckForALErrors( _GetContext() );

    return retVal;
}

//-----------------------------------------------------------------------

void ReverbEffect::SetReflectionsPan(const glm::vec3& panVector)
{
    _SetReflectionsPan( panVector );
    _SettingsUpdated();
}

//-----------------------------------------------------------------------

void ReverbEffect::SetLateReverbPan(const glm::vec3& panVector)
{
    _SetLateReverbPan( panVector );
    _SettingsUpdated();
}

//-----------------------------------------------------------------------

void ReverbEffect::SetDelayHFLimit(bool limit)
{
    _SetDelayHFLimit( limit );
    _SettingsUpdated();
}

//-----------------------------------------------------------------------

glm::vec3 ReverbEffect::GetReflectionsPan()
{
    ALfloat panVectorValues[3] = { 0.0f };
    alGetEffectfv( GetALHandle(), AL_EAXREVERB_REFLECTIONS_PAN, panVectorValues );
    CheckForALErrors( _GetContext() );

    return glm::vec3( panVectorValues[0], panVectorValues[1], panVectorValues[2] );
}

//-----------------------------------------------------------------------

glm::vec3 ReverbEffect::GetLateReverbPan()
{
    ALfloat panVectorValues[3] = { 0.0f };
    alGetEffectfv( GetALHandle(), AL_EAXREVERB_LATE_REVERB_PAN, panVectorValues );
    CheckForALErrors( _GetContext() );

    return glm::vec3( panVectorValues[0], panVectorValues[1], panVectorValues[2] );
}

//-----------------------------------------------------------------------

bool ReverbEffect::GetDelayHFLimit()
{
    ALint decayHFLimit = AL_FALSE;
    alGetEffecti( GetALHandle(), AL_EAXREVERB_DECAY_HFLIMIT, &decayHFLimit );
    CheckForALErrors( _GetContext() );

    return ( ( decayHFLimit == AL_TRUE ) ? true : false );
}

//-----------------------------------------------------------------------

void ReverbEffect::_SetFloatParameter(FloatParam param, float value)
{
    assert( ( param < e_FloatParamCount ) && "EAX Reverb FloatParam must be less than e_FloatParamCount" );

    const float c_MinValue = GetMinimumFloatParameterValue( param );
    const float c_MaxValue = GetMaximumFloatParameterValue( param );

    assert( ( value >= c_MinValue ) && ( value <= c_MaxValue ) );
    value = std::max( c_MinValue, std::min( value, c_MaxValue ) );

    _GetContext().Bind();

    alEffectf( GetALHandle(), m_FloatParamToOpenALEAXReverbFloatParamMap[param], value );
    CheckForALErrors( _GetContext() );
}

//-----------------------------------------------------------------------

void ReverbEffect::_SetReflectionsPan(const glm::vec3& panVector)
{
    static const float c_MinVectorMagnitude = 0.0f;
    static const float c_MaxVectorMagnitude = 1.0f;

    const float vectorMagnitude = glm::length( panVector );

    assert( ( vectorMagnitude >= c_MinVectorMagnitude ) &&
            ( vectorMagnitude <= c_MaxVectorMagnitude ) && "Vector magnitude is out of supported the range" );

    ALfloat panVectorValues[3] = { panVector.x, panVector.y, panVector.z };
    alEffectfv( GetALHandle(), AL_EAXREVERB_REFLECTIONS_PAN, panVectorValues );
    CheckForALErrors( _GetContext() );
}

void ReverbEffect::_SetLateReverbPan(const glm::vec3& panVector)
{
    static const float c_MinVectorMagnitude = 0.0f;
    static const float c_MaxVectorMagnitude = 1.0f;

    const float vectorMagnitude = glm::length( panVector );

    assert( ( vectorMagnitude >= c_MinVectorMagnitude ) &&
            ( vectorMagnitude <= c_MaxVectorMagnitude ) && "Vector magnitude is out of supported the range" );

    ALfloat panVectorValues[3] = { panVector.x, panVector.y, panVector.z };
    alGetEffectfv( GetALHandle(), AL_EAXREVERB_LATE_REVERB_PAN, panVectorValues );
    CheckForALErrors( _GetContext() );
}

//-----------------------------------------------------------------------

void ReverbEffect::_SetDelayHFLimit(bool limit)
{
    ALint decayHFLimit = ( limit ? AL_TRUE : AL_FALSE );
    alEffecti( GetALHandle(), AL_EAXREVERB_DECAY_HFLIMIT, decayHFLimit );
    CheckForALErrors( _GetContext() );
}

//-----------------------------------------------------------------------

}
