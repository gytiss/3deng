/**
 * Constants for identifying raw inputs from hardware/OS layers
 */

#ifndef RAWINPUTCONSTANTS_H_INCLUDED
#define RAWINPUTCONSTANTS_H_INCLUDED

#include <string>

namespace InputMapping
{
	enum RawInputButton
	{
        e_RawInputButtonReturn = 0,
        e_RawInputButtonEscape = 1,
        e_RawInputButtonBackspace = 2,
        e_RawInputButtonTab,
        e_RawInputButtonSpace,
        e_RawInputButtonExclaim,
        e_RawInputButtonQuoteDbl,
        e_RawInputButtonHash,
        e_RawInputButtonPercent,
        e_RawInputButtonDollar,
        e_RawInputButtonAmpersand,
        e_RawInputButtonQuote,
        e_RawInputButtonLeftParen,
        e_RawInputButtonRightParent,
        e_RawInputButtonAsterisk,
        e_RawInputButtonPlus,
        e_RawInputButtonComma,
        e_RawInputButtonMinus,
        e_RawInputButtonPeriod,
        e_RawInputButtonSlash,
        e_RawInputButton0,
        e_RawInputButton1,
        e_RawInputButton2,
        e_RawInputButton3,
        e_RawInputButton4,
        e_RawInputButton5,
        e_RawInputButton6,
        e_RawInputButton7,
        e_RawInputButton8,
        e_RawInputButton9,
        e_RawInputButtonColon,
        e_RawInputButtonSemicolon,
        e_RawInputButtonLess,
        e_RawInputButtonEquals,
        e_RawInputButtonGreater,
        e_RawInputButtonQuestion,
        e_RawInputButtonAt,

        e_RawInputButtonLeftBracket,
        e_RawInputButtonBackslash,
        e_RawInputButtonRightBracket,
        e_RawInputButtonCarret,
        e_RawInputButtonUnderscore,
        e_RawInputButtonBackquote,
        e_RawInputButtonA,
        e_RawInputButtonB,
        e_RawInputButtonC,
        e_RawInputButtonD,
        e_RawInputButtonE,
        e_RawInputButtonF,
        e_RawInputButtonG,
        e_RawInputButtonH,
        e_RawInputButtonI,
        e_RawInputButtonJ,
        e_RawInputButtonK,
        e_RawInputButtonL,
        e_RawInputButtonM,
        e_RawInputButtonN,
        e_RawInputButtonO,
        e_RawInputButtonP,
        e_RawInputButtonQ,
        e_RawInputButtonR,
        e_RawInputButtonS,
        e_RawInputButtonT,
        e_RawInputButtonU,
        e_RawInputButtonV,
        e_RawInputButtonW,
        e_RawInputButtonX,
        e_RawInputButtonY,
        e_RawInputButtonZ,

        e_RawInputButtonCapsLock,

        e_RawInputButtonF1,
        e_RawInputButtonF2,
        e_RawInputButtonF3,
        e_RawInputButtonF4,
        e_RawInputButtonF5,
        e_RawInputButtonF6,
        e_RawInputButtonF7,
        e_RawInputButtonF8,
        e_RawInputButtonF9,
        e_RawInputButtonF10,
        e_RawInputButtonF11,
        e_RawInputButtonF12,

        e_RawInputButtonPrintScreen,
        e_RawInputButtonScrollLock,
        e_RawInputButtonPause,
        e_RawInputButtonInsert,
        e_RawInputButtonHome,
        e_RawInputButtonPageUp,
        e_RawInputButtonDelete,
        e_RawInputButtonEnd,
        e_RawInputButtonPageDown,
        e_RawInputButtonRight,
        e_RawInputButtonLeft,
        e_RawInputButtonDown,
        e_RawInputButtonUp,

        e_RawInputButtonNumLock,
        e_RawInputButtonKeyPadDivide,
        e_RawInputButtonKeyPadMultiply,
        e_RawInputButtonKeyPadMinus,
        e_RawInputButtonKeyPadPlus,
        e_RawInputButtonKeyPadEnter,
        e_RawInputButtonKeyPad1,
        e_RawInputButtonKeyPad2,
        e_RawInputButtonKeyPad3,
        e_RawInputButtonKeyPad4,
        e_RawInputButtonKeyPad5,
        e_RawInputButtonKeyPad6,
        e_RawInputButtonKeyPad7,
        e_RawInputButtonKeyPad8,
        e_RawInputButtonKeyPad9,
        e_RawInputButtonKeyPad0,
        e_RawInputButtonKeyPadPeriod,

        e_RawInputButtonLeftControl,
        e_RawInputButtonLeftShift,
        e_RawInputButtonLeftAlt,
        e_RawInputButtonRightControl,
        e_RawInputButtonRightShift,
        e_RawInputButtonRightAlt,

        e_RawInputButtonLeftMeta,  // Windows Key or Command Key on OS X
        e_RawInputButtonRightMeta, // Windows Key or Command Key on OS X

        e_RawInputButtonMouseLeft,
        e_RawInputButtonMouseRight,
        e_RawInputButtonMouseMiddle,

        // Used on phones
        e_RawInputFingerOne,
        e_RawInputFingerTwo,

        e_RawInputButtonInvalid,                         // Invalid input button
        e_RawInputButtonCount = e_RawInputButtonInvalid  // Not a button (Used for counting)
	};

	enum RawInputAxis
	{
		e_RawInputAxisMouseRelativeX = 0,
		e_RawInputAxisMouseRelativeY = 1,
		e_RawInputAxisMouseAbsoluteX,
		e_RawInputAxisMouseAbsoluteY,
		e_RawInputAxisMouseWheelHorizontal, // Positive values when rotating to the right, negative values when rotating to the left
		e_RawInputAxisMouseWheelVertical,   // Positive values when rotating away from the user, negative values when rotating towards the user

		e_RawInputAxisFingerOneRelativeX,
		e_RawInputAxisFingerOneRelativeY,
		e_RawInputAxisFingerOneAbsoluteX,
		e_RawInputAxisFingerOneAbsoluteY,
        e_RawInputAxisFingerTwoRelativeX,
        e_RawInputAxisFingerTwoRelativeY,
        e_RawInputAxisFingerTwoAbsoluteX,
        e_RawInputAxisFingerTwoAbsoluteY,

		e_RawInputAxisInvalid,                       // Invalid input axis
		e_RawInputAxisCount = e_RawInputAxisInvalid  // Not an axis (Used for counting)
	};

	bool RawInputButtonNameToRawInputButton(const std::string& buttonName,
                                            RawInputButton& outVal);
	bool RawInputAxisNameToRawInputAxis(const std::string& axisName,
                                        RawInputAxis& outVal);

	bool RawInputButtonToRawInputButtonName(RawInputButton button, std::string& outVal);
}

#endif // RAWINPUTCONSTANTS_H_INCLUDED
