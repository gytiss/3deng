#include "Mesh.h"
#include <Resources/AnimationManager.h>
#include <cstring>

//-----------------------------------------------------------------------

Mesh::Mesh(const std::string& name,
           VAO* vao,
           size_t indexCount,
           size_t vertexCount,
           const EngineTypes::AABB& boundingBox,
           const SubMeshList& subMeshes)
: m_IndexDataBase( nullptr ),
  m_VertexDataBase( nullptr ),
  m_VertexDataStride( 0 ),
  m_Name( name ),
  m_VAO( vao ),
  m_IndexCount( indexCount ),
  m_VertexCount( vertexCount ),
  m_BoneCount( 0 ),
  m_BoundingBox( boundingBox ),
  m_SubMeshes( subMeshes ),
  m_InverseBindPose(),
  m_AnimationCompatibilityHash()
{
    assert( m_VAO && "VAO must not be null" );

    memset( m_AnimationCompatibilityHash, 0x00, MObj::c_MD5HashLength );

    m_InverseBindPose.resize( 0 ); // Make sure vector does not waste any memory
}

//-----------------------------------------------------------------------

Mesh::Mesh(const std::string& name,
           VAO* vao,
           size_t indexCount,
           size_t vertexCount,
           const EngineTypes::AABB& boundingBox,
           const SubMeshList& subMeshes,
           size_t boneCount,
           const EngineTypes::MatrixArray& inverseBindPose,
           uint8_t animationCompatibilityHash[MObj::c_MD5HashLength])
: m_IndexDataBase( nullptr ),
  m_VertexDataBase( nullptr ),
  m_VertexDataStride( 0 ),
  m_Name( name ),
  m_VAO( vao ),
  m_IndexCount( indexCount ),
  m_VertexCount( vertexCount ),
  m_BoneCount( boneCount ),
  m_BoundingBox( boundingBox ),
  m_SubMeshes( subMeshes ),
  m_InverseBindPose( inverseBindPose ),
  m_AnimationCompatibilityHash()
{
    assert( m_VAO && "VAO must not be null" );

    memcpy( m_AnimationCompatibilityHash, animationCompatibilityHash, MObj::c_MD5HashLength );
}

//-----------------------------------------------------------------------

Mesh::~Mesh()
{
    delete m_VAO;
}

//-----------------------------------------------------------------------

bool Mesh::CheckIfAnimationIsCompatible(const AnimationManager::AnimationHandle& handle) const
{
    const uint8_t* animBoneNamesMD5Hash = handle->GetBoneNamesMD5Hash();
    return !memcmp( m_AnimationCompatibilityHash, animBoneNamesMD5Hash, MObj::c_MD5HashLength );
}

//-----------------------------------------------------------------------

bool Mesh::IsSkinned() const
{
    return ( m_BoneCount > 0 );
}

//-----------------------------------------------------------------------

bool Mesh::HasMeshDataOnCPUReadableMemory() const
{
    return ( ( m_IndexDataBase != nullptr ) &&
             ( m_VertexDataBase != nullptr ) &&
             ( m_VertexDataStride > 0 ) );
}

//-----------------------------------------------------------------------

const std::string& Mesh::GetName() const
{
    return m_Name;
}

//-----------------------------------------------------------------------

VAO* Mesh::GetVAO()
{
    return m_VAO;
}

//-----------------------------------------------------------------------

size_t Mesh::GetIndexCount() const
{
    return m_IndexCount;
}

//-----------------------------------------------------------------------

size_t Mesh::GetVertexCount() const
{
    return m_VertexCount;
}

//-----------------------------------------------------------------------

size_t Mesh::GetBoneCount() const
{
    return m_BoneCount;
}

//-----------------------------------------------------------------------

const EngineTypes::AABB& Mesh::GetAABB() const
{
    return m_BoundingBox;
}

//-----------------------------------------------------------------------

const Mesh::SubMeshList& Mesh::GetSubMeshes() const
{
    return m_SubMeshes;
}

//-----------------------------------------------------------------------

const EngineTypes::MatrixArray& Mesh::GetInverseBindPose()
{
    return m_InverseBindPose;
}

//-----------------------------------------------------------------------


const EngineTypes::VertexIndexType* Mesh::GetIndexDataBase() const
{
    return ( HasMeshDataOnCPUReadableMemory() ? m_IndexDataBase : nullptr );
}

//-----------------------------------------------------------------------

const float* Mesh::GetVertexDataBase() const
{
    return ( HasMeshDataOnCPUReadableMemory() ? m_VertexDataBase : nullptr );
}

//-----------------------------------------------------------------------

unsigned int Mesh::GetVertexDataStride() const
{
    return ( HasMeshDataOnCPUReadableMemory() ? m_VertexDataStride : 0 );
}

//-----------------------------------------------------------------------

void Mesh::_SetMeshDataOnCPUReadableMemory(const EngineTypes::VertexIndexType* indexDataBase,
                                           const float* vertexDataBase,
                                           unsigned int vertexDataStride)
{
    assert( indexDataBase && "Index data must not be null" );
    assert( vertexDataBase && "Vertex data must not be null" );
    assert( ( vertexDataStride > 0 ) && "Vertex data stride must not be zero" );

    m_IndexDataBase = indexDataBase;
    m_VertexDataBase = vertexDataBase;
    m_VertexDataStride = vertexDataStride;
}

//-----------------------------------------------------------------------
