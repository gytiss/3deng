#include "Effect.h"
#include "EffectSlot.h"
#include "OpenALErrorChecking.h"

namespace AudioDevice
{

//-----------------------------------------------------------------------

Effect::Effect(Context& context, EffectSlot& effectSlot, Type type)
: m_ALEffect( AL_EFFECT_NULL ),
  m_Context( context ),
  m_EffectSlotAttachedTo( effectSlot ),
  m_Type( type )
{
    m_Context.Bind();

    alGenEffects( 1, &m_ALEffect );
    CheckForALErrors( m_Context );

    // OpenAL needs to know an effect type of this effect object
    // if it did not then this effect object would be invalid
    ALint effectType = type;
    alGetEffecti( m_ALEffect, AL_EFFECT_TYPE, &effectType );
    CheckForALErrors( m_Context );
}

//-----------------------------------------------------------------------

Effect::~Effect()
{
    if( m_ALEffect != AL_EFFECT_NULL )
    {
        alDeleteEffects( 1, &m_ALEffect );
        CheckForALErrors( m_Context );
    }
}

//-----------------------------------------------------------------------

void Effect::_SettingsUpdated()
{
    static_assert( std::is_base_of<EffectSettingsUpdateListener, EffectSlot>::value, "\"EffectSlot\" must be a subclass of the \"EffectSettingsUpdateListener\"" );

    static_cast<EffectSettingsUpdateListener&>( m_EffectSlotAttachedTo ).EffectSettingsUpdated();
}

//-----------------------------------------------------------------------

}
