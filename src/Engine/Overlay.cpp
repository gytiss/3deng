#include "Overlay.h"
#include "MsgLogger.h"

#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

#include <OpenGLErrorChecking.h>
#include <GraphicsDevice/GraphicsDevice.h>

//-----------------------------------------------------------------------

Overlay::Overlay(GraphicsDevice& graphicsDevice,
                 TextureManager& textureManager,
                 FileManager& fileManager,
                 std::string textTextureFileName,
                 size_t drawableSurfaceWidth,
                 size_t drawableSurfaceHeight)
: DrawableEntity(graphicsDevice, fileManager, false, false),
  m_GraphicsDevice(graphicsDevice),
  m_TextureManager( textureManager ),
  m_FullScreenQuadTextureHandle()
{

    m_drawableSurfaceWidth=static_cast<float>(drawableSurfaceWidth);
    m_drawableSurfaceHeight=static_cast<float>(drawableSurfaceHeight);

    m_FullScreenQuadVertexes=generateFullScreenQuad(m_drawableSurfaceWidth,m_drawableSurfaceHeight);
    prepareFullScrenQuadRenderingContext();

    _LoadTexture( textTextureFileName );

    m_RenderFromDefaultTexture=true;
}

//-----------------------------------------------------------------------

Overlay::Overlay(GraphicsDevice& graphicsDevice,
                 TextureManager& textureManager,
                 FileManager& fileManager,
                 size_t drawableSurfaceWidth,
                 size_t drawableSurfaceHeight)
: DrawableEntity(graphicsDevice, fileManager, false,false),
  m_GraphicsDevice( graphicsDevice ),
  m_TextureManager( textureManager ),
  m_FullScreenQuadTextureHandle()
{
    m_drawableSurfaceWidth=static_cast<float>(drawableSurfaceWidth);
    m_drawableSurfaceHeight=static_cast<float>(drawableSurfaceHeight);

    m_FullScreenQuadVertexes=this->generateFullScreenQuad(m_drawableSurfaceWidth,m_drawableSurfaceHeight);

    prepareFullScrenQuadRenderingContext();

    m_CustomTextureSet=false;
    m_RenderFromDefaultTexture=false;
}

//-----------------------------------------------------------------------

Overlay::~Overlay()
{
    delete[] m_FullScreenQuadVertexes;

    m_TextureManager.ReleaseTexture( m_FullScreenQuadTextureHandle );
}

//-----------------------------------------------------------------------

void Overlay::render(ShaderProg* shaderProgToUse_1, ShaderProg* UNUSED_PARAM(shaderProgToUse_2), const DrawableEntity::Transformation* UNUSED_PARAM(t))
{
    if(m_RenderFromDefaultTexture || m_CustomTextureSet)
    {
        m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionDepthTesting );
        m_GraphicsDevice.OptionDisable( GraphicsDevice::e_OptionDepthTesting );

        m_GraphicsDevice.OptionSaveCurrentState( GraphicsDevice::e_OptionBlending );
        m_GraphicsDevice.SaveCurrentBlendFunctionState();
        m_GraphicsDevice.SaveCurrentBlendEquation();


        glBindVertexArray(this->m_VAO);
        CheckForGLErrors();
        glBindBuffer(GL_ARRAY_BUFFER,this->m_VBO);
        CheckForGLErrors();

        shaderProgToUse_1->Use();

        glUniformMatrix4fv(glGetUniformLocation(shaderProgToUse_1->GetHandle(), "mvmatrix"), 1, GL_FALSE, glm::value_ptr(glm::mat4(1)));
        CheckForGLErrors();
        glUniformMatrix4fv(glGetUniformLocation(shaderProgToUse_1->GetHandle(), "pmatrix"), 1, GL_FALSE, glm::value_ptr(glm::ortho(-m_drawableSurfaceWidth / 2.0f, m_drawableSurfaceWidth / 2.0f, -m_drawableSurfaceHeight / 2.0f, m_drawableSurfaceHeight / 2.0f, 0.1f, 100.0f) ));
        CheckForGLErrors();
        glUniform4fv(glGetUniformLocation(shaderProgToUse_1->GetHandle(), "lposss"),1,glm::value_ptr(m_lightPos));
        CheckForGLErrors();

        glUniform1i(glGetUniformLocation(shaderProgToUse_1->GetHandle(), "tex2D"), Texture::e_TypeIdxDiffuseMap);
        CheckForGLErrors();
        glActiveTexture(GL_TEXTURE0);
        CheckForGLErrors();

        if(m_RenderFromDefaultTexture)
        {
            m_FullScreenQuadTextureHandle->Activate();
        }
        else if(m_CustomTextureSet)
        {
//            m_CustomTextureHandle->Activate();
        }

        if(m_AlphaBlendingEnabled)
        {
            m_GraphicsDevice.OptionEnable( GraphicsDevice::e_OptionBlending );

            m_GraphicsDevice.SetBlendFunctionState( GraphicsDevice::e_BlendFunctionSrcAlpha,
                                                    GraphicsDevice::e_BlendFunctionOneMinusSrcAlpha );

            m_GraphicsDevice.SetBlendEquation( GraphicsDevice::e_BlendEquationAdd );
        }

        glDrawArrays(GL_TRIANGLES,0,6);
        CheckForGLErrors();

        m_GraphicsDevice.RestorePreviousBlendEquation();
        m_GraphicsDevice.RestorePreviousBlendFunctionState();
        m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionBlending );
        m_GraphicsDevice.OptionRestorePreviousState( GraphicsDevice::e_OptionDepthTesting );
    }
}

//-----------------------------------------------------------------------

ModelBinObj::Vertex* Overlay::generateFullScreenQuad(float width, float height)
{
   //
   // All the triangles in this quad are in counterclockwise order
   //

   ModelBinObj::Vertex* quad = new ModelBinObj::Vertex[6];

   quad[0].position[0]=-width / 2.0f;
   quad[0].position[1]=-height / 2.0f;
   quad[0].position[2]=0.1f;
   quad[0].texCoord[0]=0.0f;
   quad[0].texCoord[1]=0.0f;

   quad[1].position[0]=width / 2.0f;
   quad[1].position[1]=-height / 2.0f;
   quad[1].position[2]=0.1f;
   quad[1].texCoord[0]=1.0f;
   quad[1].texCoord[1]=0.0f;

   quad[2].position[0]=-width / 2.0f;
   quad[2].position[1]=height / 2.0f;
   quad[2].position[2]=0.1f;
   quad[2].texCoord[0]=0.0f;
   quad[2].texCoord[1]=1.0f;

   quad[3].position[0]=-width / 2.0f;
   quad[3].position[1]=height / 2.0f;
   quad[3].position[2]=0.1f;
   quad[3].texCoord[0]=0.0f;
   quad[3].texCoord[1]=1.0f;

   quad[4].position[0]=width / 2.0f;
   quad[4].position[1]=-height / 2.0f;
   quad[4].position[2]=0.1f;
   quad[4].texCoord[0]=1.0f;
   quad[4].texCoord[1]=0.0f;

   quad[5].position[0]=width / 2.0f;
   quad[5].position[1]=height / 2.0f;
   quad[5].position[2]=0.1f;
   quad[5].texCoord[0]=1.0f;
   quad[5].texCoord[1]=1.0f;

   return quad;
}

//-----------------------------------------------------------------------

void Overlay::_LoadTexture(const std::string& texturePath)
{

    m_FullScreenQuadTextureHandle = m_TextureManager.Load2DTexture( texturePath,
                                                                    Texture::e_TypeIdxDiffuseMap,
                                                                    Texture::e_StateParamArgSCoordWrapRepeat,
                                                                    Texture::e_StateParamArgTCoordWrapRepeat,
                                                                    false );
}

//-----------------------------------------------------------------------

void Overlay::prepareFullScrenQuadRenderingContext()
{
    glGenBuffers(1,&this->m_VBO);
    CheckForGLErrors();
    glGenVertexArrays(1,&this->m_VAO);
    CheckForGLErrors();

    glBindVertexArray(this->m_VAO);
    CheckForGLErrors();
    glBindBuffer(GL_ARRAY_BUFFER,this->m_VBO);
    CheckForGLErrors();
    glBufferData ( GL_ARRAY_BUFFER, 6 * sizeof ( ModelBinObj::Vertex ), this->m_FullScreenQuadVertexes, GL_STATIC_DRAW );
    CheckForGLErrors();

    glVertexAttribPointer (0 /*in_Position*/, 3, GL_FLOAT, GL_FALSE,  sizeof ( ModelBinObj::Vertex), ( const GLvoid* ) offsetof (ModelBinObj::Vertex,position) );
    CheckForGLErrors();
    glEnableVertexAttribArray(0 /*in_Position*/);
    CheckForGLErrors();

    glVertexAttribPointer (2 /*in_uvCoord*/, 2, GL_FLOAT, GL_FALSE, sizeof ( ModelBinObj::Vertex ), ( const GLvoid* ) offsetof(ModelBinObj::Vertex,texCoord) );
    CheckForGLErrors();
    glEnableVertexAttribArray(2 /*in_uvCoord*/);
    CheckForGLErrors();
}

//-----------------------------------------------------------------------
