#include "DirectionalLight.h"
#include "ShaderPrograms.h"

//-----------------------------------------------------------------------

DirectionalLight::DirectionalLight(ShaderProgDirectionalLightUberShader& shaderProg)
: m_Enabled( true ),
  m_Color( 1.0f, 1.0f, 1.0f ), // White
  m_AmbientIntensity( 0.0f ),
  m_DiffuseIntensity( 1.0f ),
  m_DirectionInWorldSpace( 0.0f, -1.0f, 0.0f ), // Towards -Y axis
  m_InfluenceMesh(),
  m_ShaderProg( shaderProg )
{

}

//-----------------------------------------------------------------------

void DirectionalLight::RenderForLightPass(const glm::mat4& viewMatrix,
                                          const glm::mat4& projectionMatrix)
{
    m_ShaderProg.Use();
    m_ShaderProg.SetUniformLightColor( m_Color );
    m_ShaderProg.SetUniformLightAmbientIntensity( m_AmbientIntensity );
    m_ShaderProg.SetUniformLightDiffuseIntensity( m_DiffuseIntensity );
    m_ShaderProg.SetUniformInverseProjectionMatrix( glm::inverse( projectionMatrix ) );
    const glm::vec4 directionInViewSpace = ( viewMatrix * glm::vec4( m_DirectionInWorldSpace, 0.0f ) );
    m_ShaderProg.SetUniformLightDirectionInViewSpace( glm::vec3( directionInViewSpace ) );

    m_InfluenceMesh.Render( m_ShaderProg );
}

//-----------------------------------------------------------------------
