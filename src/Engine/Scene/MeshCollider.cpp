#include "MeshCollider.h"
#include "MeshFilter.h"
#include "GameObject.h"
#include <Resources/MeshManager.h>

//-----------------------------------------------------------------------

MeshCollider::MeshCollider(GameObject& owningGameObject,
                           PhysicsManager& physicsManager)
: Collider( owningGameObject, e_ColliderTypeMesh, "Mesh Collider", physicsManager )
{
    _InitializeAsDisabled();
    _FindAndSetMesh();
}

//-----------------------------------------------------------------------

void MeshCollider::_FindAndSetMesh()
{
    MeshFilter* meshFilter = GetOwningGameObject().GetComponent<MeshFilter>();

    if( meshFilter &&
        meshFilter->IsEnabled() &&
        meshFilter->m_MeshHandle.IsValid() )
    {
        if( !meshFilter->m_MeshHandle->HasMeshDataOnCPUReadableMemory() )
        {
            meshFilter->m_MeshManager.LoadMeshDataIntoCPUReadableMemory( meshFilter->m_MeshHandle );
        }

        PhysicsManager::CollisionShapeHandle shape = m_PhysicsManager.CreateMeshCollisionShape( meshFilter->m_MeshHandle );
        _ChangeCollisionShape( shape );
    }
    else
    {
        _ClearCollisionShape();
    }
}

//-----------------------------------------------------------------------
