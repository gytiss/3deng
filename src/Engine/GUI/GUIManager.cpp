#include "GUIManager.h"
#include "GUIContext.h"
#include <Exceptions/GenericException.h>
#include <Rocket/Core.h>
#include <Rocket/Controls.h>
#include <Rocket/Debugger.h>

#include <luabind/luabind.hpp>
#include <luabind/operator.hpp>

#include <LuaUtils.h>

//-----------------------------------------------------------------------

GUIManager::GUIManager(GraphicsDevice& graphicsDevice,
                       SystemInterface& systemInterface,
                       FileManager& fileManager,
                       const std::string& guiDataRootDirectoryPath,
                       bool debuggingEnabled)
: m_GraphicsDevice( graphicsDevice ),
  m_SystemInterface( systemInterface ),
  m_FileManager( fileManager ),
  m_DrawableSurfaceSize( m_SystemInterface.GetDrawableSurfaceSize() ),
  m_DataRootDirPath( guiDataRootDirectoryPath ),
  m_FileInterface( m_DataRootDirPath.c_str() ),
  m_RenderInterface( m_DataRootDirPath, m_GraphicsDevice, m_FileManager, m_DrawableSurfaceSize.widthInPixels, m_DrawableSurfaceSize.heightInPixels ),
  m_SysInterface( m_SystemInterface ),
  m_Contexts(),
  m_CurrentContext( nullptr ),
  m_LeftMouseButtonState( e_ButtonStateInvalid ),
  m_RightMouseButtonState( e_ButtonStateInvalid ),
  m_MiddleMouseButtonState( e_ButtonStateInvalid ),
  m_DebuggingEnabled( debuggingEnabled )
{
    m_SystemInterface.RegisterDrawableSurfaceSizeChangedCallback( _DrawableSurfaceSizeChaged, this );

    Rocket::Core::SetFileInterface( &m_FileInterface );
    Rocket::Core::SetRenderInterface( &m_RenderInterface );
    Rocket::Core::SetSystemInterface( &m_SysInterface );

    if( !Rocket::Core::Initialise() )
    {
        throw GenericException( "Failed to initialise libRocket" );
    }

    Rocket::Controls::Initialise();

    _LoadFonts();
}

//-----------------------------------------------------------------------

GUIManager::~GUIManager()
{
    m_SystemInterface.DeregisterDrawableSurfaceSizeChangedCallback( _DrawableSurfaceSizeChaged, this );
}

//-----------------------------------------------------------------------

void GUIManager::InputCallback(InputMapping::MappedInput& inputs,
                               float UNUSED_PARAM(frameTimeInSeconds),
                               void* guiManager)
{
    reinterpret_cast<GUIManager*>( guiManager )->_InputCallback( inputs );
}

//-----------------------------------------------------------------------

bool GUIManager::AddContext(const std::string& contextName)
{
    bool retVal = false;

    if( m_Contexts.find( contextName ) == m_Contexts.end() )
    {
        retVal = true;

        m_Contexts[contextName] = new GUIContext( contextName,
                                                  Rocket::Core::Vector2i( static_cast<int>( m_DrawableSurfaceSize.widthInPixels ),
                                                                          static_cast<int>( m_DrawableSurfaceSize.heightInPixels ) ) );

        // Set the first added context as the current one
        if( !m_CurrentContext )
        {
            // If debugging is enabled then initialize the debugger when the first context is added
            if( m_DebuggingEnabled )
            {
                Rocket::Debugger::Initialise( m_Contexts[contextName]->GetLibRocketContext() );
            }

            SwitchToContext( contextName );
        }
    }

    return retVal;
}

//-----------------------------------------------------------------------

bool GUIManager::SwitchToContext(const std::string& contextName)
{
    bool retVal = false;

    ContextMap_t::iterator it = m_Contexts.find( contextName );
    if( it != m_Contexts.end() )
    {
        retVal = true;
        m_CurrentContext = it->second;
        Rocket::Core::Factory::RegisterEventListenerInstancer( m_CurrentContext->GetEventInstancer() );

        m_CurrentContext->GetLibRocketContext()->SetDimensions( Rocket::Core::Vector2i( static_cast<int>( m_DrawableSurfaceSize.widthInPixels ) ,
                                                                                        static_cast<int>( m_DrawableSurfaceSize.heightInPixels ) ) );

        if( m_DebuggingEnabled )
        {
            // Set the current context as the context to be debugged
            Rocket::Debugger::SetContext( m_CurrentContext->GetLibRocketContext() );
        }
    }

    return retVal;
}

//-----------------------------------------------------------------------

GUIContext* GUIManager::GetCurrentContext()
{
    return m_CurrentContext;
}

//-----------------------------------------------------------------------

GUIRenderInterface& GUIManager::GetRenderInterface()
{
    return m_RenderInterface;
}

//-----------------------------------------------------------------------

void GUIManager::_LoadFonts()
{
    bool ok = false;
    const std::string fontListCfgFilePath = m_DataRootDirPath + "/FontsToLoad.cfg";
    std::string mainErrMsg = "Failed to load Font List config from file '" + fontListCfgFilePath + "' ";
    LuaUtils::LuaStateWrapper luaState;

    luaL_openlibs( luaState );
    if( LuaUtils::LoadLuaScript( luaState, fontListCfgFilePath, m_FileManager ) )
    {
        std::string secondaryErrMsg = "with the following error: ";
        ok = true;

        // Connect LuaBind to this lua state
        luabind::open( luaState );

        luabind::object topTable = luabind::globals( luaState )["Fonts"];

        if( !topTable )
        {
            secondaryErrMsg += "Global Lua table 'Fonts' does not exist";
            ok = false;
        }

        if( ok && ( luabind::type( topTable ) == LUA_TTABLE ) )
        {
            for (luabind::iterator fontIt(topTable), end; fontIt != end && ok; ++fontIt)
            {
                if( luabind::type( *fontIt ) == LUA_TSTRING )
                {
                    const std::string relativeFontPath = luabind::tostring_operator( *fontIt );
                    if( !Rocket::Core::FontDatabase::LoadFontFace( relativeFontPath.c_str() ) )
                    {
                        std::string msg = "Failed to load font \"";
                                     msg += m_DataRootDirPath;
                                     msg += "/";
                                     msg += relativeFontPath;
                                     msg += "\"";

                        throw GenericException( msg );
                    }
                }
                else
                {
                    secondaryErrMsg += "'Fonts' table has a non string member '";
                    secondaryErrMsg += luabind::tostring_operator( *fontIt );
                    secondaryErrMsg += "'";
                    ok = false;
                }
            }

        }
        else if( ok )
        {
            secondaryErrMsg += "Global Lua variable 'Fonts' is not a table";
            ok = false;
        }

        if( !ok )
        {
            mainErrMsg += secondaryErrMsg;
        }
    }

    if( !ok )
    {
        throw GenericException( mainErrMsg );
    }
}

//-----------------------------------------------------------------------

void GUIManager::_InputCallback(InputMapping::MappedInput& inputs)
{
    if( m_CurrentContext )
    {
        static const int c_NoKeyModifiers = 0;
        static const int c_LeftMouseButtonLibRocketId = 0;
        static const int c_RightMouseButtonLibRocketId = 1;
        static const int c_MiddleMouseButtonLibRocketId = 2;

        /*------------------------- Mouse Buttons --------------------------*/
        if( inputs.FindState( InputMapping::e_StateLeftMouseButtonIsDown ) )
        {
            if( m_LeftMouseButtonState != e_ButtonStateDown )
            {
                m_CurrentContext->GetLibRocketContext()->ProcessMouseButtonDown( c_LeftMouseButtonLibRocketId,
                                                                                 c_NoKeyModifiers );
                m_LeftMouseButtonState = e_ButtonStateDown;
            }
        }
        else if( ( m_LeftMouseButtonState == e_ButtonStateDown ) &&
                  ( m_LeftMouseButtonState != e_ButtonStateInvalid ) )
        {
            m_CurrentContext->GetLibRocketContext()->ProcessMouseButtonUp( c_LeftMouseButtonLibRocketId,
                                                                           c_NoKeyModifiers );
            m_LeftMouseButtonState = e_ButtonStateUp;
        }

        if( inputs.FindState( InputMapping::e_StateMiddleMouseButtonIsDown ) )
        {
            if( m_MiddleMouseButtonState != e_ButtonStateDown )
            {
                m_CurrentContext->GetLibRocketContext()->ProcessMouseButtonDown( c_MiddleMouseButtonLibRocketId,
                                                                                 c_NoKeyModifiers );
                m_MiddleMouseButtonState = e_ButtonStateDown;
            }
        }
        else if( ( m_MiddleMouseButtonState == e_ButtonStateDown ) &&
                  ( m_MiddleMouseButtonState != e_ButtonStateInvalid ) )
        {
            m_CurrentContext->GetLibRocketContext()->ProcessMouseButtonUp( c_MiddleMouseButtonLibRocketId,
                                                                           c_NoKeyModifiers );
            m_MiddleMouseButtonState = e_ButtonStateUp;
        }

        if( inputs.FindState( InputMapping::e_StateRightMouseButtonIsDown ) )
        {
            if( m_RightMouseButtonState != e_ButtonStateDown )
            {
                m_CurrentContext->GetLibRocketContext()->ProcessMouseButtonDown( c_RightMouseButtonLibRocketId,
                                                                                 c_NoKeyModifiers );
                m_RightMouseButtonState = e_ButtonStateDown;
            }
        }
        else if( ( m_RightMouseButtonState == e_ButtonStateDown ) &&
                  ( m_RightMouseButtonState != e_ButtonStateInvalid ) )
        {
            m_CurrentContext->GetLibRocketContext()->ProcessMouseButtonUp( c_RightMouseButtonLibRocketId,
                                                                           c_NoKeyModifiers );
            m_RightMouseButtonState = e_ButtonStateUp;
        }
        /*------------------------------------------------------------------*/

        if( inputs.ranges.find( InputMapping::e_RangeMouseWheelVertical ) != inputs.ranges.end() )
        {
            const int mouseWheelMovement = static_cast<int>( -inputs.ranges[InputMapping::e_RangeMouseWheelVertical] );
            m_CurrentContext->GetLibRocketContext()->ProcessMouseWheel( mouseWheelMovement, c_NoKeyModifiers );
        }

        /*------------------------- Mouse Movement -------------------------*/
        bool mouseMoved = false;
        int mouseX = 0;
        int mouseY = 0;

        if( inputs.ranges.find( InputMapping::e_RangePointerAbsoluteX ) != inputs.ranges.end() )
        {
            mouseX = static_cast<int>( inputs.ranges[InputMapping::e_RangePointerAbsoluteX] );
            mouseMoved = true;
        }

        if( inputs.ranges.find( InputMapping::e_RangePointerAbsoluteY ) != inputs.ranges.end() )
        {
            mouseY = static_cast<int>( inputs.ranges[InputMapping::e_RangePointerAbsoluteY] );
            mouseMoved = true;
        }

        if( mouseMoved )
        {
            m_CurrentContext->GetLibRocketContext()->ProcessMouseMove( mouseX, mouseY, c_NoKeyModifiers );
        }
        /*------------------------------------------------------------------*/

        /*------------------------- Keyboard Keys --------------------------*/
        if( inputs.FindAndConsumeAction( InputMapping::e_ActionToggleGUIDebugger )  )
        {
            Rocket::Debugger::SetVisible( !Rocket::Debugger::IsVisible() );
        }
        /*------------------------------------------------------------------*/

        if( !inputs.textInput.empty() )
        {
            m_CurrentContext->GetLibRocketContext()->ProcessTextInput( inputs.textInput.c_str() );
        }
    }
}

//-----------------------------------------------------------------------

void GUIManager::_SetScreenDimensions(const SystemInterface::DrawableSurfaceSize& size)
{
    m_RenderInterface.SetViewport( static_cast<int>( size.widthInPixels ), 
                                   static_cast<int>( size.heightInPixels ) );

    if( m_CurrentContext )
    {
        m_CurrentContext->GetLibRocketContext()->SetDimensions( Rocket::Core::Vector2i( static_cast<int>(size.widthInPixels ),
                                                                                        static_cast<int>( size.heightInPixels ) ) );
    }

    m_DrawableSurfaceSize = size;
}

//-----------------------------------------------------------------------

void GUIManager::_DrawableSurfaceSizeChaged(const SystemInterface::DrawableSurfaceSize& newSize, void* userData)
{
    static_cast<GUIManager*>( userData )->_SetScreenDimensions( newSize );
}

//-----------------------------------------------------------------------
