#ifndef MATERIAL_H_INCLUDED
#define MATERIAL_H_INCLUDED

#include <vector>
#include <EngineTypes.h>
#include "TextureManager.h"
#include "MaterialDescriptor.h"

class TextureManager;

class MaterialManager;

class Material
{
public:
    friend class MaterialManager;

    void Activate();

    Material(const Material& other) = delete;
    Material& operator=(Material other) = delete;

    const EngineTypes::SRGBColor& GetDiffuseColor() const
    {
        return m_MaterialDescriptor.m_DiffuseColor;
    }

    const EngineTypes::SRGBColor& GetSpecularColor() const
    {
        return m_MaterialDescriptor.m_SpecularColor;
    }

    uint8_t GetGlossiness() const
    {
        return m_MaterialDescriptor.m_Glossiness;
    }

    inline bool HasDiffuseColor() const
    {
        return m_MaterialDescriptor.HasDiffuseColor();
    }

    inline bool HasSpecularColor() const
    {
        return m_MaterialDescriptor.HasSpecularColor();
    }

    inline bool HasGlossiness() const
    {
        return m_MaterialDescriptor.HasGlossiness();
    }

    inline bool HasDiffuseTextureMap() const
    {
        return m_MaterialDescriptor.HasDiffuseTextureMap();
    }

    inline bool HasNormalTextureMap() const
    {
        return m_MaterialDescriptor.HasNormalTextureMap();
    }

    inline bool HasSpecularTextureMap() const
    {
        return m_MaterialDescriptor.HasSpecularTextureMap();
    }

    inline bool HasGlossTextureMap() const
    {
        return m_MaterialDescriptor.HasGlossTextureMap();
    }

protected:
    Material(TextureManager& textureManager);
    Material(const MaterialDescriptor& materialDescriptor);

    ~Material() = default;

private:
    MaterialDescriptor m_MaterialDescriptor;
};

#endif // MATERIAL_H_INCLUDED
