#ifndef LIGHTMANAGER_H_INCLUDED
#define LIGHTMANAGER_H_INCLUDED

#include <list>
#include <SystemInterface.h>
#include "ShaderPrograms.h"

class FBO;
class GraphicsDevice;
class DirectionalLight;
class PointLight;
class SpotLight;
class FileManager;

class LightManager
{
public:
    typedef std::list<PointLight*> PointLightList_t;
    typedef std::list<SpotLight*> SpotLightList_t;

    LightManager(GraphicsDevice& graphicsDevice,
                 FBO& lightResultsFBO,
                 SystemInterface& systemInterface,
                 FileManager& fileManager);

    ~LightManager();

    PointLight* CreateNewPointLight();
    SpotLight* CreateNewSpotLight();
    void DeletePointLight(PointLight* light);
    void DeleteSpotLight(SpotLight* light);

    inline const PointLightList_t& GetPointLights() const
    {
        return m_PointLights;
    }

    inline const SpotLightList_t& GetSpotLights() const
    {
        return m_SpotLights;
    }

   /**
    * Uses lazy initialisation to prevent use of Directional light if not needed
    */
    DirectionalLight* GetMainDirectionalLight();

    void RenderLights(const glm::mat4& viewMatrix,
                      const glm::mat4& projectionMatrix,
                      float frustumNearClippingPlaneDistance,
                      float frustumFarClippingPlaneDistance);

    void RenderLightsDebugData(const glm::mat4& viewMatrix,
                               const glm::mat4& projectionMatrix);

private:
    // Used to prevent copying
    LightManager& operator = (const LightManager& other);
    LightManager(const LightManager& other);

    void _PointLightStencilPass(PointLight& light,
                                const glm::mat4& viewMatrix,
                                const glm::mat4& projectionMatrix);
    void _SpotLightStencilPass(SpotLight& light,
                               const glm::mat4& viewMatrix,
                               const glm::mat4& projectionMatrix);
    void _PointLightPass(PointLight& light,
                         const glm::mat4& viewMatrix,
                         const glm::mat4& projectionMatrix);
    void _SpotLightPass(SpotLight& light,
                        const glm::mat4& viewMatrix,
                        const glm::mat4& projectionMatrix);
    void _DirectionalLightPass(DirectionalLight& light,
                               const glm::mat4& viewMatrix,
                               const glm::mat4& projectionMatrix);

    static void _DrawableSurfaceSizeChaged(const SystemInterface::DrawableSurfaceSize& newSize, void* userData);

    GraphicsDevice& m_GraphicsDevice;
    FBO& m_LightResultsFBO;
    SystemInterface& m_SystemInterface;
    SystemInterface::DrawableSurfaceSize m_DrawableSurfaceSize;
    DirectionalLight* m_DirectionalLight;
    PointLightList_t m_PointLights;
    SpotLightList_t m_SpotLights;
    ShaderProgPointLightUberShader m_PointLightShaderProg;
    ShaderProgSpotLightUberShader m_SpotLightShaderProg;
    ShaderProgDirectionalLightUberShader m_DirectionLightShaderProg;
    ShaderProgLightingStencilPass m_LightingStencilPassShaderProg;
    ShaderProgDebugObjects m_DebugObjectsShaderProg;
};

#endif // LIGHTMANAGER_H_INCLUDED
