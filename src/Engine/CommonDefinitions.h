#ifndef COMMONDEFINITIONS_H_INCLUDED
#define COMMONDEFINITIONS_H_INCLUDED

#include <cstring>

#ifndef _MSC_VER
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wunused-parameter"
    // Note: The above pragmas disabling "-Wunused-parameter" are needed to
    //       prevent GCC from complaining about "arr" being unused
#endif
template<size_t size, class ArrayPtr>
inline constexpr size_t ArraySize(ArrayPtr (&arr)[size])
{
    return size;
}
#ifndef _MSC_VER
    // Restore previous warning options
    #pragma GCC diagnostic pop
#endif

template <typename T>
struct IsPointerPredicate
{
    enum { value = 0 };
};

template <typename T>
struct IsPointerPredicate<T*>
{
    enum { value = 1 };
};

#ifdef UNUSED_PARAM
#elif defined(__GNUC__)
    #define UNUSED_PARAM(x) UNUSED_ ## x __attribute__((unused))
#elif defined(__LCLINT__)
    #define UNUSED_PARAM(x) /*@unused@*/ x
#elif defined(__cplusplus)
    #define UNUSED_PARAM(x)
#else
    #define UNUSED_PARAM(x) x
#endif

#ifdef _MSC_VER
    #define MUTIPLATFORM_ALIGN_OF __alignof
#else
    #define MUTIPLATFORM_ALIGN_OF alignof
#endif



template<class T>
void SafeDelete(T*& pointer)
{
    if( pointer != nullptr )
    {
        delete pointer;
        pointer = nullptr;
    }
}

#endif // COMMONDEFINITIONS_H_INCLUDED
