#!/usr/bin/env bash 

SUCCESS=0

pushd `dirname $0` > /dev/null # Save script call dir and "cd" into the script dir
SCRIPT_DIR=`pwd`
popd > /dev/null # "cd" back into the script call dir

source "$SCRIPT_DIR/AndroidBuildCommon.sh"

if [ "x$BUILD_TYPE" = 'x' ] || \
   [ "x$ARCHS_TO_BUILD_FOR" = 'x' ] || \
   [ "x$SOURCE_DIR_OF_THE_LIB_BEING_BUILT" = 'x' ] || \
   [ "x$ANDROID_CMAKE_DIR" = 'x' ] || \
   [ "x$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT" = 'x' ] || \
   [ "x$RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT" = 'x' ];
then
    echo "You should not use $0 directly, use BuildLibsForAndroid.sh instead"
    exit 1
fi

if [ "x$PRESERVE_BUILD_FILES" = 'x' ];
then
    PRESERVE_BUILD_FILES=NO
fi

ANDROID_BUILD_DIR_OF_THE_LIB_BEING_BUILT=$SOURCE_DIR_OF_THE_LIB_BEING_BUILT/androidBuild

ARMV7_BUILD_DIR_OF_THE_LIB_BEING_BUILT=$ANDROID_BUILD_DIR_OF_THE_LIB_BEING_BUILT/armv7
ARM64_BUILD_DIR_OF_THE_LIB_BEING_BUILT=$ANDROID_BUILD_DIR_OF_THE_LIB_BEING_BUILT/arm64
MIPS_BUILD_DIR_OF_THE_LIB_BEING_BUILT=$ANDROID_BUILD_DIR_OF_THE_LIB_BEING_BUILT/mips
MIPS64_BUILD_DIR_OF_THE_LIB_BEING_BUILT=$ANDROID_BUILD_DIR_OF_THE_LIB_BEING_BUILT/mips64
X86_BUILD_DIR_OF_THE_LIB_BEING_BUILT=$ANDROID_BUILD_DIR_OF_THE_LIB_BEING_BUILT/x86
X86_64_BUILD_DIR_OF_THE_LIB_BEING_BUILT=$ANDROID_BUILD_DIR_OF_THE_LIB_BEING_BUILT/x86_64

RESULTING_ARMV7_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/armv7
RESULTING_ARM64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/arm64
RESULTING_MIPS_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/mips
RESULTING_MIPS64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/mips64
RESULTING_X86_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/x86
RESULTING_X86_64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT=$RESULTING_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/x86_64
    

# Note: Android API level 18 is the lowest level with OpenGL ES 3.0 support

######################################
# Build for ARMv7
######################################
if $(ContainsElement "armv7" ${ARCHS_TO_BUILD_FOR[@]});
then
    mkdir -p $ARMV7_BUILD_DIR_OF_THE_LIB_BEING_BUILT
    mkdir -p $RESULTING_ARMV7_BINARIES_DIR_OF_THE_LIB_BEING_BUILT

    cd $ARMV7_BUILD_DIR_OF_THE_LIB_BEING_BUILT
    cmake -DCMAKE_TOOLCHAIN_FILE=$ANDROID_CMAKE_DIR/android.toolchain.cmake  \
        -DANDROID_NDK=$ANDROID_NDK_HOME                                    \
        -DCMAKE_BUILD_TYPE="$BUILD_TYPE"                                   \
        -DANDROID_ABI="armeabi-v7a"                                        \
        -DANDROID_NATIVE_API_LEVEL=android-18                              \
        -DANDROID_TOOLCHAIN_NAME=arm-linux-androideabi-4.8                 \
        -DANDROID_STL=gnustl_shared                                        \
        -DLIBRARY_OUTPUT_PATH_ROOT=$ARMV7_BUILD_DIR_OF_THE_LIB_BEING_BUILT \
        -DCMAKE_INSTALL_PREFIX=$RESULTING_ARMV7_BINARIES_DIR_OF_THE_LIB_BEING_BUILT            \
        $(ReplaceArchMarkersWithArchName "$EXTRA_CMAKE_FLAGS_FOR_THE_LIB_BEING_BUILT" "armv7") \
        $SOURCE_DIR_OF_THE_LIB_BEING_BUILT
    cmake --build $ARMV7_BUILD_DIR_OF_THE_LIB_BEING_BUILT
    make install
fi

######################################
# Build for ARM64
######################################
if $(ContainsElement "arm64" ${ARCHS_TO_BUILD_FOR[@]});
then
    mkdir -p $ARM64_BUILD_DIR_OF_THE_LIB_BEING_BUILT
    mkdir -p $RESULTING_ARM64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT

    cd $ARM64_BUILD_DIR_OF_THE_LIB_BEING_BUILT
    cmake -DCMAKE_TOOLCHAIN_FILE=$ANDROID_CMAKE_DIR/android.toolchain.cmake  \
        -DANDROID_NDK=$ANDROID_NDK_HOME                                    \
        -DCMAKE_BUILD_TYPE="$BUILD_TYPE"                                   \
        -DANDROID_ABI="arm64-v8a"                                          \
        -DANDROID_NATIVE_API_LEVEL=android-21                              \
        -DANDROID_TOOLCHAIN_NAME=aarch64-linux-android-4.9                 \
        -DANDROID_STL=gnustl_shared                                        \
        -DLIBRARY_OUTPUT_PATH_ROOT=$ARM64_BUILD_DIR_OF_THE_LIB_BEING_BUILT \
        -DCMAKE_INSTALL_PREFIX=$RESULTING_ARM64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT            \
        $(ReplaceArchMarkersWithArchName "$EXTRA_CMAKE_FLAGS_FOR_THE_LIB_BEING_BUILT" "arm64") \
        $SOURCE_DIR_OF_THE_LIB_BEING_BUILT
    cmake --build $ARM64_BUILD_DIR_OF_THE_LIB_BEING_BUILT
    make install
fi

######################################
# Build for MIPS
######################################
if $(ContainsElement "mips" ${ARCHS_TO_BUILD_FOR[@]});
then
    mkdir -p $MIPS_BUILD_DIR_OF_THE_LIB_BEING_BUILT
    mkdir -p $RESULTING_MIPS_BINARIES_DIR_OF_THE_LIB_BEING_BUILT

    cd $MIPS_BUILD_DIR_OF_THE_LIB_BEING_BUILT
    cmake -DCMAKE_TOOLCHAIN_FILE=$ANDROID_CMAKE_DIR/android.toolchain.cmake \
        -DANDROID_NDK=$ANDROID_NDK_HOME                                   \
        -DCMAKE_BUILD_TYPE="$BUILD_TYPE"                                  \
        -DANDROID_ABI="mips"                                              \
        -DANDROID_NATIVE_API_LEVEL=android-18                             \
        -DANDROID_TOOLCHAIN_NAME=mipsel-linux-android-4.8                 \
        -DANDROID_STL=gnustl_shared                                       \
        -DLIBRARY_OUTPUT_PATH_ROOT=$MIPS_BUILD_DIR_OF_THE_LIB_BEING_BUILT \
        -DCMAKE_INSTALL_PREFIX=$RESULTING_MIPS_BINARIES_DIR_OF_THE_LIB_BEING_BUILT            \
        $(ReplaceArchMarkersWithArchName "$EXTRA_CMAKE_FLAGS_FOR_THE_LIB_BEING_BUILT" "mips") \
        $SOURCE_DIR_OF_THE_LIB_BEING_BUILT
    cmake --build $MIPS_BUILD_DIR_OF_THE_LIB_BEING_BUILT
    make install
fi

######################################
# Build for MIPS64
######################################
if $(ContainsElement "mips64" ${ARCHS_TO_BUILD_FOR[@]});
then
    mkdir -p $MIPS64_BUILD_DIR_OF_THE_LIB_BEING_BUILT
    mkdir -p $RESULTING_MIPS64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT

    cd $MIPS64_BUILD_DIR_OF_THE_LIB_BEING_BUILT
    cmake -DCMAKE_TOOLCHAIN_FILE=$ANDROID_CMAKE_DIR/android.toolchain.cmake   \
        -DANDROID_NDK=$ANDROID_NDK_HOME                                     \
        -DCMAKE_BUILD_TYPE="$BUILD_TYPE"                                    \
        -DANDROID_ABI="mips64"                                              \
        -DANDROID_NATIVE_API_LEVEL=android-21                               \
        -DANDROID_TOOLCHAIN_NAME=mips64el-linux-android-4.9                 \
        -DANDROID_STL=gnustl_shared                                         \
        -DLIBRARY_OUTPUT_PATH_ROOT=$MIPS64_BUILD_DIR_OF_THE_LIB_BEING_BUILT \
        -DCMAKE_INSTALL_PREFIX=$RESULTING_MIPS64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT            \
        $(ReplaceArchMarkersWithArchName "$EXTRA_CMAKE_FLAGS_FOR_THE_LIB_BEING_BUILT" "mips64") \
        $SOURCE_DIR_OF_THE_LIB_BEING_BUILT
    cmake --build $MIPS64_BUILD_DIR_OF_THE_LIB_BEING_BUILT
    make install
fi

######################################
# Build for x86
######################################
if $(ContainsElement "x86" ${ARCHS_TO_BUILD_FOR[@]});
then
    mkdir -p $X86_BUILD_DIR_OF_THE_LIB_BEING_BUILT
    mkdir -p $RESULTING_X86_BINARIES_DIR_OF_THE_LIB_BEING_BUILT

    cd $X86_BUILD_DIR_OF_THE_LIB_BEING_BUILT
    cmake -DCMAKE_TOOLCHAIN_FILE=$ANDROID_CMAKE_DIR/android.toolchain.cmake \
        -DANDROID_NDK=$ANDROID_NDK_HOME                                   \
        -DCMAKE_BUILD_TYPE="$BUILD_TYPE"                                  \
        -DANDROID_ABI="x86"                                               \
        -DANDROID_NATIVE_API_LEVEL=android-18                             \
        -DANDROID_TOOLCHAIN_NAME=x86-4.8                                  \
        -DANDROID_STL=gnustl_shared                                       \
        -DLIBRARY_OUTPUT_PATH_ROOT=$X86_BUILD_DIR_OF_THE_LIB_BEING_BUILT  \
        -DCMAKE_INSTALL_PREFIX=$RESULTING_X86_BINARIES_DIR_OF_THE_LIB_BEING_BUILT            \
        $(ReplaceArchMarkersWithArchName "$EXTRA_CMAKE_FLAGS_FOR_THE_LIB_BEING_BUILT" "x86") \
        $SOURCE_DIR_OF_THE_LIB_BEING_BUILT
    cmake --build $X86_BUILD_DIR_OF_THE_LIB_BEING_BUILT
    make install
fi

######################################
# Build for x86_64
######################################
if $(ContainsElement "x86_64" ${ARCHS_TO_BUILD_FOR[@]});
then
    mkdir -p $X86_64_BUILD_DIR_OF_THE_LIB_BEING_BUILT
    mkdir -p $RESULTING_X86_64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT

    cd $X86_64_BUILD_DIR_OF_THE_LIB_BEING_BUILT
    cmake -DCMAKE_TOOLCHAIN_FILE=$ANDROID_CMAKE_DIR/android.toolchain.cmake   \
        -DANDROID_NDK=$ANDROID_NDK_HOME                                     \
        -DCMAKE_BUILD_TYPE="$BUILD_TYPE"                                    \
        -DANDROID_ABI="x86_64"                                              \
        -DANDROID_NATIVE_API_LEVEL=android-21                               \
        -DANDROID_TOOLCHAIN_NAME=x86_64-4.9                                 \
        -DANDROID_STL=gnustl_shared                                         \
        -DLIBRARY_OUTPUT_PATH_ROOT=$X86_64_BUILD_DIR_OF_THE_LIB_BEING_BUILT \
        -DCMAKE_INSTALL_PREFIX=$RESULTING_X86_64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT            \
        $(ReplaceArchMarkersWithArchName "$EXTRA_CMAKE_FLAGS_FOR_THE_LIB_BEING_BUILT" "x86_64") \
        $SOURCE_DIR_OF_THE_LIB_BEING_BUILT
    cmake --build $X86_64_BUILD_DIR_OF_THE_LIB_BEING_BUILT
    make install
fi

SUCCESS=1

if [ "$SUCCESS" -eq 1 ] && \
   $(ContainsElement "armv7" ${ARCHS_TO_BUILD_FOR[@]}) && \
   ! $(CheckIfCompilationResultsExist $RESULTING_ARMV7_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/lib  "$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT");
then
    SUCCESS=0
    echo "Failed to build one or more of the \"$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT\" for armv7 Android ABI"
fi

if [ "$SUCCESS" -eq 1 ] && \
   $(ContainsElement "arm64" ${ARCHS_TO_BUILD_FOR[@]}) && \
   ! $(CheckIfCompilationResultsExist $RESULTING_ARM64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/lib  "$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT")
then
    SUCCESS=0
    echo "Failed to build one or more of the \"$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT\" for arm64 Android ABI"
fi

if [ "$SUCCESS" -eq 1 ] && \
   $(ContainsElement "mips" ${ARCHS_TO_BUILD_FOR[@]}) && \
   ! $(CheckIfCompilationResultsExist $RESULTING_MIPS_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/lib   "$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT")
then
    SUCCESS=0
    echo "Failed to build one or more of the \"$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT\" for mips Android ABI"
fi

if [ "$SUCCESS" -eq 1 ] && \
   $(ContainsElement "mips64" ${ARCHS_TO_BUILD_FOR[@]}) && \
   ! $(CheckIfCompilationResultsExist $RESULTING_MIPS64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/lib "$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT")
then
    SUCCESS=0
    echo "Failed to build one or more of the \"$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT\" for mips64 Android ABI"
fi

if [ "$SUCCESS" -eq 1 ] && \
   $(ContainsElement "x86" ${ARCHS_TO_BUILD_FOR[@]}) && \
   ! $(CheckIfCompilationResultsExist $RESULTING_X86_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/lib    "$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT")
then
    SUCCESS=0
    echo "Failed to build one or more of the \"$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT\" for x86 Android ABI"
fi

if [ "$SUCCESS" -eq 1 ] && \
   $(ContainsElement "x86_64" ${ARCHS_TO_BUILD_FOR[@]}) && \
   ! $(CheckIfCompilationResultsExist $RESULTING_X86_64_BINARIES_DIR_OF_THE_LIB_BEING_BUILT/lib "$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT")
then
    SUCCESS=0
    echo "Failed to build one or more of the \"$LIST_OF_THE_FILE_NAMES_OF_THE_LIB_BEING_BUILT\" for x86_64 Android ABI"
fi

if [ $PRESERVE_BUILD_FILES == NO ];
then
    rm -rf $ANDROID_BUILD_DIR_OF_THE_LIB_BEING_BUILT
fi

# Set script return code
if [ "$SUCCESS" -eq 1 ];
then
    exit 0 # Success
else
    exit 1 # Failure
fi
