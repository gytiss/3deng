#include "PhysicsCharacterController.h"

#include <CommonDefinitions.h>
#include <algorithm>


//-----------------------------------------------------------------------

PhysicsCharacterController::Capsule::Capsule(PhysicsManager& physicsManager,
                                             CompositeTransformNode* parentTransform,
                                             float height,
                                             float radius,
                                             float distanceFromTopToGround,
                                             float distFromBottomToGround)
: m_Height( height ),
  m_Radius( radius ),
  m_DistanceFromTopToGround( distanceFromTopToGround ),
  m_DistanceFromBottomToGround( distFromBottomToGround ),
  m_Offset( ( m_Height * 0.5f ) + distFromBottomToGround ),
  m_FootDistance( m_Offset + distFromBottomToGround ),
  m_Top( 0.0f ),
  m_CollisionShape( new btCapsuleShape( m_Radius, m_Height ) ),
  m_GhostObject( new btPairCachingGhostObject() ),
  m_Transform( new CompositeTransformNode() ),
  m_PhysicsManager( physicsManager ),
  m_InPhysicsWorld( true )
{
    m_Transform->AddParent( parentTransform );

    btTransform startTransform;
    m_Transform->getWorldTransform( startTransform );

    m_GhostObject->setWorldTransform( startTransform );
	m_GhostObject->setCollisionShape( m_CollisionShape );
	m_GhostObject->setCollisionFlags( btCollisionObject::CF_CHARACTER_OBJECT );

	// Only collide with static for now (no interaction with dynamic objects)
	m_PhysicsManager.m_DynamicsWorld->addCollisionObject( m_GhostObject,
                                                          btBroadphaseProxy::CharacterFilter,
                                                          btBroadphaseProxy::StaticFilter | btBroadphaseProxy::DefaultFilter );
}

//-----------------------------------------------------------------------

PhysicsCharacterController::Capsule::~Capsule()
{
    RemoveFromThePhysicsWorld();

    delete m_GhostObject;
}

//-----------------------------------------------------------------------

PhysicsCharacterController::PhysicsCharacterController(PhysicsManager& physicsManager,
                                                       float walkHeight,
                                                       float crouchHeight,
                                                       float stepHeight,
                                                       float radius)
: m_PhysicsManager( physicsManager ),
  m_WalkHeight( walkHeight ),
  m_CrouchHeight( crouchHeight ),
  m_StepHeight( stepHeight ),
  m_Radius( radius ),
  m_Gravity( physicsManager.m_DynamicsWorld->getGravity().y() ),
  m_TimeStep( 0.0f ),
  m_CurrentPos( glm::vec3( 0.0f ) ),
  m_CurrentCapsule( nullptr ),
  m_WalkingCapsule( nullptr ),
  m_CrouchingCapsule( nullptr ),
  m_MovementParentCompositeTransform( new CompositeTransformNode() ),
  m_MinSlopeDot( 0.0f ),
  m_SlopeAffectsSpeed( false ),
  m_IntelligentJump( false ),
  m_MovementState( e_MovementStateGround ),
  m_MovementStateFilter(),

  // Prevent the KCC from moving when there's not enough room for it in the next frame
  // It doesn't work right now because I can't figure out how to add sliding. Sweep test could work,
  // but it would cause problems with penetration testing and steps
  // That said, you probably won't need it, if you design your levels correctly
  m_PredictFutureSpace( false ),
  m_FutureSpacePredictionDistance( 10.0f ),

  m_IsCrouching( false ),
  m_FallTime( 0.0f ),
  m_FallStartPos( m_CurrentPos.y ),
  m_FallDelta( 0.0f ),
  m_LinearVelocity( glm::vec3( 0.0f, 0.0f, 0.0f ) ),
  m_HeadContact(),
  m_FootContact(),
  m_EnabledCrouch( false ),
  m_StandUpCallback( nullptr ),
  m_StandUpCallbackData( nullptr ),
  m_FallCallback( nullptr ),
  m_FallCallbackData( nullptr ),

  m_JumpStartPos( 0.0f ),
  m_JumpTime( 0.0f ),
  m_JumpSpeed( 0.0f ),
  m_JumpMaxHeight( 0.0f )
{
    _Setup();

    SetMaxSlope( 50.0f, true );
    SetActiveJumpLimiter( true );

    // Setup transitions available from each state
    m_MovementStateFilter[e_MovementStateGround].push_back( e_MovementStateGround );
    m_MovementStateFilter[e_MovementStateGround].push_back( e_MovementStateJumping );
    m_MovementStateFilter[e_MovementStateGround].push_back( e_MovementStateFalling );

    m_MovementStateFilter[e_MovementStateJumping].push_back( e_MovementStateGround );
    m_MovementStateFilter[e_MovementStateJumping].push_back( e_MovementStateFalling );

    m_MovementStateFilter[e_MovementStateFalling].push_back( e_MovementStateGround );

    m_HeadContact.isValid = false;
    m_HeadContact.hitNode = nullptr;
    m_HeadContact.hitPos = btVector3( 0.0f, 0.0f, 0.0f );
    m_FootContact.isValid = false;
    m_FootContact.hitNode = nullptr;
    m_FootContact.hitPos = btVector3( 0.0f, 0.0f, 0.0f );
    m_FootContact.hitNormal = btVector3( 0.0f, 0.0f, 0.0f );
}

//-----------------------------------------------------------------------

PhysicsCharacterController::~PhysicsCharacterController()
{
  m_CurrentCapsule = nullptr;
  delete m_WalkingCapsule;
  delete m_CrouchingCapsule;
  delete m_MovementParentCompositeTransform;
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::SetCollideMask(uint32_t UNUSED_PARAM(args))
{
// TODO (Needs implementing)
//        self.__walkCapsuleNP.setCollideMask(*args)
//        self.__crouchCapsuleNP.setCollideMask(*args)
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::SetFallCallback(Callback callback, void* data)
{
    m_FallCallback = callback;
    m_FallCallbackData = data;
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::SetStandUpCallback(Callback callback, void* data)
{
    m_StandUpCallback = callback;
    m_StandUpCallbackData = data;
}

//-----------------------------------------------------------------------

const glm::vec3& PhysicsCharacterController::GetPos() const
{
    return m_MovementParentCompositeTransform->GetPosition();
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::SetQuat(glm::quat& rot)
{
    m_MovementParentCompositeTransform->SetRotation( rot );
}

//-----------------------------------------------------------------------

const glm::quat& PhysicsCharacterController::GetQuat() const
{
    return m_MovementParentCompositeTransform->GetRotation();
}

//-----------------------------------------------------------------------

float PhysicsCharacterController::GetTop() const
{
    return m_CurrentCapsule->GetTop();
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::SetMaxSlope(float degs, bool affectSpeed)
{
    if( degs == 0.0f )
    {
        m_MinSlopeDot = 0.0f;
        return;
    }

    m_MinSlopeDot = MathUtils::Round( cos( MathUtils::DegreesToRadians( degs ) ), 2 );

    m_SlopeAffectsSpeed = affectSpeed;
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::SetActiveJumpLimiter(bool val)
{
    m_IntelligentJump = val;
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::StartCrouch()
{
    if( !m_IsCrouching && !m_EnabledCrouch )
    {
        m_IsCrouching = true;
        m_EnabledCrouch = true;

        m_CurrentCapsule = m_CrouchingCapsule;

        m_WalkingCapsule->RemoveFromThePhysicsWorld();
        m_CrouchingCapsule->AddToThePhysicsWorld();
    }
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::StopCrouch()
{
    m_EnabledCrouch = false;
}

//-----------------------------------------------------------------------

bool PhysicsCharacterController::IsOnGround()
{
    if( !m_FootContact.isValid )
    {
        return false;
    }
    else if( m_MovementState == e_MovementStateGround )
    {
        float elevation = m_CurrentPos.y - m_FootContact.hitPos.y();
        return ( elevation <= ( m_CurrentCapsule->GetLevitation() + 0.02f ) );
    }
    else
    {
        return ( ( m_CurrentPos.x <= m_FootContact.hitPos.x() ) &&
                  ( m_CurrentPos.y <= m_FootContact.hitPos.y() ) &&
                  ( m_CurrentPos.z <= m_FootContact.hitPos.z() ) );
    }
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::StartJump(float maxHeight)
{
    _Jump( maxHeight );
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::StartFly()
{
    m_MovementState = e_MovementStateFlying;
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::StopFly()
{
    _Fall();
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::SetAngularMovement(float omegaInDegrees)
{
    m_MovementParentCompositeTransform->SetRotation( glm::angleAxis( MathUtils::DegreesToRadians( omegaInDegrees ), glm::vec3( 0.0f, 1.0f, 0.0f ) ) );
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::SetLinearMovement(const glm::vec3& speed)
{
    m_LinearVelocity = speed;
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::_Update(float deltaTime)
{
    m_TimeStep = deltaTime;

    _UpdateFootContact();
    _UpdateHeadContact( m_CurrentCapsule->GetHeight() * 20.0f );

    switch( m_MovementState )
    {
        case e_MovementStateGround:
        {
            _ProcessGround();
            break;
        }

        case e_MovementStateJumping:
        {
            _ProcessJumping();
            break;
        }

        case e_MovementStateFalling:
        {
            _ProcessFalling();
            break;
        }

        case e_MovementStateFlying:
        {
            _ProcessFlying();
            break;
        }

        case e_MovementStateCount:
        default:
        {
            assert(0 && "Unknown movement state");
        }
    }

    _ApplyLinearVelocity();
    _PreventPenetration();

    if( m_IsCrouching && !m_EnabledCrouch )
    {
        _StandUp();
    }
}

//-----------------------------------------------------------------------

bool PhysicsCharacterController::_CanTransitionFromStateToState(MovementState fromState, MovementState toState)
{
    const std::vector<MovementState>& stateArray = m_MovementStateFilter[fromState];
    return ( find (stateArray.begin(), stateArray.end(), toState) != stateArray.end() );
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::_Land()
{
    m_MovementState = e_MovementStateGround;
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::_Fall()
{
    m_MovementState = e_MovementStateFalling;

    m_FallStartPos = m_CurrentPos.y;
    m_FallDelta = 0.0f;
    m_FallTime = 0.0f;
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::_Jump(float maxY)
{
    if( !_CanTransitionFromStateToState( m_MovementState, e_MovementStateJumping ) )
    {
        return;
    }

    maxY += m_CurrentPos.y;
    _UpdateHeadContact( maxY );

    if( m_IntelligentJump && m_HeadContact.isValid && m_HeadContact.hitPos.y() < maxY + m_CurrentCapsule->GetDistanceFromTopToGround() )
    {
        maxY = m_HeadContact.hitPos.y() - m_CurrentCapsule->GetDistanceFromTopToGround() * 1.2f;
    }

    maxY = MathUtils::Round( maxY, 2 );

    m_JumpStartPos = m_CurrentPos.y;
    m_JumpTime = 0.0f;

    float bsq = -4.0f * m_Gravity * (maxY - m_JumpStartPos);
    if( bsq >= 0.0f )
    {
        float b = sqrt( bsq );
        m_JumpSpeed = b;
        m_JumpMaxHeight = maxY;
        m_MovementState = e_MovementStateJumping;
    }
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::_StandUp()
{
    _UpdateHeadContact( m_CurrentCapsule->GetHeight() * 20.0f );

    if( m_HeadContact.isValid && m_CurrentPos.y + m_WalkingCapsule->GetLevitation() + m_WalkingCapsule->GetHeight() >= m_HeadContact.hitPos.y() )
    {
        return;
    }

    m_IsCrouching = false;
    m_CurrentCapsule = m_WalkingCapsule;

    m_CrouchingCapsule->RemoveFromThePhysicsWorld();
    m_WalkingCapsule->AddToThePhysicsWorld();

    if( m_StandUpCallback )
    {
        m_StandUpCallback( m_StandUpCallbackData );
    }
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::_ProcessGround()
{
    if( !IsOnGround() )
    {
        _Fall();
    }
    else
    {
        m_CurrentPos.y = m_FootContact.hitPos.y();
    }
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::_ProcessFalling()
{
    m_FallTime += m_TimeStep;
    m_FallDelta = m_Gravity * pow( m_FallTime, 2.0f );

    glm::vec3 newPos = m_CurrentPos;
    newPos.y = m_FallStartPos + m_FallDelta;

    m_CurrentPos = newPos;

    if( IsOnGround() )
    {
        _Land();

        if( m_FallCallback )
        {
            m_FallCallback( m_FallCallbackData );
        }
    }
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::_ProcessJumping()
{
    if( m_HeadContact.isValid && m_CurrentCapsule->GetTop() >= m_HeadContact.hitPos.y() )
    {
// TODO Output warning maybe?
//
//            # This shouldn't happen, but just in case, if we hit the ceiling, we start to fall
//            print "Head bang"
        _Fall();
        return;
    }

    m_JumpTime += m_TimeStep;

    m_CurrentPos.y = ( m_Gravity * pow( m_JumpTime, 2.0f ) ) + ( m_JumpSpeed * m_JumpTime ) + m_JumpStartPos;

    // Adjust factor is needed to prevent the below condition
    // from not being satisfied due to floating point errors
    static const float c_AdjustFactor = 0.1f;
    if( MathUtils::Round( m_CurrentPos.y, 2 ) >= ( m_JumpMaxHeight - c_AdjustFactor ) )
    {
        _Fall();
    }
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::_ProcessFlying()
{
    if( m_FootContact.isValid && m_CurrentPos.y - 0.1f < m_FootContact.hitPos.y() && m_LinearVelocity.y < 0.0f )
    {
        m_CurrentPos.y = m_FootContact.hitPos.y();
        m_LinearVelocity.y = 0.0f;
    }

    if( m_HeadContact.isValid && m_CurrentCapsule->GetTop() >= m_HeadContact.hitPos.y() && m_LinearVelocity.y > 0.0f )
    {
        m_LinearVelocity.y = 0.0f;
    }
}

//-----------------------------------------------------------------------

BulletClosestHitRayResult PhysicsCharacterController::ray_test_closest(const btVector3 &from_pos,
                                                                       const btVector3 &to_pos,
                                                                       const CollideMask &mask) const
{
    const btVector3 from = from_pos;
    const btVector3 to = to_pos;

    BulletClosestHitRayResult cb( from, to, mask );
    m_PhysicsManager.m_DynamicsWorld->rayTest( from, to, cb );
    return cb;
}

//-----------------------------------------------------------------------

float PhysicsCharacterController::get_mass(const btRigidBody* rigidBody) const
{
    btScalar inv_mass = rigidBody->getInvMass();
    btScalar mass = inv_mass == btScalar(0.0) ? btScalar(0.0) : btScalar(1.0) / inv_mass;

    return mass;
}

//-----------------------------------------------------------------------

bool PhysicsCharacterController::_CheckFutureSpace(btVector3 globalVel)
{
    globalVel = globalVel * m_FutureSpacePredictionDistance;

    const btVector3 pFrom =  m_CurrentCapsule->GetGhostObject()->getWorldTransform().getOrigin();
    const btVector3 pUp = ( pFrom + btVector3( 0.0f, m_CurrentCapsule->GetHeight() * 2.0f, 0.0f ) ) ;
    const btVector3 pDown = ( pFrom - btVector3( 0.0f, m_CurrentCapsule->GetHeight() * 2.0f + m_CurrentCapsule->GetLevitation(), 0.0f ) );

    BulletClosestHitRayResult upTest = ray_test_closest( pFrom, pUp );
    BulletClosestHitRayResult downTest = ray_test_closest( pFrom, pDown );

    if( !( upTest.has_hit() && downTest.has_hit() ) )
    {
        return true;
    }


    if( upTest.get_node() )
    {
        const btRigidBody* upNode = btRigidBody::upcast( upTest.get_node() );
        if( get_mass( upNode ) )
        {
            return true;
        }
    }

    float space = abs( upTest.get_hit_pos().y() - downTest.get_hit_pos().y() );

    if( space < m_CurrentCapsule->GetLevitation() + m_CurrentCapsule->GetHeight() + m_CurrentCapsule->GetRadius() )
    {
        return false;
    }

    return true;
}

//-----------------------------------------------------------------------

BulletAllHitsRayResult PhysicsCharacterController::ray_test_all(const btVector3 &from_pos,
                                                                const btVector3 &to_pos,
                                                                const CollideMask &mask) const
{
    const btVector3 from = from_pos;
    const btVector3 to = to_pos;

    BulletAllHitsRayResult cb( from, to, mask );
    m_PhysicsManager.m_DynamicsWorld->rayTest( from, to, cb );

    return cb;
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::_UpdateFootContact()
{
    btTransform rigidBodyTransform;
    rigidBodyTransform = m_CurrentCapsule->GetGhostObject()->getWorldTransform();

    btVector3 pFrom = rigidBodyTransform.getOrigin();
    btVector3 pTo = ( pFrom - btVector3( 0.0f, m_CurrentCapsule->GetFootDistance(), 0.0f ) );

    BulletAllHitsRayResult result = ray_test_all(pFrom, pTo);

    if( !result.has_hits() )
    {
        m_FootContact.isValid = false;
        return;
    }

    std::vector<BulletRayHit> hits;
    result.get_hits( hits );

    HitsComparator comparator;
    comparator.pFrom = pFrom;
    std::sort(hits.begin(), hits.end(), comparator);

    for(size_t i = 0; i < hits.size(); i++)
    {
        BulletRayHit& hit = hits[i];
        if( hit.get_node() && btGhostObject::upcast( hit.get_node() ) )
        {
            continue;
        }
        else
        {
            m_FootContact.isValid = true;
            m_FootContact.hitPos = hit.get_hit_pos();
            m_FootContact.hitNormal = hit.get_hit_normal();
            m_FootContact.hitNode = hit.get_node();
            break;
        }
    }
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::_UpdateHeadContact(float testHeightLimit)
{
    btVector3 pFrom = m_CurrentCapsule->GetGhostObject()->getWorldTransform().getOrigin();
    btVector3 pTo = ( pFrom + btVector3( 0.0f, testHeightLimit, 0.0f ) );

    BulletAllHitsRayResult result = ray_test_all(pFrom, pTo);


    if( !result.has_hits() )
    {
        m_HeadContact.isValid = false;
        return;
    }

    std::vector<BulletRayHit> hits;
    result.get_hits( hits );

    HitsComparator comparator;
    comparator.pFrom = pFrom;
    std::sort(hits.begin(), hits.end(), comparator);

    for(size_t i = 0; i < hits.size(); i++)
    {
        BulletRayHit& hit = hits[i];
        if( hit.get_node() && btGhostObject::upcast( hit.get_node() ) )
        {
            continue;
        }
        else
        {
            m_HeadContact.isValid = true;
            m_HeadContact.hitPos = hit.get_hit_pos();
            m_HeadContact.hitNode = hit.get_node();
            break;
        }
    }
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::_UpdateCapsule()
{
    m_MovementParentCompositeTransform->SetPosition( m_CurrentPos );
    m_CurrentCapsule->UpdateCapsule( m_CurrentPos );
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::_ApplyGravity(glm::vec3 floorNormal)
{
    m_CurrentPos -= glm::vec3( floorNormal.x, 0.0f, floorNormal.z ) * m_Gravity * m_TimeStep * 0.1f;
}

//-----------------------------------------------------------------------

float PhysicsCharacterController::AngleBetweenTwoNormalVectorsInDegress(const glm::vec3& vec1, glm::vec3& vec2)
{
    return MathUtils::AngleBetweenTwoVectors( vec1, vec2 );
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::_ApplyLinearVelocity()
{
    glm::vec3 globalVel = glm::rotate( m_MovementParentCompositeTransform->GetRotation(), m_LinearVelocity ) * m_TimeStep;

    if( m_PredictFutureSpace && !_CheckFutureSpace( btVector3( globalVel.x, globalVel.y, globalVel.z ) ) )
    {
        return;
    }

    if( m_FootContact.isValid &&
        ( m_MinSlopeDot != 0.0f ) &&
        ( m_MovementState != e_MovementStateFlying ) &&
        ( globalVel != glm::vec3( 0.0f ) ) )
    {
        glm::vec3 normalVel = globalVel;
        normalVel = glm::normalize( normalVel );

        glm::vec3 floorNormal = glm::vec3( m_FootContact.hitNormal.x(), m_FootContact.hitNormal.y(), m_FootContact.hitNormal.z() );
        glm::vec3 c_yUpVector = glm::vec3( 0.0f, 1.0f, 0.0f );
        float absSlopeDot = MathUtils::Round( glm::dot( floorNormal, c_yUpVector ), 2 );

        if( absSlopeDot <= m_MinSlopeDot )
        {
            _ApplyGravity( floorNormal );

            if( ( globalVel.x > 0.0f ) && (globalVel.y > 0.0f) && ( globalVel.z > 0.0f ) )
            {
                glm::vec3 globalVelDir = glm::vec3( globalVel );
                globalVelDir = glm::normalize( globalVelDir );

                glm::vec3 fn = glm::vec3( floorNormal.x, 0.0f, floorNormal.z );
                fn = glm::normalize( fn );

                float velDot = 1.0f - AngleBetweenTwoNormalVectorsInDegress( globalVelDir, fn ) / 180.0f;
                if( velDot < 0.5f )
                {
                    m_CurrentPos -= glm::vec3( fn.x * globalVel.x, 0.0f, fn.z * globalVel.z ) * velDot;
                }

                globalVel *= velDot;
            }
        }
        else if( m_SlopeAffectsSpeed && ( ( globalVel.x > 0.0f ) && (globalVel.y > 0.0f) && ( globalVel.z > 0.0f ) ) )
        {
            _ApplyGravity( floorNormal );
        }
    }

    m_CurrentPos += globalVel;

    _UpdateCapsule();
}

//-----------------------------------------------------------------------

BulletContactResult PhysicsCharacterController::contact_test(btCollisionObject* bulletNode)
{
    BulletContactResult cb;

    if( bulletNode )
    {
        m_PhysicsManager.m_DynamicsWorld->contactTest( bulletNode, cb );
    }

    return cb;
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::_PreventPenetration()
{
//    glm::vec3 collisions = glm::vec3( 0.0f, 0.0f, 0.0f );
//
//    BulletContactResult result = contact_test( m_CurrentCapsule->GetRigidBody() );
//
//    std::vector<BulletContact> contacts;
//    result.get_contacts( contacts );
//
//    for(size_t i = 0; i < contacts.size(); i++)
//    {
//        BulletContact& contact = contacts[i];
//        if( contact.get_node1() && btGhostObject::upcast( contact.get_node1() ) )
//        {
//            continue;
//        }
//        else
//        {
//            const BulletManifoldPoint* mpoint = contact.get_manifold_point();
//            btVector3 normal = mpoint->get_position_world_on_b() - mpoint->get_position_world_on_a();
//            //normal.safeNormalize();
//
//            if( mpoint->get_distance() < 0.0f )
//            {
//                collisions -= glm::vec3( normal.x(), normal.y(), normal.z() ) * mpoint->get_distance();
//            }
//
//            delete mpoint;
//        }
//    }
//
//    collisions.y = 0.0f;
//    m_CurrentPos += collisions;

	// Here we must refresh the overlapping paircache as the penetrating movement itself or the
	// previous recovery iteration might have used setWorldTransform and pushed us into an object
	// that is not in the previous cache contents from the last timestep, as will happen if we
	// are pushed into a new AABB overlap. Unhandled this means the next convex sweep gets stuck.
	//
	// Do this by calling the broadphase's setAabb with the moved AABB, this will update the broadphase
	// paircache and the ghostobject's internal paircache at the same time.    /BW

	const glm::vec3 initialPos = m_CurrentPos;

    bool penetration = true;
    static const int c_MaxRecoveryAttemps = 4;

    for(int k = 0; (k < c_MaxRecoveryAttemps) && penetration; k++)
    {
        penetration = false;

        btVector3 minAabb;
        btVector3 maxAabb;

        btPairCachingGhostObject* currentGhostObject = m_CurrentCapsule->GetGhostObject();

        m_CurrentCapsule->GetCollisionShape()->getAabb( currentGhostObject->getWorldTransform(), minAabb, maxAabb );
        m_PhysicsManager.m_DynamicsWorld->getBroadphase()->setAabb( currentGhostObject->getBroadphaseHandle(),
                                                                    minAabb,
                                                                    maxAabb,
                                                                    m_PhysicsManager.m_DynamicsWorld->getDispatcher() );

        m_PhysicsManager.m_DynamicsWorld->getDispatcher()->dispatchAllCollisionPairs( currentGhostObject->getOverlappingPairCache(),
                                                                                      m_PhysicsManager.m_DynamicsWorld->getDispatchInfo(),
                                                                                      m_PhysicsManager.m_DynamicsWorld->getDispatcher() );

        btVector3 currentPosition/*(m_CurrentPos.x,m_CurrentPos.y,m_CurrentPos.z);*/ = currentGhostObject->getWorldTransform().getOrigin();
        btVector3 touchingNormal( 0.0f, 0.0f, 0.0f );
        btManifoldArray	manifoldArray;

        btScalar maxPen = btScalar( 0.0f );
        for(int i = 0; i < currentGhostObject->getOverlappingPairCache()->getNumOverlappingPairs(); i++)
        {
            manifoldArray.resize( 0 );

            btBroadphasePair* collisionPair = &currentGhostObject->getOverlappingPairCache()->getOverlappingPairArray()[i];

            if( collisionPair->m_algorithm )
            {
                collisionPair->m_algorithm->getAllContactManifolds( manifoldArray );
            }

            for(int j = 0; j < manifoldArray.size() ; j++)
            {
                btPersistentManifold* manifold = manifoldArray[j];
                //if( ( manifold->getBody0()->getCollisionFlags() & btCollisionObject::CF_STATIC_OBJECT ) ||
                //    ( manifold->getBody1()->getCollisionFlags() & btCollisionObject::CF_STATIC_OBJECT ) )
                {
                    btScalar directionSign = ( ( manifold->getBody0() == currentGhostObject ) ? btScalar( -1.0f ) : btScalar( 1.0f ) );

                    for(int p = 0; p < manifold->getNumContacts(); p++)
                    {
                        const btManifoldPoint&pt = manifold->getContactPoint( p );

                        btScalar dist = pt.getDistance();

                        if( dist < 0.0f )
                        {
                            if( dist < maxPen )
                            {
                                maxPen = dist;
                                touchingNormal = pt.m_normalWorldOnB * directionSign;//??
                            }

                            currentPosition += pt.m_normalWorldOnB * directionSign * dist * btScalar( 0.05f );
                            penetration = true;
                        }
                        else
                        {
                            //printf("touching %f\n", dist);
                        }
                    }
                }

                //manifold->clearManifold();
            }
        }

        m_CurrentPos.x = currentPosition.getX();
        m_CurrentPos.z = currentPosition.getZ();

        if( penetration )
        {
            _UpdateCapsule();
        }
    }

    m_CurrentPos.x = MathUtils::Round( m_CurrentPos.x, 2 );
    m_CurrentPos.z = MathUtils::Round( m_CurrentPos.z, 2 );

    if( fabs( initialPos.x - m_CurrentPos.x ) <= 0.01f )
    {
        m_CurrentPos.x = MathUtils::Round( initialPos.x, 2);
    }

    if( fabs( initialPos.z - m_CurrentPos.z ) <= 0.01f )
    {
        m_CurrentPos.z = MathUtils::Round( initialPos.z, 2);
    }

    _UpdateCapsule();
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::SetPos(const glm::vec3& pos)
{
    m_MovementParentCompositeTransform->SetPosition( pos );
    m_CurrentPos = m_MovementParentCompositeTransform->GetPosition();
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::SetX(float val)
{
    glm::vec3 pos = m_MovementParentCompositeTransform->GetPosition();
    pos.x = val;
    m_MovementParentCompositeTransform->SetPosition( pos );
    m_CurrentPos = pos;
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::SetY(float val)
{
    glm::vec3 pos = m_MovementParentCompositeTransform->GetPosition();
    pos.y = val;
    m_MovementParentCompositeTransform->SetPosition( pos );
    m_CurrentPos = pos;
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::SetZ(float val)
{
    glm::vec3 pos = m_MovementParentCompositeTransform->GetPosition();
    pos.z = val;
    m_MovementParentCompositeTransform->SetPosition( pos );
    m_CurrentPos = pos;
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::_SetData(float fullHeight,
                                          float stepHeight,
                                          float radius,
                                          float& outHeight,
                                          float& outLevitation,
                                          float& outRadius)
{
    if( ( fullHeight - stepHeight ) <= ( radius * 2.0f ) )
    {
        outHeight = 0.1f;
        outRadius = ( fullHeight * 0.5f ) - ( stepHeight * 0.5f );
        outLevitation = ( stepHeight + outRadius );
    }
    else
    {
        outHeight = ( fullHeight - stepHeight - ( radius * 2.0f ) );
        outLevitation = ( fullHeight - radius - ( outHeight / 2.0f ) );
        outRadius = radius;
    }
}

//-----------------------------------------------------------------------

void PhysicsCharacterController::_Setup()
{
    float walkCapsuleHeight = 0.0f;
    float walkLevitation = 0.0f;
    float walkCapsuleRadius = 0.0f;
    float crouchCapsuleHeight = 0.0f;
    float crouchLevitation = 0.0f;
    float crouchCapsuleRadius = 0.0f;
    _SetData( m_WalkHeight, m_StepHeight, m_Radius, walkCapsuleHeight, walkLevitation, walkCapsuleRadius );
    _SetData( m_CrouchHeight, m_StepHeight, m_Radius, crouchCapsuleHeight, crouchLevitation, crouchCapsuleRadius );

    m_MovementParentCompositeTransform->SetPosition( m_CurrentPos );

    m_WalkingCapsule = new Capsule( m_PhysicsManager,
                                    m_MovementParentCompositeTransform,
                                    walkCapsuleHeight,
                                    walkCapsuleRadius,
                                    m_WalkHeight,
                                    walkLevitation );

    m_CrouchingCapsule = new Capsule( m_PhysicsManager,
                                      m_MovementParentCompositeTransform,
                                      crouchCapsuleHeight,
                                      crouchCapsuleRadius,
                                      m_CrouchHeight,
                                      crouchLevitation );

    m_CrouchingCapsule->RemoveFromThePhysicsWorld();

    m_CurrentCapsule = m_WalkingCapsule;

    // Init
    _UpdateCapsule();
}

//-----------------------------------------------------------------------
