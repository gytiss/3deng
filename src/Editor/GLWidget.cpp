#include "GLWidget.h"

#include "EditorMainLoop.h"

#ifdef OS_WINDOWS
    #include <windows.h>
#elif defined( OS_APPLE )
    #include <Carbon/Carbon.h>
#elif defined( OS_GENERIC_UNIX )
    #include <X11/keysym.h>
#endif

#include <QtOpenGL/QGLContext>
#include <QtGui/QKeyEvent>
#include <QtGui/QCursor>

#include <MsgLogger.h>
#include <Exceptions/GenericException.h>
#include <StringUtils.h>
#include <MathUtils.h>
#include <cassert>

//-----------------------------------------------------------------------

GLWidget::GLWidget(MainWindow& mainWindow,
                   InputMapping::InputMapper& inputMapper,
                   FileManager& fileManager,
                   QGLContext* glContext,
                   QWidget* parent)
: QGLWidget( glContext, parent ),
  SystemInterface(),
  m_InputMapper( inputMapper ),
  m_FileManager( fileManager ),
  m_MainKeyCodeMap(),
  m_ElapsedTimer(),
  m_DrawableSurfaceSize( 0, 0 ),
  m_PreviousAbsoluteMousePosX( 0 ),
  m_PreviousAbsoluteMousePosY( 0 ),
  m_MouseCapturedInTheWIndow( false ),
  m_MainWindow( mainWindow ),
  m_EditorMainLoop( nullptr )
{
    _InitializeMainKeyCodeMap( m_MainKeyCodeMap );
    _InitializeKeyPadKeyCodeMap( m_KeyPadKeyCodeMap );

    m_InputMapper.AddCallback( MainInputCallback, 0, this );

    // Make sure we always receive mouse move events
    setMouseTracking( true );
}

//-----------------------------------------------------------------------

GLWidget::~GLWidget()
{
    SafeDelete( m_EditorMainLoop );
}

//-----------------------------------------------------------------------

void GLWidget::initializeGL()
{
    assert( ( m_EditorMainLoop == nullptr ) && "Initialization can only be performed once" );

    const QGLContext* glContext = context();

    if( glContext->isValid() )
    {
        if( _CheckIfContextSupportsOpenGLVersion( glContext, 4, 3 ) )
        {
            makeCurrent();

            _LogOpenGLContextDetails( glContext );

#ifndef OPENGL_ES
            /*------------------Setup GLEW-----------------------------*/
            glewExperimental = GL_TRUE; // Enables experimental GLEW features such as glGenVertexArrays etc.

            const GLenum initStatus = glewInit(); // Initialize GLEW

            if( initStatus != GLEW_OK ) // Check if GLEW was initialized correctly
            {
                throw GenericException( "GLEW initialization failed with the following message: " + std::string( reinterpret_cast<const char*>( glewGetErrorString( initStatus ) ) ) );
            }

            if( !GL_VERSION_4_3 ) // Check if the required OpenGL version is supported
            {
                throw GenericException( "This program requires OpenGL 4.3 or higher, but GLEW does not support it" );
            }
            /*---------------------------------------------------------*/

            if( !m_ElapsedTimer.isMonotonic() )
            {
                throw GenericException( "QElapsedTimer is not monotonic" );
            }
#endif

            m_ElapsedTimer.start();

            m_DrawableSurfaceSize.widthInPixels = width();
            m_DrawableSurfaceSize.heightInPixels = height();

            m_EditorMainLoop = new EditorMainLoop( m_MainWindow, m_InputMapper, *this, m_FileManager );
        }
        else
        {
            throw GenericException( "OpenGL context does not support OpenGL version 4.3 or higher" );
        }
    }
    else
    {
        throw GenericException( "Failed to get a valid OpenGL context" );
    }
}

//-----------------------------------------------------------------------

void GLWidget::paintGL()
{
    if( m_EditorMainLoop )
    {
        m_EditorMainLoop->Update();
    }
}

//-----------------------------------------------------------------------

void GLWidget::resizeGL(int width, int height)
{
    if( ( m_DrawableSurfaceSize.widthInPixels != static_cast<size_t>( width ) ) ||
        ( m_DrawableSurfaceSize.heightInPixels != static_cast<size_t>( height ) ) )
    {
        m_DrawableSurfaceSize.widthInPixels = width;
        m_DrawableSurfaceSize.heightInPixels = height;

        _DrawableSurfaceSizeChanged( m_DrawableSurfaceSize );
    }
}

//-----------------------------------------------------------------------

void GLWidget::keyPressEvent(QKeyEvent* event)
{
    event->accept();

    InputMapping::RawInputButton rawInputButton = InputMapping::e_RawInputButtonInvalid;

    if( _MapKeyboardEventToRawInputButton( event, rawInputButton ) )
    {
        m_InputMapper.SetRawButtonState( rawInputButton, true );
    }
}

//-----------------------------------------------------------------------

void GLWidget::keyReleaseEvent(QKeyEvent* event)
{
    event->accept();

    InputMapping::RawInputButton rawInputButton = InputMapping::e_RawInputButtonInvalid;

    if( _MapKeyboardEventToRawInputButton( event, rawInputButton ) )
    {
        m_InputMapper.SetRawButtonState( rawInputButton, false );
    }
}

//-----------------------------------------------------------------------

void GLWidget::mousePressEvent(QMouseEvent* event)
{
    event->accept();

    const Qt::MouseButton mouseButton = event->button();

    if( mouseButton == Qt::LeftButton )
    {
        m_InputMapper.SetRawButtonState( InputMapping::e_RawInputButtonMouseLeft, true );
    }
    else if( mouseButton == Qt::RightButton )
    {
        m_InputMapper.SetRawButtonState( InputMapping::e_RawInputButtonMouseRight, true );
    }
    else if( mouseButton == Qt::MiddleButton )
    {
        m_InputMapper.SetRawButtonState( InputMapping::e_RawInputButtonMouseMiddle, true );
    }
}

//-----------------------------------------------------------------------

void GLWidget::mouseReleaseEvent(QMouseEvent* event)
{
    event->accept();

    // Focus mouse and keyboard input on this window
    setFocus( Qt::MouseFocusReason );

    const Qt::MouseButton mouseButton = event->button();

    if( mouseButton == Qt::LeftButton )
    {
        m_InputMapper.SetRawButtonState( InputMapping::e_RawInputButtonMouseLeft, false );
    }
    else if( mouseButton == Qt::RightButton )
    {
        m_InputMapper.SetRawButtonState( InputMapping::e_RawInputButtonMouseRight, false );
    }
    else if( mouseButton == Qt::MiddleButton )
    {
        m_InputMapper.SetRawButtonState( InputMapping::e_RawInputButtonMouseMiddle, false );
    }
}

//-----------------------------------------------------------------------

void GLWidget::mouseMoveEvent(QMouseEvent* event)
{
    event->accept();

    QPoint globalMousePos = event->pos();
    const int absoluteMousePosX = globalMousePos.x();
    const int absoluteMousePosY = globalMousePos.y();

    if( m_MouseCapturedInTheWIndow )
    {
        // In relative mode previous absolute values are not needed so set them to initial values
        m_PreviousAbsoluteMousePosX = 0;
        m_PreviousAbsoluteMousePosY = 0;

        const int screenCenterX = ( m_DrawableSurfaceSize.widthInPixels / 2 );
        const int screenCenterY = ( m_DrawableSurfaceSize.heightInPixels / 2 );

        // Mouse coordinates relative to the center of the screen
        int relativeMousePosX = 0;
        int relativeMousePosY = 0;

        if( absoluteMousePosX > screenCenterX )
        {
            relativeMousePosX = ( absoluteMousePosX - screenCenterX );
        }
        else if( absoluteMousePosX <= screenCenterX )
        {
            relativeMousePosX = ( screenCenterX - absoluteMousePosX );
            relativeMousePosX *= -1;
        }

        if( absoluteMousePosY > screenCenterY )
        {
            relativeMousePosY = ( absoluteMousePosY - screenCenterY );
        }
        else if( absoluteMousePosY <= screenCenterY )
        {
            relativeMousePosY = ( screenCenterY - absoluteMousePosY );
            relativeMousePosY *= -1;
        }

        if( ( relativeMousePosX != 0 ) || ( relativeMousePosY != 0 ) )
        {
            m_InputMapper.SetRawAxisValue( InputMapping::e_RawInputAxisMouseRelativeX, static_cast<double>( relativeMousePosX ) );
            m_InputMapper.SetRawAxisValue( InputMapping::e_RawInputAxisMouseRelativeY, static_cast<double>( relativeMousePosY ) );

            _CenterMouseCursor();
        }
    }
    else
    {
        // Make sure that the mouse has moved before generating any new events
        if( ( absoluteMousePosX != m_PreviousAbsoluteMousePosX ) ||
            ( absoluteMousePosY != m_PreviousAbsoluteMousePosY ) )
        {
            m_PreviousAbsoluteMousePosX = absoluteMousePosX;
            m_PreviousAbsoluteMousePosY = absoluteMousePosY;

            m_InputMapper.SetRawAxisValue( InputMapping::e_RawInputAxisMouseAbsoluteX, static_cast<double>( absoluteMousePosX ) );
            m_InputMapper.SetRawAxisValue( InputMapping::e_RawInputAxisMouseAbsoluteY, static_cast<double>( absoluteMousePosY ) );
        }
    }
}

//-----------------------------------------------------------------------

void GLWidget::wheelEvent(QWheelEvent* event)
{
    event->accept();

    QPoint distancecInEighthOfADegree = event->angleDelta();

    int x = distancecInEighthOfADegree.x();
    int y = distancecInEighthOfADegree.y();


    // Make sure that the smallest possible move by mouse is one degrees (i.e. 8 eighths of a degree)
    if( ( std::abs( x ) != 0 ) && ( std::abs( x ) < 8 ) )
    {
        x = ( 8 * MathUtils::Signum( x ) );
    }

    // Make sure that the smallest possible move by mouse is one degrees (i.e. 8 eighths of a degree)
    if( ( std::abs( y ) != 0 ) && ( std::abs( y ) < 8 ) )
    {
        y = ( 8 * MathUtils::Signum( y ) );
    }

    x = ( x / 8 );
    x = ( x * -1 ); // Need to negate it, because InputMapper expects positive values when rotating right and negative values when rotating left
    y = ( y / 8 );

    if( x != 0 )
    {
        m_InputMapper.SetRawAxisValue( InputMapping::e_RawInputAxisMouseWheelHorizontal,
                                       static_cast<double>( x ) );
    }

    if( y != 0 )
    {
        m_InputMapper.SetRawAxisValue( InputMapping::e_RawInputAxisMouseWheelVertical,
                                       static_cast<double>( y ) );
    }
}

//-----------------------------------------------------------------------

const GLWidget::DrawableSurfaceSize& GLWidget::GetDrawableSurfaceSize() const
{
    return m_DrawableSurfaceSize;
}

//-----------------------------------------------------------------------

void GLWidget::SetCaptureMouseInTheWindow(bool val)
{
    m_MouseCapturedInTheWIndow = val;

    _SetMouseCursorVisible( !m_MouseCapturedInTheWIndow );

    if( m_MouseCapturedInTheWIndow )
    {
        grabKeyboard();
        grabMouse();
        _CenterMouseCursor();
    }
    else
    {
        releaseKeyboard();
        releaseMouse();

        // Focus mouse and keyboard input on this window
        setFocus( Qt::MouseFocusReason );
    }
}

//-----------------------------------------------------------------------

bool GLWidget::GetCaptureMouseInTheWindow() const
{
    return m_MouseCapturedInTheWIndow;
}

//-----------------------------------------------------------------------

uint64_t GLWidget::_GetTicksInMilliseconds()
{
    assert( m_ElapsedTimer.isValid() && "Timer must be valid" );

    return static_cast<uint64_t>( m_ElapsedTimer.elapsed() );
}

//-----------------------------------------------------------------------

void GLWidget::_CenterMouseCursor()
{
    QCursor c = cursor();

    c.setPos( mapToGlobal( QPoint( m_DrawableSurfaceSize.widthInPixels / 2,
                                   m_DrawableSurfaceSize.heightInPixels / 2 ) ) );

    setCursor( c );
}

//-----------------------------------------------------------------------

void GLWidget::_SetMouseCursorVisible(bool val)
{
    QCursor c = cursor();

    if( val )
    {
        c.setShape( Qt::ArrowCursor );
    }
    else
    {
        c.setShape( Qt::BlankCursor );
    }

    setCursor( c );
}

//-----------------------------------------------------------------------

void GLWidget::_InitializeMainKeyCodeMap(QtKeyCodesToRawInputButtonsMap& keyCodeMap)
{
    keyCodeMap[Qt::Key_Return] = InputMapping::e_RawInputButtonReturn;
    keyCodeMap[Qt::Key_Escape] = InputMapping::e_RawInputButtonEscape;
    keyCodeMap[Qt::Key_Backspace] = InputMapping::e_RawInputButtonBackspace;
    keyCodeMap[Qt::Key_Tab] = InputMapping::e_RawInputButtonTab;
    keyCodeMap[Qt::Key_Space] = InputMapping::e_RawInputButtonSpace;
    keyCodeMap[Qt::Key_Exclam] = InputMapping::e_RawInputButtonExclaim;
    keyCodeMap[Qt::Key_QuoteDbl] = InputMapping::e_RawInputButtonQuoteDbl;
    keyCodeMap[Qt::Key_NumberSign] = InputMapping::e_RawInputButtonHash;
    keyCodeMap[Qt::Key_Percent] = InputMapping::e_RawInputButtonPercent;
    keyCodeMap[Qt::Key_Dollar] = InputMapping::e_RawInputButtonDollar;
    keyCodeMap[Qt::Key_Ampersand] = InputMapping::e_RawInputButtonAmpersand;
    keyCodeMap[Qt::Key_QuoteLeft] = InputMapping::e_RawInputButtonQuote;
    keyCodeMap[Qt::Key_ParenLeft] = InputMapping::e_RawInputButtonLeftParen;
    keyCodeMap[Qt::Key_ParenRight] = InputMapping::e_RawInputButtonRightParent;
    keyCodeMap[Qt::Key_Asterisk] = InputMapping::e_RawInputButtonAsterisk;
    keyCodeMap[Qt::Key_Plus] = InputMapping::e_RawInputButtonPlus;
    keyCodeMap[Qt::Key_Comma] = InputMapping::e_RawInputButtonComma;
    keyCodeMap[Qt::Key_Minus] = InputMapping::e_RawInputButtonMinus;
    keyCodeMap[Qt::Key_Period] = InputMapping::e_RawInputButtonPeriod;
    keyCodeMap[Qt::Key_Slash] = InputMapping::e_RawInputButtonSlash;
    keyCodeMap[Qt::Key_0] = InputMapping::e_RawInputButton0;
    keyCodeMap[Qt::Key_1] = InputMapping::e_RawInputButton1;
    keyCodeMap[Qt::Key_2] = InputMapping::e_RawInputButton2;
    keyCodeMap[Qt::Key_3] = InputMapping::e_RawInputButton3;
    keyCodeMap[Qt::Key_4] = InputMapping::e_RawInputButton4;
    keyCodeMap[Qt::Key_5] = InputMapping::e_RawInputButton5;
    keyCodeMap[Qt::Key_6] = InputMapping::e_RawInputButton6;
    keyCodeMap[Qt::Key_7] = InputMapping::e_RawInputButton7;
    keyCodeMap[Qt::Key_8] = InputMapping::e_RawInputButton8;
    keyCodeMap[Qt::Key_9] = InputMapping::e_RawInputButton9;
    keyCodeMap[Qt::Key_Colon] = InputMapping::e_RawInputButtonColon;
    keyCodeMap[Qt::Key_Semicolon] = InputMapping::e_RawInputButtonSemicolon;
    keyCodeMap[Qt::Key_Less] = InputMapping::e_RawInputButtonLess;
    keyCodeMap[Qt::Key_Equal] = InputMapping::e_RawInputButtonEquals;
    keyCodeMap[Qt::Key_Greater] = InputMapping::e_RawInputButtonGreater;
    keyCodeMap[Qt::Key_Question] = InputMapping::e_RawInputButtonQuestion;
    keyCodeMap[Qt::Key_At] = InputMapping::e_RawInputButtonAt;

    keyCodeMap[Qt::Key_BracketLeft] = InputMapping::e_RawInputButtonLeftBracket;
    keyCodeMap[Qt::Key_Backslash] = InputMapping::e_RawInputButtonBackslash;
    keyCodeMap[Qt::Key_BracketRight] = InputMapping::e_RawInputButtonRightBracket;
    keyCodeMap[Qt::Key_AsciiCircum] = InputMapping::e_RawInputButtonCarret;
    keyCodeMap[Qt::Key_Underscore] = InputMapping::e_RawInputButtonUnderscore;
    keyCodeMap[Qt::Key_QuoteLeft] = InputMapping::e_RawInputButtonBackquote;
    keyCodeMap[Qt::Key_A] = InputMapping::e_RawInputButtonA;
    keyCodeMap[Qt::Key_B] = InputMapping::e_RawInputButtonB;
    keyCodeMap[Qt::Key_C] = InputMapping::e_RawInputButtonC;
    keyCodeMap[Qt::Key_D] = InputMapping::e_RawInputButtonD;
    keyCodeMap[Qt::Key_E] = InputMapping::e_RawInputButtonE;
    keyCodeMap[Qt::Key_F] = InputMapping::e_RawInputButtonF;
    keyCodeMap[Qt::Key_G] = InputMapping::e_RawInputButtonG;
    keyCodeMap[Qt::Key_H] = InputMapping::e_RawInputButtonH;
    keyCodeMap[Qt::Key_I] = InputMapping::e_RawInputButtonI;
    keyCodeMap[Qt::Key_J] = InputMapping::e_RawInputButtonJ;
    keyCodeMap[Qt::Key_K] = InputMapping::e_RawInputButtonK;
    keyCodeMap[Qt::Key_L] = InputMapping::e_RawInputButtonL;
    keyCodeMap[Qt::Key_M] = InputMapping::e_RawInputButtonM;
    keyCodeMap[Qt::Key_N] = InputMapping::e_RawInputButtonN;
    keyCodeMap[Qt::Key_O] = InputMapping::e_RawInputButtonO;
    keyCodeMap[Qt::Key_P] = InputMapping::e_RawInputButtonP;
    keyCodeMap[Qt::Key_Q] = InputMapping::e_RawInputButtonQ;
    keyCodeMap[Qt::Key_R] = InputMapping::e_RawInputButtonR;
    keyCodeMap[Qt::Key_S] = InputMapping::e_RawInputButtonS;
    keyCodeMap[Qt::Key_T] = InputMapping::e_RawInputButtonT;
    keyCodeMap[Qt::Key_U] = InputMapping::e_RawInputButtonU;
    keyCodeMap[Qt::Key_V] = InputMapping::e_RawInputButtonV;
    keyCodeMap[Qt::Key_W] = InputMapping::e_RawInputButtonW;
    keyCodeMap[Qt::Key_X] = InputMapping::e_RawInputButtonX;
    keyCodeMap[Qt::Key_Y] = InputMapping::e_RawInputButtonY;
    keyCodeMap[Qt::Key_Z] = InputMapping::e_RawInputButtonZ;

    keyCodeMap[Qt::Key_CapsLock] = InputMapping::e_RawInputButtonCapsLock;

    keyCodeMap[Qt::Key_F1] = InputMapping::e_RawInputButtonF1;
    keyCodeMap[Qt::Key_F2] = InputMapping::e_RawInputButtonF2;
    keyCodeMap[Qt::Key_F3] = InputMapping::e_RawInputButtonF3;
    keyCodeMap[Qt::Key_F4] = InputMapping::e_RawInputButtonF4;
    keyCodeMap[Qt::Key_F5] = InputMapping::e_RawInputButtonF5;
    keyCodeMap[Qt::Key_F6] = InputMapping::e_RawInputButtonF6;
    keyCodeMap[Qt::Key_F7] = InputMapping::e_RawInputButtonF7;
    keyCodeMap[Qt::Key_F8] = InputMapping::e_RawInputButtonF8;
    keyCodeMap[Qt::Key_F9] = InputMapping::e_RawInputButtonF9;
    keyCodeMap[Qt::Key_F10] = InputMapping::e_RawInputButtonF10;
    keyCodeMap[Qt::Key_F11] = InputMapping::e_RawInputButtonF11;
    keyCodeMap[Qt::Key_F12] = InputMapping::e_RawInputButtonF12;

    keyCodeMap[Qt::Key_Print] = InputMapping::e_RawInputButtonPrintScreen;
    keyCodeMap[Qt::Key_ScrollLock] = InputMapping::e_RawInputButtonScrollLock;
    keyCodeMap[Qt::Key_Pause] = InputMapping::e_RawInputButtonPause;
    keyCodeMap[Qt::Key_Insert] = InputMapping::e_RawInputButtonInsert;
    keyCodeMap[Qt::Key_Home] = InputMapping::e_RawInputButtonHome;
    keyCodeMap[Qt::Key_PageUp] = InputMapping::e_RawInputButtonPageUp;
    keyCodeMap[Qt::Key_Delete] = InputMapping::e_RawInputButtonDelete;
    keyCodeMap[Qt::Key_End] = InputMapping::e_RawInputButtonEnd;
    keyCodeMap[Qt::Key_PageDown] = InputMapping::e_RawInputButtonPageDown;
    keyCodeMap[Qt::Key_Right] = InputMapping::e_RawInputButtonRight;
    keyCodeMap[Qt::Key_Left] = InputMapping::e_RawInputButtonLeft;
    keyCodeMap[Qt::Key_Down] = InputMapping::e_RawInputButtonDown;
    keyCodeMap[Qt::Key_Up] = InputMapping::e_RawInputButtonUp;

#ifdef OS_GENERIC_UNIX
    keyCodeMap[Qt::Key_Super_L] = InputMapping::e_RawInputButtonLeftMeta;
    keyCodeMap[Qt::Key_Super_R] = InputMapping::e_RawInputButtonRightMeta;
#endif

    keyCodeMap[Qt::Key_NumLock] = InputMapping::e_RawInputButtonNumLock;
}

//-----------------------------------------------------------------------

void GLWidget::_InitializeKeyPadKeyCodeMap(QtKeyCodesToRawInputButtonsMap& keyCodeMap)
{
    keyCodeMap[Qt::Key_Slash] = InputMapping::e_RawInputButtonKeyPadDivide;
    keyCodeMap[Qt::Key_Asterisk] = InputMapping::e_RawInputButtonKeyPadMultiply;
    keyCodeMap[Qt::Key_Minus] = InputMapping::e_RawInputButtonKeyPadMinus;
    keyCodeMap[Qt::Key_Plus] = InputMapping::e_RawInputButtonKeyPadPlus;
    keyCodeMap[Qt::Key_Enter] = InputMapping::e_RawInputButtonKeyPadEnter;
    keyCodeMap[Qt::Key_1] = InputMapping::e_RawInputButtonKeyPad1;
    keyCodeMap[Qt::Key_2] = InputMapping::e_RawInputButtonKeyPad2;
    keyCodeMap[Qt::Key_3] = InputMapping::e_RawInputButtonKeyPad3;
    keyCodeMap[Qt::Key_4] = InputMapping::e_RawInputButtonKeyPad4;
    keyCodeMap[Qt::Key_5] = InputMapping::e_RawInputButtonKeyPad5;
    keyCodeMap[Qt::Key_6] = InputMapping::e_RawInputButtonKeyPad6;
    keyCodeMap[Qt::Key_7] = InputMapping::e_RawInputButtonKeyPad7;
    keyCodeMap[Qt::Key_8] = InputMapping::e_RawInputButtonKeyPad8;
    keyCodeMap[Qt::Key_9] = InputMapping::e_RawInputButtonKeyPad9;
    keyCodeMap[Qt::Key_0] = InputMapping::e_RawInputButtonKeyPad0;
    keyCodeMap[Qt::Key_Period] = InputMapping::e_RawInputButtonKeyPadPeriod;
}

//-----------------------------------------------------------------------

bool GLWidget::_MapKeyboardEventToRawInputButton(QKeyEvent* event,
                                                 InputMapping::RawInputButton& outRawInputButton)
{
    bool retVal = false;

    const Qt::KeyboardModifiers modifier = event->modifiers();
    const int keyCode = event->key();

#ifdef OS_APPLE
    static const int c_ControKeyKeyCode = Qt::Key_Meta;
    static const int c_MetaKeyKeyCode = Qt::Key_Control;
#else
    static const int c_ControKeyKeyCode = Qt::Key_Control;
    #ifdef OS_WINDOWS
        static const int c_MetaKeyKeyCode = Qt::Key_Meta;
    #endif
#endif

    if( ( modifier & Qt::KeypadModifier )
#ifdef OS_APPLE
          // On Mac OS X arrow keys are considered to be part of the key pad, hence the checks below
          && ( keyCode != Qt::Key_Left )
          && ( keyCode != Qt::Key_Right )
          && ( keyCode != Qt::Key_Up)
          && ( keyCode != Qt::Key_Down )
#endif
          )
    {
        retVal = _MapQtKeyCodeToRawInputButton( keyCode, m_KeyPadKeyCodeMap, outRawInputButton );
    }
    else
    {
        retVal = _MapQtKeyCodeToRawInputButton( keyCode, m_MainKeyCodeMap, outRawInputButton );

        if( !retVal &&
            ( ( keyCode == c_ControKeyKeyCode ) ||
#if defined( OS_WINDOWS ) || defined( OS_APPLE )
              ( keyCode == c_MetaKeyKeyCode ) ||
#endif
              ( keyCode == Qt::Key_Alt ) ||
              ( keyCode == Qt::Key_Shift ) ) )
        {
            retVal = true;

            const quint32 nativeVirtualKeyCode = event->nativeVirtualKey();

            switch( nativeVirtualKeyCode )
            {
#ifdef OS_WINDOWS
                case VK_LCONTROL:
                {
                    outRawInputButton = InputMapping::e_RawInputButtonLeftControl;
                    break;
                }

                case VK_RCONTROL:
                {
                    outRawInputButton = InputMapping::e_RawInputButtonRightControl;
                    break;
                }

                case VK_LSHIFT:
                {
                    outRawInputButton = InputMapping::e_RawInputButtonLeftShift;
                    break;
                }

                case VK_RSHIFT:
                {
                    outRawInputButton = InputMapping::e_RawInputButtonRightShift;
                    break;
                }

                case VK_LMENU:
                {
                    outRawInputButton = InputMapping::e_RawInputButtonLeftAlt;
                    break;
                }

                case VK_RMENU:
                {
                    outRawInputButton = InputMapping::e_RawInputButtonRightAlt;
                    break;
                }

                case VK_LWIN:
                {
                    outRawInputButton = InputMapping::e_RawInputButtonLeftMeta;
                    break;
                }

                case VK_RWIN:
                {
                    outRawInputButton = InputMapping::e_RawInputButtonRightMeta;
                    break;
                }
#elif defined( OS_APPLE )
                case kVK_Control:
                {
                    outRawInputButton = InputMapping::e_RawInputButtonLeftControl;
                    break;
                }

                case kVK_RightControl:
                {
                    outRawInputButton = InputMapping::e_RawInputButtonRightControl;
                    break;
                }

                case kVK_Shift:
                {
                    outRawInputButton = InputMapping::e_RawInputButtonLeftShift;
                    break;
                }

                case kVK_RightShift:
                {
                    outRawInputButton = InputMapping::e_RawInputButtonRightShift;
                    break;
                }

                case kVK_Option:
                {
                    outRawInputButton = InputMapping::e_RawInputButtonLeftAlt;
                    break;
                }

                case kVK_RightOption:
                {
                    outRawInputButton = InputMapping::e_RawInputButtonRightAlt;
                    break;
                }

                case kVK_Command:
                {
                    outRawInputButton = InputMapping::e_RawInputButtonLeftMeta;
                    break;
                }

                case kVK_RightCommand:
                {
                    outRawInputButton = InputMapping::e_RawInputButtonRightMeta;
                    break;
                }
#elif defined( OS_GENERIC_UNIX )
                case XK_Control_L:
                {
                    outRawInputButton = InputMapping::e_RawInputButtonLeftControl;
                    break;
                }

                case XK_Control_R:
                {
                    outRawInputButton = InputMapping::e_RawInputButtonRightControl;
                    break;
                }

                case XK_Shift_L:
                {
                    outRawInputButton = InputMapping::e_RawInputButtonLeftShift;
                    break;
                }

                case XK_Shift_R:
                {
                    outRawInputButton = InputMapping::e_RawInputButtonRightShift;
                    break;
                }

                case XK_Alt_L:
                {
                    outRawInputButton = InputMapping::e_RawInputButtonLeftAlt;
                    break;
                }

                case XK_Alt_R:
                {
                    outRawInputButton = InputMapping::e_RawInputButtonRightAlt;
                    break;
                }
#else
    #error "Ctrl, Shift, Alt and Meta key handling is not implemented for this platform."
#endif
                default:
                {
                    retVal = false;
                    break;
                }
            }
        }
    }

    return retVal;
}

//-----------------------------------------------------------------------

bool GLWidget::_MapQtKeyCodeToRawInputButton(int qtKeyCode,
                                             const QtKeyCodesToRawInputButtonsMap& keyCodeMap,
                                             InputMapping::RawInputButton& outRawInputButton)
{
    bool retVal = false;

    QtKeyCodesToRawInputButtonsMap::const_iterator it = keyCodeMap.find( qtKeyCode );

    if( it != keyCodeMap.end() )
    {
        outRawInputButton = it->second;
        retVal = true;
    }

    return  retVal;
}

//-----------------------------------------------------------------------

bool GLWidget::_CheckIfContextSupportsOpenGLVersion(const QGLContext* glContext,
                                                    int major,
                                                    int minor)
{
    QGLFormat glFormat = glContext->format();

    return ( ( glFormat.majorVersion() > major ) ||
             ( ( glFormat.majorVersion() == major ) && ( glFormat.minorVersion() >= minor ) ) );
}

//-----------------------------------------------------------------------

void GLWidget::_LogOpenGLContextDetails(const QGLContext* glContext)
{
    QGLFormat glFormat = glContext->format();

    const int openGLMajorVersion = glFormat.majorVersion();
    const int openGLMinorVersion = glFormat.minorVersion();
    const int depthBufferSize = glFormat.depthBufferSize();

    std::string profileNameString = "";
    if( glFormat.profile() == QGLFormat::CoreProfile )
    {
        profileNameString = "Core";
    }
    else if( glFormat.profile() == QGLFormat::CompatibilityProfile )
    {
        profileNameString = "Compatibility";
    }

    MsgLogger::LogEvent( "OpenGL Context:" );
    MsgLogger::LogEvent( "  Depth Buffer Size: " + StringUitls::NumToString( depthBufferSize ) );
    MsgLogger::LogEvent( "  Profile: " + profileNameString );
    MsgLogger::LogEvent( "  GL Version: " + StringUitls::NumToString( openGLMajorVersion ) + StringUitls::NumToString( openGLMinorVersion ) );
}

//-----------------------------------------------------------------------

void GLWidget::MainInputCallback(InputMapping::MappedInput& inputs,
                                 float UNUSED_PARAM(frameTimeInSeconds),
                                 void* glWidget)
{
    GLWidget* widget = reinterpret_cast<GLWidget*>( glWidget );

    if( inputs.FindAndConsumeAction( InputMapping::e_ActionToggleCaptureMouseInTheWindow )  )
    {
        widget->SetCaptureMouseInTheWindow( !widget->GetCaptureMouseInTheWindow() );
    }
}

//-----------------------------------------------------------------------
