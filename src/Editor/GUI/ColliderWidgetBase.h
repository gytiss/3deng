#ifndef COLLIDERWIDGETBASE_H_INCLUDED
#define COLLIDERWIDGETBASE_H_INCLUDED

#include "ColliderWidgetBaseQObjectParent.h"
#include <CommonDefinitions.h>

template <class ComponentUIType, class ColliderType>
class ColliderWidgetBase : public ColliderWidgetBaseQObjectParent
{
public:
    virtual ~ColliderWidgetBase();

protected:
    // The constructor is protected because this object is not supposed to be instanciated on its own
    ColliderWidgetBase(CategorizedListWidget* listWidgetToAddOurselvesTo, const std::string& colliderName, QWidget* parent = 0);

    void _SetColliderComponent(ColliderType* collider);
    void XCenterOffsetLineEditEditingFinishedImpl();
    void YCenterOffsetLineEditEditingFinishedImpl();
    void ZCenterOffsetLineEditEditingFinishedImpl();
    void IsTriggerCheckBoxStateChangedImpl(int state);

    ComponentUIType* m_UI;
    ColliderType* m_ColliderComponent;
};

//-----------------------------------------------------------------------

template <class ComponentUIType, class ColliderType>
ColliderWidgetBase<ComponentUIType, ColliderType>::ColliderWidgetBase(CategorizedListWidget* listWidgetToAddOurselvesTo,
                                                                      const std::string& colliderName,
                                                                      QWidget* parent)
: ColliderWidgetBaseQObjectParent( listWidgetToAddOurselvesTo, colliderName, parent ),
  m_UI( new ComponentUIType() ),
  m_ColliderComponent( nullptr )
{
    m_UI->setupUi( this );

    connect( m_UI->xCenterOffsetLineEdit, SIGNAL( editingFinished() ),
             this, SLOT( XCenterOffsetLineEditEditingFinished() ) );

    connect( m_UI->yCenterOffsetLineEdit, SIGNAL( editingFinished() ),
             this, SLOT( YCenterOffsetLineEditEditingFinished() ) );

    connect( m_UI->zCenterOffsetLineEdit, SIGNAL( editingFinished() ),
             this, SLOT( ZCenterOffsetLineEditEditingFinished() ) );

    connect( m_UI->isTriggerCheckBox, SIGNAL( stateChanged( int ) ),
             this, SLOT( IsTriggerCheckBoxStateChanged( int ) ) );

    m_UI->xCenterOffsetLineEdit->setValidator( &m_FloatValueValidator );
    m_UI->yCenterOffsetLineEdit->setValidator( &m_FloatValueValidator );
    m_UI->zCenterOffsetLineEdit->setValidator( &m_FloatValueValidator );
}

//-----------------------------------------------------------------------

template <class ComponentUIType, class ColliderType>
ColliderWidgetBase<ComponentUIType, ColliderType>::~ColliderWidgetBase()
{
    SafeDelete( m_UI );
}

//-----------------------------------------------------------------------

template <class ComponentUIType, class ColliderType>
void ColliderWidgetBase<ComponentUIType, ColliderType>::_SetColliderComponent(ColliderType* collider)
{
    if( collider )
    {
        m_UI->isTriggerCheckBox->setCheckState( collider->IsTrigger() ? Qt::Checked : Qt::Unchecked );

        GUIHelpers::SetNumericLineEditValue( m_UI->xCenterOffsetLineEdit, collider->GetCenterXPositionOffset() );
        GUIHelpers::SetNumericLineEditValue( m_UI->yCenterOffsetLineEdit, collider->GetCenterYPositionOffset() );
        GUIHelpers::SetNumericLineEditValue( m_UI->zCenterOffsetLineEdit, collider->GetCenterZPositionOffset() );
    }

    m_ColliderComponent = collider;
}

//-----------------------------------------------------------------------

template <class ComponentUIType, class ColliderType>
void ColliderWidgetBase<ComponentUIType, ColliderType>::XCenterOffsetLineEditEditingFinishedImpl()
{
    if( m_ColliderComponent )
    {
        m_ColliderComponent->SetCenterXPositionOffset( GUIHelpers::GetNumericLineEditValue( m_UI->xCenterOffsetLineEdit ) );
    }
}

//-----------------------------------------------------------------------

template <class ComponentUIType, class ColliderType>
void ColliderWidgetBase<ComponentUIType, ColliderType>::YCenterOffsetLineEditEditingFinishedImpl()
{
    if( m_ColliderComponent )
    {
        m_ColliderComponent->SetCenterYPositionOffset( GUIHelpers::GetNumericLineEditValue( m_UI->yCenterOffsetLineEdit ) );
    }
}

//-----------------------------------------------------------------------

template <class ComponentUIType, class ColliderType>
void ColliderWidgetBase<ComponentUIType, ColliderType>::ZCenterOffsetLineEditEditingFinishedImpl()
{
    if( m_ColliderComponent )
    {
        m_ColliderComponent->SetCenterZPositionOffset( GUIHelpers::GetNumericLineEditValue( m_UI->zCenterOffsetLineEdit ) );
    }
}

//-----------------------------------------------------------------------

template <class ComponentUIType, class ColliderType>
void ColliderWidgetBase<ComponentUIType, ColliderType>::IsTriggerCheckBoxStateChangedImpl(int state)
{
    if( m_ColliderComponent )
    {
        m_ColliderComponent->SetTrigger( ( state == Qt::Checked ) ? true : false );
    }
}

//-----------------------------------------------------------------------


#endif // COLLIDERWIDGETBASE_H_INCLUDED
