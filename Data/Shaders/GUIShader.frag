in vec4 VertexColor;
in vec2 TextureCoords;
out vec4 out_Diffuse;

uniform uint u_UseTexture;
uniform sampler2D u_DiffuseMap;

void main(void)
{
    if( int(u_UseTexture) == int(1) )
    {
        out_Diffuse = texture( u_DiffuseMap, TextureCoords ) * VertexColor; // Filter texture using vertex color
    }
    else
    {
        out_Diffuse = VertexColor;
    }
}
