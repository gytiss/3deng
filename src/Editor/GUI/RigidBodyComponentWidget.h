#ifndef RIGIDBODYCOMPONENTWIDGET_H_INCLUDED
#define RIGIDBODYCOMPONENTWIDGET_H_INCLUDED

#include "ComponentWidget.h"

namespace Ui
{
    class RigidBodyComponentUI;
}

class RigidBody;

class RigidBodyComponentWidget : public ComponentWidget
{
    Q_OBJECT

public:
    RigidBodyComponentWidget(CategorizedListWidget* listWidgetToAddOurselvesTo, QWidget* parent = 0);
    ~RigidBodyComponentWidget();

    void SetRigidBodyComponent(RigidBody* rigidBody);

    void ResetPaletteToDefault();

private slots:
    void MassLineEditEditingFinished();
    void DragLineEditEditingFinished();
    void AngularDragLineEditEditingFinished();
    void IsKinematicCheckBoxStateChanged(int state);
    void XFreezePositionCheckBoxStateChanged(int state);
    void YFreezePositionCheckBoxStateChanged(int state);
    void ZFreezePositionCheckBoxStateChanged(int state);
    void XFreezeRotationCheckBoxStateChanged(int state);
    void YFreezeRotationCheckBoxStateChanged(int state);
    void ZFreezeRotationCheckBoxStateChanged(int state);

private:
    const QString m_Name;
    Ui::RigidBodyComponentUI* m_UI;
    RigidBody* m_RigidBodyComponent;
};

#endif // RIGIDBODYCOMPONENTWIDGET_H_INCLUDED
