#ifndef SHADERPROGBASEUNIFORMS_H_INCLUDED
#define SHADERPROGBASEUNIFORMS_H_INCLUDED

#include <OpenGLIncludes.h>
#include <OpenGLErrorChecking.h>
#ifdef DEBUG_BUILD
    #include <Exceptions/GenericException.h>
#endif
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <cstdlib>
#include <string>
#include <vector>
#include <algorithm>

class ShaderProg;

class ShaderProgUniform
{
public:
   /**
    * Fetch location index of this uniform from the shader and cache it
    */
    void UpdateLocationIndexCache();

	inline const std::string& GetName()
	{
	    return m_Name;
	}

	inline size_t GetValueSizeInBytes() const
	{
	    assert( m_ValueSizeInBytes && "Size of a uniform must not be zero" );
	    return m_ValueSizeInBytes;
	}

protected:
    ShaderProgUniform(ShaderProg* shaderProg, const std::string& name);

    inline void SetValueSizeInBytes( size_t valSize )
    {
        m_ValueSizeInBytes = valSize;
    }

	inline void SetCachedValueExists()
	{
	    m_ValueCachedFlag = true;
	}

	inline bool CachedValueExists() const
	{
	    return m_ValueCachedFlag;
	}

	inline GLint GetLocationIndex() const
	{
	    return m_LocationIndex;
	}

#ifdef DEBUG_BUILD
	void CheckShaderIsInUse() const;
#endif

private:
    // Used to prevent copying
    ShaderProgUniform& operator = (const ShaderProgUniform& other);
    ShaderProgUniform(const ShaderProgUniform& other);

    GLint m_LocationIndex;
    const std::string m_Name;
    ShaderProg* const m_ShaderProg; ///< Shader program this uniform is registered with
    bool m_ValueCachedFlag;
    size_t m_ValueSizeInBytes;
};

//-----------------------------------------------------------------------

class ShaderProgUniformFloat: protected ShaderProgUniform
{
protected:
	ShaderProgUniformFloat(ShaderProg* shaderProg, const std::string& name)
	: ShaderProgUniform( shaderProg, name ),
	  m_CachedValue( 0.0f )
	{
	}

	inline void SetValue(float value)
	{
#ifdef DEBUG_BUILD
	    CheckShaderIsInUse();
#endif

        if( !CachedValueExists() )
        {
            m_CachedValue = value;
            SetCachedValueExists();
            glUniform1fv( GetLocationIndex(), 1, &value );
        }
        else if( value != m_CachedValue )
        {
            glUniform1fv( GetLocationIndex(), 1, &value );
            CheckForGLErrors();
        }
	}

private:
    float m_CachedValue;
};

//-----------------------------------------------------------------------

class ShaderProgUniformFloatVec2: protected ShaderProgUniform
{
protected:
	ShaderProgUniformFloatVec2(ShaderProg* shaderProg, const std::string& name)
	: ShaderProgUniform( shaderProg, name ),
	  m_CachedValue( 0.0f, 0.0f )
	{
	}

	inline void SetValue(const glm::vec2& value)
	{
#ifdef DEBUG_BUILD
        CheckShaderIsInUse();
#endif

        if( !CachedValueExists() )
        {
            m_CachedValue = value;
            SetCachedValueExists();
            glUniform2fv( GetLocationIndex(), 1, glm::value_ptr( value ) );
        }
        else if( value != m_CachedValue )
        {
            glUniform2fv( GetLocationIndex(), 1, glm::value_ptr( value ) );
            CheckForGLErrors();
        }
	}

private:
    glm::vec2 m_CachedValue;
};

//-----------------------------------------------------------------------

class ShaderProgUniformFloatVec3: protected ShaderProgUniform
{
protected:
	ShaderProgUniformFloatVec3(ShaderProg* shaderProg, const std::string& name)
	: ShaderProgUniform( shaderProg, name ),
	  m_CachedValue( 0.0f, 0.0f, 0.0f )
	{
	}

	inline void SetValue(const glm::vec3& value)
	{
#ifdef DEBUG_BUILD
        CheckShaderIsInUse();
#endif

        if( !CachedValueExists() )
        {
            m_CachedValue = value;
            SetCachedValueExists();
            glUniform3fv( GetLocationIndex(), 1, glm::value_ptr( value ) );
        }
        else if( value != m_CachedValue )
        {
            m_CachedValue = value;
            glUniform3fv( GetLocationIndex(), 1, glm::value_ptr( value ) );
            CheckForGLErrors();
        }
	}

private:
    glm::vec3 m_CachedValue;
};

//-----------------------------------------------------------------------

class ShaderProgUniformFloatVec4: protected ShaderProgUniform
{
protected:
	ShaderProgUniformFloatVec4(ShaderProg* shaderProg, const std::string& name)
	: ShaderProgUniform( shaderProg, name ),
	  m_CachedValue( 0.0f, 0.0f, 0.0f, 0.0f )
	{
	}

	inline void SetValue(const glm::vec4& value)
	{
#ifdef DEBUG_BUILD
        CheckShaderIsInUse();
#endif

        if( !CachedValueExists() )
        {
            m_CachedValue = value;
            SetCachedValueExists();
            glUniform4fv( GetLocationIndex(), 1, glm::value_ptr( value ) );
        }
        else if( value != m_CachedValue )
        {
            m_CachedValue = value;
            glUniform4fv( GetLocationIndex(), 1, glm::value_ptr( value ) );
            CheckForGLErrors();
        }
	}

private:
    glm::vec4 m_CachedValue;
};

//-----------------------------------------------------------------------

class ShaderProgUniformUnsignedInt: protected ShaderProgUniform
{
protected:
	ShaderProgUniformUnsignedInt(ShaderProg* shaderProg, const std::string& name)
	: ShaderProgUniform( shaderProg, name ),
	  m_CachedValue( 0 )
	{
	}

	inline void SetValue(unsigned int value)
	{
#ifdef DEBUG_BUILD
        CheckShaderIsInUse();
#endif

        if( !CachedValueExists() )
        {
            m_CachedValue = value;
            SetCachedValueExists();
            glUniform1uiv( GetLocationIndex(), 1, &value );
        }
        else if( value != m_CachedValue )
        {
            m_CachedValue = value;
            glUniform1uiv( GetLocationIndex(), 1, &value );
            CheckForGLErrors();
        }
	}

private:
    unsigned int m_CachedValue;
};

//-----------------------------------------------------------------------
#ifdef OPENGL_ES
template<class VectorType, void (*OpenGLVectorUniformUploadFuncAlias)(GLint, GLsizei, const GLfloat*)>
#else
template<class VectorType, void (**OpenGLVectorUniformUploadFuncAlias)(GLint, GLsizei, const GLfloat*)>
#endif
class ShaderProgUniformFloatVecArray: protected ShaderProgUniform
{
protected:
	ShaderProgUniformFloatVecArray(ShaderProg* shaderProg, const std::string& name)
	: ShaderProgUniform( shaderProg, name ),
	  m_CachedValues()
	{
	}

	inline void SetValue(size_t arrayElementCount, const VectorType* elementArray)
	{
        assert( elementArray && "Uniform element array pointer must not be null" );

#ifdef DEBUG_BUILD
        CheckShaderIsInUse();
#endif

        if( !CachedValueExists() || ( m_CachedValues.size() != arrayElementCount ) )
        {
            m_CachedValues.assign( elementArray, ( elementArray + arrayElementCount ) );
            SetCachedValueExists();
            (
#ifndef OPENGL_ES
            *
#endif
            OpenGLVectorUniformUploadFuncAlias
            )( GetLocationIndex(),
               arrayElementCount,
               glm::value_ptr( elementArray[0] ) );
            CheckForGLErrors();
        }
        else
        {
            std::pair<class std::vector<VectorType>::const_iterator, const VectorType*> mismatchingPair =
            std::mismatch ( m_CachedValues.begin(), m_CachedValues.end(), elementArray);
            if( mismatchingPair.first != m_CachedValues.end() )
            {
                m_CachedValues.assign( elementArray, ( elementArray + arrayElementCount ) );
                (
#ifndef OPENGL_ES
                *
#endif
                OpenGLVectorUniformUploadFuncAlias
                )( GetLocationIndex(),
                   arrayElementCount,
                   glm::value_ptr( elementArray[0] ) );
                CheckForGLErrors();
            }
        }
	}

private:
    std::vector<VectorType> m_CachedValues;
};

//-----------------------------------------------------------------------

typedef ShaderProgUniformFloatVecArray<glm::vec2, &glUniform2fv> ShaderProgUniformFloatVec2Array;
typedef ShaderProgUniformFloatVecArray<glm::vec3, &glUniform3fv> ShaderProgUniformFloatVec3Array;
typedef ShaderProgUniformFloatVecArray<glm::vec4, &glUniform4fv> ShaderProgUniformFloatVec4Array;

//-----------------------------------------------------------------------

class ShaderProgUniformFloatArray: protected ShaderProgUniform
{
protected:
	ShaderProgUniformFloatArray(ShaderProg* shaderProg, const std::string& name)
	: ShaderProgUniform( shaderProg, name ),
	  m_CachedValues()
	{
	}

	inline void SetValue(size_t arrayElementCount, const float* elementArray)
	{
        assert( elementArray && "Uniform element array pointer must not be null" );

#ifdef DEBUG_BUILD
        CheckShaderIsInUse();
#endif

        if( !CachedValueExists() || ( m_CachedValues.size() != arrayElementCount ) )
        {
            m_CachedValues.assign( elementArray, ( elementArray + arrayElementCount ) );
            SetCachedValueExists();
            glUniform1fv( GetLocationIndex(), static_cast<GLsizei>( arrayElementCount ), elementArray );
            CheckForGLErrors();
        }
        else
        {
            std::pair<std::vector<float>::const_iterator, const float*> mismatchingPair =
            std::mismatch ( m_CachedValues.begin(), m_CachedValues.end(), elementArray);
            if( mismatchingPair.first != m_CachedValues.end() )
            {
                m_CachedValues.assign( elementArray, ( elementArray + arrayElementCount ) );
                glUniform1fv( GetLocationIndex(), static_cast<GLsizei>( arrayElementCount ), elementArray );
                CheckForGLErrors();
            }
        }
	}

private:
    std::vector<float> m_CachedValues;
};

//-----------------------------------------------------------------------

class ShaderProgUniformUnsignedIntArray: protected ShaderProgUniform
{
protected:
	ShaderProgUniformUnsignedIntArray(ShaderProg* shaderProg, const std::string& name)
	: ShaderProgUniform( shaderProg, name ),
	  m_CachedValues()
	{
	}

	inline void SetValue(size_t arrayElementCount, const unsigned int* elementArray)
	{
        assert( elementArray && "Uniform element array pointer must not be null" );

#ifdef DEBUG_BUILD
        CheckShaderIsInUse();
#endif

        if( !CachedValueExists() || ( m_CachedValues.size() != arrayElementCount ) )
        {
            m_CachedValues.assign( elementArray, ( elementArray + arrayElementCount ) );
            SetCachedValueExists();
            glUniform1uiv( GetLocationIndex(), static_cast<GLsizei>( arrayElementCount ), elementArray );
            CheckForGLErrors();
        }
        else
        {
            std::pair<std::vector<unsigned int>::const_iterator, const unsigned int*> mismatchingPair =
            std::mismatch ( m_CachedValues.begin(), m_CachedValues.end(), elementArray);
            if( mismatchingPair.first != m_CachedValues.end() )
            {
                m_CachedValues.assign( elementArray, ( elementArray + arrayElementCount ) );
                glUniform1uiv( GetLocationIndex(), static_cast<GLsizei>( arrayElementCount ), elementArray );
                CheckForGLErrors();
            }
        }
	}

private:
    std::vector<unsigned int> m_CachedValues;
};

//-----------------------------------------------------------------------

#ifdef OPENGL_ES
template<class MatrixType, void (*OpenGLMatrixUniformUploadFuncAlias)(GLint, GLsizei, GLboolean, const GLfloat*)>
#else
template<class MatrixType, void (**OpenGLMatrixUniformUploadFuncAlias)(GLint, GLsizei, GLboolean, const GLfloat*)>
#endif
class ShaderProgUniformFloatMat: protected ShaderProgUniform
{
protected:
	ShaderProgUniformFloatMat(ShaderProg* shaderProg, const std::string& name)
	: ShaderProgUniform( shaderProg, name ),
	  m_CachedValue( 1.0f ) // Identity matrix
	{
	}

	inline void SetValue(const MatrixType& value, bool transpose = false)
	{
#ifdef DEBUG_BUILD
        CheckShaderIsInUse();
#endif

        if( !CachedValueExists() )
        {
            m_CachedValue = value;
            SetCachedValueExists();

            const GLboolean transposeMatrix = ( transpose ? GL_TRUE : GL_FALSE );
            (
#ifndef OPENGL_ES
            *
#endif
            OpenGLMatrixUniformUploadFuncAlias
            ) ( GetLocationIndex(),
                1,
                transposeMatrix,
                glm::value_ptr( value ) );
            CheckForGLErrors();
        }
        else if( value != m_CachedValue )
        {
            m_CachedValue = value;
            const GLboolean transposeMatrix = ( transpose ? GL_TRUE : GL_FALSE );
            (
#ifndef OPENGL_ES
            *
#endif
            OpenGLMatrixUniformUploadFuncAlias
            )( GetLocationIndex(),
               1,
               transposeMatrix,
               glm::value_ptr( value ) );
            CheckForGLErrors();
        }
	}

private:
    MatrixType m_CachedValue;
};

//-----------------------------------------------------------------------
#ifdef OPENGL_ES
template<class MatrixType, void (*OpenGLMatrixUniformUploadFuncAlias)(GLint, GLsizei, GLboolean, const GLfloat*)>
#else
template<class MatrixType, void (**OpenGLMatrixUniformUploadFuncAlias)(GLint, GLsizei, GLboolean, const GLfloat*)>
#endif
class ShaderProgUniformFloatMatArray : protected ShaderProgUniform
{
protected:
	ShaderProgUniformFloatMatArray(ShaderProg* shaderProg, const std::string& name)
	: ShaderProgUniform( shaderProg, name )
	{
	}

	inline void SetValue(size_t matrixCount,
                         const MatrixType* matrixArray,
                         bool transpose = false)
	{
	    assert( matrixArray && "Matrix array must not be null" );

#ifdef DEBUG_BUILD
        CheckShaderIsInUse();
#endif

        const GLboolean transposeMatrices = ( transpose ? GL_TRUE : GL_FALSE );

        (
#ifndef OPENGL_ES
        *
#endif
        OpenGLMatrixUniformUploadFuncAlias
        )( GetLocationIndex(),
		   static_cast<GLsizei>( matrixCount ),
           transposeMatrices,
           glm::value_ptr( matrixArray[0] ) );
        CheckForGLErrors();
	}
};

//-----------------------------------------------------------------------

typedef ShaderProgUniformFloatMat<glm::mat2, &glUniformMatrix2fv> ShaderProgUniformFloatMat2;
typedef ShaderProgUniformFloatMat<glm::mat3, &glUniformMatrix3fv> ShaderProgUniformFloatMat3;
typedef ShaderProgUniformFloatMat<glm::mat4, &glUniformMatrix4fv> ShaderProgUniformFloatMat4;
typedef ShaderProgUniformFloatMat<glm::mat2x3, &glUniformMatrix2x3fv> ShaderProgUniformFloatMat2x3;
typedef ShaderProgUniformFloatMat<glm::mat3x2, &glUniformMatrix3x2fv> ShaderProgUniformFloatMat3x2;
typedef ShaderProgUniformFloatMat<glm::mat2x4, &glUniformMatrix2x4fv> ShaderProgUniformFloatMat2x4;
typedef ShaderProgUniformFloatMat<glm::mat4x2, &glUniformMatrix4x2fv> ShaderProgUniformFloatMat4x2;
typedef ShaderProgUniformFloatMat<glm::mat3x4, &glUniformMatrix3x4fv> ShaderProgUniformFloatMat3x4;
typedef ShaderProgUniformFloatMat<glm::mat4x3, &glUniformMatrix4x3fv> ShaderProgUniformFloatMat4x3;

typedef ShaderProgUniformFloatMatArray<glm::mat2, &glUniformMatrix2fv> ShaderProgUniformFloatMat2Array;
typedef ShaderProgUniformFloatMatArray<glm::mat3, &glUniformMatrix3fv> ShaderProgUniformFloatMat3Array;
typedef ShaderProgUniformFloatMatArray<glm::mat4, &glUniformMatrix4fv> ShaderProgUniformFloatMat4Array;
typedef ShaderProgUniformFloatMatArray<glm::mat2x3, &glUniformMatrix2x3fv> ShaderProgUniformFloatMat2x3Array;
typedef ShaderProgUniformFloatMatArray<glm::mat3x2, &glUniformMatrix3x2fv> ShaderProgUniformFloatMat3x2Array;
typedef ShaderProgUniformFloatMatArray<glm::mat2x4, &glUniformMatrix2x4fv> ShaderProgUniformFloatMat2x4Array;
typedef ShaderProgUniformFloatMatArray<glm::mat4x2, &glUniformMatrix4x2fv> ShaderProgUniformFloatMat4x2Array;
typedef ShaderProgUniformFloatMatArray<glm::mat3x4, &glUniformMatrix3x4fv> ShaderProgUniformFloatMat3x4Array;
typedef ShaderProgUniformFloatMatArray<glm::mat4x3, &glUniformMatrix4x3fv> ShaderProgUniformFloatMat4x3Array;

#endif // SHADERPROGBASEUNIFORMS_H_INCLUDED
