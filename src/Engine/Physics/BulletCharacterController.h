#ifndef BULLETCHARACTERCONTROLLER_H_INCLUDED
#define BULLETCHARACTERCONTROLLER_H_INCLUDED

#include <bullet/BulletDynamics/Character/btKinematicCharacterController.h>
#include <Scene/CompositeTransformNode.h>

class btMotionState;
class btPairCachingGhostObject;
class btConvexShape;
class PhysicsManager;

class BulletCharacterController : public btKinematicCharacterController
{
public:
    BulletCharacterController(PhysicsManager& physicsManager,
                              float walkHeight,
                              float crouchHeight,
                              float stepHeight,
                              float radius);

    virtual ~BulletCharacterController();

    virtual void updateAction(btCollisionWorld *collisionWorld, btScalar dt);

    unsigned int GetMaxJumps() const;

    void SetMaxJumps(unsigned int maxJumps);

    unsigned int GetJumpCount() const;

    virtual bool CanJump() const;

    virtual void StartJump(float maxJumpHeight);

    const btVector3& GetWalkDirection();

    void SetLinearMovement(const glm::vec3& linearVelocity);

    void SetAngularMovement(float headingInDegrees);

    void SetPos(const glm::vec3& pos);
    const glm::vec3& GetPos();
    float GetTop();

    void StartFly() {}
    void StopFly() {}
    void StartCrouch() {}
    void StopCrouch() {}
    bool IsCrouching() { return false; }

private:
    CompositeTransformNode m_Transform;
    btPairCachingGhostObject* m_ControllerGhostObject;
    btConvexShape* m_CollisionShape;
    unsigned int m_Jumps;
    unsigned int m_MaxJumps;
    float m_Height;
    PhysicsManager& m_PhysicsManager;
};

#endif // BULLETCHARACTERCONTROLLER_H_INCLUDED
