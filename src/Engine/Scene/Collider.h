#ifndef COLLIDER_H_INCLUDED
#define COLLIDER_H_INCLUDED

#include <Physics/PhysicsManager.h>
#include "Component.h"

class GameObject;

class Collider : public Component
{
    GAME_OBJECT_COMPONENT_TYPE(e_TypeCollider)

public:
    friend class RigidBody;

    enum ColliderType
    {
        e_ColliderTypeStaticInfinitePlane = 0,
        e_ColliderTypeCapsule = 1,
        e_ColliderTypeCone = 2,
        e_ColliderTypeBox,
        e_ColliderTypeSphere,
        e_ColliderTypeMesh,

        e_ColliderTypeCount // Not a collider type (used for counting)
    };

    virtual ~Collider();

    inline ColliderType GetColliderType() const
    {
        return m_ColliderType;
    }

    void SetEnabled(bool val);

    /**
     * @param value - True if this collider is a trigger, false otherwise
     */
    void SetTrigger(bool value);
    bool IsTrigger();

    void SetSceneTransform(const SceneTypes::Transform& t);

    /**
     * Set how much offset the collider center is from the main transform set by the "SetSceneTransform()".
     * If "offset == (0.0, 0.0, 0.0)" then the collider center is not offset from the main transform,
     * hence its center is at the position set by the "SetSceneTransform()".
     */
    void SetCenterPositionOffset(const glm::vec3& offset);

    inline void SetCenterXPositionOffset(float offset)
    {
        glm::vec3 offsetVec = GetCenterPositionOffset();
        offsetVec.x = offset;
        SetCenterPositionOffset( offsetVec );
    }

    inline void SetCenterYPositionOffset(float offset)
    {
        glm::vec3 offsetVec = GetCenterPositionOffset();
        offsetVec.y = offset;
        SetCenterPositionOffset( offsetVec );
    }

    inline void SetCenterZPositionOffset(float offset)
    {
        glm::vec3 offsetVec = GetCenterPositionOffset();
        offsetVec.z = offset;
        SetCenterPositionOffset( offsetVec );
    }

    const glm::vec3 GetCenterPositionOffset() const;

    inline float GetCenterXPositionOffset() const
    {
        glm::vec3 offset = GetCenterPositionOffset();
        return offset.x;
    }

    inline float GetCenterYPositionOffset() const
    {
        glm::vec3 offset = GetCenterPositionOffset();
        return offset.y;
    }

    inline float GetCenterZPositionOffset() const
    {
        glm::vec3 offset = GetCenterPositionOffset();
        return offset.z;
    }

protected:
    // The constructor is protected because this object is not supposed to be instanciated on its own
    Collider(GameObject& owningGameObject,
             ColliderType type,
             const std::string& name,
             PhysicsManager& physicsManager);

    inline PhysicsManager::ColliderHandle& _GetColliderHandle()
    {
        return m_ColliderHandle;
    }

    inline const PhysicsManager::CollisionShapeHandle& _GetCollisionShapeHandle() const
    {
        return m_CollisionShapeHandle;
    }

    inline PhysicsManager::CollisionShapeHandle& _GetCollisionShapeHandle()
    {
        return m_CollisionShapeHandle;
    }

    void _InitializeAsDisabled();
    void _Initialize(PhysicsManager::CollisionShapeHandle shape);
    void _ChangeCollisionShape(PhysicsManager::CollisionShapeHandle newShape);
    void _ClearCollisionShape();
    void _RemoveFromThePhysicsWorld();
    void _AddToThePhysicsWorld();

    inline bool IsInitialised() const
    {
        return m_Initialised;
    }

    PhysicsManager& m_PhysicsManager;

private:
    void _Reinitialize(PhysicsManager::CollisionShapeHandle shape);

    bool m_Initialised;
    ColliderType m_ColliderType;
    PhysicsManager::CollisionShapeHandle m_CollisionShapeHandle;
    PhysicsManager::ColliderHandle m_ColliderHandle;
};

#endif // COLLIDER_H_INCLUDED
