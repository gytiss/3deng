#ifndef POINTLIGHMARKER_H_INCLUDED
#define POINTLIGHMARKER_H_INCLUDED

#include "EntityMarker.h"

class PointLighMarker : public EntityMarker
{
public:
    PointLighMarker(PhysicsManager& physicsManager,
                    MaterialManager& materialManager,
                    TextureManager& textureManager);
    void RecalculateRenderables() override;

private:
    void _SetVertexData(VBO* vbo);
    size_t _GeneratePointVertices();
    const float c_UnitRadius;
    VertexArray_t m_VertexBuffer;
    size_t m_FirstCircleVertexCount;
    size_t m_SecondCircleVertexCount;
    size_t m_ThirdCircleVertexCount;
    size_t m_PointsVertexCount;
};

#endif // POINTLIGHMARKER_H_INCLUDED
