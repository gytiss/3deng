#ifndef IMAGEUTILS_H_INCLUDED
#define IMAGEUTILS_H_INCLUDED

#include <string>
#include <cstdint>

/**
 * This class handles RGB or RGBA with top-left origin
 */

class TGAImage;
class FileManager;

class ImageUtils
{
public:
    enum ImageType { TGA = 0, PNG = 1, JPG, BMP };
    enum Format { RGB = 0, BGR = 1, RGBA = 2, BGRA = 3 };
    enum ImageOrigin { UPPER_LEFT = 0, LOWER_LEFT = 1 };

    struct ImageData_t
    {
        unsigned char depth;
        unsigned char bytesPerPixel;
        Format format;
        size_t w; // Width
        size_t h; // Height
        uint8_t* data;
    };

    /**
     * @param fileManager
     * @param it - Image type
     * @param dataBuffer - 4 bytes aligned pointer to a data buffer
     * @param dataBufferSizeInBytes - Total size of the data buffer in bytes
     * @param imageOrigin - Origin (Default: LOWER_LEFT)
     */
    static ImageData_t* LoadImage( FileManager& fileManager,
                                   ImageUtils::ImageType it,
                                   const uint8_t* dataBuffer,
                                   size_t dataBufferSizeInBytes,
                                   ImageOrigin imageOrigin = LOWER_LEFT ); // OpenGL texture origin is at the lower left corner

    static ImageData_t* LoadImage( FileManager& fileManager,
                                   ImageUtils::ImageType it,
                                   std::string fileName,
                                   ImageOrigin imageOrigin = LOWER_LEFT ); // OpenGL texture origin is at the lower left corner

    static bool SaveImage( ImageUtils::ImageType it,
                            std::string fileName,
                            const ImageUtils::ImageData_t* im );

    static void DeleteImage( const ImageUtils::ImageData_t* im );

    /**
     * Both dest and src images must be of the same depth
     * i.e. RGB and RGB or RGBA and RGBA.
     *
     * Src Image must be smaller than or equal to the rectangle
     * formed by the x,y coordinates and the width,height of the
     * dest image.
     */
    static bool InsertImage( ImageUtils::ImageData_t* destIm,
                             const ImageUtils::ImageData_t* srcIm,
                             size_t x,
                             size_t y );

    static ImageData_t* BlankImage( size_t width,
                                    size_t height,
                                    Format format,
                                    uint8_t firstChannelInitialValue = 0,
                                    uint8_t secondChannelInitialValue = 0,
                                    uint8_t thirdChannelInitialValue = 0,
                                    uint8_t fourthChannelInitialValue = 0 );

    static void ConvertRGBImageToRGBAImage( ImageUtils::ImageData_t* im );

    static void FlipVertically( ImageUtils::ImageData_t* im );

    static void FlipHorizontally( ImageUtils::ImageData_t* im );

    static void FlipVertically( uint8_t* data,
                                size_t width,
                                size_t height,
                                unsigned int bytesPerPixel );

    static void FlipHorizontally( uint8_t* data,
                                  size_t width,
                                  size_t height,
                                  unsigned int bytesPerPixel );
private:
    static void TGAImageToImageData(const TGAImage& tgaImage, ImageData_t& outImageData);
};


#endif // IMAGEUTILS_H_INCLUDED
