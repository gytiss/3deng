#ifndef GUIEVENT_H_INCLUDED
#define GUIEVENT_H_INCLUDED

#include <Rocket/Core/EventListener.h>

class GUIEventManager;

class GUIEvent : public Rocket::Core::EventListener
{
public:
	GUIEvent(const Rocket::Core::String& value,
             GUIEventManager* eventManager);
	virtual ~GUIEvent();

	/**
	 * Sends the event value through to the event processing system.
	 */
	virtual void ProcessEvent(Rocket::Core::Event& event);

	/**
	 * Destroys the event.
	 */
	virtual void OnDetach(Rocket::Core::Element* element);

private:
	Rocket::Core::String m_Value;
	GUIEventManager* m_EventManager;
};

#endif // GUIEVENT_H_INCLUDED
