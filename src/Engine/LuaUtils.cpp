#include "LuaUtils.h"
#include <MsgLogger.h>
#include <sstream>

#include <luabind/luabind.hpp>
#include <CommonDefinitions.h>
#include <Resources/FileManager.h>
#include <memory>

namespace LuaUtils
{
    std::string GetLuaErrorMsg(lua_State* L)
    {
        std::string errMsg = "";

        // Log the error message
        luabind::object msg( luabind::from_stack( L, -1 ) );
        std::ostringstream str;
        str << "lua> run-time error: " << msg;

        errMsg += str.str();

        // Log the callstack
        std::string traceback = luabind::call_function<std::string>( luabind::globals(L)["debug"]["traceback"] );
        traceback = std::string( "lua> " ) + traceback;

        errMsg += "\n";
        errMsg += traceback;

        return errMsg;
    }

    //-----------------------------------------------------------------------

    bool LoadLuaScript(lua_State* L, const std::string& scriptPath, FileManager& fileManager)
    {
        bool retVal = false;

        FileManager::File scriptFile;

        if( fileManager.OpenFile( scriptPath, FileManager::e_FileTypeBinary, scriptFile ) )
        {
            const size_t scriptDataSize = scriptFile.Size();
            std::unique_ptr<char[]> scriptData( new char[scriptDataSize] );

            if( scriptFile.Read( scriptData.get(), 1, scriptDataSize ) )
            {
                retVal = true;

                const int loadErrorCode = luaL_loadbuffer( L,
                                                           scriptData.get(),
                                                           scriptDataSize,
                                                           scriptPath.c_str() );

                if( loadErrorCode == LUA_ERRSYNTAX )
                {
                    retVal = false;
                    std::string msg = "Failed with the below syntax errors during the precompilation of file '";
                    msg += scriptPath;
                    msg += "':\n";
                    msg += GetLuaErrorMsg( L );
                    MsgLogger::LogError( msg );
                }
                else if( loadErrorCode == LUA_ERRMEM )
                {
                    MsgLogger::LogError( "Lua memory allocation error while trying to load file '" + scriptPath + "'" );
                    retVal = false;
                }

                if( retVal && ( lua_pcall( L, 0, LUA_MULTRET, 0 ) != 0 ) )
                {
                    MsgLogger::LogError( "Failed to run file '" + scriptPath + "'" );
                    retVal = false;
                }
            }
        }

        return retVal;
    }

    //-----------------------------------------------------------------------

    LuaStateWrapper::LuaStateWrapper()
    : m_LuaState( nullptr )
    {
        m_LuaState = luaL_newstate();
    }

    //-----------------------------------------------------------------------

//    LuaStateWrapper::LuaStateWrapper(lua_State* val)
//    : m_LuaState( val )
//    {
//
//    };

    //-----------------------------------------------------------------------

    LuaStateWrapper::~LuaStateWrapper()
    {
        if( m_LuaState != nullptr )
        {
            lua_close( m_LuaState );
        }
    }

    //-----------------------------------------------------------------------

    LuaStateWrapper::operator lua_State* ()
    {
        return m_LuaState;
    }

    //-----------------------------------------------------------------------
}
