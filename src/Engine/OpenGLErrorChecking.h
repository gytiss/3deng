#ifndef OPENGLERRORCHECKING_H_INCLUDED
#define OPENGLERRORCHECKING_H_INCLUDED

#include <string>

#define CheckForGLErrors() _CheckForGLErrors_(__FILE__, __LINE__)
void _CheckForGLErrors_(const std::string& fileName, int lineNum);

#endif // OPENGLERRORCHECKING_H_INCLUDED
