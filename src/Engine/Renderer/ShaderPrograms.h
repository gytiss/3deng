#ifndef SHADERPROGRAMS_H_INCLUDED
#define SHADERPROGRAMS_H_INCLUDED

#include <GraphicsDevice/ShaderProg.h>
#include <GraphicsDevice/VBO.h>
#include "ShaderProgUniforms.h"

//-----------------------------------------------------------------------

class ShaderProgGenericTextured :
    public ShaderProg,
    public ShaderProgUniformModelViewProjectionMatrix
{
public:
    ShaderProgGenericTextured(FileManager& fileManager);
};

//-----------------------------------------------------------------------

class ShaderProgEntityMarker :
    public ShaderProg,
    public ShaderProgUniformModelViewProjectionMatrix,
    public ShaderProgUniformColor
{
public:
    ShaderProgEntityMarker(FileManager& fileManager);
};

//-----------------------------------------------------------------------

class ShaderProgGBuffer :
    public ShaderProg,
    public ShaderProgUniformModelViewProjectionMatrix,
    public ShaderProgUniformNormalMatrix
{
public:
    ShaderProgGBuffer(FileManager& fileManager,
                      unsigned int vertexAttributes,
                      const std::string& vertexShaderDefines,
                      const std::string& fragmentShaderDefines);
};

//-----------------------------------------------------------------------

class ShaderProgGBufferSkinned :
    public ShaderProgGBuffer,
    public ShaderProgUniformJointMatrices
{
public:
    ShaderProgGBufferSkinned(FileManager& fileManager);
};

//-----------------------------------------------------------------------

class ShaderProgGBufferStatic :
    public ShaderProgGBuffer
{
public:
    ShaderProgGBufferStatic(FileManager& fileManager);
};

//-----------------------------------------------------------------------

class ShaderProgPointLightUberShader :
    public ShaderProg,
    public ShaderProgUniformModelViewProjectionMatrix,
    public ShaderProgUniformModelViewMatrix,
    public ShaderProgUniformProjectionA,
    public ShaderProgUniformProjectionB,
    public ShaderProgUniformScreenSize,
    public ShaderProgUniformLightColor,
    public ShaderProgUniformLightPositionInViewSpace,
    public ShaderProgUniformLightRadius,
    public ShaderProgUniformLightAmbientIntensity,
    public ShaderProgUniformLightDiffuseIntensity
{
public:
    ShaderProgPointLightUberShader(FileManager& fileManager);
};

//-----------------------------------------------------------------------

class ShaderProgDirectionalLightUberShader :
    public ShaderProg,
    public ShaderProgUniformInverseProjectionMatrix,
    public ShaderProgUniformProjectionA,
    public ShaderProgUniformProjectionB,
    public ShaderProgUniformScreenSize,
    public ShaderProgUniformLightColor,
    public ShaderProgUniformLightDirectionInViewSpace,
    public ShaderProgUniformLightAmbientIntensity,
    public ShaderProgUniformLightDiffuseIntensity
{
public:
    ShaderProgDirectionalLightUberShader(FileManager& fileManager);
};

//-----------------------------------------------------------------------

class ShaderProgSpotLightUberShader :
    public ShaderProg,
    public ShaderProgUniformModelViewProjectionMatrix,
    public ShaderProgUniformModelViewMatrix,
    public ShaderProgUniformProjectionA,
    public ShaderProgUniformProjectionB,
    public ShaderProgUniformScreenSize,
    public ShaderProgUniformLightColor,
    public ShaderProgUniformLightDirectionInViewSpace,
    public ShaderProgUniformLightPositionInViewSpace,
    public ShaderProgUniformLightRadius,
    public ShaderProgUniformLightAmbientIntensity,
    public ShaderProgUniformLightDiffuseIntensity,
    public ShaderProgUniformLightCutoffValue
{
public:
    ShaderProgSpotLightUberShader(FileManager& fileManager);
};

//-----------------------------------------------------------------------

class ShaderProgLightingStencilPass :
    public ShaderProg,
    public ShaderProgUniformModelViewProjectionMatrix
{
public:
    ShaderProgLightingStencilPass(FileManager& fileManager);
};

//-----------------------------------------------------------------------

class ShaderProgToneMappingAndGammaCorrection :
    public ShaderProg,
    public ShaderProgUniformScreenSize
{
public:
    ShaderProgToneMappingAndGammaCorrection(FileManager& fileManager);
};

//-----------------------------------------------------------------------

class ShaderProgDebugDepth :
    public ShaderProg,
    public ShaderProgUniformScreenSize,
    public ShaderProgUniformProjectionA,
    public ShaderProgUniformProjectionB
{
public:
    ShaderProgDebugDepth(FileManager& fileManager);
};

//-----------------------------------------------------------------------

class ShaderProgDebugObjects :
    public ShaderProg,
    public ShaderProgUniformModelViewProjectionMatrix,
    public ShaderProgUniformColor,
    public ShaderProgUniformUseVertexColors
{
public:
    ShaderProgDebugObjects(FileManager& fileManager);
};

//-----------------------------------------------------------------------

class ForwardRenderingShader :
    public ShaderProg,
    public ShaderProgUniformModelViewProjectionMatrix,
    public ShaderProgUniformViewMatrix,
    public ShaderProgUniformModelMatrix,
    public ShaderProgUniformNormalMatrix,
    public ShaderProgUniformLightColor,
    public ShaderProgUniformLightAmbientIntensity,
    public ShaderProgUniformLightDiffuseIntensity
{
public:
    ForwardRenderingShader(FileManager& fileManager,
                           unsigned int vertexAttributes,
                           const std::string& vertexShaderDefines,
                           const std::string& fragmentShaderDefines);
};

//-----------------------------------------------------------------------

class ForwardRenderingPointLightShader :
    public ForwardRenderingShader,
    public ShaderProgUniformLightPositionInViewSpace,
    public ShaderProgUniformLightRadius
{
public:
    ForwardRenderingPointLightShader(FileManager& fileManager,
                                     unsigned int vertexAttributes,
                                     const std::string& vertexShaderDefines,
                                     const std::string& fragmentShaderDefines);
};

//-----------------------------------------------------------------------

class ForwardRenderingSpotLightShader :
    public ForwardRenderingShader,
    public ShaderProgUniformLightPositionInViewSpace,
    public ShaderProgUniformLightDirectionInViewSpace,
    public ShaderProgUniformLightCutoffValue,
    public ShaderProgUniformLightRadius
{
public:
    ForwardRenderingSpotLightShader(FileManager& fileManager,
                                    unsigned int vertexAttributes,
                                    const std::string& vertexShaderDefines,
                                    const std::string& fragmentShaderDefines);
};

//-----------------------------------------------------------------------

class ForwardRenderingDirectionalShader :
    public ForwardRenderingShader,
    public ShaderProgUniformLightDirectionInViewSpace
{
public:
    ForwardRenderingDirectionalShader(FileManager& fileManager,
                                      unsigned int vertexAttributes,
                                      const std::string& vertexShaderDefines,
                                      const std::string& fragmentShaderDefines);
};

//-----------------------------------------------------------------------

template<class T>
class ForwardRenderingShaderSkinned :
    public T,
    public ShaderProgUniformJointMatrices
{
public:
    ForwardRenderingShaderSkinned(FileManager& fileManager)
    : T( fileManager,
         VBO::e_VertexAttrBoneWeights |
         VBO::e_VertexAttrBoneIndices,
         "RENDERING_SKINNED_MESH",
                              "" ),
      ShaderProgUniformJointMatrices( this )
    {

    }
};

typedef ForwardRenderingShaderSkinned<ForwardRenderingPointLightShader> ForwardRenderingPointLightShaderSkinned;
typedef ForwardRenderingShaderSkinned<ForwardRenderingSpotLightShader> ForwardRenderingSpotLightShaderSkinned;
typedef ForwardRenderingShaderSkinned<ForwardRenderingDirectionalShader> ForwardRenderingDirectionalLightShaderSkinned;

//-----------------------------------------------------------------------

template<class T>
class ForwardRenderingShaderStatic :
    public T
{
public:
    ForwardRenderingShaderStatic(FileManager& fileManager)
    : T( fileManager, 0, "", "" )
    {

    }
};

typedef ForwardRenderingShaderStatic<ForwardRenderingPointLightShader> ForwardRenderingPointLightShaderStatic;
typedef ForwardRenderingShaderStatic<ForwardRenderingSpotLightShader> ForwardRenderingSpotLightShaderStatic;
typedef ForwardRenderingShaderStatic<ForwardRenderingDirectionalShader> ForwardRenderingDirectionalLightShaderStatic;

//-----------------------------------------------------------------------

class ForwardRenderingNoLightingShader :
    public ShaderProg,
    public ShaderProgUniformModelViewProjectionMatrix
{
public:
    ForwardRenderingNoLightingShader(FileManager& fileManager,
                                     unsigned int vertexAttributes,
                                     const std::string& vertexShaderDefines,
                                     const std::string& fragmentShaderDefines);
};

//-----------------------------------------------------------------------

class ForwardRenderingNoLightingShaderSkinned :
    public ForwardRenderingNoLightingShader,
    public ShaderProgUniformJointMatrices
{
public:
    ForwardRenderingNoLightingShaderSkinned(FileManager& fileManager);
};

//-----------------------------------------------------------------------

class ForwardRenderingNoLightingShaderStatic :
    public ForwardRenderingNoLightingShader
{
public:
    ForwardRenderingNoLightingShaderStatic(FileManager& fileManager);
};

//-----------------------------------------------------------------------

class ShaderProgGUI :
    public ShaderProg,
    public ShaderProgUniformModelMatrix,
    public ShaderProgUniformUseTexture
{
public:
    ShaderProgGUI(FileManager& fileManager);
};

//-----------------------------------------------------------------------

class ShaderProgText :
    public ShaderProg,
    public ShaderProgUniformModelViewProjectionMatrix
{
public:
    ShaderProgText(FileManager& fileManager);
};



//-----------------------------------------------------------------------

#endif // SHADERPROGRAMS_H_INCLUDED
