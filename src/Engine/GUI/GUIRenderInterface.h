#ifndef GUIRENDERINTERFACE_H_INCLUDED
#define GUIRENDERINTERFACE_H_INCLUDED

#include <Rocket/Core/RenderInterface.h>
#include <GraphicsDevice/VAO.h>
#include <Renderer/RenderableMeshInterface.h>
#include <glm/glm.hpp>
#include <vector>

class VBO;
class IBO;
class Texture;
class ShaderProgGUI;
class GraphicsDevice;
class FileManager;

/**
 * Low level OpenGL render interface for Rocket
 */
class GUIRenderInterface : public Rocket::Core::RenderInterface
{
public:
    GUIRenderInterface(const std::string& guiDataRootDirectoryPath,
                       GraphicsDevice& graphicsDevice,
                       FileManager& fileManager,
                       size_t windowWidth,
                       size_t windowHeight);

    virtual void Render(ShaderProgGUI& shader);

    /**
     * @param width width of viewport
     * @param height height of viewport
     */
    void SetViewport(int width, int height);

    /// Called by Rocket when it wants to render geometry that it does not wish to optimise.
    virtual void RenderGeometry(Rocket::Core::Vertex* vertices, int num_vertices, int* indices, int num_indices, Rocket::Core::TextureHandle texture, const Rocket::Core::Vector2f& translation);

    /// Called by Rocket when it wants to compile geometry it believes will be static for the forseeable future.
    virtual Rocket::Core::CompiledGeometryHandle CompileGeometry(Rocket::Core::Vertex* vertices, int num_vertices, int* indices, int num_indices, Rocket::Core::TextureHandle texture);

    /// Called by Rocket when it wants to render application-compiled geometry.
    virtual void RenderCompiledGeometry(Rocket::Core::CompiledGeometryHandle geometry, const Rocket::Core::Vector2f& translation);
    /// Called by Rocket when it wants to release application-compiled geometry.
    virtual void ReleaseCompiledGeometry(Rocket::Core::CompiledGeometryHandle geometry);

    /// Called by Rocket when it wants to enable or disable scissoring to clip content.
    virtual void EnableScissorRegion(bool enable);
    /// Called by Rocket when it wants to change the scissor region.
    virtual void SetScissorRegion(int x, int y, int width, int height);

    /// Called by Rocket when a texture is required by the library.
    virtual bool LoadTexture(Rocket::Core::TextureHandle& texture_handle, Rocket::Core::Vector2i& texture_dimensions, const Rocket::Core::String& source);
    /// Called by Rocket when a texture is required to be built from an internally-generated sequence of pixels.
    virtual bool GenerateTexture(Rocket::Core::TextureHandle& texture_handle, const Rocket::Core::byte* source, const Rocket::Core::Vector2i& source_dimensions);
    /// Called by Rocket when a loaded texture is no longer required.
	virtual void ReleaseTexture(Rocket::Core::TextureHandle texture_handle);

private:
    struct GUIDataVertex
    {
        float position[3];
        float color[4];
        float texCoord[2];
    };

    struct GLScissorRect
    {
        int x;
        int y;
        int width;
        int height;
    };

    struct GUIDrawCall
    {
        GUIDrawCall()
        : modelMatrix(),
          texture( nullptr ),
          firstVertexIndex( 0 ),
          vertexIndexCount( 0 ),
          scissorEnabled( false ),
          scissorRect()
        {
        }

        glm::mat4 modelMatrix;
        Texture* texture;
        size_t firstVertexIndex;
        size_t vertexIndexCount;

        bool scissorEnabled;
        GLScissorRect scissorRect;
    };

    typedef std::vector<GUIDataVertex> VertexList;
    typedef std::vector<EngineTypes::VertexIndexType> IndexList;
    typedef std::vector<GUIDrawCall> GUIDrawCallList;

    GraphicsDevice& m_GraphicsDevice;
    FileManager& m_FileManager;

    size_t m_Width;
    size_t m_Height;

    VAO m_VAO;
    VBO* m_VBO;
    IBO* m_IBO;

    IndexList m_IndexList;
    VertexList m_VertexList;
    GUIDrawCallList m_DrawCallList;
    bool m_GLScissorEnabled;
    GLScissorRect m_ScissorRect;

    const std::string m_DataRootDirPath;
};

#endif // GUIRENDERINTERFACE_H_INCLUDED
